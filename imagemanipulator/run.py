"""
Starts the vision web service.
Port is taken from the environment
"""
import os
from app import app

if __name__ == '__main__':
    port = int(os.environ.get('IMAGE_MANIPULATOR_PORT', 15000))
    app.run(host='0.0.0.0',
            port=port,
            threaded=True,
	    use_reloader=False,
            debug=False)
