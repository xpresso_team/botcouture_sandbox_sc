# README #

Implementation of Image Manipulator

### What is this repository for? ###

It is the implementation of image manipulator. It is based on python/flask framework. It manipulates the image by adding padding and return the updated image.
It handles following request now:
#1) GET:  '/pad/<size>/' #
      -> size - size of pad to be added
      -> url - url of the image to be updated
      -> Add padding to the image url with the size and returns the updated image.

### How do I get set up? ###

* Summary of set up

```
 /bin/bash setup.sh
```

* Configuration
* Dependencies

Dependencies are listed in requirements.txt file

* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
