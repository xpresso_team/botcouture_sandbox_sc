"""
 It is a helper class which standardizes
 every error messages that is sent back to client
"""

from flask import jsonify


class APIError:
    # Success
    OK = 0

    # PARAMETER
    INVALID_PARAMETER = -1


    ERROR_CODES_MESSAGES = {
        OK: "OK",
        INVALID_PARAMETER: "Invalid parameter",
    }

    @staticmethod
    def get_error_message(error_code):
        """
            Based on the error code provided, it creates a standard error message and reply back.
        """
        if error_code in APIError.ERROR_CODES_MESSAGES:
            return jsonify({"status_code": error_code, "status_msg": APIError.ERROR_CODES_MESSAGES.get(error_code)})
        else:
            return jsonify({"status_code": -9999, "status_msg": "Unexpected error"})
