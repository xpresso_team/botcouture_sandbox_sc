"""
    Initialize flask framework.
    Setup configuration and logger.
    Everyone uses the app object from here
    to get info about the service.
"""
import logging
import sys
from flask import Flask
from flask_cors import CORS

"""
    Initialize the Flask application
"""
app = Flask(__name__)

"""
    Setting the required configuration
"""
# To allow cross domain access
app.config.from_envvar('IMAGE_MANIPULATOR_CONFIG_FILE')
cors = CORS(app)

# Setting up logger
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s-%(filename)s:%(lineno)d:%(funcName)s()- %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

# Setting up the routes
from app import views


