"""
    Contains all route served by the service currently.
"""

import os
import io
import os.path
import stat
import re
import time
import urllib

from flask import request, jsonify, redirect
from flask import send_from_directory
from flask_cors import cross_origin
from werkzeug.utils import secure_filename

from app.APIError import APIError
from app import app, logger, utils

import cv2
import numpy as np
import skimage.io as skio
import hashlib
import traceback
import hashlib
import PIL.Image
import time

# Create the upload folder if does not exist
if not os.path.exists(app.config['UPLOAD_FOLDER']):
    os.makedirs(app.config['UPLOAD_FOLDER'])

# Create the temp folder if does not exist
if not os.path.exists(app.config['TEMP_FOLDER']):
    os.makedirs(app.config['TEMP_FOLDER'])

''' get image in np.ndarray type'''
def getImage(im ):
    """ 
    input image -np.ndarray, image url, or path
    """
    if isinstance(im, str):
        if "http" in im: 
            file = io.BytesIO(urllib.urlopen(im).read())
        else:
            file = im
        im_origpil = PIL.Image.open(file)
        im_orig = np.array(im_origpil)
        if len(im_orig.shape)==3 and im_orig.shape[2]==4: #convert png to jpg
             im_origpil.load()
             im_orig = PIL.Image.new("RGB", im_origpil.size, (255,255,255))
             im_orig.paste(im_origpil, mask = im_origpil.split()[3]) #3 is the alpha channel
             im_orig = np.array(im_orig)
    else:
        im_orig = np.array(im)

    if len(im_orig.shape)==3 and im_orig.shape[2]==4:
        im_orig = im_orig[:,:,:3]
    return im_orig

@app.route('/replay/', methods=['GET'])
def replay():
    """
     Takes image url as parameter and returns url by hosting it here.
    """
    if 'url' not in request.args:
        return APIError.get_error_message(APIError.INVALID_PARAMETER)
    url = request.args.get('url')
    image_id = utils.generate_random_word(32) + ".jpg"
    image_file = app.config['TEMP_FOLDER'] + image_id
    urllib.urlretrieve(url, image_file)
    os.chmod(image_file, stat.S_IRUSR|stat.S_IWUSR)
    skio.imsave(image_file,img) #cv2.imwrite(image_file, img)
    try:
        im_orig = skio.imread(imUrl)
    except:
        print('Error in image url')
    logger.info(image_file  )
    try:
       return send_from_directory(app.config['TEMP_FOLDER'],
                                 image_id)
    except:
       return redirect(url)

@app.route('/recenter_noseg/', methods=['GET'])
@cross_origin()
def recenter_noseg():
    """
     Takes image url as parameter and add padding to its image.
    """
    st_st = time.clock()
    if 'url' not in request.args:
        return APIError.get_error_message(APIError.INVALID_PARAMETER)

    if 'resizeh' not in request.args:
        targ_sizeH = 600
    else:
        resize_str = request.args.get('resizeh')
        try:
            targ_sizeH = int(resize_str)
        except:
            targ_sizeH = 600
            traceback.print_exc()
            print("Target size is not valid - " + resize_str) 

    if 'aspect' not in request.args: # widthxheight
        targ_asp = None
    else:
        aspect_str = request.args.get('aspect')
        if 'x' not in aspect_str: 
            targ_asp = None
        else:
            aspect_splt = aspect_str.split('x')
            targ_asp = float(aspect_splt[0])/float(aspect_splt[1])

    
    # padding goes here
    # store in temporary file
    url = request.args.get('url')
    print(url)
    m = hashlib.md5()
    m.update(url.encode('utf-8'))
    print(url)
#    if '.png' in url:
#        ext = '.png'
#    elif '.tif' in url:
#        ext = '.tif'
#    else:
#        ext = '.jpg'
#    image_id = utils.generate_random_word(32) + ext
    ext = '.jpg'
    image_id = m.hexdigest() + ext
    image_file = app.config['TEMP_FOLDER'] + image_id
    if os.path.exists(image_file):
        imurl = app.config['STATIC'] + image_id
#        return redirect(imurl)
        return send_from_directory(app.config['TEMP_FOLDER'],
                                 image_id)
#        urllib.URLopener.version = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36 SE 2.X MetaSr 1.0'
#        urllib.urlretrieve(url, image_file)
#        print( "Fetching and saving image file at " + image_file)
    try:
#        im_orig = skio.imread(image_file)
        st = time.clock()
        im_orig = getImage(str(url))
        print("Fetched image " + str(url) + " in " +  str(time.clock()-st) + " seconds.")
#        if len(im_orig.shape) == 3 and im_orig.shape[2] == 4:
#            im_orig = im_orig[:,:,:3]
    except:
        print('Error in image url: ' + url + ', imagefile saved at:' + image_file)
        traceback.print_exc()
        return redirect(url)
    print(im_orig.shape)
    if len(im_orig.shape) == 0:
        return redirect(url)

    st = time.clock()
    im_recenter = im_orig
    if targ_sizeH is not None:
        newH = targ_sizeH
        newW = im_recenter.shape[1]*newH/im_recenter.shape[0]
        im_recenter = cv2.resize(im_recenter,(newW,newH))
    print("Resized image " + str(url) + " in " +  str(time.clock()-st) + " seconds.")

    st = time.clock()
    if targ_asp is not None: 
        curr_asp = im_recenter.shape[1]*1.0/im_recenter.shape[0]
        if targ_asp < curr_asp: # pad height
            pad_h = int(( (im_recenter.shape[1]*1.0/targ_asp) - im_recenter.shape[0])/2)
            pad_w = 0
        else: # pad width
            pad_w = int(( (im_recenter.shape[0]*targ_asp) - im_recenter.shape[1])/2)
            pad_h = 0
        im_recenter = cv2.copyMakeBorder(im_recenter,pad_h,pad_h,pad_w,pad_w, cv2.BORDER_CONSTANT, value=(255,255,255,255))
    print("Recentered image " + str(url) + " in " +  str(time.clock()-st) + " seconds.")
    try:
        skio.imsave(image_file,im_recenter) #cv2.imwrite(image_file, img)
        os.chmod(image_file, stat.S_IRUSR|stat.S_IWUSR|stat.S_IRGRP|stat.S_IWGRP|stat.S_IXGRP|stat.S_IROTH|stat.S_IWOTH)
        logger.info(image_file  )

        print("All processing for image  " + str(url) + " in " +  str(time.clock()-st_st) + " seconds.")
        imurl = app.config['STATIC'] + image_id
#        return redirect(imurl)
        return send_from_directory(app.config['TEMP_FOLDER'],
                                 image_id)
    except:
        return redirect(url)




@app.route('/recenter/', methods=['GET'])
@cross_origin()
def recenter():
    """
     Takes image url as parameter and add padding to its image.
    """
    if 'url' not in request.args:
        return APIError.get_error_message(APIError.INVALID_PARAMETER)
    if 'aspect' not in request.args: # widthxheight
        targ_asp = None
    else:
        aspect_str = request.args.get('aspect')
        if 'x' not in aspect_str: 
            targ_asp = None
        else:
            aspect_splt = aspect_str.split('x')
            targ_asp = float(aspect_splt[0])/float(aspect_splt[1])

    
    # padding goes here
    # store in temporary file
    url = request.args.get('url')
    if '.png' in url:
        ext = '.png'
    elif '.tif' in url:
        ext = '.tif'
    else:
        ext = '.jpg'

    image_id = utils.generate_random_word(32) + ext
    image_file = app.config['TEMP_FOLDER'] + image_id
    urllib.URLopener.version = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36 SE 2.X MetaSr 1.0'
    urllib.urlretrieve(url, image_file)
    os.chmod(image_file, stat.S_IRUSR|stat.S_IWUSR)
    img = segment_plainbg(image_file, targ_asp=targ_asp)
    if img is None:
       return redirect(url)
    skio.imsave(image_file,img) #cv2.imwrite(image_file, img)
    logger.info(image_file  )
    try:
       return send_from_directory(app.config['TEMP_FOLDER'],
                                 image_id)
    except:
       return redirect(url)

def segment_plainbg(imUrl, targ_asp=None):
    try:
        im_orig = skio.imread(imUrl)
#        if len(im_orig.shape) == 3 and im_orig.shape[2] == 4:
#            im_orig = im_orig[:,:,:3]
    except:
        print('Error in image url')
        traceback.print_exc()
        return None
    im = cv2.resize( im_orig, (120,120*im_orig.shape[0]/im_orig.shape[1]));
    pad = 30
    im_padded = cv2.copyMakeBorder(im, pad, pad, pad, pad, cv2.BORDER_REPLICATE)
    bgdModel = np.zeros((1,65),np.float64)
    fgdModel = np.zeros((1,65),np.float64)
    mask = np.zeros(im_padded.shape[:2],np.uint8)
    rect = (pad,pad,im_padded.shape[1]-pad,im_padded.shape[0]-pad)
    cv2.rectangle(mask,rect[:2],rect[2:],int(cv2.GC_PR_FGD), thickness=-1) # -ve thickness draws a filled rectangle
    cv2.grabCut(im_padded,mask,rect,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_MASK)
    mask2 = np.where((mask==2)|(mask==0),0,1).astype('uint8')
    mask2 = mask2[pad:pad+im.shape[0], pad:pad+im.shape[1]]

    mask_orig = cv2.resize(mask2,(im_orig.shape[1],im_orig.shape[0]))
    x,y,w,h = cv2.boundingRect(mask_orig)
    print(x,y,w,h)
    mask_orig = mask_orig[:,:,np.newaxis]
    #im_seg = im_orig*mask_orig

    if w > 50 and h > 50:
        im_recenter = im_orig[y:y+h, x:x+w, :]
    else:
        im_recenter = im_orig

    if targ_asp is not None: 
        curr_asp = im_recenter.shape[1]*1.0/im_recenter.shape[0]
        if targ_asp < curr_asp: # pad height
            pad_h = int(( (im_recenter.shape[1]*1.0/targ_asp) - im_recenter.shape[0])/2)
            pad_w = 0
        else: # pad width
            pad_w = int(( (im_recenter.shape[0]*targ_asp) - im_recenter.shape[1])/2)
            pad_h = 0
#        bkg_pixels = im[ mask2 == 0 ].mean()
#        print( curr_asp, targ_asp, pad_w, pad_h, bkg_pixels)
        im_recenter = cv2.copyMakeBorder(im_recenter,pad_h,pad_h,pad_w,pad_w, cv2.BORDER_CONSTANT, value=(255,255,255,255))

    return im_recenter
 
