"""
    Contains utility methods
"""
import random
import string


def generate_random_word(length):
    return ''.join(random.choice(string.lowercase) for i in range(length))
