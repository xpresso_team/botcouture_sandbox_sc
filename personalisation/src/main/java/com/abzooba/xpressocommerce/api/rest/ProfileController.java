package com.abzooba.xpressocommerce.api.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.abzooba.xpressocommerce.domain.Profile;
import com.abzooba.xpressocommerce.dto.ProfileDTO;
import com.abzooba.xpressocommerce.service.ProfileService;
import com.abzooba.xpressocommerce.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/dbservice/v1/profile")
@Api(value = "profile", description = "Profile API")
public class ProfileController extends AbstractRestHandler {

	@Autowired
	private UserService userService;

	@Autowired
	private ProfileService profileService;

	@RequestMapping(value = "", method = RequestMethod.POST, consumes = { "application/json" }, produces = { "application/json" })
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Create a Profile resource.", notes = "Returns the URL of the new resource in the Location header.")
	public void createprofile(@RequestBody ProfileDTO profileDTO, HttpServletRequest request, HttpServletResponse response) {
		Profile createdProfile = this.profileService.createProfile(profileDTO);
		response.setHeader("Location", request.getRequestURL().append("/").append(createdProfile.getId()).toString());
	}

	@RequestMapping(value = "", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get the profile of user given the entity.", notes = "You have to provide a valid user ID.")
	public @ResponseBody Profile getProfile(@ApiParam(value = "The UserID", required = true) @RequestParam(value = "psid", required = true) String psid, @ApiParam(value = "The entity", required = true) @RequestParam(value = "entity", required = true) String entity, @ApiParam(value = "The gender", required = true) @RequestParam(value = "gender", required = true) String gender, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Profile profile = this.profileService.getProfile(psid, entity, gender);
		checkResourceFound(profile);
		return profile;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get the profile of user.", notes = "You have to provide a valid user ID.")
	public @ResponseBody Page<Profile> getProfile(@ApiParam(value = "The UserID", required = true) @RequestParam(value = "psid", required = true) String psid, @ApiParam(value = "The page number (zero-based)", required = true) @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) Integer page, @ApiParam(value = "Tha page size", required = true) @RequestParam(value = "size", required = true, defaultValue = DEFAULT_PAGE_SIZE) Integer size, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Page<Profile> profile = this.profileService.getProfile(psid, page, size);
		checkResourceFound(profile);
		return profile;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = { "application/json" })
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Delete All profiles.", notes = "You have to provide a valid user ID. Once deleted the resource can not be recovered.")
	public void deleteProfile(@ApiParam(value = "The UserID", required = true) @RequestParam(value = "psid", required = true) String psid, HttpServletRequest request, HttpServletResponse response) {
		checkResourceFound(this.userService.getUser(psid));
		this.profileService.deleteProfiles(psid);
	}
}
