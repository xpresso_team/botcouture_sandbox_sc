package com.abzooba.xpressocommerce.api.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.abzooba.xpressocommerce.domain.User;
import com.abzooba.xpressocommerce.exception.DataFormatException;
import com.abzooba.xpressocommerce.service.CartService;
import com.abzooba.xpressocommerce.service.FeedbackService;
import com.abzooba.xpressocommerce.service.ProfileService;
import com.abzooba.xpressocommerce.service.UserService;
import com.abzooba.xpressocommerce.service.ViewedProductService;
import com.abzooba.xpressocommerce.service.WishlistService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/*
 * Demonstrates how to set up RESTful API endpoints using Spring MVC
 */

@RestController
@RequestMapping(value = "/dbservice/v1/users")
@Api(value = "users", description = "User API")
public class UserController extends AbstractRestHandler {

	@Autowired
	private UserService userService;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private WishlistService wishlistService;

	@Autowired
	private CartService cartService;

	@Autowired
	private FeedbackService feedbackService;

	@Autowired
	private ViewedProductService viewedProductService;

	@RequestMapping(value = "", method = RequestMethod.POST, consumes = { "application/json" }, produces = { "application/json" })
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Create a user resource.", notes = "Returns the URL of the new resource in the Location header.")
	public void createUser(@RequestBody User user, HttpServletRequest request, HttpServletResponse response) {
		User createdUser = this.userService.createUser(user);
		response.setHeader("Location", request.getRequestURL().append("/").append(createdUser.getPsid()).toString());
	}

	@RequestMapping(value = "", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get a paginated list of all users.", notes = "The list is paginated. You can provide a page number (default 0) and a page size (default 100)")
	public @ResponseBody Page<User> getAllUser(@ApiParam(value = "The page number (zero-based)", required = true) @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) Integer page, @ApiParam(value = "Tha page size", required = true) @RequestParam(value = "size", required = true, defaultValue = DEFAULT_PAGE_SIZE) Integer size, HttpServletRequest request, HttpServletResponse response) {
		return this.userService.getAllUsers(page, size);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get a single user.", notes = "You have to provide a valid user ID.")
	public @ResponseBody User getUser(@ApiParam(value = "The ID of the user.", required = true) @PathVariable("id") String id, HttpServletRequest request, HttpServletResponse response) throws Exception {
		User user = this.userService.getUser(id);
		checkResourceFound(user);
		return user;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = { "application/json" }, produces = { "application/json" })
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Update a user resource.", notes = "You have to provide a valid user ID in the URL and in the payload. The ID attribute can not be updated.")
	public void updateUser(@ApiParam(value = "The ID of the existing user resource.", required = true) @PathVariable("id") String id, @RequestBody User user, HttpServletRequest request, HttpServletResponse response) {
		checkResourceFound(this.userService.getUser(id));
		if (!id.equals(user.getPsid()))
			throw new DataFormatException("ID doesn't match!");
		this.userService.updateUser(user);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = { "application/json" })
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Delete a user resource.", notes = "You have to provide a valid user ID in the URL. Once deleted the resource can not be recovered.")
	public void deleteUser(@ApiParam(value = "The ID of the existing user resource.", required = true) @PathVariable("id") String id, HttpServletRequest request, HttpServletResponse response) {
		checkResourceFound(this.userService.getUser(id));
		this.profileService.deleteProfiles(id);
		this.wishlistService.deleteWishlist(id, null, null);
		this.cartService.deleteCart(null, id, null);
		this.feedbackService.deleteFeedback(id);
		this.viewedProductService.deleteViewedProduct(id);
		this.userService.deleteUser(id);
	}
}
