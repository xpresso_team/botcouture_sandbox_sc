package com.abzooba.xpressocommerce.api.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.abzooba.xpressocommerce.domain.ViewedProduct;
import com.abzooba.xpressocommerce.dto.ViewedProductDTO;
import com.abzooba.xpressocommerce.service.ViewedProductService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/dbservice/v1/viewedproduct")
@Api(value = "viewedproduct", description = "ViewedProduct API")
public class ViewedProductController extends AbstractRestHandler {

	@Autowired
	private ViewedProductService viewedProductService;

	@RequestMapping(value = "", method = RequestMethod.POST, consumes = { "application/json" }, produces = { "application/json" })
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Create a ViewedProduct resource.", notes = "Returns the URL of the new resource in the Location header.")
	public void createViewedproduct(@RequestBody ViewedProductDTO viewedProductDTO, HttpServletRequest request, HttpServletResponse response) {
		ViewedProduct createdViewedProduct = this.viewedProductService.createViewedProduct(viewedProductDTO);
		response.setHeader("Location", request.getRequestURL().append("/").append(createdViewedProduct.getId()).toString());
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = { "application/json" })
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Delete viewedProducts of user.", notes = "You have to provide a valid user ID. Once deleted the resource can not be recovered.")
	public void deleteViewedProduct(@ApiParam(value = "The UserID", required = true) @RequestParam(value = "psid", required = true) String psid, HttpServletRequest request, HttpServletResponse response) {
		this.viewedProductService.deleteViewedProduct(psid);
	}

}
