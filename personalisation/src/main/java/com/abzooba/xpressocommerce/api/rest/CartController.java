package com.abzooba.xpressocommerce.api.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.abzooba.xpressocommerce.domain.Cart;
import com.abzooba.xpressocommerce.dto.CartDTO;
import com.abzooba.xpressocommerce.service.CartService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/dbservice/v1/cart")
@Api(value = "cart", description = "Cart API")
public class CartController extends AbstractRestHandler {

	@Autowired
	private CartService cartService;

	@RequestMapping(value = "", method = RequestMethod.POST, consumes = { "application/json" }, produces = { "application/json" })
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Create a Cart resource.", notes = "Returns the URL of the new resource in the Location header.")
	public void createCart(@RequestBody CartDTO cartDTO, HttpServletRequest request, HttpServletResponse response) {
		Cart createdCart = this.cartService.createCart(cartDTO);
		response.setHeader("Location", request.getRequestURL().append("/").append(createdCart.getId()).toString());
	}

	@RequestMapping(value = "", method = RequestMethod.PUT, consumes = { "application/json" }, produces = { "application/json" })
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Update a cart resource.", notes = "You have to provide a valid cart ID in the URL and in the payload. The ID attribute can not be updated.")
	public void updateCart(@RequestBody CartDTO cartDTO, HttpServletRequest request, HttpServletResponse response) {
		checkResourceFound(this.cartService.getCart(cartDTO.getPsid()));
		this.cartService.updateCart(cartDTO);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get the cart of a user.", notes = "You have to provide a valid user ID.")
	public @ResponseBody Page<Cart> getCart(@ApiParam(value = "The UserID", required = true) @RequestParam(value = "psid", required = true) String psid, @ApiParam(value = "The Catalog", required = false) @RequestParam(value = "catalog", required = false) String catalog, @ApiParam(value = "The page number (zero-based)", required = true) @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) Integer page, @ApiParam(value = "Tha page size", required = true) @RequestParam(value = "size", required = true, defaultValue = DEFAULT_PAGE_SIZE) Integer size, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Page<Cart> cart = this.cartService.getAllCart(psid, catalog, page, size);
		return cart;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = { "application/json" })
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Delete either entire cart or cart of a particular catalog or a product in cart of catalog.", notes = "You have to provide a valid user ID. Once deleted the resource can not be recovered.")
	public void deleteCart(@ApiParam(value = "The UserID", required = true) @RequestParam(value = "psid", required = true) String psid, @ApiParam(value = "The Catalog", required = false) @RequestParam(value = "catalog", required = false) String catalog, @ApiParam(value = "The ProductID", required = false) @RequestParam(value = "xc_sku", required = false) String xc_sku, HttpServletRequest request, HttpServletResponse response) {
		this.cartService.deleteCart(xc_sku, psid, catalog);
	}
}
