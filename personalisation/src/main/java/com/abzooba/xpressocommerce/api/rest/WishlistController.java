package com.abzooba.xpressocommerce.api.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.abzooba.xpressocommerce.dto.CartDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.abzooba.xpressocommerce.domain.Wishlist;
import com.abzooba.xpressocommerce.dto.WishlistDTO;
import com.abzooba.xpressocommerce.service.WishlistService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/dbservice/v1/wishlist")
@Api(value = "wishlist", description = "Wishlist API")
public class WishlistController extends AbstractRestHandler {

	@Autowired
	private WishlistService wishlistService;

	@RequestMapping(value = "", method = RequestMethod.POST, consumes = { "application/json" }, produces = { "application/json" })
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Create a Wishlist resource.", notes = "Returns the URL of the new resource in the Location header.")
	public void createWishlist(@RequestBody WishlistDTO wishlistDTO, HttpServletRequest request, HttpServletResponse response) {
		Wishlist createdWishlist = this.wishlistService.createWislist(wishlistDTO);
		response.setHeader("Location", request.getRequestURL().append("/").append(createdWishlist.getId()).toString());
	}

	@RequestMapping(value = "", method = RequestMethod.PUT, consumes = { "application/json" }, produces = { "application/json" })
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Update a Wishlist resource.", notes = "You have to provide a valid PSID ID in the URL and in the payload. The ID attribute can not be updated.")
	public void updateCart(@RequestBody WishlistDTO wishlistDTO, HttpServletRequest request, HttpServletResponse response) {
		checkResourceFound(this.wishlistService.getWishlist(wishlistDTO.getPsid()));
		this.wishlistService.updateWishlist(wishlistDTO);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get the wishlist of a user.", notes = "You have to provide a valid user ID.")
	public @ResponseBody Page<Wishlist> getWishlist(@ApiParam(value = "The UserID", required = true) @RequestParam(value = "psid", required = true) String psid, @ApiParam(value = "The Catalog", required = false) @RequestParam(value = "catalog", required = false) String catalog, @ApiParam(value = "The page number (zero-based)", required = true) @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) Integer page, @ApiParam(value = "Tha page size", required = true) @RequestParam(value = "size", required = true, defaultValue = DEFAULT_PAGE_SIZE) Integer size, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Page<Wishlist> wishlist = this.wishlistService.getAllWishlist(psid, catalog, page, size);
		return wishlist;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = { "application/json" })
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Delete either single product in wishlist or of a particular catalog or entire wishlist.", notes = "You have to provide a valid user ID. Once deleted the resource can not be recovered.")
	public void deleteWishlist(@ApiParam(value = "The UserID", required = true) @RequestParam(value = "psid", required = true) String psid, @ApiParam(value = "The Catalog", required = false) @RequestParam(value = "catalog", required = false) String catalog, @ApiParam(value = "The ProductID", required = false) @RequestParam(value = "xc_sku", required = false) String xc_sku, HttpServletRequest request, HttpServletResponse response) {
		this.wishlistService.deleteWishlist(psid, xc_sku, catalog);
	}
}
