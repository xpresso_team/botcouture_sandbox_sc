package com.abzooba.xpressocommerce.api.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.abzooba.xpressocommerce.domain.RetailerPreference;
import com.abzooba.xpressocommerce.dto.RetailerPreferenceDTO;
import com.abzooba.xpressocommerce.service.RetailerPreferenceService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/dbservice/v1/retailerPreference")
@Api(value = "retailerpreference", description = "RetailerPreference API")
public class RetailerPreferenceController extends AbstractRestHandler {

	@Autowired
	private RetailerPreferenceService retailerPreferenceService;

	@RequestMapping(value = "", method = RequestMethod.POST, consumes = { "application/json" }, produces = { "application/json" })
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Create a RetailerPreference resource.", notes = "Returns the URL of the new resource in the Location header.")
	public void createRetailerPreference(@RequestBody RetailerPreferenceDTO retailerPreferenceDTO, HttpServletRequest request, HttpServletResponse response) {
		RetailerPreference createdRetailerPreference = this.retailerPreferenceService.createRetailerPreference(retailerPreferenceDTO);
		response.setHeader("Location", request.getRequestURL().append("/").append(createdRetailerPreference.getId()).toString());
	}

	@RequestMapping(value = "", method = RequestMethod.PUT, consumes = { "application/json" }, produces = { "application/json" })
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Update a retailerPreference resource.", notes = "You have to provide a valid retailerPreference ID in the URL and in the payload. The ID attribute can not be updated.")
	public void updateRetailerPreference(@RequestBody RetailerPreferenceDTO retailerPreferenceDTO, HttpServletRequest request, HttpServletResponse response) {
		checkResourceFound(this.retailerPreferenceService.getRetailerPreference(retailerPreferenceDTO.getPsid()));
		this.retailerPreferenceService.updateRetailerPreference(retailerPreferenceDTO);
	}

	@RequestMapping(value = "/{psid}", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get the retailerPreference of a user.", notes = "You have to provide a valid user ID.")
	public @ResponseBody RetailerPreference getRetailerPreference(@ApiParam(value = "The ID of the user.", required = true) @PathVariable("psid") String psid, HttpServletRequest request, HttpServletResponse response) throws Exception {
		RetailerPreference retailerPreference = this.retailerPreferenceService.getRetailerPreference(psid);
		return retailerPreference;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = { "application/json" })
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Delete retailerPreference of a User", notes = "You have to provide a valid user ID. Once deleted the resource can not be recovered.")
	public void deleteRetailerPreference(@ApiParam(value = "The UserID", required = true) @RequestParam(value = "psid", required = true) String psid, HttpServletRequest request, HttpServletResponse response) {
		this.retailerPreferenceService.deleteRetailerPreference(psid);
	}
}
