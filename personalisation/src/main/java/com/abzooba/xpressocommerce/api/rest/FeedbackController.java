package com.abzooba.xpressocommerce.api.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.abzooba.xpressocommerce.domain.Feedback;
import com.abzooba.xpressocommerce.dto.FeedbackDTO;
import com.abzooba.xpressocommerce.service.FeedbackService;
import com.abzooba.xpressocommerce.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/dbservice/v1/feedback")
@Api(value = "Feedback", description = "Feedback API")
public class FeedbackController extends AbstractRestHandler {

	@Autowired
	private FeedbackService feedbackService;

	@Autowired
	private UserService userService;

	@RequestMapping(value = "", method = RequestMethod.POST, consumes = { "application/json" }, produces = { "application/json" })
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Create a Feedback resource.", notes = "Returns the URL of the new resource in the Location header.")
	public void createFeedback(@RequestBody FeedbackDTO feedbackDTO, HttpServletRequest request, HttpServletResponse response) {
		Feedback createdFeedback = this.feedbackService.createFeedback(feedbackDTO);
		response.setHeader("Location", request.getRequestURL().append("/").append(createdFeedback.getId()).toString());
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = { "application/json" })
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Delete all feedbacks of a User", notes = "You have to provide a valid user ID. Once deleted the resource can not be recovered.")
	public void deleteFeedback(@ApiParam(value = "The UserID", required = true) @RequestParam(value = "psid", required = true) String psid, HttpServletRequest request, HttpServletResponse response) {
		checkResourceFound(this.userService.getUser(psid));
		this.feedbackService.deleteFeedback(psid);
	}
}
