package com.abzooba.xpressocommerce.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/*
 * a simple domain entity doubling as a DTO
 */
@Entity
@Table(name = "product")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Product {

	@Id
	@Column(nullable = false)
	private String xcSku;

	@Column()
	private String productname;

	@Column()
	private String brand;

	@Column()
	private String client_name;

	@Column()
	private String image;

	@Column()
	private Double price;

	@Column()
	private String gender;

	@Column()
	private String product_url;

	//	@OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
	//	private Set<Wishlist> wishlist = new HashSet<>();

	//	@OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
	//	private Set<Wishlist> viewedProducts = new HashSet<>();

	public Product() {
	}

	public String getXc_sku() {
		return xcSku;
	}

	public void setXc_sku(String xc_sku) {
		this.xcSku = xc_sku;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getClient_name() {
		return client_name;
	}

	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getProduct_url() {
		return product_url;
	}

	public void setProduct_url(String product_url) {
		this.product_url = product_url;
	}

	//	public Set<Wishlist> getWishlist() {
	//		return wishlist;
	//	}
	//
	//	public void setWishlist(Set<Wishlist> wishlist) {
	//		this.wishlist = wishlist;
	//	}

}
