package com.abzooba.xpressocommerce.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.CreationTimestamp;

/*
 * a simple domain entity doubling as a DTO
 */
@Entity
@Table(name = "retailerpreference")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RetailerPreference {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column()
	private Long id;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "psid")
	private User user;

	@Column()
	private String page_id;

	@Column()
	private String channel_id;

	@Column()
	private String validity;

	@ElementCollection
	@Column()
	private List<String> retailers;

	@Column()
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;

	@PrePersist
	protected void onCreate() {
		createdAt = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPage_id() {
		return page_id;
	}

	public void setPage_id(String page_id) {
		this.page_id = page_id;
	}

	public String getChannel_id() {
		return channel_id;
	}

	public void setChannel_id(String channel_id) {
		this.channel_id = channel_id;
	}

	public String getValidity() {
		return validity;
	}

	public void setValidity(String validity) {
		this.validity = validity;
	}

	public List<String> getRetailers() {
		return retailers;
	}

	public void setRetailers(List<String> retailers) {
		this.retailers = retailers;
	}
}
