package com.abzooba.xpressocommerce.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.CreationTimestamp;

/*
 * a simple domain entity doubling as a DTO
 */
@Entity
@Table(name = "feedback")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Feedback {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column()
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "psid")
	private User user;

	@Column(columnDefinition = "TEXT")
	private String feedback_json_string;

	@Column()
	private String sentiment;

	@Column()
	private String feedback;

	@Column()
	private String page_id;

	@Column()
	private String channel_id;

	@Column()
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;

	@PrePersist
	protected void onCreate() {
		createdAt = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getSentiment() {
		return sentiment;
	}

	public void setSentiment(String sentiment) {
		this.sentiment = sentiment;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public Feedback() {

	}

	public String getPage_id() {
		return page_id;
	}

	public void setPage_id(String page_id) {
		this.page_id = page_id;
	}

	public String getChannel_id() {
		return channel_id;
	}

	public void setChannel_id(String channel_id) {
		this.channel_id = channel_id;
	}

	public String getFeedback_json_str() {
		return feedback_json_string;
	}

	public void setFeedback_json_str(String feedback_json_str) {
		this.feedback_json_string = feedback_json_str;
	}

}
