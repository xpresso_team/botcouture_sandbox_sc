package com.abzooba.xpressocommerce.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.CreationTimestamp;

/*
 * a simple domain entity doubling as a DTO
 */
@Entity
@Table(name = "profile")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Profile {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column()
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "psid")
	private User user;

	@Column()
	private String entity;

	@Column()
	private String aspect;

	@Column()
	private String size;

	@Column()
	private String brand;

	@Column()
	private String color;

	@Column()
	private String priceUpperLimit;

	@Column()
	private String priceLowerLimit;

	@Column()
	private String style;

	@Column()
	private String page_id;

	@Column()
	private String channel_id;

	@Column()
	private String gender;

	@Column()
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;

	public void setId(Long id) {
		this.id = id;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Profile() {

	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public void setAspect(String aspect) {
		this.aspect = aspect;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getEntity() {
		return entity;
	}

	public String getSize() {
		return size;
	}

	public String getBrand() {
		return brand;
	}

	public String getColor() {
		return color;
	}

	public String getAspect() {
		return aspect;
	}

	public Long getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	public String getPage_id() {
		return page_id;
	}

	public void setPage_id(String page_id) {
		this.page_id = page_id;
	}

	public String getChannel_id() {
		return channel_id;
	}

	public void setChannel_id(String channel_id) {
		this.channel_id = channel_id;
	}

	public String getPriceUpperLimit() {
		return priceUpperLimit;
	}

	public void setPriceUpperLimit(String priceUpperLimit) {
		this.priceUpperLimit = priceUpperLimit;
	}

	public String getPriceLowerLimit() {
		return priceLowerLimit;
	}

	public void setPriceLowerLimit(String priceLowerLimit) {
		this.priceLowerLimit = priceLowerLimit;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String toString() {
		return ":" + entity + ":\t:" + aspect + ":\t:" + size + ":\t:" + brand + ":\t:" + color + ":\t:" + priceUpperLimit + ":\t:" + priceLowerLimit + ":\t:" + style + ":\t:" + page_id + ":\t:" + channel_id + ":\t:" + gender + ":";
	}

	public Profile combine(Profile that){
		Profile combined = new Profile();
		combined.setUser(this.getUser());
		combined.setEntity(this.getEntity());
		combined.setAspect(this.getAspect());
		combined.setChannel_id(this.getChannel_id());
		combined.setPage_id(this.getPage_id());
		combined.setGender("["+this.getGender()+","+that.getGender()+"]");
		combined.setStyle(merge(this.getStyle(),that.getStyle()));
		combined.setSize(merge(this.getSize(),that.getSize()));
		combined.setBrand(merge(this.getBrand(),that.getBrand()));
		combined.setColor(merge(this.getColor(),that.getColor()));
		combined.setPriceLowerLimit(String.valueOf(Math.min(Float.valueOf(this.getPriceLowerLimit()),Float.valueOf(that.getPriceLowerLimit()))));
		combined.setPriceUpperLimit(String.valueOf(Math.max(Float.valueOf(this.getPriceUpperLimit()),Float.valueOf(that.getPriceUpperLimit()))));
		return combined;
	}

	private String merge(String s1, String s2) {
		if(s1.startsWith("[") || s2.startsWith("[")){
			String regex = "\\[|\\]";
			if(s1.startsWith("[") && s2.startsWith("[")){
				return "[" + s1.replaceAll(regex, "") + "," + s2.replaceAll(regex, "") + "]";
			}else{
				if(s2.startsWith("[")){
					return "[" + s1 + "," + s2.replaceAll(regex, "") + "]";
				}else{
					return "[" + s1.replaceAll(regex, "") + "," + s2 + "]";
				}
			}
		}else{
			return "["+s1+","+s2+"]";
		}
	}

}
