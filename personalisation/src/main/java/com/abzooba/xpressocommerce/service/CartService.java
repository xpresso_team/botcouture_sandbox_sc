package com.abzooba.xpressocommerce.service;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abzooba.xpressocommerce.api.rest.AbstractRestHandler;
import com.abzooba.xpressocommerce.dao.jpa.CartRepository;
import com.abzooba.xpressocommerce.dao.jpa.ProductRepository;
import com.abzooba.xpressocommerce.dao.jpa.UserRepository;
import com.abzooba.xpressocommerce.domain.Cart;
import com.abzooba.xpressocommerce.domain.Product;
import com.abzooba.xpressocommerce.domain.User;
import com.abzooba.xpressocommerce.dto.CartDTO;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

@Service
@Transactional
public class CartService {

	private static final Logger log = LoggerFactory.getLogger(UserService.class);
	private static List<String> arrayAttributes = Arrays.asList(new String[] { "colorsgroup", "xc_hierarchy", "xc_category", "colorsavailable", "size", "department", "subcategory", "category" });
	private static String customReturnAttributes = "colorsgroup,image,gender,colorsavailable,features,size,price,productname,details,department,subcategory,category,sku,brand,_score,xc_hierarchy,xc_sku,xc_category,client_name,product_url";

	@Autowired
	private CartRepository cartRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	CounterService counterService;

	@Autowired
	GaugeService gaugeService;

	@Value("${nlpservice.ip}")
	private String IP;

	@Value("${nlpservice.port}")
	private String PORT;

	public CartService() {

	}

	public Page<Cart> getAllCart(String psid, String catalog, Integer page, Integer size) {
		if (catalog == null || catalog.isEmpty()) {
			User user = userRepository.findOne(psid);
			Page<Cart> pageOfCart = cartRepository.findAllByUser(user, new PageRequest(page, size));
			return pageOfCart;
		} else {
			User user = userRepository.findOne(psid);
			Page<Cart> pageOfCart = cartRepository.findAllByUserAndCatalog(user, catalog, new PageRequest(page, size));
			return pageOfCart;
		}
	}

	public Cart createCart(CartDTO cartDTO) {
		User user = userRepository.findOne(cartDTO.getPsid());
		Product product = productRepository.findProductByXcSku(cartDTO.getXc_sku());
		Cart cart = cartRepository.findByUserAndCatalogAndProduct(user, cartDTO.getCatalog(), product);
		if(cart == null){
			cart = new Cart();
		}
		cart.setUser(user);
		if (product != null) {
			cart.setProduct(product);
		} else {
			product = fetchProduct(cartDTO.getXc_sku());
			productRepository.save(product);
			cart.setProduct(product);
		}
		cart.setChannel_id(cartDTO.getChannel_id());
		cart.setPage_id(cartDTO.getPage_id());
		cart.setCatalog(cartDTO.getCatalog());
		if (cartDTO.getColor() != null && !cartDTO.getColor().isEmpty()) {
			cart.setColor(cartDTO.getColor());
		}
		if (cartDTO.getQuantity() != null && !cartDTO.getQuantity().isEmpty()) {
			cart.setQuantity(cartDTO.getQuantity());
		}
		if (cartDTO.getSize() != null && !cartDTO.getSize().isEmpty()) {
			cart.setSize(cartDTO.getSize());
		}
		cartRepository.save(cart);
		return cart;
	}

	public static void main(String[] args) {

	}

	public Product fetchProduct(String xc_sku) {
		Product product = new Product();
		try {
			String URL = "http://" + IP + ":" + PORT + "/datalayer/v1/elasticsearch/genericAPI/?xc_sku=" + xc_sku + "&returnField=" + customReturnAttributes;
			HttpResponse<JsonNode> response = Unirest.get(URL).header("cache-control", "no-cache").header("postman-token", "65c03a42-6028-8033-1d8c-95d9027d4470").asJson();
			JSONObject result = DirtyFixJSON(response.getBody().getObject());
			product.setXc_sku(result.getString("xc_sku"));
			product.setProductname(result.getString("productname"));
			product.setBrand(result.getString("brand"));
			product.setClient_name(result.getString("client_name"));
			product.setImage(result.getString("image"));
			product.setPrice(result.getDouble("price"));
			product.setGender(result.getString("gender"));
			product.setProduct_url(result.getString("product_url"));
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return product;
	}

	private static JSONObject DirtyFixJSON(JSONObject jObject) {
		jObject = jObject.getJSONObject("Results");
		JSONObject result = new JSONObject();
		Iterator<?> keys = jObject.keys();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			if (arrayAttributes.contains(key)) {
				JSONArray value = jObject.getJSONArray(key);
				JSONArray finalValue = new JSONArray();
				for (int i = 0; i < value.length(); i++) {
					finalValue.put(((String) value.get(i)).replace("\"", ""));
				}
				result.put(key, finalValue);
			} else {
				result.put(key, jObject.getJSONArray(key).get(0));
			}
		}
		return result;
	}

	public List<Cart> getCart(String psid) {
		User user = userRepository.findOne(psid);
		return cartRepository.findAllByUser(user);
	}

	public List<Cart> getCart(String psid, String catalog) {
		User user = userRepository.findOne(psid);
		return cartRepository.findByUserAndCatalog(user, catalog);
	}

	public Cart getCart(String psid, String catalog, String xc_sku) {
		User user = userRepository.findOne(psid);
		Product product = productRepository.findOne(xc_sku);
		return cartRepository.findByUserAndCatalogAndProduct(user, catalog, product);
	}

	public void deleteCart(Cart cart) {
		cartRepository.delete(cart);
	}

	public void updateCart(CartDTO cartDTO) {
		Cart cart = getCart(cartDTO.getPsid(), cartDTO.getCatalog(), cartDTO.getXc_sku());
		if (cartDTO.getColor() != null && !cartDTO.getColor().isEmpty()) {
			cart.setColor(cartDTO.getColor());
		}
		if (cartDTO.getQuantity() != null && !cartDTO.getQuantity().isEmpty()) {
			cart.setQuantity(cartDTO.getQuantity());
		}
		if (cartDTO.getSize() != null && !cartDTO.getSize().isEmpty()) {
			cart.setSize(cartDTO.getSize());
		}
		cartRepository.save(cart);
	}

	public void deleteCart(String xc_sku, String psid, String catalog) {
		if (xc_sku == null || xc_sku.isEmpty()) {
			if (catalog == null || catalog.isEmpty()) {
				// Clear All
				List<Cart> cart = getCart(psid);
				for (Cart w : cart) {
					AbstractRestHandler.checkResourceFound(cart);
					deleteCart(w);
				}
			} else {
				// Clear cart of a catalog
				List<Cart> cart = getCart(psid, catalog);
				for (Cart w : cart) {
					AbstractRestHandler.checkResourceFound(cart);
					deleteCart(w);
				}
			}
		} else {
			Cart cart = getCart(psid, catalog, xc_sku);
			AbstractRestHandler.checkResourceFound(cart);
			deleteCart(cart);
		}
	}
}
