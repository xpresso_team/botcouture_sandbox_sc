package com.abzooba.xpressocommerce.service;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.abzooba.xpressocommerce.domain.Wishlist;
import com.abzooba.xpressocommerce.dto.WishlistDTO;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abzooba.xpressocommerce.api.rest.AbstractRestHandler;
import com.abzooba.xpressocommerce.dao.jpa.ProductRepository;
import com.abzooba.xpressocommerce.dao.jpa.UserRepository;
import com.abzooba.xpressocommerce.dao.jpa.WishlistRepository;
import com.abzooba.xpressocommerce.domain.Product;
import com.abzooba.xpressocommerce.domain.User;
import com.abzooba.xpressocommerce.domain.Wishlist;
import com.abzooba.xpressocommerce.dto.WishlistDTO;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

@Service
@Transactional
public class WishlistService {

	private static final Logger log = LoggerFactory.getLogger(UserService.class);
	private static List<String> arrayAttributes = Arrays.asList(new String[] { "colorsgroup", "xc_hierarchy", "xc_category", "colorsavailable", "size", "department", "subcategory", "category" });
	private static String customReturnAttributes = "colorsgroup,image,gender,colorsavailable,features,size,price,productname,details,department,subcategory,category,sku,brand,_score,xc_hierarchy,xc_sku,xc_category,client_name,product_url";

	@Autowired
	private WishlistRepository wishlistRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	CounterService counterService;

	@Autowired
	GaugeService gaugeService;

	@Value("${nlpservice.ip}")
	private String IP;

	@Value("${nlpservice.port}")
	private String PORT;

	public WishlistService() {

	}

	public Page<Wishlist> getAllWishlist(String psid, String catalog, Integer page, Integer size) {
		if (catalog == null || catalog.isEmpty()) {
			User user = userRepository.findOne(psid);
			Page<Wishlist> pageOfWishlist = wishlistRepository.findAllByUser(user, new PageRequest(page, size));
			return pageOfWishlist;
		} else {
			User user = userRepository.findOne(psid);
			Page<Wishlist> pageOfWishlist = wishlistRepository.findAllByUserAndCatalog(user, catalog, new PageRequest(page, size));
			return pageOfWishlist;

		}
	}

	public Wishlist createWislist(WishlistDTO wishlistDTO) {
		User user = userRepository.findOne(wishlistDTO.getPsid());
		Product product = productRepository.findProductByXcSku(wishlistDTO.getXc_sku());
		Wishlist wishlist = wishlistRepository.findByUserAndCatalogAndProduct(user, wishlistDTO.getCatalog(), product);
		if(wishlist == null){
			wishlist = new Wishlist();
		}
		wishlist.setUser(user);
		if (product != null) {
			wishlist.setProduct(product);
		} else {
			product = fetchProduct(wishlistDTO.getXc_sku());
			productRepository.save(product);
			wishlist.setProduct(product);
		}
		wishlist.setChannel_id(wishlistDTO.getChannel_id());
		wishlist.setPage_id(wishlistDTO.getPage_id());
		wishlist.setCatalog(wishlistDTO.getCatalog());
		if (wishlistDTO.getColor() != null && !wishlistDTO.getColor().isEmpty()) {
			wishlist.setColor(wishlistDTO.getColor());
		}
		if (wishlistDTO.getSize() != null && !wishlistDTO.getSize().isEmpty()) {
			wishlist.setSize(wishlistDTO.getSize());
		}
		wishlistRepository.save(wishlist);
		return wishlist;
	}

	public void updateWishlist(WishlistDTO wishlistDTO) {
		Wishlist wishlist = getWishlist(wishlistDTO.getPsid(), wishlistDTO.getCatalog(), wishlistDTO.getXc_sku());
		if (wishlistDTO.getColor() != null && !wishlistDTO.getColor().isEmpty()) {
			wishlist.setColor(wishlistDTO.getColor());
		}
		if (wishlistDTO.getSize() != null && !wishlistDTO.getSize().isEmpty()) {
			wishlist.setSize(wishlistDTO.getSize());
		}
		wishlistRepository.save(wishlist);
	}

	public Product fetchProduct(String xc_sku) {
		Product product = new Product();
		try {
			String URL = "http://" + IP + ":" + PORT + "/datalayer/v1/elasticsearch/genericAPI/?xc_sku=" + xc_sku + "&returnField=" + customReturnAttributes;
			HttpResponse<JsonNode> response = Unirest.get(URL).header("cache-control", "no-cache").header("postman-token", "65c03a42-6028-8033-1d8c-95d9027d4470").asJson();
			JSONObject result = DirtyFixJSON(response.getBody().getObject());
			product.setXc_sku(result.getString("xc_sku"));
			product.setProductname(result.getString("productname"));
			product.setBrand(result.getString("brand"));
			product.setClient_name(result.getString("client_name"));
			product.setImage(result.getString("image"));
			product.setPrice(result.getDouble("price"));
			product.setGender(result.getString("gender"));
			product.setProduct_url(result.getString("product_url"));
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return product;
	}

	private static JSONObject DirtyFixJSON(JSONObject jObject) {
		jObject = jObject.getJSONObject("Results");
		JSONObject result = new JSONObject();
		Iterator<?> keys = jObject.keys();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			if (arrayAttributes.contains(key)) {
				JSONArray value = jObject.getJSONArray(key);
				JSONArray finalValue = new JSONArray();
				for (int i = 0; i < value.length(); i++) {
					finalValue.put(((String) value.get(i)).replace("\"", ""));
				}
				result.put(key, finalValue);
			} else {
				result.put(key, jObject.getJSONArray(key).get(0));
			}
		}
		return result;
	}

	public void deleteWishlist(Wishlist wishlist) {
		wishlistRepository.delete(wishlist);
	}

	public List<Wishlist> getWishlist(String psid) {
		User user = userRepository.findOne(psid);
		return wishlistRepository.findAllByUser(user);
	}

	public List<Wishlist> getWishlist(String psid, String catalog) {
		User user = userRepository.findOne(psid);
		return wishlistRepository.findByUserAndCatalog(user, catalog);
	}

	public Wishlist getWishlist(String psid, String catalog, String xc_sku) {
		User user = userRepository.findOne(psid);
		Product product = productRepository.findOne(xc_sku);
		return wishlistRepository.findByUserAndCatalogAndProduct(user, catalog, product);
	}

	public void deleteWishlist(String psid, String xc_sku, String catalog) {
		if (xc_sku == null || xc_sku.isEmpty()) {
			if (catalog == null || catalog.isEmpty()) {
				// Clear All
				List<Wishlist> wishlist = getWishlist(psid);
				for (Wishlist w : wishlist) {
					AbstractRestHandler.checkResourceFound(wishlist);
					deleteWishlist(w);
				}
			} else {
				// Clear wishlist of a catalog
				List<Wishlist> wishlist = getWishlist(psid, catalog);
				for (Wishlist w : wishlist) {
					AbstractRestHandler.checkResourceFound(wishlist);
					deleteWishlist(w);
				}
			}
		} else {
			System.out.println(psid);
			System.out.println(catalog);
			System.out.println(xc_sku);
			Wishlist wishlist = getWishlist(psid, catalog, xc_sku);
			System.out.println(wishlist);
			AbstractRestHandler.checkResourceFound(wishlist);
			deleteWishlist(wishlist);
		}
	}
}
