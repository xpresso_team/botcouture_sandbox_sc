package com.abzooba.xpressocommerce.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abzooba.xpressocommerce.api.rest.AbstractRestHandler;
import com.abzooba.xpressocommerce.dao.jpa.RetailerPreferenceRepository;
import com.abzooba.xpressocommerce.dao.jpa.UserRepository;
import com.abzooba.xpressocommerce.domain.RetailerPreference;
import com.abzooba.xpressocommerce.domain.User;
import com.abzooba.xpressocommerce.dto.RetailerPreferenceDTO;

@Service
@Transactional
public class RetailerPreferenceService {

	private static final Logger log = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private RetailerPreferenceRepository RetailerPreferenceRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	CounterService counterService;

	@Autowired
	GaugeService gaugeService;

	@Value("${nlpservice.ip}")
	private String IP;

	@Value("${nlpservice.port}")
	private String PORT;

	public RetailerPreferenceService() {

	}

	public RetailerPreference createRetailerPreference(RetailerPreferenceDTO retailerPreferenceDTO) {
		RetailerPreference retailerPreference = getRetailerPreference(retailerPreferenceDTO.getPsid());
		if(retailerPreference == null)
			retailerPreference = new RetailerPreference();
		User user = userRepository.findOne(retailerPreferenceDTO.getPsid());
		retailerPreference.setUser(user);
		retailerPreference.setChannel_id(retailerPreferenceDTO.getChannel_id());
		retailerPreference.setPage_id(retailerPreferenceDTO.getPage_id());
		retailerPreference.setValidity(retailerPreferenceDTO.getValidity());
		retailerPreference.setRetailers(retailerPreferenceDTO.getRetailers());
		RetailerPreferenceRepository.save(retailerPreference);
		return retailerPreference;
	}

	public void deleteRetailerPreference(RetailerPreference retailerPreference) {
		RetailerPreferenceRepository.delete(retailerPreference);
	}

	public void updateRetailerPreference(RetailerPreferenceDTO retailerPreferenceDTO) {
		RetailerPreference retailerPreference = getRetailerPreference(retailerPreferenceDTO.getPsid());
		if (retailerPreferenceDTO.getChannel_id() != null) {
			retailerPreference.setChannel_id(retailerPreferenceDTO.getChannel_id());
		}
		if (retailerPreferenceDTO.getPage_id() != null) {
			retailerPreference.setPage_id(retailerPreferenceDTO.getPage_id());
		}
		if (retailerPreferenceDTO.getValidity() != null) {
			retailerPreference.setValidity(retailerPreferenceDTO.getValidity());
		}
		if (retailerPreferenceDTO.getRetailers() != null) {
			retailerPreference.setRetailers(retailerPreferenceDTO.getRetailers());
		}
		RetailerPreferenceRepository.save(retailerPreference);
	}

	public RetailerPreference getRetailerPreference(String psid) {
		User user = userRepository.findOne(psid);
		return RetailerPreferenceRepository.findByUser(user);
	}

	public void deleteRetailerPreference(String psid) {
		RetailerPreference retailerPreference = getRetailerPreference(psid);
		AbstractRestHandler.checkResourceFound(retailerPreference);
		deleteRetailerPreference(retailerPreference);
	}
}
