package com.abzooba.xpressocommerce.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abzooba.xpressocommerce.dao.jpa.FeedbackRepository;
import com.abzooba.xpressocommerce.dao.jpa.UserRepository;
import com.abzooba.xpressocommerce.domain.Feedback;
import com.abzooba.xpressocommerce.domain.User;
import com.abzooba.xpressocommerce.dto.FeedbackDTO;

@Service
@Transactional
public class FeedbackService {

	private static final Logger log = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private FeedbackRepository feedbackRepository;

	@Autowired
	CounterService counterService;

	@Autowired
	GaugeService gaugeService;

	public FeedbackService() {

	}

	public Feedback createFeedback(FeedbackDTO feedbackDTO) {
		Feedback feedback = new Feedback();
		User user = userRepository.findOne(feedbackDTO.getPsid());
		feedback.setUser(user);
		feedback.setSentiment("");
		feedback.setFeedback("");
		feedback.setChannel_id(feedbackDTO.getChannel_id());
		feedback.setPage_id(feedbackDTO.getPage_id());
		feedback.setFeedback_json_str(feedbackDTO.getFeedback_json());
		feedbackRepository.save(feedback);
		return feedback;
	}

	public List<Feedback> getFeedback(String psid) {
		User user = userRepository.findOne(psid);
		return feedbackRepository.findAllByUser(user);
	}

	public void deleteFeedback(String psid) {
		List<Feedback> feedbacks = getFeedback(psid);
		for (Feedback feedback : feedbacks) {
			feedbackRepository.delete(feedback);
		}
	}

}
