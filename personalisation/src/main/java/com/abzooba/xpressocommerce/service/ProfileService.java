package com.abzooba.xpressocommerce.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abzooba.xpressocommerce.dao.jpa.ProfileRepository;
import com.abzooba.xpressocommerce.dao.jpa.UserRepository;
import com.abzooba.xpressocommerce.domain.Profile;
import com.abzooba.xpressocommerce.domain.User;
import com.abzooba.xpressocommerce.dto.ProfileDTO;

@Service
@Transactional
public class ProfileService {
	private static final Logger log = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	CounterService counterService;

	@Autowired
	GaugeService gaugeService;

	public ProfileService() {

	}

	public void deleteProfile(Long id) {
		profileRepository.delete(id);
	}

	public void deleteProfiles(String psid) {
		User user = userRepository.findOne(psid);
		Page<Profile> profiles = profileRepository.findAllByUser(user, new PageRequest(0, Integer.MAX_VALUE));
		for (Profile profile : profiles) {
			this.deleteProfile(profile.getId());
		}
	}

	public Page<Profile> getProfile(String psid, Integer page, Integer size) {
		User user = userRepository.findOne(psid);
		Page<Profile> profile = profileRepository.findAllByUser(user, new PageRequest(page, size));
		return profile;
	}

	public Profile getProfile(String psid, String entity, String gender) {
		User user = userRepository.findOne(psid);
		Profile profile = null ;
		if(gender.contains(",")){
			profileRepository.findByUserAndEntityAndGender(user, entity, gender);
			Profile menProfile = profileRepository.findByUserAndEntityAndGender(user, entity, "men");
			Profile womenProfile = profileRepository.findByUserAndEntityAndGender(user, entity, "women");
			if(menProfile != null && womenProfile!= null ){
				profile = menProfile.combine(womenProfile);
			}else{
				if(menProfile != null ){
					profile = menProfile;
				}else{
					if(womenProfile != null ){
						profile = womenProfile;
					}else{
						profile = new Profile();
					}
				}
			}
		}else{
			profile = profileRepository.findByUserAndEntityAndGender(user, entity, gender);
		}
		return profile;
	}

	@Transactional
	public synchronized Profile createProfile(ProfileDTO profileDTO) {
		User user = userRepository.findOne(profileDTO.getPsid());
		String entity = profileDTO.getEntity().toLowerCase();
		String gender = profileDTO.getGender().toLowerCase();
		Profile profile = profileRepository.findByUserAndEntityAndGender(user, entity,gender);
		if (profile == null) {
			profile = new Profile();
			profile.setUser(user);
			profile.setEntity(entity);
			profile.setGender(gender);
		}
		if (profileDTO.getSize() != null)
			profile.setSize(profileDTO.getSize());
		if (profileDTO.getBrand() != null)
			profile.setBrand(profileDTO.getBrand());
		if (profileDTO.getColor() != null)
			profile.setColor(profileDTO.getColor());
		if (profileDTO.getAspect() != null)
			profile.setAspect(profileDTO.getAspect());
		if (profileDTO.getPriceLowerLimit() != null)
			profile.setPriceLowerLimit(profileDTO.getPriceLowerLimit());
		if (profileDTO.getPriceUpperLimit() != null)
			profile.setPriceUpperLimit(profileDTO.getPriceUpperLimit());
		if (profileDTO.getStyle() != null)
			profile.setStyle(profileDTO.getStyle());

		profile.setChannel_id(profileDTO.getChannel_id());
		profile.setPage_id(profileDTO.getPage_id());
		profileRepository.saveAndFlush(profile);
		return profile;
	}
}
