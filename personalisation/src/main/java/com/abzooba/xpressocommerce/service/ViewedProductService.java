package com.abzooba.xpressocommerce.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abzooba.xpressocommerce.dao.jpa.ProductRepository;
import com.abzooba.xpressocommerce.dao.jpa.UserRepository;
import com.abzooba.xpressocommerce.dao.jpa.ViewedProductRepository;
import com.abzooba.xpressocommerce.domain.Product;
import com.abzooba.xpressocommerce.domain.User;
import com.abzooba.xpressocommerce.domain.ViewedProduct;
import com.abzooba.xpressocommerce.dto.ViewedProductDTO;

@Service
@Transactional
public class ViewedProductService {
	private static final Logger log = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private ViewedProductRepository viewedProductRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	CounterService counterService;

	@Autowired
	GaugeService gaugeService;

	public ViewedProductService() {
	}

	public ViewedProduct createViewedProduct(ViewedProductDTO viewedProductDTO) {
		ViewedProduct viewedProduct = new ViewedProduct();
		User user = userRepository.findOne(viewedProductDTO.getPsid());
		viewedProduct.setUser(user);
		Product product = productRepository.findProductByXcSku(viewedProductDTO.getXc_sku());
		if (product != null) {
			viewedProduct.setProduct(product);
		} else {
			product = fetchProduct(viewedProductDTO.getXc_sku());
			productRepository.save(product);
			viewedProduct.setProduct(product);
		}
		viewedProduct.setChannel_id(viewedProductDTO.getChannel_id());
		viewedProduct.setPage_id(viewedProductDTO.getPage_id());
		viewedProduct.setProduct_url(viewedProductDTO.getProduct_url());
		viewedProductRepository.save(viewedProduct);
		return viewedProduct;
	}

	public Product fetchProduct(String xc_sku) {
		Product product = new Product();
		//		HttpResponse<String> response = Unirest.get("http://localhost:8090/dbservice/v1/users/1015332708577738").header("content-type", "application/json").header("cache-control", "no-cache").header("postman-token", "1eff7ae3-1407-e5d0-4e1f-11e2ac4a04e4").asString();
		product.setXc_sku(xc_sku);
		product.setProductname("quicksilver men's cotton shirt");
		product.setBrand("quiksilver");
		product.setPrice(54.0);
		return product;
	}

	public List<ViewedProduct> getAllViewedProduct(String psid) {
		User user = userRepository.findOne(psid);
		List<ViewedProduct> pageOfViewedProduct = viewedProductRepository.findAllByUser(user);
		return pageOfViewedProduct;
	}

	public void deleteViewedProduct(String psid) {
		List<ViewedProduct> viewedProducts = getAllViewedProduct(psid);
		for (ViewedProduct viewedProduct : viewedProducts) {
			viewedProductRepository.delete(viewedProduct);
		}
	}
}
