package com.abzooba.xpressocommerce.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.abzooba.xpressocommerce.dao.jpa.UserRepository;
import com.abzooba.xpressocommerce.domain.User;

/*
 * Sample service to demonstrate what the API would use to get things done
 */
@Service
public class UserService {

	private static final Logger log = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	CounterService counterService;

	@Autowired
	GaugeService gaugeService;

	public UserService() {
	}

	public User createUser(User user) {
		return userRepository.save(user);
	}

	public User getUser(String id) {
		return userRepository.findOne(id);
	}

	public void updateUser(User user) {
		userRepository.save(user);
	}

	public void deleteUser(String id) {
		userRepository.delete(id);
	}

	public Page<User> getAllUsers(Integer page, Integer size) {
		Page<User> pageOfUsers = userRepository.findAll(new PageRequest(page, size));
		return pageOfUsers;
	}
}
