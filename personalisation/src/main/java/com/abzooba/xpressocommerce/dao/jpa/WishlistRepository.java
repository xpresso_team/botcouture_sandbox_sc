package com.abzooba.xpressocommerce.dao.jpa;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.abzooba.xpressocommerce.domain.Product;
import com.abzooba.xpressocommerce.domain.User;
import com.abzooba.xpressocommerce.domain.Wishlist;

public interface WishlistRepository extends JpaRepository<Wishlist, Long> {

	Page<Wishlist> findAllByUser(User user, Pageable pageable);

	List<Wishlist> findAllByUser(User user);

	Wishlist findByUserAndProduct(User user, Product product);

	Page<Wishlist> findAllByUserAndCatalog(User user, String catalog, Pageable pageable);

	Wishlist findByUserAndCatalogAndProduct(User user, String catalog, Product product);

	List<Wishlist> findByUserAndCatalog(User user, String catalog);

}
