package com.abzooba.xpressocommerce.dao.jpa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.abzooba.xpressocommerce.domain.User;

/**
 * Repository can be used to delegate CRUD operations against the data source: http://goo.gl/P1J8QH
 */
public interface UserRepository extends JpaRepository<User, String> {
	User findUserByTimezone(String timezone);

	Page<User> findAll(Pageable pageable);
}
