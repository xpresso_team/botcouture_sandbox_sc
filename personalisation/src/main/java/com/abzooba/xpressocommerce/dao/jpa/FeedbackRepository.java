package com.abzooba.xpressocommerce.dao.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abzooba.xpressocommerce.domain.Feedback;
import com.abzooba.xpressocommerce.domain.User;

public interface FeedbackRepository extends JpaRepository<Feedback, Long> {

	List<Feedback> findAllByUser(User user);
}
