package com.abzooba.xpressocommerce.dao.jpa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.abzooba.xpressocommerce.domain.Profile;
import com.abzooba.xpressocommerce.domain.User;

public interface ProfileRepository extends JpaRepository<Profile, Long> {
	Profile findByUserAndEntityAndGender(User user, String entity,String gender);
	Page<Profile> findAllByUser(User user, Pageable pageable);
}
