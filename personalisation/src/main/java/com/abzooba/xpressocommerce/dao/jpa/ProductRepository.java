package com.abzooba.xpressocommerce.dao.jpa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.abzooba.xpressocommerce.domain.Product;

public interface ProductRepository extends JpaRepository<Product, String> {
	Product findProductByXcSku(String xc_sku);

	Page findAll(Pageable pageable);
}
