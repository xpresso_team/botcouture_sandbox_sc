package com.abzooba.xpressocommerce.dao.jpa;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.abzooba.xpressocommerce.domain.Cart;
import com.abzooba.xpressocommerce.domain.Product;
import com.abzooba.xpressocommerce.domain.User;

public interface CartRepository extends JpaRepository<Cart, Long> {

	Page<Cart> findAllByUserAndCatalog(User user, String catalog, Pageable pageable);

	Page<Cart> findAllByUser(User user, Pageable pageable);

	List<Cart> findAllByUser(User user);

	Cart findByUserAndCatalogAndProduct(User user, String catalog, Product product);

	List<Cart> findByUserAndCatalog(User user, String catalog);

	Cart findByUser(User user);
}
