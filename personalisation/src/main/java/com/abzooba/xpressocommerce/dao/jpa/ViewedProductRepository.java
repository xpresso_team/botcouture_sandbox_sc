package com.abzooba.xpressocommerce.dao.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abzooba.xpressocommerce.domain.User;
import com.abzooba.xpressocommerce.domain.ViewedProduct;

public interface ViewedProductRepository extends JpaRepository<ViewedProduct, Long> {

	List<ViewedProduct> findAllByUser(User user);
}
