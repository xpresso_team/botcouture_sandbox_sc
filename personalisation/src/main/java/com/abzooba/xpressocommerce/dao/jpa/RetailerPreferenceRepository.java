package com.abzooba.xpressocommerce.dao.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abzooba.xpressocommerce.domain.RetailerPreference;
import com.abzooba.xpressocommerce.domain.User;

public interface RetailerPreferenceRepository extends JpaRepository<RetailerPreference, Long> {

	RetailerPreference findByUser(User user);

}
