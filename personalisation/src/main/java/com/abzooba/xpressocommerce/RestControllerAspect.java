package com.abzooba.xpressocommerce;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class RestControllerAspect {
	private static final Class<RestControllerAspect> restControllerAspectClass = RestControllerAspect.class;
	private static final Logger log = LoggerFactory.getLogger(restControllerAspectClass);

	@Before("execution(public * com.abzooba.xpressocommerce.api.rest.*Controller.*(..))")
	public void logBeforeRestCall(JoinPoint pjp) throws Throwable {
		System.out.println(pjp.toString());
		//		StringBuilder sb = new StringBuilder();
		//		sb.append(pjp.toString());
		//		sb.append("\t");
		//		for (Object arg : pjp.getArgs()) {
		//			if (!arg.getClass().getCanonicalName().contains("org")) {
		//				sb.append(arg.toString());
		//				sb.append("\t");
		//			}
		//		}
		//		sb.append("\n");
		//		log.info(sb.toString());

	}
}
