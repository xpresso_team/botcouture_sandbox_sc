package com.abzooba.xpressocommerce.dto;

public class CartDTO {

	private String psid;
	private String xc_sku;
	private String page_id;
	private String channel_id;
	private String catalog;
	private String quantity;
	private String color;
	private String size;

	public String getXc_sku() {
		return xc_sku;
	}

	public void setXc_sku(String xc_sku) {
		this.xc_sku = xc_sku;
	}

	public String getPsid() {
		return psid;
	}

	public void setPsid(String psid) {
		this.psid = psid;
	}

	public String getPage_id() {
		return page_id;
	}

	public void setPage_id(String page_id) {
		this.page_id = page_id;
	}

	public String getChannel_id() {
		return channel_id;
	}

	public void setChannel_id(String channel_id) {
		this.channel_id = channel_id;
	}

	public String getCatalog() {
		return catalog;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}
}
