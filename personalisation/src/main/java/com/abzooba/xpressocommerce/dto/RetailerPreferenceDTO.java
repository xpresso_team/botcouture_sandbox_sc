package com.abzooba.xpressocommerce.dto;

import java.util.List;

public class RetailerPreferenceDTO {

	private String psid;
	private String page_id;
	private String channel_id;
	private String validity;
	private List<String> retailers;

	public String getPsid() {
		return psid;
	}

	public void setPsid(String psid) {
		this.psid = psid;
	}

	public String getPage_id() {
		return page_id;
	}

	public void setPage_id(String page_id) {
		this.page_id = page_id;
	}

	public String getChannel_id() {
		return channel_id;
	}

	public void setChannel_id(String channel_id) {
		this.channel_id = channel_id;
	}

	public String getValidity() {
		return validity;
	}

	public void setValidity(String validity) {
		this.validity = validity;
	}

	public List<String> getRetailers() {
		return retailers;
	}

	public void setRetailers(List<String> retailers) {
		this.retailers = retailers;
	}

}
