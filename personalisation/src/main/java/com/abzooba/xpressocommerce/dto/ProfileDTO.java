package com.abzooba.xpressocommerce.dto;

public class ProfileDTO {

	private String psid;
	private String entity;
	private String aspect;
	private String brand;
	private String color;
	private String size;
	private String priceUpperLimit;
	private String priceLowerLimit;
	private String style;
	private String gender;
	private String page_id;
	private String channel_id;

	public String getPsid() {
		return psid;
	}

	public void setPsid(String psid) {
		this.psid = psid;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getAspect() {
		return aspect;
	}

	public void setAspect(String aspect) {
		this.aspect = aspect;
	}

	public String getPage_id() {
		return page_id;
	}

	public void setPage_id(String page_id) {
		this.page_id = page_id;
	}

	public String getChannel_id() {
		return channel_id;
	}

	public void setChannel_id(String channel_id) {
		this.channel_id = channel_id;
	}

	public String getPriceUpperLimit() {
		return priceUpperLimit;
	}

	public void setPriceUpperLimit(String priceUpperLimit) {
		this.priceUpperLimit = priceUpperLimit;
	}

	public String getPriceLowerLimit() {
		return priceLowerLimit;
	}

	public void setPriceLowerLimit(String priceLowerLimit) {
		this.priceLowerLimit = priceLowerLimit;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
}
