package com.abzooba.xpressocommerce.dto;

public class FeedbackDTO {

	private String psid;
	private String page_id;
	private String channel_id;
	private String feedback_json_str;

	public String getPsid() {
		return psid;
	}

	public void setPsid(String psid) {
		this.psid = psid;
	}

	public String getPage_id() {
		return page_id;
	}

	public void setPage_id(String page_id) {
		this.page_id = page_id;
	}

	public String getChannel_id() {
		return channel_id;
	}

	public void setChannel_id(String channel_id) {
		this.channel_id = channel_id;
	}

	public String getFeedback_json() {
		return feedback_json_str;
	}

	public void setFeedback_json(String feedback_json_str) {
		this.feedback_json_str = feedback_json_str;
	}
}
