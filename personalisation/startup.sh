#!/usr/bin/env bash
if [ $# -lt 2 ]; then
	echo "usage: startup.sh environment version configFile(optional)"
	exit 1
fi
ENVIRONMENT=$1
TIMESTAMP=$(date -Iseconds)
if [ ! -d logs ]; then
	mkdir logs;
fi

if [ "$1" == "prod" ]; then
	VERSION=$2
	WAR="personalizationservice-"$VERSION".war"
	URL="http://xpresso.abzooba.com:81/maven2/.m2/com/abzooba/xpressocommerce/personalizationservice/"$VERSION"/"$WAR
	if [ ! -d build ]; then
		mkdir build;
	fi
	wget $URL -P build
	if [ -n "$3" ]; then
		java -jar -Dspring.config.location=$3 "build/"$WAR >> logs/log$TIMESTAMP 2>&1 &
	else
		java -jar "build/"$WAR >> logs/log$TIMESTAMP 2>&1 &
	fi
	PID=$!
	echo $PID > "service.pid"
else
	if [ -n "$3" ]; then
		mvn spring-boot:run '-Drun.arguments="--spring.config.location='$3'"' = >> logs/log$TIMESTAMP 2>&1 &
	else
		mvn spring-boot:run >> logs/log$TIMESTAMP 2>&1 &
	fi
	PID=$!
	echo $PID > "service.pid"
fi