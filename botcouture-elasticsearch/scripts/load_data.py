import json
import os

from elasticsearch import Elasticsearch

directory = "/elasticsearch/backup"

es = Elasticsearch([{'host': 'localhost', 'port': '9200'}], http_auth=('elastic', 'changeme'))


i = 1

os.chdir(directory)

for filename in os.listdir(directory):
    if filename.endswith(".json"):
        f = open(filename)
        docket_content = json.load(f)
        # Send the data into es
        for key, value in docket_content.items():
            resp = es.index(index='botcouture', doc_type='docket', id=value['xc_sku'], body=json.dumps(value))
            print(resp)
        i = i + 1
