#!/usr/bin/env python
"""
Classifier is an image classifier specialization of Net.
"""

import numpy as np

from io import Transformer
from io import oversample as io_oversample
from io import resize_image as io_resize_image


class Classifier():
    """
    Classifier extends Net for image class prediction
    by scaling, center cropping, or oversampling.
    Parameters
    ----------
    image_dims : dimensions to scale input for cropping/sampling.
        Default is to scale to net input size for whole-image crop.
    mean, input_scale, raw_scale, channel_swap: params for
        preprocessing options.
    """

    def __init__(self, inputs_shape, input_data_format, output_data_format, image_dims=None,
                 mean=None, input_scale=None, raw_scale=None,
                 channel_swap=None):

        # configure pre-processing
        self.inputs_shape = inputs_shape
        self.transformer = Transformer(resize_to_dims=image_dims,
                                       input_data_format=input_data_format,
                                       output_data_format=output_data_format,
                                       input_scale=input_scale,
                                       mean=mean,
                                       raw_scale=raw_scale,
                                       channel_swap=channel_swap)  # channels in BGR order instead of RGB

        self.crop_dims = np.array(self.inputs_shape[2:])
        if not image_dims:
            image_dims = self.crop_dims
        self.image_dims = image_dims

    def pre_predict(self, inputs, oversample=True):
        """
        Scale to standardize input dimensions.
        Parameters
        ----------
        inputs : iterable of (H x W x K) input ndarrays.
        oversample : boolean
            average predictions across center, corners, and mirrors
            when True (default). Center-only prediction when False.
        Returns
        -------
        caffe_in: (N x K x H x W) ndarray of rescaled input.
        """
        input_ = np.zeros((len(inputs),
                           self.image_dims[0],
                           self.image_dims[1],
                           inputs[0].shape[2]),
                          dtype=np.float32)
        for ix, in_ in enumerate(inputs):
            input_[ix] = io_resize_image(in_, self.image_dims)

        if oversample:
            # Generate center, corner, and mirrored crops.
            input_ = io_oversample(input_, self.crop_dims)
        else:
            # Take center crop.
            center = np.array(self.image_dims) / 2.0
            crop = np.tile(center, (1, 2))[0] + np.concatenate([
                -self.crop_dims / 2.0,
                self.crop_dims / 2.0
            ])
            crop = crop.astype(int)
            input_ = input_[:, crop[0]:crop[2], crop[1]:crop[3], :]

        # Classify
        caffe_in = np.zeros(np.array(input_.shape)[[0, 3, 1, 2]],
                            dtype=np.float32)
        for ix, in_ in enumerate(input_):
            caffe_in[ix] = self.transformer.preprocess(in_)

        return caffe_in

    def post_predict(self, predictions, oversample=True):
        """
        For oversampling, average predictions across crops.
        Parameters
        ----------
        predictions: (N x C) ndarray of class probabilities for N images and C
            classes.
        oversample : boolean
            average predictions across center, corners, and mirrors
            when True (default). Center-only prediction when False.
        Returns
        -------
        predictions: (N x C) ndarray of class probabilities for N images and C
            classes.
        """
        if oversample:
            predictions = predictions.reshape((len(predictions) / 10, 10, -1))
            predictions = predictions.mean(1)

        return predictions
