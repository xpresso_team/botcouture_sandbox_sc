import numpy as np
import skimage.io
from scipy.ndimage import zoom
from skimage.transform import resize


## Pre-processing

class Transformer:
    """
    Transform input for feeding into a Net.
    Note: this is mostly for illustrative purposes and it is likely better
    to define your own input preprocessing routine for your needs.
    Parameters
    ----------
    net : a Net for which the input should be prepared
    """

    def __init__(self,
                 resize_to_dims,
                 input_data_format,  # data_format='NHWC'
                 output_data_format,  # data_format='NCHW'
                 input_scale=None,
                 mean=None,
                 raw_scale=None,
                 channel_swap=None,
                 channels=3
                 ):
        input_data_format = self._get_short_data_format(input_data_format)
        output_data_format = self._get_short_data_format(output_data_format)
        print('Transformer input_data_format: {}'.format(input_data_format))
        print('Transformer output_data_format: {}'.format(output_data_format))
        self.resize_to_dims = resize_to_dims
        self.input_data_format = input_data_format
        self.output_data_format = output_data_format
        self.channels = channels

        self.preprocess_transpose_axes = self._get_transpose_axes('HWC', output_data_format)
        self.deprocess_transpose_axes = self._get_transpose_axes(output_data_format, input_data_format)
        self._set_input_scale(input_scale)
        self._set_raw_scale(raw_scale)
        if mean is not None:
            self._set_mean(mean)
        if channel_swap is not None:
            self._set_channel_swap(channel_swap)

    def _get_transpose_axes(self, from_data_format, to_data_format):
        if to_data_format == from_data_format:
            return (0, 1, 2)
        if from_data_format == 'CHW':
            return (1, 2, 0)
        return (2, 0, 1)

    def _get_short_data_format(self, data_format):
        return data_format[1:]

    def preprocess(self, data):
        """
        Format input for Caffe:
        - convert to single
        - resize to input dimensions (preserving number of channels)
        - transpose dimensions to K x H x W
        - reorder channels (for instance color to BGR)
        - scale raw input (e.g. from [0, 1] to [0, 255] for ImageNet models)
        - subtract mean
        - scale feature
        Parameters
        ----------
        data : (H' x W' x K) ndarray
        Returns
        -------
        caffe_in : (K x H x W) ndarray for input to a Net
        """
        # caffe_in has shape input_data_format
        caffe_in = data.astype(np.float32, copy=False)
        input_data_format = self.input_data_format
        output_data_format = self.output_data_format
        channel_swap = self.channel_swap
        raw_scale = self.raw_scale
        mean = self.mean
        input_scale = self.input_scale

        # skimage.transform.resize method requires caffe_in shape 'HWC'
        if input_data_format != 'HWC':
            caffe_in = caffe_in.transpose((1, 2, 0))
        resize_to_dims = self.resize_to_dims
        if caffe_in.shape[:2] != resize_to_dims:
            caffe_in = resize_image(caffe_in, resize_to_dims)

        # transpose caffe_in to output_data_format
        if channel_swap is not None:
            caffe_in = caffe_in[:, :, channel_swap]
        if output_data_format != 'HWC':
            caffe_in = caffe_in.transpose(self.preprocess_transpose_axes)
        if raw_scale is not None:
            caffe_in *= raw_scale
        if mean is not None:
            caffe_in -= mean
        if input_scale is not None:
            caffe_in *= input_scale
        return caffe_in

    def deprocess(self, data):
        """
        Invert Caffe formatting; see preprocess().
        """
        decaf_in = data.copy().squeeze()
        input_data_format = self.input_data_format
        output_data_format = self.output_data_format
        channel_swap = self.channel_swap
        raw_scale = self.raw_scale
        mean = self.mean
        input_scale = self.input_scale
        if input_scale is not None:
            decaf_in /= input_scale
        if mean is not None:
            decaf_in += mean
        if raw_scale is not None:
            decaf_in /= raw_scale
        if channel_swap is not None:
            decaf_in = decaf_in[np.argsort(channel_swap), :, :]
        if output_data_format != input_data_format:
            decaf_in = decaf_in.transpose(self.deprocess_transpose_axes)

        return decaf_in

    def _set_channel_swap(self, order):
        """
        Set the input channel order for e.g. RGB to BGR conversion
        as needed for the reference ImageNet model.
        N.B. this assumes the channels are the first dimension AFTER transpose.
        Parameters
        ----------
        in_ : which input to assign this channel order
        order : the order to take the channels.
            (2,1,0) maps RGB to BGR for example.
        """
        if len(order) != self.channels:
            raise Exception('Channel swap needs to have the same number of '
                            'dimensions as the input channels.')
        self.channel_swap = order

    def _set_raw_scale(self, scale):
        """
        Set the scale of raw features s.t. the input blob = input * scale.
        While Python represents images in [0, 1], certain Caffe models
        like CaffeNet and AlexNet represent images in [0, 255] so the raw_scale
        of these models must be 255.
        Parameters
        ----------
        in_ : which input to assign this scale factor
        scale : scale coefficient
        """
        self.raw_scale = scale

    def _set_mean(self, mean):
        """
        Set the mean to subtract for centering the data.
        Parameters
        ----------
        in_ : which input to assign this mean.
        mean : mean ndarray (input dimensional or broadcastable)
        """
        output_data_format = self.output_data_format
        ms = mean.shape
        if mean.ndim == 1:
            # broadcast channels
            if ms[0] != self.channels:
                raise ValueError('Mean channels incompatible with input.')
            if output_data_format == 'CHW':
                mean = mean[:, np.newaxis, np.newaxis]
            else:
                mean = mean[np.newaxis, np.newaxis, :]
        else:
            # elementwise mean
            if len(ms) == 2:
                ms = (1,) + ms
            if len(ms) != 3:
                raise ValueError('Mean shape invalid')
            if ms[-1] != self.channels:
                mean_shape = 'CHW'
            else:
                mean_shape = 'HWC'
            if mean_shape != output_data_format:
                mean = mean.transpose(self._get_transpose_axes(mean_shape, output_data_format))
        self.mean = mean

    def _set_input_scale(self, scale):
        """
        Set the scale of preprocessed inputs s.t. the blob = blob * scale.
        N.B. input_scale is done AFTER mean subtraction and other preprocessing
        while raw_scale is done BEFORE.
        Parameters
        ----------
        in_ : which input to assign this scale factor
        scale : scale coefficient
        """
        self.input_scale = scale


## Image IO

def load_image(filename, color=True):
    """
    Load an image converting from grayscale or alpha as needed.
    Parameters
    ----------
    filename : string
    color : boolean
        flag for color format. True (default) loads as RGB while False
        loads as intensity (if image is already grayscale).
    Returns
    -------
    image : an image with type np.float32 in range [0, 1]
        of size (H x W x 3) in RGB or
        of size (H x W x 1) in grayscale.
    """
    img = skimage.img_as_float(skimage.io.imread(filename, as_grey=not color)).astype(np.float32)
    if img.ndim == 2:
        img = img[:, :, np.newaxis]
        if color:
            img = np.tile(img, (1, 1, 3))
    elif img.shape[2] == 4:
        img = img[:, :, :3]
    return img


def resize_image(im, new_dims, interp_order=1):
    """
    Resize an image array with interpolation.
    Parameters
    ----------
    im : (H x W x K) ndarray
    new_dims : (height, width) tuple of new dimensions.
    interp_order : interpolation order, default is linear.
    Returns
    -------
    im : resized ndarray with shape (new_dims[0], new_dims[1], K)
    """
    # print('-----resize_image()-------------------------------')
    # print_info(im, 'caffe_models.caffe_caffe.io.resize_image() im')
    # print_info(new_dims, 'caffe_models.caffe_caffe.io.resize_image() new_dims')
    if im.shape[-1] == 1 or im.shape[-1] == 3:
        im_min, im_max = im.min(), im.max()
        if im_max > im_min:
            # skimage is fast but only understands {1,3} channel images
            # in [0, 1].
            im_std = (im - im_min) / (im_max - im_min)
            resized_std = resize(im_std, new_dims, order=interp_order)
            resized_im = resized_std * (im_max - im_min) + im_min
        else:
            # the image is a constant -- avoid divide by 0
            ret = np.empty((new_dims[0], new_dims[1], im.shape[-1]),
                           dtype=np.float32)
            ret.fill(im_min)
            return ret
    else:
        # ndimage interpolates anything but more slowly.
        scale = tuple(np.array(new_dims, dtype=float) / np.array(im.shape[:2]))
        resized_im = zoom(im, scale + (1,), order=interp_order)
    return resized_im.astype(np.float32)


def oversample(images, crop_dims):
    """
    Crop images into the four corners, center, and their mirrored versions.
    Parameters
    ----------
    image : iterable of (H x W x K) ndarrays
    crop_dims : (height, width) tuple for the crops.
    Returns
    -------
    crops : (10*N x H x W x K) ndarray of crops for number of inputs N.
    """
    # Dimensions and center.
    # print('-----oversample()-------------------------------')
    im_shape = np.array(images[0].shape)
    crop_dims = np.array(crop_dims)
    im_center = im_shape[:2] / 2.0

    # Make crop coordinates
    h_indices = (0, im_shape[0] - crop_dims[0])
    w_indices = (0, im_shape[1] - crop_dims[1])
    crops_ix = np.empty((5, 4), dtype=int)
    curr = 0
    for i in h_indices:
        for j in w_indices:
            crops_ix[curr] = (i, j, i + crop_dims[0], j + crop_dims[1])
            curr += 1
    crops_ix[4] = np.tile(im_center, (1, 2)) + np.concatenate([
        -crop_dims / 2.0,
        crop_dims / 2.0
    ])
    crops_ix = np.tile(crops_ix, (2, 1))

    # Extract crops
    crops = np.empty((10 * len(images), crop_dims[0], crop_dims[1],
                      im_shape[-1]), dtype=np.float32)
    ix = 0
    for im in images:
        for crop in crops_ix:
            crops[ix] = im[crop[0]:crop[2], crop[1]:crop[3], :]
            ix += 1
        crops[ix - 5:ix] = crops[ix - 5:ix, :, ::-1, :]  # flip for mirrors
    return crops
