from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
"""Convolutional Neural Network for fcn.berkeleyvision.org/wlrfash3-fcn8s/deploy.prototxt, built with tf.layers."""

import tensorflow as tf
from tensorflow.contrib.learn.python.learn.estimators import model_fn as model_fn_lib
from tensorflow_models.tf_utils import wrapped_model_fn

from tensorflow_models.fcn8_vgg import FCN8VGG
from tensorflow_models.tf_utils import create_prediction_signature_def_map

tf.logging.set_verbosity(tf.logging.INFO)

# learning_rate=0.001, optimizer="SGD", dropout_rate=0.5
def model_fn(features, labels, mode, params):
    return wrapped_model_fn(_model_fn, features, labels, mode, params)

def _model_fn(features, labels, mode, params):
    """Model function for fcn.berkeleyvision.org/wlrfash3-fcn8s/deploy.prototxt.

    Arguments:
        features: Matrix of shape [n_samples, n_features...] or the dictionary of Matrices.
           Can be iterator that returns arrays of features or dictionary of arrays of features.
           The training input samples for fitting the model. If set, `input_fn` must be `None`.
        labels: Vector or matrix [n_samples] or [n_samples, n_outputs] or the dictionary of same.
           Can be iterator that returns array of labels or dictionary of array of labels.
           The training label values (class labels in classification, real numbers in regression).
           If set, `input_fn` must be `None`. Note: For classification, label values must
           be integers representing the class index (i.e. values from 0 to
           n_classes-1).
        mode: Defines whether this is training, evaluation or prediction.
           See `ModeKeys`.
        params: A dict of hyperparameters.
          The following hyperparameters are expected:
          * optimizer: string, class or optimizer instance, used as trainer.
               string should be name of optimizer, like 'SGD',
                 'Adam', 'Adagrad'. Full list in OPTIMIZER_CLS_NAMES constant.
               class should be sub-class of `tf.Optimizer` that implements
                 `compute_gradients` and `apply_gradients` functions.
               optimizer instance should be instantiation of `tf.Optimizer`
                 sub-class and have `compute_gradients` and `apply_gradients`
                 functions.
           * learning_rate: float or `Tensor`, magnitude of update per each training
                step. Can be `None`.

    Returns:
        Output tensor. Shape is [batch_size, inputs.height/2, inputs.width/2,
        filters].
    """
    data_format = params.get('data_format')
    weights_npy = params['weights_npy']
    num_classes = 18
    debug = False
    loss = None
    train_op = None

    # Input Layer
    # Reshape features to 4-D tensor: [batch_size, channels, width, height]
    # VGG_ILSVRC images are 224x224 pixels, and have three color channels
    # input_layer = tf.reshape(features, [-1, 3, 224, 224])
    input_layer = features

    vgg_fcn = FCN8VGG(weights_npy)
    vgg_fcn.build(input_layer, data_format, num_classes=num_classes, debug=debug)

    # Generate Predictions
    predictions = {
        "upscore8": vgg_fcn.upscore8,
        # "pred_up": vgg_fcn.pred_up
    }

    # Return a ModelFnOps object
    return model_fn_lib.ModelFnOps(
        mode=mode, predictions=predictions, loss=loss, train_op=train_op)


def create_signature_def_map(features, model_fn_ops):
    features_dict = {'images': features}
    return create_prediction_signature_def_map(features_dict, model_fn_ops)
