import tensorflow as tf
from tensorflow.contrib import learn

from tf_utils import create_config_proto
from tf_utils import export_model
from tf_utils import get_data_format
from tf_utils import init_session
from tf_utils import restore_weights as restore_weights_fn


class InferModel(object):
    def __init__(self, model_name, weights, device_no, input_data_format,
                 features_hw=None, channels=3, restore_weights=True):
        print("Creating {}".format(self.__class__.__name__))
        print('InferModel input_data_format: {}'.format(input_data_format))
        self.input_data_format = input_data_format
        self.op_data_format = get_data_format(device_no)
        self.should_transpose_input = input_data_format != self.op_data_format
        print('op_data_format: {}'.format(self.op_data_format))
        print('self.should_transpose_input: {}'.format(self.should_transpose_input))

        if features_hw is None:
            height = width = None
        else:
            height = features_hw[0]
            width = features_hw[1]

        if self.op_data_format == 'NCHW':
            self.features = tf.placeholder(tf.float32, [None, channels, height, width])
        else:
            self.features = tf.placeholder(tf.float32, [None, height, width, channels])

        self.mode = learn.ModeKeys.INFER
        self.params = {'model_name': model_name, 'device_no': device_no}
        self._init_model(weights, device_no)
        log_device_placement = False  # True for debug
        config = create_config_proto(log_device_placement)
        self.sess = init_session(config=config)
        if restore_weights:
            restore_weights_fn(self.sess, model_name, weights)

        # save_as_named_model(self.sess, model_name, weights)
        signature_def_map = self.create_signature_def_map_fn(self.features, self.model_fn)
        export_path_base = '/tmp/' + model_name
        model_version = 1
        #export_model(self.sess, export_path_base, model_version, signature_def_map)

    def _init_model(self, weights, device_no):
        raise NotImplementedError()
