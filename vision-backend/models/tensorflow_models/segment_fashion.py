from __future__ import print_function

import time

import numpy as np
import tensorflow as tf

import segment_fashion_model as model
from infer_model import InferModel
from serving_client import TensorflowServingClient

MODEL_NAME = 'fashion_segmentation'
INPUT_DATA_FORMAT = 'NHWC'


class FashionSegmentation(InferModel):
    def __init__(self, weights, device_no):
        """ Initialize semantic segmentation model.

        Arguments:
            weights: string, The path of the TensorFlow semantic segmentation model weights file.
            device_no: int, The device number on which to perform computation.
        """
        super(FashionSegmentation, self).__init__(model_name=MODEL_NAME,
                                                  weights=weights,
                                                  device_no=device_no,
                                                  input_data_format=INPUT_DATA_FORMAT,
                                                  restore_weights=False)

    def _init_model(self, weights, device_no):
        """
        initialize the model
        Requires checkpoint, model.ckpt.index, model.ckpt.data*
        """
        labels = None
        self.params['weights_npy'] = weights
        self.model_fn = model.model_fn(self.features, labels, self.mode, self.params)
        self.create_signature_def_map_fn = model.create_signature_def_map

    def infer(self, im):
        """
        Infer the segmented mask from the image 
        """
        batch_im = im[np.newaxis, ...]
        if self.should_transpose_input:
            batch_im = np.transpose(batch_im, (0, 2, 3, 1))
        predictions = self.model_fn.predictions

        start_time = time.time()
        prediction = self.sess.run(
            [predictions['upscore8']], {self.features: batch_im})
        print('Semantic segmentation took %.2f ms.' % (1000 * (time.time() - start_time)))
        scores = prediction[0][0, ...]
        print(scores.shape)
        if self.should_transpose_input:
            scores = np.transpose(scores, (2, 0, 1))
        return scores


class FashionSegmentationClient(TensorflowServingClient):
    def __init__(self, host, port, timeout, device_no):
        super(FashionSegmentationClient, self).__init__(model_name=MODEL_NAME,
                                                        signature_name='predict_images',
                                                        host=host,
                                                        port=port,
                                                        timeout=timeout,
                                                        device_no=device_no,
                                                        input_data_format=INPUT_DATA_FORMAT)

    def infer(self, im):
        """
        Infer the segmented mask from the image
        """
        batch_im = im[np.newaxis, ...]
        if self.should_transpose_input:
            batch_im = np.transpose(batch_im, (0, 2, 3, 1))
        # predictions = self.model_fn.predictions

        start_time = time.time()
        request = self._create_request()
        request.inputs['images'].CopyFrom(
            tf.contrib.util.make_tensor_proto(batch_im, shape=list(batch_im.shape)))
        response = self._predict(request)
        print('Semantic segmentation took %.2f ms.' % (1000 * (time.time() - start_time)))
        scores = np.array(response.outputs['upscore8'].float_val, np.float32)
        scores = np.reshape(scores, (im.shape[1], im.shape[2], 18))
        print(scores.shape)
        if self.should_transpose_input:
            scores = np.transpose(scores, (2, 0, 1))
        return scores
