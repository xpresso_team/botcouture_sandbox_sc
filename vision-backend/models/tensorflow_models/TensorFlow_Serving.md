# TensorFlow Serving Usage

### Create the SavedModels
In tensorflow_models/infer_model.py, uncomment the export_model(...) method call.  
Run all the unit tests to create and save the SavedModels.

### Copy the SavedModels to the serving container
```
sudo docker cp /tmp/classify_fashion tf_container:/serving/models

sudo docker cp /tmp/cnn_feats tf_container:/serving/models

sudo docker cp /tmp/gender_classification tf_container:/serving/models

sudo docker cp /tmp/human_pose tf_container:/serving/models

sudo docker cp /tmp/salient_objects tf_container:/serving/models

sudo docker cp /tmp/fashion_segmentation tf_container:/serving/models
```

### Start Docker container(s) and run TensorFlow Serving service
Option A: Serve all models from one docker container
```
sudo docker start -i tf_container

sudo docker cp tensorflow_models/tfserv.cfg tf_container:/serving

bazel-bin/tensorflow_serving/model_servers/tensorflow_model_server --port=9000 --model_config_file=tfserv.cfg &> logs/all_models_log &
```
Option B: Serve one model per docker container
```
bazel-bin/tensorflow_serving/model_servers/tensorflow_model_server --port=9000 --model_name=classify_fashion --model_base_path=models/classify_fashion &> logs/classify_fashion_log &

bazel-bin/tensorflow_serving/model_servers/tensorflow_model_server --port=9001 --model_name=cnn_feats --model_base_path=models/cnn_feats &> logs/cnn_feats_log &

bazel-bin/tensorflow_serving/model_servers/tensorflow_model_server --port=9002 --model_name=gender_classification --model_base_path=models/gender_classification &> logs/gender_classification_log &

bazel-bin/tensorflow_serving/model_servers/tensorflow_model_server --port=9003 --model_name=human_pose --model_base_path=models/human_pose &> logs/human_pose_log &

bazel-bin/tensorflow_serving/model_servers/tensorflow_model_server --port=9004 --model_name=salient_objects --model_base_path=models/salient_objects &> logs/salient_objects_log &

bazel-bin/tensorflow_serving/model_servers/tensorflow_model_server --port=9005 --model_name=fashion_segmentation --model_base_path=models/fashion_segmentation &> logs/fashion_segmentation_log &
```

### Run unit tests of the clients
```
workon cv

export CUDA_DEVICE_ORDER=PCI_BUS_ID

CUDA_VISIBLE_DEVICES=2 python -m tests.tensorflow_classify_fashion_test

CUDA_VISIBLE_DEVICES=2 python -m tests.tensorflow_cnn_feats_test

CUDA_VISIBLE_DEVICES=2 python -m tests.tensorflow_face_gender_test

CUDA_VISIBLE_DEVICES=2 python -m tests.tensorflow_human_pose_test

CUDA_VISIBLE_DEVICES=2 python -m tests.tensorflow_salient_objects_test

CUDA_VISIBLE_DEVICES=2 python -m tests.tensorflow_segment_fashion_test
```
