"""Convolutional Neural Network for CNN Features, built with tf.layers."""

import tensorflow as tf
from tensorflow.contrib import learn
from tensorflow.contrib.learn.python.learn.estimators import model_fn as model_fn_lib

from tf_utils import create_prediction_signature_def_map
from tf_utils import dense
from tf_utils import dropout
from tf_utils import is_nchw_data_format
from tf_utils import wrapped_model_fn
from vgg_utils import vgg_block

tf.logging.set_verbosity(tf.logging.INFO)


# learning_rate=0.001, optimizer="SGD", dropout_rate=0.5
def model_fn(features, labels, mode, params):
    return wrapped_model_fn(_model_fn, features, labels, mode, params)


def _model_fn(features, labels, mode, params):
    """Model function for VGG_ILSVRC_16_layers_deploy_mode CNN.

    Arguments:
        features: Matrix of shape [n_samples, n_features...] or the dictionary of Matrices.
           Can be iterator that returns arrays of features or dictionary of arrays of features.
           The training input samples for fitting the model. If set, `input_fn` must be `None`.
        labels: Vector or matrix [n_samples] or [n_samples, n_outputs] or the dictionary of same.
           Can be iterator that returns array of labels or dictionary of array of labels.
           The training label values (class labels in classification, real numbers in regression).
           If set, `input_fn` must be `None`. Note: For classification, label values must
           be integers representing the class index (i.e. values from 0 to
           n_classes-1).
        mode: Defines whether this is training, evaluation or prediction.
           See `ModeKeys`.
        params: A dict of hyperparameters.
          The following hyperparameters are expected:
          * optimizer: string, class or optimizer instance, used as trainer.
               string should be name of optimizer, like 'SGD',
                 'Adam', 'Adagrad'. Full list in OPTIMIZER_CLS_NAMES constant.
               class should be sub-class of `tf.Optimizer` that implements
                 `compute_gradients` and `apply_gradients` functions.
               optimizer instance should be instantiation of `tf.Optimizer`
                 sub-class and have `compute_gradients` and `apply_gradients`
                 functions.
           * learning_rate: float or `Tensor`, magnitude of update per each training
                step. Can be `None`.
 
    Returns:
        Output tensor. Shape is [batch_size, inputs.height/2, inputs.width/2,
        filters].
    """
    data_format = params.get('data_format')
    optimizer = params.get('optimizer')
    learning_rate = params.get('learning_rate')
    dropout_rate = params.get('dropout_rate')
    training = mode == learn.ModeKeys.TRAIN
    CLASSES_A = 1000
    CLASSES_C = 50

    # Input Layer
    # Reshape features to 4-D tensor: [batch_size, channels, width, height]
    # VGG_ILSVRC images are 224x224 pixels, and have three color channels
    input_layer = features

    # Convolutional Block #1 through #5
    # Input Tensor Shape: [batch_size, 224, 224, 3]
    # Output Tensor Shape: [batch_size, 7, 7, 512]
    conv5 = vgg_block(inputs=input_layer, data_format=data_format)

    # Flatten tensor into a batch of vectors
    # Transpose if is NCHW shape
    # Input Tensor Shape: [batch_size, 7, 7, 512]
    # Output Tensor Shape: [batch_size, 7 * 7 * 512]
    if is_nchw_data_format(data_format):
        conv5 = tf.transpose(conv5, [0, 2, 3, 1])
    flat_conv5 = tf.reshape(conv5, [-1, 7 * 7 * 512])

    # Dense Layer
    # Densely connected layer with 4096 neurons
    # Input Tensor Shape: [batch_size, 7 * 7 * 512]
    # Output Tensor Shape: [batch_size, 4096]
    fc6 = dense(
        inputs=flat_conv5, units=4096, activation=tf.nn.relu, name='fc6')

    # Add dropout operation; 'rate' probability that element will be kept
    drop6 = dropout(
        inputs=fc6, rate=dropout_rate, training=training, name='drop6')

    # Densely connected layer with 4096 neurons
    # Input Tensor Shape: [batch_size, 4096]
    # Output Tensor Shape: [batch_size, 4096]
    fc7 = dense(inputs=drop6, units=4096, activation=None, name='fc7')
    relu7 = tf.nn.relu(fc7, name='relu7')

    # Add dropout operation; 'rate' probability that element will be kept
    drop7 = dropout(
        inputs=relu7, rate=dropout_rate, training=training, name='drop7')

    # Logits layer -> Softmax
    # Input Tensor Shape: [batch_size, 4096]
    # Output Tensor Shape: [batch_size, CLASSES]
    fc8_c = dense(inputs=drop7, units=CLASSES_C, name='fc8_c')

    # Logits layer -> Sigmoid
    # Input Tensor Shape: [batch_size, 4096]
    # Output Tensor Shape: [batch_size, CLASSES]
    fc8_a = dense(inputs=drop7, units=CLASSES_A, name='fc8_a')

    loss = None
    train_op = None

    # Calculate Loss (for both TRAIN and EVAL modes)
    if mode != learn.ModeKeys.INFER:
        onehot_labels = tf.one_hot(indices=tf.cast(labels, tf.int32), depth=CLASSES_C)
        loss = tf.losses.softmax_cross_entropy(
            onehot_labels=onehot_labels, logits=fc8_c)

    # Configure the Training Op (for TRAIN mode)
    if training:
        train_op = tf.contrib.layers.optimize_loss(
            loss=loss,
            global_step=tf.contrib.framework.get_global_step(),
            learning_rate=learning_rate,
            optimizer=optimizer)

    # Generate Predictions
    predictions = {
        "fc6": fc6,
        "fc7": fc7,
        # "classes": tf.argmax(
        #     input=fc8_a, axis=1),
        # "probabilities": tf.nn.softmax(
        #     fc8_c, name="softmax_tensor")
    }

    # Return a ModelFnOps object
    return model_fn_lib.ModelFnOps(
        mode=mode, predictions=predictions, loss=loss, train_op=train_op)


def create_signature_def_map(features, model_fn_ops):
    features_dict = {'images': features}
    return create_prediction_signature_def_map(features_dict, model_fn_ops)
