"""VGG Utilities."""

import tensorflow as tf

from tf_utils import conv2d
from tf_utils import max_pooling2d
from tf_utils import pad_image


# TODO - convert print statements to application logging statements

def vgg_block(inputs, data_format, first_layer_padding=None):
    if first_layer_padding:
        print('first_layer_padding: {}'.format(first_layer_padding))
        # TODO - This works but better to use the value as-is and make the first padding 'valid'
        inputs = pad_image(inputs, first_layer_padding - 1, data_format)
    print('inputs shape: {}'.format(inputs.get_shape()))

    # Convolutional Block #1
    # Consists of two convolutional layers and a max pooling Layer
    # Computes 64 features using a 3x3 filter with ReLU activation.
    # Padding is added to preserve width and height.
    # Max pooling uses a 2x2 filter and stride of 2.
    # Input Tensor Shape: [batch_size, 224, 224, 3]
    # Output Tensor Shape: [batch_size, 112, 112, 64]
    conv1 = _conv_block(inputs=inputs, conv_layers=2, filters=64, data_format=data_format, block_number=1)

    # Convolutional Block #2
    # Consists of two convolutional layers and a max pooling Layer
    # Computes 128 features using a 3x3 filter with ReLU activation.
    # Padding is added to preserve width and height.
    # Max pooling uses a 2x2 filter and stride of 2.
    # Input Tensor Shape: [batch_size, 112, 112, 64]
    # Output Tensor Shape: [batch_size, 56, 56, 128]
    conv2 = _conv_block(inputs=conv1, conv_layers=2, filters=128, data_format=data_format, block_number=2)

    # Convolutional Block #3
    # Consists of three convolutional layers and a max pooling Layer
    # Computes 256 features using a 3x3 filter with ReLU activation.
    # Padding is added to preserve width and height.
    # Max pooling uses a 2x2 filter and stride of 2.
    # Input Tensor Shape: [batch_size, 56, 56, 128]
    # Output Tensor Shape: [batch_size, 28, 28, 256]
    conv3 = _conv_block(inputs=conv2, conv_layers=3, filters=256, data_format=data_format, block_number=3)

    # Convolutional Block #4
    # Consists of three convolutional layers and a max pooling Layer
    # Computes 512 features using a 3x3 filter with ReLU activation.
    # Padding is added to preserve width and height.
    # Max pooling uses a 2x2 filter and stride of 2.
    # Input Tensor Shape: [batch_size, 28, 28, 256]
    # Output Tensor Shape: [batch_size, 14, 14, 512]
    conv4 = _conv_block(inputs=conv3, conv_layers=3, filters=512, data_format=data_format, block_number=4)

    # Convolutional Block #5
    # Consists of three convolutional layers and a max pooling Layer
    # Computes 512 features using a 3x3 filter with ReLU activation.
    # Padding is added to preserve width and height.
    # Max pooling uses a 2x2 filter and stride of 2.
    # Input Tensor Shape: [batch_size, 14, 14, 512]
    # Output Tensor Shape: [batch_size, 7, 7, 512]
    conv5 = _conv_block(inputs=conv4, conv_layers=3, filters=512, data_format=data_format, block_number=5)
    return conv5


def _conv_block(inputs, conv_layers, filters, data_format, block_number):
    """Convolutional block.
    
    This block creates `conv_layers` number of convolutional layers followed by
    a max pooling layer.
    
    Each convolutional layer computes `filters` number of features using a 3x3 
    filter with ReLU activation. Padding is added to preserve width and height.
    The max pooling layer uses a 2x2 filter and stride of 2.
    
    Arguments:
        inputs: Tensor input. Shape is [batch_size, inputs.channel, 
        inputs.height, inputs.width].
        conv_layers: Integer, the number of convolutional layers in the block.
        filters: Integer, the dimensionality of the output space (i.e. the 
          number of filters in the convolution).
        block_number: Integer, the block number used in the convolutional and 
          max pooling layer names.

    Returns:
        Output tensor. Shape is [batch_size, inputs.height/2, inputs.width/2,
          filters].
    """
    net = inputs
    for i in range(conv_layers):
        net = conv2d(
            inputs=net,
            filters=filters,
            kernel_size=[3, 3],
            padding="same",
            data_format=data_format,
            activation=tf.nn.relu,
            name="conv{}_{}".format(block_number, i + 1))

    return max_pooling2d(
        inputs=net,
        pool_size=[2, 2],
        strides=2,
        data_format=data_format,
        name="pool{}".format(block_number))
