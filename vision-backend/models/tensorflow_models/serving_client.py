from __future__ import print_function

from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2
from tf_utils import get_data_format
from tf_utils import insecure_channel
import sys

MAX_MESSAGE_LENGTH = 30000000


class TensorflowServingClient(object):
    def __init__(self, model_name, signature_name, host, port, timeout, device_no, input_data_format):
        print("Creating {}".format(self.__class__.__name__),file=sys.stderr)
        print('model_name: {}, signature_name: {}'.format(model_name, signature_name),file=sys.stderr)
        print('host: {}, port: {}, timeout: {}'.format(host, port, timeout),file=sys.stderr)
        print('input_data_format: {}'.format(input_data_format),file=sys.stderr)
        self.input_data_format = input_data_format
        self.op_data_format = get_data_format(device_no)
        self.should_transpose_input = input_data_format != self.op_data_format
        print('op_data_format: {}'.format(self.op_data_format),file=sys.stderr)
        print('self.should_transpose_input: {}'.format(self.should_transpose_input),file=sys.stderr)
        self.model_name = model_name
        self.signature_name = signature_name
        self.timeout = timeout
        # channel = implementations.insecure_channel(host, int(port))
        options = [('grpc.max_send_message_length', MAX_MESSAGE_LENGTH), (
            'grpc.max_receive_message_length', MAX_MESSAGE_LENGTH)]
        channel = insecure_channel(host, int(port), options)
        self.stub = prediction_service_pb2.beta_create_PredictionService_stub(channel)

    def _create_request(self):
        request = predict_pb2.PredictRequest()
        request.model_spec.name = self.model_name
        request.model_spec.signature_name = self.signature_name
        return request

    def _predict(self, request):
        return self.stub.Predict(request, self.timeout)
