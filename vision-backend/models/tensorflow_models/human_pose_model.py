"""Convolutional Neural Network for 
RheC_RTMPPose/models/_trained_COCO/pose_deploy.prototxt, built with tf.layers.

From ZheC/Realtime_Multi-Person_Pose_Estimation
https://github.com/ZheC/Realtime_Multi-Person_Pose_Estimation
"""
from collections import namedtuple

import tensorflow as tf
from tensorflow.contrib import learn
from tensorflow.contrib.learn.python.learn.estimators import model_fn as model_fn_lib

from tf_utils import create_prediction_signature_def_map
from tf_utils import concat
from tf_utils import conv2d
from tf_utils import conv2d_sequence
from tf_utils import get_channel_index
from tf_utils import max_pooling2d
from tf_utils import wrapped_model_fn


def conv_block(inputs, conv_layers, filters, data_format, conv_name, pool_name=None):
    net = conv2d_sequence(
        inputs=inputs,
        layers=conv_layers,
        filters=filters,
        kernel_size=3,
        strides=1,  # default
        padding="same",
        data_format=data_format,
        activation=tf.nn.relu,
        name=conv_name)

    if not pool_name:
        return net

    return max_pooling2d(
        inputs=net,
        pool_size=2,
        strides=2,
        padding='valid',  # default
        data_format=data_format,
        name=pool_name)


def vgg(input_layer, data_format):
    net = input_layer
    net = conv_block(net, conv_layers=2, filters=64, data_format=data_format, conv_name='conv1_{}',
                     pool_name='pool1_stage1')
    net = conv_block(net, conv_layers=2, filters=128, data_format=data_format, conv_name='conv2_{}',
                     pool_name='pool2_stage1')
    net = conv_block(net, conv_layers=4, filters=256, data_format=data_format, conv_name='conv3_{}',
                     pool_name='pool3_stage1')
    net = conv_block(net, conv_layers=2, filters=512, data_format=data_format, conv_name='conv4_{}')

    net = conv2d(
        inputs=net,
        filters=256,
        kernel_size=3,
        strides=1,  # default
        padding="same",
        data_format=data_format,
        activation=tf.nn.relu,
        name='conv4_3_CPM')

    return conv2d(
        inputs=net,
        filters=128,
        kernel_size=3,
        strides=1,  # default
        padding="same",
        data_format=data_format,
        activation=tf.nn.relu,
        name='conv4_4_CPM')


def branch(input_layer, conv_layers, kernel_size, data_format, branch_name, conv_name, one_by_one_filters=None):
    """ 
    A branch is composed of 'conv_layers' number of convolutions with a kernel size of 'kernel_size'
    and 'same' padding followed by two 1x1 convs.
    """
    filters = 128
    if one_by_one_filters is None:
        one_by_one_filters = filters

    if branch_name == 'L1':
        last_filters = 38
    else:
        last_filters = 19

    net = conv2d_sequence(
        inputs=input_layer,
        layers=conv_layers,
        filters=filters,
        kernel_size=kernel_size,
        strides=1,  # default
        padding="same",
        data_format=data_format,
        activation=tf.nn.relu,
        name=conv_name + '_' + branch_name)

    net = conv2d(
        inputs=net,
        filters=one_by_one_filters,
        kernel_size=1,
        strides=1,  # default
        padding="same",
        data_format=data_format,
        activation=tf.nn.relu,
        name='{}_{}'.format(conv_name.format(conv_layers + 1), branch_name))

    return conv2d(
        inputs=net,
        filters=last_filters,
        kernel_size=1,
        strides=1,  # default
        padding="same",
        data_format=data_format,
        activation=None,  # default
        name='{}_{}'.format(conv_name.format(conv_layers + 2), branch_name))


def stage(l1, l2, data_format, stage, feats=None, conv_layers=5, kernel_size=7, conv_name=None,
          one_by_one_filters=None):
    """ A stage is composed of two branches (L1 and L2) in parallel. """
    channel_index = get_channel_index(data_format)
    if feats is not None:
        l1 = l2 = concat([l1, l2, feats], axis=channel_index, name='concat_stage{}'.format(stage))
    if conv_name is None:
        conv_name = 'Mconv{}_stage' + str(stage)

    l1 = branch(l1, conv_layers, kernel_size, data_format, 'L1', conv_name, one_by_one_filters)
    l2 = branch(l2, conv_layers, kernel_size, data_format, 'L2', conv_name, one_by_one_filters)

    return namedtuple('stage_output', ['l1', 'l2'])(
        l1=l1,
        l2=l2)


# learning_rate=0.001, optimizer="SGD", dropout_rate=0.5
def model_fn(features, labels, mode, params):
    return wrapped_model_fn(_model_fn, features, labels, mode, params)


def _model_fn(features, labels, mode, params):
    """Model function for RheC_RTMPPose/models/_trained_COCO/pose_deploy.prototxt.

    Arguments:
        features: Matrix of shape [n_samples, n_features...] or the dictionary of Matrices.
           Can be iterator that returns arrays of features or dictionary of arrays of features.
           The training input samples for fitting the model. If set, `input_fn` must be `None`.
        labels: Vector or matrix [n_samples] or [n_samples, n_outputs] or the dictionary of same.
           Can be iterator that returns array of labels or dictionary of array of labels.
           The training label values (class labels in classification, real numbers in regression).
           If set, `input_fn` must be `None`. Note: For classification, label values must
           be integers representing the class index (i.e. values from 0 to
           n_classes-1).
        mode: Defines whether this is training, evaluation or prediction.
           See `ModeKeys`.
        params: A dict of hyperparameters.
          The following hyperparameters are expected:
          * optimizer: string, class or optimizer instance, used as trainer.
               string should be name of optimizer, like 'SGD',
                 'Adam', 'Adagrad'. Full list in OPTIMIZER_CLS_NAMES constant.
               class should be sub-class of `tf.Optimizer` that implements
                 `compute_gradients` and `apply_gradients` functions.
               optimizer instance should be instantiation of `tf.Optimizer`
                 sub-class and have `compute_gradients` and `apply_gradients`
                 functions.
           * learning_rate: float or `Tensor`, magnitude of update per each training
                step. Can be `None`.
 
    Returns:
        Output tensor. Shape is [batch_size, inputs.height/2, inputs.width/2,
        filters].
    """
    data_format = params.get('data_format')
    optimizer = params.get('optimizer')
    learning_rate = params.get('learning_rate')
    dropout_rate = params.get('dropout_rate')
    training = mode == learn.ModeKeys.TRAIN

    feats = vgg(features, data_format=data_format)

    stage1 = stage(l1=feats, l2=feats, data_format=data_format, stage=1, conv_layers=3, kernel_size=3,
                   conv_name='conv5_{}_CPM',
                   one_by_one_filters=512)

    stage2 = stage(l1=stage1.l1, l2=stage1.l2, data_format=data_format, stage=2, feats=feats)

    stage3 = stage(l1=stage2.l1, l2=stage2.l2, data_format=data_format, stage=3, feats=feats)

    stage4 = stage(l1=stage3.l1, l2=stage3.l2, data_format=data_format, stage=4, feats=feats)

    stage5 = stage(l1=stage4.l1, l2=stage4.l2, data_format=data_format, stage=5, feats=feats)

    stage6 = stage(l1=stage5.l1, l2=stage5.l2, data_format=data_format, stage=6, feats=feats)

    loss = None
    train_op = None

    # # Calculate Loss (for both TRAIN and EVAL modes)
    # if mode != learn.ModeKeys.INFER:
    #     onehot_labels = tf.one_hot(indices=tf.cast(labels, tf.int32), depth=CLASSES)
    #     loss = tf.losses.softmax_cross_entropy(
    #         onehot_labels=onehot_labels, logits=fc8)
    #
    # # Configure the Training Op (for TRAIN mode)
    # if training:
    #     train_op = tf.contrib.layers.optimize_loss(
    #         loss=loss,
    #         global_step=tf.contrib.framework.get_global_step(),
    #         learning_rate=learning_rate,
    #         optimizer=optimizer)

    # Generate Predictions
    predictions = {
        "heatmap": stage6.l2,
        "paf": stage6.l1,
    }

    # Return a ModelFnOps object
    return model_fn_lib.ModelFnOps(
        mode=mode, predictions=predictions, loss=loss, train_op=train_op)


def create_signature_def_map(features, model_fn_ops):
    features_dict = {'images': features}
    return create_prediction_signature_def_map(features_dict, model_fn_ops)


def conversion_only_model_fn():
    """Method intended only for caffe to tensorflow conversion utility.
    Returns the model_fn with default values for features, labels, mode, params.
    The defaults are not useful for normal usage.
    """
    features = tf.zeros([1, 227, 227, 3])
    labels = tf.ones([1, 2])
    mode = learn.ModeKeys.INFER
    return model_fn(features, labels, mode, params=None)
