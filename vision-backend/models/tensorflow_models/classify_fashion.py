import numpy as np
import tensorflow as tf

import classify_fashion_model as model
from infer_model import InferModel
from serving_client import TensorflowServingClient

MODEL_NAME = 'classify_fashion'
INPUT_DATA_FORMAT = 'NCHW'

class FashionClassification(InferModel):
    def __init__(self, weights, device_no):
        """ Initialize fashion classification model.

        Arguments:
            weights: string, The path of the TensorFlow fashion classification model weights file.
            device_no: int, The device number on which to perform computation.
        """
        super(FashionClassification, self).__init__(model_name='classify_fashion',
                                                    weights=weights,
                                                    device_no=device_no,
                                                    input_data_format=INPUT_DATA_FORMAT,
                                                    features_hw=[224, 224])

    def _init_model(self, weights, device_no):
        """
        initialize the model
        Requires checkpoint, model.ckpt.index, model.ckpt.data*
        """
        labels = None
        self.model_fn = model.model_fn(self.features, labels, self.mode, self.params)
        self.create_signature_def_map_fn = model.create_signature_def_map

    def infer(self, im_patch):
        if self.should_transpose_input:
            im_patch = np.transpose(im_patch, (0, 2, 3, 1))
        probabilities = self.model_fn.predictions['probabilities']
        prediction = self.sess.run(probabilities, {self.features: im_patch})
        return prediction[0]


class FashionClassificationClient(TensorflowServingClient):
    def __init__(self, host, port, timeout, device_no):
        super(FashionClassificationClient, self).__init__(model_name='classify_fashion',
                                                          signature_name='predict_images',
                                                          host=host,
                                                          port=port,
                                                          timeout=timeout,
                                                          device_no=device_no,
                                                          input_data_format=INPUT_DATA_FORMAT)

    def infer(self, im_patch):
        if self.should_transpose_input:
            im_patch = np.transpose(im_patch, (0, 2, 3, 1))
        request = self._create_request()
        request.inputs['images'].CopyFrom(
            tf.contrib.util.make_tensor_proto(im_patch, shape=list(im_patch.shape)))
        response = self._predict(request)
        prediction = np.array(response.outputs['probabilities'].float_val)
        return prediction
