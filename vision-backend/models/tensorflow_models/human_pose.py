from __future__ import print_function

import sys
import time
from collections import namedtuple

import numpy as np
import tensorflow as tf

import human_pose_model as model
from infer_model import InferModel
from serving_client import TensorflowServingClient

MODEL_NAME = 'human_pose'
INPUT_DATA_FORMAT = 'NCHW'
SCALE_FACTOR = 8
HEATMAP_FILTERS_OUT = 19
PAF_FILTERS_OUT = 38


class HumanPoseDetection(InferModel):
    def __init__(self, weights, device_no):
        """ Initialize human pose detection model.

        Arguments:
            weights: string, The path of the TensorFlow human pose detection model weights file.
            device_no: int, The device number on which to perform computation.
        """
        super(HumanPoseDetection, self).__init__(model_name=MODEL_NAME,
                                                 weights=weights,
                                                 device_no=device_no,
                                                 input_data_format=INPUT_DATA_FORMAT)

    def _init_model(self, weights, device_no):
        """
        initialize the model
        Requires checkpoint, model.ckpt.index, model.ckpt.data*
        """
        labels = None
        self.model_fn = model.model_fn(self.features, labels, self.mode, self.params)
        self.create_signature_def_map_fn = model.create_signature_def_map

    def infer(self, images, multiplier):
        """
        Infer the heatmap and PAFs from the image 
        """
        num_ims = len(images)
        # net.forward() # dry run
        heatmaps = []
        pafs = []
        for image_to_test_padded in images:
            if self.should_transpose_input:
                image_to_test_padded = np.transpose(image_to_test_padded, (2, 0, 1))
            image_to_test_padded = np.float32(image_to_test_padded[np.newaxis, :, :, :]) / 256 - 0.5
            predictions = self.model_fn.predictions

            start_time = time.time()
            heatmap, paf = self.sess.run(
                [predictions['heatmap'], predictions['paf']], {self.features: image_to_test_padded})
            print('At scale %d, The CNN took %.2f ms.' % (multiplier, 1000 * (time.time() - start_time)),
                  file=sys.stderr)
            heatmap = np.squeeze(heatmap)
            paf = np.squeeze(paf)
            if self.should_transpose_input:
                heatmap = np.transpose(heatmap, (1, 2, 0))
                paf = np.transpose(paf, (1, 2, 0))
            heatmaps.append(heatmap)
            pafs.append(paf)

        return namedtuple('results', ['heatmap', 'paf'])(
            heatmap=heatmaps,
            paf=pafs)


class HumanPoseDetectionClient(TensorflowServingClient):
    def __init__(self, host, port, timeout, device_no):
        super(HumanPoseDetectionClient, self).__init__(model_name=MODEL_NAME,
                                                       signature_name='predict_images',
                                                       host=host,
                                                       port=port,
                                                       timeout=timeout,
                                                       device_no=device_no,
                                                       input_data_format=INPUT_DATA_FORMAT)

    def infer(self, images, multiplier):
        """
        Infer the heatmap and PAFs from the image
        """
        num_ims = len(images)
        # net.forward() # dry run
        heatmaps = []
        pafs = []
        for image_to_test_padded in images:
            out_height = image_to_test_padded.shape[0] / SCALE_FACTOR
            out_width = image_to_test_padded.shape[1] / SCALE_FACTOR
            heatmap_shape = (out_height, out_width, HEATMAP_FILTERS_OUT)
            paf_shape = (out_height, out_width, PAF_FILTERS_OUT)
            if self.should_transpose_input:
                heatmap_shape = (HEATMAP_FILTERS_OUT, out_height, out_width)
                paf_shape = (PAF_FILTERS_OUT, out_height, out_width)
                image_to_test_padded = np.transpose(image_to_test_padded, (2, 0, 1))
            image_to_test_padded = np.float32(image_to_test_padded[np.newaxis, :, :, :]) / 256 - 0.5

            start_time = time.time()
            request = self._create_request()
            request.inputs['images'].CopyFrom(
                tf.contrib.util.make_tensor_proto(image_to_test_padded, shape=list(image_to_test_padded.shape)))
            response = self._predict(request)
            heatmap = np.array(response.outputs['heatmap'].float_val)
            paf = np.array(response.outputs['paf'].float_val)
            print('At scale %d, The CNN took %.2f ms.' % (multiplier, 1000 * (time.time() - start_time)),
                  file=sys.stderr)
            heatmap = np.reshape(heatmap, heatmap_shape)
            paf = np.reshape(paf, paf_shape)
            if self.should_transpose_input:
                heatmap = np.transpose(heatmap, (1, 2, 0))
                paf = np.transpose(paf, (1, 2, 0))
            heatmaps.append(heatmap)
            pafs.append(paf)

        return namedtuple('results', ['heatmap', 'paf'])(
            heatmap=heatmaps,
            paf=pafs)
