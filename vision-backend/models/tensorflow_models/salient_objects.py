from __future__ import print_function

import numpy as np
import tensorflow as tf

import salient_objects_model as model
from decaffenated.io import Transformer
from infer_model import InferModel
from serving_client import TensorflowServingClient
from tf_utils import get_data_format

MODEL_NAME = 'salient_objects'
INPUT_DATA_FORMAT = 'NCHW'
FEATURES_HW = (300, 300)


class SalientObjDetection(InferModel):
    def __init__(self, weights, mean, device_no):
        """ Initialize the salient object segmentation model.

        Arguments:
            weights: string, The path of the TensorFlow human pose detection model weights file.
            device_no: int, The device number on which to perform computation.
        """
        self.features_hw = FEATURES_HW
        self.mean = mean
        super(SalientObjDetection, self).__init__(model_name=MODEL_NAME,
                                                  weights=weights,
                                                  device_no=device_no,
                                                  input_data_format=INPUT_DATA_FORMAT,
                                                  features_hw=list(self.features_hw))

    def _init_model(self, weights, device_no):
        """
        initialize the model
        Requires checkpoint, model.ckpt.index, model.ckpt.data*
        """
        labels = None
        self.model_fn = model.model_fn(self.features, labels, self.mode, self.params)
        self.create_signature_def_map_fn = model.create_signature_def_map

        self.transformer = _create_transformer(self.mean, device_no)

    def predict(self, im_orig):
        """
        Predict the outmap from the image 
        """
        imgData = []
        for i, img in enumerate(im_orig):
            imgData.append(self.transformer.preprocess(img))
        imgData = np.array(imgData)
        outmap = self.sess.run(self.model_fn.predictions['outmap'], {self.features: imgData})
        return outmap[:, :, :, 0]


class SalientObjDetectionClient(TensorflowServingClient):
    def __init__(self, host, port, timeout, mean, device_no):
        super(SalientObjDetectionClient, self).__init__(model_name=MODEL_NAME,
                                                        signature_name='predict_images',
                                                        host=host,
                                                        port=port,
                                                        timeout=timeout,
                                                        device_no=device_no,
                                                        input_data_format=INPUT_DATA_FORMAT)

        self.transformer = _create_transformer(mean, device_no)

    def predict(self, im_orig):
        """
        Predict the outmap from the image
        """
        imgData = []
        for i, img in enumerate(im_orig):
            imgData.append(self.transformer.preprocess(img))
        imgData = np.array(imgData)
        request = self._create_request()
        request.inputs['images'].CopyFrom(
            tf.contrib.util.make_tensor_proto(imgData, shape=list(imgData.shape)))
        response = self._predict(request)
        outmap = np.array(response.outputs['outmap'].float_val, np.float32)
        return np.reshape(outmap, (1, 314, 314))


def _create_transformer(mean, device_no):
    return Transformer(resize_to_dims=FEATURES_HW,
                       input_data_format=INPUT_DATA_FORMAT,
                       output_data_format=get_data_format(device_no),
                       mean=mean,
                       raw_scale=255,  # images in [0,255] range instead of [0,1]
                       channel_swap=(2, 1, 0))  # channels in BGR order instead of RGB
