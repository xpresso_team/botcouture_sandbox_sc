import tensorflow as tf
from tensorflow.contrib.framework.python.framework import checkpoint_utils


def list_variables_in_checkpoint(checkpoint):
    checkpoint_dir = checkpoint.rsplit('/', 1)[0]
    print('checkpoint_dir: {}'.format(checkpoint_dir))
    list_vars = checkpoint_utils.list_variables(checkpoint_dir)
    for var in list_vars:
        print(var)


def print_output_tensors():
    """Prints out all the output tensors in the default graph. 
    Intended for use with get_tensor_by_name(name) for accessing tensor values embedded in the graph."""
    for op in tf.get_default_graph().get_operations():
        print('operation name: {}, type: {}, outputs: {}'.format(op.name, op.type, [t.name for t in op.outputs]))


def get_tensor_by_name(name):
    """Returns a tensor in the default graph by name. 
    Intended for accessing tensor values embedded in the graph.
    Example Usage:
        print_output_tensors() -> 'classify_fashion/pool5/MaxPool:0'
        pool5_output_tensor = get_tensor_by_name('classify_fashion/pool5/MaxPool:0')
        pool5_output = self.sess.run([probabilities, pool5_output_tensor], {self.features: im_patch})
        print_info(pool5_output, 'pool5_output')
    """
    for op in tf.get_default_graph().get_operations():
        for tensor in op.outputs:
            if str(tensor.name) == name:
                return tensor
