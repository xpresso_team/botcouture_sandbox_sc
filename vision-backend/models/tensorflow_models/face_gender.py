import numpy as np
import tensorflow as tf

import face_gender_model as model
from decaffenated.classifier import Classifier
from infer_model import InferModel
from serving_client import TensorflowServingClient

MODEL_NAME = 'gender_classification'
INPUT_DATA_FORMAT = 'NHCW'
FEATURES_HW = (227, 227)


class GenderClassification(InferModel):
    def __init__(self, weights, mean, device_no):
        """ Initialize gender classification model.

        Arguments:
            weights: string, The path of the TensorFlow gender classification model weights file.
            device_no: int, The device number on which to perform computation.
        """
        self.features_hw = FEATURES_HW
        self.mean = mean
        super(GenderClassification, self).__init__(model_name=MODEL_NAME,
                                                   weights=weights,
                                                   device_no=device_no,
                                                   input_data_format=INPUT_DATA_FORMAT,
                                                   features_hw=list(self.features_hw))

    def _init_model(self, weights, device_no):
        """
        initialize the model
        Requires checkpoint, model.ckpt.index, model.ckpt.data*
        """
        self.classifier = _create_classifier(self.features_hw, self.mean)
        labels = None  # 2
        self.model_fn = model.model_fn(self.features, labels, self.mode, self.params)
        self.create_signature_def_map_fn = model.create_signature_def_map

    def predict(self, img_face):
        """
        Predict the gender from the image of a face 
        """
        img_face = self.classifier.pre_predict(img_face)
        if self.should_transpose_input:
            img_face = np.transpose(img_face, (0, 2, 3, 1))
        probabilities = self.model_fn.predictions['prob']
        prediction = self.sess.run(probabilities, {self.features: img_face})
        return self.classifier.post_predict(prediction)


class GenderClassificationClient(TensorflowServingClient):
    def __init__(self, host, port, timeout, mean, device_no):
        super(GenderClassificationClient, self).__init__(model_name=MODEL_NAME,
                                                         signature_name='predict_images',
                                                         host=host,
                                                         port=port,
                                                         timeout=timeout,
                                                         device_no=device_no,
                                                         input_data_format=INPUT_DATA_FORMAT)
        self.classifier = _create_classifier(FEATURES_HW, mean)

    def predict(self, img_face):
        """
        Predict the gender from the image of a face
        """
        img_face = self.classifier.pre_predict(img_face)
        if self.should_transpose_input:
            img_face = np.transpose(img_face, (0, 2, 3, 1))

        request = self._create_request()
        request.inputs['images'].CopyFrom(
            tf.contrib.util.make_tensor_proto(img_face, shape=list(img_face.shape)))
        response = self._predict(request)
        prediction = np.array(response.outputs['prob'].float_val)
        return prediction


def _create_classifier(features_hw, mean):
    return Classifier(inputs_shape=(1, 3) + features_hw,
                      input_data_format=INPUT_DATA_FORMAT,
                      output_data_format='NCHW',
                      mean=mean,
                      channel_swap=(2, 1, 0),
                      raw_scale=255,
                      image_dims=features_hw)
