"""TensorFlow Utilities."""

import os

import grpc
import tensorflow as tf
from grpc.beta import implementations


def pad_image(image, padding, data_format):
    data_format = _normalize_data_format(data_format)
    if data_format == 'channels_last':
        paddings = [[0, 0], [padding, padding], [padding, padding], [0, 0]]
    else:
        paddings = [[0, 0], [0, 0], [padding, padding], [padding, padding]]
    return tf.pad(image, paddings)


def is_nchw_data_format(data_format):
    return _normalize_data_format(data_format) == 'channels_first'


def get_channel_index(data_format):
    if is_nchw_data_format(data_format):
        return 1
    return 3


def _normalize_data_format(value):
    data_format = value.lower()
    if data_format not in {'channels_first', 'channels_last', 'nchw', 'nhwc'}:
        raise ValueError('The `data_format` argument must be one of '
                         '"channels_first", "channels_last", "NCHW","NHWC". Received: ' +
                         str(value))
    if data_format == 'nchw':
        data_format = 'channels_first'
    elif data_format == 'nhwc':
        data_format = 'channels_last'
    return data_format


def _maybe_integer_pad(inputs, kernel_size, padding, data_format):
    if isinstance(padding, int):
        if padding < 0:
            raise ValueError('The `padding` argument must be greater than zero. Received: ' +
                             str(padding))
        if padding == 0:
            padding = 'valid'
        elif padding == (kernel_size - 1) / 2:
            padding = 'same'
        else:
            inputs = pad_image(inputs, padding, data_format)
            padding = 'valid'

    return inputs, padding


def _format_value_for_logging(value):
    if isinstance(value, str):
        value = "'{}'".format(value)
    return value


def conv2d(
        inputs,
        filters,
        kernel_size,
        strides=(1, 1),
        padding='valid',
        data_format='channels_last',
        dilation_rate=(1, 1),
        activation=None,
        use_bias=True,
        kernel_initializer=None,
        bias_initializer=tf.zeros_initializer(),
        kernel_regularizer=None,
        bias_regularizer=None,
        activity_regularizer=None,
        trainable=True,
        name=None,
        reuse=None
):
    log_padding = _format_value_for_logging(padding)
    data_format = _normalize_data_format(data_format)
    inputs, padding = _maybe_integer_pad(inputs, kernel_size, padding, data_format)
    print(
        "Created conv layer: {}(filters={}, kernel_size={}, strides={}, padding={}, data_format='{}')".format(
            name, filters, kernel_size, strides, log_padding, data_format))
    return tf.layers.conv2d(
        inputs=inputs,
        filters=filters,
        kernel_size=kernel_size,
        strides=strides,
        padding=padding,
        data_format=data_format,
        dilation_rate=dilation_rate,
        activation=activation,
        use_bias=use_bias,
        kernel_initializer=kernel_initializer,
        bias_initializer=bias_initializer,
        kernel_regularizer=kernel_regularizer,
        bias_regularizer=bias_regularizer,
        activity_regularizer=activity_regularizer,
        trainable=trainable,
        name=name,
        reuse=reuse)


def max_pooling2d(
        inputs,
        pool_size,
        strides,
        padding='valid',
        data_format='channels_last',
        name=None
):
    data_format = _normalize_data_format(data_format)
    print(
        "Created pool layer: {}(pool_size={}, strides={}, padding='{}', data_format='{}')".format(name, pool_size,
                                                                                                  strides,
                                                                                                  padding, data_format))
    return tf.layers.max_pooling2d(
        inputs=inputs,
        pool_size=pool_size,
        strides=strides,
        padding=padding,
        data_format=data_format,
        name=name
    )


def dense(
        inputs,
        units,
        activation=None,
        use_bias=True,
        kernel_initializer=None,
        bias_initializer=tf.zeros_initializer(),
        kernel_regularizer=None,
        bias_regularizer=None,
        activity_regularizer=None,
        trainable=True,
        name=None,
        reuse=None
):
    print(
        "Created dense layer: {}(units={})".format(name, units))
    return tf.layers.dense(
        inputs=inputs,
        units=units,
        activation=activation,
        use_bias=use_bias,
        kernel_initializer=kernel_initializer,
        bias_initializer=bias_initializer,
        kernel_regularizer=kernel_regularizer,
        bias_regularizer=bias_regularizer,
        activity_regularizer=activity_regularizer,
        trainable=trainable,
        name=name,
        reuse=reuse
    )


def concat(
        values,
        axis,
        name='concat'
):
    print('Created concat layer: {}(axis={})'.format(name, axis))
    return tf.concat(
        values=values,
        axis=axis,
        name=name
    )


def dropout(
        inputs,
        rate=0.5,
        noise_shape=None,
        seed=None,
        training=False,
        name=None
):
    net = inputs
    if training:
        print("Created dropout layer: {}(rate={}, training={}')".format(name, rate, training))
        net = tf.layers.dropout(
            inputs=inputs,
            rate=rate,
            noise_shape=noise_shape,
            seed=seed,
            training=training,
            name=name
        )
    return net


def conv2d_transpose(
        inputs,
        filters,
        kernel_size,
        strides=(1, 1),
        padding='valid',
        data_format='channels_last',
        activation=None,
        use_bias=True,
        kernel_initializer=None,
        bias_initializer=tf.zeros_initializer(),
        kernel_regularizer=None,
        bias_regularizer=None,
        activity_regularizer=None,
        trainable=True,
        name=None,
        reuse=None
):
    log_padding = _format_value_for_logging(padding)
    data_format = _normalize_data_format(data_format)
    inputs, padding = _maybe_integer_pad(inputs, kernel_size, padding, data_format)
    print(
        "Created conv2d_transpose layer: {}(filters={}, kernel_size={}, strides={}, padding={}, data_format='{}')".format(
            name, filters, kernel_size, strides, log_padding, data_format))
    return tf.layers.conv2d_transpose(
        inputs=inputs,
        filters=filters,
        kernel_size=kernel_size,
        strides=strides,
        padding=padding,
        data_format=data_format,
        activation=activation,
        use_bias=use_bias,
        kernel_initializer=kernel_initializer,
        bias_initializer=bias_initializer,
        kernel_regularizer=kernel_regularizer,
        bias_regularizer=bias_regularizer,
        activity_regularizer=activity_regularizer,
        trainable=trainable,
        name=name,
        reuse=reuse
    )


def conv2d_sequence(
        inputs,
        layers,
        filters,
        kernel_size,
        strides=(1, 1),
        padding='valid',
        first_layer_padding=None,
        data_format='channels_last',
        dilation_rate=(1, 1),
        activation=None,
        use_bias=True,
        kernel_initializer=None,
        bias_initializer=tf.zeros_initializer(),
        kernel_regularizer=None,
        bias_regularizer=None,
        activity_regularizer=None,
        trainable=True,
        name=None,
        reuse=None
):
    net = inputs
    for i in range(layers):
        if name:
            layer_name = name.format(i + 1)
        if first_layer_padding and i == 0:
            actual_padding = first_layer_padding
        else:
            actual_padding = padding
        net = conv2d(
            inputs=net,
            filters=filters,
            kernel_size=kernel_size,
            strides=strides,
            padding=actual_padding,
            data_format=data_format,
            dilation_rate=dilation_rate,
            activation=activation,
            use_bias=use_bias,
            kernel_initializer=kernel_initializer,
            bias_initializer=bias_initializer,
            kernel_regularizer=kernel_regularizer,
            bias_regularizer=bias_regularizer,
            activity_regularizer=activity_regularizer,
            trainable=trainable,
            name=layer_name,
            reuse=reuse)
    return net


# TODO - create and pass in a DebugConfig object containing a log_device_placement attrib, etc.
def create_config_proto(log_device_placement=False):
    """
    Utility function. tf.ConfigProto is not well documented.    
    https://www.tensorflow.org/api_docs/python/tf/ConfigProto
    https://github.com/tensorflow/tensorflow/blob/master/tensorflow/core/protobuf/config.proto 
    """
    allow_soft_placement = True
    config = tf.ConfigProto(allow_soft_placement=allow_soft_placement, log_device_placement=log_device_placement)
    config.gpu_options.allow_growth = True
    return config


def init_session_and_restore_weights(model_name, weights, target='', graph=None, config=None):
    sess = init_session(target, graph, config)
    restore_weights(sess, model_name, weights)
    return sess


def init_session(target='', graph=None, config=None):
    init_op = tf.global_variables_initializer()
    sess = tf.Session(config=config)
    sess.run(init_op)
    return sess


def restore_weights(sess, model_name, weights):
    saver = tf.train.Saver(_get_named_model_variables(model_name))
    saver.restore(sess, weights)


def _is_gpu_device(device_no):
    """device_no: GPU device if >= 0 else cpu"""
    return device_no >= 0


def get_data_format(device_no):
    if _is_gpu_device(device_no):
        return 'NCHW'
    return 'NHWC'


def _get_device_name(device_no):
    if _is_gpu_device(device_no):
        device_type = 'gpu'
    else:
        device_type = 'cpu'
        device_no = 0
    return '/{}:{}'.format(device_type, device_no)


# give the example of using device fcn instead, and comment out - give the links as well
# https://www.tensorflow.org/api_docs/python/tf/Graph#device

def _maybe_with_device(model_fn, device_no):
    def with_device_fn(features, labels, mode, params):
        device_name = _get_device_name(device_no)
        print("Using device: {}".format(device_name))
        with tf.device(device_name):
            return model_fn(features, labels, mode, params)

    if device_no is not None:
        return with_device_fn
    return model_fn


def _maybe_with_variable_scope(model_fn, variable_scope):
    def with_variable_scope_fn(features, labels, mode, params):
        print("Using variable scope: {}".format(variable_scope))
        with tf.variable_scope(variable_scope):
            return model_fn(features, labels, mode, params)

    if variable_scope is not None:
        return with_variable_scope_fn
    return model_fn


def wrapped_model_fn(model_fn, features, labels, mode, params):
    model_name = params.get('model_name')
    device_no = params.get('device_no')
    params['data_format'] = get_data_format(device_no)
    print("Creating model with scope: '{}' and device no: {}".format(model_name, device_no))
    model_fn = _maybe_with_variable_scope(model_fn, model_name)  # inner scope
    model_fn = _maybe_with_device(model_fn, device_no)  # outer scope
    # with tf.name_scope(model_name): adding name_scope as well might provide a better tensorboard graph
    return model_fn(features, labels, mode, params)


def _get_named_model_checkpoint_path(checkpoint_path, model_name):
    split = checkpoint_path.rsplit('/', 1)
    return '{}/{}/{}'.format(split[0], model_name, split[1])


def _regex_exact_match(name):
    return '\\b' + name + '\\b'


def _get_named_model_variables(model_name):
    return tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=_regex_exact_match(model_name))


def save_as_named_model(sess, model_name, weights):
    named_model_weights_path = _get_named_model_checkpoint_path(weights, model_name)
    vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)
    var_rename_mapping = {model_name + '/' + str(var.name).rsplit(':', 1)[0]: var for var in vars}
    print('var_rename_mapping len: {}'.format(len(var_rename_mapping)))
    print(var_rename_mapping)
    saver = tf.train.Saver(var_rename_mapping)
    save_path = saver.save(sess, named_model_weights_path)
    print("Model saved in file: %s" % save_path)


def create_prediction_signature_def_map(features_dict, model_fn_ops, signature_name='predict_images'):
    inputs = {k: tf.saved_model.utils.build_tensor_info(v) for k, v in features_dict.items()}
    outputs = {k: tf.saved_model.utils.build_tensor_info(v) for k, v in model_fn_ops.predictions.items()}
    prediction_signature = (
        tf.saved_model.signature_def_utils.build_signature_def(
            inputs=inputs,
            outputs=outputs,
            method_name=tf.saved_model.signature_constants.PREDICT_METHOD_NAME))

    return {signature_name: prediction_signature}


def export_model(sess, export_path_base, model_version, signature_def_map):
    export_path = os.path.join(
        tf.compat.as_bytes(export_path_base),
        tf.compat.as_bytes(str(model_version)))
    print 'Exporting trained model to', export_path
    builder = tf.saved_model.builder.SavedModelBuilder(export_path)

    legacy_init_op = tf.group(tf.tables_initializer(), name='legacy_init_op')
    builder.add_meta_graph_and_variables(
        sess, [tf.saved_model.tag_constants.SERVING],
        signature_def_map=signature_def_map,
        legacy_init_op=legacy_init_op)

    builder.save()

    print 'Done exporting!'


def insecure_channel(host, port, options=None):
    """Creates an insecure Channel to a remote host.

  Args:
    host: The name of the remote host to which to connect.
    port: The port of the remote host to which to connect.
      If None only the 'host' part will be used.
    options: An optional list of key-value pairs (channel args in gRPC runtime)
    to configure the channel.

  Returns:
    A Channel to the remote host through which RPCs may be conducted.
  """
    channel = grpc.insecure_channel(host
                                    if port is None else '%s:%d' % (host, port),
                                    options)
    return implementations.Channel(channel)
