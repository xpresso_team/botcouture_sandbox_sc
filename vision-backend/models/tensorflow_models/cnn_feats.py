import numpy as np
import tensorflow as tf

import cnn_feats_model as model
import transformtools
from decaffenated.io import load_image
from infer_model import InferModel
from serving_client import TensorflowServingClient

MODEL_NAME = 'cnn_feats'
INPUT_DATA_FORMAT = 'NHWC'

class CNNFeats(InferModel):
    def __init__(self, weights, mean, device_no):
        """ Initialize CNN Feats model.

        Arguments:
            weights: string, The path of the TensorFlow CNN feats model weights file.
            mean: list of float32, The mean values of each channel of the image.
            device_no: int, The device number on which to perform computation.
        """
        super(CNNFeats, self).__init__(model_name=MODEL_NAME,
                                       weights=weights,
                                       device_no=device_no,
                                       input_data_format=INPUT_DATA_FORMAT,
                                       features_hw=[224, 224])

        # This is simply to add back the bias, re-shuffle the color channels to RGB, and so on.
        self.transformer = transformtools.SimpleTransformer(mean=mean)

    def _init_model(self, weights, device_no):
        """
        initialize the cnn
        Requires checkpoint, model.ckpt.index, model.ckpt.data*
        """
        labels = None  # 1000
        self.model_fn = model.model_fn(self.features, labels, self.mode, self.params)
        self.create_signature_def_map_fn = model.create_signature_def_map

    def extract_features_single(self, im_url, im_bbox=None, layers=['fc7'], fwd_layer=None):
        """
        extract features from cnn layers for a single image
        """
        image = load_image(im_url)
        transformed_image = self.transformer.preprocess(image, im_bbox)
        if self.should_transpose_input:
            transformed_image = np.transpose(transformed_image, (1, 2, 0))
        transformed_image = np.expand_dims(transformed_image, 0)

        predictions = self.model_fn.predictions
        layer_tensors = [predictions[layer] for layer in layers]
        layer_values = self.sess.run(layer_tensors, {self.features: transformed_image})
        feat = dict(zip(layers, layer_values))
        return feat


class CNNFeatsClient(TensorflowServingClient):
    def __init__(self, host, port, timeout, mean, device_no):
        super(CNNFeatsClient, self).__init__(model_name=MODEL_NAME,
                                             signature_name='predict_images',
                                             host=host,
                                             port=port,
                                             timeout=timeout,
                                             device_no=device_no,
                                             input_data_format=INPUT_DATA_FORMAT)

        # This is simply to add back the bias, re-shuffle the color channels to RGB, and so on.
        self.transformer = transformtools.SimpleTransformer(mean=mean)

    def extract_features_single(self, im_url, im_bbox=None, layers=['fc7'], fwd_layer=None):
        """
        extract features from cnn layers for a single image
        """
        image = load_image(im_url)
        transformed_image = self.transformer.preprocess(image, im_bbox)
        if self.should_transpose_input:
            transformed_image = np.transpose(transformed_image, (1, 2, 0))
        transformed_image = np.expand_dims(transformed_image, 0)

        request = self._create_request()
        request.inputs['images'].CopyFrom(
            tf.contrib.util.make_tensor_proto(transformed_image, shape=list(transformed_image.shape)))
        response = self._predict(request)
        feat = {layer: np.expand_dims(np.array(response.outputs[layer].float_val), axis=0) for layer in layers}
        return feat
