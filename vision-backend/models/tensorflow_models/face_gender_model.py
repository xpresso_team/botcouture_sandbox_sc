"""Convolutional Neural Network for 
cnn_age_gender_models_and_data.0.0.2/deploy_gender.prototxt, built with tf.layers."""

import tensorflow as tf
from tensorflow.contrib import learn
from tensorflow.contrib.learn.python.learn.estimators import model_fn as model_fn_lib

from tf_utils import create_prediction_signature_def_map
from tf_utils import conv2d
from tf_utils import dense
from tf_utils import dropout
from tf_utils import is_nchw_data_format
from tf_utils import max_pooling2d
from tf_utils import wrapped_model_fn

tf.logging.set_verbosity(tf.logging.INFO)


def conv_block(inputs, filters, kernel_size, strides, padding, data_format, block_number, pool_number=None,
               has_norm=True):
    net = conv2d(inputs=inputs, filters=filters, kernel_size=kernel_size,
                 strides=strides, padding=padding, data_format=data_format,
                 activation=tf.nn.relu,
                 name="conv{}".format(block_number))

    if pool_number is None:
        pool_number = block_number

    net = max_pooling2d(inputs=net,
                        pool_size=3,
                        strides=2,
                        padding='same',
                        data_format=data_format,
                        name="pool{}".format(pool_number))

    if not has_norm:
        return net

    # TODO - Make sure the parameter values coded in the lrn method call are correct.
    # The following are from the prototxt but the values in the method call came from the ethereon converter.
    # # Maybe there is some scale factor when converting between caffe impl and tensorflow impl.
    # lrn_param
    # {
    #     local_size: 5
    #     alpha: 0.0001
    #     beta: 0.75
    # }

    net = tf.nn.local_response_normalization(input=net,
                                             depth_radius=2,
                                             bias=1.0,
                                             alpha=2e-05,
                                             beta=0.75,
                                             name="norm{}".format(block_number))
    print("Created lrn layer:  norm{}()".format(block_number))
    return net


def dense_with_dropout(inputs, units, rate, training, block_number):
    """
    Densely connected layer with 'units' number of neurons followed by a 
    dropout layer with 'rate' probability that the element will be kept.
    """
    fc = dense(
        inputs=inputs, units=units, activation=tf.nn.relu,
        name="fc{}".format(block_number))

    return dropout(
        inputs=fc, rate=rate, training=training,
        name="drop{}".format(block_number))


# learning_rate=0.001, optimizer="SGD", dropout_rate=0.5
def model_fn(features, labels, mode, params):
    return wrapped_model_fn(_model_fn, features, labels, mode, params)


def _model_fn(features, labels, mode, params):
    """Model function for cnn_age_gender_models_and_data.0.0.2/deploy_gender.prototxt.

    Arguments:
        features: Matrix of shape [n_samples, n_features...] or the dictionary of Matrices.
           Can be iterator that returns arrays of features or dictionary of arrays of features.
           The training input samples for fitting the model. If set, `input_fn` must be `None`.
        labels: Vector or matrix [n_samples] or [n_samples, n_outputs] or the dictionary of same.
           Can be iterator that returns array of labels or dictionary of array of labels.
           The training label values (class labels in classification, real numbers in regression).
           If set, `input_fn` must be `None`. Note: For classification, label values must
           be integers representing the class index (i.e. values from 0 to
           n_classes-1).
        mode: Defines whether this is training, evaluation or prediction.
           See `ModeKeys`.
        params: A dict of hyperparameters.
          The following hyperparameters are expected:
          * optimizer: string, class or optimizer instance, used as trainer.
               string should be name of optimizer, like 'SGD',
                 'Adam', 'Adagrad'. Full list in OPTIMIZER_CLS_NAMES constant.
               class should be sub-class of `tf.Optimizer` that implements
                 `compute_gradients` and `apply_gradients` functions.
               optimizer instance should be instantiation of `tf.Optimizer`
                 sub-class and have `compute_gradients` and `apply_gradients`
                 functions.
           * learning_rate: float or `Tensor`, magnitude of update per each training
                step. Can be `None`.
 
    Returns:
        Output tensor. Shape is [batch_size, inputs.height/2, inputs.width/2,
        filters].
    """
    data_format = params.get('data_format')
    optimizer = params.get('optimizer')
    learning_rate = params.get('learning_rate')
    dropout_rate = params.get('dropout_rate')
    training = mode == learn.ModeKeys.TRAIN
    CLASSES = 2

    # Input Layer
    # Reshape features to 4-D tensor: [batch_size, channels, width, height]
    # images are 227x227 pixels, and have three color channels
    input_layer = features

    norm1 = conv_block(inputs=input_layer, filters=96, kernel_size=7, strides=4, padding='valid',
                       data_format=data_format, block_number=1)

    norm2 = conv_block(inputs=norm1, filters=256, kernel_size=5, strides=1, padding='same',
                       data_format=data_format, block_number=2)

    pool5 = conv_block(inputs=norm2, filters=384, kernel_size=3, strides=1, padding='same',
                       data_format=data_format, block_number=3, pool_number=5, has_norm=False)

    # Flatten tensor into a batch of vectors
    # Transpose if is NCHW shape
    # Input Tensor Shape: [batch_size, 7, 7, 384]
    # Output Tensor Shape: [batch_size, 7 * 7 * 384]
    if is_nchw_data_format(data_format):
        pool5 = tf.transpose(pool5, [0, 2, 3, 1])
    flat_pool5 = tf.reshape(pool5, [-1, 7 * 7 * 384])

    # Densely connected layer with 512 neurons and dropout
    # Input Tensor Shape: [batch_size, 7 * 7 * 512]
    # Output Tensor Shape: [batch_size, 512]
    # 'rate' probability that element will be kept
    drop6 = dense_with_dropout(inputs=flat_pool5, units=512, rate=dropout_rate, training=training, block_number=6)

    # Densely connected layer with 512 neurons and dropout
    # Input Tensor Shape: [batch_size, 512]
    # Output Tensor Shape: [batch_size, 512]
    # 'rate' probability that element will be kept
    drop7 = dense_with_dropout(inputs=drop6, units=512, rate=dropout_rate, training=training, block_number=7)

    # Logits layer -> Softmax
    # Input Tensor Shape: [batch_size, 512]
    # Output Tensor Shape: [batch_size, CLASSES]
    fc8 = dense(inputs=drop7, units=CLASSES, name='fc8')

    loss = None
    train_op = None

    # Calculate Loss (for both TRAIN and EVAL modes)
    if mode != learn.ModeKeys.INFER:
        onehot_labels = tf.one_hot(indices=tf.cast(labels, tf.int32), depth=CLASSES)
        loss = tf.losses.softmax_cross_entropy(
            onehot_labels=onehot_labels, logits=fc8)

    # Configure the Training Op (for TRAIN mode)
    if training:
        train_op = tf.contrib.layers.optimize_loss(
            loss=loss,
            global_step=tf.contrib.framework.get_global_step(),
            learning_rate=learning_rate,
            optimizer=optimizer)

    # Generate Predictions
    predictions = {
        "prob": tf.nn.softmax(
            fc8, name="softmax_tensor")
    }

    # Return a ModelFnOps object
    return model_fn_lib.ModelFnOps(
        mode=mode, predictions=predictions, loss=loss, train_op=train_op)


def create_signature_def_map(features, model_fn_ops):
    features_dict = {'images': features}
    return create_prediction_signature_def_map(features_dict, model_fn_ops)


def conversion_only_model_fn():
    """Method intended only for caffe to tensorflow conversion utility.
    Returns the model_fn with default values for features, labels, mode, params.
    The defaults are not useful for normal usage.
    """
    features = tf.zeros([1, 227, 227, 3])
    labels = tf.ones([1, 2])
    mode = learn.ModeKeys.INFER
    return model_fn(features, labels, mode, params=None)
