"""Convolutional Neural Network for 
saliency/model/deploy.prototxt, built with tf.layers.

From http://www.zhaoliming.net/research/deepsaliency
https://github.com/zlmzju/DeepSaliency
"""

import tensorflow as tf
from tensorflow.contrib import learn
from tensorflow.contrib.learn.python.learn.estimators import model_fn as model_fn_lib

from tf_utils import create_prediction_signature_def_map
from tf_utils import conv2d
from tf_utils import conv2d_sequence
from tf_utils import conv2d_transpose
from tf_utils import dropout
from tf_utils import is_nchw_data_format
from tf_utils import max_pooling2d
from tf_utils import wrapped_model_fn


def conv_block(inputs, conv_layers, filters, data_format, block_number, first_layer_padding=None):
    net = conv2d_sequence(
        inputs=inputs,
        layers=conv_layers,
        filters=filters,
        kernel_size=3,
        strides=1,  # default
        padding="same",
        first_layer_padding=first_layer_padding,
        data_format=data_format,
        activation=tf.nn.relu,
        name='conv' + str(block_number) + '_{}')

    return max_pooling2d(
        inputs=net,
        pool_size=2,
        strides=2,
        padding='same',
        data_format=data_format,
        name='pool{}'.format(block_number))


# learning_rate=0.001, optimizer="SGD", dropout_rate=0.5
def model_fn(features, labels, mode, params):
    return wrapped_model_fn(_model_fn, features, labels, mode, params)


def _model_fn(features, labels, mode, params):
    """Model function for saliency/model/deploy.prototxt.

    Arguments:
        features: Matrix of shape [n_samples, n_features...] or the dictionary of Matrices.
           Can be iterator that returns arrays of features or dictionary of arrays of features.
           The training input samples for fitting the model. If set, `input_fn` must be `None`.
        labels: Vector or matrix [n_samples] or [n_samples, n_outputs] or the dictionary of same.
           Can be iterator that returns array of labels or dictionary of array of labels.
           The training label values (class labels in classification, real numbers in regression).
           If set, `input_fn` must be `None`. Note: For classification, label values must
           be integers representing the class index (i.e. values from 0 to
           n_classes-1).
        mode: Defines whether this is training, evaluation or prediction.
           See `ModeKeys`.
        params: A dict of hyperparameters.
          The following hyperparameters are expected:
          * optimizer: string, class or optimizer instance, used as trainer.
               string should be name of optimizer, like 'SGD',
                 'Adam', 'Adagrad'. Full list in OPTIMIZER_CLS_NAMES constant.
               class should be sub-class of `tf.Optimizer` that implements
                 `compute_gradients` and `apply_gradients` functions.
               optimizer instance should be instantiation of `tf.Optimizer`
                 sub-class and have `compute_gradients` and `apply_gradients`
                 functions.
           * learning_rate: float or `Tensor`, magnitude of update per each training
                step. Can be `None`.
 
    Returns:
        Output tensor. Shape is [batch_size, inputs.height/2, inputs.width/2,
        filters].
    """
    data_format = params.get('data_format')
    optimizer = params.get('optimizer')
    learning_rate = params.get('learning_rate')
    dropout_rate = params.get('dropout_rate')
    training = mode == learn.ModeKeys.TRAIN

    net = features
    net = conv_block(net, conv_layers=2, filters=64, data_format=data_format, block_number=1, first_layer_padding=100)
    net = conv_block(net, conv_layers=2, filters=128, data_format=data_format, block_number=2)
    net = conv_block(net, conv_layers=3, filters=256, data_format=data_format, block_number=3)
    net = conv_block(net, conv_layers=3, filters=512, data_format=data_format, block_number=4)
    net = conv_block(net, conv_layers=3, filters=512, data_format=data_format, block_number=5)

    net = conv2d(
        inputs=net,
        filters=4096,
        kernel_size=7,
        strides=1,  # default
        padding="valid",
        data_format=data_format,
        activation=tf.nn.relu,
        name='conv6')

    net = dropout(inputs=net, rate=dropout_rate, training=training, name='drop6')

    net = conv2d(
        inputs=net,
        filters=4096,
        kernel_size=1,
        strides=1,  # default
        padding="valid",
        data_format=data_format,
        activation=tf.nn.relu,
        name='conv7')

    net = dropout(inputs=net, rate=dropout_rate, training=training, name='drop7')

    net = conv2d(
        inputs=net,
        filters=1,
        kernel_size=1,
        strides=1,  # default
        padding="valid",
        data_format=data_format,
        activation=None,  # default
        use_bias=False,
        name='score')

    net = conv2d_transpose(
        net,
        filters=1,
        kernel_size=63,
        strides=31,
        padding=14,
        data_format=data_format,
        activation=None,
        use_bias=False,
        trainable=True,
        name='deconv_i',
    )

    # Transpose if is NCHW shape
    if is_nchw_data_format(data_format):
        net = tf.transpose(net, [0, 2, 3, 1])
    net = tf.image.resize_image_with_crop_or_pad(
        net,
        target_height=314,
        target_width=314
    )

    outmap = tf.sigmoid(net, name='sigmoid')

    loss = None
    train_op = None

    # # Calculate Loss (for both TRAIN and EVAL modes)
    # if mode != learn.ModeKeys.INFER:
    #     onehot_labels = tf.one_hot(indices=tf.cast(labels, tf.int32), depth=CLASSES)
    #     loss = tf.losses.softmax_cross_entropy(
    #         onehot_labels=onehot_labels, logits=fc8)
    #
    # # Configure the Training Op (for TRAIN mode)
    # if training:
    #     train_op = tf.contrib.layers.optimize_loss(
    #         loss=loss,
    #         global_step=tf.contrib.framework.get_global_step(),
    #         learning_rate=learning_rate,
    #         optimizer=optimizer)

    # Generate Predictions
    predictions = {
        "outmap": outmap
    }

    # Return a ModelFnOps object
    return model_fn_lib.ModelFnOps(
        mode=mode, predictions=predictions, loss=loss, train_op=train_op)


def create_signature_def_map(features, model_fn_ops):
    features_dict = {'images': features}
    return create_prediction_signature_def_map(features_dict, model_fn_ops)


def conversion_only_model_fn():
    """Method intended only for caffe to tensorflow conversion utility.
    Returns the model_fn with default values for features, labels, mode, params.
    The defaults are not useful for normal usage.
    """
    features = tf.zeros([1, 300, 300, 3])
    labels = tf.ones([1, 2])
    mode = learn.ModeKeys.INFER
    return model_fn(features, labels, mode, params=None)
