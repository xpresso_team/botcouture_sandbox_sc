from __future__ import print_function

import caffe

from caffe_utils import initialize_device_fcn


def init(device_no):
    initialize_device_fcn(device_no)()


class FashionSegmentation:
    def __init__(self, prototxt, weights, device_no):
        """
        Initialize the fashion semantic segmentation model
        """
        self.initialize_device = initialize_device_fcn(device_no)
        self.initialize_device()
        self.segnet = caffe.Net(prototxt, weights, caffe.TEST)

    def infer(self, in_):
        """
        Infer the scores from the image 
        """
        self.initialize_device()
        self.segnet.blobs['data'].reshape(1, *in_.shape)
        self.segnet.blobs['data'].data[...] = in_
        #    print(self.segnet.blobs['data'].data.shape)
        self.segnet.forward()
        # self.segnet.clear_param_diffs()
        for blob in self.segnet.blobs:
            self.segnet.blobs[blob].diff[:] = 0

        scores = self.segnet.blobs['score'].data[0]
        return scores
