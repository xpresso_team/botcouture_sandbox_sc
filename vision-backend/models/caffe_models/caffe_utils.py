import caffe


def initialize_device_fcn(device_no):
    """  
    :param device_no: GPU device if >= 0 else cpu mode
    :return: a function that initializes the selected device
    """
    if device_no >= 0:
        def init():
            caffe.set_mode_gpu()
            caffe.set_device(device_no)
    else:
        def init():
            caffe.set_mode_cpu()

    return init
