from __future__ import print_function

import sys
import time
from collections import namedtuple

import caffe
import numpy as np

from caffe_utils import initialize_device_fcn


def init(device_no):
    initialize_device_fcn(device_no)()


class HumanPoseDetection:
    def __init__(self, prototxt, weights, device_no, model_params):
        """
        Initialize the Human pose detection model
        prototxt: string, The path of the Human Pose Detection model file.
        weights: string, The path of the Human Pose Detection model weights file.
        device_no = GPU device no if >= 0 else cpu mode
        model_params = 
        """
        self.ready = True
        self.initialize_device = initialize_device_fcn(device_no)
        self.initialize_device()
        self.device_no = device_no
        self.model = caffe.Net(prototxt, weights, caffe.TEST)

    def infer(self, images, multiplier):
        """
        Infer the heatmap and PAFs from the image 
        """
        self.initialize_device()
        num_ims = len(images)
        # net.forward() # dry run
        heatmaps = []
        pafs = []
        for image_to_test_padded in images:
            self.model.blobs['data'].reshape(*(1, 3, image_to_test_padded.shape[0], image_to_test_padded.shape[1]))
            self.model.blobs['data'].data[...] = np.transpose(np.float32(image_to_test_padded[:, :, :, np.newaxis]),
                                                          (3, 2, 0, 1)) / 256 - 0.5;
            start_time = time.time()
            output_blobs = self.model.forward()
        # self.model.clear_param_diffs()
            for blob in self.model.blobs:
                self.model.blobs[blob].diff[:] = 0
            print('At scale %d, The CNN took %.2f ms.' % (multiplier, 1000 * (time.time() - start_time)), file=sys.stderr)

        # extract outputs, resize, and remove padding
            heatmap = np.transpose(np.squeeze(self.model.blobs[output_blobs.keys()[1]].data),
                               (1, 2, 0))  # output 1 is heatmaps
            paf = np.transpose(np.squeeze(self.model.blobs[output_blobs.keys()[0]].data), (1, 2, 0))  # output 0 is PAFs
            heatmaps.append(heatmap)
            pafs.append(paf)
        return namedtuple('results', ['heatmap', 'paf'])(
            heatmap=heatmaps,
            paf=pafs)
