import caffe

from caffe_utils import initialize_device_fcn


def init(device_no):
    initialize_device_fcn(device_no)()


class FashionClassification:
    def __init__(self, prototxt, weights, device_no):
        """ Initialize Fashion Classification model.

         Arguments:
             prototxt: string, The path of the Caffe model file.
             weights: string, The path of the Caffe model weights file.
             device_no = GPU device no if >= 0 else cpu mode
         """
        self.initialize_device = initialize_device_fcn(device_no)
        self.initialize_device()
        self.net = caffe.Net(prototxt, weights, caffe.TEST)

    def infer(self, im_patch):
        """
        Infer the category from the image patch 
        """
        self.initialize_device()
        self.net.blobs["data"].data[...] = im_patch
        output = self.net.forward()
        # self.net.clear_param_diffs()
        prediction = output['prob'][0]
        for blob in self.net.blobs:
            self.net.blobs[blob].diff[:] = 0
        return prediction
