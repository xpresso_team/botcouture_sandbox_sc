import caffe

import transformtools
from caffe_utils import initialize_device_fcn


def init(device_no):
    initialize_device_fcn(device_no)()


class CNNFeats:
    def __init__(self, prototxt, weights, mean, device_no):
        """ Initialize CNN Feats model.

        Arguments:
            prototxt: string, The path of the Caffe model file.
            weights: string, The path of the Caffe model weights file.
            mean: list of float32, The mean values of each channel of the image.
            device_no = GPU device no if >= 0 else cpu mode
        """
        # The transformer simply adds back the bias, re-shuffles the color channels to RGB, and so on.
        self.transformer = transformtools.SimpleTransformer(mean=mean)
        self.initialize_device = initialize_device_fcn(device_no)
        self.initialize_device()
        self.net = caffe.Net(prototxt, weights, caffe.TEST)

    def extract_features_single(self, im_url, im_bbox=None, layers=['fc7'], fwd_layer=None):
        """
        Extract features from cnn layers for a single image
        """
        self.initialize_device()
        self.net.blobs['data'].reshape(1,  # batch size
                                       3,  # 3-channel (BGR) images
                                       224, 224)  # image size is 227x227

        image = caffe.io.load_image(im_url)
        if im_bbox:
            transformed_image = self.transformer.preprocess(image, im_bbox)
        else:
            # transformed_image = transform_image(image, False, self.mean, 256, 224)
            transformed_image = self.transformer.preprocess(image)
        self.net.blobs['data'].data[0, ...] = transformed_image
        self.net.forward(end=fwd_layer)

        feat = dict([(layer, self.net.blobs[layer].data) for layer in layers])
        return feat
