import caffe

from caffe_utils import initialize_device_fcn


def init(device_no):
    initialize_device_fcn(device_no)()


class GenderClassification:
    def __init__(self, prototxt, weights, device_no):
        """ Initialize Gender Classification model.

         Arguments:
             prototxt: string, The path of the Caffe GenderNet model file.
             weights: string, The path of the Caffe GenderNet model weights file.
            device_no = GPU device no if >= 0 else cpu mod
        """
        self.initialize_device = initialize_device_fcn(device_no)
        self.initialize_device()
        self.net = caffe.Net(prototxt, weights, caffe.TEST)
#        self.net = caffe.Classifier(prototxt, weights,
#                                    mean=mean,
#                                    channel_swap=(2, 1, 0),
#                                    raw_scale=255,
#                                    image_dims=(227, 227))

    def predict(self, img_face):
        """
        Predict the gender from the image of a face 
        """
        self.initialize_device()
        self.net.blobs["data"].data[...] = img_face
        output = self.net.forward()
        # self.net.clear_param_diffs()
        prediction = output['prob'][0]
#        prediction = self.net.predict(img_face)
        for blob in self.net.blobs:
            self.net.blobs[blob].diff[:] = 0
        return prediction
