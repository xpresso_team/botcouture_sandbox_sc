from __future__ import print_function

import caffe

from caffe_utils import initialize_device_fcn


def init(device_no):
    initialize_device_fcn(device_no)()


class SalientObjDetection:
    def __init__(self, prototxt, weights, mean, device_no):
        """
        Initialize the salient object segmentation model
        """
        self.initialize_device = initialize_device_fcn(device_no)
        self.initialize_device()
        self.net = caffe.Classifier(prototxt, weights, caffe.TEST)
        self.transformer = caffe.io.Transformer({'data': self.net.blobs['data'].data.shape})
        self.transformer.set_transpose('data', (2, 0, 1))
        self.transformer.set_mean('data', mean)
        self.transformer.set_raw_scale('data', 255)  # images in [0,255] range instead of [0,1]
        self.transformer.set_channel_swap('data', (2, 1, 0))  # channels in BGR order instead of RGB

    def predict(self, im_orig):
        """
        Predict the outmap from the image 
        """
        self.initialize_device()
        self.net.blobs['data'].reshape(len(im_orig),3,500,500)
        for i,img in enumerate(im_orig):
            imgData = self.transformer.preprocess('data', img)
            self.net.blobs['data'].data[i,...] = imgData
        self.net.forward()
        # self.net.clear_param_diffs()
        for blob in self.net.blobs:
           self.net.blobs[blob].diff[:] = 0
        outmap = self.net.blobs['outmap'].data[:, 0, :, :]
        return outmap
