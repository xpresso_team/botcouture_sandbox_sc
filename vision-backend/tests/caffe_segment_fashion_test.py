import os
from collections import namedtuple
from unittest import TestCase, main

import unittest_util as ut
from utils import print_info
from .context import FashionSegmentation

os.environ['GLOG_minloglevel'] = '2'

test_data_dir = 'tests/data/'

im_url = test_data_dir + '14716417_1608848039420808_1677131153665949696_n.jpg'
import numpy as np

DEVICE_NUMBER = 0

# SERVING_HOST = '172.17.0.2'
SERVING_HOST = '0.0.0.0'
SERVING_PORT = '9000'
SERVING_TIMEOUT = 60


# export CUDA_DEVICE_ORDER=PCI_BUS_ID
# CUDA_VISIBLE_DEVICES=2 python -m tests.caffe_segment_fashion_test


def create_segmentor(model_type, device_no=DEVICE_NUMBER):
    return FashionSegmentation(model_type=model_type,
                               model_dir=None,
                               model_name=None,
                               device_no=device_no,
                               serving_host=SERVING_HOST,
                               serving_port=SERVING_PORT,
                               serving_timeout=SERVING_TIMEOUT)


def _normalize(values):
    min = np.min(values, axis=(1, 2))[:, np.newaxis, np.newaxis]
    max = np.max(values, axis=(1, 2))[:, np.newaxis, np.newaxis]
    return (values - min) / (max - min)


def _assert_scores(scores, expected_scores, threshold_value_percentage, percent_pixels_above_threshold):
    num_elements = expected_scores.size
    abs_normalized_diff = np.abs(_normalize(scores) - _normalize(expected_scores))
    thresholded = (abs_normalized_diff > threshold_value_percentage).astype(float)
    actual_percent_pixels_above_threshold = np.sum(thresholded) / num_elements
    if actual_percent_pixels_above_threshold > percent_pixels_above_threshold:
        raise AssertionError(
            'Actual percent_pixels_above_threshold: {} exceeded desired percent_pixels_above_threshold: {} for threshold_value_percentage: {}'.format(
                actual_percent_pixels_above_threshold, percent_pixels_above_threshold, threshold_value_percentage))


def _assert_seg_prediction(seg_prediction, expected_seg_prediction, percent_pixels_not_matching):
    num_elements = expected_seg_prediction.size
    not_matching = (seg_prediction != expected_seg_prediction).astype(int)
    actual_percent_pixels_not_matching = np.sum(not_matching) / float(num_elements)
    if actual_percent_pixels_not_matching > percent_pixels_not_matching:
        raise AssertionError(
            'Actual percent_pixels_not_matching: {} exceeded desired percent_pixels_not_matching: {}'.format(
                actual_percent_pixels_not_matching, percent_pixels_not_matching))


def _infer_parameters():
    return namedtuple('params', ['im_url', 'expected_values_path'])(
        im_url=im_url,
        expected_values_path=test_data_dir + 'segmentor_infer_expected_values.npy')


def test_infer(segmentor, threshold_value_percentage=1., percent_pixels_above_threshold=1., maybe_save=False):
    params = _infer_parameters()
    in_ = segmentor._preprocess_image(params.im_url).in_
    print_info(in_, 'in_')
    scores = segmentor.model.infer(in_)
    print_info(scores, 'scores')
    if maybe_save:
        ut.maybe_save(params.expected_values_path, scores)
    expected_scores = ut.load(params.expected_values_path)
    _assert_scores(scores, expected_scores, threshold_value_percentage, percent_pixels_above_threshold)


def _segment_fashion_parameters():
    return namedtuple('params', ['im_url', 'expected_values_path'])(
        im_url=im_url,
        expected_values_path=test_data_dir + 'segmentor_segment_fashion_expected_values.npy')


def test_segment_fashion(segmentor, percent_pixels_not_matching=0., maybe_save=False):
    params = _segment_fashion_parameters()
    seg_prediction, np_array_im = segmentor.segment_fashion(params.im_url, 'women')
    print_info(seg_prediction, 'seg_prediction')
    print_info(np_array_im, 'np_array_im')
    # seg_prediction(600, 600), np_array_im(600, 600, 3)
    if maybe_save:
        ut.maybe_save(params.expected_values_path, seg_prediction)
    expected_seg_prediction = ut.load(params.expected_values_path)
    _assert_seg_prediction(seg_prediction, expected_seg_prediction, percent_pixels_not_matching)


class FashionSegmentationTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        cls._segmentor = create_segmentor('caffe')

    def test_infer(self):
        test_infer(self.__class__._segmentor, maybe_save=True)

    def test_segment_fashion(self):
        test_segment_fashion(self.__class__._segmentor, maybe_save=True)


if __name__ == '__main__':
    main()
