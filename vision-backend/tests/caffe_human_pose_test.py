import os
from collections import namedtuple
from unittest import TestCase, main

import numpy as np
import skimage.io as skio

import unittest_util as ut
from .context import HumanPoseDetection

os.environ['GLOG_minloglevel'] = '2'

test_data_dir = 'tests/data/'

im_url = test_data_dir + '5135563_look_black_skirt.jpg'

DEVICE_NUMBER = 0

# SERVING_HOST = '172.17.0.2'
SERVING_HOST = '0.0.0.0'
SERVING_PORT = '9000'
SERVING_TIMEOUT = 60


# export CUDA_DEVICE_ORDER=PCI_BUS_ID
# CUDA_VISIBLE_DEVICES=2 python -m tests.caffe_human_pose_test


def create_pose_extractor(model_type, device_no=DEVICE_NUMBER):
    params = {'model_type': 'ZheC_RTMPPose'}
    return HumanPoseDetection(model_type=model_type,
                              model_dir=None,
                              model_name=None,
                              device_no=device_no,
                              params=params,
                              serving_host=SERVING_HOST,
                              serving_port=SERVING_PORT,
                              serving_timeout=SERVING_TIMEOUT)


def _pose_image():
    return skio.imread(im_url)


# Executing in the docker container produced slightly different results than executing in the python virtualenv
# docker container results
# def expected_person_docker_container():
#     return {'Reye': [44, 409], 'neck': [98, 431], 'Rear': [51, 433], 'Lkne': [303, 451], 'Lwri': [-1, -1],
#             'Rkne': [305, 411], 'Lelb': [166, 474], 'Lsho': [96, 466], 'Rhip': [216, 408], 'Rank': [382, 415],
#             '[nose': [51, 400], 'Lear': [-1, -1], 'pt19]': [-1, -1], 'Lhip': [214, 450], 'Rsho': [100, 395],
#             'Lank': [387, 479], 'Relb': [162, 368], 'Leye': [44, 401], 'Rwri': [211, 352],
#             'bbox': {'person': {'y2': 457, 'width': 127, 'height': 343, 'x2': 549, 'y1': 0, 'x1': 282},
#                      'lower': {'y1': 181, 'x2': 485, 'x1': 373, 'y2': 422, 'pose_type': 'lower'},
#                      'full': {'y1': 65, 'x2': 501, 'x1': 360, 'y2': 422, 'pose_type': 'full'},
#                      'footwear': {'y1': 347, 'x2': 514, 'x1': 380, 'y2': 457, 'pose_type': 'person'},
#                      'upper': {'y1': 65, 'x2': 501, 'x1': 360, 'y2': 249, 'pose_type': 'upper'}}}


#  python virtualenv results
def _expected_person():
    return {'Reye': [41, 412], 'neck': [97, 431], 'Rear': [48, 434], 'Lkne': [294, 453], 'Lwri': [205, 479],
            'Rkne': [298, 412], 'Lelb': [153, 483], 'Lsho': [93, 464], 'Rhip': [212, 412], 'Rank': [377, 412],
            '[nose': [48, 404], 'Lear': [-1, -1], 'pt19]': [-1, -1], 'Lhip': [209, 457], 'Rsho': [97, 393],
            'Lank': [377, 479], 'Relb': [156, 371], 'Leye': [37, 401], 'Rwri': [205, 359],
            'bbox': {'upper': {'y1': 58, 'x2': 500, 'x1': 358, 'y2': 248, 'pose_type': 'upper'},
                     'lower': {'y1': 174, 'x2': 515, 'x1': 377, 'y2': 413, 'pose_type': 'lower'},
                     'full': {'y1': 58, 'x2': 519, 'x1': 324, 'y2': 413, 'pose_type': 'full'},
                     'eyes': {'y1': 32, 'x2': 418, 'x1': 396, 'y2': 52, 'pose_type': 'eyes'},
                     'face': {'y1': 27, 'x2': 440, 'x1': 396, 'y2': 64, 'pose_type': 'face'},
                     'person': {'y2': 447, 'width': 124, 'height': 340, 'x2': 553, 'y1': 0, 'x1': 289},
                     'footwear': {'y1': 342, 'x2': 515, 'x1': 377, 'y2': 448, 'pose_type': 'footwear'}}}


# def full_person_infer_parameters():
#     return namedtuple('params', ['im_patch', 'expected_values_path'])(
#         im_patch=transform_image(pose_image()),
#         expected_values_path=test_data_dir + 'pose_extractor_full_person_infer_expected_values.npy')


def detect_pose_parameters():
    return namedtuple('params', ['oriImg', 'person'])(
        oriImg=_pose_image(),
        person=_expected_person())


def test_detect_pose(pose_extractor):
    params = detect_pose_parameters()
    persons = pose_extractor.detect_pose([params.oriImg])
    persons = persons[0]
    # print('persons: {}'.format(persons))
    np.testing.assert_equal(len(persons), 1)
    print('persons[0]: {}'.format(persons[0]))
    ut.assert_dict_equals(persons[0], params.person, integer_rtol=2)  # e-02)


class HumanPoseDetectionTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        cls._pose_extractor = create_pose_extractor('caffe')

    # def test_full_person_infer(self):
    #     params = full_person_infer_parameters()
    #     pose_extractor = self.__class__._pose_extractor.model
    #     probability = pose_extractor.infer(params.im_patch)
    #     print_probability(probability)
    #     ut.maybe_save(params.expected_values_path, probability)
    #     expected_probability = ut.load(params.expected_values_path)
    #     ut.assert_values(probability, expected_probability)

    def test_detect_pose(self):
        pose_extractor = self.__class__._pose_extractor
        test_detect_pose(pose_extractor)


if __name__ == '__main__':
    main()
