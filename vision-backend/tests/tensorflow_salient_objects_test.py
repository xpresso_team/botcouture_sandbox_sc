from unittest import main

import caffe_salient_objects_test as base
from tensorflow_test_case import TensorFlowTestCase


# export CUDA_DEVICE_ORDER=PCI_BUS_ID
# CUDA_VISIBLE_DEVICES=2 python -m tests.tensorflow_salient_objects_test


class TensorFlowSalientObjDetectionTestCase(TensorFlowTestCase):
    def test_predict_cpu(self):
        salient_detector = base.create_salient_detector(model_type='tensorflow', device_no=-1)
        base.test_predict(salient_detector, atol=10)

    def test_predict_gpu(self):
        salient_detector = base.create_salient_detector(model_type='tensorflow')
        base.test_predict(salient_detector, atol=10)

    def test_predict_client_cpu(self):
        salient_detector = base.create_salient_detector(model_type='serving', device_no=-1)
        base.test_predict(salient_detector, atol=10)

    def test_get_salient_objects_cpu(self):
        salient_detector = base.create_salient_detector(model_type='tensorflow', device_no=-1)
        base.test_get_salient_objects(salient_detector, rtol=10)

    def test_get_salient_objects_gpu(self):
        salient_detector = base.create_salient_detector(model_type='tensorflow')
        base.test_get_salient_objects(salient_detector, rtol=10)

    def test_get_salient_objects_client_cpu(self):
        salient_detector = base.create_salient_detector(model_type='serving', device_no=-1)
        base.test_get_salient_objects(salient_detector, rtol=10)


if __name__ == '__main__':
    main()
