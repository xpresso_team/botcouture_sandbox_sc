import os
from collections import namedtuple
from unittest import TestCase, main

import unittest_util as ut
from .context import CNNFeats

os.environ['GLOG_minloglevel'] = '2'

test_data_dir = 'tests/data/'

DEVICE_NUMBER = 0

# SERVING_HOST = '172.17.0.2'
SERVING_HOST = '0.0.0.0'
SERVING_PORT = '9000'
SERVING_TIMEOUT = 60


# export CUDA_DEVICE_ORDER=PCI_BUS_ID
# CUDA_VISIBLE_DEVICES=2 python -m tests.caffe_cnn_feats_test


def create_cnnfeats(model_type, device_no=DEVICE_NUMBER):
    return CNNFeats(model_type=model_type,
                    model_dir=None,
                    model_name=None,
                    device_no=device_no,
                    serving_host=SERVING_HOST,
                    serving_port=SERVING_PORT,
                    serving_timeout=SERVING_TIMEOUT)


def _extract_features_single_no_bbox_parameters():
    return namedtuple('params', ['im_url', 'im_bbox', 'layers', 'expected_values_path'])(
        im_url=test_data_dir + '2008_004342.jpg',
        im_bbox=None,
        layers=['fc7', 'fc6'],
        expected_values_path=test_data_dir + 'cnn_feats_expected_values.npy')


def test_extract_features_single_no_bbox(cnnfeats, name, maybe_save=False):
    params = _extract_features_single_no_bbox_parameters()
    feats = cnnfeats.extract_features_single(params.im_url, params.im_bbox, params.layers)
    ut.print_values(feats, name)
    if maybe_save:
        ut.maybe_save(params.expected_values_path, feats)
    expected_feats = ut.load_to_dict(params.expected_values_path)
    ut.assert_values(feats, expected_feats)


class CNNFeatsTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        cls._cnnfeats = create_cnnfeats('caffe')

    def test_extract_features_single_no_bbox(self):
        cnnfeats = self.__class__._cnnfeats
        test_extract_features_single_no_bbox(cnnfeats, 'caffe_feats', maybe_save=True)


if __name__ == '__main__':
    main()
