from collections import namedtuple
from unittest import TestCase, main

import unittest_util as ut
from utils import print_info
from .context import VisualSearch
import os
os.environ['GLOG_minloglevel'] = '2'

test_data_dir = 'tests/data/'

im_url = test_data_dir + '14716417_1608848039420808_1677131153665949696_n.jpg'

DEVICE_NUMBER = 0


# export CUDA_DEVICE_ORDER=PCI_BUS_ID
# CUDA_VISIBLE_DEVICES=2 python -m tests.caffe_visual_search_test


def create_visual_search(model_type, params=None, device_no=DEVICE_NUMBER):
    if not params:
        params = config_parameters(model_type)
    return VisualSearch(params, device_no=device_no)


def config_parameters(model_type):
    return {'CLASSIFIER_MODEL_TYPE': model_type,
            'FEAT_MODEL_TYPE': model_type,
            'GENDER_CLASSIFIER_MODEL_TYPE': model_type,
            'POSE_EXTRACTOR_MODEL_TYPE': model_type,
            'SALIENT_DETECTOR_MODEL_TYPE': model_type,
            'SEGMENTOR_MODEL_TYPE': model_type}


def visual_search_parameters():
    return namedtuple('params', ['im_url', 'expected_values_path'])(
        im_url=im_url,
        expected_values_path=test_data_dir + 'visual_search_items_and_feats_expected_values.npy')


def test_visual_search(visual_search, integer_rtol=1e-07, maybe_save=False):
    params = visual_search_parameters()
    fpatches = visual_search.items_and_feats(params.im_url, 'women')
    print_info(fpatches, 'fpatches')
    if maybe_save:
        ut.maybe_save(params.expected_values_path, fpatches)
    expected_fpatches_final = ut.load(params.expected_values_path)
    ut.assert_dict_equals(fpatches, expected_fpatches_final, integer_rtol=integer_rtol)


class VisualSearchTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        cls._visual_search = create_visual_search('caffe')

    def test_visual_search(self):
        visual_search = self.__class__._visual_search
        test_visual_search(visual_search, maybe_save=True)


if __name__ == '__main__':
    main()
