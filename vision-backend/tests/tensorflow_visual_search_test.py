from unittest import main

import caffe_visual_search_test as base
from tensorflow_test_case import TensorFlowTestCase


# export CUDA_DEVICE_ORDER=PCI_BUS_ID
# CUDA_VISIBLE_DEVICES=2 python -m tests.tensorflow_visual_search_test


class VisualSearchTestCase(TensorFlowTestCase):
    def test_visual_search(self):
        visual_search = base.create_visual_search(model_type='tensorflow', device_no=-1)
        base.test_visual_search(visual_search, integer_rtol=1e-02)


if __name__ == '__main__':
    main()
