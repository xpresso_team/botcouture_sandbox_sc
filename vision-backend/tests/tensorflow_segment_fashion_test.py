from unittest import main

import caffe_segment_fashion_test as base
from tensorflow_test_case import TensorFlowTestCase


# export CUDA_DEVICE_ORDER=PCI_BUS_ID
# CUDA_VISIBLE_DEVICES=2 python -m tests.tensorflow_segment_fashion_test


class FashionSegmentationTestCase(TensorFlowTestCase):
    def test_infer_cpu(self):
        segmentor = base.create_segmentor(model_type='tensorflow', device_no=-1)
        base.test_infer(segmentor, threshold_value_percentage=1.0, percent_pixels_above_threshold=1.0)

    def test_infer_gpu(self):
        segmentor = base.create_segmentor(model_type='tensorflow')
        base.test_infer(segmentor, threshold_value_percentage=1.0, percent_pixels_above_threshold=1.0)

    def test_infer_client_cpu(self):
        segmentor = base.create_segmentor(model_type='serving', device_no=-1)
        base.test_infer(segmentor, threshold_value_percentage=1.0, percent_pixels_above_threshold=1.0)

    def test_segment_fashion_cpu(self):
        segmentor = base.create_segmentor(model_type='tensorflow', device_no=-1)
        base.test_segment_fashion(segmentor, percent_pixels_not_matching=1.0)

    def test_segment_fashion_gpu(self):
        segmentor = base.create_segmentor(model_type='tensorflow')
        base.test_segment_fashion(segmentor, percent_pixels_not_matching=1.0)

    def test_segment_fashion_client_cpu(self):
        segmentor = base.create_segmentor(model_type='serving', device_no=-1)
        base.test_segment_fashion(segmentor, percent_pixels_not_matching=1.0)


if __name__ == '__main__':
    main()
