import os
from collections import namedtuple
from unittest import TestCase, main

import numpy as np

import unittest_util as ut
from decaffenated.io import load_image
from utils import print_info
from .context import SalientObjDetection

os.environ['GLOG_minloglevel'] = '2'

test_data_dir = 'tests/data/'

im_url = test_data_dir + 'birds.jpg'

DEVICE_NUMBER = 0

# SERVING_HOST = '172.17.0.2'
SERVING_HOST = '0.0.0.0'
SERVING_PORT = '9000'
SERVING_TIMEOUT = 60


# export CUDA_DEVICE_ORDER=PCI_BUS_ID
# CUDA_VISIBLE_DEVICES=2 python -m tests.caffe_salient_objects_test


def create_salient_detector(model_type, device_no=DEVICE_NUMBER):
    return SalientObjDetection(model_type=model_type,
                               model_dir=None,
                               model_name=None,
                               device_no=device_no,
                               serving_host=SERVING_HOST,
                               serving_port=SERVING_PORT,
                               serving_timeout=SERVING_TIMEOUT)


def _get_predict_parameters():
    return namedtuple('params', ['im', 'expected_values_path'])(
        im=load_image(im_url),
        expected_values_path=test_data_dir + 'salient_detector_predict_expected_values.npy')


def test_predict(salient_detector, atol=0, maybe_save=False):
    params = _get_predict_parameters()
    print_info(params.im, 'im')
    outmap = salient_detector.model.predict(params.im)
    print_info(outmap, 'outmap')
    if maybe_save:
        ut.maybe_save(params.expected_values_path, outmap)
    expected_outmap = ut.load(params.expected_values_path)
    ut.assert_values(outmap, expected_outmap, rtol=1e-02, atol=atol)


def _get_salient_objects_parameters():
    return namedtuple('params', ['im_url', 'obj_rects_final'])(
        im_url=im_url,
        obj_rects_final=[(0, 88.199, 578.2, 306.6), (243.2, 93.6, 599, 275.8)])
        # obj_rects_final=[0, 121, 599, 231])


def test_get_salient_objects(salient_detector, rtol=1e-07):
    params = _get_salient_objects_parameters()
    print("Getting salient objects in " + params.im_url)
    map_final, im_orig, obj_rects_final = salient_detector.get_salient_objects([params.im_url], single_mode=False)
    # map_final(464, 600), im_orig(464, 600, 3), obj_rects_final[(0, 121, 599, 231)]
    print_info(obj_rects_final, 'obj_rects_final')
    print(obj_rects_final)
    np.testing.assert_equal(len(obj_rects_final), 2)
    # obj_rects_final (x, y, w, h)
    actual_obj_rects_final = np.asarray(obj_rects_final)
    np.testing.assert_allclose(actual_obj_rects_final, params.obj_rects_final, rtol=rtol)


class CaffeSalientObjDetectionTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        cls._salient_detector = create_salient_detector('caffe')

    def test_predict(self):
        test_predict(self.__class__._salient_detector, maybe_save=True)

    def test_get_salient_objects(self):
        test_get_salient_objects(self.__class__._salient_detector, rtol=10)


if __name__ == '__main__':
    main()
