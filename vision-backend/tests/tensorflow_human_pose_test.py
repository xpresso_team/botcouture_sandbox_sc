from unittest import main

import caffe_human_pose_test as base
from tensorflow_test_case import TensorFlowTestCase


# export CUDA_DEVICE_ORDER=PCI_BUS_ID
# CUDA_VISIBLE_DEVICES=2 python -m tests.tensorflow_human_pose_test


class TensorFlowHumanPoseDetectionTestCase(TensorFlowTestCase):
    def test_detect_pose_cpu(self):
        pose_extractor = base.create_pose_extractor(model_type='tensorflow', device_no=-1)
        base.test_detect_pose(pose_extractor)

    def test_detect_pose_gpu(self):
        pose_extractor = base.create_pose_extractor(model_type='tensorflow')
        base.test_detect_pose(pose_extractor)

    def test_detect_pose_client_cpu(self):
        pose_extractor = base.create_pose_extractor(model_type='serving', device_no=-1)
        base.test_detect_pose(pose_extractor)


if __name__ == '__main__':
    main()
