from unittest import main

import caffe_classify_fashion_test as base
from tensorflow_test_case import TensorFlowTestCase


# export CUDA_DEVICE_ORDER=PCI_BUS_ID
# CUDA_VISIBLE_DEVICES=2 python -m tests.tensorflow_classify_fashion_test

class TensorFlowFashionClassificationTestCase(TensorFlowTestCase):
    def test_full_person_infer_cpu(self):
        classifier = base.create_classifier(model_type='tensorflow', device_no=-1)
        base.test_full_person_infer(classifier.model)

    def test_full_person_infer_gpu(self):
        classifier = base.create_classifier(model_type='tensorflow')
        base.test_full_person_infer(classifier.model)

    def test_full_person_infer_client_cpu(self):
        classifier = base.create_classifier(model_type='serving', device_no=-1)
        base.test_full_person_infer(classifier.model)

    def test_full_person_classify_patches_cpu(self):
        classifier = base.create_classifier(model_type='tensorflow', device_no=-1)
        base.test_full_person_classify_patches(classifier, rtol=1e-06)

    def test_full_person_classify_patches_gpu(self):
        classifier = base.create_classifier(model_type='tensorflow')
        base.test_full_person_classify_patches(classifier, rtol=1e-06)


if __name__ == '__main__':
    main()
