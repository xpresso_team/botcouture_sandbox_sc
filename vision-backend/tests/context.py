import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from classify_fashion import FashionClassification
from cnn_feats import CNNFeats
from face_gender import GenderClassification
from human_pose import HumanPoseDetection
from salient_objects import SalientObjDetection
from segment_fashion import FashionSegmentation
from visual_search import VisualSearch
