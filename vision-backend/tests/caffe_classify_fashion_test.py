import os
from collections import namedtuple
from unittest import TestCase, main

import numpy as np

import unittest_util as ut
from txutils import Transformer
from .context import FashionClassification

os.environ['GLOG_minloglevel'] = '2'

test_data_dir = 'tests/data/'

im_url = test_data_dir + '2008_004342.jpg'

DEVICE_NUMBER = 0

# SERVING_HOST = '172.17.0.2'
SERVING_HOST = '0.0.0.0'
SERVING_PORT = '9000'
SERVING_TIMEOUT = 60

# export CUDA_DEVICE_ORDER=PCI_BUS_ID
# CUDA_VISIBLE_DEVICES=2 python -m tests.caffe_classify_fashion_test
txparams = {'image_dim': 256, 'crop_dim': 224,
            'oversample': False}
tx = Transformer(params=txparams)


def create_classifier(model_type, device_no=DEVICE_NUMBER):
    return FashionClassification(model_type=model_type,
                                 model_dir=None,
                                 model_name=None,
                                 device_no=device_no,
                                 serving_host=SERVING_HOST,
                                 serving_port=SERVING_PORT,
                                 serving_timeout=SERVING_TIMEOUT)


def _full_person_patch():
    size = 185
    return ut.image_patch(im_url, x=75, y=75, w=size, h=size)


def _full_person_infer_parameters():
    return namedtuple('params', ['im_patch', 'expected_values_path'])(
        im_patch=tx.preprocess(_full_person_patch()),
        expected_values_path=test_data_dir + 'classifier_full_person_infer_expected_values.npy')


def _full_person_classify_patches_parameters():
    return namedtuple('params', ['im_patch', 'expected_source_res'])(
        im_patch=_full_person_patch(),
        expected_source_res={'person': {'y1': 0, 'x2': 184, 'x1': 0, 'y2': 184, 'prob': 0.9977812170982361}})


def _print_probability(probability):
    print('probability shape: {}'.format(probability.shape))
    print('probability: {}'.format(probability))
    print('argmax(probability): {}'.format(np.argmax(probability)))


def test_full_person_infer(classifier, maybe_save=False):
    params = _full_person_infer_parameters()
    probability = classifier.infer(params.im_patch)
    _print_probability(probability)
    if maybe_save:
        ut.maybe_save(params.expected_values_path, probability)
    expected_probability = ut.load(params.expected_values_path)
    ut.assert_values(probability, expected_probability)


def test_full_person_classify_patches(classifier, rtol=1e-07):
    params = _full_person_classify_patches_parameters()
    source_res = classifier.classify_patches(
        params.im_patch, patches=None, gender=None, image_file_prefix=None, image_link_prefix="")
    print(source_res)
    ut.assert_dict_equals(source_res, params.expected_source_res, rtol=rtol)


class FashionClassificationTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        cls._classifier = create_classifier('caffe')

    def test_full_person_infer(self):
        classifier = self.__class__._classifier
        test_full_person_infer(classifier.model, maybe_save=True)

    def test_full_person_classify_patches(self):
        classifier = self.__class__._classifier
        test_full_person_classify_patches(classifier)


if __name__ == '__main__':
    main()
