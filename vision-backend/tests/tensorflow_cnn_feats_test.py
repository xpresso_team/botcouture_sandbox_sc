from unittest import main

import caffe_cnn_feats_test as base
from tensorflow_test_case import TensorFlowTestCase


# export CUDA_DEVICE_ORDER=PCI_BUS_ID
# CUDA_VISIBLE_DEVICES=2 python -m tests.tensorflow_cnn_feats_test


class TensorFlowCNNFeatsTestCase(TensorFlowTestCase):
    def test_extract_features_single_no_bbox_cpu(self):
        cnnfeats = base.create_cnnfeats(model_type='tensorflow', device_no=-1)
        base.test_extract_features_single_no_bbox(cnnfeats, 'tensorflow_feats')

    def test_extract_features_single_no_bbox_gpu(self):
        cnnfeats = base.create_cnnfeats(model_type='tensorflow')
        base.test_extract_features_single_no_bbox(cnnfeats, 'tensorflow_feats')

    def test_extract_features_single_no_bbox_client_cpu(self):
        cnnfeats = base.create_cnnfeats(model_type='serving', device_no=-1)
        base.test_extract_features_single_no_bbox(cnnfeats.model, 'tensorflow_feats')


if __name__ == '__main__':
    main()
