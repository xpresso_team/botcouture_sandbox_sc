from unittest import main

import caffe_face_gender_test as base
from tensorflow_test_case import TensorFlowTestCase


# export CUDA_DEVICE_ORDER=PCI_BUS_ID
# CUDA_VISIBLE_DEVICES=2 python -m tests.tensorflow_face_gender_test


class TensorFlowGenderClassificationTestCase(TensorFlowTestCase):
    def test_gender_predict_cpu(self):
        gender_classifier = base.create_gender_classifier(model_type='tensorflow', device_no=-1)
        base.test_gender_predict(gender_classifier)

    def test_gender_predict_gpu(self):
        gender_classifier = base.create_gender_classifier(model_type='tensorflow')
        base.test_gender_predict(gender_classifier, rtol=0.007)

    def test_gender_predict_client_cpu(self):
        gender_classifier = base.create_gender_classifier(model_type='serving', device_no=-1)
        base.test_gender_predict(gender_classifier)

    def test_gender_classify_cpu(self):
        gender_classifier = base.create_gender_classifier(model_type='tensorflow', device_no=-1)
        base.test_gender_classify(gender_classifier)

    def test_gender_classify_gpu(self):
        gender_classifier = base.create_gender_classifier(model_type='tensorflow')
        base.test_gender_classify(gender_classifier)

    def test_gender_classify_client_cpu(self):
        gender_classifier = base.create_gender_classifier(model_type='serving', device_no=-1)
        base.test_gender_classify(gender_classifier)


if __name__ == '__main__':
    main()
