import collections
import os.path

import cv2
import matplotlib.pyplot as plt
import numpy as np


def create_sequential_data(shape):
    return np.arange(np.prod(shape)).astype(float).reshape(shape)


def maybe_save(data_path, data):
    if not os.path.exists(data_path): save(data_path, data)


def save(data_path, data):
    print("Saving data to path: {}".format(data_path))
    with open(data_path, 'wb') as data_out:
        np.save(data_out, data)


def load(data_path):
    return np.load(data_path)


def load_to_dict(data_path):
    return load(data_path).item()


def print_values(values, name):
    if isinstance(values, dict):
        for key, val in values.items():
            print(
                "{}['{}'] mean: {}, std: {}, min: {}, max: {}".format(name, key, np.mean(val), np.std(val), np.min(val),
                                                                      np.max(val)))
        for key, val in values.items():
            print("{}['{}'] values: {}".format(name, key, val[:, :10]))
    else:
        print("{} mean: {}, std: {}, min: {}, max: {}".format(name, np.mean(values), np.std(values), np.min(values),
                                                              np.max(values)))
        print("{} values: {}".format(name, values.flat[:10]))


def assert_values(actual, desired, rtol=1e-03, atol=0):
    # rtol = 1e-07 numpy default fails, 1e-03 is smallest that passes
    if isinstance(desired, dict):
        for key, val in desired.items():
            np.testing.assert_allclose(actual[key], desired[key], rtol=rtol, atol=atol)
    else:
        np.testing.assert_allclose(actual, desired, rtol=rtol, atol=atol)


def assert_dict_equals(actual, desired, rtol=1e-07, integer_rtol=None):
    try:
        data_path = collections.deque()
        _do_assert_dict_equals(actual, desired, data_path, rtol=rtol, integer_rtol=integer_rtol)
    except AssertionError as e:
        raise AssertionError("{}\nPath: {}".format(str(e), ''.join(data_path)))


def _do_assert_dict_equals(actual, desired, data_path, rtol=1e-07, integer_rtol=None):
    if isinstance(desired, dict):
        np.testing.assert_equal(len(actual.keys()), len(desired.keys()))
        for key, desired_val in desired.items():
            data_path.append("['{}']".format(key))
            # print('asserting dict key: {}'.format(key))
            _do_assert_dict_equals(actual[key], desired_val, data_path, rtol=rtol, integer_rtol=integer_rtol)
            data_path.pop()
    elif isinstance(desired, list):
        np.testing.assert_equal(len(actual), len(desired))
        for i, desired_val in enumerate(desired):
            data_path.append("[{}]".format(i))
            # print('asserting list item: {}'.format(i))
            _do_assert_dict_equals(actual[i], desired_val, data_path, rtol=rtol, integer_rtol=integer_rtol)
            data_path.pop()
    # elif isinstance(desired, namedtuple):
    #    _do_assert_dict_equals(actual._asdict(), desired._asdict(), data_path, rtol=rtol, integer_rtol=integer_rtol)
    elif isinstance(desired, int):
        if integer_rtol is None:
            # print('integer_rtol is None, comparing: {} == {}'.format(actual, desired))
            np.testing.assert_equal(actual, desired)
        else:
            # print('integer_rtol is {}, comparing: {} ~= {}'.format(integer_rtol, actual, desired))
            np.testing.assert_allclose(actual, desired, rtol=integer_rtol)
    elif isinstance(desired, float):
        # print('comparing floats: {} == {}'.format(actual, desired))
        np.testing.assert_allclose(actual, desired, rtol=rtol)
    else:
        # print('comparing {}: {} == {}'.format(type(desired), actual, desired))
        if desired != actual:
            raise AssertionError(
                'Actual value: {} not equal to desired value: {}'.format(actual, desired))


def image_patch(image_path, x, y, w, h):
    # print('image patch x, y, w, h: {}, {}, {}, {}'.format(x, y, w, h))
    img = cv2.imread(image_path)
    img_patch = img[y:y + h, x:x + w, :]
    # print('image patch shape: {}'.format(img_patch.shape))
    return img_patch


def bgr_to_rgb(bgr_img):
    return bgr_img[..., ::-1]


def plot_image(img, format='BGR'):
    print('plotting image with shape: {}'.format(img.shape))
    img = np.squeeze(img)
    if img.shape[0] == 3:
        img = np.transpose(img, [1, 2, 0])
    if format == 'BGR':
        img = bgr_to_rgb(img)
    plt.imshow(img)
    plt.show()


desired = {'Reye': [44, 409], 'neck': [98, 431], 'Rear': [51, 433], 'Lkne': [303, 451], 'Lwri': [-1, -1],
           'Rkne': [305, 411], 'Lelb': [166, 474], 'Lsho': [96, 466], 'Rhip': [216, 408], 'Rank': [382, 415],
           '[nose': [51, 400], 'Lear': [-1, -1], 'pt19]': [-1, -1], 'Lhip': [214, 450], 'Rsho': [100, 395],
           'Lank': [387, 479], 'Relb': [162, 368], 'Leye': [44, 401], 'Rwri': [211, 352],
           'bbox': {'person': {'y2': 457, 'width': 127, 'height': 343, 'x2': 549, 'y1': 0, 'x1': 282},
                    'lower': {'y1': 181, 'x2': 485, 'x1': 373, 'y2': 422, 'pose_type': 'lower'},
                    'full': {'y1': 65, 'x2': 501, 'x1': 360, 'y2': 422, 'pose_type': 'full'},
                    'footwear': {'y1': 347, 'x2': 514, 'x1': 380, 'y2': 457, 'pose_type': 'person'},
                    'upper': {'y1': 65, 'x2': 501, 'x1': 360, 'y2': 249, 'pose_type': 'upper'}}}

import copy

actual = copy.deepcopy(desired)
actual['Reye'] = [44, 410]  # [44, 409]
# actual['bbox']['person']['x2'] = 548  # 549

if __name__ == '__main__':
    # assert_dict_equals(actual, desired)
    # assert_dict_equals(actual, desired, integer_rtol=1e-03)
    assert_dict_equals(actual, desired, integer_rtol=1e-02)
