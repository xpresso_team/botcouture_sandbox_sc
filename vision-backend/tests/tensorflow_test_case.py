from unittest import TestCase

import tensorflow as tf


# export CUDA_DEVICE_ORDER=PCI_BUS_ID
# CUDA_VISIBLE_DEVICES=2 time python -m tests.tensorflow_*_test


class TensorFlowTestCase(TestCase):
    def tearDown(self):
        tf.reset_default_graph()
