import os
from collections import namedtuple
from unittest import TestCase, main

import PIL.Image as PILim
import numpy as np

import unittest_util as ut
from .context import GenderClassification

os.environ['GLOG_minloglevel'] = '2'

test_data_dir = 'tests/data/'

im_url = test_data_dir + 'age_gender_example_image.jpg'

DEVICE_NUMBER = 0

# SERVING_HOST = '172.17.0.2'
SERVING_HOST = '0.0.0.0'
SERVING_PORT = '9000'
SERVING_TIMEOUT = 60


# export CUDA_DEVICE_ORDER=PCI_BUS_ID
# CUDA_VISIBLE_DEVICES=2 python -m tests.caffe_face_gender_test


def create_gender_classifier(model_type, device_no=DEVICE_NUMBER):
    return GenderClassification(model_type=model_type,
                                model_dir=None,
                                model_name=None,
                                device_no=device_no,
                                serving_host=SERVING_HOST,
                                serving_port=SERVING_PORT,
                                serving_timeout=SERVING_TIMEOUT)


def _face_image():
    return np.array(PILim.open(im_url))


def _gender_predict_parameters():
    return namedtuple('params', ['img_face', 'expected_values_path'])(
        img_face=_face_image(),
        expected_values_path=test_data_dir + 'gender_classifier_predict_expected_values.npy')


def _gender_classify_parameters():
    return namedtuple('params', ['img_face'])(
        img_face=_face_image())


def test_gender_predict(gender_classifier, rtol=1e-03, maybe_save=False):
    params = _gender_predict_parameters()
    prediction = gender_classifier.classify([params.img_face])
    print('prediction: {}'.format(prediction))
    np.testing.assert_equal(prediction, 'women')
    if maybe_save:
        ut.maybe_save(params.expected_values_path, prediction)
    # expected_prediction = ut.load(params.expected_values_path)
    # ut.assert_values(prediction, expected_prediction, rtol)


def test_gender_classify(gender_classifier):
    params = _gender_classify_parameters()
    gpredict = gender_classifier.classify([params.img_face])
    np.testing.assert_equal(gpredict, 'women')


class GenderClassificationTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        cls._gender_classifier = create_gender_classifier('caffe')

    def test_gender_predict(self):
        gender_classifier = self.__class__._gender_classifier
        test_gender_predict(gender_classifier, maybe_save=True)

    def test_gender_classify(self):
        gender_classifier = self.__class__._gender_classifier
        test_gender_classify(gender_classifier)


if __name__ == '__main__':
    main()
