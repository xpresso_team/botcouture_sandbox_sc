#!/bin/bash

set -e

export DEVICE_NUMBER=1
export CUDA_DEVICE_ORDER=PCI_BUS_ID

function unittest {
    printf "\nExecuting $1\n"
    cmd='CUDA_VISIBLE_DEVICES=$DEVICE_NUMBER python -m tests.'
    cmd+=$1
    eval $cmd
}

#unittest caffe_classify_fashion_test
#unittest caffe_cnn_feats_test
unittest caffe_face_gender_test
unittest caffe_human_pose_test
unittest caffe_salient_objects_test
unittest caffe_segment_fashion_test
unittest caffe_visual_search_test

#unittest tensorflow_classify_fashion_test
#unittest tensorflow_cnn_feats_test
#unittest tensorflow_face_gender_test
#unittest tensorflow_human_pose_test
#unittest tensorflow_salient_objects_test
#unittest tensorflow_segment_fashion_test
#unittest tensorflow_visual_search_test
