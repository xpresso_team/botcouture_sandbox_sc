curr_dir=`pwd`
if [ "$1" == "serving" ]
then
    echo "Serving argument received, setting tf_serve to true"
    tf_serve=1
    deploy="$2"
else
    echo "Serving argument not received, setting tf_serve to false"
    tf_serve=0
    deploy="$1"
fi
echo "Setting deploy to $deploy"
if [ $tf_serve ]
then
    export CUDA_DEVICE_ORDER=PCI_BUS_ID
    export CUDA_VISIBLE_DEVICES=0
    tensorflow_model_server --port=9000 --model_config_file=tensorflow_models/tfserv.cfg &
fi
if [ $deploy == "" ]
then
    echo "Testing complete"
elif [ $deploy == "production" ]
then
   export VISIONBACKENDSERVICE_CONFIG=${curr_dir}/config/vis.cfg
   python backendapi.py 
elif [ $deploy == "staging" ]
then
   export VISIONBACKENDSERVICE_CONFIG=${curr_dir}/config/visstaging.cfg
   python backendapi.py 
elif [ $deploy == "ashley" ]
then
   export VISIONBACKENDSERVICE_CONFIG=${curr_dir}/config/visashley.cfg
   python backendapi.py 
elif [ $deploy == "experimental" ]
then
   workon cv
   export VISIONBACKENDSERVICE_CONFIG=${curr_dir}/config/visexp.cfg
   python visionbackendservice/scripts/backendapi.py
fi
