curr_dir=`pwd`
if [ "$1" == "" ]
then
    echo "Testing complete"
elif [ "$1" == "production" ]
then
   export VISIONBACKENDSERVICE_CONFIG=${curr_dir}/config/vis.cfg
   nohup python backendapi.py &> backendservice_out.log &
elif [ $1 == "staging" ]
then
   export VISIONBACKENDSERVICE_CONFIG=${curr_dir}/config/visstaging.cfg
   nohup python backendapi.py &> backendservice_out.log &
elif [ "$1" == "ashley" ]
then
   export VISIONBACKENDSERVICE_CONFIG=${curr_dir}/config/visashley.cfg
   nohup python backendapi.py &> backendservice_out.log &
elif [ "$1" == "experimental" ]
then
   workon cv
   export VISIONBACKENDSERVICE_CONFIG=${curr_dir}/config/visexp.cfg
   nohup python backendapi.py &> backendservice_out.log &
fi
