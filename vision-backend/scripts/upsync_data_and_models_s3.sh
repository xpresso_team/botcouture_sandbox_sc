if [ ! -d "lib_dependencies" ]
then
    mkdir "lib_dependencies"
fi

if [ ! -d "data" ]
then
    mkdir "data"
fi


curr_dir=`pwd`

if [ "$1" == "--dryrun" ]
then 
   dryrun="--dryrun"
else
   dryrun=""
fi

echo "Syncing models and library dependencies"

aws s3 sync lib_dependencies/abz_fashion_image_classification s3://vision-data-and-models/abz_fashion_image_classification --region us-east-2  ${dryrun}
aws s3 sync lib_dependencies/fcn.berkeleyvision.org s3://vision-data-and-models/fcn.berkeleyvision.org --region us-east-2 ${dryrun}
aws s3 sync lib_dependencies/saliency s3://vision-data-and-models/saliency --region us-east-2 ${dryrun}
aws s3 sync lib_dependencies/cnn_age_gender_models_and_data.0.0.2 s3://vision-data-and-models/cnn_age_gender_models_and_data.0.0.2 --region us-east-2 ${dryrun}
aws s3 sync lib_dependencies/RheC_RTMPPose s3://vision-data-and-models/RheC_RTMPPose --region us-east-2 ${dryrun}
aws s3 sync data/feats_models  s3://vision-data-and-models/feats_models --region us-east-2 ${dryrun}
