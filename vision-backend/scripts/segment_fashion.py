from __future__ import print_function

from collections import namedtuple
import json
import math
import cv2
import matplotlib.patches as mpatches
import numpy as np
from skimage import img_as_ubyte

from utils import value_or_default
from util import getImage
from threading import Lock
import time
import sys


class FashionSegmentation(object):
    def __init__(self, model_type, model_dir, model_name, device_no,
                 serving_host=None, serving_port=None, serving_timeout=None):
        """
        initialize the fashion semantic segmentation model

        Arguments:
            model_type: string, Selects the type of the model,
                    either 'caffe' or 'tensorflow'.
                    Optional with default of 'caffe'.
            model_dir: string, The base directory of the semantic segmentation model.
                    Optional with default of 'lib_dependencies/fcn.berkeleyvision.org/'
                    for 'caffe' model_type and default of
                    'lib_dependencies/tensorflow/fcn.berkeleyvision.org/' for 'tensorflow' model_type.                             
            model_name: string, The name of the  semantic segmentation model weights file.
                    Optional with default of 'models/wlrfash3-fcn8s.caffemodel'
                    for 'caffe' model_type and default of
                    'model.ckpt' for 'tensorflow' model_type.
            device_no: int, GPU device no if >= 0 else cpu mode
        """
        model_dir = value_or_default(model_dir, 'visiondata-and-models/fcn.berkeleyvision.org/')

        # fcn_ROOT = 'lib_dependencies/fcn.berkeleyvision.org/'
        styleclassdata_dir = model_dir + 'data/wlrfash-fcn/'
        # with open(styleclassdata_dir + 'segstyle_stdcategories.json') as filedata:
        #    self.segstyle_stdcategories = json.load(filedata)
        with open(styleclassdata_dir + 'segcategories_pose.json') as filedata:
            self.segcategories_pose = json.load(filedata)
        with open(styleclassdata_dir + 'segcategories_men.json') as filedata:
            self.segcategories_men = json.load(filedata)
        with open(styleclassdata_dir + 'cat_labels.json') as fp:
            self.cat_labels = json.load(fp)

        model_type = value_or_default(model_type, 'caffe')
        model_dir = model_dir + 'wlrfash3-fcn8s/'
        self.segmentorlock = Lock()

        if model_type.lower() == 'caffe':
            model_name = value_or_default(model_name, 'models/wlrfash3-fcn8s.caffemodel')
            weights = model_dir + model_name
            prototxt = model_dir + 'deploy.prototxt'
            import caffe_models.segment_fashion as segment_fashion
            segment_fashion.init(device_no)
            from caffe_models.segment_fashion import FashionSegmentation
            self.model = FashionSegmentation(prototxt, weights, device_no)
        elif model_type.lower() == 'serving':
            host = value_or_default(serving_host, '0.0.0.0')
            port = value_or_default(serving_port, 9000)
            timeout = value_or_default(serving_timeout, 60)
            from tensorflow_models.segment_fashion import FashionSegmentationClient
            self.model = FashionSegmentationClient(host, port, timeout, device_no)
        else:
            model_dir = value_or_default(model_dir, 'visiondata-and-models/tensorflow/fcn.berkeleyvision.org/')
            model_name = value_or_default(model_name, 'tf_fashfcn8s.npy')  # 'model.ckpt')
            weights = model_dir + model_name
            import tensorflow_models.segment_fashion as  segment_fashion
            from tensorflow_models.segment_fashion import FashionSegmentation
            self.model = FashionSegmentation(weights, device_no)
        self.ready = True

    def _preprocess_image(self, im):
        im = getImage(im)
        new_height = 600
        new_width = int(new_height * im.shape[1] * 1.0 / im.shape[0])
        hfac = new_height * 1.0 / im.shape[0]
        wfac = new_width * 1.0 / im.shape[1]
        im = cv2.resize(im, (new_width, new_height))

        im_res = im
        im_res = np.array(im_res)
        if len(im_res.shape) == 3 and im_res.shape[2] == 4:
            im_res = im_res[:, :, :3]

        in_ = np.array(im_res, dtype=np.float32)
        in_ = in_[:, :, ::-1]
        in_ -= np.array((104.00698793, 116.66876762, 122.67891434))
        in_ = in_.transpose((2, 0, 1))
        # im = im[np.newaxis, :, :, :]
        return namedtuple('preprocessed_image', ['im', 'im_res', 'in_'])(
            im=im,
            im_res=im_res,
            in_=in_)

    def segment_fashion(self, im, gender, visualize=False):
        im_res, seg_prediction = self.semantic_segfcn(im, gender)
        cat_patches = []
        num_labels = max(4, len(np.unique(seg_prediction)) - 2)
        plt_rows = int(3 + math.ceil(1.0 * num_labels / 4))
        cplot = 0
        for i, label in enumerate(np.unique(seg_prediction)):
            temp = im_res[seg_prediction == label, :].mean(axis=0)
            im_res[seg_prediction == label, :] = temp
            cat_patches.append(mpatches.Patch(color=temp / 255, label=self.cat_labels[str(label)]))
            if label == 1 or label == 12:
                continue
            x, y, w, h = cv2.boundingRect(img_as_ubyte(seg_prediction == label))
            temp = np.array(im_res)
            temp[seg_prediction != label, :] = 0
            cplot += 1

        #        if visualize:
        #            a = plt.subplot2grid((plt_rows,4),(0,0),rowspan=3,colspan=2)
        #            plt.imshow(im)
        #            a.set_title('original')
        #            a = plt.subplot2grid((plt_rows,4),(0,2),rowspan=3,colspan=2)
        #            plt.imshow(im_res)
        #            plt.legend(handles=cat_patches, loc="upper left", bbox_to_anchor=(1,1))
        #            a.set_title('segmented')
        return seg_prediction, np.array(im_res)

    ''' segment fashion image for semantic segmentation API endpoint '''

    def semantic_segfcn(self, img, gender=None):
        """
        Input image - np.ndarray
        Output pose - semantically segmented and annotated image
        """
        im_orig = getImage(img)
        if not self.ready:
            return None

        st = time.clock()
        image = self._preprocess_image(im_orig)
        im = image.im
        im_res = image.im_res
        in_ = image.in_

        with self.segmentorlock:
            scores = self.model.infer(in_)
        if gender == "men":
            for i in range(1, scores.shape[0]):
                if self.segcategories_men[self.cat_labels[str(i)]] == "None":
                    scores[i, :, :] *= 0
        seg_prediction = scores.argmax(axis=0)
        print(seg_prediction.shape)

        print("FCN based semantic segmentation done in  - " + str(time.clock() - st) + " seconds.", file=sys.stderr)
        return im_res, seg_prediction

    # TODO - investigate handling this better
    def cat_labels(self, unique_seg_prediction):
        return self.cat_labels[str(unique_seg_prediction)]

    # TODO - investigate handling this better
    def segcategories_pose(self, cat_label):
        return self.segcategories_pose[cat_label]
