from __future__ import print_function
import dlib

import numpy as np

# import caffe
from matplotlib import pyplot as plt
from decaffenated.io import resize_image
from utils import value_or_default
from util import pad_rectbox
import cv2
import sys
from txutils import Transformer


class GenderClassification(object):
    def __init__(self, model_type, model_dir, model_name, device_no,
                 serving_host=None, serving_port=None, serving_timeout=None, params=None):
        """ Initialize the gender classification model. 

        Arguments:
            model_type: string, Selects the type of the model,
                    either 'caffe' or 'tensorflow'.
                    Optional with default of 'caffe'.
            model_dir: string, The base directory of the gender classification model.
                    Optional with default of 'lib_dependencies/cnn_age_gender_models_and_data.0.0.2/'
                    for 'caffe' model_type and default of
                    'lib_dependencies/tensorflow/cnn_age_gender_models_and_data.0.0.2/' for 'tensorflow' model_type.                             
            model_name: string, The name of the gender classification model weights file.
                    Optional with default of 'gender_net.caffemodel'
                    for 'caffe' model_type and default of
                    'model.ckpt' for 'tensorflow' model_type.
            device_no: int, GPU device no if >= 0 else cpu mode
        """
        model_dir_was_none = model_dir is None
        model_dir = value_or_default(model_dir, 'visiondata-and-models/dex_gendernet/')

        ''' read the mean image file '''
        # mean_filename = model_dir + 'mean.binaryproto'
        # proto_data = open(mean_filename, "rb").read()
        # a = caffe.io.caffe_pb2.BlobProto.FromString(proto_data)
        # mean = caffe.io.blobproto_to_array(a)[0]

        self.model_type = value_or_default(model_type, 'caffe')

        def_params = {'mean': model_dir + 'imagenet_mean.binaryproto',
                      'image_dim': 256, 'crop_dim': 224,
                      'oversample': False}
        txparams = value_or_default(params, def_params)
        self.tx = Transformer(params=txparams)
        if self.model_type.lower() == 'caffe':
            model_name = value_or_default(model_name, 'gendernet.caffemodel')
            weights = model_dir + model_name
            prototxt = model_dir + 'deploy.prototxt'
            from caffe_models import face_gender
            face_gender.init(device_no)
            from caffe_models.face_gender import GenderClassification
            self.model = GenderClassification(prototxt, weights, device_no)
        else:
            if model_dir_was_none:
                model_dir = 'visiondata-and-models/tensorflow/cnn_age_gender_models_and_data.0.0.2/'
            mean = np.load(model_dir + 'mean.npy')
            in_shape = [3, 227, 227]
            m_min, m_max = mean.min(), mean.max()
            normal_mean = (mean - m_min) / (m_max - m_min)
            mean = resize_image(normal_mean.transpose((1, 2, 0)), in_shape[1:]).transpose((2, 0, 1)) * (
                    m_max - m_min) + m_min
            if model_type.lower() == 'serving':
                host = value_or_default(serving_host, '0.0.0.0')
                port = value_or_default(serving_port, 9000)
                timeout = value_or_default(serving_timeout, 60)
                from tensorflow_models.face_gender import GenderClassificationClient
                self.model = GenderClassificationClient(host, port, timeout, mean, device_no)
            else:
                model_name = value_or_default(model_name, 'model.ckpt')
                weights = model_dir + model_name
                import tensorflow_models.face_gender as face_gender
                from tensorflow_models.face_gender import GenderClassification
                self.model = GenderClassification(weights, mean, device_no)

    def detect_faces(self, img, source, interactive=False):
        detector = dlib.get_frontal_face_detector()
        dets = detector(img, 1)
        height = img.shape[0]
        width = img.shape[1]
        print("Number of faces detected: {}".format(len(dets)), file=sys.stderr)
        faces = []
        for i, d in enumerate(dets):
            h_F = 0.1 * (d.bottom() - d.top())
            w_F = 0.1 * (d.right() - d.left())
            if (d.bottom() - d.top()) * (d.right() - d.left()) > 0:
                faces.append(
                    (d.left() - w_F, d.top() - h_F, d.right() - d.left() + 2 * w_F, d.bottom() - d.top() + 2 * h_F))
                if interactive:
                    cv2.rectangle(img, (d.left(), d.top()), (d.right(), d.bottom()), (255, 0, 0), 1, 1)
            print("Detection {}: Left: {} Top: {} Right: {} Bottom: {}".format(
                i, d.left(), d.top(), d.right(), d.bottom()), file=sys.stderr)
        img_face = []
        if len(faces) > 0:
            for (x, y, w, h) in faces[:1]:
                winh = h / 4
                winw = w / 4
                obj_rect = (x, y, w, h)
                fpbbox = pad_rectbox(obj_rect, img.shape, pad_winr=0.4)
                img_face = img[fpbbox[1]:fpbbox[1] + fpbbox[3], fpbbox[0]:fpbbox[0] + fpbbox[2], :]
                #                img_face = img[int(y):int(y + h), int(x):int(x + w)]
                source['face'] = {'x1': x, 'y1': y, 'x2': x + w, 'y2': y + h, 'pose_type': 'face'}
                print("using face detected by dlib " + str(img_face.shape), file=sys.stderr)
                if source and "noPose" in source:
                    source = {}
                    x1 = x - 2 * winw
                    y1 = y + h - winw
                    x2 = x + w + 2 * winw
                    y2 = y + 3 * h
                    if x1 < 0:
                        x1 = 0
                    if x2 > width:
                        x2 = width - 1
                    if y2 > height:
                        y2 = height - 1

                    if y2 - y1 > h:
                        source['upper'] = {'x1': x1, 'y1': y1, 'x2': x2, 'y2': y2,
                                           'pose_type': 'upper',
                                           'crop_rect': str(x1) + "," + str(y1) + "," + str(x2) + "," + str(y2)}
                    else:
                        source['noPose'] = {'x1': 0, 'x2': width, 'y1': 0, 'y2': height, 'pose_type': 'noPose'}
            if interactive:
                _ = plt.figure()
                plt.imshow(img)
        elif 'face' in source:
            if "person" in source:
                xoffset = source["person"]["x1"]
                yoffset = source["person"]["y1"]
            else:
                xoffset = 0
                yoffset = 0
            face_rect = (source["face"]["x1"] - xoffset, source["face"]["y1"] - yoffset,
                         source["face"]["x2"] - source["face"]["x1"],
                         source["face"]["y2"] - source["face"]["y1"])
            fpbbox = pad_rectbox(face_rect, img.shape, pad_winr=0.4)
            img_face = img[fpbbox[1]:fpbbox[1] + fpbbox[3], fpbbox[0]:fpbbox[0] + fpbbox[2], :]
            print("using face detected during human pose computation " + str(img_face.shape), file=sys.stderr)

        return source, img_face

    def classify(self, img_face):
        gender_list = ['women', 'men']
        if self.model_type.lower() == "caffe":
            img_face = img_face[0]
            prediction = self.model.predict(self.tx.preprocess(img_face))
            print("Predicted Gender scores for women,men: " + str(prediction), file=sys.stderr)
        else:
            prediction = self.model.predict(img_face)
            prediction = prediction[0]
        gpredict = gender_list[prediction.argmax()]
        gscore = np.amax(prediction)
        return gpredict, gscore
