from __future__ import print_function

# import caffe
import cv2
import numpy as np
import time
from util import pad_rectbox

from decaffenated.io import load_image
from utils import value_or_default
from threading import Lock


class SalientObjDetection(object):
    def __init__(self, model_type, model_dir, model_name, device_no,
                 serving_host=None, serving_port=None, serving_timeout=None):
        """
        Initialize the salient objects segmentation model with the selected model type.

        Arguments:
            model_type: string, Selects the type of the model,
                    either 'caffe' or 'tensorflow'.
                    Optional with default of 'caffe'.
            model_dir: string, The base directory of the salient objects model.
                    Optional with default of 'lib_dependencies/saliency/model/'
                    for 'caffe' model_type and default of
                    'lib_dependencies/tensorflow/saliency/' for 'tensorflow' model_type.                             
            model_name: string, The name of the  salient objects model weights file.
                    Optional with default of 'train_iter_40000.caffemodel'
                    for 'caffe' model_type and default of
                    'model.ckpt' for 'tensorflow' model_type.
            device_no: int, GPU device no if >= 0 else cpu mode
        """
        model_type = value_or_default(model_type, 'caffe')

        mean = np.array([104.00698793, 116.66876762, 122.67891434])
        self.sobjectslock = Lock()
        self.maxbatchsize = 10
        if model_type.lower() == 'caffe':
            # self.device_initializer = device_initializer(device_no)
            # self.initialize_device()
            model_dir = value_or_default(model_dir, 'lib_dependencies/saliency/model/')
            model_name = value_or_default(model_name, 'train_iter_40000.caffemodel')
            weights = model_dir + model_name
            prototxt = model_dir + 'deploy.prototxt'
            import caffe_models.salient_objects as salient_objects
            salient_objects.init(device_no)
            from caffe_models.salient_objects import SalientObjDetection
            self.model = SalientObjDetection(prototxt, weights, mean, device_no)
        elif model_type.lower() == 'serving':
            host = value_or_default(serving_host, '0.0.0.0')
            port = value_or_default(serving_port, 9000)
            timeout = value_or_default(serving_timeout, 60)
            from tensorflow_models.salient_objects import SalientObjDetectionClient
            self.model = SalientObjDetectionClient(host, port, timeout, mean, device_no)
        else:
            model_dir = value_or_default(model_dir, 'lib_dependencies/tensorflow/saliency/model/')
            model_name = value_or_default(model_name, 'model.ckpt')
            weights = model_dir + model_name
            from tensorflow_models.salient_objects import SalientObjDetection
            self.model = SalientObjDetection(weights, mean, device_no)

    def process_saliency(self, im_orig, map_final, single_mode=True):
        map_final -= map_final.min()
        map_final /= map_final.max()
        map_final = np.ceil(map_final * 255)
        kernel = np.ones((5, 5), np.uint8)
        map_final_er = cv2.erode(map_final, kernel, iterations=3)
        ret, thresh = cv2.threshold(map_final_er, 25, 255, 0)
        thresh = np.asarray(thresh, dtype=np.uint8)
        print(thresh.shape, thresh.dtype)
        #    contours, hierarchy = cv2.findContours(thresh, mode=cv2.RETR_EXTERNAL, method=cv2.CHAIN_APPROX_SIMPLE)
        im2, contours, hierarchy = cv2.findContours(thresh, 1, 2)
        #    print(contours)

        obj_rects = []
        area_max = 0
        obj_areas = []
        for c in contours:
            x, y, w, h = cv2.boundingRect(c)
            pad_w = w / 5
            pad_h = h / 5
            x = (x - pad_w) if x - pad_w > 0 else 0
            y = (y - pad_h) if y - pad_h > 0 else 0
            w = (w + 2 * pad_w) if (x + w + 2 * pad_w < thresh.shape[1]) else (thresh.shape[1] - 1)
            h = (h + 2 * pad_h) if (y + h + 2 * pad_h < thresh.shape[0]) else (thresh.shape[0] - 1)
            temp = np.sum(map_final[y:y + h, x:x + w])
            if temp >= area_max:
                area_max = temp
            obj_rects.append((x, y, w, h))
            obj_areas.append(temp)

        obj_rects_final = []
        obj_areas_final = []
        for ia, a in enumerate(obj_areas):
            if a > 0.20 * area_max:
                obj_rects_final.append(pad_rectbox(obj_rects[ia], im_orig.shape, pad_winr=0.0))
                obj_areas_final.append(a)

        #        if len(obj_rects) == 0:
        #            obj_rects.append((0,0,im_orig.shape[1]-1,im_orig.shape[0]-1))
        #            return map_final, im_orig, obj_rects

        obj_rects_final.append((0, 0, im_orig.shape[1] - 1, im_orig.shape[0] - 1))
        if single_mode and len(obj_rects_final) > 1:
            im_orig, obj_rects_final = self._find_one_salient(im_orig, obj_rects_final)

        return map_final, obj_rects_final

    def get_salient_objects(self, img_list, single_mode=True):
        # self.initialize_device()
        if not isinstance(img_list, list):
            print("Wrong argument - Expecting list of images")
            return None, None, None
        im_r = []
        Ws = []
        Hs = []
        im_orig = []
        for i, img in enumerate(img_list):
            if isinstance(img, np.ndarray):
                im_orig.append(img)
            elif isinstance(img, str):
                # im_orig = caffe.io.load_image(img)
                im_orig.append(load_image(img))
            (H, W, C) = im_orig[i].shape  # C=3
            Ws.append(W)
            Hs.append(H)
            newW = 300
            newH = (newW * im_orig[i].shape[0]) / im_orig[i].shape[1]
            scaleX = W * 1.0 / newW
            scaleY = H * 1.0 / newH
            im_r.append(cv2.resize(im_orig[i], (newW, newH)))
        # pre-process the image

        # process the image
        # imgData = self.transformer.preprocess('data', im_orig)
        segmaps = []
        sobjs = []
        num_ims = len(img_list)
        num_batches = int(num_ims / self.maxbatchsize) + 1
        for nb in range(num_batches):
            currbatchsize = min(self.maxbatchsize, num_ims - nb * self.maxbatchsize)
            currstart = nb * self.maxbatchsize
            im_r_currbatch = im_r[currstart:currstart + currbatchsize]
            im_r_currbatch = np.array(im_r_currbatch)
            with self.sobjectslock:
                outmap = self.model.predict(im_r_currbatch)
            for si in range(outmap.shape[0]):
                map_final = cv2.resize(outmap[si, ...], (Ws[currstart + si], Hs[currstart + si]))
                map_final, obj_rects_final = self.process_saliency(im_orig[currstart + si], map_final, single_mode)
                segmaps.append(map_final)
                sobjs.append(obj_rects_final)
        return segmaps, im_orig, sobjs

    def _find_one_salient(self, im_orig, obj_rects_final):
        """ 
        return the object which is central to image
        """
        center_x = int(im_orig.shape[0] / 2)
        center_y = int(im_orig.shape[1] / 2)
        min_dist = 100000000
        sobj_rect = ()
        obj_ind = 0;
        iter_i = 0;
        for x, y, w, h in obj_rects_final:
            p_x = int(x + w / 2)
            p_y = int(y + h / 2)
            p_dist = ((center_x - p_x) ^ 2 + (center_y - p_y) ^ 2)
            if p_dist < min_dist:
                sobj_rect = (x, y, w, h)
                min_dist = p_dist
                obj_ind = iter_i
            iter_i = iter_i + 1

        obj_rects_final = []
        obj_rects_final.append(sobj_rect)
        return im_orig, obj_rects_final
