import io
import urllib

import cv2
import numpy as np
from cStringIO import StringIO
import PIL.Image


def showBGRimage(a, fmt='jpeg'):
    a = np.uint8(np.clip(a, 0, 255))
    a[:, :, [0, 2]] = a[:, :, [2, 0]]  # for B,G,R order
    f = StringIO()
    PIL.Image.fromarray(a).save(f, fmt)
    display(Image(data=f.getvalue()))


def showmap(a, fmt='png'):
    a = np.uint8(np.clip(a, 0, 255))
    f = StringIO()
    PIL.Image.fromarray(a).save(f, fmt)
    display(Image(data=f.getvalue()))


# def checkparam(param):
#    octave = param['octave']
#    starting_range = param['starting_range']
#    ending_range = param['ending_range']
#    assert starting_range <= ending_range, 'starting ratio should <= ending ratio'
#    assert octave >= 1, 'octave should >= 1'
#    return starting_range, ending_range, octave

def getJetColor(v, vmin, vmax):
    c = np.zeros((3))
    if (v < vmin):
        v = vmin
    if (v > vmax):
        v = vmax
    dv = vmax - vmin
    if (v < (vmin + 0.125 * dv)):
        c[0] = 256 * (0.5 + (v * 4))  # B: 0.5 ~ 1
    elif (v < (vmin + 0.375 * dv)):
        c[0] = 255
        c[1] = 256 * (v - 0.125) * 4  # G: 0 ~ 1
    elif (v < (vmin + 0.625 * dv)):
        c[0] = 256 * (-4 * v + 2.5)  # B: 1 ~ 0
        c[1] = 255
        c[2] = 256 * (4 * (v - 0.375))  # R: 0 ~ 1
    elif (v < (vmin + 0.875 * dv)):
        c[1] = 256 * (-4 * v + 3.5)  # G: 1 ~ 0
        c[2] = 255
    else:
        c[2] = 256 * (-4 * v + 4.5)  # R: 1 ~ 0.5
    return c


def colorize(gray_img):
    out = np.zeros(gray_img.shape + (3,))
    for y in range(out.shape[0]):
        for x in range(out.shape[1]):
            out[y, x, :] = getJetColor(gray_img[y, x], 0, 1)
    return out


def padRightDownCorner(img, stride, padValue):
    h = img.shape[0]
    w = img.shape[1]

    pad = 4 * [None]
    pad[0] = 0  # up
    pad[1] = 0  # left
    pad[2] = 0 if (h % stride == 0) else stride - (h % stride)  # down
    pad[3] = 0 if (w % stride == 0) else stride - (w % stride)  # right

    img_padded = img
    pad_up = np.tile(img_padded[0:1, :, :] * 0 + padValue, (pad[0], 1, 1))
    img_padded = np.concatenate((pad_up, img_padded), axis=0)
    pad_left = np.tile(img_padded[:, 0:1, :] * 0 + padValue, (1, pad[1], 1))
    img_padded = np.concatenate((pad_left, img_padded), axis=1)
    pad_down = np.tile(img_padded[-2:-1, :, :] * 0 + padValue, (pad[2], 1, 1))
    img_padded = np.concatenate((img_padded, pad_down), axis=0)
    pad_right = np.tile(img_padded[:, -2:-1, :] * 0 + padValue, (1, pad[3], 1))
    img_padded = np.concatenate((img_padded, pad_right), axis=1)

    return img_padded, pad


def pad_bbox(patch, pad_win, shape_bound):
    patch["x1"] = patch["x1"] - pad_win if patch["x1"] - pad_win >= 0 else 0
    patch["y1"] = patch["y1"] - pad_win if patch["y1"] - pad_win >= 0 else 0
    patch["x2"] = patch["x2"] + pad_win if patch["x2"] + pad_win < shape_bound[1] else shape_bound[1] - 1
    patch["y2"] = patch["y2"] + pad_win if patch["y2"] + pad_win < shape_bound[0] else shape_bound[0] - 1
    return patch


# imUrl = '/home/ubuntu/research/VOCdevkit/VOC2011/JPEGImages/2008_004342.jpg'
'''segment near plain background for images where only one item has been predicted'''


def segplainbg(im_orig):
    """
    segment plain background
    """
    if len(im_orig.shape) == 3 and im_orig.shape[2] == 4:
        im_orig = im_orig[:, :, :3]
    print("Starting plain bg segmentation")
    im = cv2.resize(im_orig, (120, 120 * im_orig.shape[0] / im_orig.shape[1]))
    pad = 30
    im_padded = cv2.copyMakeBorder(im, pad, pad, pad, pad, cv2.BORDER_REPLICATE)
    bgdModel = np.zeros((1, 65), np.float64)
    fgdModel = np.zeros((1, 65), np.float64)
    mask = np.zeros(im_padded.shape[:2], np.uint8)
    rect = (pad, pad, im_padded.shape[1] - pad, im_padded.shape[0] - pad)
    cv2.rectangle(mask, rect[:2], rect[2:], int(cv2.GC_PR_FGD), thickness=-1)  # -ve thickness draws a filled rectangle
    cv2.grabCut(im_padded, mask, rect, bgdModel, fgdModel, 5, cv2.GC_INIT_WITH_MASK)
    mask2 = np.where((mask == 2) | (mask == 0), 0, 1).astype('uint8')
    mask2 = mask2[pad:pad + im.shape[0], pad:pad + im.shape[1]]

    im_mask_orig = cv2.resize(mask2, (im_orig.shape[1], im_orig.shape[0]))
    x, y, w, h = cv2.boundingRect(im_mask_orig)
    # mask_orig = im_mask_orig[:,:,np.newaxis]

    # im_res = im_orig[y:y+h, x:x+w, :]
    im_mask = im_mask_orig[y:y + h, x:x + w]
    bbox = [x, y, x + w, y + h]
    if w >= 10 and h >= 10:
        return im_mask, bbox
    else:
        return None, None


def addTextOnImage(cvim_orig, source_patch, tag):
    """
    Add tag corresponding to the source patch in the image
    """
    center_x = int((source_patch["x1"] + source_patch["x2"]) / 2)
    center_y = int((source_patch["y1"] + source_patch["y2"]) / 2)
    radius1 = int(cvim_orig.shape[1] / 70)
    radius2 = int(cvim_orig.shape[0] / 50)
    cv2.circle(cvim_orig, (center_x, center_y), radius1, (0, 255, 255), -1)
    cv2.circle(cvim_orig, (center_x, center_y), radius2, (0, 200, 200), 1)
    font_scale = float(cvim_orig.shape[0] * 1.0 / 300)
    ts, baseline = cv2.getTextSize(tag, cv2.FONT_HERSHEY_PLAIN, font_scale, 2)
    cv2.rectangle(cvim_orig, (center_x + 10, center_y + baseline), (center_x + 10 + ts[0], center_y - ts[1]), (0, 0, 0),
                  -1)
    cv2.putText(cvim_orig, tag, (center_x + 10, center_y), cv2.FONT_HERSHEY_PLAIN, font_scale, (0, 225, 225), 2,
                cv2.LINE_AA)


''' get image in np.ndarray type'''


def getImage(im):
    """
    input image -np.ndarray, image url, or path
    """
    if isinstance(im, str):
        if "http" in im:
            file = io.BytesIO(urllib.urlopen(im).read())
        else:
            file = im
        im_origpil = PIL.Image.open(file)
        im_orig = np.array(im_origpil)
        if len(im_orig.shape) == 3 and im_orig.shape[2] == 4:  # convert png to jpg
            im_origpil.load()
            im_orig = PIL.Image.new("RGB", im_origpil.size, (255, 255, 255))
            im_orig.paste(im_origpil, mask=im_origpil.split()[3])  # 3 is the alpha channel
            im_orig = np.array(im_orig)
    else:
        im_orig = np.array(im)

    if len(im_orig.shape) == 3 and im_orig.shape[2] == 4:
        im_orig = im_orig[:, :, :3]
    return im_orig


def pad_rectbox(objrect, shape_bound, pad_winr):
    x, y, w, h = objrect
    padwinx = w * pad_winr
    padwiny = h * pad_winr
    x = x - padwinx if x - padwinx >= 0 else 0
    y = y - padwiny if y - padwiny >= 0 else 0
    w = w + 2 * padwinx if x + w + 2 * padwinx < shape_bound[1] else shape_bound[1] - 1
    h = h + 2 * padwiny if y + h + 2 * padwiny < shape_bound[0] else shape_bound[0] - 1
    return (int(x), int(y), int(w), int(h))

# if __name__ == "__main__":
#    config_reader()
