import sys 

from utils import value_or_default


class CNNFeats(object):
    def __init__(self, model_type, model_dir, model_name, device_no,
                 serving_host=None, serving_port=None, serving_timeout=None):
        """ Initialize CNN features model with the selected model type.

        Arguments:
            model_type: string, Selects the type of the model,
                    either 'caffe' or 'tensorflow'.
                    Optional with default of 'caffe'.
            model_dir: string, The base directory of the CNN feats model.
                    Optional with default of 'data/feats_models/'
                    for 'caffe' model_type and default of
                    'data/tensorflow/feats_models/' for 'tensorflow' model_type.                             
            model_name: string, The name of the CNN feats model weights file.
                    Optional with default of 'df_attr-cats_0.3-1.0.caffemodel'
                    for 'caffe' model_type and default of
                    'model.ckpt' for 'tensorflow' model_type.
            device_no: int, GPU device no if >= 0 else cpu mode
        """
        model_type = value_or_default(model_type, 'caffe')

        mean = [103.939, 116.779, 123.68]
        if model_type.lower() == 'caffe':
            model_dir = value_or_default(model_dir, 'visiondata-and-models/feats_models/')
            model_name = value_or_default(model_name, 'df_attr-cats_0.3-1.0.caffemodel')
            weights = model_dir + model_name
            prototxt = model_dir + 'deploy.prototxt'  # 'vgg16/VGG_ILSVRC_16_layers_deploy.prototxt' #'deploy.prototxt'
            import caffe_models.cnn_feats as cnn_feats
            cnn_feats.init(device_no)
            from caffe_models.cnn_feats import CNNFeats
            self.model = CNNFeats(prototxt, weights, mean, device_no)
        elif model_type.lower() == 'serving':
            host = value_or_default(serving_host, '0.0.0.0')
            port = value_or_default(serving_port, 9000)
            timeout = value_or_default(serving_timeout, 60)
            from tensorflow_models.cnn_feats import CNNFeatsClient
            self.model = CNNFeatsClient(host, port, timeout, mean, device_no)
        else:
            model_dir = value_or_default(model_dir, 'visiondata-and-models/tensorflow/feats_models/')
            model_name = value_or_default(model_name, 'model.ckpt')
            weights = model_dir + model_name
            from tensorflow_models.cnn_feats import CNNFeats
            self.model = CNNFeats(weights, mean, device_no)

    def extract_features_single(self, im_url, im_bbox=None, layers=['fc7']):
        """
        Extract features from cnn layers for a single image
        """
        fwd_layer = 'fc7'
        if len([x for x in layers if 'probs' in x]) > 0:
            fwd_layer = None
        elif 'fc7' in layers:
            fwd_layer = 'fc7'
        elif 'fc6' in layers:
            fwd_layer = 'fc6'
        #        elif len(layers) == 1 and 'conv5' in layers:
        #            fwd_layer = 'conv5'
        else:
            print("Currently not supporting feature representation from layers - " + ",".join(layers))
        return self.model.extract_features_single(im_url, im_bbox, layers, fwd_layer)
