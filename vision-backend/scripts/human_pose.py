from __future__ import print_function

import math
import sys

import cv2 as cv
import matplotlib
import matplotlib.pylab as plt
import numpy as np
from scipy.ndimage.filters import gaussian_filter

import util
from config_reader import pose_config_reader
from utils import value_or_default
from threading import Lock
from util import getImage


class HumanPoseDetection(object):
    def __init__(self, model_type, model_dir, model_name, device_no, params,
                 serving_host=None, serving_port=None, serving_timeout=None):
        """
        Initialize the human pose detection model.
        Arguments:
            model_type: string, Selects the type of the model,
                    either 'caffe' or 'tensorflow'.
                    Optional with default of 'caffe'.
            model_dir: string, The base directory of the human pose detection model.
                    Optional with default of 'lib_dependencies/RheC_RTMPPose/models/_trained_COCO/'
                    for 'caffe' model_type and default of
                    'lib_dependencies/tensorflow/RheC_RTMPPose/models/_trained_COCO/' for 'tensorflow' model_type.
            model_name: string, The name of the human pose detection model weights file.
                    Optional with default of 'models/fashcategories.caffemodel'
                    for 'caffe' model_type and default of
                    'model.ckpt' for 'tensorflow' model_type.
            device_no: int, GPU device no if >= 0 else cpu mode
        """
        self.ready = True
        self.poseextractorlock = Lock()
        self.maxbatchsize = 10
        if "data_dir" in params:
            self.data_dir = params["data_dir"]
        else:
            self.data_dir = ""

        if params["model_type"] == "ZheC_RTMPPose":
            self.model_params = pose_config_reader(self.data_dir)
            self._init_model(model_type, model_dir, model_name, device_no, self.model_params, serving_host,
                             serving_port, serving_timeout)
        else:
            print("Undefined model type. Uninitialized.", file=sys.stderr)
            self.ready = False

    def _init_model(self, model_type, model_dir, model_name, device_no, model_params, serving_host, serving_port,
                    serving_timeout):
        model_type = value_or_default(model_type, 'caffe')

        if model_type.lower() == 'caffe':
            # caffemodel = 'lib_dependencies/RheC_RTMPPose/models/_trained_COCO/pose_iter_440000.caffemodel'
            # deployFile = 'lib_dependencies/RheC_RTMPPose/models/_trained_COCO/pose_deploy.prototxt'
            model_name = value_or_default(model_name, 'models/_trained_COCO/pose_iter_440000.caffemodel')
            # weights = model_dir + model_name
            weights = self.data_dir + model_params["model"]["caffemodel"]
            # prototxt = model_dir + 'deploy.prototxt'
            prototxt = self.data_dir + model_params["model"]["deployFile"]
            import caffe_models.human_pose as human_pose
            human_pose.init(device_no)
            from caffe_models.human_pose import HumanPoseDetection
            self.model = HumanPoseDetection(prototxt, weights, device_no, model_params)
        elif model_type.lower() == 'serving':
            host = value_or_default(serving_host, '0.0.0.0')
            port = value_or_default(serving_port, 9000)
            timeout = value_or_default(serving_timeout, 60)
            from tensorflow_models.human_pose import HumanPoseDetectionClient
            self.model = HumanPoseDetectionClient(host, port, timeout, device_no)
        else:
            model_dir = value_or_default(model_dir, 'lib_dependencies/tensorflow/RheC_RTMPPose/models/_trained_COCO/')
            model_name = value_or_default(model_name, 'model.ckpt')
            weights = model_dir + model_name
            import tensorflow_models.human_pose as human_pose
            from tensorflow_models.human_pose import HumanPoseDetection
            self.model = HumanPoseDetection(weights, device_no)

    ''' get human pose in image '''

    def get_pose(img):
        """
        Input image - np.ndarray
        Output pose - joint locations (x,y), body bbox as {"x1":<x1>,"y1":<y1>,"x2":<x2>,"y2":<y2>}
        """
        im_orig = getImage(img)
        st = time.clock()
        persons = pose_extractor.detect_pose(im_orig, visualize=False, visualize_all=False)
        print("CNN based human pose detection done in  - " + str(time.clock() - st) + " seconds.", file=sys.stderr)
        return persons

    def detect_pose(self, orig_imlist, visualize=False, visualize_all=False):
        if not self.ready:
            return None
        self.visualize = visualize
        self.visualize_all = visualize_all
        new_height = 200
        oriImg = []
        num_ims = len(orig_imlist)
        scale_x = []
        scale_y = []
        new_width = []
        persons = []
        heatmap_avg = []
        paf_avg = []
        for imi, orig_im in enumerate(orig_imlist):
            new_width.append(new_height * orig_im.shape[1] / orig_im.shape[0])
            tscale_y = (orig_im.shape[0] * 1.0) / new_height
            tscale_x = (orig_im.shape[1] * 1.0) / new_width[imi]
            print(tscale_x, tscale_y)
            scale_x.append(tscale_x)
            scale_y.append(tscale_y)

            oriImgtemp = cv.resize(orig_im, (new_width[imi], new_height))
            print(oriImgtemp.shape)
            oriImg.append(oriImgtemp)
            heatmap_avg.append(np.zeros((oriImgtemp.shape[0], oriImgtemp.shape[1], 19)))
            paf_avg.append(np.zeros((oriImgtemp.shape[0], oriImgtemp.shape[1], 38)))

        multiplier = [x * self.model_params['model']['boxsize'] / new_height for x in
                      self.model_params['scale_search']]

        if self.visualize_all:
            # first figure shows padded images
            f, axarr = plt.subplots(1, len(multiplier))
            f.set_size_inches((20, 5))
            # second figure shows heatmaps
            f2, axarr2 = plt.subplots(1, len(multiplier))
            f2.set_size_inches((20, 5))
            # third figure shows PAFs
            f3, axarr3 = plt.subplots(2, len(multiplier))
            f3.set_size_inches((20, 10))

        for m in range(len(multiplier)):
            scale = multiplier[m]
            imageToTest_padded = []
            for oriImgtemp in oriImg:
                imageToTest = cv.resize(oriImgtemp, (0, 0), fx=scale, fy=scale, interpolation=cv.INTER_CUBIC)
                imageToTest_paddedtemp, pad = util.padRightDownCorner(imageToTest, self.model_params['model']['stride'],
                                                                      self.model_params['model']['padValue'])
                # print (imageToTest_padded.shape, file=sys.stderr)
                imageToTest_padded.append(imageToTest_paddedtemp)

            with self.poseextractorlock:
                inferred = self.model.infer(imageToTest_padded, m)
            # print('inferred: {}'.format(inferred))

            for imi in range(num_ims):
                heatmap = cv.resize(inferred.heatmap[imi], (0, 0), fx=self.model_params['model']['stride'],
                                    fy=self.model_params['model']['stride'], interpolation=cv.INTER_CUBIC)
                heatmap = heatmap[:imageToTest_padded[imi].shape[0] - pad[2],
                          :imageToTest_padded[imi].shape[1] - pad[3], :]
                heatmap = cv.resize(heatmap, (oriImg[imi].shape[1], oriImg[imi].shape[0]), interpolation=cv.INTER_CUBIC)

                paf = cv.resize(inferred.paf[imi], (0, 0), fx=self.model_params['model']['stride'],
                                fy=self.model_params['model']['stride'], interpolation=cv.INTER_CUBIC)
                paf = paf[:imageToTest_padded[imi].shape[0] - pad[2], :imageToTest_padded[imi].shape[1] - pad[3], :]
                paf = cv.resize(paf, (oriImg[imi].shape[1], oriImg[imi].shape[0]), interpolation=cv.INTER_CUBIC)

                heatmap_avg[imi] = heatmap_avg[imi] + heatmap / len(multiplier)
                paf_avg[imi] = paf_avg[imi] + paf / len(multiplier)
                if self.visualize_all:
                    axarr[m].imshow(imageToTest_padded[imi][:, :, [2, 1, 0]])
                    axarr[m].set_title('Input image: scale %d' % m)
                    # visualization
                    axarr2[m].imshow(oriImg[:, :, [2, 1, 0]])
                    ax2 = axarr2[m].imshow(heatmap[:, :, 3], alpha=.5)  # right wrist
                    axarr2[m].set_title('Heatmaps (Rwri): scale %d' % m)

                    axarr3.flat[m].imshow(oriImg[imi][:, :, [2, 1, 0]])
                    ax3x = axarr3.flat[m].imshow(paf[:, :, 16], alpha=.5)  # right elbow
                    axarr3.flat[m].set_title('PAFs (x comp. of Rwri to Relb): scale %d' % m)
                    axarr3.flat[len(multiplier) + m].imshow(oriImg[imi][:, :, [2, 1, 0]])
                    ax3y = axarr3.flat[len(multiplier) + m].imshow(paf[:, :, 17], alpha=.5)  # right wrist
                    axarr3.flat[len(multiplier) + m].set_title('PAFs (y comp. of Relb to Rwri): scale %d' % m)
                persons.append(
                    self.process_posemaps(paf_avg[imi], heatmap_avg[imi], oriImg[imi], scale_x[imi], scale_y[imi],
                                          orig_imlist[imi]))
        return persons

    def process_posemaps(self, paf_avg, heatmap_avg, oriImg, scale_x, scale_y, orig_im):

        U = paf_avg[:, :, 16] * -1
        V = paf_avg[:, :, 17]
        X, Y = np.meshgrid(np.arange(U.shape[1]), np.arange(U.shape[0]))
        M = np.zeros(U.shape, dtype='bool')
        M[U ** 2 + V ** 2 < 0.5 * 0.5] = True
        U = np.ma.masked_array(U, mask=M)
        V = np.ma.masked_array(V, mask=M)

        if self.visualize_all:
            f2.subplots_adjust(right=0.93)
            cbar_ax = f2.add_axes([0.95, 0.15, 0.01, 0.7])
            _ = f2.colorbar(ax2, cax=cbar_ax)

            f3.subplots_adjust(right=0.93)
            cbar_axx = f3.add_axes([0.95, 0.57, 0.01, 0.3])
            _ = f3.colorbar(ax3x, cax=cbar_axx)
            cbar_axy = f3.add_axes([0.95, 0.15, 0.01, 0.3])
            _ = f3.colorbar(ax3y, cax=cbar_axy)

            plt.imshow(oriImg[:, :, [2, 1, 0]])
            plt.imshow(heatmap_avg[:, :, 2], alpha=.5)
            fig = matplotlib.pyplot.gcf()
            cax = matplotlib.pyplot.gca()
            fig.set_size_inches(20, 20)
            fig.subplots_adjust(right=0.93)
            cbar_ax = fig.add_axes([0.95, 0.15, 0.01, 0.7])
            _ = fig.colorbar(ax2, cax=cbar_ax)
            # 1
            plt.figure()
            plt.imshow(oriImg[:, :, [2, 1, 0]], alpha=.5)
            s = 5
            Q = plt.quiver(X[::s, ::s], Y[::s, ::s], U[::s, ::s], V[::s, ::s],
                           scale=50, headaxislength=4, alpha=.5, width=0.001, color='r')

            fig = matplotlib.pyplot.gcf()
            fig.set_size_inches(20, 20)

        # print (heatmap_avg.shape)
        all_peaks = []
        peak_counter = 0

        for part in range(19 - 1):
            x_list = []
            y_list = []
            map_ori = heatmap_avg[:, :, part]
            map = gaussian_filter(map_ori, sigma=3)

            map_left = np.zeros(map.shape)
            map_left[1:, :] = map[:-1, :]
            map_right = np.zeros(map.shape)
            map_right[:-1, :] = map[1:, :]
            map_up = np.zeros(map.shape)
            map_up[:, 1:] = map[:, :-1]
            map_down = np.zeros(map.shape)
            map_down[:, :-1] = map[:, 1:]

            peaks_binary = np.logical_and.reduce(
                (map >= map_left, map >= map_right, map >= map_up, map >= map_down, map > self.model_params['thre1']))
            peaks = zip(np.nonzero(peaks_binary)[1], np.nonzero(peaks_binary)[0])  # note reverse
            peaks_with_score = [x + (map_ori[x[1], x[0]],) for x in peaks]
            id = range(peak_counter, peak_counter + len(peaks))
            peaks_with_score_and_id = [peaks_with_score[i] + (id[i],) for i in range(len(id))]

            all_peaks.append(peaks_with_score_and_id)
            peak_counter += len(peaks)

        # find connection in the specified sequence, center 29 is in the position 15
        limbSeq = [[2, 3], [2, 6], [3, 4], [4, 5], [6, 7], [7, 8], [2, 9], [9, 10], \
                   [10, 11], [2, 12], [12, 13], [13, 14], [2, 1], [1, 15], [15, 17], \
                   [1, 16], [16, 18], [3, 17], [6, 18]]
        # the middle joints heatmap correpondence
        mapIdx = [[31, 32], [39, 40], [33, 34], [35, 36], [41, 42], [43, 44], [19, 20], [21, 22], \
                  [23, 24], [25, 26], [27, 28], [29, 30], [47, 48], [49, 50], [53, 54], [51, 52], \
                  [55, 56], [37, 38], [45, 46]]

        connection_all = []
        special_k = []
        mid_num = 10

        for k in range(len(mapIdx)):
            score_mid = paf_avg[:, :, [x - 19 for x in mapIdx[k]]]
            candA = all_peaks[limbSeq[k][0] - 1]
            candB = all_peaks[limbSeq[k][1] - 1]
            nA = len(candA)
            nB = len(candB)
            indexA, indexB = limbSeq[k]
            if (nA != 0 and nB != 0):
                connection_candidate = []
                for i in range(nA):
                    for j in range(nB):
                        vec = np.subtract(candB[j][:2], candA[i][:2])
                        norm = math.sqrt(vec[0] * vec[0] + vec[1] * vec[1]) + 0.1
                        vec = np.divide(vec, norm)

                        startend = zip(np.linspace(candA[i][0], candB[j][0], num=mid_num), \
                                       np.linspace(candA[i][1], candB[j][1], num=mid_num))

                        vec_x = np.array([score_mid[int(round(startend[I][1])), int(round(startend[I][0])), 0] \
                                          for I in range(len(startend))])
                        vec_y = np.array([score_mid[int(round(startend[I][1])), int(round(startend[I][0])), 1] \
                                          for I in range(len(startend))])

                        score_midpts = np.multiply(vec_x, vec[0]) + np.multiply(vec_y, vec[1])
                        score_with_dist_prior = sum(score_midpts) / len(score_midpts) + min(
                            0.5 * oriImg.shape[0] / norm - 1, 0)
                        criterion1 = len(np.nonzero(score_midpts > self.model_params['thre2'])[0]) > 0.8 * len(
                            score_midpts)
                        criterion2 = score_with_dist_prior > 0
                        if criterion1 and criterion2:
                            connection_candidate.append(
                                [i, j, score_with_dist_prior, score_with_dist_prior + candA[i][2] + candB[j][2]])

                connection_candidate = sorted(connection_candidate, key=lambda x: x[2], reverse=True)
                connection = np.zeros((0, 5))
                for c in range(len(connection_candidate)):
                    i, j, s = connection_candidate[c][0:3]
                    if (i not in connection[:, 3] and j not in connection[:, 4]):
                        connection = np.vstack([connection, [candA[i][3], candB[j][3], s, i, j]])
                        if (len(connection) >= min(nA, nB)):
                            break

                connection_all.append(connection)
            else:
                special_k.append(k)
                connection_all.append([])

        # last number in each row is the total parts number of that person
        # the second last number in each row is the score of the overall configuration
        subset = -1 * np.ones((0, 20))
        candidate = np.array([item for sublist in all_peaks for item in sublist])

        for k in range(len(mapIdx)):
            if k not in special_k:
                partAs = connection_all[k][:, 0]
                partBs = connection_all[k][:, 1]
                indexA, indexB = np.array(limbSeq[k]) - 1

                for i in range(len(connection_all[k])):  # = 1:size(temp,1)
                    found = 0
                    subset_idx = [-1, -1]
                    for j in range(len(subset)):  # 1:size(subset,1):
                        if subset[j][indexA] == partAs[i] or subset[j][indexB] == partBs[i]:
                            subset_idx[found] = j
                            found += 1

                    if found == 1:
                        j = subset_idx[0]
                        if (subset[j][indexB] != partBs[i]):
                            subset[j][indexB] = partBs[i]
                            subset[j][-1] += 1
                            subset[j][-2] += candidate[partBs[i].astype(int), 2] + connection_all[k][i][2]
                    elif found == 2:  # if found 2 and disjoint, merge them
                        j1, j2 = subset_idx
                        print("found = 2", file=sys.stderr)
                        membership = ((subset[j1] >= 0).astype(int) + (subset[j2] >= 0).astype(int))[:-2]
                        if len(np.nonzero(membership == 2)[0]) == 0:  # merge
                            subset[j1][:-2] += (subset[j2][:-2] + 1)
                            subset[j1][-2:] += subset[j2][-2:]
                            subset[j1][-2] += connection_all[k][i][2]
                            subset = np.delete(subset, j2, 0)
                        else:  # as like found == 1
                            subset[j1][indexB] = partBs[i]
                            subset[j1][-1] += 1
                            subset[j1][-2] += candidate[partBs[i].astype(int), 2] + connection_all[k][i][2]

                    # if find no partA in the subset, create a new subset
                    elif not found and k < 17:
                        row = -1 * np.ones(20)
                        row[indexA] = partAs[i]
                        row[indexB] = partBs[i]
                        row[-1] = 2
                        row[-2] = sum(candidate[connection_all[k][i, :2].astype(int), 2]) + connection_all[k][i][2]
                        subset = np.vstack([subset, row])

        # delete some rows of subset which has few parts occur
        deleteIdx = [];
        for i in range(len(subset)):
            if subset[i][-1] < 4 or subset[i][-2] / subset[i][-1] < 0.4:
                deleteIdx.append(i)
        subset = np.delete(subset, deleteIdx, axis=0)

        # visualize
        colors = [[255, 0, 0], [255, 85, 0], [255, 170, 0], [255, 255, 0], [170, 255, 0], [85, 255, 0], [0, 255, 0], \
                  [0, 255, 85], [0, 255, 170], [0, 255, 255], [0, 170, 255], [0, 85, 255], [0, 0, 255], [85, 0, 255], \
                  [170, 0, 255], [255, 0, 255], [255, 0, 170], [255, 0, 85]]
        cmap = matplotlib.cm.get_cmap('hsv')

        canvas = orig_im  # cv.imread(test_image) # B,G,R order

        if self.visualize:
            for i in range(18):
                rgba = np.array(cmap(1 - i / 18. - 1. / 36))
                rgba[0:3] *= 255
                for j in range(len(all_peaks[i])):
                    cv.circle(canvas, (int(all_peaks[i][j][0] * scale_x), int(all_peaks[i][j][1] * scale_y)), 4,
                              colors[i], thickness=-1)

        to_plot = cv.addWeighted(orig_im, 0.3, canvas, 0.7, 0)

        # visualize 2
        stickwidth = 4

        persons = [{partx: [-1, -1] for partx in self.model_params['model']['part_str']} for pi in range(len(subset))]
        for i in range(17):
            for n in range(len(subset)):
                index = subset[n][np.array(limbSeq[i]) - 1]
                if -1 in index:
                    continue
                cur_canvas = canvas.copy()
                Y = candidate[index.astype(int), 0] * scale_y
                X = candidate[index.astype(int), 1] * scale_x
                persons[n][self.model_params['model']['part_str'][limbSeq[i][0] - 1]] = [int(X[0]), int(Y[0])]
                persons[n][self.model_params['model']['part_str'][limbSeq[i][1] - 1]] = [int(X[1]), int(Y[1])]
                mX = np.mean(X)
                mY = np.mean(Y)
                length = ((X[0] - X[1]) ** 2 + (Y[0] - Y[1]) ** 2) ** 0.5
                angle = math.degrees(math.atan2(X[0] - X[1], Y[0] - Y[1]))
                polygon = cv.ellipse2Poly((int(mY), int(mX)), (int(length / 2), stickwidth), int(angle), 0, 360, 1)
                cv.fillConvexPoly(cur_canvas, polygon, colors[i])
                canvas = cv.addWeighted(canvas, 0.4, cur_canvas, 0.6, 0)

        maxPersonArea = 0
        for n, pax in enumerate(persons):
            minx = min([px[1] for px in persons[n].values() if -1 not in px])
            maxx = max([px[1] for px in persons[n].values() if -1 not in px])
            miny = min([px[0] for px in persons[n].values() if -1 not in px])
            maxy = max([px[0] for px in persons[n].values() if -1 not in px])
            padx = (maxx - minx) / 4
            pady = (maxy - miny) / 4
            persons[n]["bbox"] = {}
            persons[n]["bbox"]["person"] = {"x1": minx, "y1": miny, "x2": maxx, "y2": maxy, "width": maxx - minx,
                                            "height": maxy - miny}
            if (maxy - miny) * (maxx - minx) > maxPersonArea:
                maxPersonArea = (maxy - miny) * (maxx - minx)

        small_persons = [pi for pi, pp in enumerate(persons) if
                         pp["bbox"]["person"]["width"] * pp["bbox"]["person"]["height"] < maxPersonArea]
        persons = np.delete(persons, small_persons, axis=0)
        face_points = ["Reye", "Leye", "Rear", "Lear", "[nose"]
        upper_points = ["Rsho", "Lsho", "Rhip", "Lhip", "Neck", "Lelb", "Relb"]
        face_upper_points = ['Reye', 'Leye', 'neck', 'Lsho', 'Rsho', 'Lelb', 'Relb', '[nose', 'Lear', 'Rear']
        #        face_upper_points = face_points+upper_points
        for pax in persons:
            pad_win = abs(pax["Rsho"][1] - pax["Lsho"][1]) / 2
            pax["bbox"]["person"] = util.pad_bbox(pax["bbox"]["person"], pad_win * 2, orig_im.shape)

            face_upper_parray = [rval for rkey, rval in pax.iteritems() if
                                 rkey in face_upper_points and -1 not in rval and rkey != 'bbox']
            if len(face_upper_parray) > 0:
                bb = cv.boundingRect(np.array(face_upper_parray))
                pax["bbox"]["faceupper"] = {'x1': bb[1], 'y1': bb[0] - pad_win, 'x2': bb[1] + bb[3],
                                            'y2': bb[0] + bb[2] + 2 * pad_win, 'pose_type': 'faceupper'}
            #                pax["bbox"]["faceupper"] = util.pad_bbox( pax["bbox"]["faceupper"], pad_win, orig_im.shape )

            bbpts = [rval for rkey, rval in pax.iteritems() if
                     rkey not in face_points and -1 not in rval and rkey != 'bbox']
            if len(bbpts) > 0:
                bb = cv.boundingRect(np.array(bbpts))
                pax["bbox"]["full"] = {'x1': bb[1], 'y1': bb[0], 'x2': bb[1] + bb[3], 'y2': bb[0] + bb[2],
                                       'pose_type': 'full'}
                pax["bbox"]["full"] = util.pad_bbox(pax["bbox"]["full"], pad_win, orig_im.shape)
            else:
                # to-do: handle face/head objects in visual_search
                #                pax["bbox"]["noPose"] = {'x1':0,'y1':0,'x2':orig_im.shape[1]-1,'y2':orig_im.shape[0]-1}
                del pax["bbox"]["person"]
            if -1 not in pax["Lhip"] and -1 not in pax["Rhip"]:
                bb = cv.boundingRect(np.array([pax["Rsho"], pax["Lsho"], pax["Lhip"], pax["Rhip"]]))
                pax["bbox"]["upper"] = {'x1': bb[1], 'y1': bb[0], 'x2': bb[1] + bb[3], 'y2': bb[0] + bb[2],
                                        'pose_type': 'upper'}
                pax["bbox"]["upper"] = util.pad_bbox(pax["bbox"]["upper"], pad_win, orig_im.shape)
                if -1 not in pax["Lank"] and -1 not in pax["Rank"]:
                    bb = cv.boundingRect(np.array([pax["Rhip"], pax["Lhip"], pax["Lank"], pax["Rank"]]))
                    pax["bbox"]["lower"] = {'x1': bb[1], 'y1': bb[0], 'x2': bb[1] + bb[3], 'y2': bb[0] + bb[2],
                                            'pose_type': 'lower'}
                    pax["bbox"]["lower"] = util.pad_bbox(pax["bbox"]["lower"], pad_win, orig_im.shape)

                    bb = cv.boundingRect(
                        np.array([pax["Rank"], pax["Lank"], [pax["Lank"][0] + pad_win, pax["Lank"][1]]]))
                    pax["bbox"]["footwear"] = {'x1': bb[1], 'y1': bb[0], 'x2': bb[1] + bb[3], 'y2': bb[0] + bb[2],
                                               'pose_type': 'footwear'}
                    pax["bbox"]["footwear"] = util.pad_bbox(pax["bbox"]["footwear"], pad_win, orig_im.shape)
                elif -1 not in pax["Lkne"] and -1 not in pax["Rkne"]:
                    bb = cv.boundingRect(np.array([pax["Rhip"], pax["Lhip"], pax["Lkne"], pax["Rkne"]]))
                    pax["bbox"]["lower"] = {'x1': bb[1], 'y1': bb[0], 'x2': bb[1] + bb[3], 'y2': bb[0] + bb[2],
                                            'pose_type': 'lower'}
                    pax["bbox"]["lower"] = util.pad_bbox(pax["bbox"]["lower"], pad_win, orig_im.shape)
            pad_win_face = None
            if -1 not in pax["Reye"] and -1 not in pax["Leye"]:
                pad_win_face = abs(pax["Reye"][1] - pax["Leye"][1]) / 2
                bb = cv.boundingRect(np.array([pax["Leye"], pax["Reye"]]))
                pax["bbox"]["eyes"] = {'x1': bb[1], 'y1': bb[0], 'x2': bb[1] + bb[3],
                                       'y2': bb[0] + bb[2] + pad_win_face, 'pose_type': 'eyes'}
                pax["bbox"]["eyes"] = util.pad_bbox(pax["bbox"]["eyes"], pad_win_face, orig_im.shape)

            face_parray = [rval for rkey, rval in pax.iteritems() if
                           rkey in face_points and -1 not in rval and rkey != 'bbox']
            if len(face_parray) > 0:
                bb = cv.boundingRect(np.array(face_parray))
                if pad_win_face is None:
                    pad_win_face = bb[3]
                pax["bbox"]["face"] = {'x1': bb[1], 'y1': bb[0] - pad_win_face, 'x2': bb[1] + bb[3],
                                       'y2': bb[0] + bb[2] + 2 * pad_win_face, 'pose_type': 'face'}
                pax["bbox"]["face"] = util.pad_bbox(pax["bbox"]["face"], pad_win_face, orig_im.shape)

            if self.visualize:
                pcount = 0
                for pose_type, patch in pax["bbox"].iteritems():
                    cv.rectangle(canvas, (patch["x1"], patch["y1"]), (patch["x2"], patch["y2"]),
                                 (255 - pcount, 0, pcount), 1, 1)
                    pcount += 30

        if self.visualize:
            plt.imshow(to_plot[:, :, [2, 1, 0]])
            fig = matplotlib.pyplot.gcf()
            fig.set_size_inches(12, 12)

            plt.imshow(canvas[:, :, [2, 1, 0]])
            fig = matplotlib.pyplot.gcf()
            fig.set_size_inches(12, 12)

        return persons
