from __future__ import print_function

import hashlib
import os
import stat
import sys
import time
import cv2
import numpy as np
import skimage.io as skio
from skimage import img_as_ubyte

from classify_fashion import FashionClassification
from cnn_feats import CNNFeats
from color_feats import ColorFeats
from face_gender import GenderClassification
from human_pose import HumanPoseDetection
from salient_objects import SalientObjDetection
from segment_fashion import FashionSegmentation
from util import segplainbg, addTextOnImage, getImage


class VisualSearch(object):
    def __init__(self, params, device_no=None):
        """ Initialize CNN Feats model with the selected model type.

        Arguments:
            classifier_model_type: string, Selects the type of the model,
                    either 'caffe' or 'tensorflow'.
                    Optional with default of 'caffe'.
            classifier_model_dir: string, The base directory of the fashion classification model.
                    Optional with default of 'lib_dependencies/abz_fashion_image_classification/'
                    for 'caffe' model_type and default of
                    'lib_dependencies/tensorflow/abz_fashion_image_classification/' for 'tensorflow' model_type.
            classifier_model_name: string, The name of the fashion classification model weights file.
                    Optional with default of 'models/fashcategories.caffemodel'
                    for 'caffe' model_type and default of
                    'model.ckpt' for 'tensorflow' model_type.

            cnnfeats_model_type: string, Selects the type of the model,
                    either 'caffe' or 'tensorflow'.
                    Optional with default of 'caffe'.
            cnnfeats_model_dir: string, The base directory of the CNN feats model.
                    Optional with default of 'data/feats_models/'
                    for 'caffe' model_type and default of
                    'data/tensorflow/feats_models/' for 'tensorflow' model_type.
            cnnfeats_model_name: string, The name of the CNN feats model weights file.
                    Optional with default of 'df_attr-cats_0.3-1.0.caffemodel'
                    for 'caffe' model_type and default of
                    'model.ckpt' for 'tensorflow' model_type.

            gender_classifier_model_type: string, Selects the type of the model,
                    either 'caffe' or 'tensorflow'.
                    Optional with default of 'caffe'.
            gender_classifier_model_dir: string, The base directory of the gender classification model.
                    Optional with default of 'lib_dependencies/cnn_age_gender_models_and_data.0.0.2/'
                    for 'caffe' model_type and default of
                    'lib_dependencies/tensorflow/cnn_age_gender_models_and_data.0.0.2/' for 'tensorflow' model_type.
            gender_classifier_model_name: string, The name of the gender classification model weights file.
                    Optional with default of 'gender_net.caffemodel'
                    for 'caffe' model_type and default of
                    'model.ckpt' for 'tensorflow' model_type.

            pose_extractor_model_type: string, Selects the type of the model,
                    either 'caffe' or 'tensorflow'.
                    Optional with default of 'caffe'.
            pose_extractor_model_dir: string, The base directory of the human pose detection model.
                    Optional with default of 'lib_dependencies/RheC_RTMPPose/models/_trained_COCO/'
                    for 'caffe' model_type and default of
                    'lib_dependencies/tensorflow/RheC_RTMPPose/models/_trained_COCO/' for 'tensorflow' model_type.
            pose_extractor_model_name: string, The name of the human pose detection model weights file.
                    Optional with default of 'models/fashcategories.caffemodel'
                    for 'caffe' model_type and default of
                    'model.ckpt' for 'tensorflow' model_type.

            salient_detector_model_type: string, Selects the type of the model,
                    either 'caffe' or 'tensorflow'.
                    Optional with default of 'caffe'.
            salient_detector_model_dir: string, The base directory of the salient objects model.
                    Optional with default of 'lib_dependencies/saliency/model/'
                    for 'caffe' model_type and default of
                    'lib_dependencies/tensorflow/saliency/' for 'tensorflow' model_type.
            salient_detector_model_name: string, The name of the  salient objects model weights file.
                    Optional with default of 'train_iter_40000.caffemodel'
                    for 'caffe' model_type and default of
                    'model.ckpt' for 'tensorflow' model_type.

            segmentor_model_type: string, Selects the type of the model,
                    either 'caffe' or 'tensorflow'.
                    Optional with default of 'caffe'.
            segmentor_model_dir: string, The base directory of the semantic segmentation model.
                    Optional with default of 'lib_dependencies/fcn.berkeleyvision.org/'
                    for 'caffe' model_type and default of
                    'lib_dependencies/tensorflow/fcn.berkeleyvision.org/' for 'tensorflow' model_type.
            segmentor_model_name: string, The name of the  semantic segmentation model weights file.
                    Optional with default of 'models/wlrfash3-fcn8s.caffemodel'
                    for 'caffe' model_type and default of
                    'model.ckpt' for 'tensorflow' model_type.
            device_no: int, GPU device no if >= 0 else cpu mode
        """
        cl_params = {'catlist': params.get('CLASSIFIER_CATLIST'),
                     'catposemap': params.get('CLASSIFIER_CATPOSEMAP'),
                     'prototxt': params.get('CLASSIFIER_DEPLOY_PROTOTXT')}
        self.classifier = FashionClassification(model_type=params.get('CLASSIFIER_MODEL_TYPE'),
                                                model_dir=params.get('CLASSIFIER_MODEL_DIR'),
                                                model_name=params.get('CLASSIFIER_MODEL_NAME'),
                                                serving_host=params.get('CLASSIFIER_SERVING_HOST'),
                                                serving_port=params.get('CLASSIFIER_SERVING_PORT'),
                                                serving_timeout=params.get('CLASSIFIER_SERVING_TIMEOUT'),
                                                params=cl_params,
                                                device_no=device_no)

        self.cnnfeats = CNNFeats(model_type=params.get('FEAT_MODEL_TYPE'),
                                 model_dir=params.get('FEAT_MODEL_DIR'),
                                 model_name=params.get('FEAT_MODEL_NAME'),
                                 serving_host=params.get('FEAT_SERVING_HOST'),
                                 serving_port=params.get('FEAT_SERVING_PORT'),
                                 serving_timeout=params.get('FEAT_SERVING_TIMEOUT'),
                                 device_no=device_no)

        self.colorfeats = ColorFeats()

        self.gender_classifier = GenderClassification(model_type=params.get('GENDER_CLASSIFIER_MODEL_TYPE'),
                                                      model_dir=params.get('GENDER_CLASSIFIER_MODEL_DIR'),
                                                      model_name=params.get('GENDER_CLASSIFIER_MODEL_NAME'),
                                                      serving_host=params.get('GENDER_CLASSIFIER_SERVING_HOST'),
                                                      serving_port=params.get('GENDER_CLASSIFIER_SERVING_PORT'),
                                                      serving_timeout=params.get('GENDER_CLASSIFIER_SERVING_TIMEOUT'),
                                                      device_no=device_no)

        poseparams = {'model_type': 'ZheC_RTMPPose'}
        self.pose_extractor = HumanPoseDetection(model_type=params.get('POSE_EXTRACTOR_MODEL_TYPE'),
                                                 model_dir=params.get('POSE_EXTRACTOR_MODEL_DIR'),
                                                 model_name=params.get('POSE_EXTRACTOR_MODEL_NAME'),
                                                 serving_host=params.get('POSE_EXTRACTOR_SERVING_HOST'),
                                                 serving_port=params.get('POSE_EXTRACTOR_SERVING_PORT'),
                                                 serving_timeout=params.get('POSE_EXTRACTOR_SERVING_TIMEOUT'),
                                                 device_no=device_no, params=poseparams)

        self.salient_detector = SalientObjDetection(model_type=params.get('SALIENT_DETECTOR_MODEL_TYPE'),
                                                    model_dir=params.get('SALIENT_DETECTOR_MODEL_DIR'),
                                                    model_name=params.get('SALIENT_DETECTOR_MODEL_NAME'),
                                                    serving_host=params.get('SALIENT_DETECTOR_SERVING_HOST'),
                                                    serving_port=params.get('SALIENT_DETECTOR_SERVING_PORT'),
                                                    serving_timeout=params.get('SALIENT_DETECTOR_SERVING_TIMEOUT'),
                                                    device_no=device_no)

        self.segmentor = FashionSegmentation(model_type=params.get('SEGMENTOR_MODEL_TYPE'),
                                             model_dir=params.get('SEGMENTOR_MODEL_DIR'),
                                             model_name=params.get('SEGMENTOR_MODEL_NAME'),
                                             serving_host=params.get('SEGMENTOR_SERVING_HOST'),
                                             serving_port=params.get('SEGMENTOR_SERVING_PORT'),
                                             serving_timeout=params.get('SEGMENTOR_SERVING_TIMEOUT'),
                                             device_no=device_no)

    def _getHumanPosePatches(self, img):
        """compute pose in image using deep pose module"""
        height = img.shape[0]
        width = img.shape[1]
        persons = self.pose_extractor.detect_pose([img], visualize=False, visualize_all=False)
        persons = persons[0]
        print(persons, file=sys.stderr)
        if len(persons) > 0 and "bbox" in persons[0]:
            source = persons[0]["bbox"]
            if "face" in source or "upper" in source:
                return source

        source = {'noPose': {'x1': 0, 'x2': width, 'y1': 0, 'y2': height, 'pose_type': 'noPose'}}

        return source

    def _getGenderAndPose(self, imUrl, im):
        """identify gender from human body patches"""
        im_orig = im
        if len(im_orig.shape) == 3 and im_orig.shape[2] == 4:
            im_orig = im_orig[:, :, :3]

        source = self._getHumanPosePatches(im_orig)
        if "person" in source:
            img = im_orig[source["person"]["y1"]:source["person"]["y2"], source["person"]["x1"]:source["person"]["x2"],
                  :]
        else:
            img = im_orig

        # if "faceupper" in source: img_faceupper = im_orig[source["faceupper"]["y1"]:source["faceupper"]["y2"],
        # source["faceupper"]["x1"]:source["faceupper"]["x2"],:] self.gender_classifier.classify([img_faceupper]) del
        # source["faceupper"] else: gpredict = None
        source, img_face = self.gender_classifier.detect_faces(img, source)
        print(source)
        if len(img_face) > 0:
            print(img_face.shape, file=sys.stderr)
            source["gender_confidence"] = 0
            gpredict, gscore = self.gender_classifier.classify([img_face])
            if gpredict == "men" and gscore <= 0.9:
                gpredict = self.classifier.check_if_women(img, source)
                print("After re-verfying for gender = " + gpredict)
            if gscore > 0.9:
                source["gender_confidence"] = 1
        else:
            gpredict = None
        return source, gpredict

    def _segdet_categories(self, imUrl, gender=None, toSegment=True, qcat=None, send_segmap=False,
                           image_link_prefix=""):
        """
        detect upper/lower/full body bbox and deduce with semantic segmentation.
        actual function call that returns "source" object which has recommendations corresponding to pose patch.
        """

        im_orig = getImage(imUrl)
        m = hashlib.md5()
        m.update(imUrl)
        image_file_prefix = 'uploads/' + m.hexdigest()
        image_link_prefix += 'uploads/' + m.hexdigest()
        if qcat is not None:  # case of category known => bot_vsi didn't find xc_sku indexed
            source_res = {}
            seg_mask, bbox = segplainbg(im_orig)
            if seg_mask is None or bbox is None:
                pc_patch = {"x1": 0, "y1": 0, "x2": im.shape[1] - 1, "y2": im.shape[0] - 1}
            else:
                pc_patch = {"x1": bbox[0], "y1": bbox[1], "x2": bbox[2], "y2": bbox[3], "seg_mask": seg_mask}
            if gender is not None and gender in ["men", "women"]:
                pc_patch["gender"] = gender
            source_res[qcat] = pc_patch
            return source_res

        st = time.clock()
        source, gender1 = self._getGenderAndPose(imUrl.replace('https', 'http'), im_orig)
        print("Gender and pose detection done in  - " + str(time.clock() - st) + " seconds.", file=sys.stderr)
        print(source, gender1, file=sys.stderr)
        if gender is None or gender not in ["men", "women"]:
            gender = gender1
        if "gender_confidence" in source:
            gender_score = source["gender_confidence"]
            del source["gender_confidence"]
        else:
            gender_score = 0

        clothing_poses = ["upper", "full", "lower"]
        pose_types = {"full": 0, "upper": 1, "lower": 2, "footwear": 4, "bag": 3}
        if gender == "men":
            del clothing_poses[1]

        person = []
        if "noPose" in source:
            st = time.clock()
            segmaps, imgs, sobjss = self.salient_detector.get_salient_objects([im_orig], single_mode=False)
            sobjs = sobjss[0]
            print("Detected saliency in - " + str(time.clock() - st) + " seconds.", file=sys.stderr)
            st = time.clock()
            cls_result = self.classifier.classify_patches(im_orig, patches=sobjs, gender=gender,
                                                          image_file_prefix=image_file_prefix,
                                                          image_link_prefix=image_link_prefix, ignore_person=True)
            print("Classification of salient object done in  " + str(time.clock() - st) + " seconds.", file=sys.stderr)
            if cls_result is not None and "person" not in cls_result:
                return cls_result

            if cls_result is not None:
                if cls_result["person"]["prob"] > 0.9:
                    person = cls_result["person"]
                    im = im_orig[int(person["y1"]):int(person["y2"]), int(person["x1"]):int(person["x2"]), :]
                elif "pred_cats" in cls_result and len(cls_result["pred_cats"]) > 0:
                    del cls_result["person"]
                    return cls_result
            else:
                return source
        else:
            if "full" in source:
                print("Checking if full pose patch belongs to non-fcn supported category: ex ethnic categories.",
                      file=sys.stderr)
                st = time.clock()
                source_res = self.classifier.check_if_nonfcncat(im_orig, source_patches=source, gender=gender,
                                                                image_file_prefix=image_file_prefix,
                                                                image_link_prefix=image_link_prefix)
                print("Classification of full pose bbox done in  - " + str(time.clock() - st) + " seconds.",
                      file=sys.stderr)
                if source_res is not None:
                    return source_res

            if "person" in source and "footwear" in source and toSegment:
                person = source["person"]
                im = im_orig[int(person["y1"]):int(person["y2"]), int(person["x1"]):int(person["x2"]), :]
            else:
                if "face" in source:
                    del source["face"]
                if "person" in source:
                    del source["person"]
                if "bag" in source:
                    del source["bag"]
                if "eyes" in source:
                    del source["eyes"]
                if "faceupper" in source:
                    del source["faceupper"]

                st = time.clock()
                source_res = self.classifier.classify_patches(im_orig, patches=source, gender=gender,
                                                              image_file_prefix=image_file_prefix,
                                                              image_link_prefix=image_link_prefix)
                print("Classification of pose bboxes done in  - " + str(time.clock() - st) + " seconds.",
                      file=sys.stderr)
                return source_res

        new_height = 600
        new_width = int(new_height * im.shape[1] * 1.0 / im.shape[0])
        hfac = new_height * 1.0 / im.shape[0]
        wfac = new_width * 1.0 / im.shape[1]
        #    print( hfac, wfac, new_width, new_height)
        #    im = im.resize((new_width, new_height), Image.ANTIALIAS)
        im = cv2.resize(im, (new_width, new_height))
        #    _ = plt.figure()
        #    plt.imshow(im_person)

        st = time.clock()
        seg_prediction, simg = self.segmentor.segment_fashion(im, gender)
        print("FCN based semantic segmentation done in  - " + str(time.clock() - st) + " seconds.", file=sys.stderr)
        source_res = {}
        cvim_orig = np.array(im_orig)
        cvim_origcopy = cvim_orig.copy()

        for l in np.unique(seg_prediction):
            cat = self.segmentor.cat_labels[str(l)]
            pred_pose = self.segmentor.segcategories_pose[cat]
            if pred_pose == "None":
                continue

            x, y, w, h = cv2.boundingRect(img_as_ubyte(seg_prediction == l))
            mask = (seg_prediction[y:y + h, x:x + w] == l)
            if pred_pose in source and (pred_pose not in "footwear" and pred_pose not in "bags"):
                patch = source[pred_pose]
                print(pred_pose, patch)
                p_y1 = (patch["y1"] - person["y1"]) * hfac
                p_y2 = (patch["y2"] - person["y1"]) * hfac
                p_x1 = (patch["x1"] - person["x1"]) * wfac
                p_x2 = (patch["x2"] - person["x1"]) * wfac
                patch_mask = (seg_prediction[int(p_y1):int(p_y2), int(p_x1):int(p_x2)] == l)
                if np.sum(patch_mask == True) * 1.0 / ((p_y2 - p_y1) * (p_x2 - p_x1)) < 0.05:
                    print(np.sum(patch_mask == True))
                    print((p_y2 - p_y1), (p_x2 - p_x1))
                    continue

            if person:
                cat_patch = {"x1": int(x / wfac) + person["x1"], "y1": int(y / hfac) + person["y1"],
                             "x2": int((x + w) / wfac) + person["x1"], "y2": int((y + h) / hfac) + person["y1"],
                             "pose_type": pred_pose, "gender": gender}
            else:
                cat_patch = {"x1": int(x / wfac), "y1": int(y / hfac), "x2": int((x + w) / wfac),
                             "y2": int((y + h) / hfac),
                             "pose_type": pred_pose, "gender": gender}

            # ####################################################### ##Finer classification for fcn detected
            # entities - not working as expected TODO next iteration print("Finding finer category for the fcn
            #  predicted cat - " + cat, file=sys.stderr) st = time.clock() cat = self.classifier.classify_fcncat(
            #  im_orig, source_patches=source, cat_patch=cat_patch, source_cat=cat, gender=gender,
            #  image_file_prefix=image_file_prefix, image_link_prefix=image_link_prefix) print("Finer classification
            #  done :" + str(cat) + " - in " + str(time.clock()-st) + " seconds.", file=sys.stderr) if cat is None:
            #  continue #####################################################
            source_res[cat] = cat_patch
            source_res[cat]["crop_rect"] = str(source_res[cat]["x1"]) + ',' + str(source_res[cat]["y1"]) + ',' + str(
                source_res[cat]["x2"]) + ',' + str(source_res[cat]["y2"])

            source_patch = source_res[cat]
            addTextOnImage(cvim_orig, source_patch, cat)
            #            _ = plt.figure()
            patch_curr = im_orig[source_res[cat]["y1"]:source_res[cat]["y2"],
                         source_res[cat]["x1"]:source_res[cat]["x2"], :]
            mask = np.where((mask == True), 1, 0).astype('uint8')
            mask = cv2.resize(mask, (patch_curr.shape[1], patch_curr.shape[0]))
            obj = np.asarray(patch_curr * mask[:, :, np.newaxis], dtype=np.uint8)
            obj[mask == 0] = 255
            source_res[cat]["seg_mask"] = mask
            skio.imsave(image_file_prefix + "_" + cat + '.jpg', obj)
            os.chmod(image_file_prefix + "_" + cat + ".jpg", stat.S_IRUSR | stat.S_IWUSR)
        cv2.addWeighted(cvim_orig, 0.4, cvim_origcopy, 0.6, 0, cvim_origcopy)
        skio.imsave(image_file_prefix + '_tagged.jpg', cvim_origcopy)
        source_res["tagged_img"] = image_link_prefix + '_tagged.jpg'
        source_res["gender_confidence"] = gender_score

        if send_segmap:
            segmap = np.zeros(im_orig.shape, dtype=seg_prediction.dtype)
            if "person" in source:
                segmap[person["y1"]:person["y2"], person["x1"]:person["x2"]] = seg_prediction
            else:
                segmap = seg_prediction
            skio.imsave(image_file_prefix + '_segmap.png', segmap)
            source_res["segmap"] = image_file_prefix + '_segmap.png'
        return source_res

    # def predictItemsAndGetFeats(self, imPath, qgender=None, qcat=None, toSegment=True):
    def items_and_feats(self, imPath, qgender=None, qcat=None, toSegment=True, image_link_prefix=""):
        """
        Find items bbox and corresponding features
        """
        st = time.clock()
        fpatches = self._segdet_categories(imPath, gender=qgender, toSegment=toSegment, qcat=qcat,
                                           image_link_prefix=image_link_prefix)
        print("Finding items and corresponding items in image - " + imPath, file=sys.stderr)
        # TODO - restore the following logging
        # logger.info("Finding items and corresponding items in image - " + imPath)
        for p, fp in fpatches.items():
            print("Current patch " + str(p), file=sys.stderr)
            if p in ["pred_cats", "gender_confidence", "tagged_img", "upload_img"]:
                continue
            feats = {}
            bbox = [fp["x1"], fp["y1"], fp["x2"], fp["y2"]]
            # feature = cnnfeats[0].extract_features_single(image_file_prefix+'.jpg',
            print("Extracting features for " + str(p) + " bbox: " + str(bbox), file=sys.stderr)
            feature = self.cnnfeats.extract_features_single(imPath,
                                                            im_bbox=bbox,
                                                            layers=['fc7', 'fc6'])
            qfeat = feature['fc7'].reshape(1, 4096)
            if "seg_mask" in fp:
                seg_mask = fp["seg_mask"]
                del fp["seg_mask"]
            else:
                seg_mask = None
            cfeat = self.colorfeats.getColorHist(imPath, bbox=bbox, mask=seg_mask)
            feats['qfeat'] = qfeat.tolist()
            feats['cfeat'] = cfeat.tolist()
            fp['feats'] = feats
        fpatches['status_code'] = 1
        return fpatches

    def get_feats(self, im_path, bbox):
        feats = {}
        feature = self.cnnfeats.extract_features_single(im_path,
                                                        im_bbox=bbox,
                                                        layers=['fc7', 'fc6'])
        qfeat = feature['fc7'].reshape(1, 4096)
        seg_mask = None
        cfeat = self.colorfeats.getColorHist(im_path, bbox=bbox, mask=seg_mask)
        feats['qfeat'] = qfeat.tolist()
        feats['cfeat'] = cfeat.tolist()
        return feats
