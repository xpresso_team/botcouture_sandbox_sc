from __future__ import print_function

import json
import sys

import cv2
import numpy as np
import skimage.io as skio
import copy

from txutils import Transformer
from util import segplainbg, addTextOnImage
from utils import value_or_default
from threading import Lock


class FashionClassification(object):
    def __init__(self, model_type, model_dir, model_name, device_no,
                 serving_host=None, serving_port=None, serving_timeout=None, params=None):
        """ Initialize classification model with the selected model type.

        Arguments:
            model_type: string, Selects the type of the model,
                    either 'caffe' or 'tensorflow'.
                    Optional with default of 'caffe'.
            model_dir: string, The base directory of the fashion classification model.
                    Optional with default of 'lib_dependencies/abz_fashion_image_classification/'
                    for 'caffe' model_type and default of
                    'lib_dependencies/tensorflow/abz_fashion_image_classification/' for 'tensorflow' model_type.                             
            model_name: string, The name of the fashion classification model weights file.
                    Optional with default of 'models/fashcategories.caffemodel'
                    for 'caffe' model_type and default of
                    'model.ckpt' for 'tensorflow' model_type.
            device_no: int, GPU device no if >= 0 else cpu mode
        """
        model_dir_was_none = model_dir is None
        model_dir = value_or_default(model_dir, 'lib_dependencies/abz_fashion_image_classification/')

        self.catnetlock = Lock()
        def_params = {'image_dim': 256, 'crop_dim': 224,
                      'oversample': False}
        txparams = value_or_default(params, def_params)
        self.tx = Transformer(params=txparams)
        catlistfile = "data/category_list.json"
        catposemapfile = "data/category_posemap.json"
        prototxtfile = "deploy.prototxt"
        nonfcncat_file = "data/nonfcn_category_list_v1.6.8.json"
        segcategories_classes_file = "data/segcategories_classify_v1.6.8.json"
        femalecat_file = "data/female_category_list_v1.6.8.json"
        catnet_coarse_file = None
        #        catnet_coarse_file = "data/category_list_grouping.json"
        if params is not None:
            if 'catlist' in params:
                catlistfile = params['catlist']
            if 'catposemap' in params:
                catposemapfile = params['catposemap']
            if 'prototxt' in params:
                prototxtfile = params['prototxt']
            if 'catnet_coarse_file' in params:
                catnet_coarse_file = params['catnet_coarse_file']
            if 'nonfcncat_file' in params:
                nonfcncat_file = params['nonfcncat_file']
            if 'femalecat_file' in params:
                femalecat_file = params['femalecat_file']
            if 'segcategories_classes_file' in params:
                segcategories_classes_file = params['segcategories_classes_file']

        with open(model_dir + catlistfile) as fp:
            self.catlist = json.load(fp)
        with open(model_dir + catposemapfile) as fp:
            self.posemap = json.load(fp)
        if catnet_coarse_file is not None:
            with open(model_dir + catnet_coarse_file) as fp:
                self.category_coarse_group = json.load(fp)
            self.category_coarse = list(set([y for x, y in self.category_coarse_group.items()]))
            print("List of coarse categories: " + str(self.category_coarse), file=sys.stderr)
        else:
            self.category_coarse_group = None
        with open(model_dir + nonfcncat_file) as fp:
            self.nonfcncat_list = json.load(fp)
        with open(model_dir + femalecat_file) as fp:
            self.femalecat_list = json.load(fp)
        with open(model_dir + segcategories_classes_file) as fp:
            self.segcategories_classify = json.load(fp)

        model_type = value_or_default(model_type, 'caffe')

        if model_type.lower() == 'caffe':
            model_name = value_or_default(model_name, 'models/fashcategories.caffemodel')
            weights = model_dir + model_name
            prototxt = model_dir + prototxtfile
            from caffe_models import classify_fashion
            classify_fashion.init(device_no)
            from caffe_models.classify_fashion import FashionClassification
            self.model = FashionClassification(prototxt, weights, device_no)
        elif model_type.lower() == 'serving':
            host = value_or_default(serving_host, '0.0.0.0')
            port = value_or_default(serving_port, 9000)
            timeout = value_or_default(serving_timeout, 60)
            from tensorflow_models.classify_fashion import FashionClassificationClient
            self.model = FashionClassificationClient(host, port, timeout, device_no)
        else:
            if model_dir_was_none:
                model_dir = 'lib_dependencies/tensorflow/abz_fashion_image_classification/'
            model_name = value_or_default(model_name, 'model.ckpt')
            weights = model_dir + model_name
            import tensorflow_models.classify_fashion as classify_fashion
            from tensorflow_models.classify_fashion import FashionClassification
            self.model = FashionClassification(weights, device_no)

    def check_if_women(self, im, source_patches):
        """
        Verifiying a face gender prediction of male based on the lower and full body patch
        to belong to non-female specific categories. Ex. check not dress/skirt
        """
        patches = {}
        if "full" in source_patches:
            patches["full"] = source_patches["full"]
        if "lower" in source_patches:
            patches["lower"] = source_patches["lower"]
        source_res = self.classify_patches(im, patches)
        if any([x for x in self.femalecat_list if x in source_res]):
            return "women"
        else:
            return "men"

    def check_if_nonfcncat(self, im, source_patches=None, gender=None, image_file_prefix=None, image_link_prefix=None,
                           ignore_person=False):
        """
        Classify patches identified by human pose detection. 
        Use generic fashion classifier to classify image or patches identified.
        """
        patches = {}
        if "full" in source_patches:
            patches["full"] = source_patches["full"]
        source_res = self.classify_patches(im, patches, gender=gender, image_file_prefix=image_file_prefix,
                                           image_link_prefix=image_link_prefix)
        if any([x for x in self.nonfcncat_list if x in source_res]):
            return source_res
        else:
            return None

    def classify_fcncat(self, im, source_patches, cat_patch, source_cat, gender=None, image_file_prefix=None,
                        image_link_prefix=None, ignore_person=False):
        """
        Classify patches identified by human pose detection. 
        Use generic fashion classifier to classify image or patches identified.
        """
        patch = cat_patch
        if patch['pose_type'] in source_patches:
            patch = source_patches[patch['pose_type']]
        im_patch = im[patch["y1"]:patch["y2"], patch["x1"]:patch["x2"], :]
        if im_patch.shape[0] * im_patch.shape[1] == 0:
            return None
        fine_cats = self.segcategories_classify[source_cat]
        with self.catnetlock:
            prediction = self.model.infer(self.tx.preprocess(im_patch))
        # prediction_argsorted = prediction.argsort()[::-1] finecats_predicted = [[self.catlist[x],prediction[x]] for
        # x in prediction_argsorted if self.catlist[x] in fine_cats]
        pcat, probs = self.rank_classes(prediction)
        finecats_predicted = [[x, y] for x, y in zip(pcat, probs) if x in fine_cats]
        print("finer categories prediction: " + str(finecats_predicted), file=sys.stderr)
        if len(finecats_predicted) > 0:
            top_cat = finecats_predicted[0]
            if top_cat[1] > 0.10:
                return top_cat[0]
            else:
                return None
        else:
            return None

    def rank_classes(self, prediction, coarse=False):
        """
        Rank class predictions while also using the semantic information
        """
        # coarse grouping and ungrouping to strongly predict broader category and then suggest finer category
        # Cannot use right now TODO when finer classifier production ready
        if coarse and self.category_coarse_group is not None:
            prediction_coarse = []
            for i, cat in enumerate(self.category_coarse):
                prediction_coarse.append(np.sum(
                    [x for ix, x in enumerate(prediction) if self.category_coarse_group[self.catlist[ix]] == cat]))
            prediction_coarse = np.array(prediction_coarse)
            print("Coarse prediction - \n" + str(self.category_coarse) + "\n  " + str(prediction_coarse),
                  file=sys.stderr)
            top_inds = prediction_coarse.argsort()[::-1][:5]
            pcat = np.array(self.category_coarse)[top_inds]
            print(pcat, file=sys.stderr)
            probs = prediction_coarse[top_inds]
            print(probs, file=sys.stderr)
        else:
            catlist_uniq = list(set(self.catlist))
            prediction_uniq = np.array(
                [np.sum(prediction[[iy for iy, y in enumerate(self.catlist) if y == x]]) for x in catlist_uniq])
            top_inds = prediction_uniq.argsort()[::-1][:5]
            pcat = np.array(catlist_uniq)[top_inds]
            print(pcat, file=sys.stderr)
            probs = prediction_uniq[top_inds]
            print(probs, file=sys.stderr)
        return pcat, probs

    def classify_patches(self, im, patches=None, gender=None, image_file_prefix=None, image_link_prefix=None,
                         ignore_person=False):
        """
        Classify patches identified by human pose detection. 
        Use generic fashion classifier to classify image or patches identified.
        """
        ignore_list = ["others"]
        if ignore_person:
            ignore_list.append("full_person")
        if patches is None:
            with self.catnetlock:
                prediction = self.model.infer(self.tx.preprocess(im))
            pcat, probs = self.rank_classes(prediction)
            if pcat[0] == "full_person":
                source_res = {"person": {"x1": 0, "y1": 0,
                                         "x2": im.shape[1] - 1, "y2": im.shape[0] - 1,
                                         "prob": probs[0].tolist()}}
            #            return source_res
            source_res["pred_cats"] = []  # {"noPose":{"pred_cats":[]}}
            strong_pred = False
            for pc, pp in zip(pcat, probs):
                if pc != "full_person":
                    if pp > 0.45 and pc not in ignore_list:
                        seg_mask, bbox = segplainbg(im)
                        if seg_mask is None or bbox is None:
                            pc_patch = {"x1": 0, "y1": 0, "x2": im.shape[1] - 1, "y2": im.shape[0] - 1}
                        else:
                            pc_patch = {"x1": bbox[0], "y1": bbox[1], "x2": bbox[2], "y2": bbox[3],
                                        "seg_mask": seg_mask}
                        if gender is not None and gender in ["men", "women"]:
                            pc_patch["gender"] = gender
                        source_res[pc] = pc_patch
                        strong_pred = True
                    elif pc not in ignore_list and pp > 0.1:
                        source_res["pred_cats"].append(pc)
            if len(source_res["pred_cats"]) == 0:
                del source_res["pred_cats"]
            elif "person" not in source_res:
                source_res["noPose"] = {"x1": 0, "y1": 0, "x2": im.shape[1] - 1, "y2": im.shape[0] - 1}
            if "person" not in source_res and "pred_cats" not in source_res:
                return None
            return source_res
        elif isinstance(patches, list):
            source_res = {}
            im_copy = im.copy()
            objectPerson = None
            objectOther = None
            for x, y, w, h in patches:
                im_patch = im[y:y + h, x:x + h, :]
                patch = {"x1": x, "x2": x + w, "y1": y, "y2": y + h}
                with self.catnetlock:
                    prediction = self.model.infer(self.tx.preprocess(im_patch))
                pcat, probs = self.rank_classes(prediction)

                if not ignore_person and pcat[0] == "full_person" and (
                        objectPerson is None or probs[0] > objectPerson["prob"]):
                    objectPerson = {"prob": probs[0],
                                    "patch": {"x1": x, "y1": y, "x2": x + w, "y2": y + h, "prob": probs[0]}}
                elif objectOther is None or probs[0] > objectOther["probs"][0]:
                    objectOther = {"probs": probs, "pcat": pcat, "patch": patch}
            if objectPerson is not None:
                source_res["person"] = objectPerson["patch"]
            #            return source_res

            if objectOther is not None:
                strong_pred = False
                source_res["pred_cats"] = []  # {"noPose":{"pred_cats":[]}}
                pc_patch = objectOther["patch"]
                if image_file_prefix is not None and image_link_prefix is not None:
                    print("saving salient object image to " + image_file_prefix + "_salient.jpg", file=sys.stderr)
                    skio.imsave(image_file_prefix + '_salient.jpg',
                                im[pc_patch["y1"]:pc_patch["y2"], pc_patch["x1"]:pc_patch["x2"], :])
                for pc, pp in zip(objectOther["pcat"], objectOther["probs"]):
                    if pc not in ignore_list and pp > 0.45:
                        if gender is not None and gender in ["men", "women"]:
                            pc_patch["gender"] = gender
                        source_res[pc] = pc_patch
                        strong_pred = True
                        addTextOnImage(im, pc_patch, pc)
                        cv2.addWeighted(im, 0.4, im_copy, 0.6, 0, im_copy)
                        if image_file_prefix is not None and image_link_prefix is not None:
                            skio.imsave(image_file_prefix + '_tagged.jpg', im_copy)
                            source_res["tagged_img"] = image_link_prefix + '_tagged.jpg'
                    elif pc not in ignore_list and pp > 0.1:
                        source_res["pred_cats"].append(pc)

                if not strong_pred:
                    if len(source_res["pred_cats"]) == 0 and "person" not in source_res:
                        return None
                    source_res["noPose"] = pc_patch
            return source_res

        else:
            source_res = {}
            im_copy = im.copy()
            for pk, patch in patches.items():
                print("Classifying " + pk + " body patch.", file=sys.stderr)
                im_patch = im[patch["y1"]:patch["y2"], patch["x1"]:patch["x2"], :]
                if im_patch.shape[0] * im_patch.shape[1] == 0:
                    continue
                with self.catnetlock:
                    prediction = self.model.infer(self.tx.preprocess(im_patch))
                pcat, probs = self.rank_classes(prediction)

                for pc, pp in zip(pcat, probs):
                    if (pk == self.posemap[pc] or pk == "noPose") and pp > 0.10:
                        if gender is not None and gender == "men" and pc in self.femalecat_list:
                            continue
                        if gender is not None and gender in ["men", "women"]:
                            patch["gender"] = gender
                        #                        patch['prob'] = pp
                        source_res[pc] = patch
                        addTextOnImage(im_copy, patch, pc)
                        break
            if image_file_prefix is not None and image_link_prefix is not None:
                cv2.addWeighted(im, 0.6, im_copy, 0.4, 0, im_copy)
                skio.imsave(image_file_prefix + '_tagged.jpg', im_copy)
                source_res["tagged_img"] = image_link_prefix + '_tagged.jpg'
            return source_res
