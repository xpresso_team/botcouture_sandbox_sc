from configobj import ConfigObj
import numpy as np


def pose_config_reader(data_dir = ""):
    config = ConfigObj(data_dir + 'RheC_RTMPPose_config')

#    print(config)
    model_param = config['param']
    model_id = model_param['modelID']
    model_param['model'] = config['models'][model_id]
    model = model_param['model']
    model['boxsize'] = int(model['boxsize'])
    model['stride'] = int(model['stride'])
    model['padValue'] = int(model['padValue'])
    #model_param['starting_range'] = float(model_param['starting_range'])
    #model_param['ending_range'] = float(model_param['ending_range'])
    model_param['octave'] = int(model_param['octave'])
    model_param['starting_range'] = float(model_param['starting_range'])
    model_param['ending_range'] = float(model_param['ending_range'])
    model_param['scale_search'] = map(float, model_param['scale_search'])
    model_param['thre1'] = float(model_param['thre1'])
    model_param['thre2'] = float(model_param['thre2'])
    model_param['thre3'] = float(model_param['thre3'])
    model_param['mid_num'] = int(model_param['mid_num'])
    model_param['min_num'] = int(model_param['min_num'])
    model_param['crop_ratio'] = float(model_param['crop_ratio'])
    model_param['bbox_ratio'] = float(model_param['bbox_ratio'])

    return model_param

if __name__ == "__main__":
    pose_config_reader()
