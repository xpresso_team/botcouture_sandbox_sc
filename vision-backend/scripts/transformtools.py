import numpy as np
import scipy.misc
import cv2


class SimpleTransformer:

    """
    SimpleTransformer is a simple class for preprocessing and deprocessing
    images for caffe.
    """

    def __init__(self, mean=[128, 128, 128], im_shape=[224,224]):
        self.mean = np.array(mean, dtype=np.float32)
        self.scale = 1.0
        self.im_shape = im_shape

    def set_mean(self, mean):
        """
        Set the mean to subtract for centering the data.
        """
        self.mean = mean

    def set_scale(self, scale):
        """
        Set the data scaling.
        """
        self.scale = scale

    def preprocess(self, im_orig, bbox=None):
        """
        preprocess() emulate the pre-processing occurring in the vgg16 caffe
        prototxt.
        """
        if len(im_orig.shape) == 3 and im_orig.shape[2] == 4:
            im_orig = im_orig[:,:,:3]
        if bbox:
            im = im_orig[bbox[1]:bbox[3],bbox[0]:bbox[2],:]
            print(bbox)
        else:
            im = im_orig
        im = scipy.misc.imresize(im, self.im_shape)
        im = np.float32(im)
        im = im[:, :, ::-1]  # change to BGR
        im -= self.mean
        im *= self.scale
        im = im.transpose((2, 0, 1))

        return im

    def deprocess(self, im):
        """
        inverse of preprocess()
        """
        im = im.transpose(1, 2, 0)
        im /= self.scale
        im += self.mean
        im = im[:, :, ::-1]  # change to RGB

        return np.uint8(im)


class CaffeSolver:

    """
    Caffesolver is a class for creating a solver.prototxt file. It sets default
    values and can export a solver parameter file.
    Note that all parameters are stored as strings. Strings variables are
    stored as strings in strings.
    """

    def __init__(self, testnet_prototxt_path="testnet.prototxt",
                 trainnet_prototxt_path="trainnet.prototxt", debug=False):

        self.sp = {}

        # critical:
        self.sp['base_lr'] = '0.001'
        self.sp['momentum'] = '0.9'

        # speed:
        self.sp['test_iter'] = '100'
        self.sp['test_interval'] = '250'

        # looks:
        self.sp['display'] = '25'
        self.sp['snapshot'] = '2500'
        self.sp['snapshot_prefix'] = '"snapshot"'  # string within a string!

        # learning rate policy
        self.sp['lr_policy'] = '"fixed"'

        # important, but rare:
        self.sp['gamma'] = '0.1'
        self.sp['weight_decay'] = '0.0005'
        self.sp['train_net'] = '"' + trainnet_prototxt_path + '"'
        self.sp['test_net'] = '"' + testnet_prototxt_path + '"'

        # pretty much never change these.
        self.sp['max_iter'] = '100000'
        self.sp['test_initialization'] = 'false'
        self.sp['average_loss'] = '25'  # this has to do with the display.
        self.sp['iter_size'] = '1'  # this is for accumulating gradients

        if (debug):
            self.sp['max_iter'] = '12'
            self.sp['test_iter'] = '1'
            self.sp['test_interval'] = '4'
            self.sp['display'] = '1'

    def add_from_file(self, filepath):
        """
        Reads a caffe solver prototxt file and updates the Caffesolver
        instance parameters.
        """
        with open(filepath, 'r') as f:
            for line in f:
                if line[0] == '#':
                    continue
                splitLine = line.split(':')
                self.sp[splitLine[0].strip()] = splitLine[1].strip()

    def write(self, filepath):
        """
        Export solver parameters to INPUT "filepath". Sorted alphabetically.
        """
        f = open(filepath, 'w')
        for key, value in sorted(self.sp.items()):
            if not(type(value) is str):
                raise TypeError('All solver parameters must be strings')
            f.write('%s: %s\n' % (key, value))

# Default parameters are for VGG net
# Input: Height x Width x Channel                                  
# Output: #Sample x Channel x Height x Width
def transform_image(img, over_sample = False, mean_pix = [103.939, 116.779, 123.68], image_dim = 256, crop_dim = 224):
  # convert to BGR
  if len(img.shape) < 3 or img.shape[2] == 1:                      
    img = cv2.cvtColor(img, cv2.cv.CV_GRAY2BGR)                    
  # resize image, the shorter side is set to image_dim
  if img.shape[0] < img.shape[1]:                                  
    # Note: OpenCV uses width first...                             
    dsize = (int(np.floor(float(image_dim)*img.shape[1]/img.shape[0])), image_dim)  
  else:
    dsize = (image_dim, int(np.floor(float(image_dim)*img.shape[0]/img.shape[1])))      
  img = cv2.resize(img, dsize, interpolation=cv2.INTER_CUBIC)      
  
  # convert to float32
  img = img.astype(np.float32, copy=False)                         
  
  if over_sample:
    imgs = np.zeros((10, crop_dim, crop_dim, 3), dtype=np.float32) 
  else:
    imgs = np.zeros((1, crop_dim, crop_dim, 3), dtype=np.float32)

  # crop
  indices_y = [0, img.shape[0]-crop_dim]
  indices_x = [0, img.shape[1]-crop_dim]
  center_y = np.floor(indices_y[1]/2)
  center_x = np.floor(indices_x[1]/2)

  imgs[0] = img[center_y:center_y+crop_dim, center_x:center_x+crop_dim, :]
  if over_sample:
    curr = 1
    for i in indices_y:
      for j in indices_x:
        imgs[curr] = img[i:i+crop_dim, j:j+crop_dim, :]
        imgs[curr+5] = imgs[curr, :, ::-1, :]
        curr += 1
    imgs[5] = imgs[0, :, ::-1, :]

  # subtract mean
  for c in range(3):
    imgs[:, :, :, c] = imgs[:, :, :, c] - mean_pix[c]
  # reorder axis
  return np.rollaxis(imgs, 3, 1)                                                                                                      


