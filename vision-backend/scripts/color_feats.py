import sys
import os
import io
import urllib

import numpy as np
import os.path as osp
import matplotlib.pyplot as plt

from copy import copy
import csv
import scipy.misc

import time
from skimage import img_as_ubyte
import skimage.io as skio
import scipy.misc as scm
import PIL.Image as PILim
import math
from sklearn.neighbors import KDTree
import cv2


class ColorFeats(object):
    def __init__(self, params=None):
        """
        initialize Color feature extractor and parameters
        """
        if params is not None and "nbins" in params:
            self.nbins = params["nbins"]
        else:
            self.nbins = 10
        self.histsize = self.nbins * 3
        self.imWidth = 200

    def getColorHist(self, im, bbox=None, mask=None):
        """
        compute color histogram on predefined parameters and return
        """
        if bbox is None:
            bbox = []
        if isinstance(im, str):
            if "http" in im:
                file = io.BytesIO(urllib.urlopen(im).read())
            else:
                file = im
            im = np.array(PILim.open(file))
            if len(im.shape) == 3 and im.shape[2] == 4:
                im = im[:, :, :3]
        elif isinstance(im, np.ndarray):
            file = im
        else:
            print("incorrect image argument")
            return np.array([])
        if bbox:
            im = im[bbox[1]:bbox[3], bbox[0]:bbox[2], :]
        imHeight = int(self.imWidth * im.shape[0] * 1.0 / im.shape[1])

        if mask is not None:
            assert (mask.shape[:2] == im.shape[:2])
            mask = cv2.resize(mask, (self.imWidth, imHeight))
        rgb = cv2.resize(im, (self.imWidth, imHeight))
        rgbrange = [[0, 255], [0, 255], [0, 255]]
        coloured_hist = self.getHist(rgb, rgbrange, mask)
        hsv = cv2.cvtColor(rgb, cv2.COLOR_BGR2HSV)
        hsvrange = [[0, 180], [0, 255], [0, 255]]
        coloured_hist = np.concatenate((coloured_hist, self.getHist(hsv, hsvrange, mask)), axis=1)
        lab = cv2.cvtColor(rgb, cv2.COLOR_BGR2Lab)
        labrange = [[0, 255], [0, 255], [0, 255]]
        coloured_hist = np.concatenate((coloured_hist, self.getHist(lab, labrange, mask)), axis=1)
        return coloured_hist

    def getHist(self, imcs, ranges, mask=None):
        """
        split channels and compute hist
        """
        if len(imcs.shape) == 2:
            channels = 1
        elif len(imcs.shape) == 3:
            channels = imcs.shape[2]
        hist = []
        for i in range(channels):
            temphist = cv2.calcHist([imcs], [i], mask, [self.nbins], ranges[i])
            hist.append(cv2.normalize(temphist, temphist))
        return np.array(hist).reshape((1, self.histsize))

# """
# Usage:::


# temp = ColorFeats()
# imUrl = "http://media1.modcloth.com/community_outfit_image/000/000/010/330/img_462w_29c62613ad56.jpg"
# bbox = [ 76, 269, 348, 472 ]
# print(type(imUrl))
# print(isinstance(imUrl,str))
# print( temp.getColorHist(imUrl, bbox).shape )


# """
