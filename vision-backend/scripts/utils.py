"""
    Contains utility methods
"""
import hashlib
import random
import string

import numpy as np


def _print_info_str(obj, name):
    if obj is None:
        return '{} type: {}'.format(name, type(obj))
    else:
        if isinstance(obj, list) and len(obj) > 0:
            return '{} type: {}, length: {}, {}'.format(name, type(obj), len(obj),
                                                        _print_info_str(obj[0], name + '[0]'))
        elif isinstance(obj, np.ndarray):
            return '{} type: {}, shape: {}, min: {}, max: {}, mean: {}, std: {}' \
                .format(name, type(obj), obj.shape, np.min(obj), np.max(obj), np.mean(obj), np.std(obj))
        else:
            return '{} type: {}, value: {}'.format(name, type(obj), obj)


def print_info(obj, name):
    print(_print_info_str(obj, name))


def generate_random_word(length):
    return ''.join(random.choice(string.lowercase) for i in range(length))


# generates md5 hash of a file
def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def value_or_default(value, default_value):
    return default_value if value is None else value
