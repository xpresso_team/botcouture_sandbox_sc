import numpy as np
import cv2
import caffe


class Transformer:
    def __init__(self, params):
        """
        # Default parameters are for VGG net
        :param params: {'mean':  [103.939, 116.779, 123.68],  #default vgg net
                        'image_dim': 256,
                        'crop_dim': 224,
                        'oversample': False}
                        
        """
        self.mean = [103.939, 116.779, 123.68]
        if 'mean' in params:
            if isinstance(params['mean'],str):
                proto_data = open(params['mean'], "rb").read()
                blob = caffe.proto.caffe_pb2.BlobProto()
                blob.ParseFromString(proto_data)
                self.mean = np.array(caffe.io.blobproto_to_array(blob))
            elif isinstance(params['mean'],list):
                self.mean = params['mean']
        self.image_dim = 256
        if 'image_dim' in params:
            self.image_dim = params['image_dim']
        self.crop_dim = 224
        if 'crop_dim' in params:
            self.crop_dim = params['crop_dim']
        self.oversample = False
        if 'oversample' in params:
            self.oversample = params['oversample']


    def preprocess(self,img):
        """
        # Input: Height x Width x Channel 
        
        :return:#Sample x Channel x Height x Width
        """


        # convert to BGR
        if len(img.shape) < 3 or img.shape[2] == 1:
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        if isinstance(self.mean,list):
            # resize image, the shorter side is set to image_dim
            if img.shape[0] < img.shape[1]:
            # Note: OpenCV uses width first...
                dsize = (int(np.floor(float(self.image_dim)*img.shape[1]/img.shape[0])), self.image_dim)
            else:
                dsize = (self.image_dim, int(np.floor(float(self.image_dim)*img.shape[0]/img.shape[1])))
            img = cv2.resize(img, dsize, interpolation=cv2.INTER_CUBIC)
        else:
            dsize = (self.image_dim, self.image_dim)
            img = cv2.resize(img, dsize, interpolation=cv2.INTER_CUBIC)
            img = img - np.transpose(self.mean[0,:,:,:], (1,2,0))


  # convert to float32
        img = img.astype(np.float32, copy=False)

        if self.oversample:
            imgs = np.zeros((10, self.crop_dim, self.crop_dim, 3), dtype=np.float32)
        else:
            imgs = np.zeros((1, self.crop_dim, self.crop_dim, 3), dtype=np.float32)

  # crop
        indices_y = [0, img.shape[0]-self.crop_dim]
        indices_x = [0, img.shape[1]-self.crop_dim]
        center_y = int(np.floor(indices_y[1]/2))
        center_x = int(np.floor(indices_x[1]/2))

        imgs[0] = img[center_y:center_y+self.crop_dim, center_x:center_x+self.crop_dim, :]
        if self.oversample:
            curr = 1
            for i in indices_y:
                for j in indices_x:
                    imgs[curr] = img[i:i+self.crop_dim, j:j+self.crop_dim, :]
                    imgs[curr+5] = imgs[curr, :, ::-1, :]
                    curr += 1
            imgs[5] = imgs[0, :, ::-1, :]

        # subtract mean
        if isinstance(self.mean,list):
            for c in range(3):
                imgs[:, :, :, c] = imgs[:, :, :, c] - self.mean[c]
            imgs = np.rollaxis(imgs, 3, 1)
        else:
            imgs = np.rollaxis(imgs, 3, 1)
            # reorder axis
        return imgs
