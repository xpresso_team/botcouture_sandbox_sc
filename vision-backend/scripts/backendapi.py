from __future__ import print_function

import base64
import binascii
import hashlib
import logging.handlers
import os
import stat
import sys
import time
import traceback
import urllib

import requests_cache
import ubjson
from flask import Flask
from flask import redirect
from flask import request
from flask import jsonify
from flask import send_from_directory
from flask_caching import Cache
from werkzeug.utils import secure_filename
import numpy as np
import skimage.io as skio
import cv2

import utils
from visual_search import VisualSearch
from flasgger import Swagger

################### SETTING UP APP CONFIG ################################
# Initialize flask app and fetch config variables
app = Flask(__name__)

app.config['SWAGGER'] = {
    "swagger_version": "2.0",
    "info": {
        "title": "Vision backend api documentation",
        "description": "This is the documentation for the vision backend service. This documentation allows you to "
                       "check the various api that are built.",
    }
}

swagger = Swagger(app)

app.config.from_envvar("VISIONBACKENDSERVICE_CONFIG")
app.config['STATIC_FOLDER'] = 'static'
app.config['UPLOAD_FOLDER'] = './uploads/'

requests_cache.install_cache('vs_backend cache')
cache = Cache(app, config={'CACHE_TYPE': 'simple'})

# GPU_DEVICE_NO = gpu device on which the CNNs must compute
gpu_device_no = app.config.get('GPU_DEVICE_NO', 0)
print('gpu device setting: #{}'.format(gpu_device_no))

# DEPLOY_MODE = production/ashley/experimental
if "DEPLOY_MODE" in app.config:
    app_DEPLOY_MODE = app.config['DEPLOY_MODE'] + " Backend"
else:
    app_DEPLOY_MODE = "unknown deployment"

# EXPOSED_PORT = port exposed for flask service
if "EXPOSED_PORT" in app.config:
    exposed_port = app.config['EXPOSED_PORT']
else:
    exposed_port = 5003

# Create the upload folder if does not exist   
if not os.path.exists(app.config['UPLOAD_FOLDER']):
    os.makedirs(app.config['UPLOAD_FOLDER'])

pref = app.config['DATAURL_PREFIX']

# Logger setup
logger = logging.getLogger('myLogger')
logger.setLevel(logging.INFO)
# add handler to the logger
handler = logging.handlers.SysLogHandler('/dev/log')
# add syslog format to the handler
formatter = logging.Formatter(
    'Vision ' + app_DEPLOY_MODE + ': { "loggerName":"%(name)s", "asciTime":"%(asctime)s", "pathName":"%(pathname)s", '
                                  '"logRecordCreationTime":"%(created)f", "functionName":"%(funcName)s", '
                                  '"levelNo":"%(levelno)s", "lineNo":"%(lineno)d", "time":"%(msecs)d", '
                                  '"levelName":"%(levelname)s", "message":"%(message)s"}')
handler.formatter = formatter
logger.addHandler(handler)
#

# to handle user agent issue for urllib.urlretrieve
urllib.URLopener.version = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11'

visual_search = VisualSearch(app.config, device_no=gpu_device_no)


# For a given file, return whether it's an allowed type or not
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ['jpg', 'jpeg', 'png', 'bmp']


def getImageFromRequest(request):
    """
    Single function that can handle "image_url" as GET request, or image upload via "file" as POST argument
    """
    new_file_path = None
    if request.method == 'GET' and 'image_url' in request.args:
        request_imurl = urllib.unquote(str(request.args.get('image_url')))
        print("query image url args- " + ", ".join([k + ": " + request.args.get(k) for k in request.args]),
              file=sys.stderr)
        logger.info("query image url args- " + ", ".join([k + ": " + request.args.get(k) for k in request.args]))
        if '.png' in request_imurl:
            extension = '.png'
        elif '.tif' in request_imurl:
            extension = '.tif'
        else:
            extension = '.jpg'
        m = hashlib.md5()
        m.update(request_imurl + str(time.time()))
        upkeyID = m.hexdigest()
        image_file = str(app.config['UPLOAD_FOLDER'] + upkeyID + extension)
        try:
            print("Request image url - " + request_imurl + " saving to file - " + image_file, file=sys.stderr)
            urllib.urlretrieve(request_imurl.strip('\r\n\t '), image_file)
            os.chmod(image_file, stat.S_IRUSR | stat.S_IWUSR)
            uploaded_file = image_file  # str(request.args.get('image_url'))
            new_file_path = uploaded_file
            print("New file path is:" + new_file_path, file=sys.stderr)
        except:
            traceback.print_exc()
        return new_file_path
    elif request.method == 'POST' and 'file' in request.files:
        uploaded_file = request.files['file']
        print("Uploaded file name - " + uploaded_file.filename, file=sys.stderr)

        if uploaded_file and allowed_file(uploaded_file.filename):
            filename = secure_filename(uploaded_file.filename)
            if '.png' in filename:
                extension = '.png'
            elif '.tif' in filename:
                extension = '.tif'
            else:
                extension = '.jpg'
            m = hashlib.md5()
            m.update(str(filename) + str(time.time()))
            upkeyID = m.hexdigest()
            new_file_path = os.path.join(app.config['UPLOAD_FOLDER'], upkeyID + extension)
            print("bot_vsu query image upload POST args- uploaded file:" + new_file_path, file=sys.stderr)
            logger.info("bot_vsu query image upload POST args- uploaded file:" + new_file_path)
            uploaded_file.save(new_file_path)
            os.chmod(new_file_path, stat.S_IRUSR | stat.S_IWUSR)
        return new_file_path
    elif request.method == 'POST':
        post_data = request.get_data(parse_form_data=True)
        print(post_data)
        try:
            filename = utils.generate_random_word(32) + str(int(time.time()))
            m = hashlib.md5()
            m.update(filename + str(time.time()))
            upkeyID = m.hexdigest()
            new_file_path = os.path.join(app.config['UPLOAD_FOLDER'], upkeyID)
            base64str = base64.decodestring(post_data[post_data.find(",") + 1:])
            with open(new_file_path, "wb") as f:
                f.write(base64str)
                f.close()
            os.chmod(new_file_path, stat.S_IRUSR)
            logger.info("bot_vsu query image upload POST base64- uploaded file:" + new_file_path)
        except binascii.Error:
            traceback.print_exc()
            pass
        return new_file_path


@cache.cached(timeout=36000)
@app.route('/feats/', methods=['GET', 'POST'])
def getfeats():
    """
    API for fetching item and categories pose predicted in the image
    ---
    tags:
        -feats
    parameters:
        - name : image_url
          description : GET method
          in : query
          required : false
          type : string

        - name : file
          description : POST method
          in : formData
          required : false
          type : file
    produces :
        - application/json
    responses:
        200:
            description : successful operation
    """
    im_path = getImageFromRequest(request)
    if "json" in request.args:
        jsontype = str(request.args.get("json"))
    else:
        jsontype = "binary"

    bbox = None
    if 'bbox' in request.args:
        bbox = [int(x) for x in request.args.get('bbox').split(':')]
        if not all(isinstance(i, int) for i in bbox):
            status_msg = "Invalid bbox argument. Need integers separated by ':' ex- x1:y1:x2:y2"

    feats = visual_search.get_feats(im_path, bbox)
    if bbox is None:
        bbox = []
    fpatches = {"noPose": {"feats": feats}}

    if jsontype == "binary":
        return ubjson.dumpb(fpatches)
    else:
        return jsonify(fpatches)


@cache.cached(timeout=36000)
@app.route('/itemsandfeats/', methods=['GET', 'POST'])
def itemsandfeats():
    """
    API for fetching item and categories pose predicted in the image
    ---
    tags:
        -itemsandfeats
    parameters:
        - name : image_url
          description : GET method
          in : query
          required : false
          type : string

        - name : file
          description : POST method
          in : formData
          required : false
          type : file
    produces :
        - application/json
    responses:
        200:
            description : successful operation
    """
    im_path = getImageFromRequest(request)
    if "json" in request.args:
        jsontype = str(request.args.get("json"))
    else:
        jsontype = "binary"
    if im_path is None:
        return ubjson.dumpb({"status_code": -1, "status_msg": "Image URL not valid"})
    qgender = Nonecv2
    qcat = None
    toSegment = True
    if "gender" in request.args:
        qgender = str(request.args.get("gender"))
        qgender = None if qgender == "" else qgender
    if "qcat" in request.args:
        qcat = str(request.args.get("qcat"))
        qcat = None if qcat == "" else qcat
    if "toSegment" in request.args:
        toSegment = str(request.args.get("toSegment"))
        toSegment = True if toSegment == "1" else False

    items_result = visual_search.items_and_feats(im_path, qgender=qgender, qcat=qcat, toSegment=toSegment,
                                                 image_link_prefix=pref)
    if jsontype == "binary":
        return ubjson.dumpb(items_result)
    else:
        return jsonify(items_result)


@app.route('/data/')
def get_image_data():
    """
    get dynamic image data
    """
    url = ""
    if 'url' in request.args:
        url = request.args.get('url')

    try:
        split_url = url.split('/')
        last_name = split_url[-1]
        folder_new = '/'.join(split_url[:-1])
        return send_from_directory(folder_new, last_name)
    except:
        traceback.print_exc()
        return redirect(url)


@cache.cached(timeout=36000)
@app.route('/humanpose/', methods=['GET', 'POST'])
def humanpose():
    """
    API for fetching human pose predicted in the image
    ---
    tags:
        -humanpose
    parameters:
        - name : image_url
          description : GET method
          in : query
          required : false
          type : string

        - name : file
          description : POST method
          in : formData
          required : false
          type : file
    produces :
        - application/json
    responses:
        200:
            description : successful operation
    """
    im_path = getImageFromRequest(request)
    if im_path is None:
        return jsonify({"status_code": -1, "status_msg": "Image not valid"})
    pose_result = visual_search.pose_extractor.get_pose(im_path)
    return jsonify({"results": list(pose_result)})


@cache.cached(timeout=36000)
@app.route('/segmentfashion/', methods=['GET', 'POST'])
def segmentfashion():
    """
    API for fetching human pose predicted in the image
    ---
    tags:
        -segmentfashion
    parameters:
        - name : image_url
          description : GET method
          in : query
          required : false
          type : string

        - name : file
          description : POST method
          in : formData
          required : false
          type : file
    produces :
        - application/json
    responses:
        200:
            description : successful operation
    """
    im_path = getImageFromRequest(request)
    if im_path is None:
        return jsonify({"status_code": -1, "status_msg": "Image not valid"})
    im = skio.imread(im_path)
    im = cv2.resize(im, (300, 400))
    skio.imsave(im_path, im)
    gender = None
    if "gender" in request.args:
        gender = str(request.args.get("gender"))
        if gender not in ["men", "women"]:
            gender = None
    seg_prediction = visual_search.segmentor.semantic_segfcn(im_path, gender=gender)
    seg_prediction = seg_prediction - 1
    seg_prediction_ch = np.zeros((seg_prediction.shape[0], seg_prediction.shape[1], 4), dtype=np.uint8)
    ch1 = ch2 = np.zeros(seg_prediction.shape, dtype=np.uint8)
    ch3 = np.ones(seg_prediction.shape, dtype=np.uint8) * 255
    seg_prediction_ch[:, :, 0] = seg_prediction
    seg_prediction_ch[:, :, 1] = ch1
    seg_prediction_ch[:, :, 2] = ch2
    seg_prediction_ch[:, :, 3] = ch3

    im_path_split = im_path.rsplit('.', 1)
    im_seg_path = im_path_split[0] + '_seg.png'
    print(seg_prediction.shape, file=sys.stderr)
    skio.imsave(im_seg_path, seg_prediction_ch)
    ret_res = {}
    ret_res = {"segmented_image_url": pref + im_seg_path}
    return jsonify({"results": ret_res})


if __name__ == '__main__':
    app.run(host='0.0.0.0',
            port=exposed_port,
            threaded=True,
            debug=False)
