var _            = require( 'lodash'     )
var kafka        = require( 'kafka-node' )
var nconf        = require( 'nconf'      )

var Producer     = kafka.Producer
var KeyedMessage = kafka.KeyedMessage
// var client       = new kafka.Client( '52.44.255.113:2181' )
// var client       = new kafka.Client( '34.203.102.251:2181' )
var client       = new kafka.Client( nconf.get( "KAFKA_HOST" ) )
var producer     = new Producer( client )

/*
var km       = new KeyedMessage( 'key', 'message' )
var payloads = [
    { topic: 'ashleyexp', messages: { a : 1, b : { c : 3, d : { e : 4 } } }, partition: 0 },
    { topic: 'ashleyexp', messages: ['hello', 'world', km] }
]
//var payloads = [{ topic: 'ashleyexp', messages: JSON.stringify( { a : 1, b : { c : 3, d : { e : 4 } } } ), partition: 0 }]
*/

client.once( 'connect', () => {
  console.log('Kafka Client Ready')
  /*
  client.loadMetadataForTopics( [], ( err, results ) => {
  if ( err ) {
    console.log('_ERROR 1_')
    return console.error(err)
  }
  console.log('RESULT_-')
  console.log('%j', _.get(results, '1.metadata'));
})
*/
})

producer.on( 'ready', () => {
  console.log( 'Kafka Producer Ready' )
  /*
  producer.send( payloads, (err, data) => {
    if ( err ) {
      console.log( 'Error while sending data ', err )
      return
    }
    console.log('ready data')
    console.log(data)
  })
  */
})
 
producer.on( 'error', err => console.log( 'Kafka Producer ERROR', err ) )
client.on(   'error', err => console.log( 'Kafka Client ERROR'  , err ) )

/**
 * Send incoming user message if not sent
 */
sendIncomingMessage = session => {
   
  // TODO: Think of a better way
  // This hack is for premptive message sent by Feedback message timeout
  if ( _.has( session, 'userData.analytics.in' ) ) {
    if ( ! session.userData.analytics.in.timestamp ) {
      delete session.userData.analytics.in
    }
  }

  if ( _.has( session, 'userData.analytics.in' ) ) {
    let event = session.userData.analytics.in
    event.session_id = session.userData.session_id
    event.message_number = ++session.userData.message_number
    console.log( 'Kafka Sending In message-> ', event )
    // Send to Kafka
    let payload = [ { topic: nconf.get( "KAFKA_TOPIC" ), messages: JSON.stringify( event ), partition: 0 } ]
    if(nconf.get( "kafka_status" )){
    producer.send( payload, ( err, data ) => {
      if ( err ) {
        console.log( 'error', err )
        return
      }
    })
    }
    delete session.userData.analytics.in
  }
}

sendOutgoingMessage = ( session, event ) => {

  sendIncomingMessage( session )

  // Send to Kafka
  event.message_number = ++session.userData.message_number
  console.log( 'Kafka Sending OUT message-> ', event )

  let payload = [ { topic: nconf.get( "KAFKA_TOPIC" ), messages: JSON.stringify( event ), partition: 0 } ]
  if(nconf.get( "kafka_status" )){
  producer.send( payload, ( err, data ) => {
    if ( err ) {
      console.log( 'error', err )
      return
    }
  })
  }

}

module.exports = {
  // producer : producer,
  sendIncomingMessage : sendIncomingMessage,
  sendOutgoingMessage : sendOutgoingMessage 
}
