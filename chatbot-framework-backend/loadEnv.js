/**
 * Load values from config files
 */
var _     = require( 'lodash' )
var hjson = require( 'hjson'  )
var nconf = require( 'nconf'  )

nconf.use( 'memory' )
nconf.argv().env()

let confName = nconf.get( 'conf' ).trim()

if ( _.includes( [ 'JeanieExp','dev','dev_EXP','dev_Prod','dev_SS', 'exp', 'prod' ], confName ) ) {
	console.log( 'Loading: ./config/' + confName + '.json' )

  nconf.add( 'file1', { file : './config/' + confName + '.json', type: 'file', format : hjson } );
	//console.log( 'The config is ' )
  //console.log( nconf )

} else {

	console.log( `
  Usage:
  On linux
  To Start :- npm start
  To Stop :-  npm stop
  To check status :- npm run status
  
  On Windows
    npm run bot-exp
    - OR -
    npm run bot-prod
    - OR -
    node MultiChannelChatBot --conf prod|exp|dev` )

  process.exit( 0 )

}

// Defaults are loaded last as values are not overwritten
// i.e. older values win
nconf.add( 'file', { type: 'file', file : './config/defaults.json', format : hjson } )
nconf.load()
