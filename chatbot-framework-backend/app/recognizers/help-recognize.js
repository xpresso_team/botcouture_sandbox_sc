/**
 * Created by aashish_amber on 13/3/17.
 */
var request    = require( 'superagent'             )
var Constants  = require( '../constants'           )
var ApiService = require( '../services/ApiService' )

module.exports = {

  recognize : ( context, callback ) => {


    let text = context.message.text
    let flag = 0
    // console.log( text )

    // Find help
    // Make a wit.ai call
    ApiService.getEmotion( text,context )
    .then( emotionJSON => {
      console.log("\n this is where it is going on\n")


      //if ((emotionJSON) && (emotionJSON.entities) && (emotionJSON.entities.intent) && (emotionJSON.entities.intent.length > 0) && (emotionJSON.entities.intent[0].value)) {
      if ((emotionJSON) && (emotionJSON.intent) && (emotionJSON.intent.name)) {

        // console.log(emotionJSON.entities.intent[0].value)
          var type
        //switch ( emotionJSON.entities.intent[0].value ) {
        switch ( emotionJSON.intent.name ) {
          case "Help" :
            var intent = {
              intent : "HELP_ASK",
              score : 1,
              entities : [ { entity : text, entityType: 'HELP', data: { emotionJSON : emotionJSON } } ]
            }
            flag=1
              type ="Help"
            console.log("flag is "+flag)
            break;
          case "customer_support" :
              intent = {
                  intent: 'CUSTOMER-QUERY-RESPONSE',
                  score: 1,
                  entities: [{
                      data: {text :text }
                  }]
              }
              flag=1
              type ="customer_support"
              break;
        }
        if ( flag ) {
          console.log( 'HelpRecognizer -> '+type )

          // ANALYTICS
          context.userData.analytics.in.message_type_flag = 'IN'
          callback( null, intent )
          return
        } else {
          console.log( 'HelpRecognizer <-' )
          console.log("\n this is where it is going on\n")
          callback.call( null, null, { intent : null, score : 0 } );
          console.log("\n this is where it is going on\n")
          return
        }

      } else {
        console.log( 'HelpRecognizer <-' )
              console.log("\n this is where it is going on second\n")
        callback.call( null, null, { intent : null, score : 0 } );
        return
      }
    })
    .catch( err => {
      // console.log( err )
      console.log( 'HelpRecognizer <-' )
      callback( err, null )
    })

  }
}
