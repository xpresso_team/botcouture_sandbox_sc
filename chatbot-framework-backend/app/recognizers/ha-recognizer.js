var request   = require( 'superagent'  )
var Constants = require( '../constants' )
var url       = require( 'url' )
var _         = require( 'lodash' )
module.exports = {

    recognize : function( context, callback ) {




        let user_type
        let info = { data : { user_type : '',msg : '', user_id:'',agent_id:'' }}
        let intent2
        if (_.has(context, 'userData.status')) {
            user_type = context.userData.status
        }

        //console.log("user_type is ",user_type)
        //console.log("user data is ",JSON.stringify(context.userData))


        let flag

        if (user_type == "user") {
            if((!context.userData.user_wait_queue)&&(!context.userData.user_engaged_queue.wait)&&(context.userData.user_engaged_queue.flag)){
                info.data.agent_id= context.userData.user_engaged_queue.id
                    info.data.msg=context.message.text
                        info.data.user_id= context.message.address.user.id
                            info.data.user_type = user_type
                flag= true
                intent2 = 'HA.TEXT.MESSAGE'

            }else if ((!context.userData.user_wait_queue)&&(context.userData.user_wait_queue_accepted)){

                console.log("user wait accept resend")
                flag= true
                intent2='USER.WAIT.SEND'

            }else if ((!context.userData.user_wait_queue)&&(context.userData.user_engaged_queue.wait)){

                console.log("user match accept resend")
                flag= true
                intent2='USER.ACCEPT.SEND'
                console.log(" out intent is ",intent2)

            }

        }else if(user_type == "agent") {

            if((!context.userData.agent_wait_queue)&&(!context.userData.user_engaged_queue.wait)&&(context.userData.user_engaged_queue.flag)) {

                info.data.agent_id = context.message.address.user.id
                info.data.msg = context.message.text
                info.data.user_id = context.userData.user_engaged_queue.id
                info.data.user_type = user_type
                flag= true
                intent2 = 'HA.TEXT.MESSAGE'
            }

        }

        //console.log(" out intent is ",intent2)


        if ( flag ) {

            let intent = {
                intent   : intent2,
                score    : 1,
                entities : [info]
            }
            callback( null, intent )

        } else {
            console.log('HA recognizer <-')
            callback( null, { intent : null, score : 0 } )
        }

    }
}