var _             = require( 'lodash'                             )
var request       = require( 'superagent'                         )
var Constants     = require( '../constants'                       )
var ApiService    = require( '../services/ApiService'             )
var TypeQuery     = require( '../dialogs/type_a_query/type-query' )
var StateService  = require( '../services/StateService'           )
var StateService2 = require( '../services/StateService2'          )

module.exports = {

  recognize : ( context, callback ) => {

    let query = context.message.text

    ApiService.getIntent( query, context )
    .then( result => {

      result = StateService2.sanitizeIntentResults( result )

      let nothing = ! result.entity
      nothing = nothing && ! ( result.price           && result.price.length           > 0 )
      nothing = nothing && ! ( result.subcategory     && result.subcategory.length     > 0 )
      nothing = nothing && ! ( result.features        && result.features.length        > 0 )
      nothing = nothing && ! ( result.brand           && result.brand.length           > 0 )
      nothing = nothing && ! ( result.details         && result.details.length         > 0 )
      nothing = nothing && ! ( result.colorsavailable && result.colorsavailable.length > 0 )
      nothing = nothing && ! ( result.gender          && result.gender.length          > 0 )
      nothing = nothing && ! ( result.size            && result.size.length            > 0 )
      nothing = nothing && ! ( result.xc_category     && result.xc_category.length     > 0 )

      if ( nothing ) {
        console.log( 'Intent-recognizer <-' )
        callback.call( null, null, { intent : null, score : 0 } )
        return
      } else {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
         * Maintain current state even in free form NLP query
         */
        let currentNode = null
        let state = null
        let resetState = null

        currentNode = context.userData.currentNode
        if ( ! currentNode ) {
          // Switch to default Node
          currentNode = Constants.NODE.NLP
        }
        let currentState = context.userData[ currentNode ] || {}


        result = _.omitBy( result, _.isNil )
        if ( result.entity ) {
          // Result has entity
          if ( currentState.entity && currentState.entity === result.entity ) {
            // State has entity and is same as result, i.e. entity didn't change
            // Don't change current Node, just update currentState
            state = result
            resetState = false
          } else {
            // State does not have entity or is different from results, i.e. entity changed
            // Switch to NLP by default and also reset state
            currentNode = Constants.NODE.NLP
            state = result
            resetState = true
          }
        } else {
          // Result does not have entity, apply filter on current state
          state = result
          resetState = false
        }

          let intent

          if(currentNode === Constants.NODE.COLLECTIONS){

              intent = {
                  intent: 'RESULTS.DIALOG',
                  score: 1,
                  entities: [{
                      entity: query,
                      entityType: 'FREE.FORM.TEXT',
                      data: {NODE: currentNode, resetState: resetState, state: state,query_type : "OLD"}
                  }]
              }


          }else {

              intent = {
                  intent: 'RESULTS.DIALOG',
                  score: 1,
                  entities: [{
                      entity: query,
                      entityType: 'FREE.FORM.TEXT',
                      data: {NODE: currentNode, resetState: resetState, state: state}
                  }]
              }
          }

        /**
         *  If NLP, continue older behaviour of sending to TYPE.A.QUERY dialog
         */
        if ( currentNode === Constants.NODE.NLP ) {
          /*intent = {
            intent : "TYPE.A.QUERY",
            score : 1,
            entities : [ { entity : query, entityType : 'FREE.FORM.TEXT', data : { userData : result } } ]
          }*/

            intent = {
                intent : "TYPE.A.QUERY.PROFILE",
                score : 1,
                entities : [ { entity : query, entityType : 'FREE.FORM.TEXT', data : { userData : result } } ]
            }

        }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /*
        intent = {
          intent : "TYPE.A.QUERY",
          score : 1,
          entities : [ { entity : query, entityType : 'FREE.FORM.TEXT', data : { userData : result } } ]
        }
        console.log( 'Intent-recognizer ->' )
        */
        console.log( 'Intent-recognizer ->' )
        // ANALYTICS
        context.userData.analytics.in.message_type_flag  = 'INQ'      // User input is a NLP Query
        context.userData.analytics.in.nlp_query_response = result    // NLP response
        context.userData.analytics.out.message_type_flag = 'ONR'

        callback( null, intent )
        return
      }

    })
    .catch( err => {
      console.log( err )
      console.log( 'Intent-recognizer <-' )
        let intent = {
            intent : "TYPE.A.QUERY",
            score : 1,
            entities : [ { entity : query, entityType : 'FREE.FORM.TEXT', data : { api_failed : true} } ]
        }
        console.log( 'Intent-recognizer ->' )
        callback( null, intent )
        return
      //callback( err, null )
    })

  }
}

