var _            = require( 'lodash'                   )
var Constants    = require( '../constants'             )
var StateService = require( '../services/StateService' )


// QUick reply buttons
module.exports = {
  
  recognize : function( context, callback ) {
    
    //let prompts = context.userData.propmpts
    //let prompts2 = StateService.getPrompts2( context )
    let text = context.message.text
      //console.log("Prompt  2 "+ text)

    // No active prompts2
    /*
    if ( !prompts2 || prompts2.length === 0 ) {
      callback( null, { intent : null, score : 0 } )
      return
    }
    */

    // Check each prompt2 then in reverse order
    // for(let i = 0, l = prompts2.length; i < l; i++ ) {
        console.log("entered Prompt recognizer")    
    let match = StateService.getMatchingPrompt( context, text )

    if ( match ) {
      let intent = {
        intent   : match.button.triggerIntent,
        score    : 1,
        entities : [ { entity : text, entityType : match.button.entityType, prompt2Id : match.prompt2Id, data : match.button.data } ]
      }
      console.log( 'Prompt TWO - Recognizer (' + match.button.triggerIntent + ') ->' )
      
      // ANALYTICS
      context.userData.analytics.in.message_type_flag = 'IQR'    // User input is a quick reply press
      
      callback( null, intent )
      return
    }

    /*
    for(let l = prompts2.length - 1; l >= 0; l-- ) {
      let prompt2 = prompts2[l]
      let match = prompt2.buttons.find( button => button.buttonText === text )
      if ( match ) {
        let intent = {
          intent   : match.triggerIntent,
          score    : 1,
          entities : [ { entity : text, entityType : match.entityType, prompt2Id : prompt2.id, data : match.data } ]
        }
        console.log( 'Prompt TWO - Recognizer (' + match.triggerIntent + ') ->' )
        callback( null, intent )
        return
      }
    }
    */

    // text does not match any prompt
    console.log('PromptTWO-Recognizer <-')
    callback( null, { intent : null, score : 0 } )

  }

}