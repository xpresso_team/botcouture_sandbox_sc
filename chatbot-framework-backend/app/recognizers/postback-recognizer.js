var Constants    = require( '../constants' )
var StateService = require( '../services/StateService' )

module.exports = {
  
  recognize : function( context, callback ) {
    
    let text = context.message.text
    console.log("\n"+text+"\n")    
    let regEx = matches = null


      /////// agent commands////
      regEx   = /@product/g
      matches = null
      matches = regEx.exec( text )
      if ((matches)&&(context.userData.status=="agent") ) {
          console.log("Match is ",matches)
          console.log("agent recommeding a product")
          var payload =  text.trim().split(/\s+/);
          //console.log(payload)
          //payload =  payload[1].trim().split(',');
          payload =  payload[1];
          //console.log(payload)
          //console.log('Profile->')
          context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
          callback( null, {
              intent   : 'PRODUCT.SUGGEST',
              score    : 1,
              // eg: { entity : skuId }
              entities : [{data :payload} ]
          })
          return
      }



      /////// other commands////


      //NLP RELAXATION
      regEx   = /RELAXATION::(.*)/g
      matches = null
      matches = regEx.exec( text )
      if ( matches ) {
          var attributes =  text.split("::");
          var attributes =  attributes[1]
          console.log('Relaxed attributes are for '+attributes)
          context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
          callback( null, {
              intent   : 'NLP-RELAXATION',
              score    : 1,
              // eg: { entity : skuId }
              entities : [ { entity : text, entityType : "RELAXATION", data : { attributes : attributes } } ]
          })
          return
      }




      // FAQ
      regEx   = /FAQ::(.*)/g
      matches = null
      matches = regEx.exec( text )
      if ( matches ) {
          var payload =  text.split("::");
          var FAQ = payload[1];
          console.log('FAQ for '+FAQ)
          context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
          callback( null, {
              intent   : 'FAQ',
              score    : 1,
              // eg: { entity : skuId }
              entities : [ { entity : text, entityType : payload[0], data : { FAQ : FAQ } } ]
          })
          return
      }

    /**
     * Postback buttons for Collection/DesignTrends/Gift Ideas on Inspiration Page
     * e.g: EXPLORE::collectionType::collectionName::gender
     */

    regEx   = /EXPLORE::(.*)::(.*)::(.*)::(.*)/g
    matches = null 
    matches = regEx.exec( text )
    if ( matches ) {
      console.log('Explore Collection Recognizer (Explore Collections) ->')
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      callback( null, {
        intent   : 'VIEW.COLLECTION.CATEGORY',
        score    : 1,
        entities : [ { entity : text, entityType : 'EXPLORE.INSPIRATION.TRIGGER', data : { collectionType : matches[1], collectionName : matches[2], gender : matches[3], client_name : matches[4] } } ]
      })
      return
    }


    regEx   = /EXPLORE::(.*)::(.*)::(.*)/g
    matches = null 
    matches = regEx.exec( text )
    if ( matches ) {
      console.log('SELECTING A SHOP AFTER LIST->')
      console.log("\ncollectionType:",matches[1])
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      callback( null, {
        intent   : 'SELECT-RETAILER',
        score    : 1,
        entities : [ { entity : text, entityType : 'SELECT-RETAILER-TRIGGER', data : { collectionType : matches[1], collectionName : matches[2], gender : matches[3] } } ]
      })
      return
    }


      // View Similar Postback buttons below items
      regEx   = /VIEWSIMILAR::(.*)::(.*)/g
      matches = null
      matches = regEx.exec( text )
      if ( matches ) {
          console.log('View Similar Recognizer (View Similar)->')
          context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
          callback( null, {
              intent   : 'VIEW.SIMILAR.ITEMS',
              score    : 1,
              entities : [ { entity : text, entityType : 'VIEW.SIMILAR.POSTBACK', data : { xc_sku : matches[1] } } ]
          })
          return
      }

      // View Similar Postback buttons below items
      regEx   = /VIEWSIMILAR::(.*)/g
      matches = null
      matches = regEx.exec( text )
      if ( matches ) {
          console.log('View Similar Recognizer (View Similar)->')
          context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
          callback( null, {
              intent   : 'VIEW.SIMILAR.ITEMS',
              score    : 1,
              entities : [ { entity : text, entityType : 'VIEW.SIMILAR.POSTBACK', data : { xc_sku : matches[1] } } ]
          })
          return
      }


      // WishList
      regEx   = /Wishlist/g
      matches = null
      matches = regEx.exec( text )
      if ( matches ) {
          console.log('Wishlist->')
          context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
          callback( null, {
              intent   : 'WISHLIST.VIEW.ALL.ITEMS',
              score    : 1,
              // eg: { entity : skuId }
              entities : [ { entity : matches[1] } ]
          })
          return
      }

      // Profile
      regEx   = /Profile/g
      matches = null
      matches = regEx.exec( text )
      if ( matches ) {
          console.log('Profile->')
          context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
          callback( null, {
              intent   : 'PERSONALIZATION.WELCOME',
              score    : 1,
              // eg: { entity : skuId }
              entities : [ { entity : matches[1] } ]
          })
          return
      }

      // View it on
      regEx   = /VIEWITON::(.*)/g
      matches = null
      matches = regEx.exec( text )
      if ( matches ) {
          var payload =  text.split("::");
          var url = payload[1];
          var flow = payload[2]
          var client = payload[3]
          var xc_sku = payload[4]
          console.log('Viewing product on '+text)
          context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
          callback( null, {
              intent   : 'VIEW.IT.ON',
              score    : 1,
              // eg: { entity : skuId }
              entities : [ { entity : text, entityType : flow, data : { url : url,client:client ,xc_sku:xc_sku} } ]
          })
          return
      }




      regEx   = /PLAY::/g
      matches = null
      matches = regEx.exec( text )
      if ( matches ) {
          var payload =  text.split("::");
          var PLAY = payload[0];
          var video = payload[1]
          var flow = payload[2]
          //console.log(PLAY)
          console.log('Play video '+video)
          context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
          callback( null, {
              intent   : 'PLAY',
              score    : 1,
              // eg: { entity : skuId }
              entities : [ { entity : text, entityType : payload[0], data : { video : video ,flow:flow} } ]
          })
          return
      }

    // GUIDE ME Postbacks after getting filtered by a client_name
    regEx = /GUIDEME::(.*)::CLIENT_NAME::(.*)::(.*)/g 
    matches = null
    matches = regEx.exec( text )
    if ( matches ) {
      console.log('Postback Recognizer (Guide Me-Category-client_name) ->')
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      callback( null, {
        intent   : 'GUIDE.ME.CATEGORY',
        score    : 1,
        entities : [ { entity : text, entityType : 'GUIDE.ME.CATEGORY.CLIENTNAME', data : { gender : matches[1], client_name:matches[2] } } ]
      })
      return
    }

    // GUIDE ME Postbacks
    // Guide-Me-Select-Category-Next-Page Postback button
    regEx   = /GUIDEME::(.*)::CLIENT_NAME::(.*)::NEXTPAGE::(.*)/g
    matches = null 
    matches = regEx.exec( text )
    if ( matches ) {
      console.log('Postback Recognizer (Guide Me-Category-NEXT PAGE) ->')
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      callback( null, {
        intent   : 'GUIDE.ME.CATEGORY',
        score    : 1,
        entities : [ { entity : text, entityType : 'GUIDE.ME.CATEGORY.NEXTPAGE', data : { gender : matches[1],client_name: matches[2], pageNo : matches[3] } } ]
      })
      return
    }
    // Guide-Me-Select-Category-Previous-Page Postback button
    regEx   = /GUIDEME::(.*)::PREVIOUSPAGE::(.*)/g
    matches = null 
    matches = regEx.exec( text )
    if ( matches ) {
      console.log('Postback Recognizer (Guide Me-Category-PREVIOUS PAGE) ->')
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      callback( null, {
        intent   : 'GUIDE.ME.CATEGORY',
        score    : 1,
        entities : [ { entity : text, entityType : 'GUIDE.ME.CATEGORY.PREVIOUSPAGE', data : { gender : matches[1], pageNo : matches[2], client_name : [] } } ]
      })
      return
    }

    // SELECT SIZE
    // Guide-Me-Select-Category Postback button
    regEx   = /GUIDEME::(.*)::(.*)::(.*)/g
    matches = null 
    matches = regEx.exec( text )
    if ( matches ) {
      console.log('Postback Recognizer (Guide Me-Category-CATEGORY SELECT) ->')
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      callback( null, {
        intent   : 'GUIDE.ME.PERSONALISE',
        score    : 1,
        entities : [ { entity : text, entityType : 'GUIDE.ME.CATEGORY', data : { gender : matches[1], client_name : matches[2],category : matches[3] } } ]
      })
      return
    }
    // Guide-Me-Select-Size-Next-Page Postback button
    regEx   = /GUIDEMESIZE::(.*)::(.*)::(.*)::NEXTPAGE::(.*)/g
    matches = null
    matches = regEx.exec( text )
    if ( matches ) {
      console.log('Postback Recognizer (Guide Me-Size-NEXT PAGE) ->')
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      callback( null, {
        intent   : 'GUIDE.ME.SIZE',
        score    : 1,
        entities : [ { entity : text, entityType : 'GUIDE.ME.SIZE.NEXTPAGE', data : { gender : matches[1], category : matches[2], client_name : matches[3], pageNo : matches[4] } } ]
      })
      return
    }
    // Guide-Me-Select-Size-Previous-Page Postback button
    regEx   = /GUIDEMESIZE::(.*)::(.*)::PREVIOUSPAGE::(.*)/g
    matches = null
    matches = regEx.exec( text )
    if ( matches ) {
      console.log('Postback Recognizer (Guide Me-Size-PREVIOUS PAGE) ->')
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      callback( null, {
        intent   : 'GUIDE.ME.SIZE',
        score    : 1,
        entities : [ { entity : text, entityType : 'GUIDE.ME.SIZE.PREVIOUSPAGE', data : { gender : matches[1], category : matches[2], pageNo : matches[3] } } ]
      })
      return
    }

    // Guide Me - Show Results
    // Guide-Me-Select-Size Postback button
    regEx   = /GUIDEMESIZE::(.*)::(.*)::(.*)::(.*)/g
    matches = null 
    matches = regEx.exec( text )
    if ( matches ) {
      console.log('Postback Recognizer (Guide Me-Category-SIZE SELECT) ->')
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      callback( null, {
        intent   : 'GUIDE.ME.RESULTS',
        score    : 1,
        // entity is not used any more since we are using Generic API
        // entities : [ { entity: text, entityType : 'GUIDE.ME.SIZE', data : { entity : matches[2], xc_category: [ matches[2] ], gender : [ matches[1] ], size: [ matches[3] ] } } ]
        entities : [ { entity: text, entityType : 'GUIDE.ME.SIZE', data : { xc_category: [ matches[2] ], gender : [ matches[1] ], size: [ matches[3] ], client_name :  matches[4]  } } ]
      })
      return
    }
    // End Guide Me

    /**
     * Personalization
     */
    // Personalization Welcome
    regEx   = /PERSONALIZATION_WELCOME::(.*)::(.*)::(.*)::(.*)/g
    matches = null 
    matches = regEx.exec( text )
    if ( matches ) {
      let data = {}
      data[ matches[3] ] = matches[ 4 ]
      console.log('Postback Recognizer (Personalization - ' + matches[1] + ' ) ->')
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      callback( null, {
        intent   : matches[1],
        score    : 1,
        entities : [ { entity: text, entityType : matches[2], data : data } ]
      })
      return
    }
    // Personalization Welcome
    regEx   = /PERSONALIZATION_WELCOME::(.*)::(.*)/g
    matches = null 
    matches = regEx.exec( text )
    if ( matches ) {
      let data = {}
      console.log('Postback Recognizer (Personalization - ' + matches[1] + ' ) ->')
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      callback( null, {
        intent   : matches[1],
        score    : 1,
        entities : [ { entity: text, entityType : matches[2], data : data } ]
      })
      return
    }

    // Personlaization fill out profile (this NEXT PAGE is before the next regEx because it will be matched by that also)
    regEx   = /PERSONALIZATION_FILL_OUT_PROFILE::(.*)::(.*)::(.*)::NEXTPAGE::(.*)::(.*)/g
    matches = null 
    matches = regEx.exec( text )
    if ( matches ) {
      console.log('Postback Recognizer (Personalization - Fill Out Profile NEXT PAGE ) ->')
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      callback( null, {
        intent   : 'PERSONALIZATION.SHOW.ATTRIBUTES.LIST',
        score    : 1,
        entities : [ { entity: text, entityType : 'PERSONALIZATION.FILL.OUT.PROFILE.ENTITY', data : { POST_BACK_PREFIX : 'PERSONALIZATION_FILL_OUT_PROFILE', unfilledEntity : matches[1], attribute : matches[2], REPLACE_DIALOG : matches[3], pageNo : matches[4], gender: matches[5] } } ]
      })
      return
    }
    // User clicked profile > fill out profile > this postback
    regEx   = /PERSONALIZATION_FILL_OUT_PROFILE::(.*)::(.*)::(.*)::(.*)::(.*)/g
    matches = null 
    matches = regEx.exec( text )
    if ( matches ) {
      console.log('Postback Recognizer (Personalization - Fill Out Profile ) ->')
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      callback( null, {
        intent   : 'PERSONALIZATION.SAVE.TO.PROFILE',
        score    : 1,
        entities : [ { entity: text, entityType : 'PERSONALIZATION.FILL.OUT.PROFILE.OPTION', data : { unfilledEntity : matches[1], attribute : matches[2], option : matches[3], REPLACE_DIALOG : matches[4], gender: matches[5] } } ]
      })
      return
    }
    // Personalization edit profile
    regEx   = /PERSONALIZATION_EDIT_PREFERENCES::(.*)::(.*)::(.*)::NEXTPAGE::(.*)::(.*)/g
    matches = null 
    matches = regEx.exec( text )
    if ( matches ) {
      console.log('Postback Recognizer (Personalization - Edit Pref NEXT PAGE ) ->')
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      callback( null, {
        intent   : 'PERSONALIZATION.SHOW.ATTRIBUTES.LIST',
        score    : 1,
        entities : [ { entity: text, entityType : 'PERSONALIZATION.EDIT.PREFERENCES.ENTITY', data : { POST_BACK_PREFIX : 'PERSONALIZATION_EDIT_PREFERENCES', unfilledEntity : matches[1], attribute : matches[2], REPLACE_DIALOG : matches[3], pageNo : matches[4], gender: matches[5] } } ]
      })
      return
    }
    // User clicked on edit prefernces > entity > this postback
    regEx   = /PERSONALIZATION_EDIT_PREFERENCES::(.*)::(.*)::(.*)::(.*)::(.*)/g
    matches = null 
    matches = regEx.exec( text )
    if ( matches ) {
      console.log('Postback Recognizer (Personalization - Fill Out Profile ) ->')
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      callback( null, {
        intent   : 'PERSONALIZATION.SAVE.TO.PROFILE',
        score    : 1,
        entities : [ { entity: text, entityType : 'PERSONALIZATION.EDIT.PREFERENCES.OPTION', data : { unfilledEntity : matches[1], attribute : matches[2], option : matches[3], REPLACE_DIALOG : matches[4], gender: matches[5] } } ]
      })
      return
    }

    /**
     * Wishlist
     */
    regEx   = /WISHLIST::(.*)::(.*)::(.*)::(.*)/g
    matches = null 
    matches = regEx.exec( text )
    if ( matches ) {
      console.log('Postback Recognizer (Wishlist - Show Options ) ->')
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      console.log("postback identified")
      callback( null, {
        intent   : 'WISHLIST.SHOW.OPTIONS',
        score    : 1,
        entities : [ { entity: text, entityType : 'TYPE.A.QUERY.MORE.OPTIONS', data : { xc_sku : matches[1], productName : matches[2], type : matches[3], imageUrl: matches[4] } } ]
      })
      return
    }

      /**
       * Wishlist
       */
      regEx   = /WISHLIST::(.*)::(.*)::(.*)/g
      matches = null
      matches = regEx.exec( text )
      if ( matches ) {
          console.log('Postback Recognizer (Wishlist - Show Options ) ->')
          context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
          console.log("postback identified")
          callback( null, {
              intent   : matches[3],
              score    : 1,
              entities : [ { data : { xc_sku : matches[1], productName : matches[2] } } ]
          })
          return
      }
      // INTRO_Ready
      regEx   = /Ready/g
      matches = null
      matches = regEx.exec( text )
      if ( matches ) {
          console.log('Ready->')
          context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
          callback( null, {
              intent   : 'INTRO_Ready',
              score    : 1,
          })
          return
      }

    /**
     * VIEW MORE, found below Vision results, when status_code = 1
     */
    regEx   = /VIEWMORE::(.*)::(.*)::(.*)::(.*)::(.*)/g
    matches = null 
    matches = regEx.exec( text )
    if ( matches ) {
      console.log('Postback Recognizer ( VIEW MORE ( with Gender ) ) ->')
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      callback( null, {
        intent   : 'RESULTS.DIALOG',
        score    : 1,
        entities : [ { entity: text, entityType : 'IMAGE.UPLOAD.VIEW.MORE', data : { NODE : Constants.NODE.IMAGEUPLOADVIEWMORE, resetState : true, state : { imageUrl : matches[1], gender : [ matches[2] ], entity : matches[3], isSocialUrl: String( matches[4] ) === 'true',upload_id : matches[5]} } } ]
      })
      return
    }
    
    regEx   = /VIEWMORE::(.*)::(.*)::(.*)::(.*)/g
    matches = null 
    matches = regEx.exec( text )
    if ( matches ) {
      console.log('Postback Recognizer ( VIEW MORE ( no Gender ) ) ->')
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      callback( null, {
        intent   : 'RESULTS.DIALOG',
        score    : 1,
        entities : [ { entity: text, entityType : 'IMAGE.UPLOAD.VIEW.MORE', data : { NODE : Constants.NODE.IMAGEUPLOADVIEWMORE, resetState : true, state : { imageUrl : matches[1], entity : matches[2], isSocialUrl: String( matches[3] ) === 'true',upload_id : matches[4] } } } ]
      })
      return
    }
    
    regEx   = /IMAGEUPLOADVIEWMORE::(.*)::(.*)::(.*)/g
    matches = null 
    matches = regEx.exec( text )
    if ( matches ) {
      console.log('Postback Recognizer ( UPLOADIMAGE VIEW MORE CLIENTNAME ) ->')
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      callback( null, {
        intent   : 'RESULTS.DIALOG',
        score    : 1,
        entities : [ { entity: text, entityType : 'IMAGE.UPLOAD.VIEW.MORE', data : { NODE : Constants.NODE.IMAGEUPLOADVIEWMORE, resetState : false, state : { client_name : matches[1], imageUrl : matches[2], gender : matches[3] } } } ]
      })
      return
    }

    /**
     * Generic Postback recognizer
     */
    regEx   = /POSTBACK::(.*)/g
    matches = null 
    matches = regEx.exec( text )
    if ( matches ) {
      let intent = JSON.parse( matches[1] )
      console.log( 'Postback Recognizer ( GENERIC ) ->', intent.intent )
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      if ( intent.score === undefined || intent.score === null ) {
        intent.score = 1
      }
      callback( null, intent )
      return
    }

    regEx   = /SKIP::(.*)::(.*)/g
    matches = null 
    matches = regEx.exec( text )
    if ( matches ) {
      context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
      callback( null, {
        intent   : 'RESULTS.DIALOG',
        score    : 1,
        entities : [ { entity: text, entityType : 'SKIP', data : {} } ]
      })
      return
    }

    regEx   = /COLLECTIONSHOP::(.*)::(.*)::(.*)/g
      matches = null
      matches = regEx.exec( text )
      if ( matches ) {
          var payload = 
          context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
          callback( null, {
              intent   : 'APPLY.RETAILER.WEBVIEW',
              score    : 1,
              entities : [ { entity: text, entityType: 'APPLY-RETAILER', data : { collectionType : matches[1], collectionName : matches[2], gender : matches[3]} } ]
          })
          return
      }

      regEx   = /NLPSHOP::(.*)/g
      matches = null
      matches = regEx.exec( text )
      if ( matches ) {
          var payload = 
          context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
          callback( null, {
              intent   : 'APPLY.RETAILER.WEBVIEW',
              score    : 1,
              entities : [ { entity: text, entityType: 'APPLY-RETAILER', data : { gender : matches[1]} } ]
          })
          return
      }
    
      regEx   = /GUIDEMESHOP::(.*)::(.*)/g
      matches = null
      matches = regEx.exec( text )
      if ( matches ) {
          var payload = 
          context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
          callback( null, {
              intent   : 'APPLY.RETAILER.WEBVIEW',
              score    : 1,
              entities : [ { entity: text, entityType: 'APPLY-RETAILER', data:{ gender:matches[1], pageNo:matches[2]} } ]
          })
          return
      }

      regEx   = /IMAGEUPLOADVIEWMORESHOP::(.*)::(.*)/g
      matches = null
      matches = regEx.exec( text )
      if ( matches ) {
          var payload = 
          context.userData.analytics.in.message_type_flag = 'POSTBACK'    // User input is a Postback press
          callback( null, {
              intent   : 'APPLY.RETAILER.WEBVIEW',
              score    : 1,
              entities : [ { entity: text, entityType: 'APPLY-RETAILER', data:{ imageUrl:matches[1], gender:matches[2]} } ]
          })
          return
      }

    // Nothing matched       
    if ( !matches ) {
      console.log('Postback Recognizer <-')
      callback( null, { intent : null, score : 0 } )
    }    
    
    
  }
}


