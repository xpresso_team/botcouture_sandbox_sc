var Constants = require( '../constants' )

// TODO: Think of a better name for this module
// REPLACE THIS WITH Prompt Recognizer
module.exports = {
  
  recognize : function( context, callback ) {
    
    const text = context.message.text

    //console.log('MULTI STEP: ', context.userData )

    if ( context.userData.previousPrompt === 'IMAGE.UPLOAD.CATEGORY' ) {
      
      console.log( 'Previous Prompt was: ', context.userData.previousPrompt, context.message.text )
      console.log( 'Multi-Step-Buttons-recognizer ->')
      callback.call( null, null, {
        intent   : 'IMAGE.UPLOAD.CATEGORY',
        score    : 1,
        entities : [ { entity : context.message.text } ]
      })
    
    } else {
      console.log( 'Multi-Step-Buttons-recognizer <-')
      callback.call( null, null, { intent : null, score : 0 } )
    
    }
    
  }
}