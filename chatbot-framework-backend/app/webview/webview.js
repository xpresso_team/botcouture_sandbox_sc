var _ = require('lodash')
var UserService = require('../services/UserService')
var ApiService = require('../services/ApiService')

/**
 * TODO: Do this better way
 */
webview = function (req, res, next) {


    try{

    console.log('@@@@@@@@@@@@ Request to web view @@@@@@@@@@@@@')

    console.log('Slug->', req.params.slug)

    // res.send( { q : req.query, p : req.params } )
    let address = {
        user: {
            id: req.params.psid
        },
        channelId: 'facebook',
        bot: {
            id: req.params.botid
        }
    }

    console.log('req.params before-> ', JSON.stringify(req.params))

    console.log('Address-> ', address)

    console.log('req.params-> ', JSON.stringify(req.params))



    /**
     *
     get All answers of a User
     */

    console.log('matching slugs')

    if (req.params.slug == 'getUser') {
        UserService.getUser(address)
            .then(userObj => {
                // console.log( 'getUser', userObj )
                res.send(userObj)
                return next()
            })
            .catch(err => {
                res.send(400)
                console.log(err)
            })
    }

    if (req.params.slug == 'updateUser') {
        let gender = req.params.gender
        UserService.updateUser(address, {gender: gender})
            .then(noop => {
                // console.log('Saving Gender', gender, noop.status)
                res.send(200)
                return next()
            })
            .catch(err => {
                res.send(400)
                console.log(err, err.status)
            })
    }


    /**
     * Get user's whole profile for all entities
     */
    if (req.params.slug == 'getUserPreferences') {

        UserService.getUserProfile(address)
            .then(userProfile => {
                // console.log('-->',userProfile)
                res.send(userProfile)
                return next()
            })
            .catch(err => {
                res.send(400)
                console.log(err)
            })
    }


    /**
     * Get usr's preference for a given entity
     * For ProfileItemComponent
     */
    if (req.params.slug == 'getUserPreferencesByEntity') {
        let entity = req.params.entity
        let gender = req.params.gender
        UserService.getUserProfileByEntity(address, entity,gender)
            .then(userProfile => {
                console.log('getUserPreferencesByEntity -->', userProfile)
                res.send(userProfile)
                return next()
            })
            .catch(err => {
                res.send(400)
                console.log(err)
            })
    }

    /**
     * Overwrite user's preference
     */
    // if ( req.params.slug == 'saveUserProfileByEntity' ) {

    //   let entityName     = req.params.entity
    //   let attribute      = req.params.attribute
    //   let attributeValue = req.params.value   // <- array

    //   UserService.saveUserProfileByEntity( address, entityName, attribute, attributeValue )
    //   .then( result => {
    //     console.log( 'saveUserProfileByEntity -> ', result )
    //     res.send( 200, result )
    //     return next()
    //   })
    // }


    if (req.params.slug == 'saveUserProfileByEntity') {
        let unfilledEntity = req.params.entity
        let attribute = req.params.attribute
        let value = req.params.value    // <-- array
        let gender = req.params.gender    // <-- array
        UserService.saveUserProfileByEntity(address, unfilledEntity, attribute, value,gender)
            .then(success => {
                console.log(`Success saving ${unfilledEntity}'s ${attribute} as ${value}`)
                res.send(`Success saving ${unfilledEntity}'s ${attribute} as ${value}`)
                return next()
            })
    }

    // if ( req.params.slug == 'appendToUserProfileByEntity' ) {
    //   let entityName = req.params.entity
    //   let filterOn = req.params.attribute
    //   let attributeValue = req.params.value
    //   UserService.appendToUserProfileByEntity( address, entityName, filterOn, attributeValue )
    //   .then( result => {
    //     res.send( 200, result )
    //     return next()
    //   })
    //   .catch( err => {
    //     res.send( 400 )
    //     console.log( err )
    //   })
    // }

    if (req.params.slug == 'saveToWishlist') {
        let xc_sku = req.params.xc_sku
        let size = req.params.size
        let color = req.params.color
        let catalog = req.params.catalog

        console.log("catalog for wishlist is", catalog)

        UserService.saveToWishlist(address, xc_sku, color, size, catalog)
            .then(wishlist => {
                res.send('Success')
                return next()
            })
            .catch(err => {
                res.send(400)
                console.log(err)
            })
    }

    /*if (req.params.slug == 'removeFromWishlist') {
        let xc_sku = req.params.xc_sku
        UserService.removeFromWishlist(address, xc_sku)
            .then(wishlist => {
                res.send('Success')
                return next()
            })
            .catch(err => {
                res.send(400)
                console.log(err)
            })
    }*/

    /**
     * Check if the item is already present in wishlsit
     */
    if (req.params.slug == 'isItemInWishlist') {
        let xc_sku = req.params.xc_sku
        UserService.getUsersWishlist(address)
            .then(wishlist => {
                if (!wishlist) wishlist = []
                let wishlistSkus = _.map(wishlist, item => item.xc_sku)
                let itemAlreadyInWishlist = _.includes(wishlistSkus, xc_sku)
                res.send({item_present_in_wishlist: itemAlreadyInWishlist})
                return next()
            })
            .catch(err => {
                res.send(400)
                console.log(err)
            })
    }

    /**
     * Check if the item is already present in wishlsit
     */
    // if ( req.params.slug == 'getSimilar' ) {
    //   var msg = new builder.Message().address(address);
    //   msg.text('Hello, this is a notification');
    //   msg.textLocale('en-US');
    //   bot.send(msg);
    // }

    /**
     * Get sizes for a given category // not used any more
     */
    if (req.params.slug == 'getSizes') {
        let gender = req.params.gender
        let xc_category = req.params.xc_category
        ApiService.getGuideMeSizes(gender, xc_category, "Webview")
            .then(sizeObj => {
                let sizeList = sizeObj.Results.size || []
                //sizeList = _.orderBy( sizeList )
                res.send({sizeList: sizeList})
                return next()
            })
            .catch(err => {
                console.log('getSizes Error ', err)
                res.send(400)
            })
    }

    /**
     * Get attributes (e.g. size values) for given multiple entities
     */
    if (req.params.slug == 'getAttributesByEntities') {
        let quesArray = req.params.quesArray
        let prom = []
        console.log(quesArray)
        _.each(quesArray, ques => {
            // TODO: Make this generic instead of just sizes
            //prom.push( ApiService.getGuideMeSizes( ques.gender, ques.entity ) )    // ques.attribute === 'size'
            prom.push(ApiService.getAttributeValue(ques.gender, ques.entity, ques.attribute, "Webview"))    // ques.attribute === 'size'
        })

        Promise.all(prom)
            .then(sizeObj => {
                console.log('attributes obj before  -> ', sizeObj)


                sizeObj = _.map(sizeObj, obj => {
                    // obj == {
                    //   xc_category: [ 't-shirt' ],
                    //   client_name: [ '' ],
                    //   Results: { size: [Object] }
                    // }
                    return {entity: obj.Results.xc_category[0], attributes: obj.Results[obj.Results.attribute]}
                })
                console.log('attributes Obj -> ', sizeObj)
                res.send(sizeObj)
                return next()
                // let sizeList = sizeObj.Results.size || []
                // sizeList = _.orderBy( sizeList )
                // res.send( { sizeList : sizeList } )
                // return next()
            })
            .catch(err => {
                console.log('getSizes Error ', err)
                res.send(400)
            })
    }

    /**
     * Get attributes (e.g. size values) for given multiple entities
     */
    if (req.params.slug == 'getUserPreferencesByEntities') {
        let quesArray = req.params.quesArray
        // console.log( 'quesArray => ', quesArray)
        // Get whole of user's profile, and filter here
        UserService.getUserProfile(address)
            .then(results => {
                // console.log( 'getUserPreferencesByEntities -> ', results )
                results = _.filter(results, result => {
                    // TODO: enable following when backend perferences are per gender
                    // return _.find( quesArray, ques => ques.entity === result.entity && ques.gender === result.gender )
                    console.log("quesArray given is", quesArray)
                    console.log("user prefernce obtained is ", results)

                    return _.find(quesArray, ques => (ques.entity).toLowerCase() === result.entity.toLowerCase())
                })
                res.send(results)
                return next()
            })
            .catch(err => {
                console.log('getUserPreferencesByEntities Error ', err)
                res.send(400)
            })
    }

    /**
     * Get attributes (e.g. size values) for given multiple entities
     */
    if (req.params.slug == 'saveUserPreferencesByEntities') {
        let preferences = req.params.preferences
        // console.log( '-->>> ', preferences, gender )
        let prom = []
        _.each(preferences, pref => {
            prom.push(UserService.saveUserProfileByEntity(address, pref.entity, pref.attributeName, pref.attributes, pref.attributeGender))
        })
        Promise.all(prom)
            .then(result => {
                res.send({ok: 'ok'})
                return next()
            })
            .catch(err => {
                console.log('saveUserPreferencesByEntities Error ', err)
                res.send(400)
            })
    }

    /*******************************************
     ************** Shopping Cart ***************
     ********************************************/

    /**
     * Get all carts with count of items
     */
    if (req.params.slug == 'addItemToCart') {

        let catalog = req.params.catalog
        let color = req.params.color
        let quantity = req.params.quantity
        let size = req.params.size
        let xc_sku = req.params.xc_sku

        UserService.addItemToCart(address, catalog, color, quantity, size, xc_sku)
            .then(result => {
                console.log('addItemToCart', result)
                res.send(result)
                return next()
            })
            .catch(err => {
                console.log('addItemToCart Error ', err)
                res.send(400)
            })
    }

    /**
     * Get all carts with count of items
     */
    if (req.params.slug == 'getUsersAllCarts') {
        UserService.getUsersAllCarts(address)
            .then(result => {

                result = _.groupBy(result, cart => cart.client_name)
                result = _.map(result, (val, key) => {
                    return {catalog: key, itemCount: val.length}
                })

                let allCarts = [
                    {catalog: "Macy's", itemCount: 2},
                    {catalog: "Kohls", itemCount: 4},
                    {catalog: "Forever 21", itemCount: 5}
                ]

                // console.log( result )
                res.send(result)
                return next()
            })
            .catch(err => {
                console.log('getUsersAllCarts Error ', err)
                res.send(400)
            })
    }

    /**
     * Clear all carts
     */
    if (req.params.slug == 'clearAllCarts') {
        UserService.clearAllCarts(address)
            .then(result => {
                // console.log( result )
                res.send(result)
                return next()
            })
            .catch(err => {
                console.log('clearAllCarts Error ', err)
                res.send(400)
            })
    }

    /**
     * Clear a cart by catalog
     */
    if (req.params.slug == 'clearCartByCatalog') {
        let catalog = req.params.catalog
        UserService.clearCartByCatalog(address, catalog)
            .then(result => {
                console.log(result)
                res.send(result)
                return next()
            })
            .catch(err => {
                console.log('clearCartByCatalog Error ', err)
                res.send(400)
            })
    }

    /**
     * Get a cart for given catalog
     * also tag each item in the cart if it is in user's wishlist
     */
    if (req.params.slug == 'getUsersCartByCatalog') {
        let catalog = req.params.catalog
        Promise.all([
            UserService.getUsersCartByCatalog(address, catalog),
            UserService.getUsersWishlist(address)
        ]).then(result => {
            let [cart, wishlist] = result
            cart.items = _.map(cart.items, item => {
                let exists = _.find(wishlist, wl => wl.xc_sku === item.item.xc_sku)
                item.inWishlist = false
                if (exists) {
                    item.inWishlist = true
                }
                return item
            })
            // console.log( 'getUsersCartByCatalog ->',result )
            res.send(cart)
            return next()
        })
            .catch(err => {
                console.log('getUsersCartByCatalog Error ', err)
                res.send(400)
            })
    }

    /**
     * Remove item from cart
     */
    if (req.params.slug == 'removeItemFromCart') {
        let catalog = req.params.catalog
        let xc_sku = req.params.xc_sku
        UserService.removeItemFromCart(address, catalog, xc_sku)
            .then(result => {
                // console.log( 'removeItemFromCart --> ', result )
                res.send(result)
                return next()
            })
            .catch(err => {
                console.log('removeItemFromCart Error ', err)
                res.send(400)
            })
    }


    /**
     * Update cart item (change size, color, quantity)
     * TODO: Make this generic
     */
    if (req.params.slug == 'updateCartItem') {
        let catalog = req.params.catalog
        let xc_sku = req.params.xc_sku
        let quantity = req.params.quantity
        UserService.updateCartItem(address, catalog, quantity, xc_sku)
            .then(result => {
                // console.log( 'updateCartItem --> ', result )
                res.send(result)
                return next()
            })
            .catch(err => {
                console.log('updateCartItem Error ', err)
                res.send(400)
            })
    }


    /**
     * Remove item from cart and move to wishlist
     */
    if (req.params.slug == 'moveToWishlist') {
        let xc_sku = req.params.xc_sku
        let catalog = req.params.catalog
        let size = req.params.size
        let color = req.params.color
        // console.log( 'moveToWishlist', xc_sku )
        Promise.all([
            UserService.removeItemFromCart(address, catalog, xc_sku),
            UserService.saveToWishlist(address, xc_sku, color, size, catalog)
        ])
            .then(result => {
                let [itemRemoved, itemSaved] = result
                res.send(result)
                return next()
            })
            .catch(err => {
                console.log('moveToWishlist Error ', err)
                res.send(400)
            })
    }

    /**
     * Remove item from wishlist and move to cart
     */
    if (req.params.slug == 'moveToCart') {
        let catalog = req.params.catalog
        let color = req.params.color
        let quantity = req.params.quantity
        let size = req.params.size
        let xc_sku = req.params.xc_sku


        // console.log( 'moveToWishlist', xc_sku )
        Promise.all([
            UserService.removeFromWishlist(address, xc_sku, catalog),
            UserService.addItemToCart(address, catalog, color, quantity, size, xc_sku)
        ])
            .then(result => {
                let [itemRemoved, itemSaved] = result
                res.send(result)
                return next()
            })
            .catch(err => {
                console.log('moveToCart Error ', err)
                res.send(400)
            })
    }


    /**
     * Remove item from wishlist
     */
    if (req.params.slug == 'removeItemFromWishlist') {
        let xc_sku = req.params.xc_sku
        let catalog = req.params.catalog
        // console.log( 'removeItemFromWishlist', xc_sku )

        UserService.removeFromWishlist(address, xc_sku, catalog)
            .then(result => {
                // console.log( 'removeItemFromWishlist --> ', result )
                res.send({ok: 'ok'})
                return next()
            })
            .catch(err => {
                console.log('removeItemFromWishlist Error ', err)
                res.send(400)
            })
    }


    /**
     * Get all wishlist with count of items
     */
    if (req.params.slug == 'getUsersAllWishlists') {
        UserService.getUsersWishlist(address)
            .then(result => {

                result = _.groupBy(result, cart => cart.client_name)
                result = _.map(result, (val, key) => {
                    return {catalog: key, itemCount: val.length}
                })

                let allCarts = [
                    {catalog: "Macy's", itemCount: 2},
                    {catalog: "Kohls", itemCount: 4},
                    {catalog: "Forever 21", itemCount: 5}
                ]

                // console.log( result )
                res.send(result)
                return next()
            })
            .catch(err => {
                console.log('getUsersALLWishlist Error ', err)
                res.send(400)
            })
    }

    /**
     * Get a wishlist for given catalog
     */
    if (req.params.slug == 'getUsersAllWishlistByCatalog') {
        let catalog = req.params.catalog
        Promise.all([
            UserService.getUsersWishlistByCatalog(address, catalog),
        ]).then(result => {
            res.send(result[0])
            return next()
        })
            .catch(err => {
                console.log('getUsersCartByCatalog Error ', err)
                res.send(400)
            })
    }


    /*******************************************
     ************** Product Details *************
     ********************************************/

    /**
     * Product details
     */
    if (req.params.slug == 'getProductDetails') {
        let xc_sku = req.params.xc_sku
        // console.log( 'getProductDetails', xc_sku )
        Promise.all([
            ApiService.getProductDetails(xc_sku, "uniq"),
            UserService.getUsersWishlist(address),
            UserService.getUsersAllCarts(address)
        ])
            .then(result => {
                console.log('getProductDetails')
                let [product, wishlist, carts] = result
                if (_.has(product, 'Results.Results[0]')) {
                    product = product.Results.Results[0]
                    console.log("product is ", product)
                    if (product.colorsavailable) {
                        //product.colorsavailable = JSON.parse(product.colorsavailable)
                        product.colorsavailable = product.colorsavailable
                    }
                    if (product.size) {
                        //product.size = JSON.parse(product.size)
                        product.size = product.size
                    }
                    if (product.subcategory) {
                        //product.subcategory = JSON.parse(product.subcategory)
                        product.subcategory = product.subcategory
                    }
                    product.inCart = false        //  <-- TODO: change this to actual value once backend is ready
                } else {
                    product = {}
                }
                // console.log( 'getProductDetails --> ', product, wishlist )
                product.inWishlist = false
                if (_.find(wishlist, item => item.xc_sku === product.xc_sku)) {
                    product.inWishlist = true
                }
                product.inCart = false
                if (_.find(carts, item => item.xc_sku === product.xc_sku)) {
                    product.inCart = true
                }
                // console.log( 'CARTS', carts )
                res.send(product)
                return next()
            })
            .catch(err => {
                console.log('getProductDetails Error ', err)
                res.send(400)
            })
    }


    /**
     * Group_sku details
     */

    if (req.params.slug == 'getGroupProductDetails') {
        let group_sku = req.params.group_sku
        console.log('getProductDetails', group_sku)
        console.log('getProductDetails address', address)
        Promise.all([
            ApiService.getProductDetails(group_sku, "multi"),
            UserService.getUsersWishlist(address),
            UserService.getUsersAllCarts(address)
        ])
            .then(result => {
                console.log('getGroupProductDetails')
                let [product, wishlist, carts] = result
                product = product.Results.Results
                let Final_result = []
                let int_result = []
                if (product.length > 0) {
                    Final_result = _.map(product, (item) => {
                        int_result = item
                        console.log("product is ", int_result)
                        if (int_result.colorsavailable) {
                            //int_result.colorsavailable = JSON.parse(int_result.colorsavailable)
                            int_result.colorsavailable = int_result.colorsavailable
                        }
                        if (int_result.size) {
                            //int_result.size = JSON.parse(int_result.size)
                            int_result.size = int_result.size
                        }
                        if (int_result.subcategory) {
                            //int_result.subcategory = JSON.parse(int_result.subcategory)
                            int_result.subcategory = int_result.subcategory
                        }
                        // console.log( 'getProductDetails --> ', product, wishlist )
                        int_result.inWishlist = false
                        if (_.find(wishlist, item => item.xc_sku === int_result.xc_sku)) {
                            int_result.inWishlist = true
                        }
                        int_result.inCart = false
                        if (_.find(carts, item => item.xc_sku === int_result.xc_sku)) {
                            int_result.inCart = true
                        }
                        console.log("product final is is ", int_result)
                        return int_result
                    })
                } else {
                    product = {}
                }

                console.log(Final_result)

                res.send(Final_result)
                return next()
            })
            .catch(err => {
                console.log('getProductDetails Error ', err)
                res.send(400)
            })
    }
    /**
     * Show recommendation
     */
    if (req.params.slug == 'recommendProduct') {

        console.log("recommend product service called")

        address.type = req.params.type
        UserService.recommendProduct(address)
            .then(result => {
                console.log(result)
                res.send(result)
                return next()
            })
            .catch(err => {
                console.log('recommendProduct Error ', err)
                res.send(400)
            })
    }

    if (req.params.slug == 'sendFeedback') {
        let session = {}
        session.message = {}
        session.message.address = address
        session.userData = {}
        session.userData.Feedback = req.params.feedback


        UserService.sendFeedback(session)
            .then(success => {
                console.log(`Success in sending feedback`)
                //res.send({success: 'success'})
                res.send('success')
                return next()
            }).catch(err => {
            console.log('feedback ', err)
            res.send(400)
        })
    }

    if(req.params.slug == 'getretailerPreference') {
      UserService.getretailerPreference(address)
        .then(ret => {
          ret = "{"+JSON.parse(ret.text)+"}"
          console.log(ret)
          res.send(ret)
        }) .catch(err => {
          console.log('getretailerPreference error is ',err)
          res.send(400)
        })
    }


    if(req.params.slug == 'getallRetailers') {
      console.log("entered getallRetailers call")
      let user = {}
      user.userId = address.user.id
      user.botId  = address.bot.id
      user.channelId = 'facebook'
      console.log(user.userId)
      ApiService.getallRetailers(user)
        .then(ret => {
          console.log("\n"+ret)
          res.send(ret)
        }) .catch( err => {
          console.log('getallRetailers error is ',err)
          res.send(400)
        })
    }

    if(req.params.slug == 'setretailerPreference') {
        let retpref=req.params.selectedretailers
        UserService.setretailerPreference(address,retpref)
          .then(success => {
            console.log('success in setting retailer preference')
            res.send('success')
            return next()
          }) .catch(err => {
            console.log('setretailerpreference', err)
            res.send(400)
          })
    }



    /**
     * Clear all wishlist
     */
    if (req.params.slug == 'clearAllWishlists') {
        UserService.clearWishlist(address)
            .then(result => {
                // console.log( result )
                res.send(result)
                return next()
            })
            .catch(err => {
                console.log('clearAllWishlists Error ', err)
                res.send(400)
            })
    }

    /**
     * Clear a wishlist by catalog
     */
    if (req.params.slug == 'clearWishlistByCatalog') {
        /*let catalog = req.params.catalog
         Promise.all([UserService.getUsersWishlistByCatalog(address, catalog)
         ]).then(result => {
         let prom = []
         _.each(result, xc_sku => {
         prom.push(UserService.removeFromWishlist(address, xc_sku))
         })
         Promise.all(prom)
         .then(result => {
         res.send({ok: 'ok'})
         return next()
         })
         .catch(err => {
         console.log('clearWishlistByCatalog internal Error ', err)
         res.send(400)
         })
         }).catch(err => {
         console.log('clearWishlistByCatalog Error ', err)
         res.send(400)
         })
         */

        let catalog = req.params.catalog
        UserService.clearWishlistByCatalog(address, catalog)
            .then(result => {
                console.log(result)
                res.send(result)
                return next()
            })
            .catch(err => {
                console.log('clearWishlistByCatalog Error ', err)
                res.send(400)
            })

    }


    /// Feedback from friend


    /**
     * get All Predefined Questions
     */
    if (req.params.slug == 'getAllPredefQuestions') {

        console.log("getAllPredefQuestions called ")
        UserService.getAllPredefQuestions()
            .then(result => {
                console.log("Questions are ", result)
                res.send(result)
                return next()
            })
            .catch(err => {
                console.log('getAllQuestions Error ', err)
                res.send(400)
            })
    }

    /**
     *
     add User selected and added Questions to db
     */
    if (req.params.slug == 'addUserQuestions') {

        let sender_name = req.params.sender_name
        //let pre_defined_question = req.params.pre_defined_question
        //let custom_question = req.params.custom_question
        let question = req.params.questions


        console.log("addUserQuestions called ")
        //UserService.addUserQuestions(address, sender_name, pre_defined_question, custom_question)
        UserService.addUserQuestions(address, sender_name, question)
            .then(result => {
                console.log("addUserQuestions are ", result)
                res.send(result)
                return next()
            })
            .catch(err => {
                console.log('addUserQuestions Error ', err)
                res.send(400)
            })
    }


    /**
     *
     get User selected and added Questions from db
     */
    if (req.params.slug == 'getUserQuestions') {

        let id = req.params.entryID

        id = parseInt(id)
        id = id.toString()

        console.log(id)

        console.log("getUserQuestions called ")
        UserService.getUserQuestions()
            .then(result => {
                console.log("getUserQuestions are ", result)
                result = _.filter(result, val => {
                    return val.id == id
                })
                console.log("user added question is ", result)
                result = result[0]

                result.question = JSON.parse(result.question)

                res.send(result)
                return next()

                /*let question_key_array = result.pre_defined_question.split(',')

                console.log("getGivenPredefQuestionss called ")
                UserService.getGivenPredefQuestions(question_key_array)
                    .then(questions => {
                        console.log("Questions are ", questions)
                        result.pre_defined_question = JSON.stringify(questions)
                        res.send(result)
                        return next()
                    })
                    .catch(err => {
                        console.log('getAllQuestions Error ', err)
                        res.send(400)
                    })*/
            })
            .catch(err => {
                console.log('getUserQuestions Error ', err)
                res.send(400)
            })
    }

    /**
     *
     Store User given answer in db
     */
    if (req.params.slug == 'sendUserAnswers') {

        let feedback_id = req.params.feedback_id
        feedback_id = parseInt(feedback_id)
        feedback_id = feedback_id.toString()
        let sender_name = req.params.sender_name
        let receiverPsid = req.params.receiverPsid
        let receiver_name = req.params.receiver_name
        let answer = req.params.answers
        //let pre_defined_question_answer = req.params.pre_defined_question_answer
        //let custom_question_answer = req.params.custom_question_answer

        console.log("sendUserAnswers called ")
        //UserService.sendUserAnswers(address, feedback_id, sender_name, receiverPsid, receiver_name, pre_defined_question_answer, custom_question_answer)
        UserService.sendUserAnswers(address, feedback_id, sender_name, receiverPsid, receiver_name, answer)
            .then(result => {
                console.log("sendUserAnswers are ", result)
                res.send(result)
                return next()
            })
            .catch(err => {
                console.log('sendUserAnswers Error ', err)
                res.send(400)
            })
    }




    /**
     *
     get a particular answers of a User
     */
    if (req.params.slug == 'retrieveUserAnswer') {

        let id = req.params.entryID
        id = parseInt(id)
        id = id.toString()

        console.log(id)

        console.log(" retrieveUserAnswer called ")
        UserService.retrieveAllUsersAnswers()
            .then(result => {
                console.log(" retrieveUserAnswer are ", result)
                result = _.filter(result, val => {
                    return val.id == id
                })
                console.log("retrieveUserAnswer is ", result)
                result= result[0]
                console.log("filteres answer array ",result)
                console.log("filteres answer is ",result.answer)
                result.answer = JSON.parse(result.answer)

                res.send(result)
                return next()


                /*var question_key_array = _.map(result, item => {

                    item.pre_defined_question_answer = JSON.parse(item.pre_defined_question_answer)

                    var temp = _.map(item.pre_defined_question_answer, pre_defined_question => {
                        return pre_defined_question.key
                    })

                    item.pre_defined_question_answer = JSON.stringify(item.pre_defined_question_answer)

                    //console.log("pre_defined_question is ",temp)

                    return temp
                })

                //console.log("question_key_array is ",question_key_array)

                question_key_array = [].concat.apply([], question_key_array);

                question_key_array = _(question_key_array).uniq().value()

                //console.log("question_key_array is ",question_key_array)


                console.log("getGivenPredefQuestionss called ")
                UserService.getGivenPredefQuestions(question_key_array)
                    .then(questions => {
                        console.log( "Questions are ",questions )

                        result = _.map(result, items => {

                            //console.log("items.pre_defined_question_answer before is ",items.pre_defined_question_answer)

                            items.pre_defined_question_answer = JSON.parse(items.pre_defined_question_answer)

                            //console.log("items.pre_defined_question_answer is ",items.pre_defined_question_answer)

                            items.pre_defined_question_answer = _.map(items.pre_defined_question_answer, item => {

                                //console.log("item.key is ",item.key)

                                item.key = item.key.toString()

                                //console.log(" string item.key is ",item.key)

                                for (let i = 0; i < questions.length; i++) {


                                    questions[i].key = questions[i].key.toString()

                                    //console.log(" questions[i] is ",questions[i])

                                    if (item.key == questions[i].key) {
                                        //console.log("matched item is ",item)
                                        //console.log("questions  is ",questions[i])
                                        item.question = questions[i].message
                                        break
                                    }

                                }

                                //console.log("item with question is ",item)


                                return item

                            })

                            //console.log(" modified items.pre_defined_question_answer is ",items.pre_defined_question_answer)


                            items.pre_defined_question_answer = JSON.stringify(items.pre_defined_question_answer)

                            //console.log(" strigified modified items.pre_defined_question_answer is ",items.pre_defined_question_answer)


                            return items
                        })

                        //console.log("results is ",result)
                        res.send(result)
                        return next()
                    })
                    .catch(err => {
                        console.log('retrieveUserAnswer Error ', err)
                        res.send(400)
                    })
*/
            })
            .catch(err => {
                console.log('retrieveUserAnswer Error ', err)
                res.send(400)
            })
    }

        if (req.params.slug == 'retrieveAnswer') {

            console.log("Inside retrieveAnswer")

            let feedback_id = req.params.feedback_id
            console.log("feedback_id ",feedback_id)
            feedback_id = parseInt(feedback_id)
            feedback_id = feedback_id.toString()

            console.log(feedback_id)

            console.log("retrieveAllAnswers called ")
            UserService.retrieveAllUsersAnswers()
                .then(result => {
                    console.log("retrieveAllAnswers are ", result)
                    result = _.filter(result, val => {
                        return val.feedback_id == feedback_id
                    })

                    result = _.map(result, val => {
                        val.answer = JSON.parse(val.answer)
                        return val
                    })

                    console.log("retrieveAllAnswers is ", result)

                    res.send(result)
                    return next()


                  /*  var question_key_array = _.map(result, item => {

                        item.pre_defined_question_answer = JSON.parse(item.pre_defined_question_answer)

                        var temp = _.map(item.pre_defined_question_answer, pre_defined_question => {
                            return pre_defined_question.key
                        })

                        item.pre_defined_question_answer = JSON.stringify(item.pre_defined_question_answer)

                        //console.log("pre_defined_question is ",temp)

                        return temp
                    })

                    //console.log("question_key_array is ",question_key_array)

                    question_key_array = [].concat.apply([], question_key_array);

                    question_key_array = _(question_key_array).uniq().value()

                    //console.log("question_key_array is ",question_key_array)


                    console.log("getGivenPredefQuestionss called ")
                    UserService.getGivenPredefQuestions(question_key_array)
                        .then(questions => {
                            console.log( "Questions are ",questions )

                            result = _.map(result, items => {

                                //console.log("items.pre_defined_question_answer before is ",items.pre_defined_question_answer)

                                items.pre_defined_question_answer = JSON.parse(items.pre_defined_question_answer)

                                //console.log("items.pre_defined_question_answer is ",items.pre_defined_question_answer)

                                items.pre_defined_question_answer = _.map(items.pre_defined_question_answer, item => {

                                    //console.log("item.key is ",item.key)

                                    item.key = item.key.toString()

                                    //console.log(" string item.key is ",item.key)

                                    for (let i = 0; i < questions.length; i++) {


                                        questions[i].key = questions[i].key.toString()

                                        //console.log(" questions[i] is ",questions[i])

                                        if (item.key == questions[i].key) {
                                            //console.log("matched item is ",item)
                                            //console.log("questions  is ",questions[i])
                                            item.question = questions[i].message
                                            break
                                        }

                                    }

                                    //console.log("item with question is ",item)


                                    return item

                                })

                                //console.log(" modified items.pre_defined_question_answer is ",items.pre_defined_question_answer)


                                items.pre_defined_question_answer = JSON.stringify(items.pre_defined_question_answer)

                                //console.log(" strigified modified items.pre_defined_question_answer is ",items.pre_defined_question_answer)


                                return items
                            })

                            //console.log("results is ",result)
                            res.send(result)
                            return next()
                        })
                        .catch(err => {
                            console.log('retrieveAllAnswers Error ', err)
                            res.send(400)
                        })
                   */
                })

                .catch(err => {
                    console.log('retrieveAllAnswers Error ', err)
                    res.send(400)
                })
        }


    console.log('matching slugs ended')
    } catch (err) {
        console.log("Some error happened in web view", err)
    }

}


module.exports = webview
