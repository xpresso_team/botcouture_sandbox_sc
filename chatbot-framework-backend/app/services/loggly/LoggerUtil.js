/**
 * Created by aashish_amber on 1/2/17.
 */
'use strict'; //Basically it enables the Javascript strict mode

var fs = require('fs');
var winston = require('winston');
require('winston-loggly-bulk');
require('winston-daily-rotate-file');
var nconf = require('nconf')

// var args = null;
// try {
//     args = fs.readFileSync('.env','utf8');
//     args = args.trim();
// } catch (err) {
//     console.log(err);
// }

let LOGGLY_TOKEN = nconf.get( 'LOGGLY_TOKEN' )
let LOGGLY_TAGS  = nconf.get( 'LOGGLY_TAGS'  )
let subdomain    = nconf.get( 'LOGGLY_SUBDOMAIN'  )

winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {'timestamp':true});
winston.add(winston.transports.Loggly, {
    token: LOGGLY_TOKEN,
    subdomain: subdomain,
    tags: LOGGLY_TAGS,
    json: true,
    level: 'verbose',
    timestamp:true
});
winston.add(winston.transports.DailyRotateFile,{
    filename: './logs/log',
    datePattern: 'yyyy-MM-dd.',
    prepend: true,
    level: 'verbose',
});

module.exports = winston;