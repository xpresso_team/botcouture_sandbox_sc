var _ = require('lodash')
var builder = require('botbuilder')
var crypto = require('crypto')
var Constants = require('../constants')
var request = require('superagent')
var nconf = require('nconf')
var uniRest = require('unirest')
var winston = require('./loggly/LoggerUtil')
var TextService = require('./TextService')
var CacheService = require('./CacheService')


let NLP_URL = nconf.get('NLP_URL')
let NLP_PORT = nconf.get('NLP_PORT')
let NLP_DB_PORT = nconf.get('NLP_DB_PORT')
let GENERIC_API_HOST = nconf.get('GENERIC_API_HOST')
//let INTENT_HOST = 'http://' + NLP_URL + ':' + NLP_PORT+"/intent/"
let INTENT_HOST = 'http://' + NLP_URL + "/nlpstgintent/"
//let STRUCTURED_SEARCH_HOST = 'http://' + NLP_URL + ':' + NLP_DB_PORT+"/structuredSearch/"
let STRUCTURED_SEARCH_HOST = 'http://' + NLP_URL + "/nlpstgstructuredSearch/"
//let GENERIC_SEARCH_HOST = 'http://' + NLP_URL + ':' + NLP_DB_PORT+"/genericAPI/"
let GENERIC_SEARCH_HOST = 'http://' + GENERIC_API_HOST + "/genericAPI/"
let VISION_HOST = 'http://' + nconf.get('VISION_HOST') + nconf.get("VISION_EXT")
let Collection_HOST = 'http://' + nconf.get('Collection_HOST')
let WIT_AI_KEY = nconf.get('WIT_AI_KEY')
let WIT_AI_HOST = "http://" + nconf.get('WIT_AI_HOST')

var clientid_alt= {
  "express"       : "exp",
  "forever21"     : "f21",
  "kohl's"        : "kls",
  "lkbennett"     : "lkb",
  "macy's"        : "2",
  "nordstorm"     : "ns",
  "renttherunway" : "rtr"
}
//// This function is depreciated after controller service

/*
 structuredSearch = userData => {


 let str = STRUCTURED_SEARCH_HOST+'?entity=' + userData.entity + '&gender=' + userData.gender.join(',')
 str += userData.price && userData.price.length ? '&price=' + userData.price.join(',') : ''
 str += userData.subcategory && userData.subcategory.length ? '&subcategory=' + userData.subcategory.join(',') : ''
 str += userData.features && userData.features.length ? '&features=' + userData.features.join(',') : ''
 str += userData.brand && userData.brand.length ? '&brand=' + userData.brand.join(',') : ''
 str += userData.details && userData.details.length ? '&details=' + userData.details.join(',') : ''
 str += userData.colorsavailable && userData.colorsavailable.length ? '&colorsavailable=' + userData.colorsavailable.join(',') : ''
 str += userData.xc_category && userData.xc_category.length ? '&xc_hierarchy_str=' + userData.xc_category.join(',') : ''
 str += userData.size && userData.size.length ? '&size=' + userData.size.join(',') : ''
 str += '&src='+nconf.get('client_name')

 // console.log('STRUCTURED SEARCH: ' + str)
 winston.log('info', 'STRUCTURED SEARCH: ' + str)
 let startTime = process.hrtime()
 let elapsed = null

 let key = crypto.createHash( 'md5' ).update( str ).digest( 'hex' )
 let val = CacheService.get( key )
 if ( val ) {
 elapsed = process.hrtime(startTime)[1] / 1000000
 winston.log('warn', 'Time taken to get structuredSearch API ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
 return Promise.resolve(val)
 }

 return request.get(encodeURI(str))
 .then( res => {
 elapsed = process.hrtime(startTime)[1] / 1000000
 winston.log('warn', 'Time taken to get structuredSearch API ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
 let val = JSON.parse(res.text)
 CacheService.set( key, val )
 return val
 })

 }*/


structuredSearch = (userData, session) => {


    let controller_host = nconf.get('controller')
    let url = controller_host

    let address
    let userId
    let page_id
    let channel_id
    let is_personalized = false
    let client_name = userData.client_name || []

    if (!_.isString(session)) {
        address = session.message.address
        userId = address.user.id
        page_id = address.bot.id
        channel_id = address.channelId
        is_personalized = !session.userData.guestMode
    } else {

        address = ""
        userId = ""
        page_id = ""
        channel_id = ""
    }


    let {entity, price, subcategory, features, brand, details, colorsavailable, gender, size, xc_category} = userData
    let intent = {entity, price, subcategory, features, brand, details, colorsavailable, gender, size, xc_category,client_name}
    console.log("\n"+intent)
    console.log("\n"+client_name)
    let qparameter = intent
    let postItem =
        {
            "is_personalized": is_personalized,
            "psid": userId,
            "page_id": page_id,
            "client_id": "",
            "client_name": client_name,
            "channel_id": channel_id,
            "intent": intent,
            "qtype": "nlp",
            "qcontext": "/nlpstgstructuredSearch/",
            "qmethod": "GET",
            "qparameter": qparameter,
        }

    var str = url + JSON.stringify(postItem)

    console.log('Structured searach query', str)
    winston.log('info', 'STRUCTURED SEARCH: ' + str)
    let startTime = process.hrtime()
    let elapsed = null

    let key = crypto.createHash('md5').update(JSON.stringify(str)).digest('hex')
    let val = CacheService.get(key)
/*
    if (val) {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get structuredSearch API ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        return Promise.resolve(val)
    }
*/


    return request.post(url).send(postItem)
        .then(res => {
            elapsed = process.hrtime(startTime)[1] / 1000000
            winston.log('warn', 'Time taken to get structuredSearch API ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
            let val = JSON.parse(res.text)
            CacheService.set(key, val)
            //console.log("data obtained from structured search is ", val)
            return val
        })

}

/**
 * Social URLS
 */
/*getSocialVisualSearchResult = (imgurl, gender, session) => {
 let url = VISION_HOST + 'bot_vsl/?fashion_url=' + encodeURIComponent(imgurl)
 if (gender && gender.length > 0) {
 url += '&gender=' + gender.join(',')
 }

 console.log('Socail URL API :' + url)
 winston.log('info', 'Socail URL API :' + url)
 var startTime = process.hrtime();
 try {
 let elapsed = null

 let key = crypto.createHash('md5').update(url).digest('hex')
 let val = CacheService.get(key)
 if (val) {
 elapsed = process.hrtime(startTime)[1] / 1000000
 winston.log('warn', "Time taken to get VISUAL SOCIAL Search result of social url " + imgurl + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s")
 return Promise.resolve(val)
 }
 return request.get(url).then(res => {
 if (res.statusCode === 200) {
 var elapsed = process.hrtime(startTime)[1] / 1000000;
 winston.log('warn', "Time taken to get VISUAL SOCIAL Search result of social url " + imgurl + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
 let val = JSON.parse(res.text)
 CacheService.set(key, val)
 return val
 } else {
 builder.Prompts.choice(session, TextService.text('Api failure message'), [Constants.Labels.Introduction, Constants.Labels.Help], {
 maxRetries: 0,
 listStyle: builder.ListStyle.button
 })
 session.endDialog()
 }
 })
 } catch (err) {
 builder.Prompts.choice(session, TextService.text('Api failure message'), [Constants.Labels.Introduction, Constants.Labels.Help], {
 maxRetries: 0,
 listStyle: builder.ListStyle.button
 })
 session.endDialog()


 }
 },*/

getSocialVisualSearchResult = (imgurl, gender2,client_name, session) => {

    let NODE = session.userData.currentNode

    let userData = session.userData[NODE]

    let controller_host = nconf.get('controller')
    let url = controller_host


    let address
    let userId
    let page_id
    let channel_id
    let client_id
    let is_personalized = false

    if (!_.isString(session)) {
        address = session.message.address
        userId = address.user.id
        page_id = address.bot.id
        channel_id = address.channelId
        is_personalized = !session.userData.guestMode
    } else {

        address = ""
        userId = ""
        page_id = ""
        channel_id = ""
    }

    if(_.isArray(client_name)) {
      client_id = client_name.map(val => {
        console.log(val)
        let id
        if(clientid_alt.hasOwnProperty(val)) {
            id=clientid_alt[val]
            console.log("\n",id)
        } else {
          id = []
        }
        return id
      })
    } else {
      let id
      if(clientid_alt.hasOwnProperty(client_name)) {
          id=clientid_alt[client_name]
          console.log("\n",id)
      } else {
        id = []
      }
      client_id = id
    }

    let {price, subcategory, features, brand, details, colorsavailable, gender, size, xc_category} = userData
    let intent = {price, subcategory, features, brand, details, colorsavailable, gender, size, xc_category}
    //console.log(intent)


    let qparameter
    if(client_name.length>0) {
      qparameter = {
          image_url: imgurl,
          gender: gender2,
          client_name : client_name
      }
    } else {
      qparameter = {
          image_url : imgurl,
          gender : gender2,
      }
    }

    let postItem =
        {
            "is_personalized": is_personalized,
            "psid": userId,
            "page_id": page_id,
            "client_id": client_id,
            "client_name": client_name,
            "channel_id": channel_id,
            "intent": intent,
            "qtype": "vision",
            "qcontext": "/bot_vsl/",
            "qmethod": "GET",
            "qparameter": qparameter
        }


    var str = url + JSON.stringify(postItem)

    console.log('Visual Search social Image API api call', str)
    winston.log('info', 'Visual Search social Image API api call: ' + str)
    let startTime = process.hrtime()
    let elapsed = null

    let key = crypto.createHash('md5').update(JSON.stringify(str)).digest('hex')
    let val = CacheService.get(key)
   /* if (val) {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get VISUAL Search social result Image API Results call ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        return Promise.resolve(val)
    }*/

    try {

        return request.post(url).send(postItem)
            .then(res => {

                if (res.statusCode === 200) {

                    var elapsed = process.hrtime(startTime)[1] / 1000000;
                    winston.log('warn', "Time taken to get VISUAL Search social Image result of url " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
                    let val = JSON.parse(res.text)
                    CacheService.set(key, val)
                    return val
                } else {
                    builder.Prompts.choice(session, TextService.text('Api failure message'), [Constants.Labels.Introduction, Constants.Labels.Help], {
                        maxRetries: 0,
                        listStyle: builder.ListStyle.button
                    })
                    session.endDialog()
                }
            })
    } catch (err) {
        builder.Prompts.choice(session, TextService.text('Api failure message'), [Constants.Labels.Introduction, Constants.Labels.Help], {
            maxRetries: 0,
            listStyle: builder.ListStyle.button
        })
        session.endDialog()
    }

},


    /**
     * Visual URLs
     */
    /*    getVisualSearchResult = (imgurl, gender, session) => {
     let url = VISION_HOST + 'bot_vsu/?image_url=' + encodeURIComponent(imgurl)
     if (gender && gender.length > 0) {
     url += '&gender=' + gender.join(',')
     }
     console.log('Image getVisualSearchResult API', url)
     winston.log('info', 'Image getVisualSearchResult API :' + url)
     var startTime = process.hrtime();
     try {

     let elapsed = null

     let key = crypto.createHash('md5').update(url).digest('hex')
     let query = null
     let val = CacheService.get(key)
     if (val) {
     elapsed = process.hrtime(startTime)[1] / 1000000
     winston.log('warn', "Time taken to get VISUAL Search result of url " + imgurl + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s")
     return Promise.resolve(val)
     }
     return request.get(url).then(res => {
     if (res.statusCode === 200) {
     console.log(res.statusCode)
     var elapsed = process.hrtime(startTime)[1] / 1000000;
     winston.log('warn', "Time taken to get VISUAL Search result of url " + imgurl + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
     let val = JSON.parse(res.text)
     CacheService.set(key, val)
     return val
     } else {
     builder.Prompts.choice(session, TextService.text('Api failure message'), [Constants.Labels.Introduction, Constants.Labels.Help], {
     maxRetries: 0,
     listStyle: builder.ListStyle.button
     })
     session.endDialog()
     }
     })
     } catch (err) {
     builder.Prompts.choice(session, TextService.text('Api failure message'), [Constants.Labels.Introduction, Constants.Labels.Help], {
     maxRetries: 0,
     listStyle: builder.ListStyle.button
     })
     session.endDialog()


     }
     },*/


    getVisualSearchResult = (imgurl, gender2,client_name, session) => {

        let NODE = session.userData.currentNode

        let userData = session.userData[NODE]

        let controller_host = nconf.get('controller')
        let url = controller_host


        let address
        let userId
        let page_id
        let channel_id
        let client_id
        let is_personalized = false

        if (!_.isString(session)) {
            address = session.message.address
            userId = address.user.id
            page_id = address.bot.id
            channel_id = address.channelId
            is_personalized = !session.userData.guestMode
        } else {

            address = ""
            userId = ""
            page_id = ""
            channel_id = ""
        }

        if(_.isArray(client_name)) {
          client_id = client_name.map(val => {
            console.log(val)
            let id
            if(clientid_alt.hasOwnProperty(val)) {
                id=clientid_alt[val]
                console.log("\n",id)
            } else {
              id = []
            }
            return id
          })
        } else {
          let id
          if(clientid_alt.hasOwnProperty(client_name)) {
              id=clientid_alt[client_name]
              console.log("\n",id)
          } else {
            id = []
          }
          client_id = id
        }


        let {price, subcategory, features, brand, details, colorsavailable, gender, size, xc_category} = userData
        let intent = {price, subcategory, features, brand, details, colorsavailable, gender, size, xc_category}
        //console.log(intent)

        let qparameter
        if(client_id.length>0) {
          qparameter = {
              image_url: imgurl,
              gender: gender2,
              client_name : client_name
          }
        } else {
          qparameter = {
              image_url : imgurl,
              gender : gender2,
          }
        }

        let postItem =
            {
                "is_personalized": is_personalized,
                "psid": userId,
                "page_id": page_id,
                "client_id": client_id,
                "client_name": client_name,
                "channel_id": channel_id,
                "intent": intent,
                "qtype": "vision",
                "qcontext": "/bot_vsu/",
                "qmethod": "GET",
                "qparameter": qparameter
            }


        var str = url + JSON.stringify(postItem)

        console.log('Visual Search Image API api call', str)
        winston.log('info', 'Visual Search Image API api call: ' + str)
        let startTime = process.hrtime()
        let elapsed = null

        let key = crypto.createHash('md5').update(JSON.stringify(str)).digest('hex')
        let val = CacheService.get(key)
       /* if (val) {
            elapsed = process.hrtime(startTime)[1] / 1000000
            winston.log('warn', 'Time taken to get VISUAL Search result Image API Results call ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
            return Promise.resolve(val)
        }*/

        try {

            return request.post(url).send(postItem)
                .then(res => {
                    if (res.statusCode === 200) {

                        var elapsed = process.hrtime(startTime)[1] / 1000000;
                        winston.log('warn', "Time taken to get VISUAL Search Image result of url " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
                      //  console.log("actual results is ",res)
                        let val = JSON.parse(res.text)
                      //  console.log("results obtsined after api call is ",val)
                        CacheService.set(key, val)
                        return val
                    } else {
                        builder.Prompts.choice(session, TextService.text('Api failure message'), [Constants.Labels.Introduction, Constants.Labels.Help], {
                            maxRetries: 0,
                            listStyle: builder.ListStyle.button
                        })
                        session.endDialog()
                    }
                })
        } catch (err) {
            builder.Prompts.choice(session, TextService.text('Api failure message'), [Constants.Labels.Introduction, Constants.Labels.Help], {
                maxRetries: 0,
                listStyle: builder.ListStyle.button
            })
            session.endDialog()
        }

    },

    /**
     * Visual search with category
     */
    // TODO: Think of better name
    /*    getAltVisualSearchResult = (category, imageUrl, gender, session) => {
     console.log("Startin ALT visual search api query: ")
     let url = VISION_HOST + 'bot_vsk/?upkey=' + encodeURIComponent(imageUrl) + '&category=' + category
     if (gender && gender.length > 0) {
     url += '&gender=' + gender.join(',')
     }
     console.log('Image API', url)
     winston.log('info', 'ALT Image API :' + url)
     var startTime = process.hrtime();
     try {

     let elapsed = null

     let key = crypto.createHash('md5').update(url).digest('hex')
     let val = CacheService.get(key)
     if (val) {
     elapsed = process.hrtime(startTime)[1] / 1000000
     winston.log('warn', "Time taken to get ALT VISUAL Search result of url " + imageUrl + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + " s")
     return Promise.resolve(val)
     }
     return request.get(url).then(res => {
     if (res.statusCode === 200) {
     var elapsed = process.hrtime(startTime)[1] / 1000000;
     winston.log('warn', "Time taken to get ALT VISUAL Search result of url " + imageUrl + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
     let val = JSON.parse(res.text)
     CacheService.set(key, val)
     return val
     } else {
     builder.Prompts.choice(session, TextService.text('Api failure message'), [Constants.Labels.Introduction, Constants.Labels.Help], {
     maxRetries: 0,
     listStyle: builder.ListStyle.button
     })
     session.endDialog()
     }

     })
     } catch (err) {
     builder.Prompts.choice(session, TextService.text('Api failure message'), [Constants.Labels.Introduction, Constants.Labels.Help], {
     maxRetries: 0,
     listStyle: builder.ListStyle.button
     })
     session.endDialog()
     }

     },*/




    getAltVisualSearchResult = (category, imageUrl, gender2, session) => {

        let NODE = session.userData.currentNode

        let userData = session.userData[NODE]

        let controller_host = nconf.get('controller')
        let url = controller_host

        let client_name = nconf.get('client_name')

        let address
        let userId
        let page_id
        let channel_id
        let is_personalized = false

        if (!_.isString(session)) {
            address = session.message.address
            userId = address.user.id
            page_id = address.bot.id
            channel_id = address.channelId
            is_personalized = !session.userData.guestMode
        } else {

            address = ""
            userId = ""
            page_id = ""
            channel_id = ""
        }


        let {price, subcategory, features, brand, details, colorsavailable, gender, size, xc_category} = userData
        let intent = {price, subcategory, features, brand, details, colorsavailable, gender, size, xc_category}
        //console.log(intent)


        let qparameter = {
            upkey: imageUrl,
            category: category,
            gender: gender2,
        }

        let postItem =
            {
                "is_personalized": is_personalized,
                "psid": userId,
                "page_id": page_id,
                "client_id": client_id,
                "client_name": client_name,
                "channel_id": channel_id,
                "intent": intent,
                "qtype": "vision",
                "qcontext": "/bot_vsk/",
                "qmethod": "GET",
                "qparameter": qparameter
            }


        var str = url + JSON.stringify(postItem)

        console.log('ALT Visual search Image API api call', str)
        winston.log('info', 'Alt Visual social Image API api call: ' + str)
        let startTime = process.hrtime()
        let elapsed = null

        let key = crypto.createHash('md5').update(JSON.stringify(str)).digest('hex')
        let val = CacheService.get(key)
       /* if (val) {
            elapsed = process.hrtime(startTime)[1] / 1000000
            winston.log('warn', 'Time taken to get ALT VISUAL Search result Image API Results call ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
            return Promise.resolve(val)
        }*/

        try {

            return request.post(url).send(postItem)
                .then(res => {

                    if (res.statusCode === 200) {

                        var elapsed = process.hrtime(startTime)[1] / 1000000;
                        winston.log('warn', "Time taken to get  ALT VISUAL Search Image result of url " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
                        let val = JSON.parse(res.text)
                        CacheService.set(key, val)
                        return val
                    } else {
                        builder.Prompts.choice(session, TextService.text('Api failure message'), [Constants.Labels.Introduction, Constants.Labels.Help], {
                            maxRetries: 0,
                            listStyle: builder.ListStyle.button
                        })
                        session.endDialog()
                    }
                })
        } catch (err) {
            builder.Prompts.choice(session, TextService.text('Api failure message'), [Constants.Labels.Introduction, Constants.Labels.Help], {
                maxRetries: 0,
                listStyle: builder.ListStyle.button
            })
            session.endDialog()
        }

    }

/**
 * View Similar button
 */
/*getSimilarItems = (sku, session) => {
 var url = VISION_HOST + 'bot_vsi/?xc_sku=' + encodeURIComponent(sku)
 console.log('getsimilar items- ', url)
 winston.log('info', 'Visual similar Image API :' + url)
 var startTime = process.hrtime();
 try {
 return request.get(url).then(res => {
 if (res.statusCode === 200) {

 var elapsed = process.hrtime(startTime)[1] / 1000000;
 winston.log('warn', "Time taken to get  VISUAL SIMILAR Search result of url " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
 return JSON.parse(res.text)

 } else {
 builder.Prompts.choice(session, TextService.text('Api failure message'), [Constants.Labels.Introduction, Constants.Labels.Help], {
 maxRetries: 0,
 listStyle: builder.ListStyle.button
 })
 session.endDialog()
 }
 })

 } catch (err) {
 builder.Prompts.choice(session, TextService.text('Api failure message'), [Constants.Labels.Introduction, Constants.Labels.Help], {
 maxRetries: 0,
 listStyle: builder.ListStyle.button
 })
 session.endDialog()
 }
 },*/


getSimilarItems = (sku, session) => {

    let NODE = session.userData.currentNode

    let userData = session.userData[NODE]

    let controller_host = nconf.get('controller')
    let url = controller_host

    let client_name = nconf.get('client_name')

    let address
    let userId
    let page_id
    let channel_id
    let is_personalized = false

    if (!_.isString(session)) {
        address = session.message.address
        userId = address.user.id
        page_id = address.bot.id
        channel_id = address.channelId
        is_personalized = !session.userData.guestMode
    } else {

        address = ""
        userId = ""
        page_id = ""
        channel_id = ""
    }


    let {price, subcategory, features, brand, details, colorsavailable, gender, size, xc_category} = userData
    let intent = {price, subcategory, features, brand, details, colorsavailable, gender, size, xc_category}
    //console.log(intent)


    let qparameter = {
        xc_sku: sku
    }

    let postItem =
        {
            "is_personalized": is_personalized,
            "psid": userId,
            "page_id": page_id,
            "client_id": "",
            "client_name": client_name,
            "channel_id": channel_id,
            "intent": intent,
            "qtype": "vision",
            "qcontext": "/bot_vsi/",
            "qmethod": "GET",
            "qparameter": qparameter,
        }


    var str = url + JSON.stringify(postItem)

    console.log('Visual similar Image API api call', str)
    winston.log('info', 'Visual similar Image API api call: ' + str)
    let startTime = process.hrtime()
    let elapsed = null

    let key = crypto.createHash('md5').update(JSON.stringify(str)).digest('hex')
    let val = CacheService.get(key)
   /* if (val) {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get Visual similar Image API Results call ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        return Promise.resolve(val)
    }*/

    try {

        return request.post(url).send(postItem)
            .then(res => {

                if (res.statusCode === 200) {

                    var elapsed = process.hrtime(startTime)[1] / 1000000;
                    winston.log('warn', "Time taken to get  VISUAL SIMILAR Search result of url " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
                    let val = JSON.parse(res.text)
                    CacheService.set(key, val)
                    return val
                } else {
                    builder.Prompts.choice(session, TextService.text('Api failure message'), [Constants.Labels.Introduction, Constants.Labels.Help], {
                        maxRetries: 0,
                        listStyle: builder.ListStyle.button
                    })
                    session.endDialog()
                }
            })
    } catch (err) {
        builder.Prompts.choice(session, TextService.text('Api failure message'), [Constants.Labels.Introduction, Constants.Labels.Help], {
            maxRetries: 0,
            listStyle: builder.ListStyle.button
        })
        session.endDialog()
    }

}


// The Intent query
/*   // Query 1 : Synaptica
 getIntent = (query, session) => {
 let source = 'all'
 var int1URL = INTENT_HOST+'?src=' + encodeURIComponent(source) + '&query=' + encodeURIComponent(query)
 console.log(int1URL)
 winston.log('info', 'Intent API :' + int1URL)
 var startTime = process.hrtime();
 return request.get(int1URL).then(res => {
 var elapsed = process.hrtime(startTime)[1] / 1000000;
 winston.log('warn', "Time taken to get  INTENT of url " + int1URL + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
 return JSON.parse(res.text)
 })
 },*/


getIntent = (query, session) => {
    let controller_host = nconf.get('controller')
    let url = controller_host

    let client_name = nconf.get('client_name')

    let address
    let userId
    let page_id
    let channel_id
    let is_personalized = false

    if (!_.isString(session)) {
        address = session.message.address
        userId = address.user.id
        page_id = address.bot.id
        channel_id = address.channelId
        is_personalized = !session.userData.guestMode
    } else {

        address = ""
        userId = ""
        page_id = ""
        channel_id = ""
    }

    let intent = {}


    let qparameter = {
        query: query
    }
    let postItem =
        {
            "is_personalized": is_personalized,
            "psid": userId,
            "page_id": page_id,
            "client_id": "",
            "client_name": "",
            "channel_id": channel_id,
            "intent": intent,
            "qtype": "nlp",
            "qcontext": "/nlpstgintent/",
            "qmethod": "GET",
            "qparameter": qparameter,
        }


    var str = url + JSON.stringify(postItem)

    console.log('intent  query', str)
    winston.log('info', 'intent query: ' + str)
    let startTime = process.hrtime()
    let elapsed = null

    let key = crypto.createHash('md5').update(JSON.stringify(str)).digest('hex')
    let val = CacheService.get(key)
    /*if (val) {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get intent  query API ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        return Promise.resolve(val)
    }*/


    return request.post(url).send(postItem)
        .then(res => {
            elapsed = process.hrtime(startTime)[1] / 1000000
            winston.log('warn', 'Time taken to get intent  query API ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
            let val = JSON.parse(res.text)
            CacheService.set(key, val)
            console.log("data obtained from intent  query is ", val)
            return val
        })

}



// The emotion Handler

 getEmotion= (query, session) => {
    let controller_host = nconf.get('controller')
    let url = controller_host

    let client_name = nconf.get('client_name')

    let address
    let userId
    let page_id
    let channel_id
    let is_personalized = false

    if (!_.isString(session)) {
        address = session.message.address
        userId = address.user.id
        page_id = address.bot.id
        channel_id = address.channelId
        is_personalized = !session.userData.guestMode
    } else {

        address = ""
        userId = ""
        page_id = ""
        channel_id = ""
    }

    let intent = {}

    let qparameter = {
        query: query
    }
    let postItem =
        {
            "is_personalized": is_personalized,
            "psid": userId,
            "page_id": page_id,
            "client_id": "",
            "client_name": client_name,
            "channel_id": channel_id,
            "intent": intent,
            "qtype": "emotion",
            "qcontext": "",
            "qmethod": "GET",
            "qparameter": qparameter
        }


    var str = url + JSON.stringify(postItem)

    console.log('intent  query', str)
    winston.log('info', 'Emotion query: ' + str)
    let startTime = process.hrtime()
    let elapsed = null

    let key = crypto.createHash('md5').update(JSON.stringify(str)).digest('hex')
    let val = CacheService.get(key)
    /*if (val) {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', "Time taken to get  EMOTION of url  by Wit/RASA " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
        return Promise.resolve(val)
    }*/
    return request.post(url).send(postItem)
        .then(res => {
            elapsed = process.hrtime(startTime)[1] / 1000000
            winston.log('warn', "Time taken to get  EMOTION of url  by Wit/RASA " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
            let val = JSON.parse(res.text)
            CacheService.set(key, val)
            console.log("data obtained from intent  query is ", val)
            return val
        })

},


/*

    // The emption Handler
     getEmotion = query => {
     var startTime = process.hrtime();
     var url = nconf.get('Xpresso_emotion_handler_url') + encodeURI(query) + '&token=' + encodeURI(nconf.get('Xpresso_emotion_handler_token'))
     winston.log('info', 'Emotion API :' + url)
     return request.get(url).then(res => {
     var elapsed = process.hrtime(startTime)[1] / 1000000;
     winston.log('warn', "Time taken to get  EMOTION of url  by WIT " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
     return JSON.parse(res.text)
     })
     },


*/

// The emption Handler
    /*
     getEmotion = query => {
     var mode = nconf.get('EMOTION_HANDLER');
     var resultJSON
     var startTime = process.hrtime();
     if (mode == "rasa") {
     var url = 'http://' + nconf.get('RASA_HANDLER') + ':' + nconf.get('RASA_PORT') + '/parse?token=' + nconf.get('RASA_TOKEN') + '&q=' + encodeURI(query)
     winston.log('info', 'Emotion API :' + url)
     return request.get(url).then(res => {
     var temp_json = res.body;
     //console.log(temp_json)
     resultJSON = {}
     resultJSON["msg_id"] = "12345"
     resultJSON["_text"] = temp_json[0]["_text"]
     //console.log(temp_json)
     resultJSON["entities"] = {}
     resultJSON["entities"]["intent"] = []
     var intent = {}
     intent["confidence"] = temp_json[0]["confidence"]
     //console.log(resultJSON)
     var re = new RegExp('"', 'g');
     //console.log(re)
     //console.log(temp_json[0])
     //console.log(temp_json[0]["intent"])
     //console.log(intent)
     intent["value"] = temp_json[0]["intent"].replace(re, "")
     //console.log(intent)
     resultJSON["entities"]["intent"][0] = intent
     //console.log("new response body is :- ")
     //console.log(resultJSON)

     var elapsed = process.hrtime(startTime)[1] / 1000000;
     winston.log('warn', "Time taken to get  EMOTION of url  by RASA " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
     return resultJSON
     })
     } else {
     var url = 'https://' + nconf.get('WIT_AI_HOST', '') + '/message?access_token=' + nconf.get('WIT_AI_KEY', '') + '&q=' + encodeURI(query)
     winston.log('info', 'Emotion API :' + url)
     return request.get(url).then(res => {
     var elapsed = process.hrtime(startTime)[1] / 1000000;
     winston.log('warn', "Time taken to get  EMOTION of url  by WIT " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
     return JSON.parse(res.text)
     })
     }
     console.log('Wit.ai / rasa-> ', url)

     },*/





// COLLECTIONS
    /* getCollections = (type, gender, session) => {
     let url = Collection_HOST + '/l1/?gender=' + gender + '&type=' + type
     console.log('L1', url)
     winston.log('info', 'Cpllection L1 API :' + url)
     var startTime = process.hrtime();
     return request.get(url).then(res => {
     var elapsed = process.hrtime(startTime)[1] / 1000000;
     winston.log('warn', "Time taken to get  COLLECTION L1 url " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
     return JSON.parse(res.text)
     })
     },*/

    getCollections = (type, gender, session) => {

        let controller_host = nconf.get('controller')
        let url = controller_host

        let client_name = nconf.get('client_name')

        let address
        let userId
        let page_id
        let channel_id
        let is_personalized = false

        if (!_.isString(session)) {
            address = session.message.address
            userId = address.user.id
            page_id = address.bot.id
            channel_id = address.channelId
            is_personalized = !session.userData.guestMode
        } else {

            address = ""
            userId = ""
            page_id = ""
            channel_id = ""
        }


        let intent = {}
        //console.log(intent)


        let qparameter = {
            gender: gender,
            type: type

        }


        let postItem =
            {
                "is_personalized": is_personalized,
                "psid": userId,
                "page_id": page_id,
                "client_id": "",
                "client_name": client_name,
                "channel_id": channel_id,
                "intent": intent,
                "qtype": "collection",
                "qcontext": "/l1/",
                "qmethod": "GET",
                "qparameter": qparameter,
            }


        var str = url + JSON.stringify(postItem)

        console.log('COLLECTION L1 url api call', str)
        winston.log('info', 'COLLECTION L1 url api call: ' + str)
        let startTime = process.hrtime()
        let elapsed = null

        let key = crypto.createHash('md5').update(JSON.stringify(str)).digest('hex')
        let val = CacheService.get(key)
      /*  if (val) {
            elapsed = process.hrtime(startTime)[1] / 1000000
            winston.log('warn', 'Time taken to get COLLECTION L1 url api ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
            return Promise.resolve(val)
        } */


        return request.post(url).send(postItem)
            .then(res => {
                elapsed = process.hrtime(startTime)[1] / 1000000
                winston.log('warn', 'Time taken to get COLLECTION L1 url call ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
                let val = JSON.parse(res.text)
                CacheService.set(key, val)
                return val
            })

    }


/*getCollectionCategories = (type, gender, collectionName) => {
 let url = Collection_HOST + '/l2/?gender=' + encodeURIComponent(gender) + '&' + encodeURIComponent(type) + '=' + encodeURIComponent(collectionName)
 console.log('L2', url)
 winston.log('info', 'Cpllection L2 API :' + url)
 var startTime = process.hrtime();
 return request.get(url).then(res => {
 var elapsed = process.hrtime(startTime)[1] / 1000000;
 winston.log('warn', "Time taken to get  COLLECTION L2 url " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
 return JSON.parse(res.text)
 })
 },*/


getCollectionCategories = (type, gender, collectionName,client_name, session) => {

    let controller_host = nconf.get('controller')
    let url = controller_host


    let address
    let userId
    let page_id
    let channel_id
    let is_personalized = false

    if (!_.isString(session)) {
        address = session.message.address
        userId = address.user.id
        page_id = address.bot.id
        channel_id = address.channelId
        is_personalized = !session.userData.guestMode
    } else {

        address = ""
        userId = ""
        page_id = ""
        channel_id = ""
    }


    let intent = {}
    //console.log(intent)


    let qparameter = {
        gender: gender,
        client_name : client_name,
    }
    qparameter[type] = collectionName


    let postItem =
        {
            "is_personalized": is_personalized,
            "psid": userId,
            "page_id": page_id,
            "client_id": "",
            "client_name": client_name,
            "channel_id": channel_id,
            "intent": intent,
            "qtype": "collection",
            "qcontext": "/l2/",
            "qmethod": "GET",
            "qparameter": qparameter,
        }


    var str = url + JSON.stringify(postItem)

    console.log('COLLECTION L2 url api call', str)
    winston.log('info', 'COLLECTION L2 url api call: ' + str)
    let startTime = process.hrtime()
    let elapsed = null

    let key = crypto.createHash('md5').update(JSON.stringify(str)).digest('hex')
    let val = CacheService.get(key)
/*
    if (val) {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get COLLECTION L2 url api ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        return Promise.resolve(val)
    }
*/


    return request.post(url).send(postItem)
        .then(res => {
            elapsed = process.hrtime(startTime)[1] / 1000000
            winston.log('warn', 'Time taken to get COLLECTION L2 url call ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
            let val = JSON.parse(res.text)
            CacheService.set(key, val)
            return val
        })

}


/*getCollectionItems = (type, collectionName, gender, category) => {
 let url = Collection_HOST + '/l3/?gender=' + encodeURIComponent(gender[0]) + '&category=' + encodeURIComponent(category) + '&' + encodeURIComponent(type) + '=' + encodeURIComponent(collectionName)
 console.log('L3', url)
 winston.log('info', 'Cpllection L3 API :' + url)
 var startTime = process.hrtime();
 return request.get(url).then(res => {
 var elapsed = process.hrtime(startTime)[1] / 1000000;
 winston.log('warn', "Time taken to get  COLLECTION L3 url " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
 return JSON.parse(res.text)
 })
 },*/

getCollectionItems = (type, collectionName, gender2, category, client_name, session) => {
    let NODE = session.userData.currentNode

    let userData = session.userData[NODE]

    let controller_host = nconf.get('controller')
    let url = controller_host

    let address
    let userId
    let page_id
    let channel_id
    let client_id
    let is_personalized = false

    if (!_.isString(session)) {
        address = session.message.address
        userId = address.user.id
        page_id = address.bot.id
        channel_id = address.channelId
        is_personalized = !session.userData.guestMode
    } else {

        address = ""
        userId = ""
        page_id = ""
        channel_id = ""
    }

    let {price, subcategory, features, brand, details, colorsavailable, gender, size, xc_category} = userData
    let intent = {price, subcategory, features, brand, details, colorsavailable, gender, size, xc_category}
    console.log("collection l3 intent is ",intent)

    if(_.isArray(client_name)) {
      client_id = client_name.map(val => {
        console.log(val)
        let id
        if(clientid_alt.hasOwnProperty(val)) {
            id=clientid_alt[val]
            console.log("\n",id)
        } else {
          id = []
        }
        return id
      })
    } else {
      let id
      if(clientid_alt.hasOwnProperty(client_name)) {
          id=clientid_alt[client_name]
          console.log("\n",id)
      } else {
        id = []
      }
      client_id = id
    }
    //console.log(intent)
    let qparameter
    if(client_id.length>0) {
        qparameter = {
            gender: gender2,
            category: category,
            client_id : client_id
        }
    } else {
      qparameter = {
          gender: gender2,
          category: category,
        }
    }
    qparameter[type] = collectionName


    let postItem =
        {
            "is_personalized": is_personalized,
            "psid": userId,
            "page_id": page_id,
            "client_id": client_id,
            "client_name": client_name,
            "channel_id": channel_id,
            "intent": intent,
            "qtype": "collection",
            "qcontext": "/l3/",
            "qmethod": "GET",
            "qparameter": qparameter,
        }


    var str = url + JSON.stringify(postItem)

    console.log('COLLECTION L3 url api call', str)
    winston.log('info', 'COLLECTION L3 url api call: ' + str)
    let startTime = process.hrtime()
    let elapsed = null

    let key = crypto.createHash('md5').update(JSON.stringify(str)).digest('hex')
    let val = CacheService.get(key)
    /*
    if (val) {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get COLLECTION L3 url api ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        return Promise.resolve(val)
    }*/


    return request.post(url).send(postItem)
        .then(res => {
            elapsed = process.hrtime(startTime)[1] / 1000000
            winston.log('warn', 'Time taken to get COLLECTION L3 url call ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
            let val = JSON.parse(res.text)
            CacheService.set(key, val)
            return val
        })

}


/* // GUIDE ME QUERIES
 getGuideMeCategories = gender => {
 // Old API
 // let url = SYNAPTICA_HOST + '/xcCategory/?gender=' + encodeURIComponent(gender)

 // New Generic API
 // 'http://exp.commerce.xpresso.ai:9091/genericAPI/?gender=women&returnField=xc_category'
 let url = GENERIC_SEARCH_HOST + '?gender=' + encodeURIComponent(gender) + '&returnField=xc_category' + '&client_name=' + nconf.get('client_name')

 console.log('GuideMeCategorries', url)
 winston.log('info', 'GuideMeCategorries API :' + url)
 var startTime = process.hrtime();
 let elapsed = null

 return request.get(url).then(res => {
 elapsed = process.hrtime(startTime)[1] / 1000000;
 winston.log('warn', "Time taken to get  GUIDE ME  Categories url " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
 let ret = JSON.parse(res.text)
 // xc_category is double escaped
 ret.Results.xc_category = _.map(ret.Results.xc_category, xc_cat => xc_cat.replace(/\"/g, ''))
 return ret
 })


 },*/


// GUIDE ME QUERIES


getGuideMeCategories = (gender,client_name, session) => {
    let controller_host = nconf.get('controller')
    let url = controller_host

    let address
    let userId
    let page_id
    let channel_id
    let is_personalized = false

    if (!_.isString(session)) {
        address = session.message.address
        userId = address.user.id
        page_id = address.bot.id
        channel_id = address.channelId
        is_personalized = !session.userData.guestMode
    } else {

        address = ""
        userId = ""
        page_id = ""
        channel_id = ""
    }

    let intent = {}
    let qparameter = {
        client_name : client_name,
        gender: gender,
        returnField: "xc_category",

    }
    let postItem =
        {
            "is_personalized": is_personalized,
            "psid": userId,
            "page_id": page_id,
            "client_id": "",
            "client_name": client_name,
            "channel_id": channel_id,
            "intent": intent,
            "qtype": "datalayer",
            "qcontext": "/genericAPI/",
            "qmethod": "GET",
            "qparameter": qparameter,
        }


    var str = url + JSON.stringify(postItem)

    console.log('GUIDE ME  Categories url', str)
    winston.log('info', 'GUIDE ME  Categories url: ' + str)
    let startTime = process.hrtime()
    let elapsed = null

    let key = crypto.createHash('md5').update(JSON.stringify(str)).digest('hex')
    let val = CacheService.get(key)
    /*
    if (val) {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get s GUIDE ME  Categories url API ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        return Promise.resolve(val)
    }*/


    return request.post(url).send(postItem)
        .then(res => {
            elapsed = process.hrtime(startTime)[1] / 1000000
            winston.log('warn', 'Time taken to get GUIDE ME  Categories url API ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
            let val = JSON.parse(res.text)
            val.Results.xc_category = _.map(val.Results.xc_category, xc_cat => xc_cat.replace(/\"/g, ''))
            CacheService.set(key, val)
            // console.log("data obtained from structured search is ", val)
            return val
        })

}


/*
 getGuideMeSizes = (gender, category) => {
 // Old API
 // let url = SYNAPTICA_HOST + '/sizeList/?xc_category=' + encodeURIComponent(category) + '&gender=' + encodeURIComponent(gender)

 // New Generic API
 // 'http://exp.commerce.xpresso.ai:9091/genericAPI/?gender=women&xc_category=shirt&returnField=size'
 let url = GENERIC_SEARCH_HOST + '?gender=' + encodeURIComponent(gender) + '&xc_category=' + encodeURIComponent(category) + '&returnField=size&client_name=' + nconf.get('client_name')

 if (!gender) {
 url = GENERIC_SEARCH_HOST + '?xc_category=' + encodeURIComponent(category) + '&returnField=size&client_name=' + nconf.get('client_name')
 }

 console.log('GuideMeSizes', url)
 winston.log('info', 'GuideMeSizes API :' + url)
 var startTime = process.hrtime();
 return request.get(url).then(res => {
 var elapsed = process.hrtime(startTime)[1] / 1000000;
 winston.log('warn', "Time taken to get  GUIDE ME  Size url " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
 let ret = JSON.parse(res.text)
 // size is double escaped
 ret.Results.size = _.map(ret.Results.size, size => size.replace(/\"/g, ''))
 return ret
 })
 },
 */


getGuideMeSizes = (gender, category, session) => {


    let controller_host = nconf.get('controller')
    let url = controller_host

    let client_name = nconf.get('client_name')

    let address
    let userId
    let page_id
    let channel_id
    let is_personalized = false

    if (!_.isString(session)) {
        address = session.message.address
        userId = address.user.id
        page_id = address.bot.id
        channel_id = address.channelId
        is_personalized = !session.userData.guestMode
    } else {

        address = ""
        userId = ""
        page_id = ""
        channel_id = ""
    }


    let intent = {}
    let qparameter = {

        gender: gender,
        xc_category: category,
        returnField: "size",

    }
    let postItem =
        {
            "is_personalized": is_personalized,
            "psid": userId,
            "page_id": page_id,
            "client_id": "",
            "client_name": client_name,
            "channel_id": channel_id,
            "intent": intent,
            "qtype": "datalayer",
            "qcontext": "/genericAPI/",
            "qmethod": "GET",
            "qparameter": qparameter,
        }


    var str = url + JSON.stringify(postItem)

    console.log('GUIDE ME  Size url', str)
    winston.log('info', 'GUIDE ME  Size url: ' + str)
    let startTime = process.hrtime()
    let elapsed = null

    let key = crypto.createHash('md5').update(JSON.stringify(str)).digest('hex')
    let val = CacheService.get(key)
   /* if (val) {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get s GUIDE ME  Size url API ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        return Promise.resolve(val)
    }*/


    return request.post(url).send(postItem)
        .then(res => {
            elapsed = process.hrtime(startTime)[1] / 1000000
            winston.log('warn', 'Time taken to get GUIDE ME  Size url API ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
            let val = JSON.parse(res.text)
            val.Results.size = _.map(val.Results.size, size => size.replace(/\"/g, ''))
            CacheService.set(key, val)
            // console.log("data obtained from structured search is ", val)
            return val
        })

}


/*getAttributeValue = (gender, category, attribute) => {
 // Old API
 // let url = SYNAPTICA_HOST + '/sizeList/?xc_category=' + encodeURIComponent(category) + '&gender=' + encodeURIComponent(gender)

 // New Generic API
 // 'http://exp.commerce.xpresso.ai:9091/genericAPI/?gender=women&xc_category=shirt&returnField=size'
 console.log("category is ", category)
 let category_final = category
 console.log("category_final before is ", category_final)
 category_final = _.toLower(category_final)
 console.log("category_final after is ", category_final)
 let url = GENERIC_SEARCH_HOST + '?gender=' + encodeURIComponent(gender) + '&xc_category=' + encodeURIComponent(category_final) + '&returnField=' + attribute + '&client_name=' + nconf.get('client_name')

 if (!gender) {
 url = GENERIC_SEARCH_HOST + '?xc_category=' + encodeURIComponent(category_final) + '&returnField=' + attribute + '&client_name=' + nconf.get('client_name')
 }

 console.log('getAttributeValue', url)
 winston.log('info', 'getAttributeValue API :' + url)
 var startTime = process.hrtime();
 return request.get(url).then(res => {
 var elapsed = process.hrtime(startTime)[1] / 1000000;
 winston.log('warn', "Time taken to get  getAttributeValue url " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
 let ret = JSON.parse(res.text)
 console.log("obtained result is ", ret)

 ret.Results.attribute = attribute
 ret.Results.xc_category = [category]
 // size is double escaped
 if (attribute == "size") {
 ret.Results.size = _.map(ret.Results.size, size => size.replace(/\"/g, ''))
 }
 return ret
 })
 },*/


getAttributeValue = (gender, category, attribute, session) => {

    let category_final = category
    //console.log("category_final before is ", category_final)
    category_final = _.toLower(category_final)
    //console.log("category_final after is ", category_final)

    let controller_host = nconf.get('controller')
    let url = controller_host

    let client_name = nconf.get('client_name')

    let address
    let userId
    let page_id
    let channel_id
    let is_personalized = false

    if (!_.isString(session)) {
        address = session.message.address
        userId = address.user.id
        page_id = address.bot.id
        channel_id = address.channelId
        is_personalized = !session.userData.guestMode
    } else {

        address = ""
        userId = ""
        page_id = ""
        channel_id = ""
    }


    let intent = {}
    let qparameter = {

        gender: gender,
        xc_category: category_final,
        returnField: attribute,

    }
    let postItem =
        {
            "is_personalized": is_personalized,
            "psid": userId,
            "page_id": page_id,
            "client_id": "",
            "client_name": client_name,
            "channel_id": channel_id,
            "intent": intent,
            "qtype": "datalayer",
            "qcontext": "/genericAPI/",
            "qmethod": "GET",
            "qparameter": qparameter,
        }


    var str = url + JSON.stringify(postItem)

    console.log('getAttributeValue api call', str)
    winston.log('info', 'getAttributeValue api call: ' + str)
    let startTime = process.hrtime()
    let elapsed = null

    let key = crypto.createHash('md5').update(JSON.stringify(str)).digest('hex')
    let val = CacheService.get(key)
    /*if (val) {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get getAttributeValue api call ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        return Promise.resolve(val)
    }*/


    return request.post(url).send(postItem)
        .then(res => {
            elapsed = process.hrtime(startTime)[1] / 1000000
            winston.log('warn', 'Time taken to get getAttributeValue api call ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
            let val = JSON.parse(res.text)

            val.Results.attribute = attribute
            val.Results.xc_category = [category]
            if (attribute == "price") {
               console.log("Price obtained is",val.Results.xc_category ,val.Results.attribute)
             }
            // size is double escaped
            if (attribute == "size") {

                //console.log("sizes are", val.Results.size)
                val.Results.size = _.map(val.Results.size, size => size.replace(/\"/g, ''))
            }
            if (attribute == "colorsavailable") {
                //console.log("colorsavailable are", val.Results.colorsavailable)
                val.Results.colorsavailable = _.map(val.Results.colorsavailable, colorsavailable => colorsavailable.replace(/\\/g, ''))
                //val.Results.colorsavailable = _.map(val.Results.colorsavailable, colorsavailable => colorsavailable.replace(/\"/g, ''))
            }
            CacheService.set(key, val)
            return val
        })

}


/**
 * Get results
 */
// getGuideMeResults = ( gender, xc_category, size ) => {
/*getGuideMeResults = (state, session) => {
 // Old API
 // let url = SYNAPTICA_HOST + '/sizeList/?xc_category=' + encodeURIComponent(category) + '&gender=' + encodeURIComponent(gender)

 // New Generic API
 // 'http://exp.commerce.xpresso.ai:9091/genericAPI/?gender=women&xc_category=dress&size=m&returnField=xc_sku,image&productDetails=true'
 // let url = SYNAPTICA_HOST + '/genericAPI/?gender=' + encodeURIComponent(gender) + '&xc_category=' + encodeURIComponent(xc_category) + '&size=' + encodeURIComponent(size) +
 // '&returnField=image,product_url,gender,xc_sku,colorsavailable,size,price,productname,subcategory,sku,client_name,brand&productDetails=true'

 let url = GENERIC_SEARCH_HOST + '?'
 //url += state.entity                                          ? '&entity='          + state.entity                    : ''
 url += state.gender && state.gender.length ? '&gender=' + state.gender.join(',') : ''
 url += state.price && state.price.length ? '&price=' + state.price.join(',') : ''
 url += state.subcategory && state.subcategory.length ? '&subcategory=' + state.subcategory.join(',') : ''
 url += state.features && state.features.length ? '&feature=' + state.features.join(',') : ''
 url += state.brand && state.brand.length ? '&brand=' + state.brand.join(',') : ''
 url += state.details && state.details.length ? '&details=' + state.details.join(',') : ''
 url += state.colorsavailable && state.colorsavailable.length ? '&colorsavailable=' + state.colorsavailable.join(',') : ''
 url += state.xc_category && state.xc_category.length ? '&xc_category=' + state.xc_category.join(',') : ''
 url += state.size && state.size.length ? '&size=' + state.size.join(',') : ''
 url += '&returnField=image,product_url,gender,xc_sku,colorsavailable,size,price,productname,subcategory,sku,client_name,brand&productDetails=true' + '&client_name=' + nconf.get('client_name')


 console.log('getGuideMeResults', url)
 winston.log('info', 'getGuideMeResults API :' + url)
 var startTime = process.hrtime();

 let key = crypto.createHash('md5').update(url).digest('hex')
 let val = CacheService.get(key)
 if (val) {
 elapsed = process.hrtime(startTime)[1] / 1000000
 winston.log('warn', "Time taken to get  GUIDE ME Results url " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
 return Promise.resolve(val)
 }

 return request.get(url).then(res => {
 var elapsed = process.hrtime(startTime)[1] / 1000000;
 winston.log('warn', "Time taken to get  GUIDE ME Results url " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
 let ret = JSON.parse(res.text)

 if (ret && ret.Results) {
 ret.Results = ret.Results.Results
 } else {
 ret.Results = []
 }

 CacheService.set(key, ret)
 return ret
 })


 },*/


getGuideMeResults = (state, session) => {

    let NODE = session.userData.currentNode

    let userData = session.userData[NODE]

    let controller_host = nconf.get('controller')
    let url = controller_host

    let client_name = session.userData[NODE].client_name || state.client_name

    let address
    let userId
    let page_id
    let channel_id
    let is_personalized = false

    if (!_.isString(session)) {
        address = session.message.address
        userId = address.user.id
        page_id = address.bot.id
        channel_id = address.channelId
        is_personalized = !session.userData.guestMode
    } else {

        address = ""
        userId = ""
        page_id = ""
        channel_id = ""
    }

    let {price, subcategory, features, brand, details, colorsavailable, gender, size, xc_category} = userData
    let intent = {price, subcategory, features, brand, details, colorsavailable, gender, size, xc_category}
    //console.log(intent)


    let return_field = "image,product_url,gender,xc_sku,colorsavailable,size,price,productname,subcategory,sku,client_name,brand"
    let qparameter = {price, subcategory, features, brand, details, colorsavailable, gender, size, xc_category,client_name}
    qparameter.returnField = return_field
    qparameter.productDetails=true

    let postItem =
        {
            "is_personalized": is_personalized,
            "psid": userId,
            "page_id": page_id,
            "client_id": "",
            "client_name": client_name,
            "channel_id": channel_id,
            "intent": intent,
            "qtype": "datalayer",
            "qcontext": "/genericAPI/",
            "qmethod": "GET",
            "qparameter": qparameter,
        }


    var str = url + JSON.stringify(postItem)

    console.log('GUIDE ME Results api call', str)
    winston.log('info', 'GUIDE ME Results api call: ' + str)
    let startTime = process.hrtime()
    let elapsed = null

    let key = crypto.createHash('md5').update(JSON.stringify(str)).digest('hex')
    let val = CacheService.get(key)
    /*if (val) {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get GUIDE ME Results call ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        return Promise.resolve(val)
    }*/


    return request.post(url).send(postItem)
        .then(res => {
            elapsed = process.hrtime(startTime)[1] / 1000000
            winston.log('warn', 'Time taken to get GUIDE ME Results call ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
            let val = JSON.parse(res.text)


            if (val && val.Results) {
                val.Results = val.Results.Results
            } else {
                val.Results = []
            }
            CacheService.set(key, val)
            return val
        })

}


senderActions = (senderID, token, flag) => {
    var label = flag ? "typing_on" : "typing_off";
    return uniRest.post('https://graph.facebook.com/v2.6/me/messages?access_token=' + token)
        .headers({'Content-Type': 'application/json'})
        .send({
            "recipient": {
                "id": senderID
            },
            "sender_action": label
        })
        .end(function (response) {
            // let currentUser = DataUtil.userMap.get(senderID);
            // winston.log('info',`Typing indicator for ${senderID} is ${flag}`);
            // winston.log('info', `Typing indicator for ${senderID} ` + currentUser.name + ` is ${flag}`);
        });
},


    /**
     * Create a default set of category buttons in case vision api does not
     * returs a list of categories
     */
    createCategoryButtons = function () {

        return image_category = [
            "BAG", "BELTS", "COATS AND JACKETS", "DRESS", "FOOTWEAR", "GLOVES",
            "HOODY", "JEANS", "JEWELLERY", "JUMPSUIT", "LEGGINGS", "RAINCOATS AND UMBRELLAS", "SHIRT",
            "SHORTS", "SKIRT", "SLEEPWEAR", "SOCKS", "SUIT", "SWEATER", "SWIMWEAR", "T-SHIRT", "TIE",
            "TOP", "TROUSERS", "UNDERGARMENTS", "WATCH"
        ]

    },

    getUniqueAttributeValues = (queryObj, returnField) => {

        let queryFields = ''
        _.each(queryObj, (val, key) => {
            if (val && key === 'entity') {
                queryFields += key + '=' + val + '&'
            }
            if (val && _.isArray(val) && val.length > 0) {
                queryFields += key + '=' + val[0] + '&'
            }
        })


        //let url = `http://${ GENERIC_API_HOST }/genericAPI/?gender=men&xc_category=shirt&colorsavailable=black&returnField=${ returnField }`
        let url = `http://${ GENERIC_API_HOST }/genericAPI/?${ queryFields }returnField=${ returnField }`
        url = url + '&client_name=' + nconf.get('client_name')

        console.log('Generic API ', url)
        winston.log('info', 'Generic  API :' + url)
        var startTime = process.hrtime();
        return request.get(url).then(res => {
            var elapsed = process.hrtime(startTime)[1] / 1000000;
            winston.log('warn', "Time taken to get  Generic API url " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
            //console.log("resturend is ",JSON.parse(res.text))
            return JSON.parse(res.text)
        })


    }


/*
getProductDetails = (sku, type) => {
  let url = ""
  if (type == "uniq") {
    let url = `${GENERIC_SEARCH_HOST}?xc_sku=${xc_sku}&returnField=image,product_url,details,features,gender,xc_sku,colorsavailable,size,price,productname,subcategory,sku,client_name,brand&productDetails=true&client_name=`
    url = `${GENERIC_SEARCH_HOST}?xc_sku=${sku}&returnField=image,product_url,details,features,gender,xc_sku,colorsavailable,size,price,productname,subcategory,sku,client_name,brand,group_sku&productDetails=true&client_name=`
  } else if (type == "multi") {
  url = `${GENERIC_SEARCH_HOST}?group_sku=${sku}&returnField=image,product_url,details,features,gender,xc_sku,colorsavailable,size,price,productname,subcategory,sku,client_name,brand,group_sku&productDetails=true&client_name=`
  }
 console.log('getProductDetails', url)

 return request.get(url).then(result => {
 return JSON.parse(result.text)
 })

 }

}
*/


getProductDetails = (xc_sku, session) => {

    let controller_host = nconf.get('controller')
    let url = controller_host

    let client_name = nconf.get('client_name')

    let address
    let userId
    let page_id
    let channel_id
    let is_personalized = false
    let qparameter = {}

    if (!_.isString(session)) {
        qparameter.xc_sku = xc_sku
        address = session.message.address
        userId = address.user.id
        page_id = address.bot.id
        channel_id = address.channelId
        is_personalized = !session.userData.guestMode
    } else {
        if (session == "uniq") {
            qparameter.xc_sku = xc_sku
        } else if (session == "multi") {
            qparameter.group_sku = xc_sku

        }
        address = ""
        userId = ""
        page_id = ""
        channel_id = ""
    }

    let intent = {}
    //console.log(intent)

    let return_field = "image,product_url,details,features,gender,xc_sku,colorsavailable,size,price,productname,subcategory,sku,client_name,brand,group_sku"
    qparameter.returnField = return_field
    qparameter.productDetails=true

    console.log(qparameter)



    let postItem =
        {
            "is_personalized": is_personalized,
            "psid": userId,
            "page_id": page_id,
            "client_id": "",
            "client_name": client_name,
            "channel_id": channel_id,
            "intent": intent,
            "qtype": "datalayer",
            "qcontext": "/genericAPI/",
            "qmethod": "GET",
            "qparameter": qparameter,
        }


    var str = url + JSON.stringify(postItem)

    console.log('getProductDetails api call', str)
    winston.log('info', 'getProductDetails api call: ' + str)
    let startTime = process.hrtime()
    let elapsed = null

    let key = crypto.createHash('md5').update(JSON.stringify(str)).digest('hex')
    let val = CacheService.get(key)
    /*if (val) {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get getProductDetails api call ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        return Promise.resolve(val)
    }*/


    return request.post(url).send(postItem)
        .then(res => {
            elapsed = process.hrtime(startTime)[1] / 1000000
            winston.log('warn', 'Time taken to get getProductDetails api call ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
            let val = JSON.parse(res.text)
            console.log(val)
            CacheService.set(key, val)
            return val
        })

}


getallRetailers = (user) => {
    console.log('entered api call for getallRetailers')
    let controller_host = nconf.get('controller')
    let url = controller_host

    let userId
    let page_id
    let channel_id
    let is_personalized = false

    if (!_.isString(user)) {
        userId = user.userId
        page_id = user.botId
        channel_id = user.channelId
    } else {

        address = ""
        userId = ""
        page_id = ""
        channel_id = ""
    }

    let intent = {}
    let qparameter = {
        returnField: "client_name",

    }
    let postItem =
        {
            "is_personalized": is_personalized,
            "psid": userId,
            "page_id": page_id,
            "client_id": "",
            "client_name": "",
            "channel_id": channel_id,
            "intent": intent,
            "qtype": "datalayer",
            "qcontext": "/genericAPI/",
            "qmethod": "GET",
            "qparameter": qparameter,
        }


    var str = url + JSON.stringify(postItem)

    console.log('Get all Retailers list', str)
    winston.log('info', 'GET ALL  Retailers url: ' + str)
    let startTime = process.hrtime()
    let elapsed = null

    /*
    let key = crypto.createHash('md5').update(JSON.stringify(str)).digest('hex')
    let val = CacheService.get(key)

    if (val) {
        elapsed = process.hrtime(startTime)[1] / 1000000
        winston.log('warn', 'Time taken to get all Retailers url API ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
        return Promise.resolve(val)
    }
    */

    return request.post(url).send(postItem)
        .then(res => {
            elapsed = process.hrtime(startTime)[1] / 1000000
            winston.log('warn', 'Time taken to get all Retailers API ' + str + ' is\t' + ( process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000 ) + ' s')
            console.log('\n'+res.text+"\n")
            let val = JSON.parse(res.text)
            console.log(val+"\n")
            //let val = JSON.parse(res)
            //val.client_name = _.map(val.client_name, client_name => client_name.replace(/\"/g, ''))
            //CacheService.set(key, val)
            // console.log("data obtained from structured search is ", val)
            let retarr = val.Results.client_name
            let ind = retarr.indexOf("macys")
            retarr.splice(ind,1)
            return retarr
        })

}

module.exports = {
    // NLP & GUIDE-ME
    structuredSearch: structuredSearch,
    getSocialVisualSearchResult: getSocialVisualSearchResult,
    getVisualSearchResult: getVisualSearchResult,
    getAltVisualSearchResult: getAltVisualSearchResult,
    getSimilarItems: getSimilarItems,
    getIntent: getIntent,
    getEmotion: getEmotion,
    // COLLECTIONS
    getCollections: getCollections,
    getCollectionCategories: getCollectionCategories,
    getCollectionItems: getCollectionItems,
    // GUIDE ME
    getGuideMeCategories: getGuideMeCategories,
    getGuideMeSizes: getGuideMeSizes,
    getGuideMeResults: getGuideMeResults,
    // MISC
    senderActions: senderActions,
    createCategoryButtons: createCategoryButtons,
    getUniqueAttributeValues: getUniqueAttributeValues,
    getProductDetails: getProductDetails,
    getAttributeValue: getAttributeValue,
    getallRetailers : getallRetailers,
}
