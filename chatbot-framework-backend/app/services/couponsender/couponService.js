var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../../constants')
const uuidV4 = require('uuid/v4')
var request = require('superagent')
var nconf = require('nconf')
var CouponSender = require('./Sender')
var toTitleCase = require('to-title-case')
var winston = require('./../loggly/LoggerUtil')

let CouponSender_URL = nconf.get('CouponSender_URL')


/**
 * Sends Coupon
 */
couponSender = (bot) => {

    let i = 0
    let j = i + 1


    for (i = 0; i < j; i++) {

        let page
        if (i == 0) {
            page = i * 1000

        } else {

            page = (i * 1000) + 1

        }

        let pageSize = (i + 1) * 1000

        console.log(" value of i is ", i, j)


        couponRetriver(page, pageSize).then(results => {

            let coupons = results.Results

            if ((coupons) && (coupons.length > 0)) {

                console.log(page + " Set of coupons retrieved with " + coupons.length + " no of coupons")
                console.log(coupons)

                for (let k = 0; k < coupons.length; k++) {

                    console.log(coupons[k])
                    let promotions = coupons[k].promotions

                    if (nconf.get('sending_method') == "FB") {

                        let elements = []

                        for (let l = 0; l < Math.min(promotions.length, 10); l++) {
                            let element = {}

                            element.title = toTitleCase(coupons[k].promotions[l].coupon_name)
                            element.image_url = coupons[k].promotions[l].promotion_image
                            element.subtitle = "Coupon Code :- " + coupons[k].promotions[l].coupon_code
                            element.default_action = {}
                            element.default_action.type = "web_url"
                            element.default_action.url = coupons[k].promotions[l].promotion_url

                            console.log("creating  card", element)
                            elements.push(element)

                        }
                        CouponSender.sendCoupon2(coupons[k].psid, elements)


                    } else if (nconf.get('sending_method') == "NATIVE") {

                        let address = Constants.user_address
                        address.channelId = coupons[k].channel_id
                        address.user.id = coupons[k].psid
                        address.user.name = coupons[k].first_name + " " + coupons[k].last_name
                        address.conversation.id = address.user.id + "-" + address.bot.id

                        let cards = []

                        for (let l = 0; l < Math.min(promotions.length, 10); l++) {
                            let card = new builder.HeroCard()
                                .title(toTitleCase(coupons[k].promotions[l].coupon_name))
                                .subtitle()
                                .text("Coupon Code :- " + coupons[k].promotions[l].coupon_code)
                                .images([
                                    new builder.CardImage().url(coupons[k].promotions[l].promotion_image)])

                                .tap(builder.CardAction.openUrl(null, coupons[k].promotions[l].promotion_url))


                            cards.push(card)

                        }
                        console.log("creating  card", cards)
                        CouponSender.sendCoupon1(address, bot, cards)
                    } else {
                        console.log("  Coupons not send as no correct sending method send in config")
                    }
                }

            }
            else {
                j = i - 1
                console.log("All coupons retrived")
            }

        }).catch(err => {
            j = i - 1
            console.log("Some error happened in retriving coupons ", err)


        })
    }

}

couponRetriver = (page, pageSize) => {

    let url = CouponSender_URL + '?page=' + encodeURIComponent(page) + '&pageSize=' + encodeURIComponent(pageSize)
    console.log('couponRetriver url call is ', url)
    winston.log('info', 'couponRetriver url call is :' + url)
    var startTime = process.hrtime();
    return request.get(url).then(res => {
        var elapsed = process.hrtime(startTime)[1] / 1000000;
        winston.log('warn', "Time taken for couponRetriver url call is  " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
        return JSON.parse(res.text)
    })
}


module.exports = {
    couponSender: couponSender,
    couponRetriver: couponRetriver
}