var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../../constants')
var request = require('superagent')
const uuidV4 = require('uuid/v4')
var nconf = require('nconf')
var uniRest = require('unirest')


/**
 * Sends Coupon
 *
 * page=10&pageSize=2000
 */
sendCoupon1 = (address, bot, cards) => {

    console.log("Sending card", cards)

    var msg = new builder.Message().address(address).attachmentLayout(builder.AttachmentLayout.carousel).attachments(cards);
    bot.send(msg);

}

sendCoupon2 = (psid, elements) => {
    let token = nconf.get('FACEBOOK_PAGE_TOKEN', '')
    console.log("Sending card", elements)

    uniRest.post('https://graph.facebook.com/v2.6/me/messages?access_token=' + token)
        .headers({'Content-Type': 'application/json'})
        .send({
            "recipient": {
                "id": psid
            },
            "message": {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "generic",
                        "elements": elements
                    }
                }
            }
        })
        .end(function (response) {
            console.log('info', 'Coupon send for psid\t' + psid+ " " +JSON.stringify(response.body));
        });

}


module.exports = {
    sendCoupon1: sendCoupon1,
    sendCoupon2:sendCoupon2
}
