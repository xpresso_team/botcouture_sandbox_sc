var _ = require('lodash')
const HashMap = require('hashmap')
var builder = require('botbuilder')
var Constants = require('../constants')
var request = require('superagent')
var nconf = require('nconf')
const uuidV4  = require( 'uuid/v4' )
var winston = require('./loggly/LoggerUtil')


let webViewUserList=new HashMap()
let userList = []

let dummy_list=new HashMap()



Add_user = (ID, session) => {



console.log("ID ", ID)
//console.log("session ", session)
//console.log("has id ", dummy_list.has(ID))

    //let id = dummy_list.get(ID);

    if(!(dummy_list.has(ID))){

        dummy_list.set(ID, session);

    }


    console.log("has id ", dummy_list.has(ID))
    console.log("ID ", ID)
    console.log("session saved is ", dummy_list.get(ID))

}

return_user = (ID) => {




    //let id = dummy_list.get(ID);

    console.log("list is  ",dummy_list)
    console.log("has id ", dummy_list.has(ID))
    console.log("retrived session ",dummy_list.get(ID))

        return dummy_list.get(ID);

}


let FACEBOOK_PAGE_TOKEN = nconf.get( 'FACEBOOK_PAGE_TOKEN' )

/**
 * Returns a user for a given channelId and userId, undefined if not found
 */
getUserByAddress = address => _.find(userList, {channelId: address.channelId, userId: address.user.id})

/**
 * Clear and reset timer, essentially extending feedback timeout after every user message
 */
setBothTimer = (address, bot,session) => {


    let bot2

    if(session){
        bot2=session
    }else{
        bot2=bot
    }

    //console.log(" adresss is " ,JSON.stringify(address))
    //console.log(" bot is ",bot2)

    let id = _.findIndex(userList, {channelId: address.channelId, userId: address.user.id})

    if (id < 0) {

        let defaultFilters = {
            entity: null,
            price: [],
            subcategory: [],
            features: [],
            brand: [],
            details: [],
            colorsavailable: [],
            gender: [],
            size: [],
            xc_category: []
        }

        let user = {
            channelId: address.channelId,
            userId: address.user.id,
            name: address.user.name || 'User',
            active: false,
            askedFeedback: false,
            feedbackTimer: null,
            goodbyeTimer: null,
            usersFilterPositive: _.cloneDeep(defaultFilters),
            usersFilterNegative: _.cloneDeep(defaultFilters),
        }

        userList.push(user)
        id = userList.length - 1
    }

    clearTimeout(userList[id].feedbackTimer)
    clearTimeout(userList[id].goodbyeTimer)
    //userList[id].active = true

    userList[id].feedbackTimer = setTimeout(function () {
        //bot.send( { address: address, text: 'YOLO!!!', type: 'message', user: address.user})
        bot2.beginDialog(address, 'xpresso-bot:feedback', {data: null})
    }, nconf.get("Feddback_timeout") * 60000)

    userList[id].goodbyeTimer = setTimeout(function () {
        //bot.send( { address: address, text: 'This is the final Goodbye message!', type: 'message', user: address.user})
        bot2.beginDialog(address, 'xpresso-bot:goodbye', {data: null})
    }, (nconf.get("Feddback_timeout")+nconf.get("Goodbyee_timeout")) * 60000)

}

/**
 * Remove feedback timer
 */
unsetFeedbackTimer = address => {
    let id = _.findIndex(userList, {channelId: address.channelId, userId: address.user.id})
    if (id > -1) {
        clearTimeout(userList[id].feedbackTimer)
        console.log(" feedbackTimer reset for ",address.user.name)
    }
}

/**
 * Remove goodbye timer
 */
unsetGoodbyeTimer = address => {
    let id = _.findIndex(userList, {channelId: address.channelId, userId: address.user.id})
    if (id > -1) {
        clearTimeout(userList[id].goodbyeTimer)
        console.log(" goodbyeTimer reset for ",address.user.name)
    }
}

/**
 * Sets user feedback state
 */
setUserFeedbacState = (address, askedFeedback) => {
    let id = _.findIndex(userList, {channelId: address.channelId, userId: address.user.id})
    if (id > -1) {
        userList[id].askedFeedback = askedFeedback
    }
}

/**
 * Sets user active state
 */
setActiveState = (address, active) => {
    let id = _.findIndex(userList, {channelId: address.channelId, userId: address.user.id})
    if (id > -1) {
        userList[id].active = active
    }
}

/**
 * Returns user's active state
 */
isUserActive = address => {
    let id = _.findIndex(userList, {channelId: address.channelId, userId: address.user.id})
    if (id > -1) {
        return userList[id].active
    }
    return false;
}


/**
 * Ends user's session
 */
startNewSession = ( address, session ) => {
    session.userData.session_id = uuidV4()
    session.userData.message_number = -1
}

/**
 *
 */
addSessionId = ( address, session ) => {

  if ( ! session.userData.session_id ) {
    startNewSession( address, session )
  }

}

/**
 * Ends user's session
 */
endCurrentSession = ( address, session ) => {
    session.userData.session_id = null
    session.userData.message_number = -1

    try {
        getretailerPreference(address)
            .then( ret => {
              if(ret) {
                if(typeof ret.validity!=="undefined"&&ret.validity==="session") {
                    session.userData.client_name=[]
                    session.userData['NLP'].client_name = []
                    session.userData['COLLECTIONS'].client_name = [] 
                    session.userData['GUIDEME'].client_name = []
                    session.userData['IMAGEUPLOADVIEWMORE'].client_name = []                                                           
                    removeretailerPreference(address)
                        .then( rem => {
                          if(rem) {
                            console.log("\n retailer preference has been removed")
                            console.log("clientname has been removed.client_name now is ",session.userData.client_name)
                          }
                        })
                        .catch( err => {
                          console.log("\n error occured while removing retailer preference",err)
                        })
                } else if(typeof ret.validity=="undefined"||ret.validity!=="permanent") {
                    session.userData.client_name=[]
                } else {
                    console.log("entered else condition")
                }
              }
            })
            .catch( err => {
                session.userData.client_name=[]
                console.log("\n error in getting retailer preference is ",err)
            })
          }
    catch(err) {
        console.log("\n error in changing retailer preference is ", err)
      }


}

/**
 * Add a new user to User List
 */
/*
 addUser : address => {
 let user = {
 channelId        : address.channelId,
 userId           : address.user.id,
 name             : address.user.name,
 active           : true,
 feedbackTimer    : null,
 goodbyeTimer     : null
 }
 userList.push( user )
 },
 */

/**
 * Gets or creates a user
 */
createUserIfNotExists = address => {

    let USER_SERVICE_HOST = nconf.get('USER_SERVICE_HOST')
    let userId = address.user.id
    let first_name= null
    let last_name = null

    if (address.user.name) {
        first_name = address.user.name.split(' ')[0]
        if (address.user.name.split(' ')[1]) {
            last_name = address.user.name.split(' ')[1]
        }
    }

    let url = `http://${ USER_SERVICE_HOST }/dbservice/v1/users/${ userId }`

    let startTime = process.hrtime()

    console.log("\n create user if not exist call is  : ",url)

    return request.get(url).then(res => JSON.parse(res.text))
        .then(user => {
            // User exists in the db
            var elapsed = process.hrtime(startTime)[1] / 1000000;
            winston.log('warn', "Time taken to get User url createUserIfNotExists : User Exists : " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
            return user
        })
        .catch( err => {
            // User does not exists, create new user
            let newUser = {
                channel_id: address.channelId,
                page_id: address.bot.id,
                first_name: first_name,
                last_name: last_name,
                psid: userId
            }
            let startTime = process.hrtime()
            let url = `http://${ USER_SERVICE_HOST }/dbservice/v1/users`
            console.log("The new user creation call is ",url,newUser)
            return request.post(url).send(newUser)
                .then(newUserSuccess => {
                    // New User Created
                    var elapsed = process.hrtime(startTime)[1] / 1000000;
                    winston.log('warn', "Time taken to create NEW User url createUserIfNotExists : User NOT Exists : " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
                    return newUserSuccess
                })
                .catch(err => {
                    // Error creating new user
                    console.log('Error creating new user', err)
                })
        })

}

//Deletes an create an empty user profile

deleteUserPrefernce = address => {

    let USER_SERVICE_HOST = nconf.get('USER_SERVICE_HOST')
    let userId = address.user.id
    let first_name= null
    let last_name = null

    if (address.user.name) {
        first_name = address.user.name.split(' ')[0]
        if (address.user.name.split(' ')[1]) {
            last_name = address.user.name.split(' ')[1]
        }
    }

    let url = `http://${ USER_SERVICE_HOST }/dbservice/v1/users/${ userId }`

    let startTime = process.hrtime()
    return request.del(url).then(res => JSON.parse(res.text))
        .then(user => {
            // delete User which exists in the db
            var elapsed = process.hrtime(startTime)[1] / 1000000;
            winston.log('warn', "Time taken to delete user is  : User Exists : " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
           console.log("Existing user profile deleted")
            //  create new user after deleting
            let newUser = {
                channel_id: address.channelId,
                page_id: address.bot.id,
                first_name: first_name,
                last_name: last_name,
                psid: userId
            }
            let startTime = process.hrtime()
            let url = `http://${ USER_SERVICE_HOST }/dbservice/v1/users`
            console.log("The new user creation call is ",url,newUser)
            return request.post(url).send(newUser)
                .then(newUserSuccess => {
                    // New User Created
                    var elapsed = process.hrtime(startTime)[1] / 1000000;
                    winston.log('warn', "Time taken to create NEW User url while deleting : User NOT Exists : " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
                    return {"user" : newUserSuccess ,"Status" : "NEW"}
                })
                .catch(err => {
                    // Error creating new user
                    console.log('Error creating new user', err)
                })
        })
        .catch( err => {
            // User does not exists, create new user
            let newUser = {
                channel_id: address.channelId,
                page_id: address.bot.id,
                first_name: first_name,
                last_name: last_name,
                psid: userId
            }
            let startTime = process.hrtime()
            let url = `http://${ USER_SERVICE_HOST }/dbservice/v1/users`
            console.log("The new user creation call is ",url,newUser)
            return request.post(url).send(newUser)
                .then(newUserSuccess => {
                    // New User Created
                    var elapsed = process.hrtime(startTime)[1] / 1000000;
                    winston.log('warn', "Time taken to create NEW User url while deleting : User NOT Exists : " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
                    return {"user" : newUserSuccess ,"Status" : "NEW"}
                })
                .catch(err => {
                    // Error creating new user
                    console.log('Error creating new user', err)
                })
        })

}


checkUserExist = address => {

    let USER_SERVICE_HOST = nconf.get('USER_SERVICE_HOST')
    let userId = address.user.id
    let first_name= null
    let last_name = null

    if (address.user.name) {
        first_name = address.user.name.split(' ')[0]
        if (address.user.name.split(' ')[1]) {
            last_name = address.user.name.split(' ')[1]
        }
    }
    let url = `http://${ USER_SERVICE_HOST }/dbservice/v1/users/${ userId }`
    let startTime = process.hrtime()
    return request.get(url).then(res => JSON.parse(res.text))
        .then(user => {
            // User exists in the db
            var elapsed = process.hrtime(startTime)[1] / 1000000;
            console.log(" User exists so now deleting and creating a new profile")
            winston.log('warn', "Time taken to get User url checkUserExist  : User Exists : " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
            return deleteUserPrefernce( address )
        })
        .catch( err => {
            // User does not exists, create new user
            let newUser = {
                channel_id: address.channelId,
                page_id: address.bot.id,
                first_name: first_name,
                last_name: last_name,
                psid: userId
            }
            let startTime = process.hrtime()
            let url = `http://${ USER_SERVICE_HOST }/dbservice/v1/users`
            console.log("The new user creation call is ",url,newUser)
            return request.post(url).send(newUser)
                .then(newUserSuccess => {
                    // New User Created
                    var elapsed = process.hrtime(startTime)[1] / 1000000;
                    console.log(" User does not exist so creating a new profile")
                    winston.log('warn', "Time taken to create NEW User url checkUserExist  : User NOT Exists : " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
                    return {"user" : newUserSuccess ,"Status" : "NEW"}
                })
                .catch(err => {
                    // Error creating new user
                    console.log('Error creating new user', err)
                })
        })

}




/**
 * Returns the user Object
 * Creates a new user if not exists
 */
getUser = address => {

  //Get User Profile
  let USER_SERVICE_HOST = nconf.get( 'USER_SERVICE_HOST' )
  let userId            = address.user.id
  let url               = `http://${ USER_SERVICE_HOST }/dbservice/v1/users/${ userId }`
  url                   = encodeURI( url )
  // console.log('Get User Object: ', url)

  let user = createUserIfNotExists( address )
    .then( noop => {
      return request.get( url )
    })
    .then( res => JSON.parse( res.text ) )
    .then( res => {
      res.gender = res.gender === 'no_preference' ? "men,women" : res.gender
      return res
    })

  return user

}

/**
 * Updates a user
 */
updateUser = ( address, userObj ) => {
  //Get User Profile
  let USER_SERVICE_HOST = nconf.get( 'USER_SERVICE_HOST' )
  let userId            = address.user.id
  let url               = `http://${ USER_SERVICE_HOST }/dbservice/v1/users/${ userId }`
  url                   = encodeURI( url )
  userObj.psid = userId
  console.log( 'Update User Object: ', url, userObj )
  return request.put( url ).send( userObj )
}


/**
 * Returns user profile for all entities
 * Creates a user if not exists
 * returns Array:
 *    [ { id: 4,
       user: [Object],
       entity: 'shoe',
       aspect: 'string',
       size: 'ABHISHEK',
       brand: 'string',
       color: 'string' },
 { id: 5,
   user: [Object],
   entity: 'string',
   aspect: 'string',
   size: 'string',
   brand: 'string',
   color: 'string' } ]

 *  User Profile means the user preferences ( brands, sizes )
 *  use getUser() to get the userObject
 */
getUserProfile = address => {

    //Get User Profile
    let USER_SERVICE_HOST = nconf.get('USER_SERVICE_HOST')
    let userId = address.user.id
    let url = `http://${ USER_SERVICE_HOST }/dbservice/v1/profile/list?psid=${ userId }&page=0&size=100`
    url = encodeURI(url)
    //console.log('Get User Profile: ', url)

    let startTime = process.hrtime()
    return createUserIfNotExists(address).then(noop => {
        return request.get(url)
            .then( res => JSON.parse(res.text))
            .then( userProfile => {
                return userProfile.content
            })
            .then( userProfileArray => {

                userProfileArray = _.map( userProfileArray, userProfile => {
                    userProfile.size   = JSON.parse( userProfile.size   )
                    //userProfile.aspect = JSON.parse( userProfile.aspect )
                    userProfile.brand  = JSON.parse( userProfile.brand  )
                    userProfile.color  = JSON.parse( userProfile.color  )
                    return userProfile
                })
                var elapsed = process.hrtime(startTime)[1] / 1000000
                winston.log('warn', "Time taken to GET USER PROFILE " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
                return userProfileArray

            })
            .catch(err => {
                let emptyProfile = []
                console.log('error getting profile : getUserProfile :', err.text)
                var elapsed = process.hrtime(startTime)[1] / 1000000
                winston.log('warn', "Time taken to GET USER PROFILE " + url + " is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
                return emptyProfile
            })
    })

}

/**
 * Returns user profile for a given entity
 * NOTE: This does [NOT] check for existence of a user
 */
getUserProfileByEntity = ( address, entity,gender ) => {


           console.log("gender for prefernce is",gender)
           gender = gender ===  'no_preference'? "men,women" : gender
           console.log("gender for prefernce selection is",gender)
           //Get User Profile
           let userId = address.user.id

           let url = 'http://' + nconf.get('USER_SERVICE_HOST') + '/dbservice/v1/profile?psid=' + userId + '&entity=' + entity + '&gender=' + gender
           url = encodeURI(url)

           console.log('Get User Profile By Entity: ', url)

           return createUserIfNotExists(address).then(noop => {
               return request.get(url)
           })
               .then(userProfile => JSON.parse(userProfile.text))
               .then(userProfile => {

                   console.log("userProfile  to check now is ", JSON.stringify(userProfile))

                   userProfile.size = JSON.parse(userProfile.size)
                   //userProfile.aspect = JSON.parse( userProfile.aspect )
                   userProfile.brand = JSON.parse(userProfile.brand)
                   userProfile.color = JSON.parse(userProfile.color)

                   // Get the negative profile
                   userProfile.usersFilterNegative = null

                   console.log("userProfile now  is ", JSON.stringify(userProfile))

                   let id = _.findIndex(userList, {channelId: address.channelId, userId: address.user.id})
                   if (id > -1) {
                       userProfile.usersFilterNegative = userList[id].usersFilterNegative
                   }

                   return userProfile
               })
               .catch(err => {
                   let emptyProfile = {
                       id: null,
                       user: {
                           psid: userId,
                           first_name: null,
                           last_name: null,
                           profile_pic: null,
                           locale: null,
                           timezone: null,
                           gender: null,
                           is_payment_enabled: null,
                           channel_id: address.channelId,
                           page_id: address.bot.id
                       },
                       entity: entity,
                       aspect: null,
                       size: null,
                       brand: null,
                       color: null,
                       priceUpperLimit: null,
                       priceLowerLimit: null,
                       gender: gender
                   }

                   // Get the negative profile
                   emptyProfile.usersFilterNegative = null
                   let id = _.findIndex(userList, {channelId: address.channelId, userId: address.user.id})
                   if (id > -1) {
                       emptyProfile.usersFilterNegative = userList[id].usersFilterNegative
                   }

                   console.log('error getting profile : getUserProfileByEntity : ', err.error)
                   return emptyProfile
               })

}

/**
 * Save user's profile for a given attribute
 * This will OVERWRITE existing value for that attribute
 * NOTE: This does check for existence of a user
 * attributeValue is an ARRAY
 */
saveUserProfileByEntity = (address, entity, attribute, attributeValue, attributeGender) => {

    let USER_SERVICE_HOST = nconf.get('USER_SERVICE_HOST')
    let userId = address.user.id
    let url = `http://${ USER_SERVICE_HOST }/dbservice/v1/profile`
    url = encodeURI(url)
    let userProfile = {
        psid: userId,
        entity: entity,
        channel_id: address.channelId,
        page_id: address.bot.id
    }

    //attributeGender = attributeGender === 'men,women' ? "no_preference" : attributeGender

    userProfile.gender = attributeGender
    if(attribute=="price"){

        if(attributeValue && attributeValue.length>0) {
            userProfile.priceLowerLimit = attributeValue[0]
            userProfile.priceUpperLimit = attributeValue[1]
        }else{
            userProfile.priceLowerLimit = ''
            userProfile.priceUpperLimit = ''
        }



    }else{
        userProfile[attribute] = JSON.stringify(attributeValue)

    }


    console.log("User profile prefernce is ",userProfile)

    //console.log('Save User Profile By Entity: ', url, userProfile)

    return createUserIfNotExists(address)
        .then(noop => {

            return request.post(url).send(userProfile)
        })
        .then(res => {
            if (isJson(res.text)) {
                return JSON.parse(res.text)
            } else {
                return res
            }
        })


}

/**
 * Add a value to user's profile for a given attribute and entity
 * This will not overwrite but append, by seriealizing into an array
 * attributeValue is NOT an ARRAY
 */
appendToUserProfileByEntity = (address, entity, attribute, attributeValue,gender) => {

  let userId = address.user.id

    //gender = gender === 'men,women' ? "no_preference" : gender

  return createUserIfNotExists( address ).then( noop => {
    return getUserProfileByEntity( address, entity,gender )
  })
  .then( userProfile => {

      console.log("userProfile is ",userProfile )

    let newAttributeValue = null

    if ( userProfile[ attribute ] === null ) {
      // No previous attribute was set
      newAttributeValue = [ attributeValue ]
    } else {
      if ( _.includes(userProfile[ attribute ], '<NOPREFERENCE>') ) {
          newAttributeValue = [ attributeValue ]
      } else {
        if ( _.isArray( userProfile[ attribute ] ) ) {
          userProfile[ attribute ].push( attributeValue )
          newAttributeValue = userProfile[ attribute ]
        } else {
          newAttributeValue = [ attributeValue ]
        }
      }
    }
    console.log( 'tobe saved', newAttributeValue )
    return _.uniq( newAttributeValue )
  })
  .then( newAttributeValue => {
    return saveUserProfileByEntity( address, entity, attribute, newAttributeValue ,gender)
  })

}

/**
 * Returns an unfilled question from the profile allQuestions
 * which are not filled by the user
 */
getUserProfileUnfilledQuestion = ( userProfileArray, allQuestions ) => {

    // Find an unfilled entity in user profile
    let unfilledEntity = null
    let unfilledProfile = _.find( allQuestions, ( question ) => {
        let entity     = question[1]
        let attribute  = question[2]
        let userPref   = _.find( userProfileArray, { entity : entity } )
        unfilledEntity = entity

        if ( ! userPref ) return  true                    // attribute does not exists
        if ( userPref[ attribute ] == null ) return true  // attribute is null
        return false

    } )

    if( unfilledProfile ) {
        return { unfilledProfile : unfilledProfile, unfilledEntity : unfilledEntity }
    }
    return { unfilledProfile : null, unfilledEntity : null }
}

/**
 * Check if given string is a valid json
 */
isJson = str => {
    try {
        JSON.parse(str);
    } catch (e) {
        return false
    }
    return true
}

/**
 * Save user's filters in user profile
 * [DEPRICATED]
 */
saveUsersPositvePreference = (address, userPreference) => {

    let id = _.findIndex(userList, {channelId: address.channelId, userId: address.user.id})

    if (id > -1) {

        let e = userPreference

        if (e.entity) userList[id].usersFilterPositive.entity = e.entity
        if (e.price && e.price.length > 0) userList[id].usersFilterPositive.price = e.price
        if (e.subcategory && e.subcategory.length > 0) userList[id].usersFilterPositive.subcategory = e.subcategory
        if (e.features && e.features.length > 0) userList[id].usersFilterPositive.features = e.features
        if (e.brand && e.brand.length > 0) userList[id].usersFilterPositive.brand = e.brand
        if (e.details && e.details.length > 0) userList[id].usersFilterPositive.details = e.details
        if (e.colorsavailable && e.colorsavailable.length > 0) userList[id].usersFilterPositive.colorsavailable = e.colorsavailable
        if (e.gender && e.gender.length > 0) userList[id].usersFilterPositive.gender = e.gender
        if (e.size && e.size.length > 0) userList[id].usersFilterPositive.size = e.size
        if (e.xc_category && e.xc_category.length > 0) userList[id].usersFilterPositive.xc_category = e.xc_category

        return Promise.resolve(userList[id].usersFilterPositive)

    } else {

        throw new Error('User Not Found!')
        return

    }
}

/**
 * Save user's 'No dont save and dont ask again' filters
 * [NOT DEPRICATED]
 */
saveUsersNegativePreference = (address, userPreference) => {

    let id = _.findIndex(userList, {channelId: address.channelId, userId: address.user.id})

    if (id > -1) {

        if ( ! userList[id].usersFilterNegative ) {
            userList[id].usersFilterNegative = {
                entity: null,
                price: [],
                subcategory: [],
                features: [],
                brand: [],
                details: [],
                colorsavailable: [],
                gender: [],
                size: [],
                xc_category: []
            }
        }

        let uf = userList[id].usersFilterNegative
        let e = userPreference

        if ( e.entity                                          ) uf.entity          = e.entity
        if ( e.price           && e.price.length           > 0 ) uf.price           = uf.price.concat( e.price                     )
        if ( e.subcategory     && e.subcategory.length     > 0 ) uf.subcategory     = uf.subcategory.concat( e.subcategory         )
        if ( e.features        && e.features.length        > 0 ) uf.features        = uf.features.concat( e.features               )
        if ( e.brand           && e.brand.length           > 0 ) uf.brand           = uf.brand.concat( e.brand                     )
        if ( e.details         && e.details.length         > 0 ) uf.details         = uf.details.concat( e.details                 )
        if ( e.colorsavailable && e.colorsavailable.length > 0 ) uf.colorsavailable = uf.colorsavailable.concat( e.colorsavailable )
        if ( e.gender          && e.gender.length          > 0 ) uf.gender          = uf.gender.concat( e.gender                   )
        if ( e.size            && e.size.length            > 0 ) uf.size            = uf.size.concat( e.size                       )
        if ( e.xc_category     && e.xc_category.length     > 0 ) uf.xc_category     = uf.xc_category.concat( e.xc_category         )

// console.log( 'updated -ve', userList[id].usersFilterNegative, e )

        return Promise.resolve( userList[id].usersFilterNegative )

    } else {
        throw new Error('User Not Found!')
        return
    }
}

/**
 * Clears all -ve filters
 */
clearAllNegativeFilter = ( address ) => {
    let id = _.findIndex( userList, { channelId: address.channelId, userId: address.user.id } )
    if (id > -1) {
      userList[id].usersFilterNegative = {
        entity: null,
        price: [],
        subcategory: [],
        features: [],
        brand: [],
        details: [],
        colorsavailable: [],
        gender: [],
        size: [],
        xc_category: []
      }
    }
}

/**
 * Returns all elements in a users Wishlist
 */
getUsersWishlist = address => {

    let WISHLIST_HOST = nconf.get('WISHLIST_HOST')
    let userId = address.user.id
    let url = `http://${ WISHLIST_HOST }/dbservice/v1/wishlist/list?psid=${ userId }&page=0&size=100`

    console.log('Get All wishlist ', url)
    return request.get(url)
    .then( res => JSON.parse( res.text ) )
    .then( res => {
        //console.log(res)
        res = _.map(res.content, item => item.product)
        return res
    })

    // return createUserIfNotExists(address)
    //     .then(noop => {
    //         return request.get(url)
    //     })
    //     .then(res => {
    //         if (isJson(res.text)) {
    //             return JSON.parse(res.text)
    //         } else {
    //             return res
    //         }
    //     })
    //     .then(res => {
    //         //console.log(res)
    //         res = _.map(res.content, item => item.product)
    //         return res
    //     })

}

/**
 * Get a wishlist for given catalog
 * [done]
 */
getUsersWishlistByCatalog = ( address, catalog ) => {
    let WISHLIST_HOST = nconf.get('WISHLIST_HOST')
    let userId = address.user.id
    let url = `http://${ WISHLIST_HOST }/dbservice/v1/wishlist/list?psid=${ userId }&page=0&size=100`

    console.log('getUsersWishlistByCatalog ', url)

    return request.get( url ).then( res => JSON.parse(res.text) )
        .then( res => {
            let items = _.map( res.content, item => {
                return {
                    size: item.size,
                    color: item.color,
                    item : item.product,
                    catalog : item.catalog
                }
            })
            items = _.filter( items, item => item.catalog === catalog )
            return {
                catalog : catalog,
                items : items
            }
        })

}



/**
 * Save item to user's wishlist
 */
saveToWishlist = (address, xc_sku,color,size,catalog ) => {

    let WISHLIST_HOST = nconf.get('WISHLIST_HOST')
    let userId = address.user.id
    let url = "http://" + WISHLIST_HOST + nconf.get('user_db').wishlist
    let wishlistItem = {
        psid: userId,
        xc_sku: xc_sku,
        channel_id: address.channelId,
        page_id: address.bot.id,
        size:size,
        color:color,
        catalog : catalog
    }

    console.log('SAve to wishlist ', url, wishlistItem)

    return createUserIfNotExists(address)
        .then(noop => {
            return request.post(url).send(wishlistItem)
        })
        .then(res => {
            if (isJson(res.text)) {
                return JSON.parse(res.text)
            } else {
                return res
            }
        })
}

/**
 * Product Buy Soft signal view product
 */
viewedProduct = (address, xc_sku) => {

    let viewedProductHost = nconf.get('WISHLIST_HOST')
    let userId = address.user.id
    let url = "http://" + viewedProductHost + nconf.get("user_db").viewedproduct
    let viewedProduct = {
        psid: userId,
        xc_sku: xc_sku
    }

    console.log('viewed Product ', url, viewedProduct)

    return createUserIfNotExists(address)
        .then(noop => {
            return request.post(url).send(viewedProduct)
        })
        .then(res => {
            if (isJson(res.text)) {
                return JSON.parse(res.text)
            } else {
                return res
            }
        })

}



/**
 * Product Buy Soft signal view product
 */
sendFeedback = (session) => {

    let address = session.message.address
    let viewedProductHost = nconf.get('WISHLIST_HOST')
    let userId = session.message.address.user.id
    let url = "http://" + viewedProductHost + nconf.get("user_db").feedback
    let feedback = {
        psid: userId,
        feedback_json: JSON.stringify(session.userData.Feedback),
    }

    console.log('Feedback sent ', url, feedback)

    return createUserIfNotExists(address)
        .then(noop => {
            return request.post(url).send(feedback)
        })
        .then(res => {
            if (isJson(res.text)) {
                return JSON.parse(res.text)
            } else {
                return res
            }
        })

}

getretailerPreference = (address) => {
  console.log("entered userservice getretailerpreference")
  let USER_SERVICE_HOST = nconf.get('USER_SERVICE_HOST')
  let userId = address.user.id.toString()
  let url = `http://${USER_SERVICE_HOST}/dbservice/v1/retailerPreference/${userId}`

  console.log("\nGet User retailer Preference url: ",url)

  return createUserIfNotExists(address)
      .then(noop => {

          console.log("\n actual Get User retailer Preference url call is : ",url)
        return request.get(url)
      })
      .then(res => {

          //console.log("\nGet User retailer Preference value direct : ",res)
        if(res) {

            //console.log("\nGet User retailer Preference value: ",res)
              try {
              res = JSON.parse(res.text)
              return res
            } catch(err) {
                let ret = {}
                ret.client_name = []
                ret.valiity = ""
                return ret
              }
        }
        else {
            console.log("\nGet User retailer Preference in else value: ",res)
          let ret = {}
          ret.client_name = []
          ret.valiity = ""
          return ret
        }
      })
}

setretailerPreference = (address, retpref) => {
  let USER_SERVICE_HOST = nconf.get('USER_SERVICE_HOST')
  let userId = address.user.id
  let retailerPreference={
    "channel_id": address.channelId,
    "page_id": address.bot.id.toString(),
    "psid": address.user.id.toString(),
    "retailers": retpref.retailers,
    "validity": retpref.validity
  }

  let url = `http://${USER_SERVICE_HOST}/dbservice/v1/retailerPreference/${userId}`

  console.log("\nGet User retailer Preference url: ",url)

  return createUserIfNotExists(address)
      .then(noop => {
        console.log('\n entered get request')
        return request.get(url)
      })
      .then(res => {
        console.log('\n entered set request')
        url = `http://${USER_SERVICE_HOST}/dbservice/v1/retailerPreference`
        if(res) {
          return request.post(url).send(retailerPreference)
        }
        else {
          return request.post(url).send(retailerPreference)
        }
      })
}


removeretailerPreference =  (address) => {
  console.log("entered userservice removeretailerpreference")
  let USER_SERVICE_HOST = nconf.get('USER_SERVICE_HOST')
  let userId = address.user.id.toString()
  let url = `http://${USER_SERVICE_HOST}/dbservice/v1/retailerPreference/delete?psid=${userId}`

  console.log("\nGet User retailer Preference url: ",url)

  return createUserIfNotExists(address)
      .then(noop => {
        return request.delete(url)
      })
      .then(res => {
        if (isJson(res.text)) {
            return JSON.parse(res.text)
        } else {
            return res
        }
      })
}
/**
 * Remove one item from user's wishlist
 */
removeFromWishlist = (address, xc_sku, catalog) => {

    //http://exp.commerce.xpresso.ai:8090/dbservice/v1/wishlist/delete?psid=1306415456092513&xc_sku=2_3429671_0001
    console.log('address removeFromWishlist', address)
    let WISHLIST_HOST = nconf.get('WISHLIST_HOST')
    let userId = address.user.id
    let url = `http://${ WISHLIST_HOST }/dbservice/v1/wishlist/delete?psid=${ userId }&xc_sku=${ xc_sku }&catalog=${ catalog }`

    console.log('Remove item from wishilist ', url)

    return createUserIfNotExists(address)
        .then(noop => {
            return request.delete(url)
        })
        .then(res => {
            if (isJson(res.text)) {
                return JSON.parse(res.text)
            } else {
                return res
            }
        })

}

/**
 * Remove all items from wishlist
 */
clearWishlist = (address) => {

    let WISHLIST_HOST = nconf.get('WISHLIST_HOST')
    let userId = address.user.id
    let url = `http://${ WISHLIST_HOST }/dbservice/v1/wishlist/delete?psid=${ userId }`

    console.log('Remove ALL wishilist ', url)

    return createUserIfNotExists(address)
        .then(noop => {
            return request.delete(url)
        })
        .then(res => {
            if (isJson(res.text)) {
                return JSON.parse(res.text)
            } else {
                return res
            }
        })
}

/**
* Remove all items from a catalog in wishlist
*/
clearWishlistByCatalog= (address,Catalog) => {
  let WISHLIST_HOST = nconf.get('WISHLIST_HOST')
  let userId = address.user.id
  let url = `http://${ WISHLIST_HOST }/dbservice/v1/wishlist/delete?psid=${ userId }&Catalog=${ Catalog }`
  console.log('Remove ALL wishilist ', url)
  return createUserIfNotExists(address)
      .then(noop => {
          return request.delete(url)
      })
      .then(res => {
          if (isJson(res.text)) {
              return JSON.parse(res.text)
          } else {
              return res
          }
      })
}


/**
 * Get user's Facebook profile for given page specific id
 */
getFacebookProfile = psid => {

    let url = `https://graph.facebook.com/v2.6/${psid}?fields=first_name,last_name,profile_pic,locale,timezone,gender&access_token=${FACEBOOK_PAGE_TOKEN}`

    return request.get( url ).then( res => {
        let profile = JSON.parse( res.text )
        return profile
    })
}

/*******************************************
************** Shopping Cart ***************
********************************************/

/**
 * Get all carts with count of items
 * [done]
 */
getUsersAllCarts = ( address ) => {

  let USER_SERVICE_HOST = nconf.get('USER_SERVICE_HOST')
  let url = `http://${ USER_SERVICE_HOST }/dbservice/v1/cart/list?psid=${ address.user.id }&page=0&size=100`

  console.log('getUsersAllCarts ', url)

  return request.get( url ).then( res => JSON.parse(res.text) )
  .then(res => {
      res = _.map(res.content, item => item.product)
      return res
  })

}

/**
 * Clear all carts
 * [done]
 */
clearAllCarts = ( address ) => {

  let USER_SERVICE_HOST = nconf.get('USER_SERVICE_HOST')
  let url = `http://${ USER_SERVICE_HOST }/dbservice/v1/cart/delete?psid=${ address.user.id }`

  console.log('clearAllCarts ', url)

  return createUserIfNotExists(address)
  .then( noop => request.delete( url ) )
  .then( res => {
    // API does not returns anything
    return { text: 'All items removed from cart' }
  })

}


/**
 * Clear cart by catalog
 * [done]
 */
clearCartByCatalog = ( address, catalog ) => {

  let USER_SERVICE_HOST = nconf.get('USER_SERVICE_HOST')
  let url = `http://${ USER_SERVICE_HOST }/dbservice/v1/cart/delete?psid=${ address.user.id }&catalog=${ catalog }`

  console.log('clearCartByCatalog ', url)

  return request.delete( url ).then( res => {
    // API does not returns anything
    return { text: 'Cart cleared' }
  })

}

/**
 * Get a cart for given catalog
 * [done]
 */
getUsersCartByCatalog = ( address, catalog ) => {


  let USER_SERVICE_HOST = nconf.get('USER_SERVICE_HOST')
  let url = `http://${ USER_SERVICE_HOST }/dbservice/v1/cart/list?psid=${ address.user.id }&page=0&size=100`

  console.log('getUsersAllCarts ', url)

  return request.get( url ).then( res => JSON.parse(res.text) )
  .then( res => {
    let items = _.map( res.content, item => {
        return {
            size: item.size,
            color: item.color,
            quantity: item.quantity,
            item : item.product,
            catalog : item.catalog
        }
    })
    items = _.filter( items, item => item.catalog === catalog )
    return {
        catalog : catalog,
        items : items
    }
  })


/*
  let WISHLIST_HOST = nconf.get('WISHLIST_HOST')
  let userId = address.user.id
  let url = `http://${ WISHLIST_HOST }/dbservice/v1/wishlist/list?psid=${ userId }&page=0&size=100`

  console.log('getUsersCartByCatalog ', url)

  let cart = {
    catalog: catalog,
    items: [
      { size: 'M', color: 'white', quantity: 1,
        item : {
          "image": "https:\/\/slimages.macysassets.com\/is\/image\/MCY\/products\/7\/optimized\/2339837_fpx.tif",
          "product_url": "http:\/\/www.macys.com\/shop\/product\/alfani-mens-solid-long-sleeve-iridescent-shirt-only-at-macys?ID=1723160",
          "gender": "men",
          "xc_sku": "2_1723160_0004",
          "_score": "19.704264",
          "client_id": "2",
          "colorsavailable": "[\"bing cherry\"]",
          "size": "[\"s\",\"m\",\"xl\"]",
          "price": "26.0",
          "productname": "alfani men's solid long-sleeve iridescent shirt, only at macy's",
          "subcategory": "[\"shirt\"]",
          "sku": "1723160",
          "client_name": "macy's",
          "brand": "alfani",
          "confidence": "19.704264",
          "suggested": "false"
        }
      },
      { size: 'M', color: 'white', quantity: 1,
          item : {
          "image": "https:\/\/slimages.macysassets.com\/is\/image\/MCY\/products\/7\/optimized\/8272327_fpx.tif",
          "product_url": "http:\/\/www.macys.com\/shop\/product\/jaywalker-mens-bleach-splatter-plaid-cotton-shirt-only-at-macys?ID=4464406",
          "gender": "men",
          "xc_sku": "2_4464406_0001",
          "_score": "19.07576",
          "client_id": "2",
          "colorsavailable": "[\"bleach spl\"]",
          "size": "[\"s\",\"m\",\"l\",\"xl\",\"xxl\"]",
          "price": "48.0",
          "productname": "jaywalker men's bleach-splatter plaid cotton shirt, only at macy's ",
          "subcategory": "[\"shirt\"]",
          "sku": "4464406",
          "client_name": "macy's",
          "brand": "jaywalker",
          "confidence": "19.07576",
          "suggested": "false"
        }
      },
      { size: 'M', color: 'white', quantity: 1,
        item: {
          "image": "https:\/\/slimages.macysassets.com\/is\/image\/MCY\/products\/3\/optimized\/3919643_fpx.tif",
          "product_url": "http:\/\/www.macys.com\/shop\/product\/metal-mulisha-mens-explicit-buffalo-plaid-flannel-shirt?ID=3030908",
          "gender": "men",
          "xc_sku": "2_3030908_0001",
          "_score": "19.07576",
          "client_id": "2",
          "colorsavailable": "[\"black\"]",
          "size": "[\"s\",\"m\",\"l\"]",
          "price": "56.0",
          "productname": "metal mulisha men's explicit buffalo-plaid flannel shirt ",
          "subcategory": "[\"shirt\"]",
          "sku": "3030908",
          "client_name": "macy's",
          "brand": "metal mulisha",
          "confidence": "19.07576",
          "suggested": "false"
        }
      }
    ]

  }

  return Promise.resolve( cart )

  // return createUserIfNotExists(address)
  // .then( noop => request.get( url ) )
  // .then( res => {
  //     if (isJson(res.text)) {
  //         return JSON.parse(res.text)
  //     } else {
  //         return res
  //     }
  // })
  // .then(res => {
  //     res = _.map(res.content, item => item.product)
  //     return res
  // })
  */
}

/**
 * Get a cart for given catalog
 * [done]
 */
removeItemFromCart = ( address, catalog, xc_sku ) => {

  let USER_SERVICE_HOST = nconf.get('USER_SERVICE_HOST')
  let url = `http://${ USER_SERVICE_HOST }/dbservice/v1/cart/delete?psid=${ address.user.id }&catalog=${ catalog }&xc_sku=${ xc_sku }`

  console.log('removeItemFromCart ', url)

  return request.delete( url ).then( res => {
    // API does not returns anything
    return { text: 'Item removed from cart' }
  })

}

/**
 * Get a cart for given catalog
 * [done]
 */
updateCartItem = ( address, catalog, quantity, xc_sku ) => {

  let USER_SERVICE_HOST = nconf.get('USER_SERVICE_HOST')
  // let url = `http://${ USER_SERVICE_HOST }/dbservice/v1/cart/delete?psid=${ address.user.id }&catalog=${ catalog }&xc_sku=${ xc_sku }`
  let url = `http://${ USER_SERVICE_HOST }/dbservice/v1/cart`

  let cartItem = {
    catalog    : catalog,
    channel_id : address.channelId,
    page_id    : address.bot.id,
    psid       : address.user.id,
    quantity   : quantity,
    xc_sku     : xc_sku
  }

  console.log('updateCartItem ', url, cartItem)

  return request.put( url ).send( cartItem ).then( res => {
    // API does not returns anything
    return { text: 'Item updated in cart' }
  })

}

/**
 * Add an item to cart
 * [done]
 */
addItemToCart = (address, catalog, color, quantity, size, xc_sku) => {

  let USER_SERVICE_HOST = nconf.get('USER_SERVICE_HOST')
  let userId = address.user.id
  let url = `http://${ USER_SERVICE_HOST }/dbservice/v1/cart`

  let cartItem = {
    catalog    : catalog,
    channel_id : address.channelId,
    color      : color,
    page_id    : address.bot.id,
    psid       : userId,
    quantity   : quantity,
    size       : size,
    xc_sku     : xc_sku
  }
  console.log( 'add Item To Cart ', url, cartItem )
  // return Promise.resolve({ text : 'success' })

  return createUserIfNotExists( address ).then( noop => {
    return request.post( url ).send( cartItem )
  })
  .then( res => {
    // API does not returns anything
    return { text: 'Item added to cart' }
  })

}



/**
 * Product recommendation
 * [done]
 */
recommendProduct = (address) => {


    let url = nconf.get('Recommender')

    let postItem = {
        channel_id : address.channelId,
        page_id    : address.bot.id,
        psid       : address.user.id,
        type       : address.type
    }
    console.log( 'Recommender api call', url, postItem )
    // return Promise.resolve({ text : 'success' })

    return createUserIfNotExists( address ).then( noop => {
        return request.post( url ).send( postItem )
    })
        .then( res => {
            // API does not returns anything
            let val = JSON.parse(res.text)
            console.log(val)
            return (val)
        })

}

/**
 * get All Predefined Questions
 * [done]
 */
getAllPredefQuestions = () => {
    let url = nconf.get('Friend_Feddback_backend_url')+ "predefinedQuestions"

    console.log( 'Friend_Feddback_backend_url call', url)
    // return Promise.resolve({ text : 'success' })

    return request.get( url)
        .then( res => {
            let val = JSON.parse(res.text)
            console.log(val)
            return (val)
        })

}

/**
 * get Selected Predefined Questions
 * [done]
 */
getGivenPredefQuestions = (question_key_array) => {
    let url = nconf.get('Friend_Feddback_backend_url')+ "predefinedQuestions"

    console.log( 'getGivenPredefQuestions call', url)
    // return Promise.resolve({ text : 'success' })

    return request.get( url)
        .then( res => {
            let val = JSON.parse(res.text)

            console.log("question_key_array is",question_key_array )

            var result = _.map(question_key_array, item => _.filter(val, val => {

                console.log("val element is ",val)
                console.log("question_key_array element is ",item)
                val.id = val.id.toString()
                return val.id == item
            }))

            console.log("filtered result is ",result)

            var result = _.map(result, item => {
                return {
                    "key": item[0].id,
                    "message": item[0].message
                }
            })

            console.log(result)
            return (result)
        })
}


/**
 * add User selected and added Questions to db
 * [done]
 */
//addUserQuestions = (address,sender_name,pre_defined_question,custom_question) => {
addUserQuestions = (address,sender_name,question) => {
    let url = nconf.get('Friend_Feddback_backend_url')+ "userFeedbackQuestion"

    console.log( 'Friend_Feddback_backend_url call', url)
    // return Promise.resolve({ text : 'success' })

    let postItem = {
        channel_id : address.channelId,
        page_id    : address.bot.id,
        senderPsid       : address.user.id,
        sender_name       : sender_name,
        question : JSON.stringify(question)
        //pre_defined_question: pre_defined_question,
        //custom_question:custom_question
    }

    return request.post( url ).send( postItem )
        .then( res => {
            let val = JSON.parse(res.text)

            val.id = val.id.toString()
            let num_pad = 6-val.id.length

            for(let i = 0;i<num_pad;i++){
                val.id = "0"+ val.id
            }

            console.log(val)
            return (val)
        })

}

/**
 * get All user selected Questions
 * [done]
 */
getUserQuestions = () => {
    let url = nconf.get('Friend_Feddback_backend_url')+ "userFeedbackQuestion"

    console.log( 'Friend_Feddback_backend_url call', url)
    // return Promise.resolve({ text : 'success' })

    return request.get( url)
        .then( res => {
            let val = JSON.parse(res.text)
            //console.log(val)
            return (val)
        })

}

/**
 * add User answer in  db
 * [done]
 */
//sendUserAnswers = (address,feedback_id,sender_name,receiverPsid,receiver_name,pre_defined_question_answer,custom_question_answer) => {
sendUserAnswers = (address,feedback_id,sender_name,receiverPsid,receiver_name,answer) => {
    let url = nconf.get('Friend_Feddback_backend_url')+ "userFeedbackAnswer"

    console.log( 'Friend_Feddback_backend_url call', url)
    // return Promise.resolve({ text : 'success' })

    let postItem = {
        channel_id : address.channelId,
        page_id    : address.bot.id,
        senderPsid : address.user.id,
        feedback_id : feedback_id,
        sender_name  : sender_name,
        receiverPsid  : receiverPsid,
        receiver_name     : receiver_name,
        answer :  JSON.stringify(answer)
       // pre_defined_question_answer     : pre_defined_question_answer,
        //custom_question_answer     : custom_question_answer
    }

    console.log("user answer is ",postItem)

    return request.post( url ).send( postItem )
        .then( res => {
            let val = JSON.parse(res.text)

            val.id = val.id.toString()
            let num_pad = 6-val.id.length

            for(let i = 0;i<num_pad;i++){
                val.id = "0"+ val.id
            }

            console.log(val)
            return (val)
        })

}

/**
 * get All user selected Questions
 * [done]
 */
retrieveAllUsersAnswers = () => {
    let url = nconf.get('Friend_Feddback_backend_url')+ "userFeedbackAnswer"

    console.log( 'Friend_Feddback_backend_url call', url)
    // return Promise.resolve({ text : 'success' })

    return request.get( url)
        .then( res => {
            let val = JSON.parse(res.text)
            //console.log(val)
            return (val)
        })

}









module.exports = {
    getUserByAddress: getUserByAddress,
    setBothTimer: setBothTimer,
    unsetFeedbackTimer: unsetFeedbackTimer,
    unsetGoodbyeTimer:unsetGoodbyeTimer,
    setUserFeedbacState: setUserFeedbacState,
    setActiveState: setActiveState,
    isUserActive: isUserActive,
    startNewSession : startNewSession,
    addSessionId : addSessionId,
    endCurrentSession : endCurrentSession,
    Add_user:Add_user,
    return_user :return_user,
    dummy_list :dummy_list,
    // Userdb
    getUser: getUser,
    checkUserExist:checkUserExist,
    updateUser : updateUser,
    getUserProfile: getUserProfile,
    getUserProfileByEntity: getUserProfileByEntity,
    saveUserProfileByEntity: saveUserProfileByEntity,
    appendToUserProfileByEntity: appendToUserProfileByEntity,
    getUserProfileUnfilledQuestion: getUserProfileUnfilledQuestion,
    saveUsersPositvePreference: saveUsersPositvePreference,
    saveUsersNegativePreference: saveUsersNegativePreference,
    clearAllNegativeFilter: clearAllNegativeFilter,
    // Wishlist
    getUsersWishlist: getUsersWishlist,
    saveToWishlist: saveToWishlist,
    viewedProduct: viewedProduct,
    removeFromWishlist: removeFromWishlist,
    sendFeedback :sendFeedback,
    clearWishlist: clearWishlist,
    getUsersWishlistByCatalog:getUsersWishlistByCatalog,
    clearWishlistByCatalog:clearWishlistByCatalog,
    // Misc
    getFacebookProfile : getFacebookProfile,
    // Shopping Cart
    getUsersAllCarts : getUsersAllCarts,
    clearAllCarts : clearAllCarts,
    clearCartByCatalog : clearCartByCatalog,
    getUsersCartByCatalog : getUsersCartByCatalog,
    removeItemFromCart : removeItemFromCart,
    updateCartItem : updateCartItem,
    addItemToCart : addItemToCart,

    // retailerPreference
    getretailerPreference : getretailerPreference,
    setretailerPreference : setretailerPreference,
    removeretailerPreference :removeretailerPreference,

    // Product recommendation
    recommendProduct :recommendProduct,

    // Feedback from friend

    getAllPredefQuestions : getAllPredefQuestions,
    addUserQuestions:addUserQuestions,
    getUserQuestions:getUserQuestions,
    getGivenPredefQuestions:getGivenPredefQuestions,
    sendUserAnswers:sendUserAnswers,
    retrieveAllUsersAnswers:retrieveAllUsersAnswers


}
