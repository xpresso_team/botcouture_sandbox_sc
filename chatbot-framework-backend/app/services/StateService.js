var _         = require( 'lodash'       )
var builder   = require( 'botbuilder'   )
var Constants = require( '../constants' )
var request   = require( 'superagent'   )
const uuidV4  = require( 'uuid/v4'      )


/**
 * Returns a copy of state
 * [DEPRICATED]
 */
// cloneUserData = session => {
//   let userData = _.cloneDeep( session.userData )
//   delete userData.quickReplies
//   delete userData.Feedback
//   delete userData.GUIDEME
//   delete userData.guestMode
//   return userData
// }

/**
 * Reset whole state
 * [DEPRICATED]
 */
// resetState = session => {
//   /*
//   // Don't do this, there are other things attached to userData
//   session.userData = { 
//     entity          : null,
//     price           : [],
//     subcategory     : [],
//     features        : [],
//     brand           : [],
//     details         : [],
//     colorsavailable : [],
//     gender          : [],
//     size            : [],
//     xc_category     : []
//   }
//   */
//   session.userData.entity          = null
//   session.userData.price           = []
//   session.userData.subcategory     = []
//   session.userData.features        = []
//   session.userData.brand           = []
//   session.userData.details         = []
//   session.userData.colorsavailable = []
//   session.userData.gender          = []
//   session.userData.size            = []
//   session.userData.xc_category     = []
//   session.userData = {}
//   console.log( 'After Reset: ', session.userData)
// }

/**
 * Reset All Filters, except entity, xc_category and gender
 * Filter here means All attributes except entity, xc_category, gender
 * [ DEPRICATED ]
 */
// resetAllFilters = session => {
//   //session.userData.entity          = null
//   session.userData.price           = []
//   session.userData.subcategory     = []
//   session.userData.features        = []
//   session.userData.brand           = []
//   session.userData.details         = []
//   session.userData.colorsavailable = []
//   //session.userData.gender          = []
//   session.userData.size            = []
//   //session.userData.xc_category     = []
// }

/**
 * Deletes a given value from the given filter type
 * [DEPRICATED]
 */
// deleteElementFromFilter = ( session, filterType, filterValue ) => {
//   return _.pull( session.userData[ filterType ], filterValue )
// }

/**
 * Reset a given filter type
 * [DEPRICATED]
 */
// resetAFilter = ( session, filterType ) => {
//   session.userData[ filterType ] = []
// }

/**
 * Returns if a filter of given type is active
 * [ DEPRICATED ]
 */
// isAFilterActive = ( session, filterType ) => {
//   return session.userData[ filterType ] && session.userData[ filterType ].length !== 0
// }

/**
 * Returns if at least one filter is active
 * [ DEPRICATED ]
 */
// isAnyFilterActive = session => {
//   console.log( 'State 1' )
//   let brandFilterActive = isAFilterActive( session, 'brand' )
//   let priceFilterActive = isAFilterActive( session, 'price' )
//   let colorFilterActive = isAFilterActive( session, 'colorsavailable' )
//   let sizeFilterActive  = isAFilterActive( session, 'size' )
//   let showEditFiltersButton = brandFilterActive || priceFilterActive || colorFilterActive || sizeFilterActive
  
//   return showEditFiltersButton

// } 

/**
 * Is any filter active except the one given
 * [DEPRICATED]
 */
// isAnyFilterActiveExcept = ( session, except ) => {

// console.log('except', except)
//   let remaing = _.difference( [ 'brand', 'price', 'colorsavailable', 'size' ], [ except ] )
//   let res = false
//   _.each( remaing, item => {
//     res = res || isAFilterActive( session, item )  
//   })
//   return res
// }

/**
 * Remove 'anything' from the intent results
 */
// sanitizeIntentResults = userData => {

//   userData.entity          = userData.entity === 'anything' ? null : userData.entity
//   userData.price           = _.difference( userData.price           , ['anything'] )
//   userData.subcategory     = _.difference( userData.subcategory     , ['anything'] )
//   userData.features        = _.difference( userData.features        , ['anything'] )
//   userData.brand           = _.difference( userData.brand           , ['anything'] )
//   userData.details         = _.difference( userData.details         , ['anything'] )
//   userData.colorsavailable = _.difference( userData.colorsavailable , ['anything'] )
//   userData.gender          = _.difference( userData.gender          , ['anything'] )
//   userData.size            = _.difference( userData.size            , ['anything'] )
//   userData.xc_category     = _.difference( userData.xc_category     , ['anything'] )

//   return userData
// }

// Overwrite State with new values where available
// overWriteState = ( session, e ) => {
//   if ( e.entity                                          ) session.userData.entity          = e.entity
//   if ( e.price           && e.price.length           > 0 ) session.userData.price           = e.price
//   if ( e.subcategory     && e.subcategory.length     > 0 ) session.userData.subcategory     = e.subcategory
//   if ( e.features        && e.features.length        > 0 ) session.userData.features        = e.features
//   if ( e.brand           && e.brand.length           > 0 ) session.userData.brand           = e.brand
//   if ( e.details         && e.details.length         > 0 ) session.userData.details         = e.details
//   if ( e.colorsavailable && e.colorsavailable.length > 0 ) session.userData.colorsavailable = e.colorsavailable
//   if ( e.gender          && e.gender.length          > 0 ) session.userData.gender          = e.gender  
//   if ( e.size            && e.size.length            > 0 ) session.userData.size            = e.size  
//   if ( e.xc_category     && e.xc_category.length     > 0 ) session.userData.xc_category     = e.xc_category  
// }

// A set of Quick Reply buttons is called a Prompt (TWO)
/**
 * Reset user specific prompts
 * Prompts: { id: uuid, button: [] }
 */
resetPrompt2 = session => {
  //session.userData.promptsCollection = []
  //console.log( 'RESETTING PROMPT2' )
  session.userData.quickReplies = { 
    promptsCollection : [],       // prompts are saved here
    head              : -1,       // head is the last occupied position, this is incrimented before saving a prompt
    MAX_ITEMS         : 20        // Maximum items to be saved, cycles back to 0 after that
  }
}

/**
 * Returns prompts array (used in prompts recognizers)
 */
getPrompts2 = session => {
  //return session.userData.promptsCollection
  if ( ! session.userData.quickReplies ) {
    resetPrompt2( session )
  }
  return session.userData.quickReplies.promptsCollection
},

/**
 * Returns the id of the next prompt to be saved
 * Should be used just before saving a prompt
 */
getNextFreePrompt2Id = session => {
  // if ( ! session.userData.promptsCollection ) return 0
  // return session.userData.promptsCollection.length
  if ( ! session.userData.quickReplies ) {
    resetPrompt2( session )
    // session.userData.quickReplies = { promptsCollection : [], head : -1, MAX_ITEMS : 10 }
  }
  //console.log( 'Next free prompt ', ( session.userData.quickReplies.head + 1 ) % session.userData.quickReplies.MAX_ITEMS )
  return ( session.userData.quickReplies.head + 1 ) % session.userData.quickReplies.MAX_ITEMS 
}

/**
 * Add a set of quick replys to the array
 */
addPrompt2 = ( session, prompt2 ) => {
  // if( ! session.userData.promptsCollection ) {
  //   session.userData.promptsCollection = []
  // }
  // prompt2.id = session.userData.promptsCollection.length
  // session.userData.promptsCollection.push( prompt2 )
  if ( ! session.userData.quickReplies ) {
    resetPrompt2( session )
    //session.userData.quickReplies = { promptsCollection : [], head : -1, MAX_ITEMS : 10 }
  }
  if ( ! ( prompt2 && prompt2.buttons && prompt2.buttons.length > 0 )  ) {
    return
  }
  prompt2.id = uuidV4()
  session.userData.quickReplies.head = getNextFreePrompt2Id( session )
  session.userData.quickReplies.promptsCollection[ session.userData.quickReplies.head ] = prompt2
  //console.log( 'Adding prompt UUID: ', prompt2.id, ' at ', session.userData.quickReplies.head )

}

/**
 * Replace a prompt object at the given id
 * Will add if the promptid not found
 * [NOT USED ANYMORE]
 */
// updatePrompt2 = ( session, prompt2Id, prompt2 ) => {
//   // if ( ! session.userData.quickReplies ) return
//   // let index = _.findIndex( session.userData.promptsCollection, { id : prompt2Id } )
//   // prompt2.id = index
//   // session.userData.promptsCollection[ index ] = prompt2
//   if ( ! session.userData.quickReplies ) {
//     resetPrompt2( session )
//   }
//   let index = _.findIndex( session.userData.quickReplies.promptsCollection, { id : prompt2Id } )
//   if ( index < 0 ) {
//     //  Index not found, maybe it got recycled
//     addPrompt2( session, prompt2 )
//   } else {
//     prompt2.id = session.userData.quickReplies.promptsCollection[ index ].id
//     session.userData.quickReplies.promptsCollection[ index ] = prompt2
//     //console.log('UPDATING PROMPT', prompt2.id )
//   }

// }

/**
 * Returns the matching button
 * Used by prompt recognizer
 */
getMatchingPrompt = ( session, userInput ) => {

  if( ! userInput ){
    return null
    console.log("That get prompt 2 has empty", userInput)
  }
  let prompts2  = getPrompts2( session )
  let head      = session.userData.quickReplies.head
  let MAX_ITEMS = session.userData.quickReplies.MAX_ITEMS
  
  // Check if MAX_ITEMS values is changed
  if ( head > MAX_ITEMS ) resetPrompt2()

  //console.log( 'head ', head, ' MAX_ITEMS ', MAX_ITEMS)

  for ( let l = head; l >= 0; l-- ) {
    let prompt2 = prompts2[l]
    //console.log( l, ' :: ', prompt2.id )
    let match = prompt2.buttons.find( button => matchWithSynonyms( button.buttonText, userInput ) )
    if ( match ) return { prompt2Id : prompt2.id, button : match }
  }

  for ( let l = MAX_ITEMS - 1; l > head; l-- ) {
    let prompt2 = prompts2[l]
    //console.log( ' l ', l )
    if ( ! prompt2 ) continue
    //console.log( l, ' :: ', prompt2.id )
    let match = prompt2.buttons.find( button => matchWithSynonyms( button.buttonText, userInput ) )
    if ( match ) return { prompt2Id : prompt2.id, button : match }
  }

  return null
}

/**
 * Matches a given string with its synonyms
 */
matchWithSynonyms = ( buttonText, userInput ) => {
  
  // Check if its a direct match
  if ( buttonText === userInput ) return true
  // Do(n't) a case insensitive search
  // if ( _.toLower( buttonText ) === _.toLower( userInput ) ) return true
  

  let synonymsArray = Constants.SYNONYMS[ buttonText ]
  let synonymObj = _.cloneDeep( Constants.SYNONYMS.SYN_DEFAULTS )
    
  if ( synonymsArray && synonymsArray.length > 0 ) {
    
    return _.some( synonymsArray, synonym => {

      if ( _.isString( synonym ) ) {
        synonym = { text: synonym }
      }

      // Use default settings for missing parameters
      _.merge( synonymObj, synonym )
      // console.log( 'synonymObj -> ', synonymObj )

      if ( synonymObj.isCaseSensitive ) {
        if ( synonymObj.text === userInput ) return true        
      } else {
        if ( _.toLower( synonymObj.text ) === _.toLower( userInput ) ) return true
      }

      if ( synonymObj.isPartialMatch ) {
        let flags = synonymObj.isCaseSensitive ? '' : 'i'
        let regEx = new RegExp( synonymObj.text, flags )
        return regEx.test( userInput )
      }

    })

  }

  return false
  
}

module.exports = {
  // cloneUserData           : cloneUserData,
  // resetState              : resetState,
  // resetAllFilters         : resetAllFilters,
  // deleteElementFromFilter : deleteElementFromFilter,
  // resetAFilter            : resetAFilter,
  // isAFilterActive         : isAFilterActive,
  // isAnyFilterActive       : isAnyFilterActive,
  // isAnyFilterActiveExcept : isAnyFilterActiveExcept, 
  // sanitizeIntentResults   : sanitizeIntentResults,
  // overWriteState          : overWriteState,
  resetPrompt2          : resetPrompt2,
  getPrompts2           : getPrompts2,
  getNextFreePrompt2Id  : getNextFreePrompt2Id,
  addPrompt2            : addPrompt2,
  // updatePrompt2      : updatePrompt2,
  getMatchingPrompt     : getMatchingPrompt,
}