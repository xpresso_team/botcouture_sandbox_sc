var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../constants')
var request = require('superagent')
const uuidV4 = require('uuid/v4')
var nconf = require('nconf')
var UtilService = require('./UtilService')

/**
 * Returns a copy of state
 */
cloneState = (key, session, defaults) => {
    let state = _.cloneDeep(session.userData[key])
    if (defaults) {
        _.merge(state, defaults)
    }
    return state
}

/**
 * Reset whole state
 */
resetState = (key, session) => {
    /*
     // Don't do this, there are other things attached to userData
     session.userData = {
     entity          : null,
     price           : [],
     subcategory     : [],
     features        : [],
     brand           : [],
     details         : [],
     colorsavailable : [],
     gender          : [],
     size            : [],
     xc_category     : []
     }
     */
    if (!session.userData[key]) session.userData[key] = {}

    session.userData[key].entity = null
    session.userData[key].price = []
    session.userData[key].subcategory = []
    session.userData[key].features = []
    session.userData[key].brand = []
    session.userData[key].details = []
    session.userData[key].colorsavailable = []
    session.userData[key].gender = []
    session.userData[key].size = []
    session.userData[key].xc_category = []
    session.userData[key].client_name = []
}

/**
 * Reset All Filters, except entity, xc_category and gender
 * Filter here means All attributes except entity, xc_category, gender
 */
resetAllFilters = (key, session) => {
    //session.userData[key].entity        = null
    session.userData[key].price = []
    //session.userData[key].subcategory = []
    //session.userData[key].features = []
    session.userData[key].brand = []
    //session.userData[key].details = []
    session.userData[key].colorsavailable = []
    //session.userData[key].gender        = []
    session.userData[key].size = []
    //session.userData[key].xc_category   = []
}

/**
 * Deletes a given value from the given filter type
 */
deleteElementFromFilter = (key, session, filterType, filterValue) => {
    return _.pull(session.userData[key][filterType], filterValue)
}

/**
 * Reset a given filter type
 */
resetAFilter = (key, session, filterType) => {
    session.userData[key][filterType] = []
}

/**
 * Returns if a filter of given type is active
 * [TODO]: 2 added to the name
 */
isAFilterActive2 = (key, session, filterType) => {
    //console.log( 'State 2' )
    return session.userData[key][filterType] && session.userData[key][filterType].length !== 0
}

/**
 * Returns if at least one filter is active
 */
isAnyFilterActive = (key, session) => {

    let brandFilterActive = isAFilterActive2(key, session, 'brand')
    let priceFilterActive = isAFilterActive2(key, session, 'price')
    let colorFilterActive = isAFilterActive2(key, session, 'colorsavailable')
    let sizeFilterActive = isAFilterActive2(key, session, 'size')
    let showEditFiltersButton = brandFilterActive || priceFilterActive || colorFilterActive || sizeFilterActive

    return showEditFiltersButton

}

/**
 * Is any filter active except the one given
 */
isAnyFilterActiveExcept = (key, session, except) => {

    console.log('except', except)
    let remaing = _.difference(['brand', 'price', 'colorsavailable', 'size'], [except])
    let res = false
    _.each(remaing, item => {
        res = res || isAFilterActive2(key, session, item)
    })
    return res
}

/**
 * Remove 'anything' from the intent results
 */
sanitizeIntentResults = userData => {

    userData.entity = userData.entity === 'anything' ? null : userData.entity
    userData.price = _.difference(userData.price, ['anything'])
    userData.subcategory = _.difference(userData.subcategory, ['anything'])
    userData.client_name = _.difference(userData.client_name, ['anything'])
    userData.features = _.difference(userData.features, ['anything'])
    userData.brand = _.difference(userData.brand, ['anything'])
    userData.details = _.difference(userData.details, ['anything'])
    userData.colorsavailable = _.difference(userData.colorsavailable, ['anything'])
    userData.gender = _.difference(userData.gender, ['anything'])
    userData.size = _.difference(userData.size, ['anything'])
    userData.xc_category = _.difference(userData.xc_category, ['anything'])

    return userData
}

// Overwrite State with new values where available
overWriteState = (key, session, e) => {
    if (e.entity) session.userData[key].entity = e.entity
    if (e.price && e.price.length > 0) session.userData[key].price = e.price
    if (e.subcategory && e.subcategory.length > 0) session.userData[key].subcategory = e.subcategory
    if (e.client_name && e.client_name.length>0) session.userData[key].client_name = e.client_name    
    if (e.features && e.features.length > 0) session.userData[key].features = e.features
    if (e.brand && e.brand.length > 0) session.userData[key].brand = e.brand
    if (e.details && e.details.length > 0) session.userData[key].details = e.details
    if (e.colorsavailable && e.colorsavailable.length > 0) session.userData[key].colorsavailable = e.colorsavailable
    if (e.gender && e.gender.length > 0) session.userData[key].gender = e.gender
    if (e.size && e.size.length > 0) session.userData[key].size = e.size
    if (e.xc_category && e.xc_category.length > 0) session.userData[key].xc_category = e.xc_category
}


/**
 * Returns QuickReply buttons for Add/Edit filters
 */
getFiltersButtons = (session, NODE, results) => {

    // Check if a filter is applied
    let brandFilterActive = isAFilterActive2(NODE, session, 'brand')
    let priceFilterActive = isAFilterActive2(NODE, session, 'price')
    let colorFilterActive = isAFilterActive2(NODE, session, 'colorsavailable')
    let sizeFilterActive = isAFilterActive2(NODE, session, 'size')
    let showEditFiltersButton = isAnyFilterActive(NODE, session)

    // Show filters for attributed which are not set
    let showBrandFilter = UtilService.hasDistinctAttributes(results, 'brand')           // && ! brandFilterActive
    let showPriceFilter = UtilService.hasDistinctAttributes(results, 'price')           // && ! priceFilterActive
    let showColorFilter = UtilService.hasDistinctAttributes(results, 'colorsavailable') // && ! colorFilterActive
    let showSizeFilter = UtilService.hasDistinctAttributes(results, 'size')            // && ! sizeFilterActive

    let filter_off = []

    for (keys in nconf.get('price')) {
        // console.log("keys are  ",keys)
        if (!nconf.get('price')[keys]) {
            filter_off.push(Constants.NODE[keys])
            // console.log("Price Filter progress ",filter_off)
        }

    }

    // console.log("Price Filter off for ",filter_off)

    if (_.includes(filter_off, NODE)) {
        priceFilterActive = false
        showPriceFilter = false
    }

    // Show 'Add Filter' button if there is atleast one filter available
    let showAddFiltersButton = showBrandFilter || showPriceFilter || showColorFilter || showSizeFilter


    let buttons = []
    if (showAddFiltersButton) {
        buttons.push({
            buttonText: 'Add Filters',
            triggerIntent: 'RESULTS.ADD.FILTERS',
            entityType: 'NLP.FILTER.BUTTON',
            data: {
                NODE: NODE,
                state: cloneState(NODE, session, {pageNo: 0}),
                guestMode: session.userData.guestMode,
                showBrandFilter: showBrandFilter,
                showPriceFilter: showPriceFilter,
                showColorFilter: showColorFilter,
                showSizeFilter: showSizeFilter,
            }
        })
    }

    if (showEditFiltersButton) {
        buttons.push({
            buttonText: 'Edit Filters',
            triggerIntent: 'RESULTS.EDIT.FILTERS',
            entityType: 'NLP.FILTER.BUTTON',
            data: {
                NODE: NODE,
                state: cloneState(NODE, session, {pageNo: 0}),
                brandFilterActive: brandFilterActive,
                priceFilterActive: priceFilterActive,
                colorFilterActive: colorFilterActive,
                sizeFilterActive: sizeFilterActive,
            }
        })
    }

    return buttons

}


checkResumeShopping = (session) => {

    let temp_node = session.userData.currentNode

    if (temp_node) {
        switch (temp_node) {
            case Constants.NODE.GUIDEME             :
                if ((!_.isEmpty(session.userData[temp_node]))&&(session.userData[temp_node].xc_category )&& (session.userData[temp_node].xc_category.length >0)) {
                    return true
                } else {
                    return false
                }
                break;
            case Constants.NODE.NLP                 :
                if ((!_.isEmpty(session.userData[temp_node]))&&(session.userData[temp_node].entity)) {
                    return true
                } else {
                    return false
                }
                break;
            case Constants.NODE.IMAGEUPLOAD         :
                if ((!_.isEmpty(session.userData[temp_node]))&&(session.userData[temp_node].imageUrl)) {
                    return true
                } else {
                    return false
                }
                break;
            case Constants.NODE.IMAGEUPLOADVIEWMORE :
                if ((!_.isEmpty(session.userData[temp_node]))&&session.userData[temp_node].imageUrl ) {
                    return true
                } else {
                    return false
                }
                break;
            case Constants.NODE.IMAGEUPLOADCATEGORY :
                if ((!_.isEmpty(session.userData[temp_node]))&&(session.userData[temp_node].entity) && (session.userData[temp_node].upload_id) ) {
                    return true
                } else {
                    return false
                }
                break;
            case Constants.NODE.COLLECTIONS         :
                if ((!_.isEmpty(session.userData[temp_node]))&&(session.userData[temp_node].collectionType )&& (session.userData[temp_node].collectionName) && (session.userData[temp_node].entity)&& (session.userData[temp_node].gender) ) {
                    return true
                } else {
                    return false
                }
                break;
            default :
                return false
                break;
        }

    } else {
        return false
    }

}

module.exports = {
    cloneState: cloneState,
    resetState: resetState,
    resetAllFilters: resetAllFilters,
    deleteElementFromFilter: deleteElementFromFilter,
    resetAFilter: resetAFilter,
    isAFilterActive2: isAFilterActive2,
    isAnyFilterActive: isAnyFilterActive,
    isAnyFilterActiveExcept: isAnyFilterActiveExcept,
    sanitizeIntentResults: sanitizeIntentResults,
    overWriteState: overWriteState,
    checkResumeShopping: checkResumeShopping,
    /**
     * Returns QuickReply buttons for Add/Edit filters
     */
    getFiltersButtons: getFiltersButtons
}