var _ = require('lodash')
const nconf = require('nconf')
var builder = require('botbuilder')
var Constants = require('../../constants')
var StateService = require('../../services/StateService')
var UserService = require('../../services/UserService')
var UtilService = require('../../services/UtilService')
var TextService = require('../../services/TextService')
var BotService = require('../../services/BotService')
var HumanAugmentationService = require('../../services/human-augmentation-service')
var HumanHelpService = require('./human-help-service')

module.exports = {
    Dialog: [
        function (session, args, next) {

            UserService.setActiveState(session.message.address, true)

            let userName = session.message.user && session.message.user.name ? session.message.user.name : 'User'
            let channelName = session.message.address.channelId
            let firstName = userName.split(' ')[0]
            let LastName = userName.split(' ')[1]
            let HA_CLIENT_ORGANIZATION = nconf.get("HUMAN_AUGMENTATION_SERVER_CLIENT_ORGANIZATION")
            let psid = session.message.address.user.id
            let urls = args.entities[0].data
            let user_id = session.userData.user_engaged_queue.id

            let session2 = HumanHelpService.return_user(user_id)
            let getProductdetails = HumanAugmentationService.getProductdetails(urls)

            Promise.all([getProductdetails])
                .then(([results]) => {
                    if (results.length > 0) {
                        session2.sendTyping()
                        session.sendTyping()
                        let pageSize = 10 + 2
                        let pageNo = 0
                        let pagedResults = UtilService.pageList(results, pageNo, pageSize, null, null)
                        UtilService.createCards(session, pagedResults).then(cards => {

                            console.log(JSON.stringify(cards))

                            if (session.message.source === "facebook") {
                                console.log("sending  FB specific ")
                                BotService.sendCard(session2, cards, true, "generic")

                            } 
                            else {
                                BotService.sendCard(session2, cards)
                            }
                            BotService.sendText(session, {
                                text: 'product send confirmation'
                            })

                            let quick_replies =[]

                            let promptObj = { id : null, buttons : [] }
                            promptObj.buttons.push({
                                buttonText    : "STOP",
                                triggerIntent : 'HA.STOP',
                                data          : {mode : "user" }
                            })
                            StateService.addPrompt2(session2, promptObj)

                            let msg = TextService.text( 'Stop_chat_msg' )

                            if (session.message.source === "facebook") {

                                let element = {
                                    "content_type":"text",
                                    "title": "",
                                    "payload":"STOP",
                                    "image_url":"https://twemoji.maxcdn.com/2/72x72/1f6d1.png"
                                }
                                quick_replies.push(element)
                                console.log(msg,quick_replies)
                                BotService.sendFbQuickReply(session2,msg,quick_replies)
                                HumanHelpService.Add_user(user_id, session2)
                            }


                            session.endDialog()
                        }).catch(err => {
                            session.sendTyping()
                            BotService.sendText(session, {
                                text: 'Product suggest error'
                            })
                            console.log("some error happened in HA send card product suggest" + err)
                            session.endDialog()

                        })
                        session.endDialog()
                    } else {
                        session.sendTyping()
                        BotService.sendText(session, {
                            text: 'Product suggest no product'
                        })
                        session.endDialog()
                    }
                    session.endDialog()
                })
                .catch(err => {
                    console.log("some error happened in HA get product details product suggest" + err)
                    session.sendTyping()
                    BotService.sendText(session, {
                        text: 'Product suggest error'
                    })
                    session.endDialog()
                })
            session.endDialog()
        }
    ]
};