var _            = require( 'lodash'                      )
const nconf   = require( 'nconf'               )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../../constants'             )
var StateService = require( '../../services/StateService' )
var UserService = require( '../../services/UserService' )
var TextService  = require( '../../services/TextService'  )
var BotService   = require( '../../services/BotService'   )
var HumanAugmentationService = require('../../services/human-augmentation-service')
var HumanHelpService = require('./human-help-service')

module.exports = {
    Dialog: [
        function ( session,args,next ) {

            UserService.setActiveState( session.message.address, true)

            session.sendTyping()

            let userName    = session.message.user && session.message.user.name ? session.message.user.name : 'User'
            let channelName = session.message.address.channelId
            let firstName   = userName.split( ' ' )[0]
            let LastName    = userName.split( ' ' )[1]
            let HA_CLIENT_ORGANIZATION = nconf.get( "HUMAN_AUGMENTATION_SERVER_CLIENT_ORGANIZATION" )
            let psid = session.message.address.user.id
            let data

            console.log("The dialog called by ",userName)

            let queue_position = session.userData.user_wait_queue_length

            BotService.sendText(session, {
                text: 'wait ask again'
            })



            let promptObj = {id: null, buttons: []}
            promptObj.buttons.push({
                buttonText: 'Yes',
                triggerIntent: 'USER.WAIT',
                //triggerIntent: 'HA.STOP',
                entityType: '',
                data: {wait: true, queue_length: queue_position + 1}
            })
            promptObj.buttons.push({
                buttonText: 'No',
                triggerIntent: 'USER.WAIT',
                //triggerIntent: 'HA.STOP',
                entityType: '',
                data: {wait: false, queue_length: queue_position + 1}
            })
            StateService.addPrompt2(session, promptObj)
            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)

            BotService.sendQuickReplyButtons(session, {
                text: session.userData.user_wait_queue_text,
                data: {name: userName, count: queue_position + 1}
            }, buttonsText)
            HumanHelpService.setWaitTimer(session.message.address, session, 'xpresso-bot:user-wait', nconf.get("HA_response_timeout"), {timeout: true})
            session.userData.status = "user"
            session.userData.user_wait_queue = true
            session.userData.user_wait_queue_accepted = session.userData.user_wait_queue_accepted
            session.userData.user_wait_queue_length = session.userData.user_wait_queue_length
            session.userData.user_wait_queue_text = session.userData.user_wait_queue_text
            HumanHelpService.Add_user(session.message.address.user.id, session)
            session.endDialog()

        }
    ]
};