var _ = require('lodash')
const nconf = require('nconf')
var builder = require('botbuilder')
var Constants = require('../../constants')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var UserService = require('../../services/UserService')
var TextService = require('../../services/TextService')
var BotService = require('../../services/BotService')
var HumanAugmentationService = require('../../services/human-augmentation-service')
var HumanHelpService = require('./human-help-service')

module.exports = {
    Dialog: [
        function (session, args, next) {

            let user_type

            UserService.setActiveState(session.message.address, true)


            let HA = nconf.get("Human_augmentauion")
            let within_time = true
            let days = nconf.get("HA_TIMIMGS")["DAYS"]

            let start_time = nconf.get("HA_TIMIMGS")["START"]
            let end_given = nconf.get("HA_TIMIMGS")["STOP"]
            let end_time = end_given - 4000

            console.log("Start time is ", start_time)
            console.log("end_time is ", end_time)

            let user_time = parseInt(session.userData.current_local_time)
            let user_day  = session.userData.current_local_date

            console.log("user_time is ", user_time)

            if (user_time < start_time) {
                within_time = false

            } else if (user_time > end_time) {
                within_time = false
            }

           if(! _.includes( days, user_day )){
               within_time = false
           }


            let start = start_time.toString()
            start = start.substr(0, 4)
            let end = end_given.toString()
            end = end.substr(0, 4)

            console.log(start_time.toString())
            console.log("start", start)
            console.log("end", end)


            if (!HA) {

                session.sendTyping()
                let promptObj = {id: null, buttons: []}
                if (StateService2.checkResumeShopping(session)) {
                    promptObj.buttons.push({
                        buttonText: 'Back to Shopping',
                        triggerIntent: 'RESULTS.DIALOG',
                        entityType: '',
                        data: {}
                    })
                }
                StateService.addPrompt2(session, promptObj)
                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                buttonsText.push(Constants.Labels.Introduction)
                buttonsText.push(Constants.Labels.Help)
                BotService.sendQuickReplyButtons(session, {text: 'HA disabled',data: {
                    cs_email_id: nconf.get("cs_email_id"),
                    cs_phone_numer: nconf.get("cs_phone_numer")
                }}, buttonsText)
                session.endDialog()
            } else if ((!within_time) && (session.userData.status != "agent")) {
                session.sendTyping()

                let promptObj = {id: null, buttons: []}
                if (StateService2.checkResumeShopping(session)) {
                    promptObj.buttons.push({
                        buttonText: 'Back to Shopping',
                        triggerIntent: 'RESULTS.DIALOG',
                        entityType: '',
                        data: {}
                    })
                }
                StateService.addPrompt2(session, promptObj)
                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                buttonsText.push(Constants.Labels.Introduction)
                buttonsText.push(Constants.Labels.Help)

                BotService.sendQuickReplyButtons(session, {
                    text: 'Not within HA time',
                    data: {
                        start: start,
                        end: end,
                        start_day: days[0],
                        end_day: days[days.length-1]
                    }
                }, buttonsText)


                session.endDialog()

            } else {


                if (_.has(args, 'entities[0].session')) {
                    session = args.entities[0].session
                    console.log("replaced seesion is ", session)
                }


                if (_.has(args, 'entities[0].data.user_type')) {
                    user_type = args.entities[0].data.user_type
                } else {
                    console.log("******************** No ARGS RECIEVED ******************")
                    session.endDialog()
                    return;
                }

                if (_.has(session, 'userData.status')) {
                    if (session.userData.status == "agent") {
                        if (session.message.text === "SPECIALIST") {
                            session.sendTyping()
                            BotService.sendText(session, {
                                text: 'Specialist asking specialist'
                            })
                            session.endDialog()
                            return;
                        }


                    }
                }

                let userName = session.message.user && session.message.user.name ? session.message.user.name : 'User'
                let channelName = session.message.address.channelId
                let firstName = userName.split(' ')[0]
                let LastName = userName.split(' ')[1]
                let HA_CLIENT_ORGANIZATION = nconf.get("HUMAN_AUGMENTATION_SERVER_CLIENT_ORGANIZATION")
                let psid = session.message.address.user.id
                let data

                if (user_type == "user") {

                    data = {
                        channel_id: channelName,
                        client_organization: HA_CLIENT_ORGANIZATION,
                    }

                    let AllUsersWaitingInQueue = HumanAugmentationService.getAllUsersWaitingInQueue(data)
                    let NextAvailableAgent = HumanAugmentationService.getNextAvailableAgent(data)
                    let getAgentUserQueue = HumanAugmentationService.getAgentUserQueue(data)

                    data = {
                        channel_id: channelName,
                        client_organization: HA_CLIENT_ORGANIZATION,
                        psid: psid
                    }
                    let getUserAccountStatus = HumanAugmentationService.getUserAccountStatus(data)

                    Promise.all([AllUsersWaitingInQueue, NextAvailableAgent, getAgentUserQueue, getUserAccountStatus])
                        .then(([UsersWaitingInQueue, AvailableAgent, AgentUserQueue, UserAccountStatus]) => {

                            //if ((session.userData.status = "user") && (session.userData.user_wait_queue = true)) {
                            if (((UserAccountStatus.user_queue) && (UserAccountStatus.user_queue.length > 0)) || ((UserAccountStatus.agent_user_queue) && (UserAccountStatus.agent_user_queue.length > 0))) {

                                if (((UserAccountStatus.user_queue) && (UserAccountStatus.user_queue.length > 0))) {

                                    let queue_position = _.findIndex(UsersWaitingInQueue, {psid: psid})
                                    console.log("queue_position is ", queue_position)
                                    session.sendTyping()

                                    let promptObj = {id: null, buttons: []}
                                    promptObj.buttons.push({
                                        buttonText: 'Yes',
                                        triggerIntent: 'USER.WAIT',
                                        entityType: '',
                                        data: {wait: true, queue_length: queue_position + 1}
                                    })
                                    promptObj.buttons.push({
                                        buttonText: 'No',
                                        triggerIntent: 'USER.WAIT',
                                        entityType: '',
                                        data: {wait: false, queue_length: queue_position + 1}
                                    })
                                    StateService.addPrompt2(session, promptObj)
                                    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                                    BotService.sendQuickReplyButtons(session, {
                                        text: 'Already in wait queue',
                                        data: {count: queue_position + 1}
                                    }, buttonsText)
                                    HumanHelpService.setWaitTimer(session.message.address, session, 'xpresso-bot:user-wait', nconf.get("HA_response_timeout"), {timeout: true})
                                    session.userData.status = "user"
                                    session.userData.user_wait_queue = true
                                    session.userData.user_wait_queue_accepted = "YES"
                                    session.userData.user_wait_queue_length = queue_position
                                    session.userData.user_wait_queue_text = 'Already in wait queue'
                                    HumanHelpService.Add_user(session.message.address.user.id, session)
                                    session.endDialog()
                                } else if (((UserAccountStatus.agent_user_queue) && (UserAccountStatus.agent_user_queue.length > 0))) {
                                    session.sendTyping()
                                    BotService.sendText(session, {
                                        text: 'Already talking to agent',
                                        data: {
                                            name: firstName + " " + LastName,
                                            agent_name: session.userData.user_engaged_queue.name
                                        }
                                    })

                                    session.endDialog()

                                }
                                session.endDialog()

                            } else {

                                //console.log(UsersWaitingInQueue)
                                //console.log(AvailableAgent)
                                if (UsersWaitingInQueue && UsersWaitingInQueue.length > 0) {

                                    data = {
                                        channel_id: channelName,
                                        psid: psid,
                                        client_organization: HA_CLIENT_ORGANIZATION,
                                    }
                                    session.sendTyping()
                                    HumanAugmentationService.addUserToWaitingQueue(data)
                                        .then(results => {

                                            let promptObj = {id: null, buttons: []}
                                            promptObj.buttons.push({
                                                buttonText: 'Yes',
                                                triggerIntent: 'USER.WAIT',
                                                entityType: '',
                                                data: {wait: true, queue_length: UsersWaitingInQueue.length + 1}
                                            })
                                            promptObj.buttons.push({
                                                buttonText: 'No',
                                                triggerIntent: 'USER.WAIT',
                                                entityType: '',
                                                data: {wait: false, queue_length: UsersWaitingInQueue.length + 1}
                                            })
                                            StateService.addPrompt2(session, promptObj)
                                            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                                            BotService.sendQuickReplyButtons(session, {
                                                text: 'All agents busy Message',
                                                data: {count: UsersWaitingInQueue.length + 1}
                                            }, buttonsText)
                                            HumanHelpService.setWaitTimer(session.message.address, session, 'xpresso-bot:user-wait', nconf.get("HA_response_timeout"), {timeout: true})
                                            session.userData.status = "user"
                                            session.userData.user_wait_queue = true
                                            session.userData.user_wait_queue_accepted = "NO"
                                            session.userData.user_wait_queue_length = UsersWaitingInQueue.length
                                            session.userData.user_wait_queue_text = 'All agents busy Message'
                                            HumanHelpService.Add_user(session.message.address.user.id, session)
                                            session.endDialog()

                                        })
                                        .catch(err => {

                                            console.log("some error happened in HA login")
                                            let msg
                                            //console.log(msg)

                                            if (err && err.status) {

                                                msg = err.response.text

                                            } else {
                                                msg = TextService.text('Api failure message')
                                            }


                                            let buttonsText = []
                                            buttonsText.push(Constants.Labels.Help)
                                            BotService.sendQuickReplyButtons(session, msg
                                                , buttonsText)

                                            session.error(err)
                                            session.endDialog()
                                        })
                                    session.endDialog()

                                } else if ((AvailableAgent) && (AvailableAgent.agent_queue) && (AvailableAgent.agent_queue.length > 0)) {


                                    data = {
                                        channel_id: channelName,
                                        psid: psid,
                                        client_organization: HA_CLIENT_ORGANIZATION,
                                    }
                                    session.sendTyping()
                                    HumanAugmentationService.addUserToWaitingQueue(data)
                                        .then(results => {
                                            HumanHelpService.Add_user(session.message.address.user.id, session)
                                            session.userData.status = "user"
                                            session.userData.user_wait_queue = true
                                            HumanHelpService.Add_user(session.message.address.user.id, session)

                                        }).then(data => {

                                        data = {
                                            channel_id: channelName,
                                            user_psid: psid,
                                            agent_psid: AvailableAgent.psid,
                                            client_organization: HA_CLIENT_ORGANIZATION,


                                        }
                                        HumanAugmentationService.addUserAndAgentToEngagedQueue(data)
                                            .then(results => {
                                                session.userData.user_wait_queue = false
                                                session.userData.user_engaged_queue = {}
                                                session.userData.user_engaged_queue.flag = false
                                                session.userData.user_engaged_queue.wait = true
                                                session.userData.user_engaged_queue.id = AvailableAgent.psid
                                                session.userData.user_engaged_queue.name = AvailableAgent.agent_name
                                                HumanHelpService.Add_user(session.message.address.user.id, session)


                                                let session2 = HumanHelpService.return_user(AvailableAgent.psid)

                                                session2.userData.agent_wait_queue = false
                                                session2.userData.user_engaged_queue = {}
                                                session2.userData.user_engaged_queue.flag = false
                                                session2.userData.user_engaged_queue.wait = true
                                                session2.userData.user_engaged_queue.id = session.message.address.user.id
                                                session2.userData.user_engaged_queue.name = userName
                                                session2.userData.session_id = session.userData.session_id
                                                HumanHelpService.Add_user(AvailableAgent.psid, session2)


                                                let promptObj = {id: null, buttons: []}
                                                promptObj.buttons.push({
                                                    buttonText: 'Yes',
                                                    triggerIntent: 'USER.MATCH.ACCEPT',
                                                    entityType: '',
                                                    data: {wait: true}
                                                })
                                                promptObj.buttons.push({
                                                    buttonText: 'No',
                                                    triggerIntent: 'USER.MATCH.ACCEPT',
                                                    entityType: '',
                                                    data: {wait: false}
                                                })
                                                StateService.addPrompt2(session, promptObj)
                                                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)

                                                BotService.sendQuickReplyButtons(session, {
                                                    text: 'Agent available engage confirmation',
                                                    data: {name: AvailableAgent.agent_name}
                                                }, buttonsText)
                                                HumanHelpService.return_user(AvailableAgent.psid).sendTyping()
                                                BotService.sendText(HumanHelpService.return_user(AvailableAgent.psid), {
                                                    text: 'user available engage confirmation',
                                                    data: {name: firstName + " " + LastName}
                                                })
                                                HumanHelpService.setWaitTimer(session.message.address, session, 'xpresso-bot:user-match-accept', nconf.get("HA_response_timeout"), {timeout: true})
                                                session.endDialog()

                                            })
                                            .catch(err => {

                                                console.log("some error happened in addUserAndAgentToEngagedQueue login", err)

                                                let msg
                                                //console.log(msg)

                                                //if (err && err.status) {

                                                //msg = err.response.text

                                                //} else {
                                                msg = TextService.text('Api failure message')
                                                //}


                                                let buttonsText = []
                                                buttonsText.push(Constants.Labels.Help)
                                                BotService.sendQuickReplyButtons(session, msg
                                                    , buttonsText)

                                                session.error(err)
                                                session.endDialog()
                                            })
                                        session.endDialog()
                                    })
                                        .catch(err => {

                                            console.log("some error happened in addUser to wait queue")
                                            let msg
                                            //console.log(msg)

                                            if (err && err.status) {

                                                msg = err.response.text

                                            } else {
                                                msg = TextService.text('Api failure message')
                                            }


                                            let buttonsText = []
                                            buttonsText.push(Constants.Labels.Help)
                                            BotService.sendQuickReplyButtons(session, msg
                                                , buttonsText)

                                            session.error(err)
                                            session.endDialog()
                                        })
                                    session.endDialog()

                                } else {

                                    if (AgentUserQueue && AgentUserQueue.length > 0) {

                                        console.log(" its when all agents are busy")

                                        data = {
                                            channel_id: channelName,
                                            psid: psid,
                                            client_organization: HA_CLIENT_ORGANIZATION,
                                        }
                                        session.sendTyping()
                                        HumanAugmentationService.addUserToWaitingQueue(data)
                                            .then(results => {

                                                console.log(" its when all agents are busy")
                                                let promptObj = {id: null, buttons: []}
                                                promptObj.buttons.push({
                                                    buttonText: 'Yes',
                                                    triggerIntent: 'USER.WAIT',
                                                    //triggerIntent: 'HA.STOP',
                                                    entityType: '',
                                                    data: {wait: true, queue_length: UsersWaitingInQueue.length + 1}
                                                })
                                                promptObj.buttons.push({
                                                    buttonText: 'No',
                                                    triggerIntent: 'USER.WAIT',
                                                    //triggerIntent: 'HA.STOP',
                                                    entityType: '',
                                                    data: {wait: false, queue_length: UsersWaitingInQueue.length + 1}
                                                })
                                                StateService.addPrompt2(session, promptObj)
                                                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)

                                                BotService.sendQuickReplyButtons(session, {
                                                    text: 'All agents occupied',
                                                    data: {name: userName, count: UsersWaitingInQueue.length + 1}
                                                }, buttonsText)
                                                HumanHelpService.setWaitTimer(session.message.address, session, 'xpresso-bot:user-wait', nconf.get("HA_response_timeout"), {timeout: true})
                                                session.userData.status = "user"
                                                session.userData.user_wait_queue = true
                                                session.userData.user_wait_queue_accepted = "NO"
                                                session.userData.user_wait_queue_length = UsersWaitingInQueue.length
                                                session.userData.user_wait_queue_text = 'All agents occupied'
                                                HumanHelpService.Add_user(session.message.address.user.id, session)
                                                session.endDialog()
                                            })
                                            .catch(err => {

                                                console.log("some error happened in HA login")
                                                let msg
                                                //console.log(msg)

                                                if (err && err.status) {

                                                    msg = err.response.text

                                                } else {
                                                    msg = TextService.text('Api failure message')
                                                }


                                                let buttonsText = []
                                                buttonsText.push(Constants.Labels.Help)
                                                BotService.sendQuickReplyButtons(session, msg
                                                    , buttonsText)

                                                session.error(err)
                                                session.endDialog()
                                            })
                                        session.endDialog()

                                    } else {

                                        let promptObj = {id: null, buttons: []}
                                        if (StateService2.checkResumeShopping(session)) {
                                            promptObj.buttons.push({
                                                buttonText: 'Back To Shopping',
                                                triggerIntent: 'RESULTS.DIALOG',
                                                entityType: 'WISHLIST.BACK.TO.SHOPPING',
                                                data: {}
                                            })
                                        }
                                        session.sendTyping()
                                        StateService.addPrompt2(session, promptObj)
                                        let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                                        buttonsText.push(Constants.Labels.Introduction)
                                        buttonsText.push(Constants.Labels.Help)
                                        BotService.sendQuickReplyButtons(session, {
                                            text: 'All agents unavailable Message'
                                        }, buttonsText)
                                        session.endDialog()
                                    }
                                    session.endDialog()

                                }
                                session.endDialog()

                            }
                            session.endDialog()

                        })
                        .catch(err => {

                            console.log("some error happened in getAllUsersWaitingInQueue/getNextAvailableAgent")
                            let msg
                            //console.log(msg)

                            if (err && err.status) {

                                msg = err.response.text

                            } else {
                                msg = TextService.text('Api failure message')
                            }


                            let buttonsText = []
                            buttonsText.push(Constants.Labels.Help)
                            BotService.sendQuickReplyButtons(session, msg
                                , buttonsText)

                            session.error(err)
                            session.endDialog()
                        })
                    session.endDialog()
                } else if (user_type == "agent") {


                    data = {
                        channel_id: channelName,
                        client_organization: HA_CLIENT_ORGANIZATION,
                    }

                    let AllUsersWaitingInQueue = HumanAugmentationService.getAllUsersWaitingInQueue(data)

                    session.sendTyping()
                    Promise.all([AllUsersWaitingInQueue])
                        .then(([UsersWaitingInQueue]) => {

                            console.log("engaing an agent with human")

                            if (UsersWaitingInQueue && UsersWaitingInQueue.length > 0) {


                                HumanHelpService.Add_user(session.message.address.user.id, session)


                                data = {
                                    channel_id: channelName,
                                    user_psid: UsersWaitingInQueue[0].psid,
                                    agent_psid: psid,
                                    client_organization: HA_CLIENT_ORGANIZATION,
                                }
                                HumanAugmentationService.addUserAndAgentToEngagedQueue(data)
                                    .then(results => {
                                        session.userData.agent_wait_queue = false
                                        session.userData.user_engaged_queue = {}
                                        session.userData.user_engaged_queue.flag = false
                                        session.userData.user_engaged_queue.wait = true
                                        session.userData.user_engaged_queue.id = UsersWaitingInQueue[0].psid
                                        session.userData.user_engaged_queue.name = UsersWaitingInQueue[0].user_name
                                        HumanHelpService.Add_user(session.message.address.user.id, session)

                                        let session2 = HumanHelpService.return_user(UsersWaitingInQueue[0].psid)

                                        session2.userData.user_wait_queue = false
                                        session2.userData.user_engaged_queue = {}
                                        session2.userData.user_engaged_queue.flag = false
                                        session2.userData.user_engaged_queue.wait = true
                                        session2.userData.user_engaged_queue.id = psid
                                        session2.userData.user_engaged_queue.name = userName

                                        session.userData.session_id = session2.userData.session_id
                                        HumanHelpService.Add_user(session.message.address.user.id, session)

                                        //HumanHelpService.Add_user(UsersWaitingInQueue[0].psid, session2)

                                        let promptObj = {id: null, buttons: []}
                                        promptObj.buttons.push({
                                            buttonText: 'Yes',
                                            triggerIntent: 'USER.MATCH.ACCEPT',
                                            entityType: '',
                                            data: {wait: true}
                                        })
                                        promptObj.buttons.push({
                                            buttonText: 'No',
                                            triggerIntent: 'USER.MATCH.ACCEPT',
                                            entityType: '',
                                            data: {wait: false}
                                        })
                                        StateService.addPrompt2(session2, promptObj)
                                        session2.sendTyping()
                                        let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                                        BotService.sendQuickReplyButtons(session2, {
                                            text: 'Agent available engage confirmation',
                                            data: {name: userName}
                                        }, buttonsText)
                                        BotService.sendText(session, {
                                            text: 'user available engage confirmation',
                                            data: {name: UsersWaitingInQueue[0].user_name}
                                        })
                                        //console.log(JSON.stringify(session2.userData.quickReplies))
                                        HumanHelpService.setWaitTimer(session2.message.address, session2, 'xpresso-bot:user-match-accept', nconf.get("HA_response_timeout"), {timeout: true})
                                        session2.endDialog()
                                        console.log("session 2 dialog end")
                                        HumanHelpService.Add_user(UsersWaitingInQueue[0].psid, session2)
                                        console.log("session dialog about to end")
                                        session.endDialog()
                                        console.log("session dialog end")

                                    })
                                    .catch(err => {
                                        o
                                        console.log("some error happened in addUserAndAgentToEngagedQueue login", err)

                                        let msg

                                        msg = TextService.text('Api failure message')
                                        let buttonsText = []
                                        buttonsText.push(Constants.Labels.Help)
                                        BotService.sendQuickReplyButtons(session, msg
                                            , buttonsText)

                                        session.error(err)
                                        session.endDialog()
                                    })
                                session.endDialog()

                            } else {
                                session.userData.agent_wait_queue = true
                                session.userData.user_engaged_queue = null
                                BotService.sendText(session, {
                                    text: 'No user in wait queue'
                                })


                                session.endDialog()


                            }


                            session.endDialog()
                        })
                        .catch(err => {

                            console.log("some error happened in getAllUsersWaitingInQueue/getNextAvailableAgent")
                            let msg
                            //console.log(msg)

                            if (err && err.status) {

                                msg = err.response.text

                            } else {
                                msg = TextService.text('Api failure message')
                            }


                            let buttonsText = []
                            buttonsText.push(Constants.Labels.Help)
                            BotService.sendQuickReplyButtons(session, msg
                                , buttonsText)

                            session.error(err)
                            session.endDialog()
                        })
                    session.endDialog()

                }
                session.endDialog()
            }

            session.endDialog()

        }
    ]
};