var _            = require( 'lodash'                      )
const nconf   = require( 'nconf'               )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../../constants'             )
var StateService = require( '../../services/StateService' )
var UserService = require( '../../services/UserService' )
var TextService  = require( '../../services/TextService'  )
var BotService   = require( '../../services/BotService'   )
var HumanAugmentationService = require('../../services/human-augmentation-service')
var HumanHelpService = require('./human-help-service')

module.exports = {
    Dialog: [
        function ( session,args,next ) {

            UserService.setActiveState( session.message.address, true)

            let userName    = session.message.user && session.message.user.name ? session.message.user.name : 'User'
            let channelName = session.message.address.channelId
            let firstName   = userName.split( ' ' )[0]
            let LastName    = userName.split( ' ' )[1]
            let HA_CLIENT_ORGANIZATION = nconf.get( "HUMAN_AUGMENTATION_SERVER_CLIENT_ORGANIZATION" )
            let psid = session.message.address.user.id
            let data = args.entities[0].data
            let user_type

/// data format is { data : { user_type : '',msg : '', user_id:'',agent_id:'' }}
            //console.log(JSON.stringify(data))

            if (_.has(args, 'entities[0].data.user_type')) {
                user_type = args.entities[0].data.user_type
            } else {
                next();
                return;
            }

            if (user_type == "user") {

                let agent_id = data.agent_id
                    let msg = data.msg

                let session2 = HumanHelpService.return_user(agent_id)
                session2.sendTyping()
                BotService.sendText(session2,msg)


            }else if (user_type == "agent") {
                let user_id = data.user_id
                let msg = data.msg
                let quick_replies =[]

                console.log(" session data is ",JSON.stringify(session.userData))
                console.log(" user id is ",user_id)

                let session2 = HumanHelpService.return_user(user_id)
                session2.sendTyping()
                let promptObj = { id : null, buttons : [] }
                promptObj.buttons.push({
                    buttonText    : "STOP",
                    triggerIntent : 'HA.STOP',
                    data          : {mode : "user" }
                })
                StateService.addPrompt2(session2, promptObj)

                if (session.message.source === "facebook") {

                    let element = {
                        "content_type":"text",
                        "title": "",
                        "payload":"STOP",
                        "image_url":nconf.get("Stop")
                    }
                    quick_replies.push(element)
                    console.log(msg,quick_replies)
                    BotService.sendFbQuickReply(session2,msg,quick_replies)
                    HumanHelpService.Add_user(user_id, session2)
                }else{
                    BotService.sendText(session2,msg)
                }
                session.endDialog()
            }session.endDialog()


        }
    ]
};