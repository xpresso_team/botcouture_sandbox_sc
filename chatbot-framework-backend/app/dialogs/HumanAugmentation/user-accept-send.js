var _            = require( 'lodash'                      )
const nconf   = require( 'nconf'               )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../../constants'             )
var StateService = require( '../../services/StateService' )
var UserService = require( '../../services/UserService' )
var TextService  = require( '../../services/TextService'  )
var BotService   = require( '../../services/BotService'   )
var HumanAugmentationService = require('../../services/human-augmentation-service')
var HumanHelpService = require('./human-help-service')

module.exports = {
    Dialog: [
        function ( session,args,next ) {

            UserService.setActiveState( session.message.address, true)

            session.sendTyping()

            let userName    = session.message.user && session.message.user.name ? session.message.user.name : 'User'
            let channelName = session.message.address.channelId
            let firstName   = userName.split( ' ' )[0]
            let LastName    = userName.split( ' ' )[1]
            let HA_CLIENT_ORGANIZATION = nconf.get( "HUMAN_AUGMENTATION_SERVER_CLIENT_ORGANIZATION" )
            let psid = session.message.address.user.id
            let data

            console.log("The dialog called by ",userName)


            BotService.sendText(session, {
                text: 'wait confirm again'
            })

            let promptObj = {id: null, buttons: []}
            promptObj.buttons.push({
                buttonText: 'Yes',
                triggerIntent: 'USER.MATCH.ACCEPT',
                entityType: '',
                data: {wait: true}
            })
            promptObj.buttons.push({
                buttonText: 'No',
                triggerIntent: 'USER.MATCH.ACCEPT',
                entityType: '',
                data: {wait: false}
            })
            StateService.addPrompt2(session, promptObj)
            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)

            BotService.sendQuickReplyButtons(session, {
                text: 'Agent available engage confirmation',
                data: {name: session.userData.user_engaged_queue.name}
            }, buttonsText)

            HumanHelpService.setWaitTimer(session.message.address, session, 'xpresso-bot:user-match-accept', nconf.get("HA_response_timeout"), {timeout: true})
            HumanHelpService.Add_user(session.message.address.user.id, session)
            session.endDialog()

        }
    ]
};