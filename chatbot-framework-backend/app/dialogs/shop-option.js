/**
 * Created by aashish_amber on 13/5/17.
 */
var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../constants')
var StateService = require('../services/StateService')
var TextService = require('../services/TextService')
var BotService = require('../services/BotService')
var UserService = require('../services/UserService')

module.exports = {
    Dialog: [
        function (session, args) {

            session.sendTyping()

            UserService.setActiveState(session.message.address, true)

            let userName = session.message.user && session.message.user.name ? session.message.user.name : 'User'
            let channelName = session.message.source ? 'on ' + _.startCase(_.toLower(session.message.source)) : ''
            let firstName = userName.split(' ')[0]

            let cards = []
            let card

            if (args && args.entities && args.entities.length > 0 && args.entities[0].entity === "Shop4Me") {

                if (session.message.source === "facebook") {
                    console.log("Sending for ", session.message.source,args.entities[0].entity)

                    let postBackButtons = []

                    postBackButtons.push(
                        {
                            "type": "postback",
                            "title": Constants.Labels.Inspiration,
                            "payload": Constants.Labels.Inspiration
                        }
                    )

                    postBackButtons.push(
                        {
                            "type": "postback",
                            "title": Constants.Labels.SearchItem,
                            "payload": Constants.Labels.SearchItem
                        }
                    )

                    let payload = {
                        "template_type": "button",
                        "text": TextService.text('With Personlaise Msg'),
                        "buttons": postBackButtons
                    }

                    BotService.sendCard(session, postBackButtons, true,"button",TextService.text('With Personlaise Msg'))

                } else {

                    BotService.sendText(session, {text: 'With Personlaise Msg'})
                    card = new builder.HeroCard(session)
                        .title(TextService.text('Button Template Title')[0])
                        .buttons([
                            builder.CardAction.postBack(session, Constants.Labels.Inspiration, Constants.Labels.Inspiration),
                            builder.CardAction.postBack(session, Constants.Labels.SearchItem, Constants.Labels.SearchItem),
                        ])
                    cards.push(card)
                    BotService.sendCard(session, cards)
                }
                session.userData.guestMode = false
            } else if (args && args.entities && args.entities.length > 0 && args.entities[0].entity === "Shop4Other") {
                session.userData.guestMode = true

                if (session.message.source === "facebook") {
                    console.log("Sending for ", session.message.source,args.entities[0].entity)
                    let postBackButtons = []

                    postBackButtons.push(
                        {
                            "type": "postback",
                            "title": Constants.Labels.Inspiration,
                            "payload": Constants.Labels.Inspiration
                        }
                    )

                    postBackButtons.push(
                        {
                            "type": "postback",
                            "title": Constants.Labels.SearchItem,
                            "payload": Constants.Labels.SearchItem
                        }
                    )

                    let payload = {
                        "template_type": "button",
                        "text": TextService.text('Without Personlaise Msg'),
                        "buttons": postBackButtons
                    }

                    BotService.sendCard(session, postBackButtons, true,"button",TextService.text('Without Personlaise Msg'))

                } else {
                    BotService.sendText(session, {text: 'Without Personlaise Msg'})
                    card = new builder.HeroCard(session)
                        .title(TextService.text('Button Template Title')[0])
                        .buttons([
                            builder.CardAction.postBack(session, Constants.Labels.Inspiration, Constants.Labels.Inspiration),
                            builder.CardAction.postBack(session, Constants.Labels.SearchItem, Constants.Labels.SearchItem),
                        ])
                    cards.push(card)
                    BotService.sendCard(session, cards)
                }

            }


            session.endDialog()
        }]
};

