var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../../constants')
var ApiService = require('../../services/ApiService')
var TextService = require('../../services/TextService')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var UserService = require('../../services/UserService')
var BotService = require('../../services/BotService')
var ResultsService = require('../results/results-service')
var TypeAQueryService = require( '../type_a_query/type-a-query-service' )
var nconf = require('nconf')
var toTitleCase = require('to-title-case')

module.exports = {

    Label: "NLP-RELAXATION",

    Dialog: [

        function (session, args, next) {
            session.sendTyping()
            UserService.setActiveState(session.message.address, true)
            let NODE = session.userData.currentNode
            let attributes

            console.log("args are",args)


            if (_.has(args, 'entities[0].data.attributes')) {
                attributes = JSON.parse(args.entities[0].data.attributes)


            console.log("attributes are ", attributes)


            let intent = {
                intent: 'RESULTS.DIALOG',
                score: 1,
                entities: [{
                    entityType: 'RESULT.TOGGLE',
                }]
            }

            let attributesToReset = attributes.remove_attributes

            for(let i =0;i< attributesToReset.length;i++){
                StateService2.resetAFilter(NODE, session, attributesToReset[i])
            }

            let attributesToSet = attributes.combination

            for(let i =0;i< attributesToSet.length;i++){

                session.userData[NODE][attributesToSet[i].attribute_key] =attributesToSet[i].attribute_value

            }




            session.replaceDialog('xpresso-bot:results-dialog', intent)

            }else if(_.has(args, 'entities[0].data.to_remove')){
                if(args.entities[0].entity == "Yes") {

                    attributes = args.entities[0].data.to_remove


                    console.log("attributes are ", attributes)


                    let intent = {
                        intent: 'RESULTS.DIALOG',
                        score: 1,
                        entities: [{
                            entityType: 'RESULT.TOGGLE',
                        }]
                    }


                    for (let i = 0; i < attributes.length; i++) {
                        StateService2.resetAFilter(NODE, session, attributes[i])
                    }
                    session.replaceDialog('xpresso-bot:results-dialog', intent)
                }else if(args.entities[0].entity == "No"){
                    let NODE = Constants.NODE.NLP
                    ResultsService.setVariableByState( session, NODE, 'nlp_reset_flag', 1 )
                    let buttonsText = []
                    for (let i = 0; i < TextService.text('No_suggestion_required_quick_reply').length; i++) {
                        buttonsText.push(TextService.text('No_suggestion_required_quick_reply')[i])
                    }
                    BotService.sendQuickReplyButtons(session, {text: 'No_suggestion_required'}, buttonsText)
                    session.endDialog()
                }

                session.endDialog()

            }else {
                session.endDialog()
                next();
                return;
            }

        }]

}