var _            = require( 'lodash'                      )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../../constants'             )
var ApiService   = require( '../../services/ApiService'   )
var StateService = require( '../../services/StateService' )
var StateService2 = require( '../../services/StateService2' )
var TextService  = require( '../../services/TextService'  )
var UtilService  = require( '../../services/UtilService'  )
var UserService  = require( '../../services/UserService'  )
var BotService   = require( '../../services/BotService'   )
var nconf        = require( 'nconf')

module.exports = {
  Label: Constants.Labels.TypeQuery,
  Dialog: [
    function ( session, args, next ) {

      // Fall through
      if ( ! args ) { next(); return; }
      session.sendTyping()
        UserService.setActiveState( session.message.address, true)
        let personalisation_enabled = nconf.get('Personalisation')

        if (!personalisation_enabled) {
            let promptObj = {
                id: null,
                buttons: [
                    {
                        buttonText    : 'Men',
                        triggerIntent : 'TYPE.A.QUERY',
                        entityType    : 'TYPE.A.QUERY.GENDER',
                        data          : { userData : { gender : [ 'men' ], pageNo : 0 } }
                    },
                    {
                        buttonText    : 'Women',
                        triggerIntent : 'TYPE.A.QUERY',
                        entityType    : 'TYPE.A.QUERY.GENDER',
                        data          : { userData : { gender : [ 'women' ], pageNo : 0 } }
                    }]
            }
            StateService.addPrompt2(session, promptObj)

            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)

            BotService.sendQuickReplyButtons(session, {
                text: 'Type a Query Gender Question',
                data: {entity: session.userData[Constants.NODE.NLP].entity}
            }, buttonsText)
            // builder.Prompts.choice( session, TextService.text( 'Type a Query Gender Question', { entity : session.userData.entity } ), buttonsText, {
            //   maxRetries: 0,
            //   listStyle: builder.ListStyle.button
            // })
            session.endDialog()

        } else {


            // Get user's gender
            UserService.getUser(session.message.address)
                .then(userObj => {

                    session.userData.gender = userObj.gender

                    if ( userObj.gender && ! session.userData.guestMode ) {

                        // let intent = {
                        //     intent: 'TYPE.A.QUERY.POSTBACK',
                        //     score: 1,
                        //     entities: [{
                        //         entity: userObj.gender,
                        //         entityType: 'TYPE.A.QUERY.GENDER',
                        //         data: {gender: userObj.gender, pageNo: 0}
                        //     }]
                        // }
                        // TODO : ???
                        let args = {
                            entities: [ { data : { userData : { gender : [ userObj.gender ] } } } ]
                        }
                        session.replaceDialog('xpresso-bot:type-query', args )

                    } else {

                        if (session.userData.guestMode) {
                           // BotService.sendText(session, 'Guest Mode Reminder')
                            // session.send( TextService.text( 'Guest Mode Reminder' ) )
                        }

                        let promptObj = {
                            id: null,
                            buttons: [
                                {
                                    buttonText: 'Men',
                                    triggerIntent: 'TYPE.A.QUERY',
                                    entityType: 'TYPE.A.QUERY.GENDER',
                                    data : { userData : { gender : [ 'men' ], pageNo: 0 } }
                                    // data: { gender: 'men', pageNo: 0 }
                                },
                                {
                                    buttonText: 'Women',
                                    triggerIntent: 'TYPE.A.QUERY',
                                    entityType: 'TYPE.A.QUERY.GENDER',
                                    data : { userData : { gender : [ 'women' ], pageNo: 0 } }
                                    // data: { gender: 'women', pageNo: 0 }
                                }]
                        }

                        StateService.addPrompt2(session, promptObj)
                        let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                        BotService.sendQuickReplyButtons(session, {
                            text: 'Type a Query Gender Question',
                            data: {entity: session.userData[Constants.NODE.NLP].entity}
                        }, buttonsText)

                        session.endDialog()
                    }

                })
                .catch(err => {

                    let promptObj = { id : null, buttons : [] }
                    if (StateService2.checkResumeShopping(session)) {
                        promptObj.buttons.push({
                            buttonText: 'Back To Shopping',
                            triggerIntent: 'RESULTS.DIALOG',
                            entityType: 'WISHLIST.BACK.TO.SHOPPING',
                            data: {}
                        })
                    }
                    StateService.addPrompt2(session, promptObj)
                    //console.log("postback replied")

                    let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
                    buttonsText.push(Constants.Labels.Introduction)
                    buttonsText.push(Constants.Labels.Help)
                    BotService.sendQuickReplyButtons( session, { text : 'Api failure message'}, buttonsText )
                    session.error(err)
                    session.endDialog()
                })

            session.endDialog()
        }
    }
  ]
}