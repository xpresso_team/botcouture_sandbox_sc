var _            = require( 'lodash'                      )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../../constants'             )
var StateService = require( '../../services/StateService' )
var StateService2 = require( '../../services/StateService2' )
var TextService  = require( '../../services/TextService'  )
var UserService  = require( '../../services/UserService'  )
var ApiService   = require( '../../services/ApiService'   )
var UtilService  = require( '../../services/UtilService'  )
var BotService   = require( '../../services/BotService'   )
var toTitleCase  = require( 'to-title-case'               )

module.exports = {
  Label: Constants.Labels.Introduction,
  Dialog: [   
  function ( session, args, next ) {

    if ( ! args ) { next(); return; }
    session.sendTyping()

      UserService.setActiveState( session.message.address, true)

    let entityType = args.entities[0].entityType
    if ( _.includes( [ 'PERSONALIZATION.EDIT.PREFERENCES.ENTITY', 'PERSONALIZATION.FILL.OUT.PROFILE.ENTITY' ], entityType ) ) {
        
      let unfilledEntity   = args.entities[0].data.unfilledEntity
      let gender           = args.entities[0].data.gender
      let attribute        = args.entities[0].data.attribute
      let question         = args.entities[0].data.question
      let POST_BACK_PREFIX = args.entities[0].data.POST_BACK_PREFIX
      let REPLACE_DIALOG   = args.entities[0].data.REPLACE_DIALOG
      //console.log( 'BACK -->', args.entities[0].data)
      // Get xc_category from intent first
      ApiService.getIntent( unfilledEntity,session )
      .then( intentObj => {

        // Get a list of unique values
        // return ApiService.getUniqueAttributeValues( intentObj, attribute )
        
        // ALTERNATIVELY
        let userData = intentObj
        if ( ! userData.gender || userData.gender.length == 0 ) {
          userData.gender = [ gender ]
        }
        return ApiService.structuredSearch( userData,session).then( result => {
          
          result.Results = result.Results || []
          //if ( result.Results.length === 0 ) result.Results = result[ "Suggested Results" ]
          
          let optionList = []
          if ( _.includes( [ 'size', 'colorsavailable', 'subcategory' ], attribute ) ) {
            optionList = _( result.Results ).filter( item => item[ attribute ] ).flatMap( item => JSON.parse( item[ attribute ] ) ).uniq().value()
          } else {
            optionList = _( result.Results ).map( item  => item[ attribute ] ).uniq().value() 
          }

          optionList = _.sortBy( optionList )
          return optionList

        })

      })
      .then( optionList => {
        
        optionList = optionList || []

        if ( optionList.length ) {
          
          optionList = optionList.map( option => {
            return { text : option,  postback : POST_BACK_PREFIX + '::' + unfilledEntity + '::' + attribute + '::' + option + '::' + REPLACE_DIALOG + '::' + gender}
          })
          
          let pageNo   = Number( args.entities[0].data.pageNo ) || 0
          let pageSize = 28 + 2                             // 2 for Prev/Next buttons      
          pageSize     = pageSize < 30 ? pageSize : 30      // We can show 3 * 10 buttons on FB in total
          let showNext = { text : 'Next Page' , postback : POST_BACK_PREFIX + '::' + unfilledEntity + '::' + attribute + '::' + REPLACE_DIALOG + '::NEXTPAGE::' + ( pageNo + 1 ) + '::' + gender }
          showNext = ( pageSize - 2 ) * ( pageNo + 1 ) < optionList.length ? showNext : null
          let showPrevious = null

          let optionListWindow = UtilService.pageList( optionList, pageNo, pageSize, showPrevious, showNext )

          let cards = []
          for( let i = 0; i < optionListWindow.length; i = i + 3 ) {
            let buttons = []
            if ( i     < optionListWindow.length ) buttons.push( builder.CardAction.postBack( session, optionListWindow[ i     ].postback, _.toUpper( optionListWindow[ i     ].text ) ) )      // i know
            if ( i + 1 < optionListWindow.length ) buttons.push( builder.CardAction.postBack( session, optionListWindow[ i + 1 ].postback, _.toUpper( optionListWindow[ i + 1 ].text ) ) )
            if ( i + 2 < optionListWindow.length ) buttons.push( builder.CardAction.postBack( session, optionListWindow[ i + 2 ].postback, _.toUpper( optionListWindow[ i + 2 ].text ) ) )
            let card = new builder.HeroCard( session ).title( 'Select ' + _.startCase( attribute ) + ' for ' + _.startCase( unfilledEntity ) ).buttons( buttons )
            cards.push( card )
          }

          if ( question ) {
            // TODO: Make sure questin is always here, not just for first time
            BotService.sendText( session, question )
            // session.send( question )
          }
          BotService.sendCard( session, cards )
          // var reply = new builder.Message( session ).attachmentLayout( builder.AttachmentLayout.carousel ).attachments( cards );
          // session.send( reply )
          
          // Quick reply buttons: Skip, Finish Profile Later, Edit Previous
          let promptObj = { id : null, buttons : [] }        
          //promptObj.buttons.push( { buttonText : 'Skip', triggerIntent : 'PERSONALIZATION.FILL.OUT.PROFILE', entityType : 'PERSONALIZATION.FILL.OUT.PROFILE.SKIP', data : { unfilledEntity : unfilledEntity, attribute : attribute, option : '<SKIP>' } } )
          promptObj.buttons.push( { buttonText : 'No Preference', triggerIntent : 'PERSONALIZATION.SAVE.TO.PROFILE', entityType : 'PERSONALIZATION.SHOW.ATTRIBUTE.LIST.NO.PREFERENCE', 
            data : { unfilledEntity : unfilledEntity, attribute : attribute, option : '<NOPREFERENCE>', REPLACE_DIALOG : REPLACE_DIALOG, gender : gender } } )
          promptObj.buttons.push( { buttonText : 'Finish Profile Later', triggerIntent : 'INTRODUCTION', entityType : 'PERSONALIZATION.FILL.OUT.PROFILE.FINISHLATER', data : { } } )

          StateService.addPrompt2( session, promptObj )
          let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
          BotService.sendQuickReplyButtons( session, 'You can also...', buttonsText )
          // builder.Prompts.choice( session, 'You can also...', buttonsText, {
          //   maxRetries: 0,
          //   listStyle: builder.ListStyle.button
          // })
          
        } else {

          BotService.sendText( session, 'Looks like ' + _.startCase( unfilledEntity ) + ' does not have ' + _.startCase( attribute ) + 's to choose from.' )
          // session.send( 'Looks like ' + _.startCase( unfilledEntity ) + ' does not have ' + _.startCase( attribute ) + 's to choose from.' )
          
          let promptObj = { id : null, buttons : [] }
          promptObj.buttons.push( {
            buttonText    : 'Complete Profile Later',
            triggerIntent : 'SEARCH-ITEM',
            entityType    : 'PERSONALIZATION.WELCOME.BUTTON',
            data          : {  }
          } )

          promptObj.buttons.push( {
            buttonText    : 'Shopping for Someone Else',
            triggerIntent : 'SEARCH-ITEM',
            entityType    : 'PERSONALIZATION.WELCOME.BUTTON',
            data          : {  }
          } )

          promptObj.buttons.push( {
            buttonText    : 'Help',
            triggerIntent : 'HELP',
            entityType    : 'PERSONALIZATION.WELCOME.BUTTON',
            data          : {  }
          } )

          StateService.addPrompt2( session, promptObj )

          let buttons = _.map( promptObj.buttons, button => button.buttonText )
          BotService.sendQuickReplyButtons( session, 'Personalization Welcome', buttons )
          // builder.Prompts.choice( session, TextService.text( 'Personalization Welcome' ), buttons, {
          //   maxRetries: 0,
          //   listStyle: builder.ListStyle.button
          // })

          /*
          // TODO: Think of a better way to handle this case
          let args = {
            intent: 'PERSONALIZATION.SAVE.TO.PROFILE',
            score : 1,
            entities: [ {
              enitity    : '',
              entityType : 'PERSONALIZATION.SHOW.ATTRIBUTE.LIST.NO.PREFERENCE',
              data       : { unfilledEntity : unfilledEntity, attribute : attribute, option : '<NOPREFERENCE>', REPLACE_DIALOG : REPLACE_DIALOG }
            } ]
          }
          session.replaceDialog( REPLACE_DIALOG, args )
          */
        }

      })
      .catch( err => {

          let promptObj = { id : null, buttons : [] }
          if (StateService2.checkResumeShopping(session)) {
              promptObj.buttons.push({
                  buttonText: 'Back To Shopping',
                  triggerIntent: 'RESULTS.DIALOG',
                  entityType: 'WISHLIST.BACK.TO.SHOPPING',
                  data: {}
              })
          }
          StateService.addPrompt2(session, promptObj)
          //console.log("postback replied")

          let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
          buttonsText.push(Constants.Labels.Introduction)
          buttonsText.push(Constants.Labels.Help)
          BotService.sendQuickReplyButtons( session, { text : 'Api failure message'}, buttonsText )
        //BotService.sendQuickReplyButtons( session, 'Api failure message', [ Constants.Labels.Introduction, Constants.Labels.Help ] )
        // builder.Prompts.choice( session, TextService.text( 'Api failure message' ), [ Constants.Labels.Introduction, Constants.Labels.Help ], {
        //   maxRetries: 0,
        //   listStyle: builder.ListStyle.button
        // })
        session.error( err )      
      })

    }
    
  },
  function ( session, result ) {
    session.reset()
  }]
}