var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../../constants')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var TextService = require('../../services/TextService')
var UserService = require('../../services/UserService')
var BotService = require('../../services/BotService')
var ResultsService = require('../results/results-service')
const uuidV4 = require('uuid/v4')
var nconf = require('nconf')

module.exports = {
    Label: Constants.Labels.Introduction,
    Dialog: [
        function (session,args) {

            session.sendTyping()

            ResultsService.setVariableByState(session, Constants.NODE.NLP, 'nlp_reset_flag', 1)
            UserService.setActiveState(session.message.address, true)

            let userProfilePromise = UserService.getUserProfile(session.message.address)
            let userObjPromise = UserService.getUser(session.message.address)

            let userName = session.message.user && session.message.user.name ? session.message.user.name : 'User'
            let channelName = session.message.source ? 'on ' + _.startCase(_.toLower(session.message.source)) : ''
            let firstName = userName.split(' ')[0]
            let url = nconf.get('WebView')["Url"] + nconf.get('WebView')["Profile"] + "?psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()
            console.log("url is "+ url)

            let msg = ""

            if(_.has(args,'entities[0].data.ProfileMsg')&&(args.entities[0].data.ProfileMsg=="new_user")){
                msg = TextService.text( 'New user Profile creation msg')

            }else{

                msg = TextService.text('Profile Msg')

            }

            if((args)&&(args.entities)&&(args.entities.length>0)&&(args.entities[0].data)&&(args.entities[0].data.ProfileMsg)&&(args.entities[0].data.ProfileMsg=="new_user")){
                msg = TextService.text( 'New user Profile creation msg')

            }else{

                    msg = TextService.text('Profile Msg')

            }

            Promise.all([userProfilePromise, userObjPromise])
                .then(([userProfileArray, userObj]) => {

                    let gender = userObj.gender
                    session.userData.gender = gender
                    let postBackButtons = []
                    let urlButtons = []

                    if(gender && (args)&&(args.entities)&&(args.entities.length>0)&&(args.entities[0].entityType)&& (args.entities[0].entityType ==='USE MY PROFILE')){

                        let intent = {
                            intent: 'Shop-Option',
                            score: 1,
                            entities: [{
                                entity: "Shop4Me"
                            }]
                        }
                        session.replaceDialog('xpresso-bot:shop-option', intent)

                    }
                    else if (gender) {
                        if (session.message.source === "facebook") {

                            postBackButtons.push(
                                {
                                    "type": "postback",
                                    "title": TextService.text('Profile Option')[1],
                                    "payload": TextService.text('Profile Postback')[1]
                                }
                            )

                            postBackButtons.push(
                                {
                                    "type": "web_url",
                                    "title": TextService.text('Profile Option')[2],
                                    "url": url,
                                    "messenger_extensions": true,
                                    "webview_height_ratio": nconf.get('WebView_size')["Profile"]
                                }
                            )


                        } else {

                            postBackButtons.push({
                                buttonText: TextService.text('Profile Option')[1],
                                postback: TextService.text('Profile Postback')[1]
                            })


                            urlButtons.push({
                                buttonUrl: url,
                                buttonText: TextService.text('Profile Option')[2]
                            })
                        }


                    } else {

                        if (session.message.source === "facebook") {

                            postBackButtons.push(
                                {
                                    "type": "web_url",
                                    "title": TextService.text('Profile Option')[0],
                                    "url": url ,
                                    "messenger_extensions": true,
                                    "webview_height_ratio": nconf.get('WebView_size')["Profile"]
                                }
                            )

                            let intent = {
                                intent: 'PERSONALIZATION.WELCOME',
                                entities: [{
                                    entityType: 'USE MY PROFILE',
                                    data: {ProfileMsg:"new_user"}
                                }]
                            }

                            let PostBack = 'POSTBACK::' + JSON.stringify(intent)

                            postBackButtons.push(
                                {
                                    "type": "postback",
                                    "title": TextService.text('Profile Option')[1],
                                    "payload": PostBack
                                }
                            )

                        } else {


                            urlButtons.push({
                                buttonUrl: url,
                                buttonText: TextService.text('Profile Option')[0]
                            })

                            let intent = {
                                intent: 'PERSONALIZATION.WELCOME',
                                entities: [{
                                    entityType: 'USE MY PROFILE',
                                    data: {ProfileMsg:"new_user"}
                                }]
                            }

                            let PostBack = 'POSTBACK::' + JSON.stringify(intent)

                            postBackButtons.push({
                                buttonText: TextService.text('Profile Option')[1],
                                postback: PostBack
                            })


                        }

                    }

                    if (session.message.source === "facebook") {



                        postBackButtons.push(
                            {
                                "type": "postback",
                                "title": TextService.text('Profile Option')[3],
                                "payload": TextService.text('Profile Postback')[3]
                            }
                        )

                        let payload = {
                            "template_type": "button",
                            "text":msg,
                            "buttons": postBackButtons
                        }

                        BotService.sendCard(session, postBackButtons,true,"button",msg)
                        session.endDialog()


                    } else {

                        // 3rd buttonn is common

                        postBackButtons.push({
                            buttonText: TextService.text('Profile Option')[3],
                            postback: TextService.text('Profile Postback')[3]
                        })

                        //builder.CardAction.openUrl(session, item.product_url || 'http://www.google.com/', 'View on ' + _.startCase(item.client_name)),
                        let cardButtons = _.map(postBackButtons, button => builder.CardAction.postBack(session, button.postback, button.buttonText))
                        cardButtons = cardButtons.concat(_.map(urlButtons, button => builder.CardAction.openUrl(session, button.buttonUrl, button.buttonText)))
                        let card = new builder.HeroCard(session).title(TextService.text('Button Template Title')[0]).buttons(cardButtons)
                        BotService.sendText(session, msg)
                        BotService.sendCard(session, [card])
                        session.endDialog()

                    }



                })
                .catch(err => {

                    let promptObj = {id: null, buttons: []}
                    if (StateService2.checkResumeShopping(session)) {
                        promptObj.buttons.push({
                            buttonText: 'Back To Shopping',
                            triggerIntent: 'RESULTS.DIALOG',
                            entityType: 'WISHLIST.BACK.TO.SHOPPING',
                            data: {}
                        })
                    }
                    StateService.addPrompt2(session, promptObj)
                    //console.log("postback replied")

                    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                    buttonsText.push(Constants.Labels.Introduction)
                    buttonsText.push(Constants.Labels.Help)
                    BotService.sendQuickReplyButtons(session, {text: 'Api failure message'}, buttonsText)
                    session.error(err)
                    session.endDialog()
                })

            session.endDialog()

        }]

}