var _            = require( 'lodash'                      )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../../constants'             )
var StateService = require( '../../services/StateService' )
var StateService2 = require( '../../services/StateService2' )
var TextService  = require( '../../services/TextService'  )
var UserService  = require( '../../services/UserService'  )
var ApiService   = require( '../../services/ApiService'   )
var UtilService  = require( '../../services/UtilService'  )
var BotService   = require( '../../services/BotService'   )
var toTitleCase  = require( 'to-title-case'               )

module.exports = {
  Label: Constants.Labels.Introduction,
  Dialog: [   
  function ( session, args, next ) {

    if ( ! args ) { next(); return; }
    session.sendTyping()
      UserService.setActiveState( session.message.address, true)

    let entityType     = args.entities[0].entityType
    let unfilledEntity = args.entities[0].data.unfilledEntity
    let gender         = args.entities[0].data.gender 
    let attribute      = args.entities[0].data.attribute
    let option         = args.entities[0].data.option
    let REPLACE_DIALOG = args.entities[0].data.REPLACE_DIALOG

    if ( _.includes( [ 'PERSONALIZATION.FILL.OUT.PROFILE.OPTION', 'PERSONALIZATION.FILL.OUT.PROFILE.SKIP', 'PERSONALIZATION.EDIT.PREFERENCES.OPTION' ], entityType ) ) {

      UserService.appendToUserProfileByEntity( session.message.address, unfilledEntity, attribute, option,session.userData.Profile_gender)
      .then( success => {
        BotService.sendText( session, 'Your preferences: ' + toTitleCase( unfilledEntity ) + ' ' + toTitleCase( attribute ) + ': ' + toTitleCase( option ) )
        // session.send( 'Your preferences: ' + toTitleCase( unfilledEntity ) + ' ' + toTitleCase( attribute ) + ': ' + toTitleCase( option ) )
        session.replaceDialog('xpresso-bot:'+REPLACE_DIALOG, args )
      })
      .catch( err => {

          let promptObj = { id : null, buttons : [] }
          if (StateService2.checkResumeShopping(session)) {
              promptObj.buttons.push({
                  buttonText: 'Back To Shopping',
                  triggerIntent: 'RESULTS.DIALOG',
                  entityType: 'WISHLIST.BACK.TO.SHOPPING',
                  data: {}
              })
          }
          StateService.addPrompt2(session, promptObj)
          //console.log("postback replied")

          let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
          buttonsText.push(Constants.Labels.Introduction)
          buttonsText.push(Constants.Labels.Help)
          BotService.sendQuickReplyButtons( session, { text : 'Api failure message'}, buttonsText )
        session.error( err )
      })

    }

    if ( 'PERSONALIZATION.SHOW.ATTRIBUTE.LIST.NO.PREFERENCE' === entityType ) {

      UserService.saveUserProfileByEntity( session.message.address, unfilledEntity, attribute, [ option ],session.userData.Profile_gender )
      .then( success => {
        BotService.sendText( session, 'Cleared preferences for ' + _.startCase( unfilledEntity ) + ' ' + _.startCase( attribute ) )
        // session.send( 'Cleared preferences for ' + _.startCase( unfilledEntity ) + ' ' + _.startCase( attribute ) )
        session.replaceDialog( 'xpresso-bot:'+REPLACE_DIALOG, args )
      })
      .catch( err => {
          let promptObj = { id : null, buttons : [] }
          if (StateService2.checkResumeShopping(session)) {
              promptObj.buttons.push({
                  buttonText: 'Back To Shopping',
                  triggerIntent: 'RESULTS.DIALOG',
                  entityType: 'WISHLIST.BACK.TO.SHOPPING',
                  data: {}
              })
          }
          StateService.addPrompt2(session, promptObj)
          //console.log("postback replied")

          let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
          buttonsText.push(Constants.Labels.Introduction)
          buttonsText.push(Constants.Labels.Help)
          BotService.sendQuickReplyButtons( session, { text : 'Api failure message'}, buttonsText )
        session.error( err )
      })

    }
    
  },
  function ( session, result ) {
    session.reset()
  }]
};