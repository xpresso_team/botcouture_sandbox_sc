var _            = require( 'lodash'                      )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../../constants'             )
var StateService = require( '../../services/StateService' )
var StateService2 = require( '../../services/StateService2' )
var TextService  = require( '../../services/TextService'  )
var UserService  = require( '../../services/UserService'  )
var BotService   = require( '../../services/BotService'   )

module.exports = {
  Label: Constants.Labels.Introduction,
  Dialog: [   
  function ( session, args, next ) {

    if ( !args ) { next(); return; }
    session.sendTyping()

    let entityType = args.entities[0].entityType
    let gender     = args.entities[0].data.gender
      UserService.setActiveState( session.message.address, true)
      
    if ( entityType === 'PERSONALIZATION.WELCOME.ASK.GENDER' ) {

      let promptObj = { id : null, buttons : [] }

      promptObj.buttons.push( {
        buttonText    : 'Men',
        triggerIntent : 'PERSONALIZATION.ASK.GENDER',
        entityType    : 'PERSONALIZATION.ASK.GENDER.MEN',
        data          : { gender : 'men' }
      } )

      promptObj.buttons.push( {
        buttonText    : 'Women',
        triggerIntent : 'PERSONALIZATION.ASK.GENDER',
        entityType    : 'PERSONALIZATION.ASK.GENDER.WOMEN',
        data          : { gender : 'women' }
      } )

      StateService.addPrompt2( session, promptObj )

      let buttons = _.map( promptObj.buttons, button => button.buttonText )
      BotService.sendQuickReplyButtons( session,TextService.text('Gender_Select'), buttons )
      // builder.Prompts.choice( session, 'First, please select your gender: ', buttons, {
      //   maxRetries: 0,
      //   listStyle: builder.ListStyle.button
      // })

    }

    if ( _.includes( [ 'PERSONALIZATION.ASK.GENDER.MEN', 'PERSONALIZATION.ASK.GENDER.WOMEN' ], entityType ) ) {

      UserService.updateUser( session.message.address, { gender : gender } )
      .then( noop => {

        let args = {
          intent   : 'PERSONALIZATION.FILL.OUT.PROFILE',
          entities : [ { entity: gender, entityType: 'PERSONALIZATION.ASKS.GENDER', data : { gender : gender } } ]
        }
        
        session.replaceDialog( 'xpresso-bot:personalization-fill-out-profile', args )

      })
      .catch( err => {

          let promptObj = { id : null, buttons : [] }
          if (StateService2.checkResumeShopping(session)) {
              promptObj.buttons.push({
                  buttonText: 'Back To Shopping',
                  triggerIntent: 'RESULTS.DIALOG',
                  entityType: 'WISHLIST.BACK.TO.SHOPPING',
                  data: {}
              })
          }
          StateService.addPrompt2(session, promptObj)
          //console.log("postback replied")

          let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
          buttonsText.push(Constants.Labels.Introduction)
          buttonsText.push(Constants.Labels.Help)
          BotService.sendQuickReplyButtons( session, { text : 'Api failure message'}, buttonsText )

  
        //BotService.sendQuickReplyButtons( session, 'Api failure message', [ Constants.Labels.Introduction, Constants.Labels.Help ] )/// builder.Prompts.choice( session, TextService.text( 'Api failure message' ), [ Constants.Labels.Introduction, Constants.Labels.Help ], {
        //   maxRetries: 0,
        //   listStyle: builder.ListStyle.button
        // })
        session.error( err )
      })

    }

  },
  function ( session, result ) {
    session.reset()
  }]
};