var _            = require( 'lodash'                      )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../../constants'             )
var StateService = require( '../../services/StateService' )
var StateService2 = require( '../../services/StateService2')
var TextService  = require( '../../services/TextService'  )
var UserService  = require( '../../services/UserService'  )
var ApiService   = require( '../../services/ApiService'   )
var UtilService  = require( '../../services/UtilService'  )
var BotService   = require( '../../services/BotService'   )
var toTitleCase  = require( 'to-title-case'               )


module.exports = {
  Label: Constants.Labels.Introduction,
  Dialog: [   
  function ( session, args, next ) {

    if ( ! args ) { next(); return; }
    session.sendTyping()

    let entityType = args.entities[0].entityType
    let gender     = args.entities[0].data.gender
      UserService.setActiveState( session.message.address, true)

//// Handle incoming response ////////////////////////////////////

    if ( entityType === 'PERSONALIZATION.WELCOME.FILL.OUT.PROFILE.BUTTON' ) {
      BotService.sendText( session, 'Personalization Fill Out Profile' )
    }

//// Handle outgoing response ////////////////////////////////////

    let allQuestions = TextService.getAllPersonalizationQuestion()
    allQuestions= _.filter( allQuestions, ( arr, key ) => arr[0] === gender )

    UserService.getUserProfile( session.message.address )
    .then( userProfileArray => {

      let { unfilledProfile, unfilledEntity } = UserService.getUserProfileUnfilledQuestion( userProfileArray, allQuestions )
      
      // console.log( unfilledProfile, unfilledEntity )

      if ( unfilledProfile ) {
        
        // console.log( 'unfilledProf', unfilledProfile)
        // console.log ( unfilledEntity, unfilledProfile )
        
        let attribute = unfilledProfile[ 2 ]
        let question  = unfilledProfile[ 3 ]

        let args = {
          intent: 'PERSONALIZATION.SAVE.TO.PROFILE',
          score : 1,
          entities: [{
            entity : null,
            entityType : 'PERSONALIZATION.FILL.OUT.PROFILE.ENTITY',
            data: {
              unfilledEntity: unfilledEntity,
              gender : gender,
              attribute : attribute,
              question : question,
              POST_BACK_PREFIX : 'PERSONALIZATION_FILL_OUT_PROFILE', 
              REPLACE_DIALOG : 'personalization-fill-out-profile'
            }
          }]
        }
        session.replaceDialog( 'xpresso-bot:personalization-show-attributes-list', args )

      } else {
        //BotService.sendText(session, 'You already have filled your profile.')

        let msg = 'Your preferences are: \n\n'
        _.each( userProfileArray, userProfile => {
          _.each( [ 'aspect', 'brand', 'size', 'color' ], attribute => {
            
            if ( userProfile[ attribute ] && userProfile[ attribute ] !== null ) {
              let attrStr = ''
              if ( _.includes( userProfile[ attribute ], '<NOPREFERENCE>' ) ) {
                attrStr = "No preference"
              } else {
                attrStr = userProfile[ attribute ].join( ',  ')
              }  
              msg += ' - ' + toTitleCase( userProfile.entity ) + ' ' + toTitleCase( attribute ) + ': ' + toTitleCase( attrStr ) + '\n\n'
            }
          })
        })
          BotService.sendText(session, msg )
        let promptObj = { id : null, buttons : [] }        
        promptObj.buttons.push( { buttonText : 'Edit Preferences', triggerIntent : 'PERSONALIZATION.EDIT.PREFERENCES', entityType : 'PERSONALIZATION.FILL.OUT.PROFILE.EDIT.PREFERENCES', data : { gender : gender } } )
        promptObj.buttons.push( { buttonText : 'Start Shopping', triggerIntent : 'INTRODUCTION', entityType : 'PERSONALIZATION.WELCOME.BUTTON', data : {  } } )
        promptObj.buttons.push( { buttonText : 'Help', triggerIntent : 'HELP', entityType : 'PERSONALIZATION.FILL.OUT.PROFILE.HELP', data : { } } )
        StateService.addPrompt2( session, promptObj )
        let buttonsText = _.map( promptObj.buttons, button => button.buttonText )

        BotService.sendQuickReplyButtons( session, 'You can also...', buttonsText )
      }

    })
    .catch( err => {

        let promptObj = { id : null, buttons : [] }

        // if (StateService2.checkResumeShopping(session)) {
        //     promptObj.buttons.push({
        //         buttonText: 'Back To Shopping',
        //         triggerIntent: 'RESULTS.DIALOG',
        //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
        //         data: {}
        //     })
        // }
        StateService.addPrompt2(session, promptObj)
        //console.log("postback replied")

        let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
        buttonsText.push(Constants.Labels.Introduction)
        buttonsText.push(Constants.Labels.Help)
        BotService.sendQuickReplyButtons( session, { text : 'Api failure message'}, buttonsText )
      session.error( err )
    })

    
  },
  function ( session, result ) {
    session.reset()
  }]
};