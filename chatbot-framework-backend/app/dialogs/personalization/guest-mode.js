var _            = require( 'lodash'                      )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../../constants'             )
var StateService = require( '../../services/StateService' )
var TextService  = require( '../../services/TextService'  )
var UserService  = require( '../../services/UserService'  )
var BotService   = require( '../../services/BotService'   )

module.exports = {
  Label: Constants.Labels.Introduction,
  Dialog: [   
  function ( session, args, next ) {

    if ( !args ) { next(); return; }
    session.sendTyping()
      UserService.setActiveState( session.message.address, true)

    let entityType = args.entities[0].entityType
    let guestmode  = String( args.entities[0].data.guestmode ) === 'true'
    // console.log(  'Guestmode: ', guestmode,  guestmode === true, guestmode === false )

    // Always display message if shopping for someone else,
    // Only display message in case of shopping for self if changing the guestmode
    if ( guestmode || guestmode !== session.userData.guestMode ) {
      BotService.sendText( session, guestmode ? TextService.text( 'User Mode Message' )[0] : TextService.text( 'User Mode Message' )[1] )
    }

    session.userData.guestMode = guestmode
    session.replaceDialog('xpresso-bot:introduction')

  },
  function ( session, result ) {
    session.reset()
  }]
};