var _            = require( 'lodash'                      )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../../constants'             )
var StateService = require( '../../services/StateService' )
var StateService2 = require( '../../services/StateService2' )
var TextService  = require( '../../services/TextService'  )
var UserService  = require( '../../services/UserService'  )
var ApiService   = require( '../../services/ApiService'   )
var UtilService  = require( '../../services/UtilService'  )
var BotService   = require( '../../services/BotService'   )

module.exports = {
  Label: Constants.Labels.Introduction,
  Dialog: [   
  function ( session, args, next ) {

    if ( ! args ) { next(); return; }
    session.sendTyping()

    let entityType = args.entities[0].entityType
    let gender     = args.entities[0].data.gender
      UserService.setActiveState( session.message.address, true)

    let allQuestions = TextService.getAllPersonalizationQuestion()
    // TODO: Move in a service
    allQuestions = _.filter( allQuestions, ( arr, key ) => arr[0] === gender )
    let allEntites = _.map( allQuestions, arr => arr[1] )

    let promptObj = { id : null, buttons : [] }
    _.each( allEntites, entity => {
      
      // TODO: Move in a service
      let relevantRow = _.find( allQuestions, item => item[1] === entity )
      let attribute   = relevantRow[2]
      let question    = relevantRow[3]

      promptObj.buttons.push({
        buttonText    : _.startCase( entity ),
        triggerIntent : 'PERSONALIZATION.SHOW.ATTRIBUTES.LIST',
        entityType    : 'PERSONALIZATION.EDIT.PREFERENCES.ENTITY',
        //data          : { unfilledEntity : entity, gender : gender, attribute : allQuestions[ entity ][0], question : allQuestions[ entity ][1], POST_BACK_PREFIX : 'PERSONALIZATION_EDIT_PREFERENCES', REPLACE_DIALOG : 'personalization-edit-preferences' }
        data          : { unfilledEntity : entity, gender : gender, attribute : attribute, question : question, POST_BACK_PREFIX : 'PERSONALIZATION_EDIT_PREFERENCES', REPLACE_DIALOG : 'personalization-edit-preferences' }
      })
    })

    promptObj.buttons.push( { buttonText : 'Start Shopping', triggerIntent : 'INTRODUCTION', entityType : 'PERSONALIZATION.WELCOME.BUTTON', data : {  } } )

      if (StateService2.checkResumeShopping(session)) {
          promptObj.buttons.push({
              buttonText: 'Back To Shopping',
              triggerIntent: 'RESULTS.DIALOG',
              entityType: 'WISHLIST.BACK.TO.SHOPPING',
              data: {}
          })
      }

    StateService.addPrompt2( session, promptObj )
    let buttonsText = _.map( promptObj.buttons, button => button.buttonText )

    BotService.sendQuickReplyButtons( session, 'Personalization Edit Preferences', buttonsText )    
    // builder.Prompts.choice( session, TextService.text( 'Personalization Edit Preferences' ), buttonsText, {
    //   maxRetries: 0,
    //   listStyle: builder.ListStyle.button
    // })
    
  },
  function ( session, result ) {
    session.reset()
  }]
}
