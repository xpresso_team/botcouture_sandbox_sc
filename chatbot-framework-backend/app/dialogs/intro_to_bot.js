/**
 * Created by aashish_amber on 18/3/17.
 */
var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../constants')
var StateService = require('../services/StateService')
var TextService = require('../services/TextService')
var BotService = require('../services/BotService')
var UserService = require('../services/UserService')

module.exports = {
    Label: Constants.Labels.INTRO_TO_BOT,
    Dialog: [
        function (session) {

            session.sendTyping()

            // Reset state at start
            //StateService.resetState( session )
            //StateService.resetPrompt2( session )
            UserService.setActiveState(session.message.address, true)

            // UserService.checkUserExist(session.message.address)
            //     .then(userObj => {

                    let userName = session.message.user && session.message.user.name ? session.message.user.name : 'User'
                    let channelName = session.message.source ? 'on ' + _.startCase(_.toLower(session.message.source)) : ''
                    let firstName = userName.split(' ')[0]

                    let NODE2 = Constants.NODE.NLP
                    session.userData.currentNode = NODE2
                    session.userData[NODE2] = {}
                    session.userData[NODE2].nlp_reset_flag = true

                    BotService.sendText(session, {text: 'Get Started Message', data: {user_first_name: firstName}})
                    // session.send( TextService.text( 'Get Started Message', { user_first_name : firstName } ) )
                    let cards = []
                    let card = new builder.HeroCard(session)
                        .title(TextService.text('Ready confirmation title')[0])
                        .buttons([
                            builder.CardAction.postBack(session, "Ready", TextService.text('Ready confirmation message')[0]),
                        ])
                    cards.push(card)
                    BotService.sendCard(session, cards)
                    // var reply = new builder.Message( session ).attachmentLayout( builder.AttachmentLayout.carousel ).attachments( cards );
                    // session.send( reply )
                    session.endDialog()
                // })
                // .catch(err => {
                //
                //     let promptObj = {id: null, buttons: []}
                //
                //     // if (StateService2.checkResumeShopping(session)) {
                //     //     promptObj.buttons.push({
                //     //         buttonText: 'Back To Shopping',
                //     //         triggerIntent: 'RESULTS.DIALOG',
                //     //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
                //     //         data: {}
                //     //     })
                //     // }
                //
                //     StateService.addPrompt2(session, promptObj)
                //
                //     let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                //     let button_arr = [Constants.Labels.Introduction, Constants.Labels.Help]
                //     for (let i = 0; i < button_arr.length; i++) {
                //         buttonsText.push(button_arr[i])
                //     }
                //     BotService.sendQuickReplyButtons(session, {text: 'Api failure message'}, buttonsText)
                //     session.error(err)
                //     session.endDialog()
                // })
        }]
};
