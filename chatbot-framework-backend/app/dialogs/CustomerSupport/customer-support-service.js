const _ = require('lodash')
const nconf = require('nconf')
var Constants = require('../../constants')
var request = require("request")
var winston = require('../../services/loggly/LoggerUtil')
var BotService = require('../../services/BotService')
var StateService2 = require('../../services/StateService2')
var StateService = require('../../services/StateService')


var getsupport = function (query,current_id,prev_id,multi_term ,session, callback) {
    var startTime = process.hrtime();
    var url = nconf.get('Customer_support_url')+"parse?"+ "token=" + nconf.get('Customer_support_token') + "&text=" + encodeURIComponent(query)+"&current_id="+current_id+"&prev_id="+prev_id+"&multi_term="+multi_term+"&client="+nconf.get('Customer_support_client')
    winston.log('info', " Customer support query api call is " + url);
    request({
        url: url,
    }, function (error, response, body) {
        if (!error) {
            try {
                if (response.statusCode === 200) {
                    var elapsed = process.hrtime(startTime)[1] / 1000000;
                    winston.log('warn', "Time taken to find  support query api answer is\t" + (process.hrtime(startTime)[0] + elapsed.toFixed(3) / 1000) + " s");
                    var output = JSON.parse(response.body)
                    console.log(JSON.stringify(output))
                    callback(output)
                } else {

                    let promptObj = {id: null, buttons: []}
                    if (StateService2.checkResumeShopping(session)) {
                        promptObj.buttons.push({
                            buttonText: 'Back To Shopping',
                            triggerIntent: 'RESULTS.DIALOG',
                            entityType: 'WISHLIST.BACK.TO.SHOPPING',
                            data: {}
                        })
                    }
                    StateService.addPrompt2(session, promptObj)
                    //console.log("postback replied")

                    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                    buttonsText.push(Constants.Labels.Introduction)
                    buttonsText.push(Constants.Labels.Help)
                    BotService.sendQuickReplyButtons(session, {text: 'Api failure message'}, buttonsText)
                    session.endDialog()
                    // BotService.sendText(session, {text: 'Api failure message'})
                    winston.log('info', "Error Occurred while find  support query api answer \t" + response.statusCode);
                }
            } catch (err) {

                let promptObj = {id: null, buttons: []}
                if (StateService2.checkResumeShopping(session)) {
                    promptObj.buttons.push({
                        buttonText: 'Back To Shopping',
                        triggerIntent: 'RESULTS.DIALOG',
                        entityType: 'WISHLIST.BACK.TO.SHOPPING',
                        data: {}
                    })
                }
                StateService.addPrompt2(session, promptObj)
                //console.log("postback replied")

                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                buttonsText.push(Constants.Labels.Introduction)
                buttonsText.push(Constants.Labels.Help)
                BotService.sendQuickReplyButtons(session, {text: 'Api failure message'}, buttonsText)

                //BotService.sendText(session, {text: 'Api failure message'})
                winston.log('error', "Error Occurred while find  support query api answer :\t");
                winston.log('error', err)
                session.endDialog()

            }
        } else {

            console.log("Customer support api error happened",error)

            let promptObj = {id: null, buttons: []}
            if (StateService2.checkResumeShopping(session)) {
                promptObj.buttons.push({
                    buttonText: 'Back To Shopping',
                    triggerIntent: 'RESULTS.DIALOG',
                    entityType: 'WISHLIST.BACK.TO.SHOPPING',
                    data: {}
                })
            }
            StateService.addPrompt2(session, promptObj)
            //console.log("postback replied")

            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
            buttonsText.push(Constants.Labels.Introduction)
            buttonsText.push(Constants.Labels.Help)
            BotService.sendQuickReplyButtons(session, {text: 'Api failure message'}, buttonsText)

            //BotService.sendText(session, {text: 'Api failure message'})
            winston.log('error', "Error Occurred while find  support query api answer :\t");
            winston.log('error', error)
            session.endDialog()

        }

    });
}

module.exports = {
    getsupport: getsupport
}