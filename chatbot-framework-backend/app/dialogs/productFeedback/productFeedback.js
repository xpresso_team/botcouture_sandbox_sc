/**
 * Created by aashish_amber on 15/3/17.
 */
var _ = require('lodash')
var builder = require('botbuilder')
const nconf = require('nconf')
var Constants = require('../../constants')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var UserService = require('../../services/UserService')
var TextService = require('../../services/TextService')
var BotService = require('../../services/BotService')


module.exports = {
    Label: Constants.Labels.Feedback,
    Dialog: [
        function (session, result, next) {

            session.sendTyping()
            builder.Prompts.text(session, TextService.text('Feedback_ask_text'))
            session.userData.Feedback.emotion = 'Feedback'


        },
        function (session, result, next) {

            session.sendTyping()

            session.userData.Feedback ={}
            if (result && result.resumed === builder.ResumeReason.completed) {


                let promptObj = {id: null, buttons: []}
                if (StateService2.checkResumeShopping(session)) {
                    promptObj.buttons.push({
                        buttonText: 'Back To Shopping',
                        triggerIntent: 'RESULTS.DIALOG',
                        entityType: 'WISHLIST.BACK.TO.SHOPPING',
                        data: {}
                    })
                }
                StateService.addPrompt2(session, promptObj)
                console.log("postback replied")

                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                buttonsText.push(Constants.Labels.Introduction)
                buttonsText.push(Constants.Labels.Help)
                BotService.sendQuickReplyButtons(session, {text: 'Dislike_final_response'}, buttonsText)
                session.userData.Feedback.text = session.message.text
                UserService.sendFeedback(session)
                session.endDialog()
            } else {
                session.endDialog()

            }

        }
    ]
};