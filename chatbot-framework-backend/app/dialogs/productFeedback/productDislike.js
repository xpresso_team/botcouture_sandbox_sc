var _            = require( 'lodash'                   )
var builder      = require( 'botbuilder'               )
var Constants    = require( '../../constants'             )
var StateService = require( '../../services/StateService' )
var StateService2 = require( '../../services/StateService2' )
var UserService  = require( '../../services/UserService'  )
var TextService  = require( '../../services/TextService'  )
var BotService   = require( '../../services/BotService'   )

module.exports = {
    Label: Constants.Labels.Goodbye,
    Dialog: [
        function ( session,args ) {

            if ( ! args ) { session.endDialog(); return; }

            session.userData.Feedback.emotion = "Negative"
            let promptObj = {id: null, buttons: []}

            session.userData.Feedback ={}

            if(args.entities[0].entityType=="dislike"){


                session.userData.Feedback.text = args.entities[0].data.emotion

                session.sendTyping()

                UserService.sendFeedback(session)

                if (StateService2.checkResumeShopping(session)) {
                    promptObj.buttons.push({
                        buttonText: 'Back To Shopping',
                        triggerIntent: 'RESULTS.DIALOG',
                        entityType: 'WISHLIST.BACK.TO.SHOPPING',
                        data: {}
                    })
                }
                StateService.addPrompt2(session, promptObj)
                console.log("postback replied")

                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                buttonsText.push(Constants.Labels.Introduction)
                buttonsText.push(Constants.Labels.Help)
                BotService.sendQuickReplyButtons(session, {text: 'Dislike_final_response'}, buttonsText)
                session.endDialog()


            }else{

                let Dislike_reasons = TextService.text('Dislike reasons')


                for(let i=0;i<Dislike_reasons.length;i++){

                    let triggerIntent = 'PRODUCT-DISLIKE'

                    if(i ==3){

                        triggerIntent = 'PRODUCT-FEEDBACK'

                    }

                    promptObj.buttons.push({
                        buttonText: Dislike_reasons[i],
                        triggerIntent: triggerIntent,
                        entityType: 'dislike',
                        data: {emotion: Dislike_reasons[i]}
                    })

                }
                StateService.addPrompt2(session, promptObj)
                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)

                BotService.sendQuickReplyButtons(session, {text: 'DISLIKE_first_RESPONSE'}, buttonsText)
                session.endDialog()
            }



        }]

};