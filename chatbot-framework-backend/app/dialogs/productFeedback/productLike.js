var _            = require( 'lodash'                   )
var builder      = require( 'botbuilder'               )
var Constants    = require( '../../constants'             )
var StateService = require( '../../services/StateService' )
var StateService2 = require( '../../services/StateService2' )
var UserService  = require( '../../services/UserService'  )
var TextService  = require( '../../services/TextService'  )
var BotService   = require( '../../services/BotService'   )

module.exports = {
    Label: Constants.Labels.Goodbye,
    Dialog: [
        function ( session,args ) {

            if ( ! args ) { session.endDialog(); return; }

            session.sendTyping()

            session.userData.Feedback ={}

            session.userData.Feedback.emotion = "Positive"

            session.userData.Feedback.text = args.entities[0].data.emotion

            UserService.sendFeedback(session)

            let promptObj = {id: null, buttons: []}
            if (StateService2.checkResumeShopping(session)) {
                promptObj.buttons.push({
                    buttonText: 'Back To Shopping',
                    triggerIntent: 'RESULTS.DIALOG',
                    entityType: 'WISHLIST.BACK.TO.SHOPPING',
                    data: {}
                })
            }
            StateService.addPrompt2(session, promptObj)
            console.log("postback replied")

            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
            buttonsText.push(Constants.Labels.Introduction)
            buttonsText.push(Constants.Labels.Help)
            BotService.sendQuickReplyButtons(session, {text: 'LIKE_RESPONSE'}, buttonsText)
            session.endDialog()

        }]

};