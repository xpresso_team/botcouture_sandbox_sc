var _          = require( 'lodash'                 )
var builder    = require( 'botbuilder'             )
var Constants  = require( '../constants'           )
var ApiService = require( '../services/ApiService' )
var UtilService = require( '../services/UtilService' )
var TextService = require( '../services/TextService' )
var StateService = require( '../services/StateService' )
var StateService2 = require( '../services/StateService2' )
var BotService   = require( '../services/BotService'   )

module.exports = {
  Label: Constants.Labels.ViewSimilarItems,
  Dialog: [
    function ( session, args, next ) {

      if ( !args ) { next(); return; }

      session.sendTyping()

      let xc_sku   = args.entities[0].data.xc_sku
      let pageNo   = args.entities[0].data.pageNo || 0
      let pageSize = 10 + 2

      
      ApiService.getSimilarItems( xc_sku, session )
      .then( result => {

        if ( result.results ) {

            console.log("Results is ",result.results)

            return UtilService.createAndSendImageResult( session, result.results ).then( cards => {

                let temp_node = session.userData.currentNode
                let state = session.userData[temp_node]

                console.log("Node is ",temp_node)
                console.log("state is ",state)

                console.log("cards is ",cards)

                let pagedResults = UtilService.pageList(cards, pageNo, pageSize, null, null)

                console.log("pagedResults is ",pagedResults)

                if (pagedResults.length > 0) {

                    BotService.sendText(session,TextService.text('View Similar Product Msg'))

                    if (session.message.source === "facebook") {

                        BotService.sendCard(session, pagedResults,true,"generic")

                    } else {
                        BotService.sendCard(session, pagedResults)
                    }


                    let onLastPage = ( pageSize - 2 ) * ( pageNo + 1 ) >= cards.length
                    let promptObj = {id: null, buttons: []}
                    if (!onLastPage) {
                        promptObj.buttons.push({
                            buttonText: 'Show More',
                            triggerIntent: 'VIEW.SIMILAR.ITEMS',
                            entityType: 'VIEW.SIMILAR.ITEMS',
                            data: {xc_sku: xc_sku, pageNo: pageNo + 1}
                        })
                    }

                    promptObj.buttons.push({
                        buttonText: 'Back To Results',
                        triggerIntent: 'RESULTS.DIALOG',
                        entityType: 'WISHLIST.THUMBS.DOWN.BUTTON',
                        data: {}
                    })

                    StateService.addPrompt2(session, promptObj)
                    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                    buttonsText = buttonsText.concat(['Back To Search', Constants.Labels.Introduction, Constants.Labels.Help])

                    BotService.sendQuickReplyButtons(session, 'Image similar result option message', buttonsText)
                    session.endDialog()
                }
                else {
                    // TODO: Handel all the cases
                    // session.send('No similar result found. { ' + result.status_code + ' | ' + result.status_msg + ' }')

                    BotService.sendText( session, 'No similar products found!' )
                    BotService.sendQuickReplyButtons( session, 'What would you like to do?', [ Constants.Labels.Introduction, Constants.Labels.Help ] )

                    // session.send( 'No similar products found!' )
                    // builder.Prompts.choice( session, 'What would you like to do?', [ Constants.Labels.Introduction, Constants.Labels.Help ], {
                    //   maxRetries: 0,
                    //   listStyle: builder.ListStyle.button
                    // })
                    session.endDialog()
                }
                session.endDialog()
            })
            session.endDialog()

        } else {
          // TODO: Handel all the cases
          // session.send('No similar result found. { ' + result.status_code + ' | ' + result.status_msg + ' }')

          BotService.sendText( session, 'No similar products found!' )
          BotService.sendQuickReplyButtons( session, 'What would you like to do?', [ Constants.Labels.Introduction, Constants.Labels.Help ] )

          // session.send( 'No similar products found!' )
          // builder.Prompts.choice( session, 'What would you like to do?', [ Constants.Labels.Introduction, Constants.Labels.Help ], {
          //   maxRetries: 0,
          //   listStyle: builder.ListStyle.button
          // })
            session.endDialog()
        }
          session.endDialog()

      })
      .catch( err => {

          let promptObj = { id : null, buttons : [] }
          if (StateService2.checkResumeShopping(session)) {
              promptObj.buttons.push({
                  buttonText: 'Back To Shopping',
                  triggerIntent: 'RESULTS.DIALOG',
                  entityType: 'WISHLIST.BACK.TO.SHOPPING',
                  data: {}
              })
          }
          StateService.addPrompt2(session, promptObj)
          //console.log("postback replied")

          let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
          buttonsText.push(Constants.Labels.Introduction)
          buttonsText.push(Constants.Labels.Help)
          BotService.sendQuickReplyButtons( session, { text : 'Api failure message' }, buttonsText )
          session.endDialog()
        session.error( err )
      })

        session.endDialog()
    }
  ]
};