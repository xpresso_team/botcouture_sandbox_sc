var _              = require( 'lodash'                       )
var builder        = require( 'botbuilder'                   )
var Constants      = require( '../../constants'              )
var ApiService     = require( '../../services/ApiService'    )
//var StateService = require( '../../services/StateService'  )
var StateService2  = require( '../../services/StateService2' )
var UtilService    = require( '../../services/UtilService'   )
var TextService    = require( '../../services/TextService'   )
var GuideMeService = require( './guide-me-service'           )
var UserService  = require( '../../services/UserService'  )
var ResultsService = require( '../results/results-service'  )


module.exports = {
  Label: Constants.Labels.GuideMe,
  Dialog: [
  function ( session, args, next ) {

    // Fall through
    if ( !args ) { next(); return; }
    session.sendTyping()
    

    let NODE = Constants.NODE.GUIDEME
      //session.userData[Constants.NODE.NLP].nlp_reset_flag=1
      ResultsService.setVariableByState( session, Constants.NODE.NLP, 'nlp_reset_flag', 1 )
      UserService.setActiveState( session.message.address, true);
      session.userData.currentNode = NODE;

    console.log("\ncllient name is ",args.entities[0].data.client_name)
    args.entities[0].data = StateService2.sanitizeIntentResults( args.entities[0].data )

    console.log("\n client_name inside guide-me-results is ",args.entities[0].data.client_name)

      //console.log("Guide me results ",JSON.stringify(args.entities[0].data))
    session.userData[ NODE ] = GuideMeService.getDefaultState( NODE )

      //console.log("Guide me results default sate ",JSON.stringify(session.userData[ NODE ]))
    _.merge( session.userData[ NODE ], args.entities[0].data )

      //console.log("Guide me results new sate ",JSON.stringify(session.userData[ NODE ]))

    session.userData.currentNode = NODE
    session.replaceDialog( 'xpresso-bot:results-dialog' )

  }]

}