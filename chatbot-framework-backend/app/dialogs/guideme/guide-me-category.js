var _            = require( 'lodash'                      )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../../constants'             )
var ApiService   = require( '../../services/ApiService'   )
var StateService = require( '../../services/StateService' )
var StateService2 = require( '../../services/StateService2' )
var UtilService  = require( '../../services/UtilService'  )
var TextService  = require( '../../services/TextService'  )
var BotService   = require( '../../services/BotService'   )
var ResultsService = require( '../results/results-service'  )
var toTitleCase  = require( 'to-title-case'               )
var UserService  = require( '../../services/UserService'  )

module.exports = {
  Label: Constants.Labels.GuideMe,
  Dialog: [
  function ( session, args, next ) {


    // Fall through
    if ( !args ) { next(); return; }
    session.sendTyping()
    //session.userData[Constants.NODE.NLP].nlp_reset_flag=1 
    ResultsService.setVariableByState( session, Constants.NODE.NLP, 'nlp_reset_flag', 1 ) 
    UserService.setActiveState( session.message.address, true);
    let NODE = Constants.NODE.GUIDEME;
    session.userData.currentNode = NODE;



    let entityType = args.entities[0].entityType
    let gender     = args.entities[0].data.gender
    let pageNo     = Number( args.entities[0].data.pageNo ) || 0
    let client_name= args.entities[0].data.client_name
    console.log("\nclient_name\n")  
      let guest_mode = args.entities[0].data.guest_mode
      if(guest_mode === false){
          session.userData.guestMode = guest_mode
      }else if(guest_mode === true){
          session.userData.guestMode = guest_mode
      }

    ApiService.getGuideMeCategories( gender,client_name, session )
    .then( categoryObj => {
      
      let categoryList = categoryObj.Results.xc_category || []
      
      if ( categoryList.length > 0 ) {

        categoryList = categoryList.map( category => {
          return { text : toTitleCase( category ) , postback : 'GUIDEME::' + gender + '::' + client_name+ '::'+ category }
        })

        let pageSize = 30                                // Actual page size will be 2 less (left for Prev/Next buttons)      
        pageSize     = pageSize < 30 ? pageSize : 30     // We can show 3 * 10 buttons on FB in total

        // -2 is the actual space available for items, +1 for zero based pageNos
        let showNext = ( pageSize - 2 ) * ( pageNo + 1) < categoryList.length ? { text : 'Next Page' , postback : 'GUIDEME::' + gender +'::'+'CLIENT_NAME::'+client_name+'::NEXTPAGE::'+ ( pageNo + 1 )  } : null
        let showPrevious = null
        let categoryListWindow = UtilService.pageList( categoryList, pageNo, pageSize, showPrevious, showNext )

        let cards = []
        for( let i = 0; i < categoryListWindow.length; i = i + 3 ) {
          let buttons = []
          if ( i     < categoryListWindow.length ) buttons.push( builder.CardAction.postBack( session, categoryListWindow[ i     ].postback, categoryListWindow[ i     ].text ) )      // I know
          if ( i + 1 < categoryListWindow.length ) buttons.push( builder.CardAction.postBack( session, categoryListWindow[ i + 1 ].postback, categoryListWindow[ i + 1 ].text ) )
          if ( i + 2 < categoryListWindow.length ) buttons.push( builder.CardAction.postBack( session, categoryListWindow[ i + 2 ].postback, categoryListWindow[ i + 2 ].text ) )
          let card = new builder.HeroCard( session ).title( _.startCase( gender ) ).buttons( buttons )
          cards.push( card )
        }
        
        if ( cards.length > 0 ) {

          BotService.sendText( session, { text : 'Product Category display msg', data : { gender : toTitleCase( gender ) } } )
          BotService.sendCard( session, cards )
        }

        session.endDialog()

      } else {
        // Backend returned nothing
        BotService.sendText( session,TextService.text('No categories found Msg'))
          let promptObj = { id : null, buttons : [] }
          if (StateService2.checkResumeShopping(session)) {
              promptObj.buttons.push({
                  buttonText: 'Back To Shopping',
                  triggerIntent: 'RESULTS.DIALOG',
                  entityType: 'WISHLIST.BACK.TO.SHOPPING',
                  data: {}
              })
          }
          StateService.addPrompt2(session, promptObj)

          let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
          let button_arr = [ Constants.Labels.Introduction, Constants.Labels.Help ]
          for(let i = 0;i<button_arr.length;i++){
              buttonsText.push(button_arr[i])
          }
          BotService.sendQuickReplyButtons( session, { text : 'Take a tour Message'}, buttonsText )
        session.endDialog()
      }

    })
    .catch( err => {
      if ( err.status ) {
        // Backend send a non-200 error
          let promptObj = { id : null, buttons : [] }

          // if (StateService2.checkResumeShopping(session)) {
          //     promptObj.buttons.push({
          //         buttonText: 'Back To Shopping',
          //         triggerIntent: 'RESULTS.DIALOG',
          //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
          //         data: {}
          //     })
          // }
          StateService.addPrompt2(session, promptObj)

          let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
          let button_arr = [ Constants.Labels.Introduction, Constants.Labels.Help ]
          for(let i = 0;i<button_arr.length;i++){
              buttonsText.push(button_arr[i])
          }
          BotService.sendQuickReplyButtons( session, { text : 'Error Msg'}, buttonsText )

        // builder.Prompts.choice( session, TextService.text( 'Something went wrong! Please try again.' ), [ Constants.Labels.Introduction, Constants.Labels.Help ], {
        //   maxRetries: 0,
        //   listStyle: builder.ListStyle.button
        // })
      } else {
        // Backend not reponding
          let promptObj = { id : null, buttons : [] }
          // if (StateService2.checkResumeShopping(session)) {
          //     promptObj.buttons.push({
          //         buttonText: 'Back To Shopping',
          //         triggerIntent: 'RESULTS.DIALOG',
          //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
          //         data: {}
          //     })
          // }
          StateService.addPrompt2(session, promptObj)

          let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
          let button_arr = [ Constants.Labels.Introduction, Constants.Labels.Help ]
          for(let i = 0;i<button_arr.length;i++){
              buttonsText.push(button_arr[i])
          }
          BotService.sendQuickReplyButtons( session, { text : 'Api failure message'}, buttonsText )
      }
      session.error( err )
      session.endDialog()
    })

    // session.endDialog()

  }]
}