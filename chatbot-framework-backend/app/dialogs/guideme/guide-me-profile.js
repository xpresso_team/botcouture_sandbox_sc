var _              = require( 'lodash'                      )
var builder        = require( 'botbuilder'                  )
var Constants      = require( '../../constants'             )
var ApiService     = require( '../../services/ApiService'   )
var StateService   = require( '../../services/StateService' )
var StateService2   = require( '../../services/StateService2' )
var GuideMeService = require( './guide-me-service'           )
var TextService    = require( '../../services/TextService'  )
var UserService    = require( '../../services/UserService'  )
var BotService     = require( '../../services/BotService'   )
var ResultsService = require( '../results/results-service'  )
var nconf          = require( 'nconf'                       )
const uuidV4 = require('uuid/v4')

module.exports = {
    Label: Constants.Labels.GuideMe,
    Dialog: [
        function ( session, args, next ) {

            session.sendTyping()
            //session.userData[Constants.NODE.NLP].nlp_reset_flag=1
            ResultsService.setVariableByState( session, Constants.NODE.NLP, 'nlp_reset_flag', 1 )

            UserService.setActiveState( session.message.address, true)

            let NODE = Constants.NODE.GUIDEME;
            session.userData[ NODE ] = GuideMeService.getDefaultState( NODE )
            session.userData.currentNode = NODE;
            if(typeof session.userData[NODE].client_name=='undefined') {
                session.userData[NODE].client_name=[]
            }


            let url = nconf.get('WebView')["Url"] + nconf.get('WebView')["Profile"] + "?psid=" + session.message.address.user.id + "&botid=" + session.message.address.bot.id + '&x=' + uuidV4()

            let msg

            if((args)&&(args.entities)&&(args.entities.length>0)&&(args.entities[0].data)&&(args.entities[0].data.GuideMeMsg)&&(args.entities[0].data.GuideMeMsg=="new_user")){
                msg = TextService.text( 'New user Profile creation msg')

            }else{
                msg = TextService.text( 'Profile Guide me message')
            }


            // Get user's gender
                UserService.getUser(session.message.address)
                    .then(userObj => {

                        let gender = userObj.gender
                        session.userData.gender = gender
                        if(gender && (args)&&(args.entities)&&(args.entities.length>0)&&(args.entities[0].entityType)&& (args.entities[0].entityType ==='USE MY PROFILE')){

                            let intent = {
                                intent: 'GENDER.UPDATE',
                                score: 1,
                                entities: [{
                                    entity: 'SELECT-RETAILER',
                                    entityType: 'GUIDE.ME',
                                    data: {gender: userObj.gender,pageNo: 0,guest_mode:false,dialog :'select-retailer'}
                                }]
                            }
                            //session.replaceDialog('xpresso-bot:guide-me-category', intent)
                            session.replaceDialog('xpresso-bot:gender-update', intent)


                        }
                        else if(userObj.gender && !session.userData.guestMode) {

                            let intent = {
                                intent: 'SELECT-RETAILER',
                                score: 1,
                                entities: [{
                                    entity: userObj.gender,
                                    entityType: 'GUIDE.ME',
                                    data: {gender: userObj.gender, pageNo: 0,guest_mode:false}
                                }]
                            }
                            session.replaceDialog('xpresso-bot:select-retailer', intent)

                        } else {

                            let postBackButtons = []
                            let urlButtons = []
                            if (gender) {
                                if (session.message.source === "facebook") {

                                    let intent = {
                                        intent: 'GENDER.UPDATE',
                                        entities: [{
                                            entity: 'SELECT-RETAILER',
                                            entityType: 'GUIDE-ME',
                                            data: {GuideMeMsg:null, gender: userObj.gender,guest_mode:false, pageNo: 0,dialog :'select-retailer'}
                                        }]
                                    }

                                    let PostBack = 'POSTBACK::' + JSON.stringify(intent)


                                    postBackButtons.push(
                                        {
                                            "type": "postback",
                                            "title": TextService.text('Profile Option')[1],
                                            "payload": PostBack
                                        }
                                    )

                                    postBackButtons.push(
                                        {
                                            "type": "web_url",
                                            "title": TextService.text('Profile Option')[2],
                                            "url": url,
                                            "messenger_extensions": true,
                                            "webview_height_ratio": nconf.get('WebView_size')["Profile"]
                                        }
                                    )


                                } else {

                                    let intent = {
                                        intent: 'GENDER.UPDATE',
                                        entities: [{
                                            entity: 'SELECT-RETAILER',
                                            entityType: 'GUIDE-ME',
                                            data: {GuideMeMsg:null, gender: userObj.gender,guest_mode:false, pageNo: 0,dialog :'select-retailer'}
                                        }]
                                    }


                                    let PostBack = 'POSTBACK::' + JSON.stringify(intent)

                                    postBackButtons.push({
                                        buttonText: TextService.text('Profile Option')[1],
                                        postback: PostBack
                                    })


                                    urlButtons.push({
                                        buttonUrl: url,
                                        buttonText: TextService.text('Profile Option')[2]
                                    })
                                }


                            } else {

                                if (session.message.source === "facebook") {

                                    postBackButtons.push(
                                        {
                                            "type": "web_url",
                                            "title": TextService.text('Profile Option')[0],
                                            "url": url ,
                                            "messenger_extensions": true,
                                            "webview_height_ratio": nconf.get('WebView_size')["Profile"]
                                        }
                                    )

                                    let intent = {
                                        intent: 'GUIDE-ME',
                                        entities: [{
                                            entity: userObj.gender,
                                            entityType: "USE MY PROFILE",
                                            data: {GuideMeMsg:"new_user", gender: userObj.gender,guest_mode:false, pageNo: 0}
                                        }]
                                    }


                                    let PostBack = 'POSTBACK::' + JSON.stringify(intent)

                                    postBackButtons.push(
                                        {
                                            "type": "postback",
                                            "title": TextService.text('Profile Option')[1],
                                            "payload": PostBack
                                        }
                                    )

                                } else {


                                    urlButtons.push({
                                        buttonUrl: url,
                                        buttonText: TextService.text('Profile Option')[0]
                                    })

                                    let intent = {
                                        intent: 'GUIDE-ME',
                                        entities: [{
                                            entity: userObj.gender,
                                            entityType: "USE MY PROFILE",
                                            data: {GuideMeMsg:"new_user", gender: userObj.gender,guest_mode:false, pageNo: 0}
                                        }]
                                    }

                                    let PostBack = 'POSTBACK::' + JSON.stringify(intent)

                                    postBackButtons.push({
                                        buttonText: TextService.text('Profile Option')[1],
                                        postback: PostBack
                                    })


                                }

                            }
                            // 3rd buttonn is common

                            let intent = {
                                intent: 'GUIDE.ME.GENDER',
                                entities: [{
                                    entity: userObj.gender,
                                    entityType: 'GUIDE-ME',
                                    data: {GuideMeMsg:null, gender: userObj.gender,guest_mode:true, pageNo: 0}
                                }]
                            }

                            let PostBack = 'POSTBACK::' + JSON.stringify(intent)

                            if (session.message.source === "facebook") {




                                postBackButtons.push(
                                    {
                                        "type": "postback",
                                        "title": TextService.text('Profile Option')[3],
                                        "payload": PostBack
                                    }
                                )

                                let payload = {
                                    "template_type": "button",
                                    "text":msg,
                                    "buttons": postBackButtons
                                }

                                BotService.sendCard(session, postBackButtons,true,"button",msg)
                                session.endDialog()


                            } else {



                                postBackButtons.push({
                                    buttonText: TextService.text('Profile Option')[3],
                                    postback: PostBack
                                })

                                //builder.CardAction.openUrl(session, item.product_url || 'http://www.google.com/', 'View on ' + _.startCase(item.client_name)),
                                let cardButtons = _.map(postBackButtons, button => builder.CardAction.postBack(session, button.postback, button.buttonText))
                                cardButtons = cardButtons.concat(_.map(urlButtons, button => builder.CardAction.openUrl(session, button.buttonUrl, button.buttonText)))
                                let card = new builder.HeroCard(session).title(TextService.text('Button Template Title')[0]).buttons(cardButtons)
                                BotService.sendText(session, msg)
                                BotService.sendCard(session, [card])
                                session.endDialog()

                            }


                            session.endDialog()

                        }


                    })
                    .catch(err => {

                        let promptObj = {id: null, buttons: []}
                        // if (StateService2.checkResumeShopping(session)) {
                        //     promptObj.buttons.push({
                        //         buttonText: 'Back To Shopping',
                        //         triggerIntent: 'RESULTS.DIALOG',
                        //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
                        //         data: {}
                        //     })
                        // }
                        StateService.addPrompt2(session, promptObj)

                        let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
                        let button_arr = [ Constants.Labels.Introduction, Constants.Labels.Help ]
                        for(let i = 0;i<button_arr.length;i++){
                            buttonsText.push(button_arr[i])
                        }
                        BotService.sendQuickReplyButtons( session, { text : 'Api failure message' }, buttonsText )
                        session.error(err)
                        session.endDialog()
                    })


        }]

}
