var _              = require( 'lodash'                      )
var builder        = require( 'botbuilder'                  )
var Constants      = require( '../../constants'             )
var ApiService     = require( '../../services/ApiService'   )
var StateService   = require( '../../services/StateService' )
var StateService2   = require( '../../services/StateService2' )
var TextService    = require( '../../services/TextService'  )
var UserService    = require( '../../services/UserService'  )
var BotService     = require( '../../services/BotService'   )
var ResultsService = require( '../results/results-service'  )
var nconf          = require( 'nconf'                       )

module.exports = {
  Label: Constants.Labels.GuideMe,
  Dialog: [   
  function ( session, args, next ) {

      session.sendTyping()
      //session.userData[Constants.NODE.NLP].nlp_reset_flag=1
      ResultsService.setVariableByState( session, Constants.NODE.NLP, 'nlp_reset_flag', 1 )

      UserService.setActiveState( session.message.address, true)

      let NODE = Constants.NODE.GUIDEME;
      session.userData.currentNode = NODE;

      // Begin state
      BotService.sendText(session, 'Guide Me Message')


      let personalisation_enabled = nconf.get('Personalisation')
      if (!personalisation_enabled) {
          let prompt2 = {
              id: null,
              buttons: [
                  {
                      buttonText: 'Men',
                      triggerIntent: 'SELECT-RETAILER',
                      entityType: 'GUIDE.ME.GENDER',
                      data: {gender: 'men', pageNo: 0}
                  },
                  {
                      buttonText: 'Women',
                      triggerIntent: 'SELECT-RETAILER',
                      entityType: 'GUIDE.ME.GENDER',
                      data: {gender: 'women', pageNo: 0}
                  }]
          }
          StateService.addPrompt2(session, prompt2)

          BotService.sendQuickReplyButtons(session, 'Collection Gender Question', ['Men', 'Women'])
          // builder.Prompts.choice( session, TextService.text( 'Collection Gender Question' ), [ 'Men', 'Women' ], {
          //   maxRetries : 0,
          //   listStyle  : builder.ListStyle.button
          // } )

          session.endDialog()


      }else{

      // Get user's gender
      UserService.getUser(session.message.address)
          .then(userObj => {

              session.userData.gender = userObj.gender

              if (userObj.gender && !session.userData.guestMode) {

                  let intent = {
                      intent: 'SELECT-RETAILER',
                      score: 1,
                      entities: [{
                          entity: userObj.gender,
                          entityType: 'GUIDE.ME.GENDER',
                          data: {gender: userObj.gender, pageNo: 0}
                      }]
                  }
                  session.replaceDialog('xpresso-bot:guide-me-category', intent)

              } else {

                  if (session.userData.guestMode) {
                      BotService.sendText(session, 'Guest Mode Reminder')
                  }

                  let prompt2 = {
                      id: null,
                      buttons: [
                          {
                              buttonText: 'Men',
                              triggerIntent: 'SELECT-RETAILER',
                              entityType: 'GUIDE.ME.GENDER',
                              data: {gender: 'men', pageNo: 0}
                          },
                          {
                              buttonText: 'Women',
                              triggerIntent: 'SELECT-RETAILER',
                              entityType: 'GUIDE.ME.GENDER',
                              data: {gender: 'women', pageNo: 0}
                          }]
                  }
                  StateService.addPrompt2(session, prompt2)

                  BotService.sendQuickReplyButtons(session, 'Collection Gender Question', ['Men', 'Women'])
                  // builder.Prompts.choice( session, TextService.text( 'Collection Gender Question' ), [ 'Men', 'Women' ], {
                  //   maxRetries : 0,
                  //   listStyle  : builder.ListStyle.button
                  // } )

                  session.endDialog()

              }

          })
          .catch(err => {

              let promptObj = { id : null, buttons : [] }
              // if (StateService2.checkResumeShopping(session)) {
              //     promptObj.buttons.push({
              //         buttonText: 'Back To Shopping',
              //         triggerIntent: 'RESULTS.DIALOG',
              //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
              //         data: {}
              //     })
              // }
              StateService.addPrompt2(session, promptObj)

              let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
              let button_arr = [ Constants.Labels.Introduction, Constants.Labels.Help ]
              for(let i = 0;i<button_arr.length;i++){
                  buttonsText.push(button_arr[i])
              }
              BotService.sendQuickReplyButtons( session, { text : 'Api failure message' }, buttonsText )
              session.error(err)
              session.endDialog()
          })
  }
    
  }]

}