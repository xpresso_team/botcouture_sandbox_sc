/**
 * Created by aashish_amber on 9/3/17.
 */
var _            = require( 'lodash'                   )
var builder      = require( 'botbuilder'               )
var Constants    = require( '../constants'             )
var ApiService   = require( '../services/ApiService'   )
var TextService  = require( '../services/TextService'  )
var UtilService  = require( '../services/UtilService'  )
var StateService = require( '../services/StateService' )
var UserService  = require( '../services/UserService'  )
var KafkaService = require( '../../kafka-service'      )
var nconf       = require( 'nconf'                   )
var BotService   = require( '../services/BotService'   )

module.exports = {
    Label: Constants.Labels.ViewItOn,
    Dialog: [
        function ( session, args, next ) {
        try{

            session.sendTyping()

            // Fall through
            if ( !args || !args.entities || !( args.entities.length > 0 ) ) { next(); return; }

            var flow   = args.entities[0].entityType
            var url    = args.entities[0].data.url
            var client = args.entities[0].data.client
            let xc_sku = args.entities[0].data.xc_sku
            let cards  = []
            console.log('INSIDE MORE', args.entities[0].data )
            console.log(session.message.address)
            console.log(session.message)

            url = "http://"+nconf.get('Website_redirect_url')+"ChannelId="+session.message.address.channelId+"&PageId="+session.message.address.bot.id+"&PId="+session.message.address.user.id+"&Xc_sku="+xc_sku+"&url="+encodeURIComponent(url)
            switch (flow) {
            case "WARN":

                //session.send(TextService.text( 'Website redirection warning Message')[0].replace("{{client}}", client))
                session.userData.analytics.in.click_item_sku = xc_sku
                
                BotService.sendText( session, { text : 'New_Website_redirection_warning_Message', data : { client : client } } )

                console.log("the url is "+ url)

                let card = new builder.HeroCard(session)
                    .title(TextService.text( 'New_Redirection_option')[0])
                    //.text(TextService.text( 'Website redirection warning Message')[0].replace("{{client}}", client))
                    .buttons([
                        //builder.CardAction.openUrl(session, url, TextService.text( 'Continue to website message')[0].replace("{{website}}", _.startCase(client))),
                        //builder.CardAction.openUrl(session, url, _.startCase(client)+".com"),
                        builder.CardAction.openUrl(session, url, TextService.text( 'Product_Details' )[0]),
                        //builder.CardAction.openUrl(session, url, "com"),
                        //builder.CardAction.postBack(session, 'BACK', TextService.text( 'Keep Shopping Message')[0])
                        builder.CardAction.postBack(session, 'BACK', TextService.text( 'Stay_here')[0])

                    ])
                cards.push( card )

                BotService.sendCard( session, cards )

                // var reply = new builder.Message( session ).attachmentLayout( builder.AttachmentLayout.carousel ).attachments( cards );
                // session.send( reply )

                //UserService.viewedProduct( session.message.address, xc_sku )
                session.endDialog()

                break;
            case "BACK":


                    break;
            default:
                break;
        }


                }
                catch( err)  {
                    console.log( 'ERROR dialog: ', err )
                    next( )
                }

        },
        function ( session, result ) {
            session.reset()
        }]

};
