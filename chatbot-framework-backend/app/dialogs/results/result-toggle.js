var _           = require( 'lodash'                     )
var builder     = require( 'botbuilder'                 )
var Constants   = require( '../../constants'            )
var ApiService  = require( '../../services/ApiService'  )
var TextService = require( '../../services/TextService' )
var StateService = require( '../../services/StateService' )
var StateService2 = require( '../../services/StateService2' )
var UserService = require( '../../services/UserService' )
var BotService    = require( '../../services/BotService'    )
var ResultsService = require( '../results/results-service'  )
var nconf        = require( 'nconf')
var toTitleCase   = require( 'to-title-case'                )

module.exports = {

    Label: Constants.Labels.ExploreCollections,

    Dialog: [

        function ( session, args, next ) {

            session.sendTyping()
            UserService.setActiveState( session.message.address, true)
            let guest_mode = session.userData.guestMode
            let NODE = session.userData.currentNode
            var entity = session.userData[session.userData.currentNode].entity
            console.log("entity is " , entity)
            let data
            if(!guest_mode) {


                BotService.sendText(session, {
                    text: 'Switch to guest mode',
                })


                let intent = {
                    intent: 'RESULTS.DIALOG',
                    score: 1,
                    entities: [{
                        entityType: 'RESULT.TOGGLE',
                    }]
                }

                session.userData.guestMode = !session.userData.guestMode
                StateService2.resetAFilter( NODE, session,'brand' )
                StateService2.resetAFilter( NODE, session,'size' )
                session.replaceDialog('xpresso-bot:results-dialog', intent)
            }else {

                BotService.sendText(session, {
                    text: 'Switch to personalised mode',
                })


                data = { resetState : false, state : { } }

                UserService.getUserProfileByEntity(session.message.address, entity,session.userData.Profile_gender).then(userProfile => {

                    console.log(JSON.stringify(userProfile))


                    let profile_gender
                    if (userProfile && userProfile.user){
                        profile_gender = userProfile.user.gender
                        profile_gender === 'men,women' ? "no_preference" : profile_gender
                    }

                    if(profile_gender) {
                        session.userData.Profile_gender = profile_gender
                    }


                    let brandFilter = _.difference( userProfile[ 'brand' ], userProfile.usersFilterNegative[ 'brand' ] )
                    let sizeFilter = _.difference( userProfile[ 'size' ], userProfile.usersFilterNegative[ 'size' ] )

                    //StateService2.resetAFilter( NODE, session, "colorsavailable" )
                    //StateService2.resetAFilter( NODE, session, "price" )
                    if (session.userData.gender) {
                        console.log("updated gender from user session")
                        //data.state.gender = [session.userData.gender]
                    }


                    //data.state.brand = brandFilter
                    //data.state.size = sizeFilter


                    let brand_not_set_flag = true
                    let size_not_set_flag = true

                    let msg_txt = ""


                    if((brandFilter.length>0)||(sizeFilter.length>0))
                    {

                        if(brandFilter.length>0)
                        {
                            msg_txt = msg_txt+ 'Adding Brand preference from your user profile for  ' + session.userData[NODE].entity + " : "+toTitleCase( brandFilter.join( ', ' ) )
                            brand_not_set_flag = false
                        }

                        if(sizeFilter.length>0)
                        {
                            if(msg_txt){
                                msg_txt = msg_txt +"\n"
                            }
                            msg_txt = msg_txt+'Adding Size preference from your user profile for  ' + session.userData[NODE].entity + " : "+ toTitleCase( sizeFilter.join( ', ' ) )
                            size_not_set_flag = false
                        }

                    }

                    if(msg_txt){
                        BotService.sendText(session,msg_txt)
                    }



                    msg_txt = ""

                    if((brand_not_set_flag)||(size_not_set_flag))
                    {

                        if((brand_not_set_flag)&&(size_not_set_flag)){
                            msg_txt = msg_txt + TextService.text('No Preference saved', {
                                    choise: "brand/size",
                                    entity: session.userData[NODE].entity
                                })[0]

                        }else {

                            if (brand_not_set_flag) {
                                msg_txt = msg_txt + TextService.text('No Preference saved', {
                                        choise: "brand",
                                        entity: session.userData[NODE].entity
                                    })[0]
                            }

                            if (size_not_set_flag) {
                                if (msg_txt) {
                                    msg_txt = msg_txt + "\n"
                                }
                                msg_txt = msg_txt + TextService.text('No Preference saved', {
                                        choise: "size",
                                        entity: session.userData[NODE].entity
                                    })[0]
                            }
                        }

                    }

                    if(msg_txt){
                        BotService.sendText(session,msg_txt)
                    }



                    console.log(JSON.stringify(userProfile))

                    let intent = {
                        intent: 'RESULTS.DIALOG',
                        score: 1,
                        entities: [{
                            entityType: 'RESULT.TOGGLE',
                            data: data
                        }]
                    }
                    session.userData.guestMode = !session.userData.guestMode

                    session.replaceDialog('xpresso-bot:results-dialog', intent)


                })
                    .catch(err => {
                        let promptObj = {id: null, buttons: []}
                        if (StateService2.checkResumeShopping(session)) {
                            promptObj.buttons.push({
                                buttonText: 'Back To Shopping',
                                triggerIntent: 'RESULTS.DIALOG',
                                entityType: 'WISHLIST.BACK.TO.SHOPPING',
                                data: {}
                            })
                        }
                        StateService.addPrompt2(session, promptObj)
                        //console.log("postback replied")

                        let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                        buttonsText.push(Constants.Labels.Introduction)
                        buttonsText.push(Constants.Labels.Help)
                        BotService.sendQuickReplyButtons(session, {text: 'Api failure message'}, buttonsText)

                        //BotService.sendQuickReplyButtons( session, 'Api failure message', [ Constants.Labels.Introduction, Constants.Labels.Help ] )
                        // builder.Prompts.choice( session, TextService.text( 'Api failure message' ), [ Constants.Labels.Introduction, Constants.Labels.Help ], {
                        //   maxRetries: 0,
                        //   listStyle: builder.ListStyle.button
                        // })
                        session.error(err)
                        session.endDialog()
                    })
            }

        }]

}