var _                    = require( 'lodash'                                   )
var Constants            = require( '../../constants'                          )
var ApiService           = require( '../../services/ApiService'                )
var GuideMeService       = require( '../guideme/guide-me-service'              )
var TypeAQueryService    = require( '../type_a_query/type-a-query-service'     )
var ImageService         = require( '../image_uploads/image-service'           )
var ImageViewMoreService = require( '../image_uploads/image-view-more-service' )
var ImageCategoryService = require( '../image_uploads/image-category-service'  )
var CollectionsService   = require( '../inspiration/collections-service'       )

// TODO:: Think of way to do all this automatically (possibly be regestering modules/middleware)

/**
 * Initialize a state
 */
getDefaultState = ( NODE ) => {
  
  switch( NODE ) {
    case Constants.NODE.GUIDEME             : return GuideMeService.getDefaultState( NODE ); break;
    case Constants.NODE.NLP                 : return TypeAQueryService.getDefaultState( NODE ); break;
    case Constants.NODE.IMAGEUPLOAD         : return ImageService.getDefaultState( NODE ); break;
    case Constants.NODE.IMAGEUPLOADVIEWMORE : return ImageViewMoreService.getDefaultState( NODE ); break;
    case Constants.NODE.IMAGEUPLOADCATEGORY : return ImageCategoryService.getDefaultState( NODE ); break;
    case Constants.NODE.COLLECTIONS         : return CollectionsService.getDefaultState( NODE ); break;

  }

}

/**
 * Call respective modules getResult function
 * 
 * Use specified NODE and state if provided
 * else use currentNode and its corresponding state from the session
 */
 getResults = ( session, NODE, state,toggle_gender ) => {

  NODE  = NODE  || session.userData.currentNode
  state = state || session.userData[ NODE ]
  
  switch( NODE ) {
    case Constants.NODE.GUIDEME             : return GuideMeService.getResults( state,session ); break;
    case Constants.NODE.NLP                 : return TypeAQueryService.getResults( state,session ); break;
    case Constants.NODE.IMAGEUPLOAD         : return ImageService.getResults( state, session ); break;
    case Constants.NODE.IMAGEUPLOADVIEWMORE : return ImageViewMoreService.getResults( state, session ); break;
    case Constants.NODE.IMAGEUPLOADCATEGORY : return ImageCategoryService.getResults( state, session ); break;
    case Constants.NODE.COLLECTIONS         : return CollectionsService.getResults( state,session,toggle_gender ); break;

  }

}

/**
 * 
 */
processResults = ( session, results ) => {

  switch( session.userData.currentNode ) {
    case Constants.NODE.GUIDEME               : return GuideMeService.processResults( session, results ); break;
    case Constants.NODE.NLP                   : return TypeAQueryService.processResults( session, results ); break;
    case Constants.NODE.IMAGEUPLOAD           : return ImageService.processResults( session, results ); break;
    case Constants.NODE.IMAGEUPLOADVIEWMORE   : return ImageViewMoreService.processResults( session, results ); break;
    case Constants.NODE.IMAGEUPLOADCATEGORY   : return ImageCategoryService.processResults( session, results ); break;
    case Constants.NODE.COLLECTIONS           : return CollectionsService.processResults( session, results ); break;
  }

}

/**
 * 
 */
displayResults = ( session, results ) => {

  switch( session.userData.currentNode ) {
    case Constants.NODE.GUIDEME             : return GuideMeService.displayResults( session, results ); break;
    case Constants.NODE.NLP                 : return TypeAQueryService.displayResults( session, results ); break;
    case Constants.NODE.IMAGEUPLOAD         : return ImageService.displayResults( session, results ); break;
    case Constants.NODE.IMAGEUPLOADVIEWMORE : return ImageViewMoreService.displayResults( session, results ); break;
    case Constants.NODE.IMAGEUPLOADCATEGORY : return ImageCategoryService.displayResults( session, results ); break;
    case Constants.NODE.COLLECTIONS         : return CollectionsService.displayResults( session, results ); break;
  }
}

// Misc Util functions
/**
 * Sets a variable in a given state
 */
setVariableByState = ( session, NODE, varName, varValue ) => {

  // If state is not defined for given Node
  if ( ! session.userData[ NODE ] ) {

    switch( NODE ) {
      case Constants.NODE.GUIDEME             : session.userData[ NODE ] = GuideMeService.getDefaultState( NODE ); break;
      case Constants.NODE.NLP                 : session.userData[ NODE ] = TypeAQueryService.getDefaultState( NODE ); break;
      case Constants.NODE.IMAGEUPLOAD         : session.userData[ NODE ] = ImageService.getDefaultState( NODE ); break;
      case Constants.NODE.IMAGEUPLOADVIEWMORE : session.userData[ NODE ] = ImageViewMoreService.getDefaultState( NODE ); break;
      case Constants.NODE.IMAGEUPLOADCATEGORY : session.userData[ NODE ] = ImageCategoryService.getDefaultState( NODE ); break;
      case Constants.NODE.COLLECTIONS         : session.userData[ NODE ] = CollectionsService.getDefaultState( NODE ); break;
    }

  }

  session.userData[ NODE ][ varName ] = varValue

}

module.exports = {
  getDefaultState    : getDefaultState,
  getResults         : getResults,
  processResults     : processResults,
  displayResults     : displayResults,
  setVariableByState : setVariableByState
}