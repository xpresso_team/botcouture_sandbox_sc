var _             = require( 'lodash'                             )
var builder       = require( 'botbuilder'                         )
var Constants     = require( '../../../../constants'              )
var ApiService    = require( '../../../../services/ApiService'    )
var StateService  = require( '../../../../services/StateService'  )
var StateService2 = require( '../../../../services/StateService2' )
var UtilService   = require( '../../../../services/UtilService'   )
var TextService   = require( '../../../../services/TextService'   )
var UserService   = require( '../../../../services/UserService'   )
var BotService    = require( '../../../../services/BotService'    )


// User pressed the Add Filters button after the Results
module.exports = {
  Label: Constants.Labels.GuideMe,
  Dialog: [
  function ( session, args, next ) {
    
    // Fall through
    if ( !args ) { next(); return; }
    session.sendTyping()

    let entityType        = args.entities[0].entityType
    let NODE              = args.entities[0].data.NODE
    let state             = args.entities[0].data.state
    let brandFilterActive = args.entities[0].data.brandFilterActive
    let priceFilterActive = args.entities[0].data.priceFilterActive
    let colorFilterActive = args.entities[0].data.colorFilterActive
    let sizeFilterActive  = args.entities[0].data.sizeFilterActive

    // let entityType        = args.entities[0].entityType
    // let userData          = args.entities[0].data.userData
    // let brandFilterActive = args.entities[0].data.brandFilterActive
    // let priceFilterActive = args.entities[0].data.priceFilterActive
    // let colorFilterActive = args.entities[0].data.colorFilterActive
    // let sizeFilterActive  = args.entities[0].data.sizeFilterActive

    // User pressed the 'Clear All' button below
    if ( entityType === 'RESULTS.EDIT.FILTERS.CLEARALL' ) {

      // TODO: This is a very temp fix
      UserService.saveUsersNegativePreference( session.message.address, state )

      StateService2.resetAllFilters( NODE, session )

      let args = {
        intent   : 'RESULTS.DIALOG',
        score    : 1,
        entities : [ {
          entity     : null,
          entityType : 'RESULTS.EDIT.FILTERS.CLEARALL',
          data       : { NODE : NODE }
        } ]
      }
      session.replaceDialog( 'xpresso-bot:results-dialog', args )
      return
    }


    // Create Quick reply buttons
    let buttons = []

    if ( brandFilterActive ) {
      buttons.push( { 
        buttonText    : 'Brand', 
        triggerIntent : 'RESULTS.EDIT.FILTER.ITEMS', 
        entityType    : 'RESULTS.EDIT.FILTER.BRAND', 
        data          : { NODE : NODE, state : state, filterType : 'brand' }
      } )
    }

    if ( priceFilterActive ) {
      buttons.push( { 
        buttonText    : 'Price',
        triggerIntent : 'RESULTS.EDIT.FILTER.ITEMS', 
        entityType    : 'RESULTS.EDIT.FILTER.PRICE',
        data          : { NODE : NODE, state : state, filterType : 'price' } 
      } )
    }

    if ( colorFilterActive ) {
      buttons.push( { 
        buttonText    : 'Color', 
        triggerIntent : 'RESULTS.EDIT.FILTER.ITEMS', 
        entityType    : 'RESULTS.EDIT.FILTER.COLOR', 
        data          : { NODE : NODE, state : state, filterType : 'colorsavailable' }
      } )
    }

    if ( sizeFilterActive ) {
      buttons.push( { 
        buttonText    : 'Size', 
        triggerIntent : 'RESULTS.EDIT.FILTER.ITEMS',
        entityType    : 'RESULTS.EDIT.FILTER.SIZE', 
        data          : { NODE : NODE, state : state, filterType : 'size' }
      } )
    }

    buttons.push( {
      buttonText    : 'Clear All', 
      triggerIntent : 'RESULTS.EDIT.FILTERS',
      entityType    : 'RESULTS.EDIT.FILTERS.CLEARALL', 
      data          : { NODE : NODE, state : state }
    } )

    // Dont do anything, just go back
    buttons.push( {
      buttonText    : 'Continue', 
      triggerIntent : 'RESULTS.DIALOG', 
      entityType    : 'RESULTS.EDIT.FILTER.CANCEL', 
      data          : { NODE : NODE, state : state }
    } )

    let promptObj = { id : null, buttons : buttons }
    StateService.addPrompt2( session, promptObj )
  
    let buttonsText = _.map( promptObj.buttons, button => button.buttonText )   
    BotService.sendQuickReplyButtons( session, 'Type a Query Edit Filter Select', buttonsText )
    // builder.Prompts.choice( session, TextService.text( 'Type a Query Edit Filter Select' ), buttonsText, {
    //   maxRetries: 0,
    //   listStyle: builder.ListStyle.button
    // } )
    session.endDialog()

  } /*,
  function ( session, result ) {
    session.reset()
  } */]

}