var _             = require( 'lodash'                             )
var builder       = require( 'botbuilder'                         )
var Constants     = require( '../../../../constants'              )
var ApiService    = require( '../../../../services/ApiService'    )
var StateService  = require( '../../../../services/StateService'  )
var StateService2 = require( '../../../../services/StateService2' )
var UtilService   = require( '../../../../services/UtilService'   )
var TextService   = require( '../../../../services/TextService'   )
var UserService   = require( '../../../../services/UserService'   )
var BotService    = require( '../../../../services/BotService'    )
var toTitleCase   = require( 'to-title-case'                      )


// User pressed the Add Filters button after the Results
module.exports = {
  Label: Constants.Labels.GuideMe,
  Dialog: [
  function ( session, args, next ) {
    
    // Fall through
    if ( !args ) { next(); return; }
    session.sendTyping()

    let entityType     = args.entities[0].entityType
    let NODE           = args.entities[0].data.NODE
    let state          = args.entities[0].data.state
    let filterType     = args.entities[0].data.filterType
    let attributeValue = args.entities[0].data.attributeValue     // Item press in this dialog


    // let entityType     = args.entities[0].entityType
    // let userData       = args.entities[0].data.userData
    // let filterType     = args.entities[0].data.filterType
    // let attributeValue = args.entities[0].data.attributeValue     // Item press in this dialog

    let brandFilterActive = StateService2.isAFilterActive2( NODE, session, 'brand' )
    let priceFilterActive = StateService2.isAFilterActive2( NODE, session, 'price' )
    let colorFilterActive = StateService2.isAFilterActive2( NODE, session, 'colorsavailable' )
    let sizeFilterActive  = StateService2.isAFilterActive2( NODE, session, 'size' )
    // Show Other Filters button only if other filters active except this one
    let showOtherFiltersButton = StateService2.isAnyFilterActiveExcept( NODE, session, filterType )
    // Show clear all only if more than one item available to be removed
    let showClearAllButton = session.userData[ NODE ][ filterType ] && session.userData[ NODE ][ filterType ].length > 1

    // User pressed a item button
    if ( entityType === 'RESULTS.EDIT.FILTER.ITEM.REMOVE' ) {

      // TODO: This is a very temp fix
      let tmpObj = { }
      tmpObj[ filterType ] = [ attributeValue ] 
      UserService.saveUsersNegativePreference( session.message.address, tmpObj )

      // console.log( 'Deleting ', filterType, attributeValue, session.userData[ NODE ][ filterType ] )

      let itemsRemaining = StateService2.deleteElementFromFilter( NODE, session, filterType, attributeValue )
      let state          = StateService2.cloneState( NODE, session, { pageNo : 0 } )

      if ( itemsRemaining.length > 0 ) {
        // More items in current filter remain
        let args = {
          intent   : 'RESULTS.EDIT.FILTER.ITEMS',
          score    : 1,
          entities : [ {
            entityType : 'RESULTS.EDIT.FILTER.ITEM',
            data       : { NODE: NODE, state : state, filterType : filterType }
          } ]
        }

        session.replaceDialog( 'xpresso-bot:results-edit-filter-items', args )
        return

      } else if ( StateService2.isAnyFilterActiveExcept( NODE, session, filterType ) ) {
        // Some other filter is active
        let args = {
          intent : 'RESULTS.EDIT.FILTERS',
          score  : 1,
          entities : [{
            entity     : null,
            entityType : 'RESULTS.EDIT.FILTER.ITEMS.CLEARALL',
            data : { 
              NODE              : NODE,
              state             : state, 
              brandFilterActive : StateService2.isAFilterActive2( NODE, session, 'brand' ), 
              priceFilterActive : StateService2.isAFilterActive2( NODE, session, 'price' ), 
              colorFilterActive : StateService2.isAFilterActive2( NODE, session, 'colorsavailable' ), 
              sizeFilterActive  : StateService2.isAFilterActive2( NODE, session, 'size' ) ,
            }
          }]
        }
        
        session.replaceDialog( 'xpresso-bot:results-edit-filters', args )
        return
      } else {
          // No filter remain
          let args = {
          intent   : 'RESULTS.DIALOG',
          score    : 1,
          entities : [ {
            entity     : null,
            entityType : 'RESULTS.EDIT.FILTER.ITEMS.CANCEL',
            data       : { NODE : NODE, state : state }
          } ]
        }
        
        session.replaceDialog( 'xpresso-bot:results-dialog', args )
        return
      }

    }

    // User pressed the 'Clear All' button below
    if ( entityType === 'RESULTS.EDIT.FILTER.ITEMS.CLEARALL' ) {

      // TODO: This is a very temp fix
      let tmpObj = { }
      tmpObj[ filterType ] = session.userData[ filterType ] 
      UserService.saveUsersNegativePreference( session.message.address, tmpObj )

      StateService2.resetAFilter( NODE, session, filterType )
      
      let state = StateService2.cloneState( NODE, session, { pageNo : 0 } )

      if( StateService2.isAnyFilterActive( NODE, session ) ) {
        
        let args = { 
          intent : 'RESULTS.EDIT.FILTERS',
          score  : 1,
          entities : [ {
            entity     : null,
            entityType : 'RESULTS.EDIT.FILTER.ITEMS.CLEARALL',
            data : {
              NODE              : NODE,
              state             : state, 
              brandFilterActive : StateService2.isAFilterActive2( NODE, session, 'brand' ), 
              priceFilterActive : StateService2.isAFilterActive2( NODE, session, 'price' ), 
              colorFilterActive : StateService2.isAFilterActive2( NODE, session, 'colorsavailable' ), 
              sizeFilterActive  : StateService2.isAFilterActive2( NODE, session, 'size' ) ,
            }
          } ]
        }
        
        session.replaceDialog( 'xpresso-bot:results-edit-filters', args )

      } else {
        
        let args = {
          intent   : 'RESULTS.DIALOG',
          score    : 1,
          entities : [ {
            entity     : null,
            entityType : 'RESULTS.DIALOG.EDIT.FILTER.ITEMS.CLEARALL',
            data       : { NODE : NODE, state : state }
          } ]
        }
        
        session.replaceDialog( 'xpresso-bot:results-dialog', args )

      }
      return
    }
    // End Clear All

    // Get filter values
    let filterValues = session.userData[ NODE ][ filterType ]

    // Create Quick reply buttons
    let buttons = []

    _.each( filterValues, filterValue => {
      buttons.push( { 
        buttonText    : toTitleCase( filterValue ), 
        triggerIntent : 'RESULTS.EDIT.FILTER.ITEMS', 
        entityType    : 'RESULTS.EDIT.FILTER.ITEM.REMOVE', 
        data          : { NODE : NODE, state : state, filterType : filterType, attributeValue : _.toLower( filterValue ) }
      } )
    } )
    
    if ( showClearAllButton ) {
      buttons.push( {
        buttonText    : 'Clear All', 
        triggerIntent : 'RESULTS.EDIT.FILTER.ITEMS',
        entityType    : 'RESULTS.EDIT.FILTER.ITEMS.CLEARALL', 
        data          : { NODE : NODE, state : state, filterType : filterType }
      } )
    }
    
    if ( showOtherFiltersButton ) {
      buttons.push( { 
        buttonText    : 'Other Filters', 
        triggerIntent : 'RESULTS.EDIT.FILTERS', 
        entityType    : 'RESULTS.EDIT.FILTER.BUTTON', 
        data          : { 
          NODE              : NODE,
          state             : state, 
          brandFilterActive : brandFilterActive,
          priceFilterActive : priceFilterActive,
          colorFilterActive : colorFilterActive,
          sizeFilterActive  : sizeFilterActive ,
        }
      } )
    }

    // Dont do anything, just go back
    buttons.push( {
      buttonText    : 'Continue', 
      triggerIntent : 'RESULTS.DIALOG', 
      entityType    : 'RESULTS.EDIT.FILTER.CANCEL', 
      data          : { NODE : NODE, state : state }
    } )

    let promptObj = { id : null, buttons : buttons }
    StateService.addPrompt2( session, promptObj )

    let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
    let filterTypeTxt = filterType === 'colorsavailable' ? 'color' : filterType

    BotService.sendQuickReplyButtons( session, { text : 'Type a Query Edit Filter Select Item', data : { filterType : filterTypeTxt } }, buttonsText )
    // builder.Prompts.choice( session, TextService.text( 'Type a Query Edit Filter Select Item', { filterType : filterTypeTxt } ), buttonsText, {
    //   maxRetries: 0,
    //   listStyle: builder.ListStyle.button
    // })

    session.endDialog()
  }/*,
  function ( session, result ) {
    session.reset()
  } */]

}