var _            = require( 'lodash'                      )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../../../constants'             )
var ApiService   = require( '../../../services/ApiService'   )
var StateService = require( '../../../services/StateService' )
var UtilService  = require( '../../../services/UtilService'  )
var TextService  = require( '../../../services/TextService'  )

module.exports = {
  Label: Constants.Labels.GuideMe,
  Dialog: [
  function ( session, args, next ) {

    session.sendTyping()

    // Fall through
    if ( !args || !args.entities || !( args.entities.length > 0 ) ) { next(); return; }

    let entityType = args.entities[0].entityType
    if ( 'RESULTS.PRICE.FILTER' === entityType ) {
      builder.Prompts.text( session, TextService.text( 'Type a Query Price Message' ) )
    }

  },
  function ( session, result, next ) {

    session.sendTyping() 

    if ( result && result.resumed === builder.ResumeReason.completed ) {

      // User entered text (possibly price)
      let userInput = result.response
      let args = { entities : [ { entity : userInput, entityType : 'RESULT.FILTERER.PRICE.TEXT', data : { userInput : userInput } } ] }

      // TODO: Figure out another way, instead of replaceDialog(),
      // although there is nothing wrong with this approach
      session.replaceDialog( 'xpresso-bot:results-filters-price', args )
      return

    } else {
      // Falling through
      next()
      return
    }

  },
  function ( session, result ) {
  
    session.reset()
  
  }]

}