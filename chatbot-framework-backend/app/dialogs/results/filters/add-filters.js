var _            = require( 'lodash'                         )
var builder      = require( 'botbuilder'                     )
var Constants    = require( '../../../constants'             )
var ApiService   = require( '../../../services/ApiService'   )
var StateService = require( '../../../services/StateService' )
var StateService2 = require( '../../../services/StateService2' )
var UtilService  = require( '../../../services/UtilService'  )
var TextService  = require( '../../../services/TextService'  )
var BotService   = require( '../../../services/BotService'   )


// User pressed the Add Filters button after the Results
module.exports = {
  Label: Constants.Labels.GuideMe,
  Dialog: [
  function ( session, args, next ) {
    
    session.sendTyping()

    // Fall through
    if ( !args ) { next(); return; }

    let entityType      = args.entities[0].entityType
    let NODE            = args.entities[0].data.NODE
    let state           = args.entities[0].data.state
    let guestMode       = args.entities[0].data.guestMode
    let showBrandFilter = args.entities[0].data.showBrandFilter
    let showPriceFilter = args.entities[0].data.showPriceFilter
    let showColorFilter = args.entities[0].data.showColorFilter
    let showSizeFilter  = args.entities[0].data.showSizeFilter

    // Create Quick reply buttons
    let buttons = []

    if ( showBrandFilter ) {
      buttons.push( { 
        buttonText    : 'Brand', 
        triggerIntent : 'RESULTS.FILTERS', 
        entityType    : 'RESULTS.BRAND.FILTER', 
        data          : { NODE : NODE, state : state, filterOn : 'brand', guestMode : guestMode }
      } )
    }

    if ( showPriceFilter ) {
      buttons.push( { 
        buttonText    : 'Price',
        triggerIntent : 'RESULTS.FILTERS.PRICE.PROMPT', 
        entityType    : 'RESULTS.PRICE.FILTER',
        data          : { NODE : NODE, state : state, filterOn : 'price', guestMode : guestMode } 
      } )
    }

    if ( showColorFilter ) {
      buttons.push( { 
        buttonText    : 'Color', 
        triggerIntent : 'RESULTS.FILTERS', 
        entityType    : 'RESULTS.COLOR.FILTER', 
        data          : { NODE : NODE, state : state, filterOn : 'colorsavailable', guestMode : guestMode }
      } )
    }

    if ( showSizeFilter ) {
      buttons.push( { 
        buttonText    : 'Size', 
        triggerIntent : 'RESULTS.FILTERS', 
        entityType    : 'RESULTS.SIZE.FILTER', 
        data          : { NODE : NODE, state : state, filterOn : 'size', guestMode : guestMode }
      } )
    }
    
    buttons.push( { 
      buttonText    : 'Cancel', 
      triggerIntent : 'RESULTS.DIALOG', 
      entityType    : 'RESULTS.CANCEL.FILTER', 
      data          : { NODE : NODE, state : state }
    } )
    
    let promptObj = { id : null, buttons : buttons }
    StateService.addPrompt2( session, promptObj )

    let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
    BotService.sendQuickReplyButtons( session, 'Type a Query Filterer Select', buttonsText )
    // builder.Prompts.choice( session, TextService.text( 'Type a Query Filterer Select' ), buttonsText, {
    //   maxRetries: 0,
    //   listStyle: builder.ListStyle.button
    // })
    
  },
  function ( session, result ) {
    session.reset()
  }]

}