var _ = require('lodash')
var builder = require('botbuilder')
const nconf = require('nconf')
var Constants = require('../../constants')
var ApiService = require('../../services/ApiService')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var UtilService = require('../../services/UtilService')
var TextService = require('../../services/TextService')
var BotService = require('../../services/BotService')
var toTitleCase = require('to-title-case')


getDefaultState = NODE => {
    let defaultState = {
        image_url :null,
        upload_id: null,
        entity: null,
        gender: [],
        pageNo: 0,
        pageSize: 10 + 2,
        // Filters
        price: [],
        subcategory: [],
        features: [],
        brand: [],
        details: [],
        colorsavailable: [],
        size: [],
        xc_category: [],
        client_name : []
    }
    return _.cloneDeep(defaultState)
}


/**
 *
 */
getResults = (state, session) => {
    console.log("\nentered image category service")

    console.log("Select category Image URL actual  is ",image_url)

    return ApiService.getAltVisualSearchResult(state.entity, state.upload_id, state.gender, session)
        .then(results => {

            // console.log( 'RESULTS -> ', results.results[ state.entity ].matches )

            if (!results.results[state.entity] || !results.results[state.entity].matches) {
                results.results[state.entity] = {matches: []}
            }
            results.Results = results.results[state.entity].matches
            // TODO: Exp/Prod send gender_flag at different levels
            results.gender_flag = results.results[state.entity].gender_flag || results.results.gender_flag
            delete results.results
            return results
        })
        .then(results => {

            // console.log( 'RESULTS -------------------------> ', results.Results, state )

            // Filter for brand, price, color, size

            // TODO: IMPLIMENT PRICE FILTER
            results.Results = _.filter(results.Results, product => {
                if (!state.brand || state.brand.length === 0) return true
                return _.includes(state.brand, product.brand)
            })

            results.Results = _.filter(results.Results, product => {
                if (!state.size || state.size.length === 0) return true
                return _.intersection(product.size, state.size).length
            })

            results.Results = _.filter(results.Results, product => {
                if (!state.colorsavailable || state.colorsavailable.length === 0) return true
                return _.intersection(product.colorsavailable, state.colorsavailable).length
            })

            return results

        })

}

/**
 *
 */
processResults = (session, results) => {
        console.log("\nentered image category service")

    let pageSize = session.userData[session.userData.currentNode].pageSize || 10 + 2
    let pageNo = session.userData[session.userData.currentNode].pageNo || 0
    let promptObj = {id: null, buttons: []}
    let gender_flag = null
    let cards = null
    let status_code = null
    if (results) {
        status_code = results.status_code
    }

    if (results && results.status_code === 1) {

        // let cards = UtilService.createAndSendImageResult( session, results.results )
        // pagedResults = UtilService.pageList( cards, pageNo, pageSize, null, null )

        let pagedResults = UtilService.pageList(results.Results, pageNo, pageSize, null, null)
        return UtilService.createCards(session, pagedResults).then(cards => {

            // ANALYTICS
            session.userData.analytics.out.visual_search = 3    // 3 - multi object search result
            session.userData.analytics.out.vision_engine_response = pagedResults

            gender_flag = results.gender_flag
            if (!results.gender_flag) {

                if (!session.userData[session.userData.currentNode].gender ||
                    session.userData[session.userData.currentNode].gender.length === 0 ||
                    _.includes(session.userData[session.userData.currentNode].gender, 'men')) {
                    promptObj.buttons.push(
                        {
                            buttonText: TextService.text('Image Gender Filter')[1],
                            triggerIntent: 'RESULTS.DIALOG',
                            entityType: 'IMAGE.UPLOAD.WOMEN.FILTER',
                            data: {state: {gender: ['women']}}
                        })
                }

                if (!session.userData[session.userData.currentNode].gender ||
                    session.userData[session.userData.currentNode].gender.length === 0 ||
                    _.includes(session.userData[session.userData.currentNode].gender, 'women')) {
                    promptObj.buttons.push(
                        {
                            buttonText: TextService.text('Image Gender Filter')[0],
                            triggerIntent: 'RESULTS.DIALOG',
                            entityType: 'IMAGE.UPLOAD.MEN.FILTER',
                            data: {state: {gender: ['men']}}
                        })
                }

            }

            let onLastPage = ( pageSize - 2 ) * ( pageNo + 1) >= cards.length
            if (!onLastPage) {
                promptObj.buttons.push({
                    buttonText: 'Show More',
                    triggerIntent: 'RESULTS.DIALOG',
                    entityType: 'IMAGE.UPLOAD.CATEGORY.SHOW.MORE',
                    data: {state: {pageNo: pageNo + 1}}
                })
            }
            promptObj.buttons = promptObj.buttons.concat(StateService2.getFiltersButtons(session, session.userData.currentNode, results.Results))

            return {status_code: status_code, gender_flag: gender_flag, cards: cards, promptObj: promptObj}
        }).catch(err => {

            console.log("some error happened in image category service " + err)

        })

    } else {
        promptObj.buttons = promptObj.buttons.concat(StateService2.getFiltersButtons(session, session.userData.currentNode, results.Results))

        return Promise.resolve({status_code: status_code, gender_flag: gender_flag, cards: cards, promptObj: promptObj})
    }


}

/**
 *
 */
displayResults = (session, results) => {
        console.log("\nentered image category service")

    let {status_code, gender_flag, cards, promptObj, status_msg} = results

    if (status_code === 1) {

        if (cards.length > 0) {

            BotService.sendText(session, {
                text: 'visual search category result display message',
                data: {entity: session.userData[session.userData.currentNode].entity}
            })
            if (session.message.source === "facebook") {

                BotService.sendCard(session, cards, true, "generic")

            } else {
                BotService.sendCard(session, cards)
            }

            StateService.addPrompt2(session, promptObj)
            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
            buttonsText = buttonsText.concat(TextService.text('Image Result New Payload Text'))

            BotService.sendQuickReplyButtons(session, 'Image result option message', buttonsText)

        } else {

            BotService.sendText(session, 'No Visual similar')

            let promptObj = {id: null, buttons: []}
            // if (StateService2.checkResumeShopping(session)) {
            //     promptObj.buttons.push({
            //         buttonText: 'Back To Shopping',
            //         triggerIntent: 'RESULTS.DIALOG',
            //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
            //         data: {}
            //     })
            // }

            if (nconf.get('Human_augmentauion')) {
                promptObj.buttons.push({
                    buttonText: TextService.text('Chat with Specialist')[0],
                    triggerIntent: 'HUMAN.AUGMENTATION',
                    data: {user_type: "user"}
                })
            }


            StateService.addPrompt2(session, promptObj)

            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
            for (let i = 0; i < TextService.text('No Image Result Payload Msg').length; i++) {
                buttonsText.push(TextService.text('No Image Result Payload Msg')[i])
            }
            BotService.sendQuickReplyButtons(session, 'Image result option message', buttonsText)

        }

        return

    }


    if (status_code === 0) {

        BotService.sendText(session, 'Img_server_problem')

        let promptObj = {id: null, buttons: []}
        // if (StateService2.checkResumeShopping(session)) {
        //     promptObj.buttons.push({
        //         buttonText: 'Back To Shopping',
        //         triggerIntent: 'RESULTS.DIALOG',
        //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
        //         data: {}
        //     })
        // }

        if (nconf.get('Human_augmentauion')) {
            promptObj.buttons.push({
                buttonText: TextService.text('Chat with Specialist')[0],
                triggerIntent: 'HUMAN.AUGMENTATION',
                data: {user_type: "user"}
            })
        }

        StateService.addPrompt2(session, promptObj)

        let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
        for (let i = 0; i < TextService.text('No Image Result Payload Msg').length; i++) {
            buttonsText.push(TextService.text('No Image Result Payload Msg')[i])
        }
        BotService.sendQuickReplyButtons(session, 'Image result option message', buttonsText)

        return
    }

    if (status_code === -1) {

        //BotService.sendText( session, 'Image failure for earlier postback' )
        BotService.sendText(session, status_msg)

        let promptObj = {id: null, buttons: []}
        // if (StateService2.checkResumeShopping(session)) {
        //     promptObj.buttons.push({
        //         buttonText: 'Back To Shopping',
        //         triggerIntent: 'RESULTS.DIALOG',
        //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
        //         data: {}
        //     })
        // }

        if (nconf.get('Human_augmentauion')) {
            promptObj.buttons.push({
                buttonText: TextService.text('Chat with Specialist')[0],
                triggerIntent: 'HUMAN.AUGMENTATION',
                data: {user_type: "user"}
            })
        }


        StateService.addPrompt2(session, promptObj)

        let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
        for (let i = 0; i < TextService.text('No Image Result Payload Msg').length; i++) {
            buttonsText.push(TextService.text('No Image Result Payload Msg')[i])
        }

        BotService.sendQuickReplyButtons(session, 'Image result option message', buttonsText)

        //console.log('No image found in the URL')
        // session.send( TextService.text( 'Image failure for earlier postback')[0] )
        // builder.Prompts.choice( session, TextService.text('Image result option message' )[0], TextService.text( 'No Image Result Payload Msg' ), {
        //   maxRetries: 0,
        //   listStyle: builder.ListStyle.button
        // })

        return

    }

    BotService.sendText(session, 'Img_server_problem')
    // if (StateService2.checkResumeShopping(session)) {
    //     promptObj.buttons.push({
    //         buttonText: 'Back To Shopping',
    //         triggerIntent: 'RESULTS.DIALOG',
    //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
    //         data: {}
    //     })
    // }

    if (nconf.get('Human_augmentauion')) {
        promptObj.buttons.push({
            buttonText: TextService.text('Chat with Specialist')[0],
            triggerIntent: 'HUMAN.AUGMENTATION',
            data: {user_type: "user"}
        })
    }

    StateService.addPrompt2(session, promptObj)

    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
    for (let i = 0; i < TextService.text('No Image Result Payload Msg').length; i++) {
        buttonsText.push(TextService.text('No Image Result Payload Msg')[i])
    }

    BotService.sendQuickReplyButtons(session, 'Image result option message', buttonsText)
    // session.send(TextService.text("Img_server_problem")[0])
    // builder.Prompts.choice(session, TextService.text('Image result option message')[0],TextService.text('No Image Result Payload Msg') , {
    //   maxRetries: 0,
    //   listStyle: builder.ListStyle.button
    // })

}


module.exports = {
    getDefaultState: getDefaultState,
    getResults: getResults,
    processResults: processResults,
    displayResults: displayResults
}
