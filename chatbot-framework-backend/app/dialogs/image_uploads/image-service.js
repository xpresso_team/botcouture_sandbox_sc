var _ = require('lodash')
var builder = require('botbuilder')
const nconf = require('nconf')
var Constants = require('../../constants')
var ApiService = require('../../services/ApiService')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var UtilService = require('../../services/UtilService')
var TextService = require('../../services/TextService')
var BotService = require('../../services/BotService')
var toTitleCase = require('to-title-case')

getDefaultState = NODE => {
    let defaultState = {
        imageUrl: null,
        upload_id: null,
        gender: [],
        pageNo: 0,       // pageNo, pageSize for categories quick reply buttons
        pageSize: 9 + 2,
        isSocialUrl: false
    }
    return _.cloneDeep(defaultState)
}


/**
 *
 */

getResults = (state, session) => {
    if (state.isSocialUrl) {
        return ApiService.getSocialVisualSearchResult(state.imageUrl, state.gender,state.client_name, session)
    } else {
        return ApiService.getVisualSearchResult(state.imageUrl, state.gender,state.client_name, session)
    }

}

/**
 *
 */
processResults = (session, results) => {

    console.log('process results', results)

    let pageSize = session.userData[session.userData.currentNode].pageSize || 9 + 2
    let pageNo = session.userData[session.userData.currentNode].pageNo || 0
    let promptObj = {id: null, buttons: []}
    let cards = null
    let gender_flag = null
    let status_code = null
    let upload_id = results.upload_id
    session.userData[session.userData.currentNode].upload_id = upload_id


    if(results.status_code != -1) {

        session.userData[session.userData.currentNode].imageUrl = results.results.upload_img
    }else{
        session.userData[session.userData.currentNode].imageUrl = ""
    }

//    console.log("Image URL obtained is ",results.results.upload_img)

    let image_url = session.userData[session.userData.currentNode].imageUrl

    console.log("Image URL actual  is ",image_url)


    if (results) {
        status_code = results.status_code
    }


    if (results && results.status_code === 0) {

        // Ask category from user
        let categoryList = null
        if (results.select_category && results.select_category.length > 0) {
            categoryList = results.select_category
        } else {
            // Backup image categories
            categoryList = ApiService.createCategoryButtons()
        }

        // Remove blank categories
        categoryList = _.filter(categoryList, category => category)

        _.each(categoryList, category => {
            promptObj.buttons.push({
                buttonText: toTitleCase(category),
                triggerIntent: 'RESULTS.DIALOG',
                entityType: 'IMAGE.CATEGORY',
                data: {
                    NODE: Constants.NODE.IMAGEUPLOADCATEGORY,
                    resetState: true,
                    state: {entity: category.charAt(0).toUpperCase() + category.slice(1), upload_id: results.upload_id,image_url :image_url}
                }
            })
        })

        let showNext = {
            buttonText: 'Next Page',
            triggerIntent: 'RESULTS.DIALOG',
            entityType: 'IMAGE.UPLOAD.NEXTPAGE',
            data: {state: {pageNo: pageNo + 1}}
        }

        let showPrevious = {
            buttonText: 'Previous Page',
            triggerIntent: 'RESULTS.DIALOG',
            entityType: 'IMAGE.UPLOAD.PREVIOUSPAGE',
            data: {state: {pageNo: pageNo - 1}}
        }

        showNext = ( pageSize - 2 ) * ( pageNo + 1 ) < categoryList.length ? showNext : null
        showPrevious = pageNo !== 0 && session.message.address.channelId === 'facebook' ? showPrevious : null

        promptObj.buttons = UtilService.pageList(promptObj.buttons, pageNo, pageSize, showPrevious, showNext)
        return Promise.resolve({status_code: status_code, gender_flag: gender_flag, cards: cards, promptObj: promptObj})

    } else if (results && results.status_code === 1) {

        // Image match and categories found
        return UtilService.createAndSendImageResult2(session, results.results, session.userData[session.userData.currentNode], upload_id)
            .then(cards => {

                if (cards.length > 0) {

                    // ANALYTICS
                    session.userData.analytics.out.visual_search = 3    // 3 - multi object search result
                    session.userData.analytics.out.vision_engine_response = []    // 3 - multi object search result


                    gender_flag = results.results.gender_flag
                    if (!results.results.gender_flag) {

                        if (!session.userData[session.userData.currentNode].gender ||
                            session.userData[session.userData.currentNode].gender.length === 0 ||
                            _.includes(session.userData[session.userData.currentNode].gender, 'men')) {
                            promptObj.buttons.push(
                                {
                                    buttonText: TextService.text('Image Gender Filter')[1],
                                    triggerIntent: 'RESULTS.DIALOG',
                                    entityType: 'IMAGE.UPLOAD.WOMEN.FILTER',
                                    data: {state: {gender: ['women']}}
                                })
                        }

                        if (!session.userData[session.userData.currentNode].gender ||
                            session.userData[session.userData.currentNode].gender.length === 0 ||
                            _.includes(session.userData[session.userData.currentNode].gender, 'women')) {
                            promptObj.buttons.push(
                                {
                                    buttonText: TextService.text('Image Gender Filter')[0],
                                    triggerIntent: 'RESULTS.DIALOG',
                                    entityType: 'IMAGE.UPLOAD.MEN.FILTER',
                                    data: {state: {gender: ['men']}}
                                })
                        }

                    }

                }
                return {status_code: status_code, gender_flag: gender_flag, cards: cards, promptObj: promptObj}
            }).catch(err => {

                console.log("some error happened in image service  " + err)

            })
    } else if (results && results.status_code === -1) {

        // In case vision sent something like this:
        // {
        //     "status_code": -1,
        //     "status_msg": "Something went wrong. Kindly try after sometime."
        // }
        return {
            status_code: status_code,
            gender_flag: null,
            cards: null,
            promptObj: null,
            status_msg: results.status_msg
        }
    }


}

/**
 *
 */
displayResults = (session, results) => {

    let {status_code, gender_flag, cards, promptObj, status_msg} = results

    if (status_code === 0) {

        // if (StateService2.checkResumeShopping(session)) {
        //     promptObj.buttons.push({
        //         buttonText: 'Back To Shopping',
        //         triggerIntent: 'RESULTS.DIALOG',
        //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
        //         data: {}
        //     })
        // }

        if (nconf.get('Human_augmentauion')) {
            promptObj.buttons.push({
                buttonText: TextService.text('Chat with Specialist')[0],
                triggerIntent: 'HUMAN.AUGMENTATION',
                data: {user_type: "user"}
            })
        }

        StateService.addPrompt2(session, promptObj)
        let buttonsText = _.map(promptObj.buttons, button => button.buttonText)


        BotService.sendQuickReplyButtons(session, 'Image Category Selection message', buttonsText)

        // builder.Prompts.choice( session, TextService.text( 'Image Category Selection message' )[0], buttonsText, {
        //   maxRetries: 0,
        //   listStyle: builder.ListStyle.button
        // })

        session.endDialog()
        return
    }


    if (status_code === 1) {

        if (cards.length > 0) {

            BotService.sendText(session, 'image category msg')
            if (session.message.source === "facebook") {

                BotService.sendCard(session, cards, true, "generic")

            } else {
                BotService.sendCard(session, cards)
            }

            let feedback_button = TextService.text('Product feedback')

            console.log(feedback_button)

            //let skus = _.map(cards, item => item.default_action.url)

            for(let i=0;i<feedback_button.length;i++){

                let triggerIntent

                if(i ==0){

                    triggerIntent = 'PRODUCT-LIKE'

                }
                if(i ==1){

                    triggerIntent = 'PRODUCT-DISLIKE'

                }

                if(i ==2){

                    triggerIntent = 'PRODUCT-FEEDBACK'

                }

                promptObj.buttons.push({
                    buttonText: feedback_button[i],
                    triggerIntent: triggerIntent,
                    entityType: 'Feedback',
                    data: {emotion: feedback_button[i]}
                })

            }

            if (!gender_flag) {
                StateService.addPrompt2(session, promptObj)
            }

            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
            buttonsText = buttonsText.concat(TextService.text('Image Result New Payload Text'))
            BotService.sendQuickReplyButtons(session, 'Image result option message', buttonsText)

            // session.send( TextService.text( 'image category msg' )[0] )
            // var reply = new builder.Message( session ).attachmentLayout( builder.AttachmentLayout.carousel ).attachments( cards );
            // session.send( reply )

            // if ( ! gender_flag ) {
            //   StateService.addPrompt2( session, promptObj )
            // }

            // let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
            // buttonsText = buttonsText.concat( TextService.text('Image Result New Payload Text') )
            // builder.Prompts.choice( session, TextService.text( 'Image result option message' )[0], buttonsText, {
            //   maxRetries : 0,
            //   listStyle  : builder.ListStyle.button
            // })

        } else {

            BotService.sendText(session, 'No Visual similar')

            let promptObj = {id: null, buttons: []}
            // if (StateService2.checkResumeShopping(session)) {
            //     promptObj.buttons.push({
            //         buttonText: 'Back To Shopping',
            //         triggerIntent: 'RESULTS.DIALOG',
            //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
            //         data: {}
            //     })
            // }

            if (nconf.get('Human_augmentauion')) {
                promptObj.buttons.push({
                    buttonText: TextService.text('Chat with Specialist')[0],
                    triggerIntent: 'HUMAN.AUGMENTATION',
                    data: {user_type: "user"}
                })
            }

            StateService.addPrompt2(session, promptObj)

            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
            let button_arr = TextService.text('No Image Result Payload Msg')
            for (let i = 0; i < button_arr.length; i++) {
                buttonsText.push(button_arr[i])
            }

            BotService.sendQuickReplyButtons(session, 'Image result option message', buttonsText)

            // session.send(TextService.text("No Visual similar")[0])
            // builder.Prompts.choice(session, TextService.text('Image result option message')[0],TextService.text('No Image Result Payload Msg') , {
            //   maxRetries: 0,
            //   listStyle: builder.ListStyle.button
            // })

        }

        session.endDialog()
        return
    }

    if (status_code === -1) {

        console.log('No image found in the URL')
        //BotService.sendText(session, 'Image failure for earlier postback')
        if (status_msg) {

            BotService.sendText(session, status_msg)

        } else {

            BotService.sendText(session, 'No Visual similar')

        }


        let promptObj = {id: null, buttons: []}
        if (nconf.get('Human_augmentauion')) {
            promptObj.buttons.push({
                buttonText: TextService.text('Chat with Specialist')[0],
                triggerIntent: 'HUMAN.AUGMENTATION',
                data: {user_type: "user"}
            })
        }

        StateService.addPrompt2(session, promptObj)

        let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
        for (let i = 0; i < TextService.text('No Image Result Payload Msg').length; i++) {
            buttonsText.push(TextService.text('No Image Result Payload Msg')[i])
        }

        BotService.sendQuickReplyButtons(session, 'Image result option message', buttonsText)
        session.endDialog()
        return
    }

    console.log('This should not happen')
    BotService.sendText(session, 'Img_server_problem')

    // if (StateService2.checkResumeShopping(session)) {
    //     promptObj.buttons.push({
    //         buttonText: 'Back To Shopping',
    //         triggerIntent: 'RESULTS.DIALOG',
    //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
    //         data: {}
    //     })
    // }

    if (nconf.get('Human_augmentauion')) {
        promptObj.buttons.push({
            buttonText: TextService.text('Chat with Specialist')[0],
            triggerIntent: 'HUMAN.AUGMENTATION',
            data: {user_type: "user"}
        })
    }

    StateService.addPrompt2(session, promptObj)

    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
    for (let i = 0; i < TextService.text('No Image Result Payload Msg').length; i++) {
        buttonsText.push(TextService.text('No Image Result Payload Msg')[i])
    }

    BotService.sendQuickReplyButtons(session, 'Image result option message', buttonsText)

    session.endDialog()

}


module.exports = {
    getDefaultState: getDefaultState,
    getResults: getResults,
    processResults: processResults,
    displayResults: displayResults
}
