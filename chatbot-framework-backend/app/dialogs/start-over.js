/**
 * Created by aashish_amber on 13/5/17.
 */
var _            = require( 'lodash'                      )
var builder      = require( 'botbuilder'                  )
var Constants    = require( '../constants'             )
var StateService = require( '../services/StateService' )
var TextService  = require( '../services/TextService'  )
var BotService   = require( '../services/BotService'   )
var UserService   = require( '../services/UserService'   )

module.exports = {
    Label: Constants.Labels.Introduction,
    Dialog: [
        function ( session ) {

            session.sendTyping()

            // Reset state at start
            //StateService.resetState( session )
            //StateService.resetPrompt2( session )
            UserService.setActiveState( session.message.address, true)

            let userName    = session.message.user && session.message.user.name ? session.message.user.name : 'User'
            let channelName = session.message.source ? 'on ' + _.startCase( _.toLower( session.message.source ) ) : ''
            let firstName   = userName.split( ' ' )[0]

            if (session.message.source === "facebook") {


                let postBackButtons = []


                postBackButtons.push(
                    {
                        "type": "postback",
                        "title": TextService.text('Start Over new option')[0],
                        "payload": TextService.text('Start over new Postbacks')[0]
                    }
                )

                postBackButtons.push(
                    {
                        "type": "postback",
                        "title": TextService.text('Start Over new option')[1],
                        "payload": TextService.text('Start over new Postbacks')[1]
                    }
                )

                postBackButtons.push(
                    {
                        "type": "postback",
                        "title": TextService.text('Start Over new option')[2],
                        "payload": TextService.text('Start over new Postbacks')[2]
                    }
                )

                let payload = {
                    "template_type": "button",
                    "text":TextService.text('Start Over Message')[0],
                    "buttons": postBackButtons
                }

                BotService.sendCard(session, postBackButtons,true,"button",TextService.text('Start Over Message')[0])
                session.endDialog()


            }else {

                BotService.sendText( session, { text : 'Start Over Message' } )

                let cards = []
                let card = new builder.HeroCard(session)
                    .title(TextService.text('Button Template Title')[0])
                    .buttons([
                        builder.CardAction.postBack(session, TextService.text('Start over new Postbacks')[0], TextService.text('Start Over new option')[0]),
                        builder.CardAction.postBack(session, TextService.text('Start over new Postbacks')[1], TextService.text('Start Over new option')[1]),
                        builder.CardAction.postBack(session, TextService.text('Start over new Postbacks')[2], TextService.text('Start Over new option')[2]),
                    ])
                cards.push(card)
                BotService.sendCard(session, cards)
                session.endDialog()
            }
            // var reply = new builder.Message( session ).attachmentLayout( builder.AttachmentLayout.carousel ).attachments( cards );
            // session.send( reply )

        }]
};

