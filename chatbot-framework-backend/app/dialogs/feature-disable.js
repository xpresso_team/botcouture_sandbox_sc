var _            = require( 'lodash'                   )
var builder      = require( 'botbuilder'               )
var Constants    = require( '../constants'             )
var StateService = require( '../services/StateService' )
var UserService  = require( '../services/UserService'  )
var TextService  = require( '../services/TextService'  )
var BotService   = require( '../services/BotService'   )

module.exports = {
    Label: Constants.Labels.Goodbye,
    Dialog: [
        function ( session,args ) {

            if ( ! args ) { session.endDialog(); return; }

            session.sendTyping()

            let feature = " "

            if (_.has(args, 'entities[0].data.feature')){

                feature = args.entities[0].data.feature

            }


            UserService.setActiveState( session.message.address, true )

            BotService.sendText( session, {
                text: 'Feature disabled msg',
                data: {feature: feature}
            })

            session.endDialog()

        }]

};