var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../constants')
var TextService = require('../services/TextService')
var nconf = require('nconf')
var toTitleCase = require('to-title-case')
var BotService = require('../services/BotService')
var UserService = require('../services/UserService')
var ResultsService = require('./results/results-service')


let SEARCH_ITEM_IMAGES = nconf.get('SEARCH_ITEM_IMAGES')

module.exports = {
    Label: Constants.Labels.SearchItem,
    Dialog: [
        function (session) {

            session.sendTyping()
            // session.userData[Constants.NODE.NLP].nlp_reset_flag=1
            ResultsService.setVariableByState(session, Constants.NODE.NLP, 'nlp_reset_flag', 1)
            UserService.setActiveState(session.message.address, true)

            let text = TextService.text('Search For an Item Message')
            BotService.sendText(session,text)
            let titleText = TextService.text('Search For an Item Carousel Text')
            let buttonText = TextService.text('Search For an Item Buttons Text')


            if ((nconf.get("Visual Search")) || (nconf.get("Type Search")) || (nconf.get("Guided Search"))) {


                let cards = []

                if (nconf.get('Type Search')) {

                    cards.push(new builder.HeroCard(session)
                        .title(titleText[0])
                        //.subtitle('Type A Query in Natural Language.')
                        //.text('Description text about typing a query.')
                        .images([
                            builder.CardImage.create(session, SEARCH_ITEM_IMAGES.TYPE_A_QUERY)
                        ])
                        .buttons([
                            builder.CardAction.postBack(session, Constants.Labels.TypeQuery, toTitleCase(buttonText[0])),
                        ]))
                }

                if (nconf.get('Visual Search')) {

                    cards.push(new builder.HeroCard(session)
                        .title(titleText[1])
                        .images([
                            builder.CardImage.create(session, SEARCH_ITEM_IMAGES.UPLOAD_IMAGE)
                        ])
                        .buttons([
                            builder.CardAction.postBack(session, Constants.Labels.UploadImage, toTitleCase(buttonText[1])),
                        ]))
                }

                if (nconf.get('Guided Search')) {

                    cards.push(new builder.HeroCard(session)
                        .title(titleText[2])
                        .images([
                            builder.CardImage.create(session, SEARCH_ITEM_IMAGES.GUIDE_ME)
                        ])
                        .buttons([
                            builder.CardAction.postBack(session, Constants.Labels.GuideMe, toTitleCase(buttonText[2])),
                        ]))
                }

                BotService.sendCard(session, cards)
            }

            session.endDialog()

        }
    ]
}