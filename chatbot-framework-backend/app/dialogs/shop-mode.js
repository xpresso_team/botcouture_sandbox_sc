/**
 * Created by aashish_amber on 13/5/17.
 */
var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../constants')
var StateService = require('../services/StateService')
var StateService2 = require('../services/StateService2')
var TextService = require('../services/TextService')
var BotService = require('../services/BotService')
var UserService = require('../services/UserService')

module.exports = {
    Label: Constants.Labels.ShopMode,
    Dialog: [
        function (session) {

            session.sendTyping()

            // Reset state at start
            //StateService.resetState( session )
            //StateService.resetPrompt2( session )
            UserService.setActiveState(session.message.address, true)

            let userName = session.message.user && session.message.user.name ? session.message.user.name : 'User'
            let channelName = session.message.source ? 'on ' + _.startCase(_.toLower(session.message.source)) : ''
            let firstName = userName.split(' ')[0]
            let cards = []
            let card

            if (session.message.source === "facebook") {
                console.log("Sending for ", session.message.source)

                let postBackButtons = []

                postBackButtons.push(
                    {
                        "type": "postback",
                        "title": TextService.text('Shop mode option')[0],
                        "payload": TextService.text('Shop mode Postback')[0]
                    }
                )

                postBackButtons.push(
                    {
                        "type": "postback",
                        "title": TextService.text('Shop mode option')[1],
                        "payload": TextService.text('Shop mode Postback')[1]
                    }
                )

                let payload = {
                    "template_type": "button",
                    "text": TextService.text('Shop mode msg'),
                    "buttons": postBackButtons
                }

                BotService.sendCard(session, postBackButtons, true, "button", TextService.text('Shop mode msg'))

            } else {

                BotService.sendText(session, {text: 'Shop mode msg'})

                card = new builder.HeroCard(session)
                    .title(TextService.text('Button Template Title')[0])
                    .buttons([
                        builder.CardAction.postBack(session, TextService.text('Shop mode Postback')[0], TextService.text('Shop mode option')[0]),
                        builder.CardAction.postBack(session, TextService.text('Shop mode Postback')[1], TextService.text('Shop mode option')[1])
                    ])
                cards.push(card)
                BotService.sendCard(session, cards)
            }

                //console.log('card send')

                let promptObj = {id: null, buttons: []}
                if (StateService2.checkResumeShopping(session)) {
                    promptObj.buttons.push({
                        buttonText: 'Back To Shopping',
                        triggerIntent: 'RESULTS.DIALOG',
                        entityType: 'WISHLIST.BACK.TO.SHOPPING',
                        data: {}
                    })
                }
                StateService.addPrompt2(session, promptObj)
                //console.log("postback replied")

                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                for (let i = 0; i < TextService.text('Shop mode quickreply').length; i++) {
                    buttonsText.push(TextService.text('Shop mode quickreply')[i])
                }

                BotService.sendQuickReplyButtons(session, {text: 'Shop mode quick Msg'}, buttonsText)

                console.log('quick reply  send')

                session.endDialog()
        }]
};

