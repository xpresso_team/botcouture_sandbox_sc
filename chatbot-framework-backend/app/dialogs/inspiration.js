var builder = require('botbuilder')
var Constants = require('../constants')
var TextService = require('../services/TextService')
var nconf = require('nconf')
var UtilService = require('../services/UtilService')
var BotService = require('../services/BotService')
var UserService = require('../services/UserService')

let INSPIRATION_IMAGES = nconf.get('INSPIRATION_IMAGES')

module.exports = {
    Label: Constants.Labels.Inspiration,
    Dialog: [
        function (session) {

            session.sendTyping()
            UserService.setActiveState(session.message.address, true)

            BotService.sendText(session, 'Find Inspiration Message')

            let NODE = Constants.NODE.COLLECTIONS
            session.userData.currentNode = NODE

            let intent = {
                intent: 'INSPIRATION.COLLECTIONS.PROFILE',
                entities: [{
                    entity: 'INSPIRATION.COLLECTIONS.PROFILE',
                    entityType: 'INSPIRATION.COLLECTIONS.PROFILE',
                    data: {collectionType: null}
                }]
            }

            intent.entities[0].data.collectionType = 'collections'
            let collectionPostBack = 'POSTBACK::' + JSON.stringify(intent)

            intent.entities[0].data.collectionType = 'trends'
            let trendsPostBack = 'POSTBACK::' + JSON.stringify(intent)

            // intent.entities[0].data.collectionType = 'gifts'
            // let collectionPostBack = 'POSTBACK::' + JSON.stringify( intent )

            let cards = []

            if (nconf.get('Collections')) {

                cards.push({
                    title: TextService.text('Find Inspiration Carousel Text')[0],
                    images: [{url: INSPIRATION_IMAGES.COLLECTIONS}],
                    buttons: [{
                        type: 'postBack',
                        value: collectionPostBack,
                        title: TextService.text('Find Inspiration Buttons Text')[0]
                    }]
                })
            }
            if (nconf.get('Trends')) {
                cards.push({
                    title: TextService.text('Find Inspiration Carousel Text')[1],
                    images: [{url: INSPIRATION_IMAGES.DESIGN_TRENDS}],
                    buttons: [{
                        type: 'postBack',
                        value: trendsPostBack,
                        title: TextService.text('Find Inspiration Buttons Text')[1]
                    }]
                })

            }

            cards = UtilService.createCards2(session, cards)
            BotService.sendCard(session, cards)

            // create reply with Carousel AttachmentLayout
            // var reply = new builder.Message( session ).attachmentLayout( builder.AttachmentLayout.carousel ).attachments( cards )
            // session.sendTyping()
            // session.send( reply )

            session.endDialog()

        }]
};
