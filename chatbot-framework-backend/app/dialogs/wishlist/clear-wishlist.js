var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../../constants')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var TextService = require('../../services/TextService')
var UserService = require('../../services/UserService')
var ApiService = require('../../services/ApiService')
var UtilService = require('../../services/UtilService')
var BotService = require('../../services/BotService')
var toTitleCase = require('to-title-case')


module.exports = {
    Label: Constants.Labels.Introduction,
    Dialog: [
        function (session, args, next) {

            session.sendTyping()

            if (!args) {
                next();
                return;
            }

            let entityType = args.entities[0].entityType
            //let REPLACE_DIALOG = args.entities[0].data.REPLACE_DIALOG

            // Get users wishlist
            UserService.clearWishlist(session.message.address)
                .then(wishlist => {

                    //BotService.sendText(session, TextService.text( 'Wishlist Item Save Success', { productName : toTitleCase( productName ) } ) )
                    //session.replaceDialog( REPLACE_DIALOG, null )

                    let promptObj = {id: null, buttons: []}
                    if (StateService2.checkResumeShopping(session)) {
                        promptObj.buttons.push({
                            buttonText: 'Back To Shopping',
                            triggerIntent: 'RESULTS.DIALOG',
                            entityType: 'WISHLIST.BACK.TO.SHOPPING',
                            data: {}
                        })
                    }

                    promptObj.buttons.push({
                        buttonText: 'Back to Search',
                        triggerIntent: 'SEARCH-ITEM',
                        entityType: 'SEARCH-ITEM',
                        data: {}
                    })

                    promptObj.buttons.push({
                        buttonText: 'Start Over',
                        triggerIntent: 'INTRODUCTION',
                        entityType: 'INTRODUCTION',
                        data: {}
                    })

                    StateService.addPrompt2(session, promptObj)
                    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)

                    BotService.sendQuickReplyButtons(session, 'Wishlist cleared successfully', buttonsText)
                    session.endDialog()
                })
                .catch(err => {

                    let promptObj = {id: null, buttons: []}

                    // if (StateService2.checkResumeShopping(session)) {
                    //     promptObj.buttons.push({
                    //         buttonText: 'Back To Shopping',
                    //         triggerIntent: 'RESULTS.DIALOG',
                    //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
                    //         data: {}
                    //     })
                    // }

                    StateService.addPrompt2(session, promptObj)
                    //console.log("postback replied")

                    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                    buttonsText.push(Constants.Labels.Introduction)
                    buttonsText.push(Constants.Labels.Help)
                    BotService.sendQuickReplyButtons(session, {text: 'Api failure message'}, buttonsText)
                    session.error(err)
                    session.endDialog()
                })
            session.endDialog()

        }
    ]
};