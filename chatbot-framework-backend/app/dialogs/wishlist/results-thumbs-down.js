/**
 * Created by aashish_amber on 20/4/17.
 */
var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../../constants')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var TextService = require('../../services/TextService')
var UserService = require('../../services/UserService')
var ApiService = require('../../services/ApiService')
var UtilService = require('../../services/UtilService')
var BotService   = require( '../../services/BotService'   )

module.exports = {
    Label  : Constants.Labels.Introduction,
    Dialog : [
        function( session, args, next ) {

            session.sendTyping()
            if ( ! args ) { next(); return; }

            let entityType  = args.entities[0].entityType
            let xc_sku      = args.entities[0].data.xc_sku
            let productName = args.entities[0].data.productName
            let imageUrl    = args.entities[0].data.imageUrl


            session.userData.Feedback = {}
            session.userData.Feedback.emotion = 'Negative'
            session.userData.Feedback.text = `[ IMAGE.UPLOAD ][ ${ xc_sku } ][ ${ productName } ][ ${ imageUrl } ]`
            console.log( 'Feedback', session.userData.Feedback.text )
            let promptObj = { id : null, buttons : [] }
            if (StateService2.checkResumeShopping(session)) {
                promptObj.buttons.push({
                    buttonText: 'Back To Shopping',
                    triggerIntent: 'RESULTS.DIALOG',
                    entityType: 'WISHLIST.BACK.TO.SHOPPING',
                    data: {}
                })
            }
            StateService.addPrompt2(session, promptObj)
            let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
            BotService.sendQuickReplyButtons( session, 'Thank you for your feedback!', buttonsText )
            // session.send( 'Thank you for your feedback!' )

            UserService.sendFeedback( session ).catch( err => {
                console.log( 'Error', err )
                session.error( err )
                session.endDialog()
            })
            session.endDialog()

        }]
}
