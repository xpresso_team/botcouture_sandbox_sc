/**
 * Created by aashish_amber on 13/3/17.
 */
var builder = require('botbuilder')
var Constants = require('../constants')
var TextService = require('../services/TextService')
var nconf = require('nconf')
var BotService = require('../services/BotService')
var _ = require('lodash')
var StateService = require('../services/StateService')
var StateService2 = require('../services/StateService2')

let HELP_VIDEO_THUMB = nconf.get('HELP_Thumbnail')

module.exports = {

    Label: Constants.Labels.Help_Ask,

    Dialog: [
        function (session, args, next) {

            let text = TextService.text('Help New Message')
            BotService.sendText(session,text)
            let titleText = TextService.text('Help Video Title')

            if (session.message.source === "facebook") {
                console.log("Sending for ", session.message.source, args.entities[0].entity)


                var elements = [];
                for (let i = 0; i < titleText.length; i++) {
                    if (nconf.get(titleText[i])) {
                        let element = {
                            title: titleText[i],
                            subtitle: TextService.FAQ_VID_TEXT(titleText[i])[0],
                            image_url: HELP_VIDEO_THUMB[titleText[i]],
                            buttons: []

                        };
                        element.buttons.push({
                            type: 'postback',
                            title: TextService.text('Help Video Play')[0],
                            payload: 'PLAY::' + titleText[i] + '::FAQ',
                        });
                        element.buttons.push({
                            "type": "element_share"
                        });
                        elements.push(element)
                    }
                }
                let payload = {
                    template_type: "generic",
                    sharable: true,
                    image_aspect_ratio: "square",
                    elements: elements
                }

                BotService.sendCard(session, elements, true, "generic")

            } else {

                //console.log(titleText)
                var cards = []
                for (let i = 0; i < titleText.length; i++) {
                    if (nconf.get(titleText[i])) {
                        let card = new builder.HeroCard(session)
                            .title(titleText[i])
                            .text(TextService.FAQ_VID_TEXT(titleText[i]))
                            .images([
                                //builder.CardImage.create(session, 'http://' + nconf.get('IMAGE_RESIZE_API_URL') + '/imagem/recenter_noseg/?url=' + HELP_VIDEO_THUMB[titleText[i]] + '&aspect=1.91x1')
                                builder.CardImage.create(session, HELP_VIDEO_THUMB[titleText[i]])
                            ])
                            .buttons([
                                builder.CardAction.postBack(session, 'PLAY::' + titleText[i] + '::FAQ', TextService.text('Help Video Play')[0]),

                            ])
                        //console.log('http://' + nconf.get('IMAGE_RESIZE_API_URL') + '/imagem/recenter_noseg/?url=' + HELP_VIDEO_THUMB[titleText[i]] + '&aspect=1.91x1')
                        cards.push(card)
                    }
                }

                BotService.sendCard(session, cards)
            }
            let promptObj = {id: null, buttons: []}
            if (StateService2.checkResumeShopping(session)) {
                promptObj.buttons.push({
                    buttonText: 'Back To Shopping',
                    triggerIntent: 'RESULTS.DIALOG',
                    entityType: 'WISHLIST.BACK.TO.SHOPPING',
                    data: {}
                })
            }
            StateService.addPrompt2(session, promptObj)
            console.log("postback replied")

            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
            buttonsText.push('FAQ')
            buttonsText.push(Constants.Labels.Introduction)
            BotService.sendQuickReplyButtons(session, {text: 'Help option'}, buttonsText)

            session.endDialog()

        }
    ]
}