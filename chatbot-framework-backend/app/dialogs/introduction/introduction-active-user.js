var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../../constants')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var TextService = require('../../services/TextService')
var BotService = require('../../services/BotService')
var UserService = require('../../services/UserService')


module.exports = {
    Label: Constants.Labels.Introduction,
    Dialog: [
        function (session, args, next) {

            session.sendTyping()
            UserService.setActiveState(session.message.address, true)

            // StateService.resetState( session )

            let userName = session.message.user && session.message.user.name ? session.message.user.name : 'User'
            let channelName = session.message.source ? 'on ' + _.startCase(_.toLower(session.message.source)) : ''

            //let msg = TextService.text( "Response to a Greeting", { user_first_name : userName } )
            //session.send( msg )
            let msg2 = 'You may choose one of the following options.'

            let promptObj = {id: null, buttons: []}

            // Only show Resume shopping if user has currentNode
            if (StateService2.checkResumeShopping(session)) {
                if (session.userData.currentNode) {
                    promptObj.buttons.push({
                        buttonText: 'Back to Shopping',
                        triggerIntent: 'RESULTS.DIALOG',
                        entityType: '',
                        data: {}
                    })
                }
            }

            StateService.addPrompt2(session, promptObj)
            let buttonsText = _.map(promptObj.buttons, button => button.buttonText)

            buttonsText = ['New Query', 'Back To Search'].concat(buttonsText).concat(['Start Over', 'Help'])
            // buttonsText = buttonsText.concat( [ 'New Query', 'Back To Search', 'Start Over', 'Help' ] )

            BotService.sendQuickReplyButtons(session, TextService.text('Returning_user_option_msg')[0], buttonsText)
        },
        function (session, result) {
            session.reset()
        }]
};