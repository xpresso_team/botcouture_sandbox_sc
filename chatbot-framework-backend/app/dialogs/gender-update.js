var _              = require( 'lodash'                      )
var builder        = require( 'botbuilder'                  )
var Constants      = require( '../constants'             )
var ApiService     = require( '../services/ApiService'   )
var StateService   = require( '../services/StateService' )
var StateService2   = require( '../services/StateService2' )
var TextService    = require( '../services/TextService'  )
var UserService    = require( '../services/UserService'  )
var BotService     = require( '../services/BotService'   )
var ResultsService = require( './results/results-service'  )
var nconf          = require( 'nconf'                       )
const uuidV4 = require('uuid/v4')

module.exports = {
    Label: Constants.Labels.GuideMe,
    Dialog: [
        function ( session, args, next ) {

            session.sendTyping()
            ResultsService.setVariableByState( session, Constants.NODE.NLP, 'nlp_reset_flag', 1 )

            UserService.setActiveState( session.message.address, true)


            // Get user's gender
            UserService.getUser(session.message.address)
                .then(userObj => {

                    console.log("Gender updated for ", args.entities[0].entity)

                    let gender = userObj.gender
                    session.userData.gender = gender

                    let data = args.entities[0].data

                    data.gender = gender


                    let intent = {
                        intent: args.entities[0].entity,
                        score: 1,
                        entities: [{
                            entity: userObj.gender,
                            entityType: args.entities[0].entityType,
                            data: data
                        }]
                    }


                    session.replaceDialog('xpresso-bot:'+args.entities[0].data.dialog, intent)

                })
                .catch(err => {

                    let promptObj = { id : null, buttons : [] }
                    // if (StateService2.checkResumeShopping(session)) {
                    //     promptObj.buttons.push({
                    //         buttonText: 'Back To Shopping',
                    //         triggerIntent: 'RESULTS.DIALOG',
                    //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
                    //         data: {}
                    //     })
                    // }
                    StateService.addPrompt2(session, promptObj)

                    let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
                    let button_arr = [ Constants.Labels.Introduction, Constants.Labels.Help ]
                    for(let i = 0;i<button_arr.length;i++){
                        buttonsText.push(button_arr[i])
                    }
                    BotService.sendQuickReplyButtons( session, { text : 'Api failure message'}, buttonsText )
                    session.error(err)
                    session.endDialog()
                })
            session.endDialog()

        }]

}