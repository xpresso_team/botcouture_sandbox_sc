var builder     = require( 'botbuilder'              )
var Constants   = require( '../constants'            )
var TextService = require( '../services/TextService' )
var nconf       = require( 'nconf'                   )
var BotService   = require( '../services/BotService'   )
var _            = require( 'lodash'                      )
var StateService = require( '../services/StateService' )
var StateService2 = require( '../services/StateService2' )
var UserService = require( '../services/UserService' )

let DEMO_VIDEO_URL = nconf.get( 'DEMO_VIDEO_URL' )
let HELP_VIDEO_THUMB = nconf.get('HELP_Thumbnail')


module.exports = {

  Label: Constants.Labels.Help,

  Dialog: [   
  function ( session, args, next ) {

    session.sendTyping()
      //UserService.setActiveState( session.message.address, true)
      console.log("Help")
      let text = TextService.text('Discover Features Message')
      BotService.sendText(session,text)
      let titleText = TextService.text('Help Video Title')
      //console.log(titleText)

      if (session.message.source === "facebook") {
          console.log("Sending for ", session.message.source, args.entities[0].entity)


          var elements=[];

          if(DEMO_VIDEO_URL) {

              let element = {
                  title: TextService.text('Demo video title')[0],
                  subtitle: TextService.text('Demo video text')[0],
                  image_url: HELP_VIDEO_THUMB['take_a_tour'],
                  buttons: []

              };

              element.buttons.push({
                  type: 'postback',
                  title: TextService.text('Help Video Play')[0],
                  payload: 'PLAY::' + TextService.text('Demo video title')[0] + '::NO_FAQ',
              });
              element.buttons.push({
                  "type": "element_share"
              });
              elements.push(element)
          }


          for (let i = 0; i < titleText.length; i++) {

              if(nconf.get(titleText[i])) {
                  let element = {
                      title: titleText[i],
                      subtitle: TextService.FAQ_VID_TEXT(titleText[i])[0],
                      image_url: HELP_VIDEO_THUMB[titleText[i]],
                      buttons: []

                  };

                  element.buttons.push({
                      type: 'postback',
                      title: TextService.text('Help Video Play')[0],
                      payload: 'PLAY::' + titleText[i] + '::NO_FAQ',
                  });
                  element.buttons.push({
                      "type": "element_share"
                  });
                  elements.push(element)
              }
          }
          let payload = {
              template_type: "generic",
              sharable :true,
              image_aspect_ratio :"square",
              elements: elements
          }

          BotService.sendCard(session, elements, true,"generic")

      } else {


          var cards = []
          if(DEMO_VIDEO_URL) {
              let card = new builder.HeroCard(session)
                  .title(TextService.text('Demo video title')[0])
                  .text(TextService.text('Demo video text')[0])
                  .images([
                      //builder.CardImage.create(session, 'http://' + nconf.get('IMAGE_RESIZE_API_URL') + '/imagem/recenter_noseg/?url=' + HELP_VIDEO_THUMB[titleText[i]] + '&aspect=1.91x1')
                      builder.CardImage.create(session, HELP_VIDEO_THUMB['take_a_tour'])
                  ])
                  .buttons([
                      builder.CardAction.postBack(session, 'PLAY::' + TextService.text('Demo video title')[0] + '::NO_FAQ', TextService.text('Help Video Play')[0]),

                  ])
              cards.push(card)
          }
          for (let i = 0; i < titleText.length; i++) {
              if(nconf.get(titleText[i])) {
                  let card = new builder.HeroCard(session)
                      .title(titleText[i])
                      .text(TextService.FAQ_VID_TEXT(titleText[i]))
                      .images([
                          //builder.CardImage.create(session, 'http://' + nconf.get('IMAGE_RESIZE_API_URL') + '/imagem/recenter_noseg/?url=' + HELP_VIDEO_THUMB[titleText[i]] + '&aspect=1.91x1')
                          builder.CardImage.create(session, HELP_VIDEO_THUMB[titleText[i]])
                      ])
                      .buttons([
                          builder.CardAction.postBack(session, 'PLAY::' + titleText[i] + '::NO_FAQ', TextService.text('Help Video Play')[0]),

                      ])
                  //console.log('http://' + nconf.get('IMAGE_RESIZE_API_URL') + '/imagem/recenter_noseg/?url=' + HELP_VIDEO_THUMB[titleText[i]] + '&aspect=1.91x1')
                  cards.push(card)
              }
          }

          BotService.sendCard(session, cards)
      }
        // TODO : Change the following too
      let promptObj = { id : null, buttons : [] }
      if (StateService2.checkResumeShopping(session)) {
          promptObj.buttons.push({
              buttonText: 'Back To Shopping',
              triggerIntent: 'RESULTS.DIALOG',
              entityType: 'WISHLIST.BACK.TO.SHOPPING',
              data: {}
          })
      }
      StateService.addPrompt2(session, promptObj)
      console.log("postback replied")

      let buttonsText = _.map( promptObj.buttons, button => button.buttonText )
      buttonsText.push(Constants.Labels.Introduction)
      BotService.sendQuickReplyButtons( session, TextService.text("Help option"), buttonsText )
      session.endDialog()

      session.endDialog()
  }
  ]

}
