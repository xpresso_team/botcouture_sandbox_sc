/**
 * Created by aashish_amber on 13/3/17.
 */
var builder = require('botbuilder')
var Constants = require('../constants')
var TextService = require('../services/TextService')
var request = require('superagent')
var BotService = require('../services/BotService')
var _ = require('lodash')
var StateService = require('../services/StateService')
var StateService2 = require('../services/StateService2')
var UserService = require('../services/UserService')


module.exports = {

    Label: Constants.Labels.FAQ_CAT,

    Dialog: [
        function (session, args, next) {
            console.log("faq arg")
            console.log(args)
            console.log(args.entities[0].data)
            //UserService.setActiveState( session.message.address, true)

            if (args && args.entities[0] && args.entities[0].data && (!args.entities[0].data.full_faq)) {

                var FAQ_CAT = global.FAQS_CAT
                let cards = []
                //console.log(FAQ_CAT)

                for (var Key in FAQ_CAT) {
                    let card = new builder.HeroCard(session)
                        .title(TextService.FAQS_CAT(Key)[0])
                        .buttons([
                            builder.CardAction.postBack(session, 'FAQ::' + TextService.FAQS_CAT(Key)[1], TextService.FAQS_CAT(Key)[1]),
                            builder.CardAction.postBack(session, 'FAQ::' + TextService.FAQS_CAT(Key)[2], TextService.FAQS_CAT(Key)[2]),
                            builder.CardAction.postBack(session, 'FAQ::' + TextService.FAQS_CAT(Key)[3], TextService.FAQS_CAT(Key)[3])
                        ])
                    cards.push(card)
                }
                BotService.sendText(session, 'FAQ Category display msg')
                BotService.sendCard(session, cards)
                let promptObj = {id: null, buttons: []}
                if (StateService2.checkResumeShopping(session)) {
                    promptObj.buttons.push({
                        buttonText: 'Back To Shopping',
                        triggerIntent: 'RESULTS.DIALOG',
                        entityType: 'WISHLIST.BACK.TO.SHOPPING',
                        data: {}
                    })
                }
                StateService.addPrompt2(session, promptObj)
                console.log("postback replied")

                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                console.log(TextService.text('FAQ CAT option'))
                for (let i = 0; i < TextService.text('FAQ CAT option').length; i++) {
                    buttonsText.push(TextService.text('FAQ CAT option')[i])
                }
                BotService.sendQuickReplyButtons(session, {text: 'Help option'}, buttonsText)
            } else {

                var FAQ_CAT = global.FAQS_CAT
                let cards = []
                //console.log(FAQ_CAT)
                let Key = "1"
                let card = new builder.HeroCard(session)
                    .title(TextService.FAQS_CAT(Key)[0])
                    .buttons([
                        builder.CardAction.postBack(session, 'FAQ::' + TextService.FAQS_CAT(Key)[1], TextService.FAQS_CAT(Key)[1]),
                        builder.CardAction.postBack(session, 'FAQ::' + TextService.FAQS_CAT(Key)[2], TextService.FAQS_CAT(Key)[2]),
                        builder.CardAction.postBack(session, 'FAQ::' + TextService.FAQS_CAT(Key)[3], TextService.FAQS_CAT(Key)[3])
                    ])
                cards.push(card)
                BotService.sendText(session, 'FAQ Category display msg')
                BotService.sendCard(session, cards)
                let promptObj = {id: null, buttons: []}
                if (StateService2.checkResumeShopping(session)) {
                    promptObj.buttons.push({
                        buttonText: 'Back To Shopping',
                        triggerIntent: 'RESULTS.DIALOG',
                        entityType: 'WISHLIST.BACK.TO.SHOPPING',
                        data: {}
                    })
                }
                StateService.addPrompt2(session, promptObj)
                console.log("postback replied")

                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                for (let i = 0; i < TextService.text('FAQ Wishlist option').length; i++) {
                    buttonsText.push(TextService.text('FAQ Wishlist option')[i])
                }
                BotService.sendQuickReplyButtons(session, {text: 'Help option'}, buttonsText)
            }


        },
        function (session, result) {
            session.reset()
            //session.replaceDialog( 'start' )
        }
    ]
}