var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../../constants')
var ApiService = require('../../services/ApiService')
var TextService = require('../../services/TextService')
var BotService = require('../../services/BotService')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var UserService = require('../../services/UserService')
var ResultsService = require('../results/results-service')
var toTitleCase = require('to-title-case')

module.exports = {
    Label: Constants.Labels.ExploreCollections,
    Dialog: [
        function (session, args, next) {

            if (!args) {
                next();
                return;
            }
            session.sendTyping()
            console.log("\n Entered collections-list\n")
            // session.userData[Constants.NODE.NLP].nlp_reset_flag=1
            let NODE = Constants.NODE.COLLECTIONS
            session.userData.currentNode = NODE
            ResultsService.setVariableByState(session, Constants.NODE.NLP, 'nlp_reset_flag', 1)
            UserService.setActiveState(session.message.address, true)

            let collectionType = args.entities[0].data.collectionType
            let gender = args.entities[0].data.gender
            let guest_mode = args.entities[0].data.guest_mode
            if (guest_mode === false) {
                session.userData.guestMode = guest_mode
            } else if (guest_mode === true) {
                session.userData.guestMode = guest_mode
            }


            ApiService.getCollections(collectionType, gender, session)
                .then(collections => {

                    if (collections.collection_list && collections.collection_list.length > 0) {
                       console.log(collections.collection_list)
                        BotService.sendText(session, {
                            text: 'Collection display msg',
                            data: {collection: _.startCase(collectionType), gender: _.startCase(gender)}
                        })
                        let cards = []
                        collections.collection_list.forEach(collection => {
                            let card = new builder.HeroCard(session)
                                .title(toTitleCase(collection.name))
                                //.subtitle('Explore collection ONE.')
                                //.text('Description text about exploring collection ONE.')
                                .images([
                                    builder.CardImage.create(session, collection.image)
                                ])
                                .buttons([
                                    builder.CardAction.postBack(session, 'EXPLORE::' + collectionType.toUpperCase() + '::' + collection.name + '::' + gender, TextService.text('Latest collection Button')[0])
                                ])
                            cards.push(card)
                        })

                        BotService.sendCard(session, cards)
                        session.endDialog()
                    } else {
                        BotService.sendText(session, 'Sorry, currently these collections are not available.')
                        session.endDialog()
                    }

                })
                .catch(err => {

                    let promptObj = {id: null, buttons: []}

                    // if (StateService2.checkResumeShopping(session)) {
                    //     promptObj.buttons.push({
                    //         buttonText: 'Back To Shopping',
                    //         triggerIntent: 'RESULTS.DIALOG',
                    //         entityType: 'WISHLIST.BACK.TO.SHOPPING',
                    //         data: {}
                    //     })
                    // }

                    StateService.addPrompt2(session, promptObj)
                    //console.log("postback replied")

                    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
                    buttonsText.push(Constants.Labels.Introduction)
                    buttonsText.push(Constants.Labels.Help)
                    BotService.sendQuickReplyButtons(session, {text: 'Api failure message'}, buttonsText)
                    session.error(err)
                    session.endDialog()
                })

            session.endDialog()

        }]
}
