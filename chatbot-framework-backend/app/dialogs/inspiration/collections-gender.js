var _ = require('lodash')
var builder = require('botbuilder')
var Constants = require('../../constants')
var ApiService = require('../../services/ApiService')
var TextService = require('../../services/TextService')
var StateService = require('../../services/StateService')
var UserService = require('../../services/UserService')
var BotService = require('../../services/BotService')
var ResultsService = require('../results/results-service')
var nconf = require('nconf')

module.exports = {

    Label: Constants.Labels.ExploreCollections,

    Dialog: [

        function (session, args, next) {

            if (!args) {
                next();
                return;
            }
            session.sendTyping()
            let NODE = Constants.NODE.COLLECTIONS
            session.userData.currentNode = NODE
            // session.userData[Constants.NODE.NLP].nlp_reset_flag=1
            ResultsService.setVariableByState(session, Constants.NODE.NLP, 'nlp_reset_flag', 1)
            UserService.setActiveState(session.message.address, true)

            let collectionType = args.entities[0].data.collectionType
            let guest_mode = args.entities[0].data.guest_mode
            if (guest_mode === false) {
                session.userData.guestMode = guest_mode
            } else if (guest_mode === true) {
                session.userData.guestMode = guest_mode
            }
            /*
             */
            if (session.userData.guestMode) {
                let promptObj = {id: null, buttons: []}
                promptObj.buttons.push({
                    buttonText: 'Men',
                    triggerIntent: 'INSPIRATION.COLLECTIONS.LIST',
                    entityType: 'INSPIRATION.VIEW.COLLECTIONS.GENDER',
                    data: {collectionType: collectionType, gender: 'men', guest_mode: true}
                })

                promptObj.buttons.push({
                    buttonText: 'Women',
                    triggerIntent: 'INSPIRATION.COLLECTIONS.LIST',
                    entityType: 'INSPIRATION.VIEW.COLLECTIONS.GENDER',
                    data: {collectionType: collectionType, gender: 'women', guest_mode: true}
                })

                StateService.addPrompt2(session, promptObj)
                let buttonsText = _.map(promptObj.buttons, button => button.buttonText)

                BotService.sendQuickReplyButtons(session, 'Collection Gender Question', buttonsText)
                console.log("\nLeaving collections-gender")
                session.endDialog()
            }

        }]

}
