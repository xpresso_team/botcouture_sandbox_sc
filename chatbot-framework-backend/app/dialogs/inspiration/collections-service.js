var _ = require('lodash')
const nconf = require('nconf')
var builder = require('botbuilder')
var Constants = require('../../constants')
var ApiService = require('../../services/ApiService')
var StateService = require('../../services/StateService')
var StateService2 = require('../../services/StateService2')
var UtilService = require('../../services/UtilService')
var TextService = require('../../services/TextService')
var BotService = require('../../services/BotService')
var toTitleCase = require('to-title-case')


getDefaultState = NODE => {
    let defaultState = {
        collectionType: null,
        collectionName: null,
        entity: null,
        gender: [],
        pageNo: 0,
        pageSize: 10 + 2,
        // Filters
        price: [],
        subcategory: [],
        features: [],
        brand: [],
        details: [],
        colorsavailable: [],
        size: [],
        xc_category: [],
        client_name : []
    }
    return _.cloneDeep(defaultState)
}


/**
 *
 */
getResults = (state, session, toggle_gender) => {

    let guest_mode = session.userData.guestMode

    let skip_personalisation = false


    if(toggle_gender) {
        let Old_gender = toggle_gender.Old_gender
        let New_gender = toggle_gender.New_gender
        let NODE = Constants.NODE.COLLECTIONS
        session.userData.currentNode = NODE

        if (toggle_gender.flag) {

            skip_personalisation = true

        }
    }

    console.log("skip_personalisation is ", skip_personalisation)

    if (skip_personalisation) {

        console.log("collection personalisation skipped")

        if (guest_mode) {

            BotService.sendText(session, {
                text: 'Gender different from profile collection guest_mode',
                data: {
                    entity: state.collectionType,
                    gender: Old_gender,
                    profile_gender: New_gender,
                    entity2: state.collectionType,
                    profile_gender_2: New_gender,
                    entity3: state.collectionType
                }
            })

        } else {

            BotService.sendText(session, {
                text: 'Gender different from profile collection',
                data: {
                    entity: state.collectionType,
                    gender: Old_gender,
                    profile_gender: Old_gender,
                    entity2: state.collectionType,
                    profile_gender_2: New_gender,
                    entity3: state.collectionType,
                    profile_gender_3: New_gender
                }
            })

            session.userData.guestMode = true;


        }


        let intent = {
            intent: 'INSPIRATION.COLLECTIONS.LIST',
            score: 1,
            entities: [{
                entity: state.gender,
                entityType: 'INSPIRATION.VIEW.COLLECTIONS.GENDER',
                data: {
                    collectionType: state.collectionType,
                    collectionMsg: null,
                    gender: state.gender,
                    client_name : state.client_name,
                    guest_mode: session.userData.guestMode
                }
            }]
        }

        session.replaceDialog('xpresso-bot:inspiration-collections-list', intent)
        return Promise.resolve("Skip");


    } else {


        return ApiService.getCollectionItems(state.collectionType, state.collectionName, state.gender, state.entity,state.client_name, session)
            .then(results => {
                results.Results = results.product_list
                delete results.product_list
                return results
            })
            .then(results => {

                // Filter for brand, price, color, size
                //console.log(JSON.stringify(results))
                //console.log("State",JSON.stringify(state))

                // TODO: IMPLIMENT PRICE FILTER
                results.Results = _.filter(results.Results, product => {
                    if (!state.brand || state.brand.length === 0) return true
                    return _.includes(state.brand, product.brand)
                })

                results.Results = _.filter(results.Results, product => {
                    if (!state.size || state.size.length === 0) return true
                    return _.intersection(product.size, state.size).length
                })

                results.Results = _.filter(results.Results, product => {
                    if (!state.colorsavailable || state.colorsavailable.length === 0) return true
                    return _.intersection(product.colorsavailable, state.colorsavailable).length
                })


                /*results.Results = _.filter(results.Results, product => {
                 if (!state.price || state.price.length === 0) return true
                 return _.includes(state.price, product.price)
                 })
                 */

                return results

            })

    }
}

/**
 *
 */
processResults = (session, results) => {

    let pageSize = session.userData[session.userData.currentNode].pageSize || 10 + 2     // 10 for products in carousel and + 2 for previous/next, FB allows only 10 products in carousel
    let pageNo = session.userData[session.userData.currentNode].pageNo || 0
    let productList = results.Results || []
    let cards = null
    let promptObj = {id: null, buttons: []}

    // Products found
    if (productList.length > 0) {

        let pagedResults = UtilService.pageList(productList, pageNo, pageSize, null, null)
        return UtilService.createCards(session, pagedResults).then(cards => {

            // ANALYTICS
            session.userData.analytics.out.product_response_list = pagedResults

            // Check if Show More needs to be shown
            let onLastPage = ( pageSize - 2 ) * ( pageNo + 1 ) >= productList.length

            if (!onLastPage) {
                promptObj.buttons.push({
                    buttonText: 'Show More',
                    triggerIntent: 'RESULTS.DIALOG',
                    entityType: 'VIEW.COLLECTION.SHOW.MORE',
                    data: {state: {pageNo: pageNo + 1}}
                })
            }

            promptObj.buttons = promptObj.buttons.concat(StateService2.getFiltersButtons(session, session.userData.currentNode, productList))

            return {cards: cards, promptObj: promptObj}

        }).catch(err => {

            console.log("some error happened in collection service " + err)

        })

    } else {
        promptObj.buttons = promptObj.buttons.concat(StateService2.getFiltersButtons(session, session.userData.currentNode, productList))
        return Promise.resolve({cards: cards, promptObj: promptObj})
    }
}


/**
 *
 */
displayResults = (session, results) => {

    let {cards, promptObj} = results

    if (cards && cards.length > 0) {

        BotService.sendText(session, {
            text: 'collection product display msg', data: {
                category: toTitleCase(session.userData[session.userData.currentNode].entity),
                collection: toTitleCase(session.userData[session.userData.currentNode].collectionName)
            }
        })

        if (session.message.source === "facebook") {
            BotService.sendCard(session, cards, true, "generic")
        } else {
            BotService.sendCard(session, cards)
        }

    } else {

        BotService.sendText(session, 'No products found in the collections.')
        if (nconf.get('Human_augmentauion')) {
            promptObj.buttons.push({
                buttonText: TextService.text('Chat with Specialist')[0],
                triggerIntent: 'HUMAN.AUGMENTATION',
                data: {user_type: "user"}
            })
        }
        StateService.addPrompt2(session, promptObj)

        let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
        let button_arr = [Constants.Labels.Introduction, Constants.Labels.Help]
        for (let i = 0; i < button_arr.length; i++) {
            buttonsText.push(button_arr[i])
        }
        BotService.sendQuickReplyButtons(session, {text: 'Take a tour Message'}, buttonsText)
        session.endDialog()
        return
    }

    let feedback_button = TextService.text('Product feedback')

    console.log(feedback_button)

    //let skus = _.map(cards, item => item.default_action.url)

    for(let i=0;i<feedback_button.length;i++){

        let triggerIntent

        if(i ==0){

            triggerIntent = 'PRODUCT-LIKE'

        }
        if(i ==1){

            triggerIntent = 'PRODUCT-DISLIKE'

        }

        if(i ==2){

            triggerIntent = 'PRODUCT-FEEDBACK'

        }

        promptObj.buttons.push({
            buttonText: feedback_button[i],
            triggerIntent: triggerIntent,
            entityType: 'Feedback',
            data: {emotion: feedback_button[i]}
        })

    }

    if(typeof session.userData['COLLECTIONS'].client_name!=='undefined' && session.userData['COLLECTIONS'].client_name!==null && session.userData['COLLECTIONS'].client_name.length>0 ) {
      promptObj.buttons.push({
          buttonText    : 'Remove Shop',
          triggerIntent : 'SELECT-RETAILER',
          entityType    : 'COLLECTIONS.REMOVE.SHOP',
          data          : {state : session.userData['COLLECTIONS']}
      })
    } else {
      if(nconf.get('Select_Shop_Webview')) {
            let selectionflag = 1
            console.log("client_name flag inside typeaquery is ", session.userData.client_name)
            if(session.userData.client_name.length>0) {
                promptObj.buttons.push({
                buttonText: 'Use Your Shop',
                triggerIntent: 'SELECT-RETAILER',
                entityType: 'COLLECTIONS.USEYOUR.SHOP',
                data: {state:session.userData['COLLECTIONS']}
                })    
            } else {
                promptObj.buttons.push({
                buttonText: 'Select Shop',
                triggerIntent: 'SELECT-RETAILER',
                entityType: 'COLLECTIONS.SELECT.SHOP',
                data: {state:session.userData['COLLECTIONS']}
                })
            }
        } else {
            promptObj.buttons.push({
                buttonText: 'Select Shop',
                triggerIntent: 'SELECT-RETAILER',
                entityType: 'COLLECTIONS.SELECT.SHOP',
                data: {state:session.userData['COLLECTIONS']}
            })
        }
    }

    if (session.userData.guestMode) {

        promptObj.buttons.push({
            buttonText: 'Shop 4 me',
            triggerIntent: 'RESULT.TOGGLE',
            entityType: 'WISHLIST.BACK.TO.SHOPPING',
            data: {guest_mode: true}
        })


    } else {

        promptObj.buttons.push({
            buttonText: 'Shop 4 other',
            triggerIntent: 'RESULT.TOGGLE',
            entityType: 'WISHLIST.BACK.TO.SHOPPING',
            data: {guest_mode: false}
        })

    }


    /*
    if (nconf.get('Human_augmentauion')) {
        promptObj.buttons.push({
            buttonText: TextService.text('Chat with Specialist')[0],
            triggerIntent: 'HUMAN.AUGMENTATION',
            data: {user_type: "user"}
        })
    }

    */
    StateService.addPrompt2(session, promptObj)
    let buttonsText = _.map(promptObj.buttons, button => button.buttonText)
    for (let i = 0; i < TextService.text('No Collection Result Payload Msg').length; i++) {
        buttonsText.push(TextService.text('No Collection Result Payload Msg')[i])
    }
    buttonsText.push(Constants.Labels.Help)

    BotService.sendQuickReplyButtons(session, {text: 'Take a tour Message'}, buttonsText)

    session.endDialog()

}


module.exports = {
    getDefaultState: getDefaultState,
    getResults: getResults,
    processResults: processResults,
    displayResults: displayResults
}

