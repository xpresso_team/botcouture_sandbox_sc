#!/usr/bin/env bash
cd ${ROOT_FOLDER}/scripts
kill $(cat forever.pid)
kill $(cat bot.pid)
rm -f forever.pid
rm -f bot.pid
echo "Stoping the redis server"
/etc/init.d/redis-server stop
echo "Stopped the redis server"