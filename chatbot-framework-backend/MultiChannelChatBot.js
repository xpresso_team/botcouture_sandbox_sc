var _ = require('lodash')
var restify = require('restify')
var builder = require('botbuilder')
var fs = require('fs')
var https = require('https')
var nconf = require('nconf')
var noop = require('./loadEnv')
var xpressoBot = require('./app/xpresso-bot')
var UserService = require('./app/services/UserService')
var winston = require('./app/services/loggly/LoggerUtil')
var ApiService = require('./app/services/ApiService')
var UtilService = require('./app/services/UtilService')
var Constants = require('./app/constants')
var KafkaService = require('./kafka-service')
var HumanAugmentationService = require('./app/services/human-augmentation-service')
var webview = require('./app/webview/webview')
var HumanHelpService = require('./app/dialogs/HumanAugmentation/human-help-service')
var couponScheduler = require('./app/services/couponsender/scheduler')
var StorageService = require('./app/services/StorageService')
var moment = require('moment')

let HA_CLIENT_ORGANIZATION = nconf.get("HUMAN_AUGMENTATION_SERVER_CLIENT_ORGANIZATION")

/**
 * Read text from Google sheet
 */
// TODO: Do without using global
var SpreadSheetUtil = require('./app/services/spreadsheet/SpreadSheetUtil')
let SHEET_ID = nconf.get('MESSAGE_TEXT_GOOGLE_SHEET_ID')

SpreadSheetUtil.getData(SHEET_ID, nconf.get('Messages_SHEET_NAME'), function (data) {
    global.sheetData = data

    console.log(nconf.get('Messages_SHEET_NAME'))

})
SpreadSheetUtil.getData(SHEET_ID, nconf.get('Personalization_SHEET_NAME'), function (data) {
    global.personalizationSheetData = data
    //console.log(nconf.get( 'Personalization_SHEET_NAME' ))
})
SpreadSheetUtil.getData(SHEET_ID, nconf.get('FAQ_SHEET_NAME'), function (data) {
    global.FAQS = data
    //console.log(nconf.get( 'FAQ_SHEET_NAME' ))
})
SpreadSheetUtil.getData(SHEET_ID, nconf.get('FAQ_CAT_SHEET_NAME'), function (data) {
    global.FAQS_CAT = data
    //console.log(nconf.get( 'FAQ_CAT_SHEET_NAME' ))
})
SpreadSheetUtil.getData(SHEET_ID, nconf.get('FAQ_VID_TEXT_SHEET_NAME'), function (data) {
    global.FAQ_VID_TEXT = data
    //console.log(nconf.get( 'FAQ_VID_TEXT_SHEET_NAME' ))
})

/**
 * Setup Restify Server
 */
var config = null
if (_.includes(['p'], nconf.get('conf').trim())) {
    config = {
        key: fs.readFileSync(nconf.get('key'), 'utf8'),
        certificate: fs.readFileSync(nconf.get('certificate'), 'utf8')
    }
}
var server = restify.createServer(config)
server.use(restify.bodyParser())
server.use(restify.queryParser())
// server.pre(restify.CORS({
//     origins: ['http://localhost:8080', 'http://bar.com', 'http://baz.com:8081'],   // defaults to ['*']
//     credentials: true,                 // defaults to false
//     // headers: ['x-foo']                 // sets expose-headers
// }));
server.listen(nconf.get('BOT_PORT') || 443, () => console.log('%s listening to %s', server.name, server.url))


/**
 * Initialize
 */
var connector = new builder.ChatConnector({
    appId: nconf.get('MICROSOFT_APP_ID'),
    appPassword: nconf.get('MICROSOFT_APP_PASSWORD'),
    gzipData: true
})

// Start Bot
if(nconf.get('LocalStorageService')){
    var bot = new builder.UniversalBot(connector, session => session.beginDialog('xpresso-bot:start')).set('storage', StorageService)
}else{
    var bot = new builder.UniversalBot(connector, session => session.beginDialog('xpresso-bot:start'))
}


/**
 * Set inactivity timouts and logging
 */
bot.on('receive', event => {

    //  console.log('RECIVE ', event)

    // Sender Action
    // var senderID
    // if ( event && event.sourceEvent && event.sourceEvent.sender && event.sourceEvent.sender.id ){
    //     senderID = event.sourceEvent.sender.id;
    // }
    // if ( senderID ) {
    //   ApiService.senderActions(senderID, nconf.get("FACEBOOK_PAGE_TOKEN"), true);
    // }

    // Set Feedback and Goodbye timer
    nconf.set('storageKey', 'userId')
    UserService.setBothTimer(event.address, bot)
    console.log('user is ', UserService.isUserActive(event.address))


})

// bot.on( 'send', event => {
//   console.log( 'Send event', event )
//   if( _.has( event, 'attachments[0]' ) ) {
//      console.log( 'content', event.attachments[0].content )
//   }
// })


/**
 * New FIRST time user added or user removed the bot from contacts
 */
bot.on('contactRelationUpdate', data => {


    console.log('--== contactRelationUpdate ==--')
    console.log(data)
    if (data.action === 'add') {
        // User added bot to contacts
        winston.log('info', 'USER ADDED BOT: ' + data.address.channelId + ' :: ' + data.address.user.id + ' :: ' + data.address.user.name)
        console.log('info', 'USER ADDED BOT: ' + data.address.channelId + ' :: ' + data.address.user.id + ' :: ' + data.address.user.name)
        bot.beginDialog(data.address, 'xpresso-bot:personalization-welcome', {data: data})
    }
    if (data.action === 'remove') {
        // User removed bot from contacts
        winston.log('info', 'USER REMOVED BOT: ' + data.address.channelId + ' :: ' + data.address.user.id + ' :: ' + data.address.user.name)
    }
})


// [ NOT NEEDED ] Conversation Update
// bot.on( 'conversationUpdate', data => console.log("C U:: ", data) )

// Global Error Handlers
bot.on('error', err => {
    console.log("ERROR-- ", err)
    winston.log('error', err)
})

bot.use({
    botbuilder: (session, next) => {
        session.error = err => {
            console.log("ERROR : ", err)
            winston.log('error', session.message)
        }
        next()
    }
})


/**
 * Init Bot
 */
bot.library(xpressoBot.createLibrary())
server.post('/api/messages', connector.listen())


/**
 * Webview handlers
 */
server.post('/webview/:slug', webview)

// function corsHandler(req, res, next) {
//   console.log('cors', req.headers.origin)
//     res.header('Access-Control-Allow-Origin', '*');
//     res.header( "Access-Control-Allow-Credentials", true );
//     res.header('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Encoding, Accept-Language, X-Requested-With, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token');
//     res.header( "Allow",     "GET, POST, PUT, DELETE, OPTIONS"       );
//     res.header( "Access-Control-Allow-Methods",     "GET, POST, PUT, DELETE, OPTIONS"       );
//     res.header('Access-Control-Allow-Methods', '*');
//     res.header('Access-Control-Expose-Headers', 'X-Api-Version, X-Request-Id, X-Response-Time');
//     res.header('Access-Control-Max-Age', '3600');
//     res.send(200);
//     return next();
// }
// server.opts('/.*/', corsHandler);

// server.opts('.*', (req, res, next) => {
//   res.header( "Access-Control-Allow-Credentials", true                                    );
//   // res.header( "Access-Control-Allow-Headers",     restify.CORS.ALLOW_HEADERS.join( ", " ) );
//   res.header('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Encoding, Accept-Language, X-Requested-With, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token');
//   res.header('Access-Control-Expose-Headers', 'Origin, Accept, Accept-Encoding, Accept-Language, X-Requested-With, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token');
//   res.header( "Access-Control-Allow-Methods",     "GET, POST, HEAD"       );
//   // res.header( "Access-Control-Allow-Methods",     "GET, POST, PUT, DELETE, OPTIONS"       );
//   res.header( "Access-Control-Allow-Origin",      req.headers.origin                      );
//   res.header( "Access-Control-Max-Age",           0                                       );
//   res.header( "Content-type",                     "text/plain charset=UTF-8"              );
//   res.header( "Content-length",                   0                                       );
//   res.send(204)
//   return next()
// })

/**
 * DASHBOT configuration
 */
/*const dashbotApiMap = {
    facebook: nconf.get('DASHBOT_API_KEY_FACEBOOK'),
    slack: nconf.get('DASHBOT_API_KEY_SLACK'),
    kik: nconf.get('DASHBOT_API_KEY_KIK'),
    skype: nconf.get('DASHBOT_API_KEY_SKYPE'),
    // webchat : process.env.DASHBOT_API_KEY_GENERIC  || 'FPngmmtL7e52fQXNq1g3WgVPzd5lA6vdhmP7jGU0',
}
let FACEBOOK_PAGE_TOKEN = nconf.get('FACEBOOK_PAGE_TOKEN')
const dashbot = require('dashbot')(dashbotApiMap).microsoft
dashbot.setFacebookToken(FACEBOOK_PAGE_TOKEN) // only needed for Facebook Bots
bot.use(dashbot)*/


/**
 * Middle ware run before recognizer
 * Send incoming to for Logging
 */
bot.use({

    botbuilder: (session, next) => {

        let NODE2 = Constants.NODE.NLP

        if (!session.userData[NODE2]) {
            session.userData[NODE2] = {}
            session.userData[NODE2].nlp_reset_flag = true
        }

        console.log("client_name1 in Multichannelchatbot is",session.userData.client_name)
        if(session.userData.client_name && !(session.userData.client_name.length>0)){

            session.userData.client_name = []

        }else if(!session.userData.client_name){
            session.userData.client_name = []
        }



        //if(!session.userData.Profile_gender){
        UserService.getUser(session.message.address)
            .then((userObj) => {

                console.log("userObj is ", JSON.stringify(userObj))

                console.log("Gender is ", userObj.gender)
                userObj.gender === 'men,women' ? "no_preference" : userObj.gender

                session.userData.Profile_gender = userObj.gender

                console.log("User saved gender is ", session.userData.Profile_gender)


                // }


                console.log("User saved gender is ",session.userData.Profile_gender )



                Constants.user_address.bot = session.message.address.bot

                // Send to HA backend
                //HumanAugmentationService.sendMessage( session.message );
                let personalisation_enabled = nconf.get('Personalisation')
                if (!personalisation_enabled) {
                    session.userData.guestMode = true
                }

                // Add session_id, message_number to session.userData if not present
                UserService.addSessionId(session.message.address, session)

                //console.log("message recives from user")
                //console.log("current seession data",session)

                //console.log("quickReplies recives from user")
                //console.log(JSON.stringify(session.userData.quickReplies))
                //console.log("message domain recives from user")
                //console.log(JSON.stringify(session.domain))
                //console.log("message library recives from user")
                //console.log(JSON.stringify(session.library))
                //console.log("message options recives from user")
                //console.log(JSON.stringify(session.options))
                //console.log(JSON.stringify(session.options.middleware))
                //console.log(JSON.stringify(session.options.dialogId))
                //console.log(JSON.stringify(session.options.dialogArgs))
                //console.log(JSON.stringify(session.message.address))
                //console.log(session.message.timestamp)
                //console.log(session.userData.facebookProfile.timezone)
                //console.log(JSON.stringify(session.message))
                //console.log(JSON.stringify(session.userData))

                let times
                //times = moment.utc(session.message.timestamp, moment.ISO_8601).format('YYYYMMDD HHmmss')
                //console.log(times)
                times = moment.utc(session.message.timestamp).format('YYYYMMDD HHmmss')
                console.log(times)
                //console.log(moment.utc(times).utcOffset("+05:30").format('HHmmss'))
                //console.log(moment.utc(session.message.timestamp, moment.ISO_8601).utcOffset("+05:30").format('YYYYMMDD HHmmss ZZ'))
                //console.log(moment.utc(session.message.timestamp, moment.ISO_8601).local().format('YYYYMMDD HHmmss ZZ'))

                session.userData.current_local_time = moment.utc(times).utcOffset(nconf.get("timezone")).format('HHmmss')
                session.userData.current_local_date = moment.utc(times).utcOffset(nconf.get("timezone")).format('dddd')
                console.log(session.userData.current_local_time)
                console.log(session.userData.current_local_date)


// console.log('session.message', session.message)
                // TODO: Handle image uploads from user
                /////////// Human Augmentation ///////////////////////////////////////////////////////////////////////////////////////////
                // Send incoming user message to to HA backend
                let message = {
                    session_id: session.userData.session_id,          // session id
                    sender_type: 'user',                               // one of [ 'user', 'agent', 'bot' ]
                    user_psid: session.message.address.user.id,      // If message sent by user, null otherwise
                    user_name: session.message.address.user.name,    // User name
                    agent_name: null,
                    profile_pic_url: session.userData.facebookProfile && session.userData.facebookProfile.profile_pic ? session.userData.facebookProfile.profile_pic : null,
                    agent_psid: null,                                 // If message sent by agent, null otherwise
                    bot_id: null,                                 // If message sent by bot, as given by MS Bot FW, null otherwise
                    channel_id: session.message.source,               // channel id
                    client_organization: HA_CLIENT_ORGANIZATION,               // Client org id
                    msg_timestamp: session.message.timestamp || new Date().toISOString(),          // ISO 8601 with timezone, Z format
                    text: session.message.text,                 // Text of text or quick reply message
                    carousel: null,
                    quick_reply: null
                }

                // console.log( 'msg -> ', message )
                // Sometimes Bot revieves misterious empty messages
                // with '' text and no timestammp

                let user_type
                if (_.has(session, 'userData.status')) {
                    user_type = session.userData.status
                }

                if (user_type) {

                    if (user_type == "user") {

                        /*console.log("message recives from user")
                         console.log("quickReplies recives from user")
                         console.log(JSON.stringify(session.userData.quickReplies))
                         console.log("message domain recives from user")
                         console.log(JSON.stringify(session.domain))
                         console.log("message library recives from user")
                         console.log(JSON.stringify(session.library))
                         console.log("message options recives from user")
                         console.log(JSON.stringify(session.options))*/

                        console.log("message recives from user")


                        if (HumanHelpService.has_user(session.message.address.user.id)) {

                            let session2 = HumanHelpService.return_user(session.message.address.user.id)
                            session.userData.user_wait_queue = session2.userData.user_wait_queue
                            session.userData.user_engaged_queue = session2.userData.user_engaged_queue
                            session.userData.status = session2.userData.status
                            session.userData.quickReplies = session2.userData.quickReplies
                            HumanHelpService.Add_user(session.message.address.user.id, session)

                            //console.log(JSON.stringify(session.userData.quickReplies))
                            //console.log(JSON.stringify(session.message))

                            if ((session.userData.user_engaged_queue) && (session.userData.user_engaged_queue.flag)) {

                                message.agent_psid = session.userData.user_engaged_queue.id
                                message.agent_name = session.userData.user_engaged_queue.name

                            }

                        } else {

                            session.userData.user_wait_queue = null
                            session.userData.user_engaged_queue = null
                            session.userData.status = ""

                        }

                        console.log("Session id for user is :- ", session.userData.session_id)


                    } else if (user_type == "agent") {

                        console.log("message recives from agent")

                        if (HumanHelpService.has_user(session.message.address.user.id)) {

                            console.log("present agent")

                            let session2 = HumanHelpService.return_user(session.message.address.user.id)

                            //console.log("Before data",JSON.stringify(session2.userData))
                            session.userData.agent_wait_queue = session2.userData.agent_wait_queue
                            session.userData.user_engaged_queue = session2.userData.user_engaged_queue
                            session.userData.status = session2.userData.status
                            if (session2.userData.session_id) {
                                session.userData.session_id = session2.userData.session_id
                            }


                            HumanHelpService.Add_user(session.message.address.user.id, session)
                            //console.log("updated  data",JSON.stringify(session.userData))
                            //console.log(JSON.stringify(session.message))


                            if ((session.userData.user_engaged_queue) && (session.userData.user_engaged_queue.flag)) {

                                message.sender_type = "agent"
                                message.user_psid = session.userData.user_engaged_queue.id
                                message.agent_psid = session.message.address.user.id

                                message.user_name = session.userData.user_engaged_queue.name
                                message.agent_name = session.message.address.user.name

                            } else {

                                message.sender_type = "agent"
                                message.user_psid = null
                                message.agent_psid = session.message.address.user.id
                                message.user_name = null
                                message.agent_name = session.message.address.user.name

                            }
                            message.session_id = session.userData.session_id


                        } else {

                            console.log("here is enconsistency")

                            session.userData.agent_wait_queue = null
                            session.userData.user_engaged_queue = null
                            session.userData.status = null

                        }

                        console.log("Session id for agent is :- ", session.userData.session_id)

                    }


                }

                if (message.text) {
                    HumanAugmentationService.sendMessage(message)
                }


                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                // Add analytics namespace
                session.userData.analytics = {
                    in: {
                        type: 'message',
                        agent: 'botbuilder',
                        source: session.message.source,
                        address: session.message.address,
                        text: session.message.text,

                        timestamp: session.message.timestamp,
                        channel_id: session.message.address.channelId,

                        profile_pic_link: session.userData.facebookProfile && session.userData.facebookProfile.profile_pic ? session.userData.facebookProfile.profile_pic : '',
                        locale: session.userData.facebookProfile && session.userData.facebookProfile.locale ? session.userData.facebookProfile.locale : '',
                        timezone: session.userData.facebookProfile && session.userData.facebookProfile.timezone ? session.userData.facebookProfile.timezone : '',
                        gender: session.userData.facebookProfile && session.userData.facebookProfile.gender ? session.userData.facebookProfile.gender : '',
                        is_payment_enabled: '',
                        marked_spam: '',
                        has_blocked: '',

                        session_id: '',
                        message_number: '',
                        message_type: 'IN',
                        message_chat: session.message.text,
                        message_type_flag: '',                        // Populated by recognizers
                        visual_search: '',                        // Populated by image recognizers
                        product_response_list: '',
                        nlp_query_response: '',
                        vision_file_link: '',
                        vision_engine_response: '',
                        quick_reply_buttons: '',
                        click_item_sku: '',                        // User clicked on 'View Details' of an item
                    },
                    out: {
                        type: 'message',
                        agent: 'botbuilder',
                        source: session.message.source,
                        address: session.message.address,
                        text: '',

                        timestamp: '',
                        channel_id: session.message.address.channelId,

                        profile_pic_link: '',
                        locale: '',
                        timezone: '',
                        gender: '',
                        is_payment_enabled: '',
                        marked_spam: '',
                        has_blocked: '',

                        session_id: '',
                        message_number: '',
                        message_type: 'OB',
                        message_chat: '',
                        message_type_flag: '',    // Populated by recognizers
                        visual_search: '',    // Populated by dialogs
                        product_response_list: '',
                        nlp_query_response: '',
                        vision_file_link: '',
                        vision_engine_response: '',
                        quick_reply_buttons: '',
                        click_item_sku: '',
                    }
                }

                // Send to loggly
                winston.log('info', session.message)



                /**
                 * Small hack to get userprofile from Facebook
                 * TODO: Think of a better way to do this
                 */
                // Refresh every 24 hours
                if (!session.userData.facebookProfile ||
                    new Date() - new Date(session.userData.facebookProfile.profile_last_refreshed) > 24 * 60 * 1000) {

                    UserService.getFacebookProfile(session.message.address.user.id)
                        .then(profile => {
                            profile.profile_last_refreshed = new Date().toISOString()
                            session.userData.facebookProfile = profile
                            console.log('FB Profile =>', session.userData.facebookProfile)
                            next()
                        })
                        .catch(err => {
                            // console.log( 'fb profile error', err )
                            let profile = {}
                            profile.profile_last_refreshed = new Date().toISOString()
                            session.userData.facebookProfile = profile
                            next()
                        })

                } else {
                    // User's FB profile already availabe
                    next()
                }
            })
            .catch(err => {
                console.log("Some error happened while getting gender in MC.js", err)

            })


        // next()

    },
    // send : function(event,next) {
    //   console.log('---<><><', event)
    //   next()
    // }
})

if (nconf.get('Persistent_menu_update')) {
    UtilService.createPersistentMenu()
}
if (nconf.get('Get_started_update')) {
    UtilService.createGetStartedButton()
}
if (nconf.get('Coupon_Scheduler')) {
    couponScheduler.couponScheduler(bot)
}

// Secret Per-User Emergency Reset (sometimes bot gets stuck due to changed code)
bot.endConversationAction('goodbye_reset', 'Goodbye :)', {matches: /SWORD_FISH/})   // there is another dialog named 'goodbye'
// Secret Global Emergency Reset
// Anytime the major version is incremented any existing conversations will be restarted.
bot.use(builder.Middleware.dialogVersion({version: 1.2, resetCommand: /POINT_BREAK/}))

