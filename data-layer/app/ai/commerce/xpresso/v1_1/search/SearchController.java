package ai.commerce.xpresso.v1_1.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ai.commerce.xpresso.v1_1.amazon.cloudsearch.*;
import ai.commerce.xpresso.v1_1.amazon.cloudsearch.AmazonCloudSearchQuery;
import ai.commerce.xpresso.v1_1.amazon.cloudsearch.Facet;
import ai.commerce.xpresso.v1_1.amazon.cloudsearch.XCConfig;
import ai.commerce.xpresso.v1_1.search.SearchItem;
import ai.commerce.xpresso.v1_1.search.SearchUtils;
import ai.commerce.xpresso.v1_1.utils.XCEntity;
import ai.commerce.xpresso.v1_1.utils.XCLogger;
import ai.commerce.xpresso.v1_1.amazon.cloudsearch.*;
import org.slf4j.Logger;

/**
 * @author Sudhanshu Kumar Sep 15, 2016
 *
 */

public class SearchController {

	public static Logger logger = ai.commerce.xpresso.v1_1.utils.XCLogger.getSpLogger();


	/**
	 * The method helps to build a query for CloudSearch from the search attributes
	 * @param searchType
	 * @return
	 */
	public ai.commerce.xpresso.v1_1.amazon.cloudsearch.AmazonCloudSearchQuery buildQuery(String domain,
											 String searchType,
											 boolean isSuggested,
											 String cursor,
                                             boolean isFaceted,
											 String... customReturnAttributes
                                             ) {
		ai.commerce.xpresso.v1_1.amazon.cloudsearch.AmazonCloudSearchQuery query = new ai.commerce.xpresso.v1_1.amazon.cloudsearch.AmazonCloudSearchQuery();
		query.setQueryParser(searchType);
//		query.setStart(0);  Doi Not do... we are using initial
		query.setSize(ai.commerce.xpresso.v1_1.amazon.cloudsearch.XCConfig.MAX_SEARCH_HITS);
		query.setDefaultOperator("or");
		query.cursor = cursor;


		if (customReturnAttributes.length > 0 && customReturnAttributes[0] != null) {
			query.setReturnFields(customReturnAttributes);
		} else {
			query.setReturnFields(ai.commerce.xpresso.v1_1.search.SearchUtils.getReturnAttributes(domain));
		}

        if( isFaceted){
            List<ai.commerce.xpresso.v1_1.amazon.cloudsearch.Facet>  facetList = new ArrayList<>();

            for( int i = 0; i < customReturnAttributes.length; ++i){
                ai.commerce.xpresso.v1_1.amazon.cloudsearch.Facet facet = new ai.commerce.xpresso.v1_1.amazon.cloudsearch.Facet();
                facet.field = customReturnAttributes[i];
                facet.size = 100;
                facetList.add(facet);
            }
            query.facets = facetList;
        }
		return query;
	}
	/*Removes duplicates from result set*/
	public List<ai.commerce.xpresso.v1_1.search.SearchItem> getUniqueResultSet(List<ai.commerce.xpresso.v1_1.search.SearchItem> resultSet) {
		Map<String, ai.commerce.xpresso.v1_1.search.SearchItem> uniqueResultMap = new HashMap<String, ai.commerce.xpresso.v1_1.search.SearchItem>();
		for (ai.commerce.xpresso.v1_1.search.SearchItem item : resultSet) {
			if (item.getAttributeValue("xc_sku") != null) {
				uniqueResultMap.put(item.getAttributeValue("xc_sku"), item);
				//				logger.info(item.getAttributeValue("sku") + " " + item);
			}
		}
		List<ai.commerce.xpresso.v1_1.search.SearchItem> list = new ArrayList<ai.commerce.xpresso.v1_1.search.SearchItem>(uniqueResultMap.values());
		if (list.isEmpty()) {
			list = resultSet;
		}
		return list;
	}
}
