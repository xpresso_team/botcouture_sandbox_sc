package ai.commerce.xpresso.v1.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ai.commerce.xpresso.v1.amazon.cloudsearch.*;
import ai.commerce.xpresso.v1.utils.XCEntity;
import ai.commerce.xpresso.v1.utils.XCLogger;
import org.slf4j.Logger;

/**
 * @author Sudhanshu Kumar Sep 15, 2016
 *
 */

public class SearchController {

	public static Logger logger = XCLogger.getSpLogger();


	/**
	 * The method helps to build a query for CloudSearch from the search attributes
	 * @param searchType
	 * @return
	 */
	public AmazonCloudSearchQuery buildQuery(String domain,
											 String searchType,
											 boolean isSuggested,
											 String cursor,
                                             boolean isFaceted,
											 String... customReturnAttributes
                                             ) {
		AmazonCloudSearchQuery query = new AmazonCloudSearchQuery();
		query.setQueryParser(searchType);
//		query.setStart(0);  Do Not do... we are using initial
		query.setSize(XCConfig.MAX_SEARCH_HITS);
		query.setDefaultOperator("or");
		query.cursor = cursor;


		if (customReturnAttributes.length > 0 && customReturnAttributes[0] != null) {
			query.setReturnFields(customReturnAttributes);
		} else {
			query.setReturnFields(SearchUtils.getReturnAttributes(domain));
		}

        if( isFaceted){
            List<Facet>  facetList = new ArrayList<>();

            for( int i = 0; i < customReturnAttributes.length; ++i){
                Facet facet = new Facet();
                facet.field = customReturnAttributes[i];
                facet.size = 100;
                facetList.add(facet);
            }
            query.facets = facetList;
        }
		return query;
	}
	/*Removes duplicates from result set*/
	public List<SearchItem> getUniqueResultSet(List<SearchItem> resultSet) {
		Map<String, SearchItem> uniqueResultMap = new HashMap<String, SearchItem>();
		for (SearchItem item : resultSet) {
			if (item.getAttributeValue("xc_sku") != null) {
				uniqueResultMap.put(item.getAttributeValue("xc_sku"), item);
				//				logger.info(item.getAttributeValue("sku") + " " + item);
			}
		}
		List<SearchItem> list = new ArrayList<SearchItem>(uniqueResultMap.values());
		if (list.isEmpty()) {
			list = resultSet;
		}
		return list;
	}
}
