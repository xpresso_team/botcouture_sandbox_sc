export RASA_AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
echo $RASA_AWS_SECRET_ACCESS_KEY
export RASA_AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
echo $RASA_AWS_ACCESS_KEY_ID
export RASA_AWS_REGION=$AWS_REGION
echo $RASA_AWS_REGION
export RASA_BUCKET_NAME=$BUCKET_NAME
echo $RASA_BUCKET_NAME
if [ "$1" == "train" ]
then
    aws s3 cp s3://xpresso.ai.nlp.models/$2 ./data/
    aws s3 cp s3://xpresso.ai.nlp.models/$3 ./
    python -m rasa_nlu.train -c $3
elif [ "$1" == "run" ]
then
    aws s3 cp s3://xpresso.ai.nlp.models/$2 ./
    python -m rasa_nlu.server -c $2
fi
