#! /bin/bash
## This script is used to build the project.
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user


# Build the dependencies
cd ${ROOT_FOLDER}
mvn clean process-classes package

curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
pip install flask tornado flask_restful flask_cors
pip install 'elasticsearch==6.3.1'
