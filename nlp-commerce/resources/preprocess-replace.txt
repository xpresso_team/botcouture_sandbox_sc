que	queue
did not	didn't
dnt	don't
ciggerates	cigarettes
didnot	didn't
didnt	didn't
TMobile	Tmobile
T Mobile	Tmobile
T-Mobile	Tmobile
iphone	iPhone
IPHONE6	iPhone6
iphone6 plus	iPhone6 Plus
iphone 6 plus	iPhone6 Plus
iphone-6	iPhone6
iPhone-6	iPhone6
i-phone6	iPhone6
i-phone	iPhone
web page	webpage
web site	website
WOW	Wow
cant	can't
on line	online
bank of america	Bank of America
oh no,	oh no!
Any one	anyone
Speaker phone	speakerphone
Head phone	headphone
Head set	headset
Hands free	handsfree
Im	I'm
You're	you are
Customerservice	customer service
Customer-service	customer service
Help line	helpline
Tollfree	toll free
Toll-free	toll free
Paper less	paperless
Paper-less	paperless
Digi cam	digicam
Safe house	safehouse
Alot	a lot
E mail	email
Multi task	multi-task
Best-buy	Best buy
Middle east	middle-east
Giveaway	give away
I pod	Ipod
Juke box	Jukebox
Home work	homework
Fuel efficient	Fuel Efficient
Every day	Everyday
War-fare	warfare
Ecommerce	e-commerce
I Tunes	iTunes
I-Tunes	iTunes
Hyper active	hyperactive
Sign-up	sign up
I pad	iPad
i-pad	iPad
ur	your
gud	good
gudwill	good will
problm	problem
ppl	people
ppls	people's
abt	about
pls	please
plz	please
3rd class	third class
dont	don't
rep	representative
check in	check-in
over drafted	overdrafted
over draft	overdraft
wanna	want to
wanaa	want to
smart watch	smartwatch
yrs	years
kinda	kind of
gimme	give me
aint	have not
gonna	going to
gotta	got to
lemme	let me
grt	great
ty	thank you
RT retweet
tshirt	t-shirt
t shirt	t-shirt
per cent	percent