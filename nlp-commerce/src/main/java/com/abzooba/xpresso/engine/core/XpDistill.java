/**
 *
 */
package com.abzooba.xpresso.engine.core;

import java.util.Set;

import com.abzooba.xpresso.expressions.sentiment.SentimentFunctions;
import com.abzooba.xpresso.expressions.sentiment.XpSentiment;
import com.abzooba.xpresso.expressions.statementtype.StatementTypeLexicon;
import com.abzooba.xpresso.machinelearning.XpFeatureVector;

/**
 * @author Koustuv Saha
 * 24-Mar-2014 2:45:51 pm
 * XpressoV2 Triple
 */
public class XpDistill {

    public String reviewID;
    public String review;
    public String reviewSnippet;
    public String indicativeSnippet;
    public String subject;
    public String entity;
    public String aspectCategory;
    public String opinion;
    public String opinionSentiment;
    public String snippetSentiment;
    public String statementType;
    public Set<String> emotionSet;
    XpSentenceResources xsrc;
    XpFeatureVector xpf;

    protected void setEmotionSet(Set<String> emotionSet) {
        this.emotionSet = emotionSet;
    }

    public void mismatchFix() {
        //		if (SentimentLexicon.POSITIVE_SENTIMENT.equals(this.opinionSentiment) && StatementTypeLexicon.COMPLAINT_STATEMENT.equals(this.statementType)) {
        //			this.statementType = StatementTypeLexicon.OPINION_STATEMENT;
        //		} else if (SentimentLexicon.NEGATIVE_SENTIMENT.equals(this.opinionSentiment) && StatementTypeLexicon.ADVOCACY_STATEMENT.equals(this.statementType)) {
        //			this.statementType = StatementTypeLexicon.OPINION_STATEMENT;
        //		} else if (SentimentLexicon.NEUTRAL_SENTIMENT.equals(this.opinionSentiment)) {
        //			this.statementType = StatementTypeLexicon.OPINION_STATEMENT;
        //		}

        //		System.out.println(this.toString());
        //		System.out.println(this.toString());

        if (xpf != null && xpf.isNsubjShiftPronounSubjectFeature()) {
            this.opinionSentiment = SentimentFunctions.switchSentiment(this.opinionSentiment);
            //			if (XpActivator.PW != null) {
            //				XpActivator.PW.println(this.toString());
            //			}
        }

        if ((xpf == null) || (xpf != null && !xpf.isSentimentPrimarilyStanford())) {
            if (XpSentiment.POSITIVE_SENTIMENT.equals(this.opinionSentiment)) {
                if (StatementTypeLexicon.SUGGESTION_STATEMENT.equals(this.statementType) && reviewSnippet.length() < 60) {
                    if (xpf.getStanfordSentimentStr() != null && !xpf.getStanfordSentimentStr().equals(XpSentiment.POSITIVE_SENTIMENT)) {
                        this.opinionSentiment = XpSentiment.NEUTRAL_SENTIMENT;
                    }
                } else if (!StatementTypeLexicon.ADVOCACY_STATEMENT.equals(this.statementType)) {
                    this.statementType = StatementTypeLexicon.ADVOCACY_STATEMENT;
                }

                //		}&& !StatementTypeLexicon.ADVOCACY_STATEMENT.equals(this.statementType)) {
                //			this.statementType = StatementTypeLexicon.ADVOCACY_STATEMENT;
            }
            //			else if (SentimentLexicon.NEGATIVE_SENTIMENT.equals(this.opinionSentiment) && (!StatementTypeLexicon.COMPLAINT_STATEMENT.equals(this.statementType) && !StatementTypeLexicon.SUGGESTION_STATEMENT.equals(this.statementType))) {
            //				this.statementType = StatementTypeLexicon.COMPLAINT_STATEMENT;
            //			}
            //			else if (SentimentLexicon.NEUTRAL_SENTIMENT.equals(this.opinionSentiment) && (!StatementTypeLexicon.SUGGESTION_STATEMENT.equals(this.statementType))) {
            //				this.statementType = StatementTypeLexicon.OPINION_STATEMENT;
            //			}
        } else {
            if (XpSentiment.POSITIVE_SENTIMENT.equals(this.opinionSentiment)) {
                if (StatementTypeLexicon.SUGGESTION_STATEMENT.equals(this.statementType) && reviewSnippet.length() < 60) {
                    this.opinionSentiment = XpSentiment.NEUTRAL_SENTIMENT;
                } else if (StatementTypeLexicon.COMPLAINT_STATEMENT.equals(this.statementType)) {
                    this.statementType = StatementTypeLexicon.OPINION_STATEMENT;
                }
            }
        }
        if (XpSentiment.NEGATIVE_SENTIMENT.equals(this.opinionSentiment) && (!StatementTypeLexicon.COMPLAINT_STATEMENT.equals(this.statementType) && !StatementTypeLexicon.SUGGESTION_STATEMENT.equals(this.statementType))) {
            this.statementType = StatementTypeLexicon.COMPLAINT_STATEMENT;
        } else if (XpSentiment.NEUTRAL_SENTIMENT.equals(this.opinionSentiment) && (!StatementTypeLexicon.SUGGESTION_STATEMENT.equals(this.statementType))) {
            this.statementType = StatementTypeLexicon.OPINION_STATEMENT;
        }

        //if (xpf != null && xpf.isQuestionFeature() && !xpf.isSubjectFirstPerson()) {
        //		if (xpf != null && !(xpf.isSubjectFirstPerson() || !xpf.IsSubjectSecondThirdPerson())) {
        //			//			System.out.println("\nQuestion Feature: True\n");
        //			/*Isn't it delicious -> Should not be made negative*/
        //			if (!this.statementType.equals(StatementTypeLexicon.COMPLAINT_STATEMENT) || xpf.isQuestionPositiveFeature()) {
        //				this.opinionSentiment = XpSentiment.NEUTRAL_SENTIMENT;
        //				this.statementType = StatementTypeLexicon.OPINION_STATEMENT;
        //				//				xpf.setIntentionFeature(false, null, false);
        //			}
        //
        //			//			xpf.setIntentionFeature(false, null, false);
        //		}
        //		if (this.opinionSentiment.equals(XpSentiment.NEGATIVE_SENTIMENT)) {
        //			xpf.setIntentionFeature(false, null, false);
        //		}
        //		System.out.println("Final: " + this.toString());

    }

    public XpDistill(String reviewID, String review, String reviewSnippet, String subject, String entity, String entityClass, String opinion, String opinionSentiment, String snippetSentiment, String statementType, XpSentenceResources xsrc) {

        this.reviewID = reviewID;
        this.review = review;
        this.reviewSnippet = reviewSnippet;
        this.subject = subject;
        this.entity = entity;
        this.aspectCategory = entityClass;
        this.opinion = opinion;
        this.opinionSentiment = opinionSentiment;
        this.snippetSentiment = snippetSentiment;
        this.statementType = statementType;
        //		this.emotionSet = emotionSet;
        this.xsrc = xsrc;
        this.xpf = xsrc.getXpf();
        //		this.mismatchFix();

        //		this.indicativeSnippet = this.reviewSnippet;
        if (this.aspectCategory.equals(XpExpression.OVERALL_STR)) {
            this.indicativeSnippet = this.opinion;
        } else {
            this.indicativeSnippet = this.reviewSnippet;
        }

    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        //		return (reviewID + "\t" + review + "\t" + reviewSnippet + "\t" + subject + "\t" + entity + "\t" + entityClass + "\t" + opinion + "\t" + opinionSentiment + "\t" + snippetSentiment + "\t" + statementType + "\t" + (xpf != null ? xpf.isIntentionFeature() : false));
        return (reviewID + "\t" + review + "\t" + indicativeSnippet + "\t" + subject + "\t" + entity + "\t" + aspectCategory + "\t" + opinionSentiment + "\t" + statementType + "\t" + (xpf != null ? xpf.isIntentionFeature() : false));
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((entity == null) ? 0 : entity.hashCode());
        result = prime * result + ((aspectCategory == null) ? 0 : aspectCategory.hashCode());
        result = prime * result + ((opinion == null) ? 0 : opinion.hashCode());
        result = prime * result + ((opinionSentiment == null) ? 0 : opinionSentiment.hashCode());
        result = prime * result + ((review == null) ? 0 : review.hashCode());
        result = prime * result + ((reviewID == null) ? 0 : reviewID.hashCode());
        result = prime * result + ((reviewSnippet == null) ? 0 : reviewSnippet.hashCode());
        result = prime * result + ((snippetSentiment == null) ? 0 : snippetSentiment.hashCode());
        result = prime * result + ((statementType == null) ? 0 : statementType.hashCode());
        result = prime * result + ((subject == null) ? 0 : subject.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        XpDistill other = (XpDistill) obj;
        if (entity == null) {
            if (other.entity != null)
                return false;
        } else if (!entity.equals(other.entity))
            return false;
        if (aspectCategory == null) {
            if (other.aspectCategory != null)
                return false;
        } else if (!aspectCategory.equals(other.aspectCategory))
            return false;
        if (opinion == null) {
            if (other.opinion != null)
                return false;
        } else if (!opinion.equals(other.opinion))
            return false;
        if (opinionSentiment == null) {
            if (other.opinionSentiment != null)
                return false;
        } else if (!opinionSentiment.equals(other.opinionSentiment))
            return false;
        if (review == null) {
            if (other.review != null)
                return false;
        } else if (!review.equals(other.review))
            return false;
        if (reviewID == null) {
            if (other.reviewID != null)
                return false;
        } else if (!reviewID.equals(other.reviewID))
            return false;
        if (reviewSnippet == null) {
            if (other.reviewSnippet != null)
                return false;
        } else if (!reviewSnippet.equals(other.reviewSnippet))
            return false;
        if (snippetSentiment == null) {
            if (other.snippetSentiment != null)
                return false;
        } else if (!snippetSentiment.equals(other.snippetSentiment))
            return false;
        if (statementType == null) {
            if (other.statementType != null)
                return false;
        } else if (!statementType.equals(other.statementType))
            return false;
        if (subject == null) {
            if (other.subject != null)
                return false;
        } else if (!subject.equals(other.subject))
            return false;
        return true;
    }

}