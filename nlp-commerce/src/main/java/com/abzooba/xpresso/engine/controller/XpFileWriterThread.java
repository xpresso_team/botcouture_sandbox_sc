/**
 *
 */
package  com.abzooba.xpresso.engine.controller;

import java.util.List;

import com.abzooba.xpresso.utils.FileIO;

/**
 * @author Koustuv Saha
 * Feb 11, 2015 12:38:41 PM
 * XpressoV2.7  XpFileWriterThread
 */
public class XpFileWriterThread extends Thread {

    String strText = null;
    String fileName = null;
    List<?> strLines = null;

    public XpFileWriterThread(String strText, String fileName) {
        this.strText = strText;
        this.fileName = fileName;
    }

    public XpFileWriterThread(List<?> strLines, String fileName) {
        this.strLines = strLines;
        this.fileName = fileName;
    }

    public void run() {
        if (fileName != null) {
            if (strText != null) {
                FileIO.write_file(strText, fileName, true);
            } else if (strLines != null) {
                FileIO.write_file(strLines, fileName, true);
            }
        }

        //		if (strText != null && fileName != null) {
        //			FileIO.write_file(strText, fileName, true);
        //		}
    }
}