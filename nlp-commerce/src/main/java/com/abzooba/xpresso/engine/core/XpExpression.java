/**
 *
 */
package com.abzooba.xpresso.engine.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.abzooba.xpresso.aspect.domainontology.OntologyUtils;
import com.abzooba.xpresso.aspect.domainontology.XpOntology;
import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Annotations;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.expressions.emotion.EmotionVector;
import com.abzooba.xpresso.expressions.sentiment.SWNsentiment;
import com.abzooba.xpresso.expressions.sentiment.SentimentEngine;
import com.abzooba.xpresso.expressions.sentiment.SentimentFunctions;
import com.abzooba.xpresso.expressions.sentiment.XpSentiment;
import com.abzooba.xpresso.expressions.statementtype.StatementTypeLexicon;
import com.abzooba.xpresso.machinelearning.XpFeatureVector;
import com.abzooba.xpresso.realtime.TrendInfoManipulation;
import com.abzooba.xpresso.sdgraphtraversal.ConstituencyTree;
import com.abzooba.xpresso.sdgraphtraversal.StanfordDependencyEdges;
import com.abzooba.xpresso.sdgraphtraversal.StanfordDependencyGraph;
import com.abzooba.xpresso.textanalytics.CoreNLPController;
import com.abzooba.xpresso.textanalytics.PosTags;
import com.abzooba.xpresso.textanalytics.ProcessString;
import com.abzooba.xpresso.textanalytics.VocabularyTests;

import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;

/**
 * @author Koustuv Saha 
 * 19-Aug-2014 10:08:14 am 
 * XpressoV2.0.1 XpLanguageProcessor
 */
public class XpExpression extends XpText {

	protected static final String OVERALL_STR = "Overall";
	protected static final String COMPETITION_CONCEPT = "COMPETITION";

	//	protected static final Set<XpDistill> xpDistillSet = new HashSet<XpDistill>();

	protected Map<String, XpFeatureVector> snippetFeatureMap;
	protected List<XpDistill> reviewTriples;
	protected Map<String, Set<String>> snippetEmotionMap;
	protected Map<String, Integer> entityMap;
	//	protected static Set<String> COLOUR_HASH = new HashSet<String>();

	protected String reviewID;
	protected String domainName;
	protected String subject;
	protected boolean isHistoricalDemo;
	protected boolean isDomainRelevantReview;
	protected Annotations annotation;

	//	protected static Pattern validWordPattern;

	public static void init() {
		//		COLOUR_HASH.add("red");
		//		COLOUR_HASH.add("white");
		//		COLOUR_HASH.add("black");
		//		COLOUR_HASH.add("blue");
		//		COLOUR_HASH.add("white");
		//		COLOUR_HASH.add("pink");
		//		COLOUR_HASH.add("orange");
		//		COLOUR_HASH.add("yellow");
		//		validWordPattern = Pattern.compile("[a-zA-Z]+");
	}

	public String getDomainName() {
		return this.domainName;
	}

	public String getSubject() {
		return this.subject;
	}

	public Map<String, Integer> getEntityMap() {
		if (this.entityMap == null) {
			this.processPipelineForEntities(true);
		}
		return this.entityMap;
	}

	protected void construct(Annotations annotation, String reviewID, String domainName, String subject, boolean isHistoricalDemo) {
		this.annotation = annotation;
		this.reviewID = reviewID;
		this.domainName = domainName;
		this.subject = subject;
		this.isHistoricalDemo = isHistoricalDemo;
		this.snippetFeatureMap = new LinkedHashMap<String, XpFeatureVector>();
		this.reviewTriples = new ArrayList<XpDistill>();
		this.snippetEmotionMap = new HashMap<String, Set<String>>();
	}

	/**
	 * @param isHistoricalDemo
	 * @param reviewID
	 * @param domainName
	 * @param subject
	 * @param reviewStr
	 */
	public XpExpression(Languages language, Annotations annotation, boolean isHistoricalDemo, String reviewID, String domainName, String subject, String reviewStr, boolean isMicro) {
		super(language, reviewStr, isMicro);
		this.construct(annotation, reviewID, domainName, subject, isHistoricalDemo);
	}

	//	public XpExpression(boolean isHistoricalDemo, String reviewID, String domainName, String subject, String reviewStr, Languages language) {
	//		super(reviewStr, language);
	//		switch (language) {
	//			case EN:
	//				this.construct(isHistoricalDemo, reviewID, domainName, subject);
	//			case SP:
	//				this.construct(isHistoricalDemo, reviewID, null, subject);
	//		}
	//
	//	}

	//	public XpExpression(boolean isHistoricalDemo, String reviewID, String domainName, String subject, String reviewStr, boolean isMicro, Languages language) {
	//		super(reviewStr, isMicro, language);
	//		switch (language) {
	//		case EN:
	//			this.construct(isHistoricalDemo, reviewID, domainName, subject);
	//		case SP:
	//			this.construct(isHistoricalDemo, reviewID, null, subject);
	//		}
	//	}

	/**
	 * @param reviewStr
	 */
	public XpExpression(String reviewStr) {
		// TODO Auto-generated constructor stub
		super(reviewStr);
	}

	/**
	 * @param language
	 * @param reviewStr
	 * @param equals
	 */
	public XpExpression(Languages language, String reviewStr, boolean isMicro) {
		// TODO Auto-generated constructor stub
		super(language, reviewStr, isMicro);
	}

	/**
	 * @param reviewStr
	 * @param isMicro
	 */
	public XpExpression(String reviewStr, boolean isMicro) {
		super(reviewStr, isMicro);
	}

	public Map<String, XpFeatureVector> getSnippetFeatureMap() {
		return snippetFeatureMap;
	}

	protected JSONObject getExpression() {
		List<XpDistill> reviewTriples = this.processPipeline();
		Map<String, JSONObject> netMap = new LinkedHashMap<String, JSONObject>();
		try {

			//			int i = 0;
			for (XpDistill t : reviewTriples) {
				//				if (XpActivator.PW != null && xpDistillSet.contains(t)) {
				//					XpActivator.PW.println(t.toString());
				//				} else {
				//					xpDistillSet.add(t);
				//				}

				// System.out.println(t.toString());
				String snippet = t.reviewSnippet;
				JSONObject distillJSON = netMap.get(snippet);
				JSONArray aboutMapArr;
				if (distillJSON == null) {
					distillJSON = new JSONObject();
					aboutMapArr = new JSONArray();
					//					JSONArray aboutArr = new JSONArray();
					//					distillJSON.put("About", aboutMapArr);
				} else {
					aboutMapArr = distillJSON.getJSONArray("About");
				}

				JSONObject aboutObj = new JSONObject();

				if (t.aspectCategory.equals(OVERALL_STR)) {
					//					indicative_snippet = t.opinion;
					aboutObj.put("Emotion", t.emotionSet);
				}
				//				else {
				//					indicative_snippet = snippet;
				//				}

				//				if (this.isHistoricalDemo && !isHistoricalDemoValidSnippet(indicative_snippet, t)) {
				//					continue;
				//				}

				//				JSONArray aboutMapArr = distillJSON.getJSONArray("About");
				//				if (aboutMapArr == null) {
				//					aboutMapArr = new JSONArray();
				//				}

				String entityStr = t.entity.replaceAll("[^\\w\\s\\-]", "");
				aboutObj.put("Entity", entityStr);
				aboutObj.put("Aspect", t.aspectCategory);
				aboutObj.put("Sentiment", t.opinionSentiment);
				aboutObj.put("StatementType", t.statementType);
				aboutObj.put("Indicative Snippet", t.indicativeSnippet);
				aboutMapArr.put(aboutObj);
				distillJSON.put("About", aboutMapArr);

				JSONObject intentObj = new JSONObject();
				Map<String, String> namedEntitiesMap = null;
				Set<String> intentObjectSet = null;
				if (t.xpf != null) {
					if (t.xpf.isIntentionFeature()) {
						intentObj.put("Intent Classification", true);
					} else {
						intentObj.put("Intent Classification", false);
					}
					intentObjectSet = t.xpf.getIntentionObject();

					if (t.xsrc != null) {
						namedEntitiesMap = t.xsrc.getNerMap();
					}
				}
				if (intentObjectSet == null) {
					intentObjectSet = new HashSet<String>();
					intentObj.put("Intent Object", intentObjectSet);
				} else {
					intentObj.put("Intent Object", intentObjectSet);
				}
				if (namedEntitiesMap == null) {
					namedEntitiesMap = new HashMap<String, String>();
					distillJSON.put("Named Entities", namedEntitiesMap);
				} else {
					distillJSON.put("Named Entities", namedEntitiesMap);
				}

				distillJSON.put("Intent", intentObj);
				netMap.put(snippet, distillJSON);
			}

			JSONObject netJSON = new JSONObject(netMap);
			if (netJSON.length() > 0) {
				if (domainName != null) {
					netJSON.put("Domain", this.domainName);
				} else {
					netJSON.put("Domain", "Generic");
				}
			}

			return netJSON;
			//			return netJSON.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	protected JSONObject getSentiment() {
		List<XpDistill> reviewTriples = this.processPipeline();
		Map<String, JSONObject> netMap = new LinkedHashMap<String, JSONObject>();
		try {

			//			int i = 0;
			for (XpDistill t : reviewTriples) {
				//				if (XpActivator.PW != null && xpDistillSet.contains(t)) {
				//					XpActivator.PW.println(t.toString());
				//				} else {
				//					xpDistillSet.add(t);
				//				}

				//				System.out.println(t.toString());
				String snippet = t.reviewSnippet;
				JSONObject distillJSON = netMap.get(snippet);
				JSONArray aboutMapArr;
				if (distillJSON == null) {
					distillJSON = new JSONObject();
					aboutMapArr = new JSONArray();
					//					JSONArray aboutArr = new JSONArray();
					//					distillJSON.put("About", aboutMapArr);
				} else {
					aboutMapArr = distillJSON.getJSONArray("About");
				}

				JSONObject aboutObj = new JSONObject();

				//				String indicative_snippet;
				//				if (t.entityClass.equals(OVERALL_STR)) {
				//					indicative_snippet = t.opinion;
				//				} else {
				//					indicative_snippet = snippet;
				//				}
				//
				//				if (this.isHistoricalDemo && !isHistoricalDemoValidSnippet(indicative_snippet, t)) {
				//					continue;
				//				}

				//				JSONArray aboutMapArr = distillJSON.getJSONArray("About");
				//				if (aboutMapArr == null) {
				//					aboutMapArr = new JSONArray();
				//				}

				// String entityStr = t.entity.replaceAll("[^\\w\\s\\-]", "");
				String entityStr = Pattern.compile("[^\\w\\s\\-]", Pattern.UNICODE_CHARACTER_CLASS).matcher(t.entity).replaceAll("");
				aboutObj.put("Entity", entityStr);
				aboutObj.put("Aspect", t.aspectCategory);
				aboutObj.put("Sentiment", t.opinionSentiment);
				aboutObj.put("StatementType", t.statementType);
				aboutObj.put("Indicative Snippet", t.indicativeSnippet);
				aboutMapArr.put(aboutObj);
				distillJSON.put("About", aboutMapArr);

				//				if (domainName != null) {
				//					distillJSON.put("Domain", this.domainName);
				//				} else {
				//					distillJSON.put("Domain", "Generic");
				//				}

				netMap.put(snippet, distillJSON);
			}

			JSONObject netJSON = new JSONObject(netMap);
			if (netJSON.length() > 0) {
				if (domainName != null) {
					netJSON.put("Domain", this.domainName);
				} else {
					netJSON.put("Domain", "Generic");
				}
			}

			return netJSON;
			//			return netJSON.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	public JSONObject getIntention() {
		List<XpDistill> reviewTriples = this.processPipeline();
		Map<String, JSONObject> netMap = new LinkedHashMap<String, JSONObject>();
		try {

			//			int i = 0;
			for (XpDistill t : reviewTriples) {
				String snippet = t.reviewSnippet;
				JSONObject intentObj = netMap.get(snippet);

				if (intentObj == null) {
					intentObj = new JSONObject();
				}

				//				if (t.xpf.isIntentionFeature() && !t.opinionSentiment.equals(XpSentiment.NEGATIVE_SENTIMENT)) {
				if (t.xpf.isIntentionFeature()) {
					intentObj.put("Intent", true);
				} else {
					intentObj.put("Intent", false);
				}
				intentObj.put("Intent Object", t.xpf.getIntentionObject());

				netMap.put(snippet, intentObj);
			}

			JSONObject netJSON = new JSONObject(netMap);
			if (domainName != null) {
				netJSON.put("Domain", this.domainName);
			} else {
				netJSON.put("Domain", "Generic");
			}
			return netJSON;
			//			return netJSON.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * The method processes a list of sentences(in custom stanford data structure), extracts emotions based on
	 * semantic dependency, emotion lexicon and glove dependency and returns the list of aggregated emotions.
	 * @return a JSON string containing the sentences, their corresponding emotions and the aggregated emotions from the sentences
	 */
	protected JSONObject getEmotion() {

		List<XpDistill> reviewTriples = this.processPipeline();
		Set<String> overallEmotionSet = new HashSet<String>();

		try {

			JSONObject overallJSON = new JSONObject();
			JSONArray innerJSONArr = new JSONArray();
			//			int i = 0;
			for (XpDistill t : reviewTriples) {

				//					innerJSON.put("Sentence", sentenceStr);
				//				logger.info("SentenceEmotion Map (Case1): " + snippet + "\tEmotionSet: " + sentenceEmoSet);

				if (t.aspectCategory.equals(OVERALL_STR)) {
					JSONObject innerJSON = new JSONObject();
					innerJSON.put("Sentence", t.opinion);
					innerJSON.put("Emotion", t.emotionSet);
					innerJSONArr.put(innerJSON);
					overallEmotionSet.addAll(t.emotionSet);
				}

			}
			overallJSON.put("Review", this.original_text);

			if (overallEmotionSet.size() > 1 && overallEmotionSet.contains(XpSentiment.NEUTRAL_SENTIMENT)) {
				overallEmotionSet.remove(XpSentiment.NEUTRAL_SENTIMENT);
			} else if (overallEmotionSet.size() == 0) {
				overallEmotionSet.add(XpSentiment.NEUTRAL_SENTIMENT);
			}

			overallJSON.put("Aggregated Emotions", overallEmotionSet);
			overallJSON.put("Detail", innerJSONArr);

			return overallJSON;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	protected JSONObject getExprTrend() {

		//		Map<String, JSONObject> netMap = new LinkedHashMap<String, JSONObject>();
		JSONObject netJSON = new JSONObject();
		try {
			JSONObject exprJSON = this.getExpression();
			//			logger.info(exprJSON.toString());
			if (exprJSON == null) {
				exprJSON = new JSONObject();
				netJSON.put("Expr", exprJSON);
			} else {
				exprJSON.remove("Domain");
				netJSON.put("Expr", exprJSON);
			}

			JSONArray trendingTopics = new JSONArray();
			JSONObject trendJSON = TrendInfoManipulation.prepareTrendJSON(this.domainName);
			if (trendJSON != null && trendJSON.length() > 0)
				trendingTopics = trendJSON.getJSONArray("trending topics");

			JSONObject topicJSON = this.constructTopicJson(this.reviewTriples);
			JSONObject reviewSentiJSON = new JSONObject();
			if (topicJSON != null) {
				reviewSentiJSON = topicJSON.getJSONObject("Review Sentiment");
			}

			Set<String> entitySet = this.entityMap.keySet();
			if (entitySet != null) {
				netJSON.put("entity_list", entitySet);
			} else {
				netJSON.put("entity_list", new HashSet<String>());
			}

			netJSON.put("Review Sentiment", reviewSentiJSON);
			netJSON.put("trending_topics", ((trendingTopics == null) ? new JSONArray("[]") : trendingTopics));
			//			JSONObject netJSON = new JSONObject(netMap);
			//			netJSON.put("Review", this.original_text);
			if (domainName != null) {
				netJSON.put("Domain", this.domainName);
			} else {
				netJSON.put("Domain", "Generic");
			}

			return netJSON;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	protected JSONObject getAspect() {
		List<XpDistill> reviewTriples = this.processPipeline();
		Map<String, JSONObject> netMap = new LinkedHashMap<String, JSONObject>();
		try {

			for (XpDistill t : reviewTriples) {
				String snippet = t.reviewSnippet;
				JSONObject distillJSON = netMap.get(snippet);
				JSONArray aboutMapArr;
				if (distillJSON == null) {
					distillJSON = new JSONObject();
					aboutMapArr = new JSONArray();
				} else {
					aboutMapArr = distillJSON.getJSONArray("About");
				}

				JSONObject aboutObj = new JSONObject();
				String entityStr = Pattern.compile("[^\\w\\s\\-]", Pattern.UNICODE_CHARACTER_CLASS).matcher(t.entity).replaceAll("");
				aboutObj.put("Entity", entityStr);
				aboutObj.put("Aspect", t.aspectCategory);
				//				aboutObj.put("Sentiment", t.opinionSentiment);
				//				aboutObj.put("StatementType", t.statementType);
				aboutObj.put("Indicative Snippet", t.indicativeSnippet);
				aboutMapArr.put(aboutObj);
				distillJSON.put("About", aboutMapArr);

				netMap.put(snippet, distillJSON);
			}

			JSONObject netJSON = new JSONObject(netMap);
			if (netJSON.length() > 0) {
				if (domainName != null) {
					netJSON.put("Domain", this.domainName);
				} else {
					netJSON.put("Domain", "Generic");
				}
			}
			return netJSON;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	protected JSONObject constructTopicJson(List<XpDistill> reviewTriples) {
		Map<String, List<String>> topicMap = new HashMap<String, List<String>>();

		try {
			//			logger.info("domainName : " + this.domainName);
			JSONObject netJSON = new JSONObject();
			JSONArray aboutJSONArr = new JSONArray();
			int aspectCount = 0;
			for (XpDistill t : reviewTriples) {
				//				System.out.println(t.toString());
				String topic = t.aspectCategory;
				String sentiment = t.opinionSentiment;
				//				logger.info("topic : " + topic + " sentiment : " + sentiment);
				if (!t.aspectCategory.equals(OVERALL_STR)) {
					aspectCount++;
					if (topicMap.containsKey(topic)) {
						//					logger.info(topic + " already exists");
						topicMap.get(topic).add(sentiment);
					} else {
						//					logger.info(topic + " doesn't exist, adding now");
						List<String> sentiList = new ArrayList<String>();
						sentiList.add(sentiment);
						topicMap.put(topic, sentiList);
					}
				}
			}
			//			logger.info("topicMap : \n" + topicMap);
			netJSON.put("Review", this.original_text);
			Double totalWeightage = new Double(aspectCount);
			Double totalPosWeightage = 0.0;
			Double totalNeutralWeightage = 0.0;
			Double totalNegWeightage = 0.0;

			for (String s : topicMap.keySet()) {
				JSONObject innerJSON = new JSONObject();
				innerJSON.put("Aspect", s);
				List<String> sentimentList = topicMap.get(s);
				Map<String, Double> sentimentMap = new LinkedHashMap<String, Double>();
				Double posSentimentWeightage = new Double(Collections.frequency(sentimentList, "Positive")) / new Double(sentimentList.size());
				sentimentMap.put("Positive", new Double(Math.round(posSentimentWeightage * 100.0) / 100.0));
				Double neutralSentimentWeightage = new Double(Collections.frequency(sentimentList, "Neutral")) / new Double(sentimentList.size());
				sentimentMap.put("Neutral", new Double(Math.round(neutralSentimentWeightage * 100.0) / 100.0));
				Double negSentimentWeightage = new Double(Collections.frequency(sentimentList, "Negative")) / new Double(sentimentList.size());
				sentimentMap.put("Negative", new Double(Math.round(negSentimentWeightage * 100.0) / 100.0));
				JSONObject sentimentJSON = new JSONObject(sentimentMap);
				Double weightage = new Double(sentimentList.size()) / totalWeightage;
				weightage = new Double(Math.round(weightage * 100.0) / 100.0);
				totalPosWeightage += weightage * posSentimentWeightage;
				totalNegWeightage += weightage * negSentimentWeightage;
				totalNeutralWeightage += weightage * neutralSentimentWeightage;
				innerJSON.put("Weightage", weightage);
				innerJSON.put("Sentiment", sentimentJSON);
				aboutJSONArr.put(innerJSON);
			}

			Map<String, Double> netSentiMap = new LinkedHashMap<String, Double>();
			netSentiMap.put("Positive", new Double(Math.round(totalPosWeightage * 100.0) / 100.0));
			netSentiMap.put("Neutral", new Double(Math.round(totalNeutralWeightage * 100.0) / 100.0));
			netSentiMap.put("Negative", new Double(Math.round(totalNegWeightage * 100.0) / 100.0));

			JSONObject netSentiJSON = new JSONObject(netSentiMap);

			netJSON.put("Review Sentiment", netSentiJSON);
			netJSON.put("Detail", aboutJSONArr);
			if (domainName != null) {
				netJSON.put("Domain", this.domainName);
			} else {
				netJSON.put("Domain", "Generic");
			}

			return netJSON;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	protected JSONObject getTopic() {
		//		JSONObject topicJson = new JSONObject();
		if (this.annotation.equals(XpConfig.Annotations.TOPIC)) {
			List<XpDistill> reviewTriples = this.processPipeline();
			return constructTopicJson(reviewTriples);
		} else {
			return constructTopicJson(this.reviewTriples);
		}

	}

	/**
	 * @author Koustuv Saha
	 * Apr 13, 2015
	 * @param indicative_snippet
	 * @return
	 *
	 */
	protected boolean isHistoricalDemoValidSnippet(String indicative_snippet, XpDistill xd) {
		//		if (xpDistillSet.contains(xd)) {
		//			return false;
		//		} else {
		//			xpDistillSet.add(xd);
		//		}

		logger.info("isDomainRelevantReview: " + this.isDomainRelevantReview);
		if (!this.isDomainRelevantReview) {
			//			if (XpActivator.PW != null) {
			//				XpActivator.PW.println(indicative_snippet + "\t" + xd.toString());
			//			}
			return false;
		} else if (indicative_snippet.matches("\\W+")) {
			return false;
		} else {
			int strLength = ProcessString.tokenizeString(language, indicative_snippet).size();
			if (strLength < 2) {
				return false;
			} else if (strLength < 4 && !SentimentFunctions.isPolarSentiment(xd.opinionSentiment)) {
				return false;
			} else {
				String tempString = indicative_snippet.replaceAll("\\W", "");
				tempString = tempString.replaceAll("(https://|http://)", "");
				double thresholdLength = indicative_snippet.length() * 0.6;
				if (tempString.length() < thresholdLength) {
					return false;
				}
			}
		}
		return true;
	}

	protected String determineOpinionString(String entity, String opinion) {
		String net_opinionStr;

		if (entity.contains(opinion)) {
			net_opinionStr = entity;
		} else if (opinion.endsWith(entity) || opinion.endsWith(entity.split(" ")[0])) {
			net_opinionStr = opinion;
		} else if (OntologyUtils.isRejectEntity(language, entity)) {
			net_opinionStr = opinion;
		} else {
			net_opinionStr = opinion + " " + entity;
		}
		return net_opinionStr;

	}

	protected void addToReviewTriples(XpDistill currentTriple) {
		currentTriple.mismatchFix();
		if (isHistoricalDemo && !isHistoricalDemoValidSnippet(currentTriple.indicativeSnippet, currentTriple)) {
			logger.info("Not adding this triple : " + currentTriple.toString());
			return;
		}
		logger.info("Adding this triple : " + currentTriple.toString());
		reviewTriples.add(currentTriple);
	}

	protected void addCurrentTriple(String aspect, String sentiment, String opinion, List<XpDistill> triplesList, Map<String, Set<String>> aspectSentiMap, XpDistill rawT) {

		/*Don't want to add cases when the opinion is junk and neutral. All other cases should be added*/
		if (XpSentiment.NEUTRAL_SENTIMENT.equals(sentiment) && aspectSentiMap.containsKey(aspect) && opinion.length() < 3) {
			return;
		}
		Set<String> sentiHash = aspectSentiMap.get(aspect);
		if (sentiHash == null) {
			sentiHash = new HashSet<String>();
		}

		if (!sentiHash.contains(sentiment)) {
			sentiHash.add(sentiment);

			aspectSentiMap.put(aspect, sentiHash);
			//			System.out.println("Sentiment: " + sentiment + "\tAdding Triple: " + rawT.toString());
			triplesList.add(rawT);
		}

	}

	protected void colourCheck(Map<String, Set<String>> entityOpinionMap) {
		String colouredEntity = null;
		//		boolean isColouredEntity = false;
		for (Map.Entry<String, Set<String>> entry : entityOpinionMap.entrySet()) {
			String entity = entry.getKey();
			if (entity.contains(" ")) {
				String[] entityArr = entity.split(" ");
				if ("COLOR".equals(XpOntology.getOntologyCategory(language, this.domainName, entityArr[0], null))) {
					//				if (entityArr.length == 2 && COLOUR_HASH.contains(entityArr[0].toLowerCase())) {
					//					isColouredEntity = true;
					colouredEntity = entity.split(" ")[1];
					break;
				}
			}
		}
		String[] replaceEntityArr = null;
		if (colouredEntity != null) {
			for (Map.Entry<String, Set<String>> entry : entityOpinionMap.entrySet()) {
				String entity = entry.getKey();

				/*For the entity which is just color. The red wine was good, but the white was bad. Here "white"*/
				//				if (!entity.contains(" ") && COLOUR_HASH.contains(entity.toLowerCase())) {
				if (!entity.contains(" ") && "COLOR".equals(XpOntology.getOntologyCategory(language, this.domainName, entity, null))) {
					replaceEntityArr = new String[2];
					replaceEntityArr[0] = entity;
					replaceEntityArr[1] = entity + " " + colouredEntity;
					break;
				}

				//				String[] entityArr = entity.toLowerCase().split(" ");
				//				if (COLOUR_HASH.contains(entityArr[0])) {
				//					if (entityArr.length == 1 || (entityArr.length == 2 && entityArr[1].equals("one"))) {
				//						replaceEntityArr = new String[2];
				//						replaceEntityArr[0] = entity;
				//						replaceEntityArr[1] = entityArr[0] + " " + colouredEntity;
				//						break;
				//					}
				//				}

			}
		}
		if (replaceEntityArr != null) {
			entityOpinionMap.put(replaceEntityArr[1], entityOpinionMap.get(replaceEntityArr[0]));
			entityOpinionMap.remove(replaceEntityArr[0]);
		}
	}

	protected String loopOnOpinions(String snippet, boolean needSentimentFlag, Map<String, Set<String>> entityOpinionMap, String snippetSentiment, XpSentenceResources xto, XpFeatureVector featureSet, String statementType) {
		if (!XpConfig.exprAnnotations.contains(this.annotation)) {
			return null;
		}
		this.colourCheck(entityOpinionMap);

		// if (XpConfig.USE_EMBEDDED_GRAPHDB)
		// return loopOnOpinionsGraphDB(snippet, entityOpinionMap,
		// snippetSentiment, tagSentimentMap, tagAspectMap, featureSet,
		// statementType);

		List<XpDistill> tempTripleList = new ArrayList<XpDistill>();
		Map<String, Set<String>> aspectSentiMap = new HashMap<String, Set<String>>();
		// boolean isSinglePolarAspect = false;
		// String singleAspectPolarSentiment = null;

		if (entityOpinionMap.size() == 0) {
			this.isDomainRelevantReview = true;
		}

		Map<String, String> tagSentimentMap = xto.getTagSentimentMap();
		//		vectorList.stream().forEach((obj) -> {
		//			gloveVector.add((Double) obj);
		//		});

		for (Map.Entry<String, Set<String>> entry : entityOpinionMap.entrySet()) {
			String main_entity = entry.getKey().toLowerCase();
			Set<String> opinionSet = entry.getValue();
			//			String[] sdp_aspect = tagAspectMap.get(main_entity);

			String[] in_aspect = null;

			//			if (sdp_aspect != null) {
			//				in_aspect = sdp_aspect;
			//			}
			//			System.out.println("main_entity : " + main_entity);
			in_aspect = XpOntology.findAspect(language, main_entity, domainName, subject, xto);
			//			System.out.println("in_aspect : " + in_aspect);

			if (in_aspect == null && !this.isDomainRelevantReview) {
				if (this.domainName != null) {
					String category = XpOntology.getOntologyCategory(language, this.domainName, main_entity, xto);
					if (category != null) {
						if (XpOntology.isDomainRelevantCategory(language, category, this.domainName)) {
							this.isDomainRelevantReview = true;
						} else {
							this.isDomainRelevantReview = false;
						}
					} else if (OntologyUtils.isRejectEntity(language, main_entity)) {
						/*This, that*/
						this.isDomainRelevantReview = true;
					} else {
						/*Daughter, school*/
						this.isDomainRelevantReview = false;
					}
				} else {
					this.isDomainRelevantReview = true;
				}
			}
			String entity = main_entity;
			/* Find Aspect In Opinion(); */

			for (String opinion : opinionSet) {
				String[] aspectArr = in_aspect;
				opinion = opinion.toLowerCase();

				/*
				 * If aspect is not found from SDP or the mainEntity either then we need to look in the opinionPart
				 */
				if (in_aspect == null) {
					/*opinion should be treated as tryEntity for findAspect*/
					aspectArr = XpOntology.findAspect(language, opinion, domainName, subject, xto, true, true);
				}

				String netOpinionStr = null;
				/*
				 * Suppose aspect found so far is null, so it is being called with opinion+entity. For eg: findAspect on across street
				 */
				if (aspectArr == null) {
					netOpinionStr = this.determineOpinionString(main_entity, opinion);
					//					System.out.println("Sending netOpinionStr: " + opinion);
					if (!netOpinionStr.equals(opinion) && !netOpinionStr.equals(entity)) {
						aspectArr = XpOntology.findAspect(language, netOpinionStr, domainName, subject, xto, true, true);
					}
				}
				if (aspectArr != null) {
					this.isDomainRelevantReview = true;
					entity = aspectArr[0];
					if (netOpinionStr == null) {
						netOpinionStr = determineOpinionString(main_entity, opinion);
					}

					String opinionSentiment = tagSentimentMap.get(aspectArr[1]);

					// System.out.println("OpinionSentiment1:\t" + opinion +
					// "\t" + opinionSentiment);
					if (opinionSentiment == null) {
						opinionSentiment = SentimentEngine.sentimentEvaluation(language, netOpinionStr, xto, true);
					}
					// if (aspectArr[1].equals(COMPETITION_CONCEPT)) {
					if (aspectArr[1].contains(COMPETITION_CONCEPT)) {
						//						System.out.println("aspect - " + aspectArr[1]);
						if (subject != null && XpConfig.IS_SUBJECT_MODE) {

							if (entity.contains(subject.toLowerCase())) {
								logger.info("Competition Concept aspect and subject same Aspect!=Competition: " + subject);
								continue;
							}
							opinionSentiment = SentimentFunctions.switchSentiment(opinionSentiment);
							//							System.out.println("XXXXXXXXXXXXXXXXXXXXYYYYYYYYYYYYYYYYYYY switch opinion sentiment - " + opinionSentiment);
							/*
							 * If we mine the aspect as competition we also need to determine if there is a presence of implicit  aspect in its opinion part
							 */
							this.addOpinionAspectSentiment(opinion, snippet, xto, opinionSentiment, snippetSentiment, statementType, tempTripleList, aspectSentiMap, featureSet);
							XpDistill rawT = new XpDistill(reviewID, this.original_text, snippet, subject, aspectArr[0], aspectArr[1], opinion, opinionSentiment, snippetSentiment, statementType, xto);
							this.addCurrentTriple(aspectArr[1], opinionSentiment, opinion, tempTripleList, aspectSentiMap, rawT);

						} else {
							this.addOpinionAspectSentiment(opinion, snippet, xto, opinionSentiment, snippetSentiment, statementType, tempTripleList, aspectSentiMap, featureSet);
						}
						continue;
					}
					XpDistill rawT = new XpDistill(reviewID, this.original_text, snippet, subject, aspectArr[0], aspectArr[1], opinion, opinionSentiment, snippetSentiment, statementType, xto);
					this.addCurrentTriple(aspectArr[1], opinionSentiment, opinion, tempTripleList, aspectSentiMap, rawT);

					if (in_aspect != null) {
						this.isDomainRelevantReview = true;
						this.addOpinionAspectSentiment(opinion, snippet, xto, opinionSentiment, snippetSentiment, statementType, tempTripleList, aspectSentiMap, featureSet);
					}

				}
			}
		}
		logger.info("Snippet Sentiment (Before): " + snippetSentiment);
		return computeSingleAspectPolarSentiment(snippet, needSentimentFlag, snippetSentiment, statementType, aspectSentiMap, tempTripleList);
	}

	protected void addOpinionAspectSentiment(String opinion, String snippet, XpSentenceResources xto, String opinionSentiment, String snippetSentiment, String statementType, List<XpDistill> tempTripleList, Map<String, Set<String>> aspectSentiMap, XpFeatureVector xpf) {
		//		String[] implicitAspect = XpOntology.findAspect(opinion, domainName, wordPOSMap, tagAspectMap);

		List<String> opinionWords = ProcessString.tokenizeString(language, opinion);
		String[] implicitAspect = null;
		Map<String, String> wordPOSmap = xto.getWordPOSmap();
		for (String word : opinionWords) {
			String posTag = wordPOSmap.get(word);
			if (posTag != null) {
				// if (posTag.contains("JJ") || posTag.contains("RB") || posTag.contains("VB")) {
				//				TODO main verb or verb in general (including semi-aux and aux?
				if (PosTags.isAdjective(language, posTag) || PosTags.isAdverb(language, posTag) || PosTags.isMainVerb(language, posTag)) {
					implicitAspect = XpOntology.findImplicitAspect(language, word, domainName, subject, xto);
					if (implicitAspect != null) {
						break;
					}
				}
			}
		}

		if (implicitAspect != null) {
			XpDistill rawT = new XpDistill(reviewID, this.original_text, snippet, subject, implicitAspect[0], implicitAspect[1], opinion, opinionSentiment, snippetSentiment, statementType, xto);
			this.addCurrentTriple(implicitAspect[1], opinionSentiment, opinion, tempTripleList, aspectSentiMap, rawT);
		}

	}

	protected String computeSingleAspectPolarSentiment(String snippet, boolean needSentimentFlag, String snippetSentiment, String statementType, Map<String, Set<String>> aspectSentiMap, List<XpDistill> tempTripleList) {
		//		System.out.println("entered computesingleaspectpolarsentiment");
		//		System.out.println("SnippetSentiment: " + snippetSentiment);
		//		System.out.println("NeedSentimentFlag in computeSingleAspec: \t" + needSentimentFlag + "\tSnippetSentiment: " + snippetSentiment);
		logger.info("Initial Aspect Sentiment HashMap:\t" + aspectSentiMap);
		boolean isSinglePolarAspect = false;
		if (aspectSentiMap.size() == 1) {
			isSinglePolarAspect = true;
		}
		// boolean need_flag = false;
		// if (tagSentimentMap.get(SentimentLexicon.NEED_ID) != null) {
		// need_flag = true;
		// }
		String singleAspectPolarSentiment = null;

		//		Set<String> alreadyReportedAspectsHash = new HashSet<String>();

		//		System.out.println("Aspect SentimentMap: " + aspectSentiMap + "\tSnippetSentiment: " + snippetSentiment);
		for (XpDistill rawT : tempTripleList) {

			//			String entity = rawT.entity;
			String asp = rawT.aspectCategory;

			//			String asp = entity + "#" + entityClass;

			//			if (alreadyReportedAspectsHash.contains(asp)) {
			//				continue;
			//			}

			Set<String> aspectSentiHash = aspectSentiMap.get(asp);
			//			System.out.printl
			//			System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX aspectSentiHash - " + aspectSentiHash);

			boolean addTripleCase = needSentimentFlag && isSinglePolarAspect && aspectSentiHash.size() == 1;

			//			System.out.println("addTripleCase - " + addTripleCase + " rawT.opinionSentiment - " + rawT.opinionSentiment + " snippetSentiment - " + snippetSentiment);

			if (addTripleCase) {
				rawT.opinionSentiment = snippetSentiment;
				rawT.statementType = statementType;
			}

			String opinionSentiment = rawT.opinionSentiment;
			if (isSinglePolarAspect == true && aspectSentiHash.size() > 1) {
				isSinglePolarAspect = false;
			}
			//			System.out.println(rawT);
			switch (opinionSentiment) {
				case (XpSentiment.NEGATIVE_SENTIMENT):
					this.addToReviewTriples(rawT);
					//					alreadyReportedAspectsHash.add(asp);
					if (isSinglePolarAspect) {
						singleAspectPolarSentiment = opinionSentiment;
						break;
					}
					break;
				case (XpSentiment.POSITIVE_SENTIMENT):
					//					System.out.println("Entered Here 1.");

					if (!aspectSentiHash.contains(XpSentiment.NEGATIVE_SENTIMENT) || addTripleCase || !snippetSentiment.equals(XpSentiment.NEGATIVE_SENTIMENT)) {
						//						System.out.println("Entered Here 2.Adding\t" + rawT.toString());
						this.addToReviewTriples(rawT);
						//						alreadyReportedAspectsHash.add(asp);
						if (isSinglePolarAspect) {
							singleAspectPolarSentiment = opinionSentiment;
						}
					}
					break;
				case (XpSentiment.NEUTRAL_SENTIMENT):
					if (!aspectSentiHash.contains(XpSentiment.NEGATIVE_SENTIMENT) && !aspectSentiHash.contains(XpSentiment.POSITIVE_SENTIMENT) || addTripleCase) {
						// if (XpConfig.IS_SUBJECT_MODE &&
						// rawT.entityClass.equals(COMPETITION_CONCEPT)) {
						if (XpConfig.IS_SUBJECT_MODE && rawT.aspectCategory.contains(COMPETITION_CONCEPT)) {
							rawT.opinionSentiment = SentimentFunctions.switchSentiment(snippetSentiment);
						}

						/*Currently it is setting the snippet sentiment for all the aspects. Not just single aspect*/
						if (!VocabularyTests.containsBut(language, snippet)) {
							rawT.opinionSentiment = snippetSentiment;
							rawT.statementType = statementType;
						}
						// rawT.snippetSentiment = rawT.snippetSentiment;
						this.addToReviewTriples(rawT);
						//						alreadyReportedAspectsHash.add(asp);
					}
					break;
			}

		}

		logger.info("Single Aspect Polar Sentiment: " + singleAspectPolarSentiment);

		return singleAspectPolarSentiment;

	}

	protected void linguisticFeaturize(List<?> tokenList, Map<String, String> wordPOSmap, String snippet, XpFeatureVector featureSet) {

		/**
		 * check the list of idioms for the sentiment and set the corresponding feature(POS or NEG) in the feature set to be true
		 */
		String lemmatizedSnippet = CoreNLPController.lemmatizeToString(language, snippet);
		logger.info("lemmatizedSnippet: " + lemmatizedSnippet);
		String idiomSentiment;
		idiomSentiment = XpLexMatch.sequenceContain(XpSentiment.getIdiomsMap(language), snippet);
		if (idiomSentiment == null) {
			idiomSentiment = XpLexMatch.sequenceContain(XpSentiment.getIdiomsMap(language), lemmatizedSnippet);
			//	logger.info("idiomSentiment after lemmatization: " + idiomSentiment);
		}

		if (idiomSentiment != null) {
			if (XpSentiment.POSITIVE_SENTIMENT.equals(idiomSentiment)) {
				logger.info("Positive Idiom found");
				featureSet.setPosIdiomPresentFeature(true);
			}
			if (XpSentiment.NEGATIVE_SENTIMENT.equals(idiomSentiment)) {
				logger.info("Negative Idiom found");
				featureSet.setNegIdiomPresentFeature(true);
			}
		}

		/**
		 * check the list of phrasal verbs for the sentiment and set the corresponding feature(POS or NEG) in the feature set to be true
		 * Spanish: no phrasal verb set.
		 */
		String phrasalVerbSentiment = XpLexMatch.sequenceContain(XpSentiment.getPhrasalVerbMap(language), snippet);
		//		System.out.println("phrasalVerbSentiment: " + phrasalVerbSentiment);
		if (phrasalVerbSentiment != null) {
			if (XpSentiment.POSITIVE_SENTIMENT.equals(phrasalVerbSentiment)) {
				logger.info("Positive Idiom found");
				featureSet.setPosPhrasalVerbPresentFeature(true);
			}
			if (XpSentiment.NEGATIVE_SENTIMENT.equals(phrasalVerbSentiment)) {
				logger.info("Negative Idiom found");
				featureSet.setNegPhrasalVerbPresentFeature(true);
			}
		}

		/**
		 * check the list of abuse words for the abuse sequence and set the corresponding feature(Simple Abuse or Intensifier Abuse) in the feature set to be true
		 */
		String abuseSeq;
		abuseSeq = XpLexMatch.sequenceMatch(XpSentiment.getAbuseList(language), snippet);

		if (abuseSeq != null) {
			logger.info("Abuse Word Found");
			featureSet.setAbusePresentFeature(true);
			if (featureSet.isCapitalizedPresentFeature() && XpLexMatch.isCapitalized(abuseSeq, snippet)) {
				featureSet.setIntsfNegFeature(true);
			}
		}

		/**
		 * check the list of advocate words for the advocate sequence and set the corresponding feature(Simple Advocate or Intensifier Advocate) in the feature set to be true
		 */
		String advocateSeq;
		advocateSeq = XpLexMatch.sequenceMatch(XpSentiment.getAdvocateList(language), snippet);
		if (advocateSeq != null) {
			//			System.out.println("advocateSeq " + advocateSeq);
			logger.info("Advocate Word Found");
			featureSet.setAdvocatePresentFeature(true);
			if (featureSet.isCapitalizedPresentFeature() && XpLexMatch.isCapitalized(advocateSeq, snippet)) {
				featureSet.setIntsfPosFeature(true);
			}
		}

		/**
		 * check the list of suggestion words for the suggestion sequence and set the corresponding feature(Suggestion Presence) in the feature set to be true
		 */
		String suggestionSeq;
		suggestionSeq = XpLexMatch.sequenceMatch(XpSentiment.getSuggestionList(language), snippet);
		if (suggestionSeq != null) {
			//			System.out.println("suggestionSeq " + suggestionSeq);
			logger.info("Suggestion Word Found");
			featureSet.setSuggestionPresentFeature(true);
		}

		//<<<<<<< HEAD
		String greetingSalutationSeq = XpLexMatch.sequenceMatch(XpSentiment.getGreetSalutationsRegExList(language), snippet);
		if (greetingSalutationSeq != null) {
			//			System.out.println("greetingSalutationSeq " + greetingSalutationSeq);

			logger.info("Greetings or Salutations Found:\t" + greetingSalutationSeq);
			featureSet.setGreetingsSalutationPresentFeature(true);
		}

		if (tokenList.size() >= 2) {
			/**
			 * check if the sentence starts with a base verb
			 * In case of Spanish, as pos tags are different none of these features will be taken into account.
			 */

			//<<<<<<< HEAD
			//			if (wordPOSmap.get(tokenList.get(0)).equals("VB") && !tokenList.get(0).toLowerCase().contains("thank") && !SentimentFunctions.isPolarTag(SentimentLexicon.getLexTag(language, tokenList.get(0), wordPOSmap, null))) {
			//				System.out.println("English orders");
			//				featureSet.setStartsWithVBbaseFeature(true);
			//			} else if (wordPOSmap.get(tokenList.get(0)).equals("PRP") && wordPOSmap.get(tokenList.get(1)).equals("VB") && !SentimentFunctions.isPolarTag(SentimentLexicon.getLexTag(language, tokenList.get(1), wordPOSmap, null))) {
			//				System.out.println("English orders");
			//				featureSet.setStartsWithVBbaseFeature(true);
			//			}
			//=======
			String firstWord = ((String) tokenList.get(0)).toLowerCase();
			String secondWord = ((String) tokenList.get(1)).toLowerCase();

			String firstWordTag = wordPOSmap.get(firstWord);

			/*Improve your hotel is a suggestion. Improved your hotel is not*/
			boolean noThank = !VocabularyTests.isThank(language, firstWord);
			boolean noWeakVerbExceptBe = !OntologyUtils.isWeakVerb(language, firstWord) || VocabularyTests.isBe(language, firstWord);
			if (PosTags.isVerbBase(language, firstWordTag) && noThank && noWeakVerbExceptBe) {
				featureSet.setStartsWithVBbaseFeature(true);
			} else if (VocabularyTests.isKindly(language, firstWord) || VocabularyTests.isPlease(language, firstWord, secondWord)) {
				featureSet.setStartsWithVBbaseFeature(true);
			}
			//			else if (OntologyUtils.isWeakVerb(language, firstWord)) {
			//				System.out.println("Set it here!!3");
			//				featureSet.setStartsWithWeakVerbFeature(true);
			//			}

			else if (PosTags.isPersonalPronoun(language, firstWordTag) && PosTags.isMainVerb(language, wordPOSmap.get(secondWord))) {
				/*You do this. (Not for he/she/they)*/
				if (VocabularyTests.isSecondPersonSubject(language, firstWord)) {
					featureSet.setStartsWithVBbaseFeature(true);
				}
			} else if (PosTags.isAdverb(language, firstWordTag) && PosTags.isMainVerb(language, wordPOSmap.get(secondWord))) {
				/*Just do this*/
				featureSet.setStartsWithVBbaseFeature(true);
			}

			if (snippet.endsWith("?")) {
				featureSet.setQuestionFeature(true);
			}

		}

	}

	protected void emoticonFeaturize(String snippet, XpFeatureVector featureSet) {

		// logger.info(
		// SentimentLexicon.getEmoticonsMap().toString());
		String emoticonSenti = XpLexMatch.sequenceContain(XpSentiment.getEmoticonsMap(language), snippet);
		logger.info("Emoticon Sentiment: " + emoticonSenti);
		if (XpSentiment.POSITIVE_SENTIMENT.equals(emoticonSenti) || XpSentiment.POSITIVE_TAG.equals(emoticonSenti)) {
			featureSet.setPosEmoticonPresentFeature(true);
		}
		if (XpSentiment.NEGATIVE_SENTIMENT.equals(emoticonSenti) || XpSentiment.NEGATIVE_TAG.equals(emoticonSenti)) {
			featureSet.setNegEmoticonPresentFeature(true);
		}
	}

	protected String getNeedSentiment(Map<String, String> tagSentimentMap) {
		String needSentiment = tagSentimentMap.get(XpSentiment.NEED_ID);
		logger.info("Sentiment From Need Tag: " + needSentiment);
		return needSentiment;
	}

	protected String getPossessionSentiment(Map<String, String> tagSentimentMap) {
		String possnSentiment = tagSentimentMap.get(XpSentiment.POSSN_ID);
		logger.info("Sentiment From Possession Tag: " + possnSentiment);
		return possnSentiment;
	}

	protected String computeForOverallTriples(CoreMap sentence, Tree parseTree, SemanticGraph semanticDepGraph, List<String> tokenList, List<XpDistill> overall_triples, String snippet, XpSentenceResources xto, XpFeatureVector xpf, String statementType) {
		List<String> overallSentimentList = new ArrayList<String>();

		boolean polarPresentFlag = false;

		List<String> subSnippetsList = this.getSubSnippets(parseTree, sentence);
		String snippetSentiment;
		String continueSameSentiment = XpSentiment.NEUTRAL_SENTIMENT;
		if (subSnippetsList != null) {
			logger.info("found " + subSnippetsList.size() + " subsnippets");
			logger.info("SubSnippets : " + subSnippetsList);
			List<XpDistill> tempOverallTriples = new ArrayList<XpDistill>();
			for (String subSnippet : subSnippetsList) {
				/*
				 * All subsnippets are being sent with the same and common featureSet, which is for the sentence. In this can't take the ML based Sentiment
				 */
				String subSnippetSentiment = null;
				boolean considerMLsentiment = false;
				logger.info("ConsiderMLsentiment: " + considerMLsentiment + "\tSubSnippet: " + subSnippet + "\tStatement Type : " + statementType);

				if (subSnippet.length() >= snippet.length()) {
					considerMLsentiment = true;
					subSnippetSentiment = SentimentEngine.sentimentEvaluation(language, tokenList, xto, considerMLsentiment);
				} else {
					subSnippetSentiment = SentimentEngine.sentimentEvaluation(language, subSnippet.toLowerCase(), xto, considerMLsentiment);
				}

				boolean isSentimentPolar = SentimentFunctions.isPolarSentiment(subSnippetSentiment);
				logger.info("isSentimentPolar " + isSentimentPolar);

				if (polarPresentFlag && !isSentimentPolar) {
					continue;
				}

				if (XpEngine.getMachineLearningMode() && statementType == null) {
					statementType = XpEngine.getXpStmtClassifier(language).getPredictedClass(xpf);
					logger.info("StatementType (Overall Case): " + statementType);
				}

				logger.info("statementType being added in tempTriple : " + statementType);
				XpDistill overall_rawT = new XpDistill(reviewID, this.original_text, snippet, subject, OVERALL_STR, OVERALL_STR, subSnippet, subSnippetSentiment, subSnippetSentiment, statementType, xto);
				tempOverallTriples.add(overall_rawT);
				overallSentimentList.add(subSnippetSentiment);

				if (isSentimentPolar) {
					polarPresentFlag = true;

					/*If the sentiment shifts polarity, then make this variable null, If it is neutral make it current sentiment, Rest cases, keep it as it is*/
					if (continueSameSentiment != null) {
						if (continueSameSentiment.equals(XpSentiment.NEUTRAL_SENTIMENT)) {
							//							logger.info("check1 : " + continueSameSentiment + "\t" + subSnippetSentiment);
							continueSameSentiment = subSnippetSentiment;
						} else if (!continueSameSentiment.equals(subSnippetSentiment)) {
							//							logger.info("check2 : " + continueSameSentiment + "\t" + subSnippetSentiment);
							continueSameSentiment = null;
						}
					}
				}
			}

			snippetSentiment = SentimentFunctions.calculateAggregateSentiment(overallSentimentList);
			logger.info("snippetSentiment : " + snippetSentiment);

			//			System.out.println("TempTriples: " + tempOverallTriples);
			for (XpDistill tempTriple : tempOverallTriples) {
				//				System.out.println("computeForOverallTriples l. 736 " + tempTriple.opinionSentiment);

				if (polarPresentFlag && tempTriple.opinionSentiment.equals(XpSentiment.NEUTRAL_SENTIMENT) && !StatementTypeLexicon.SUGGESTION_STATEMENT.equals(tempTriple.statementType)) {
					continue;
				}

				if (tempOverallTriples.size() == 1 || continueSameSentiment != null) {
					tempTriple.indicativeSnippet = snippet;
					tempTriple.opinionSentiment = snippetSentiment;
					logger.info("tempTriple : " + tempTriple);
					overall_triples.add(tempTriple);
					if (XpEngine.getMachineLearningMode()) {
						tempTriple.statementType = XpEngine.getXpStmtClassifier(language).getPredictedClass(xpf);
						logger.info("StatementType (Single Overall): " + statementType);
					}
					break;
				}
				overall_triples.add(tempTriple);

				//				if (polarPresentFlag) {
				//					if ((!tempTriple.opinionSentiment.equals(XpSentiment.NEUTRAL_SENTIMENT)) || StatementTypeLexicon.SUGGESTION_STATEMENT.equals(tempTriple.statementType)) {
				//						/*If continueSameSentiment, no need to report multiple overalls. Also make the indicative snippet = snippet*/
				//						if (tempOverallTriples.size() == 1 || continueSameSentiment != null) {
				//							tempTriple.opinion = snippet;
				//							overall_triples.add(tempTriple);
				//							if (XpEngine.getMachineLearningMode()) {
				//								tempTriple.statementType = XpEngine.getXpStmtClassifier(language).getPredictedClass(xpf);
				//								logger.info("StatementType (Single Overall): " + statementType);
				//							}
				//							break;
				//						}
				//						overall_triples.add(tempTriple);
				//					}
				//				} else {
				//					if (tempOverallTriples.size() == 1 || continueSameSentiment != null) {
				//						tempTriple.opinion = snippet;
				//						overall_triples.add(tempTriple);
				//						if (XpEngine.getMachineLearningMode()) {
				//							tempTriple.statementType = XpEngine.getXpStmtClassifier(language).getPredictedClass(xpf);
				//							logger.info("StatementType (Single Overall): " + statementType);
				//						}
				//						break;
				//					}
				//					overall_triples.add(tempTriple);
				//				}
			}

		} else {
			//			System.out.println("No subsnippet");
			snippetSentiment = SentimentEngine.sentimentEvaluation(language, tokenList, xto, true);
			XpDistill overall_rawT = new XpDistill(reviewID, this.original_text, snippet, subject, OVERALL_STR, OVERALL_STR, snippet, snippetSentiment, snippetSentiment, statementType, xto);
			overall_triples.add(overall_rawT);
			overallSentimentList.add(snippetSentiment);
		}
		return snippetSentiment;
	}

	protected void addOverallTriples(List<XpDistill> overallTriples, String opinionSentiment, String snippetSentiment, List<String> tokenList, StanfordDependencyGraph sdg) {
		for (XpDistill overallT : overallTriples) {
			/*
			 * If the sentence has switched overall sentiment let the opinions have their own sentiments associated
			 */
			if (opinionSentiment != null) {
				overallT.opinionSentiment = opinionSentiment;
			}
			overallT.snippetSentiment = snippetSentiment;

			//			EmotionVector ev = new EmotionVector(language, overallT.opinion, tokenList, semanticDepGraph, overallT.opinionSentiment, subject, domainName, wordPOSmap, true);
			this.addOverallTriple(overallT, tokenList, sdg);
			//			System.out.println("ReviewTriples: " + this.reviewTriples);
		}
	}

	protected void addOverallTriple(XpDistill overallTriple, List<String> tokenList, StanfordDependencyGraph sdg) {
		if (this.annotation == Annotations.EMO || this.annotation == Annotations.EXPR || this.annotation == Annotations.EXPR_TREND) {
			EmotionVector ev = new EmotionVector(language, overallTriple.opinion, tokenList, overallTriple.opinionSentiment, sdg);
			Set<String> sentenceEmoSet = ev.getEmotionList();
			overallTriple.setEmotionSet(sentenceEmoSet);
		}
		this.addToReviewTriples(overallTriple);
	}

	public List<XpDistill> processPipeline() {
		return processPipeline(true);
	}

	//	protected boolean isValidSentence(CoreMap sentence) {
	//		Matcher matcher = validWordPattern.matcher(sentence.toString());
	//		if (matcher.find())
	//			return true;
	//		return false;
	//	}

	/* Set the isSplittedSentence to be false, if you don't want splitted result */
	public List<XpDistill> processPipeline(boolean isSplittedSentence) {
		List<CoreMap> sentencesList = getSentences();
		XpFeatureVector xpf = null;
		XpSentenceResources xto = null;

		Map<String, String> wordPOSmap = null;
		if (!isSplittedSentence) {
			xpf = new XpFeatureVector();
			xpf.setStanfordSentimentFeature(this.getStanfordSentiment());
			xpf.setCountStopWordsFeature(ProcessString.countStopWords(language, this.normalized_text));
			wordPOSmap = new HashMap<String, String>();
			// featureSet.setWordBasedFeatures(this.normalized_text);
		}

		for (CoreMap sentence : sentencesList) {

			Tree parseTree = this.getParseTree(sentence);
			logger.info("Parse Tree:\t" + parseTree.toString());

			String stanfordSentiment = (!this.isMicro) ? this.getStanfordSentiment(sentence) : null;

			String snippet = sentence.toString();

			if (isSplittedSentence) {
				wordPOSmap = new HashMap<String, String>();
				xpf = new XpFeatureVector();
			}

			if (!xpf.isActionInFuture() && VocabularyTests.goingToFutureTense(language, snippet))
				xpf.setIsActionInFuture(true);
			if (stanfordSentiment != null) {
				//				System.out.println("processPipeline: stanfordSentiment: " + stanfordSentiment);
				xpf.setStanfordSentimentFeature(stanfordSentiment);
				logger.info("Stanford Sentiment:\t" + stanfordSentiment);
			}
			Map<String, String> tagSentimentMap = new HashMap<String, String>();

			List<String> tokenList = this.getTokens(domainName, sentence, wordPOSmap, tagSentimentMap, xpf);

			if (tokenList == null) {
				continue;
			}
			logger.info("TokenList:\t" + tokenList.toString());

			this.emoticonFeaturize(snippet, xpf);
			this.linguisticFeaturize(tokenList, wordPOSmap, snippet, xpf);

			SemanticGraph semanticDepGraph = null;
			StanfordDependencyGraph sdg = null;

			Map<String, String[]> tagAspectMap = new HashMap<String, String[]>();
			xto = new XpSentenceResources(sentence, wordPOSmap, tagSentimentMap, tagAspectMap, xpf);

			if ((language == Languages.EN) || ((language == Languages.SP) && XpConfig.STANFORD_SP_DEPPARSE)) {
				if (tokenList.size() < 80) {
					semanticDepGraph = this.getDependencyGraph(sentence);
					//					logger.info("Dependency Graph:\n" + semanticDepGraph.toString(SemanticGraph.OutputFormat.LIST));
					logger.info("Dependency Graph:\n" + semanticDepGraph.toString(SemanticGraph.OutputFormat.LIST));
				}
				//				sdg = new StanfordDependencyGraph(this.language, snippet, domainName, subject, semanticDepGraph, xto, xpf);
				sdg = new StanfordDependencyGraph(snippet, semanticDepGraph, xto, this);
			} else if (language == Languages.SP && !XpConfig.STANFORD_SP_DEPPARSE) {
				ConstituencyTree tree = null;
				List<SemanticGraphEdge> edges = null;
				if (tokenList.size() < 100) {
					tree = this.getConstituencyTree();
					edges = tree.extractEdges();
					//	List<SemanticGraphEdge> edges = this.getDependencyEdges();
				}
				//				sdg = new StanfordDependencyEdges(this.language, snippet, domainName, subject, tree, edges, xto, xpf);
				sdg = new StanfordDependencyEdges(snippet, tree, edges, xto, this);
				logger.info("Dependency Edges:\n" + sdg.getEdgesAsStr());
			}

			String snippetSentiment = null;
			String statementType = null;

			Map<String, Set<String>> entityOpinionMap = null;
			//			Map<String, String[]> tagAspectMap = null;
			//			if (language == Languages.EN) {
			if (sdg != null) {
				entityOpinionMap = sdg.getEntityOpinionMap();
			} //			}

			List<XpDistill> overallTriples = new ArrayList<XpDistill>();

			boolean needSentimentFlag = false;
			snippetSentiment = this.getNeedSentiment(tagSentimentMap);
			//			System.out.println("NeedSentiment in XpExpression: " + snippetSentiment + "\tTagSentimentMap: " + tagSentimentMap);
			if (snippetSentiment != null) {
				if (!xpf.isGreetingsSalutationPresentFeature()) {
					needSentimentFlag = true;
					//					System.out.println("NeedSentimentFlag: " + needSentimentFlag + "\tSnippetSentiment: " + snippetSentiment);
				}
			} else {
				snippetSentiment = getPossessionSentiment(tagSentimentMap);
			}

			/*Start - Processing for Overall Triples*/
			if (snippetSentiment != null) {
				if (XpEngine.getMachineLearningMode()) {

					statementType = XpEngine.getXpStmtClassifier(language).getPredictedClass(xpf);
					logger.info("StatementType (Need Case): " + statementType);
					if ((!statementType.equals(StatementTypeLexicon.SUGGESTION_STATEMENT) && snippetSentiment.equals(XpSentiment.NEGATIVE_SENTIMENT) || snippetSentiment.equals(XpSentiment.NEUTRAL_SENTIMENT))) {
						statementType = StatementTypeLexicon.SUGGESTION_STATEMENT;
					}
				}

				XpDistill overall_rawT = new XpDistill(reviewID, this.original_text, snippet, subject, OVERALL_STR, OVERALL_STR, snippet, snippetSentiment, snippetSentiment, statementType, xto);
				overallTriples.add(overall_rawT);

			} else {
				//				System.out.println("Snippet Sentiment (Before): " + snippetSentiment);
				snippetSentiment = this.computeForOverallTriples(sentence, parseTree, semanticDepGraph, tokenList, overallTriples, snippet, xto, xpf, statementType);
				//				System.out.println("Snippet Sentiment (After): " + snippetSentiment);
				//				logger.info("tagSentimentMap contains 'no': " + tagSentimentMap.containsKey("no"));
			}

			/*End - Processing for Overall Triples*/

			/*This is being repetitive. Need to refactor this later*/
			if (XpEngine.getMachineLearningMode() && statementType == null) {
				//				System.out.println("processPipeline: l. 879");
				statementType = XpEngine.getXpStmtClassifier(language).getPredictedClass(xpf);
				//				System.out.println(featureSet.toStringTrue());
				logger.info("StatementType (Common Case): " + statementType);
			}

			/*Start - Processing for Aspect Based Triples - Dependency Parser Based*/
			if (entityOpinionMap != null) {
				String singleAspectPolarSentiment = null;
				if (entityOpinionMap.size() > 0) {
					//					System.out.println("Single aspect analysis");

					//					logger.info("Entity-Opinion Map:\t" + entityOpinionMap.toString() + "\n");

					singleAspectPolarSentiment = this.loopOnOpinions(snippet, needSentimentFlag, entityOpinionMap, snippetSentiment, xto, xpf, statementType);
					//					System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX singleAspectPolarSentiment : " + singleAspectPolarSentiment);
					//					logger.info("tagSentimentMap contains 'no': " + tagSentimentMap.containsKey("no"));

					//					if (singleAspectPolarSentiment != null && overallTriples.size() == 1) {
					//						/* If the sentence has switched overall sentiment let the opinions have their own sentiments associated*/
					//						this.addOverallTriples(overallTriples, singleAspectPolarSentiment, snippetSentiment, tokenList, sdg);
					//					} else {
					//						this.addOverallTriples(overallTriples, null, snippetSentiment, tokenList, sdg);
					//					}

					if (this.entityMap == null) {
						this.entityMap = new HashMap<String, Integer>();
					}
					for (String entityStr : entityOpinionMap.keySet()) {
						entityStr = entityStr.toLowerCase().trim();
						entityStr = entityStr.replaceAll("(jan(uary)?|feb(ruary)?|mar(ch)?|apr(il)?|may|jun(e)?|jul(y)?|aug(ust)?|sep(tember)?|oct(ober)?|nov(ember)?|dec(ember)?)", "");
						entityStr = entityStr.replaceAll("(mon(day)?|tue(sday)?|wed(nesday)?|thu(rsday)?|fri(day)?|sat(urday)?|sun(day)?)", "");
						entityStr = entityStr.replaceAll("[0-9]+(pm|am)?", "");
						entityStr = entityStr.replaceAll("[^\\w\\s\\-]", "");
						entityStr = entityStr.trim();
						if (!entityStr.isEmpty() && !entityStr.equals("") && !OntologyUtils.isRejectEntity(language, entityStr) && !entityStr.matches("[0-9]+")) {
							//							finalEntityList.add(entityStr);
							Integer entityCount = this.entityMap.get(entityStr);
							if (entityCount != null)
								this.entityMap.put(entityStr, entityCount.intValue() + 1);
							else
								this.entityMap.put(entityStr, 1);
						}
					}
				}
				/*End - Processing for Aspect Based Triples - Dependency Parser Based*/

				/*
				 * This is when it would look for basic entity spotting. Eg:
				 * Food good. This returns a non-null dependency graph of size 0
				 */
				else {
					//				this.addOverallTriples(overallTriples, snippetSentiment, snippetSentiment);
					//				System.out.println("processPipeline: Basic sentiment analysis");

					/*Great, Very good*/
					this.isDomainRelevantReview = true;
					this.analyseBasicSentiment(snippet, snippetSentiment, statementType, overallTriples, tokenList, xto);
					//					logger.info("tagSentimentMap contains 'no': " + tagSentimentMap.containsKey("no"));
				}

				if (singleAspectPolarSentiment != null && overallTriples.size() == 1) {
					/* If the sentence has switched overall sentiment let the opinions have their own sentiments associated*/
					this.addOverallTriples(overallTriples, singleAspectPolarSentiment, snippetSentiment, tokenList, sdg);
				} else {
					this.addOverallTriples(overallTriples, null, snippetSentiment, tokenList, sdg);
				}
			}

			if (XpEngine.getInteractiveMode()) {
				logger.info("XP Feature Vector:\t" + xpf.toStringTrue());
			}
			//			if (this.reviewTriples.size() == 0) {
			//				XpDistill basicT = new XpDistill(reviewID, this.original_text, snippet, subject, OVERALL_STR, OVERALL_STR, snippet, snippetSentiment, snippetSentiment, statementType);
			//				reviewTriples.add(basicT);
			//			}

			/* call sdg.getFeatureSet() to get the featureSet vector */
			if (isSplittedSentence) {
				snippetFeatureMap.put(snippet, xpf);
			}
		}

		if (!isSplittedSentence) {
			//			System.out.println("processPipeline: SWN score");
			double score = SWNsentiment.getSWNscore(wordPOSmap);
			// System.out.println(this.normalized_text + ": " + score);
			xpf.setSentiWNSentimentFeature(score);
			//			System.out.println("line 960, snippet: " + this.normalized_text);
			snippetFeatureMap.put(this.normalized_text, xpf);
		}

		if (this.reviewTriples.size() == 0) {
			XpDistill basicT = new XpDistill(reviewID, this.original_text, this.normalized_text, subject, OVERALL_STR, OVERALL_STR, this.original_text, XpSentiment.NEUTRAL_SENTIMENT, XpSentiment.NEUTRAL_SENTIMENT, StatementTypeLexicon.OPINION_STATEMENT, xto);
			this.addOverallTriple(basicT, null, null);
			if (!reviewTriples.contains(basicT)) {
				this.addToReviewTriples(basicT);
			}
		}
		//		System.out.println("Exiting processPipeline(boolean)");
		return this.reviewTriples;
	}

	public Set<Map<String, Integer>> sentencePOSEntityExtraction(boolean isSplittedSentence) {
		Set<Map<String, Integer>> finalEntitySet = new LinkedHashSet<Map<String, Integer>>();
		List<CoreMap> sentencesList = getSentences();
		StringBuilder sb = new StringBuilder();
		for (CoreMap sentence : sentencesList) {
			Map<String, String> nerMap = getNERsentence(sentence);
			Map<String, Integer> sentenceMap = new HashMap<String, Integer>();
			List<CoreLabel> coreLabelList = sentence.get(TokensAnnotation.class);
			for (int i = 0; i < coreLabelList.size(); i++) {
				CoreLabel currElement = coreLabelList.get(i);
				String currWord = currElement.word();
				String currPos = currElement.tag();
				if (currPos.matches("NN[PS]*")) {
					sb.append(currWord);
					sb.append(" ");
				} else if (sb.length() > 0) {
					String entityStr = sb.toString().trim();
					if (!OntologyUtils.isRejectEntity(language, entityStr) && !entityStr.matches("[0-9]+") && !nerMap.containsKey(entityStr)) {
						entityStr = entityStr.toLowerCase();
						Integer count = sentenceMap.get(entityStr);
						if (count == null)
							sentenceMap.put(entityStr, 1);
						else
							sentenceMap.put(entityStr, count.intValue() + 1);
					}
					sb = new StringBuilder();
				}

			}
			if (sb.length() > 0) {
				String entityStr = sb.toString().trim();
				if (!OntologyUtils.isRejectEntity(language, entityStr) && !entityStr.matches("[0-9]+") && !nerMap.containsKey(entityStr)) {
					entityStr = entityStr.toLowerCase();
					Integer count = sentenceMap.get(entityStr);
					if (count == null)
						sentenceMap.put(entityStr, 1);
					else
						sentenceMap.put(entityStr, count.intValue() + 1);
				}
			}
			finalEntitySet.add(sentenceMap);
		}
		//		System.out.println("Final Entity Map - " + finalEntityMap);
		return finalEntitySet;

	}

	public Map<String, Integer> processPipelineForPOSbasedEntities(boolean isSplittedSentence) {
		Map<String, Integer> finalEntityMap = new HashMap<String, Integer>();
		List<CoreMap> sentencesList = getSentences();
		StringBuilder sb = new StringBuilder();
		for (CoreMap sentence : sentencesList) {
			Map<String, String> nerMap = getNERsentence(sentence);
			List<CoreLabel> coreLabelList = sentence.get(TokensAnnotation.class);
			for (int i = 0; i < coreLabelList.size(); i++) {
				CoreLabel currElement = coreLabelList.get(i);
				String currWord = currElement.word();
				String currPos = currElement.tag();
				if (currPos.matches("NN[PS]*")) {
					sb.append(currWord);
					sb.append(" ");
				} else if (sb.length() > 0) {
					String entityStr = sb.toString().trim();
					if (!OntologyUtils.isRejectEntity(language, entityStr) && !entityStr.matches("[0-9]+") && !nerMap.containsKey(entityStr)) {
						entityStr = entityStr.toLowerCase();
						Integer count = finalEntityMap.get(entityStr);
						if (count == null)
							finalEntityMap.put(entityStr, 1);
						else
							finalEntityMap.put(entityStr, count.intValue() + 1);
					}
					sb = new StringBuilder();
				}

			}
			if (sb.length() > 0) {
				String entityStr = sb.toString().trim();
				if (!OntologyUtils.isRejectEntity(language, entityStr) && !entityStr.matches("[0-9]+") && !nerMap.containsKey(entityStr)) {
					entityStr = entityStr.toLowerCase();
					Integer count = finalEntityMap.get(entityStr);
					if (count == null)
						finalEntityMap.put(entityStr, 1);
					else
						finalEntityMap.put(entityStr, count.intValue() + 1);
				}
			}
		}
		//		System.out.println("Final Entity Map - " + finalEntityMap);
		return finalEntityMap;

	}

	public Map<String, Integer> mentionBasedEntities() {
		List<CoreMap> sentencesList = getSentences();
		Map<String, Integer> allMentionEntityMap = new HashMap<String, Integer>();
		for (CoreMap sentence : sentencesList) {
			List<CoreMap> mentionEntities = getEntityMentions(sentence);
			if (mentionEntities != null) {
				for (CoreMap entity : mentionEntities) {
					String entityStr = entity.toString();
					Integer entityCount = allMentionEntityMap.get(entityStr);
					if (entityCount != null)
						allMentionEntityMap.put(entityStr, entityCount.intValue() + 1);
					else
						allMentionEntityMap.put(entityStr, 1);
				}
			}
		}

		return allMentionEntityMap;
	}

	private void processPipelineForEntities(boolean isSplittedSentence) {
		//		Set<String> finalEntityList = new HashSet<String>();
		this.entityMap = new HashMap<String, Integer>();
		List<CoreMap> sentencesList = getSentences();
		XpFeatureVector xpf = null;
		Map<String, String> wordPOSmap = null;
		if (!isSplittedSentence) {
			xpf = new XpFeatureVector();
			xpf.setStanfordSentimentFeature(this.getStanfordSentiment());
			xpf.setCountStopWordsFeature(ProcessString.countStopWords(language, this.normalized_text));
			wordPOSmap = new HashMap<String, String>();
		}

		for (CoreMap sentence : sentencesList) {

			//			Map<String, String> nerMap = this.getNERsentence(sentence);
			//			List<CoreMap> entityMentions = this.getEntityMentions(sentence);

			//			String stanfordSentiment = (!this.isMicro) ? this.getStanfordSentiment(sentence) : null;
			String snippet = sentence.toString();

			if (isSplittedSentence) {
				wordPOSmap = new HashMap<String, String>();
				xpf = new XpFeatureVector();

			}
			//			if (!xpf.isActionInFuture() && VocabularyTests.goingToFutureTense(language, snippet))
			//				xpf.setIsActionInFuture(true);
			//			if (stanfordSentiment != null) {
			//				//				System.out.println("processPipeline: stanfordSentiment: " + stanfordSentiment);
			//				xpf.setStanfordSentimentFeature(stanfordSentiment);
			//				logger.info("Stanford Sentiment:\t" + stanfordSentiment);
			//			}
			Map<String, String> tagSentimentMap = new HashMap<String, String>();

			double tic = System.currentTimeMillis();
			List<String> tokenList = this.getTokens(domainName, sentence, wordPOSmap, tagSentimentMap, xpf);
			//			logger.info("tagSentimentMap contains 'no': " + tagSentimentMap.containsKey("no"));
			logger.info("getTokens duration: " + (System.currentTimeMillis() - tic) + " ms");

			if (tokenList == null) {
				continue;
			}
			logger.info("TokenList:\t" + tokenList.toString());

			//			this.emoticonFeaturize(snippet, xpf);
			//			this.linguisticFeaturize(tokenList, wordPOSmap, snippet, xpf);

			SemanticGraph semanticDepGraph = null;

			StanfordDependencyGraph sdg = null;

			Map<String, String[]> tagAspectMap = new HashMap<String, String[]>();
			XpSentenceResources xto = new XpSentenceResources(sentence, wordPOSmap, tagSentimentMap, tagAspectMap, xpf);

			if ((language == Languages.EN) || ((language == Languages.SP) && XpConfig.STANFORD_SP_DEPPARSE)) {
				if (tokenList.size() < 80) {
					semanticDepGraph = this.getDependencyGraph(sentence);
					logger.info("Dependency Graph:\n" + semanticDepGraph.toString(SemanticGraph.OutputFormat.LIST));
				}
				sdg = new StanfordDependencyGraph(snippet, semanticDepGraph, xto, this);
			} else if (language == Languages.SP) {
				ConstituencyTree tree = null;
				List<SemanticGraphEdge> edges = null;
				if (tokenList.size() < 100) {
					tree = this.getConstituencyTree();
					edges = tree.extractEdges();
				}
				sdg = new StanfordDependencyEdges(snippet, tree, edges, xto, this);
				logger.info("Dependency Edges:\n" + sdg.getEdgesAsStr());
			}

			if (sdg != null) {
				Map<String, Set<String>> entityOpinionMap = sdg.getEntityOpinionMap();
				if (entityOpinionMap != null) {
					for (String entityStr : entityOpinionMap.keySet()) {
						entityStr = entityStr.toLowerCase().trim();
						entityStr = entityStr.replaceAll("\b(jan(uary)?|feb(ruary)?|mar(ch)?|apr(il)?|may|jun(e)?|jul(y)?|aug(ust)?|sep(tember)?|oct(ober)?|nov(ember)?|dec(ember)?)\b", "");
						entityStr = entityStr.replaceAll("\b(mon(day)?|tue(sday)?|wed(nesday)?|thu(rsday)?|fri(day)?|sat(urday)?|sun(day)?)\b", "");
						entityStr = entityStr.replaceAll("[0-9]+(pm|am)?", "");
						entityStr = entityStr.replaceAll("[^\\w\\s\\-]", "");
						entityStr = entityStr.trim();
						if (!entityStr.isEmpty() && !entityStr.equals("") && !OntologyUtils.isRejectEntity(language, entityStr) && !entityStr.matches("[0-9]+")) {
							//							finalEntityList.add(entityStr);
							Integer entityCount = entityMap.get(entityStr);
							if (entityCount != null)
								this.entityMap.put(entityStr, entityCount.intValue() + 1);
							else
								this.entityMap.put(entityStr, 1);
						}
					}
				}
			}

		}
		//		return finalEntityList;
		logger.info("FinalEntityMap in XpExpression : " + entityMap);
	}

	public void analyseBasicSentiment(String snippet, String snippetSentiment, String statementType, List<XpDistill> overallTriples, List<String> tokenList, XpSentenceResources xto) {
		// snippet = snippet.toLowerCase();

		// List<String> snippetParts =
		// ProcessString.tokenizeString(snippet);
		// System.out.println("SnippetParts: " + snippetParts);
		// System.out.println("TokenList: " + tokenList);
		// this.analyseBasicSentiment();

		//		String[] entityClass = null;
		//		String entityWord = null;
		List<String[]> entityClassList = null;
		Set<XpDistill> basicTriplesHash = null;

		List<String> nonNerTokenList = new ArrayList<String>(tokenList);

		Map<String, String> nerMap = xto.getNerMap();

		for (Map.Entry<String, String> entry : nerMap.entrySet()) {
			List<String> entryWords = ProcessString.tokenizeString(language, entry.getKey());
			for (String currWd : entryWords) {
				nonNerTokenList.remove(currWd.toLowerCase());
			}
		}

		for (String word : nonNerTokenList) {
			// entityClass = this.spotEntity(word.toLowerCase(), subject,
			// snippetSentiment, wordPOSmap);
			//			CAUTION: in Spanish tagAspectMap is null so far; necessary to use domainName = null to avoid error
			//			System.out.println("Sending from AnalyseBasicSentiment: " + word);
			String[] entityClass = XpOntology.findAspect(language, word, domainName, subject, xto);
			//			System.out.println("Aspect for " + word + "\t" + Arrays.toString(entityClass));

			if (entityClass != null) {
				// entityWord = word;
				if (entityClassList == null) {
					entityClassList = new ArrayList<String[]>();
				}

				//				String entityWord = entityClass[0];
				//				System.out.println("Subject Mode: " + XpConfig.IS_SUBJECT_MODE);
				//				if (subject != null && XpConfig.IS_SUBJECT_MODE && entityClass[1].contains(COMPETITION_CONCEPT) && entityWord.contains(subject.toLowerCase())) {
				//					logger.info( "Competition Concept aspect and subject same Aspect!=Competition: " + subject);
				//					entityClass = null;
				//					continue;
				//				}

				if (entityClass[1].contains(COMPETITION_CONCEPT)) {
					if (subject != null && XpConfig.IS_SUBJECT_MODE) {
						if (entityClass[0].contains(subject.toLowerCase())) {
							logger.info("Competition Concept aspect and subject same Aspect!=Competition: " + subject);
							continue;
						}
					} else {
						continue;
					}
				}

				entityClassList.add(entityClass);
				//				break;
			}
		}
		//		Map<List<String>, String> basicSentimentMap = SentimentEngine.sentimentEvaluationAdvanced(tokenList, tagSentimentMap, featureSet);
		//		for (Map.Entry<List<String>, String> entry : basicSentimentMap.entrySet()) {
		//			snippetSentiment = entry.getValue();
		//		}

		/* Basic text spotting starts here */
		//		if (entityClass != null) {
		Map<String, String> tagSentimentMap = xto.getTagSentimentMap();
		if (entityClassList != null) {
			for (String[] entityClass : entityClassList) {
				String entityWord = entityClass[0];
				if (XpSentiment.NEUTRAL_SENTIMENT.equals(snippetSentiment)) {
					tagSentimentMap.clear();

					for (String wd : nonNerTokenList) {
						//						String tag = SentimentEngine.getDomainContextualSentimentTag(language, entityWord, wd.toLowerCase(), domainName, subject, xto);
						String tag = XpOntology.getDomainContextualSentimentTag(language, entityWord, entityClass, wd.toLowerCase(), domainName, subject, xto);

						if (tag != null) {
							String[] tagArr = tag.split("#");
							String sentiTag = tagArr[0];
							String aspect = tagArr[1].toUpperCase();
							if (sentiTag != null) {
								logger.info("ContextualSentiment Tag of " + wd + " in " + entityWord + " \t " + sentiTag + "\t" + aspect);
								tagSentimentMap.put(wd, sentiTag);
								snippetSentiment = SentimentEngine.sentimentEvaluation(language, tokenList, xto, false);

								if (XpOntology.isValidAspectCategory(language, aspect, domainName)) {
									entityClass[1] = aspect;
								}
							}
						}
					}

				}
				if (snippetSentiment == null) {
					snippetSentiment = XpSentiment.NEUTRAL_SENTIMENT;
				}

				// if (snippet.length() > 0 && !snippet.matches("\\W+")) {
				if (snippet.length() > 0 && !Pattern.compile("\\W+", Pattern.UNICODE_CHARACTER_CLASS).matcher(snippet).matches()) {
					XpDistill basicT = new XpDistill(reviewID, this.original_text, snippet, subject, entityWord, entityClass[1], entityWord, snippetSentiment, snippetSentiment, statementType, xto);
					if (basicTriplesHash == null) {
						basicTriplesHash = new HashSet<XpDistill>();
					}
					basicTriplesHash.add(basicT);
				}
				//				System.out.println("Adding\n" + basicT);
				// XpEngine.addToTriples(basicT);
			}
		}

		if (basicTriplesHash != null) {
			for (XpDistill basicTriple : basicTriplesHash) {
				this.addToReviewTriples(basicTriple);
			}
		} else {
			//			if (this.reviewTriples.size() == 0) {
			if (overallTriples.size() == 0) {
				XpDistill basicOverallTriple = new XpDistill(reviewID, this.original_text, snippet, subject, OVERALL_STR, OVERALL_STR, snippet, snippetSentiment, snippetSentiment, statementType, xto);
				overallTriples.add(basicOverallTriple);
			}
			//			this.addToReviewTriples(basicOverallTriple);

			//			}
		}

		//		System.out.println("Exit from Analyse Basic Sentiment");
		//		} else {
		//		}
	}
	// protected String[] spotEntity(String str, String subject, String
	// snippetSentiment, Map<String, String> wordPOSMap) {
	// return XpOntology.findAspect(str, this.domainName, wordPOSMap);
	// }

	// protected String[] spotEntity(String str, String subject, String
	// snippetSentiment, Map<String, String> wordPOSMap) {
	//
	// // System.out.println("Checking entity for " + str);
	// // String entity = OntoWorks.findAspect(str, subject, str_sentiment,
	// // snippetSentiment);
	// String[] aspectArr = null;
	// // aspectArr = XpOntology.findAspect(str, subject, domainName,
	// snippetSentiment, wordPOSMap);
	// aspectArr = XpOntology.findAspect(str, domainName, wordPOSMap);
	//
	// // System.out.println("Entity found 1: " + entity);
	//
	// if (aspectArr == null) {
	// List<String> tokenList = ProcessString.tokenizeString(str);
	// if (tokenList.size() > 1) {
	// for (String token : tokenList) {
	// if (XpUtils.isRejectEntity(token)) {
	// continue;
	// } else {
	// aspectArr = XpOntology.getImplicitAspect(token, domainName);
	// }
	// if (aspectArr != null) {
	// logger.info( "ImplicitEntity\t" + str + ":\t" + token + ":\t" +
	// Arrays.toString(aspectArr));
	// break;
	// }
	// }
	// }
	// // System.out.println("Entity found 2: " + entity);
	// }
	//
	// // if (entity == null) {
	// // return null;
	// // } else {
	// // System.out.println("Returning from spotEntity: " + entity);
	// return aspectArr;
	// // }
	// }

}