/**
 *
 */
package  com.abzooba.xpresso.machinelearning;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpEngine;
import com.abzooba.xpresso.expressions.sentiment.XpSentiment;
import com.abzooba.xpresso.expressions.statementtype.StatementTypeLexicon;
import com.abzooba.xpresso.utils.FileIO;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.evaluation.NominalPrediction;
import weka.classifiers.evaluation.Prediction;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

/**
 * @author Koustuv Saha
 * 08-Jul-2014 9:42:59 am
 * XpressoV2.0.1  ClassifierML
 */
public class XpClassifierML {

    //		private static final String FEATURE_PATTERN = "is.*Feature";
    private static final String FEATURE_PATTERN = "(is|get).*Feature";
    //	private static final String MISC_FEATURE_PATTERN = "get*Feature";

    public static boolean IS_BASE_CLASS = false;

    //	private static final String trainingInputFileEN = "data/taggedDataML.txt";
    //	private static final String trainingInputFileSP = "data/taggedDataML-sp.txt";
    //	private static final Languages LANGUAGE = Languages.EN;
    private static final Languages LANGUAGE = Languages.EN;

    //	private static String trainingInputFile = XpConfig.TRAINING_INPUT_FILE;
    // private static String trainingInputFile =
    // "./data/taggedDataRetag3_sample.txt";

    protected final String classLabel = "Class";

    private static String outputFile = null;
    // private String modelFile = "./data/modelFile.model";
    private String modelFile;
    //	private String modelFile = XpConfig.DATA_DIR + "/" + "modelFile.model";

    //	private static final Integer MAX_TRAIN = 100000;

    private final Languages language;

    private Classifier classifier;
    private ALGORITHM algorithm = ALGORITHM.RANDOMFOREST;

    public Instances allInstances;

    public enum ALGORITHM {
        SVM, RANDOMFOREST, LOGIT, NB
    };

    public enum SPLITMETHOD {
        ALL, SPLIT, KFOLD
    };

    public enum CLASSIFICATION_TYPE {
        SENTIMENT, STATEMENT
    };

    CLASSIFICATION_TYPE classification_type;

    protected PrintStream stdout;

    public PrintStream getStdout() {
        return stdout;
    }

    public void setStdout(PrintStream stdout) {
        this.stdout = stdout;
    }

    private static Map<Languages, List<String>> rawTrainingData = new ConcurrentHashMap<Languages, List<String>>();

    private final double trainFrac = 0.8;
    private final int kFold = 10;

    private List<String> targetClasses;

    private static List<String> featureVector = new ArrayList<String>();

    // private static Method[] xpMethodList = XpFeatureSet.class.getMethods();

    private static Map<Languages, Map<String, FeatureClass>> featureLabelMap = new ConcurrentHashMap<Languages, Map<String, FeatureClass>>();

    //	public static void init(boolean readFromModel) {
    //		Method[] xpMethodList = XpFeatureSet.class.getMethods();
    //		for (Method method : xpMethodList) {
    //			String methodName = method.getName();
    //			if (methodName.matches(featurePattern)) {
    //				featureVector.add(methodName);
    //			}
    //		}
    //		// System.out.println("Initial: " + featureVector.toString());
    //		Collections.sort(featureVector);
    //		// System.out.println("Initial: " + featureVector.toString());
    //		if (!readFromModel) {
    //			vectorizeLabelledData();
    //		}
    //	}

    public static void init(boolean readFromModel) {
        //		System.out.println("Inside ML Init");
        Method[] xpMethodList = XpFeatureVector.class.getMethods();

        for (Method method : xpMethodList) {
            String methodName = method.getName();
            if (methodName.matches(FEATURE_PATTERN)) {
                featureVector.add(methodName);
            }
        }
        // System.out.println("Initial: " + featureVector.toString());
        Collections.sort(featureVector);
        // System.out.println("Initial: " + featureVector.toString());
        for (Languages language : XpConfig.LANGUAGES) {
            List<String> rawData = new ArrayList<String>();
            rawTrainingData.put(language, rawData);
            Map<String, FeatureClass> featureLabel = new LinkedHashMap<String, FeatureClass>();
            featureLabelMap.put(language, featureLabel);
        }
        //		if (!readFromModel) {
        //			//			for (Languages language : XpConfig.LANGUAGES) {
        //			vectorizeLabelledData(LANGUAGE);
        //			//			}
        //		}
    }

    //	private static void vectorizeLabelledData(Languages language) {
    //
    //		System.out.println("Inside vectorized labelled Data in " + language);
    //		List<String> trainFileLines = new ArrayList<String>();
    //		switch (language) {
    //			case SP:
    //				FileIO.read_file(trainingInputFileSP, trainFileLines);
    //				break;
    //			case EN:
    //				FileIO.read_file(trainingInputFileEN, trainFileLines);
    //				break;
    //		}
    //		// HashSet<String> trainingVectorHash = new HashSet<String>();
    //
    //		for (Integer i = 0; i < trainFileLines.size(); i++) {
    //
    //			if (i > MAX_TRAIN) {
    //				break;
    //			}
    //			String line = trainFileLines.get(i);
    //			String[] strArr = line.split("\t");
    //
    //			if (strArr.length >= 3) {
    //				String[] classValue = new String[2];
    //				String text = strArr[1];
    //				classValue[0] = strArr[2];
    //				if (strArr.length == 4) {
    //					classValue[1] = strArr[3];
    //				}
    //				// labelMap.put(text, classValue);
    //
    //				// XpText review = new XpText((i).toString(), "Walmart", text);
    //				XpExpression review = new XpExpression(language, Annotations.SENTI, false, (i).toString(), null, null, text, false);
    //				review.processPipeline(true);
    //
    //				Map<String, XpFeatureVector> snippetFeatureMap = review.getSnippetFeatureMap();
    //				XpFeatureVector xpf = null;
    //				for (Map.Entry<String, XpFeatureVector> entry2 : snippetFeatureMap.entrySet()) {
    //					//					System.out.println("Snippet: " + entry2.getKey());
    //					xpf = entry2.getValue();
    //					if (xpf == null) {
    //						continue;
    //					}
    //					Double[] featureValues = getFeatureVectorFromXPF(xpf);
    //					FeatureClass fc = new FeatureClass(featureValues, classValue);
    //					// String hashCodeFC = fc.toString();
    //					// System.out.println("HashCode: " + hashCodeFC);
    //					// if (!trainingVectorHash.contains(hashCodeFC) &&
    //					// !featureLabelMap.containsKey(text)) {
    //					if (!featureLabelMap.get(language).containsKey(text)) {
    //						featureLabelMap.get(language).put(text, fc);
    //						rawTrainingData.get(language).add(text);
    //						// trainingVectorHash.add(hashCodeFC);
    //						if ((i % 100) == 0) {
    //							System.out.println("Training: " + i);
    //						}
    //					}
    //				}
    //			}
    //		}
    //
    //	}

    //	private static Double[] getFeatureVectorFromXPF(XpFeatureSet xpf) {
    //		Double[] featureValues = new Double[featureVector.size()];
    //		for (int i = 0; i < featureVector.size(); ++i) {
    //			try {
    //				Method featureMethod = XpFeatureSet.class.getMethod(featureVector.get(i));
    //				featureValues[i] = (double) (((Boolean) featureMethod.invoke(xpf)) ? 1 : 0);
    //			} catch (Exception ex) {
    //				ex.printStackTrace();
    //			}
    //		}
    //		return featureValues;
    //	}

    private static Double[] getFeatureVectorFromXPF(XpFeatureVector xpf) {
        //		int dynamicFeaturesSize = xpf.getIsDynamicConceptFeature().length;
        //		System.out.println("Size of dynamicFeatureSize: " + dynamicFeaturesSize);
        //		Double[] featureValues = new Double[featureVector.size() + dynamicFeaturesSize];
        //		System.out.println("Inside getFeatureVectorFromXPF");
        Double[] featureValues = new Double[featureVector.size()];
        for (int i = 0; i < featureVector.size(); ++i) {
            try {
				/*Retrieves the method with the name in featureVector[i]*/
                //				System.out.println("Retrieving " + featureVector.get(i));
                Method featureMethod = XpFeatureVector.class.getMethod(featureVector.get(i));
                Object methodOutput = featureMethod.invoke(xpf);
                if (methodOutput.getClass().equals(Boolean.class)) {
                    //					System.out.println("Boolean Method: " + featureMethod.getName());
                    featureValues[i] = (double) (((Boolean) methodOutput) ? 1 : 0);
                    //					System.out.println("featureValue[" + i + "] is " + featureValues[i]);
                } else if (methodOutput.getClass().equals(Integer.class)) {
                    //					System.out.println("Integer Method: " + featureMethod.getName());
                    featureValues[i] = (int) methodOutput * 1.0;
                    //					System.out.println("featureValue[" + i + "] is " + featureValues[i]);
                } else {
                    //					System.out.println("Method: " + featureMethod.getName() + "\t" + featureMethod.getReturnType());
                    featureValues[i] = (double) methodOutput;
                    //					System.out.println("featureValue[" + i + "] is " + featureValues[i]);
                }
                //				if (featureMethod.getReturnType().equals(Boolean.class)) {
                //					System.out.println("Boolean Method: " + featureMethod.getName());
                //				} else {
                //					System.out.println("Method: " + featureMethod.getName() + "\t" + featureMethod.getReturnType());
                //					featureValues[i] = (double) (((Boolean) featureMethod.invoke(xpf)) ? 1 : 0);
                //				}
                //				featureValues[i] = (double) (((Boolean) featureMethod.invoke(xpf)) ? 1 : 0);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        //		for (int i = featureVector.size(); i < xpf.getIsDynamicConceptFeature().length; i++) {
        //			featureValues[i] = (double) (xpf.getIsDynamicConceptFeature()[i - featureVector.size()] ? 1 : 0);
        //		}
        //		System.out.println("FeatureValues size: " + featureValues.length);
        return featureValues;
    }

    public XpClassifierML(Languages language, CLASSIFICATION_TYPE ct, ALGORITHM algo, boolean readFromModel) {

        //		trainingInputFile = XpConfig.TRAINING_INPUT_FILE;
        this.language = language;
        this.classification_type = ct;
        try {
            stdout = outputFile != null ? new PrintStream(new FileOutputStream(outputFile)) : System.out;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            stdout = System.out;
        }

        algorithm = algo;
        switch (ct) {
            case SENTIMENT:
                targetClasses = new ArrayList<String>();
                targetClasses.add(XpSentiment.POSITIVE_SENTIMENT);
                targetClasses.add(XpSentiment.NEGATIVE_SENTIMENT);
                targetClasses.add(XpSentiment.NEUTRAL_SENTIMENT);
                modelFile = XpConfig.getSentiModel(this.language);
                prepareTrainingDataForClassifier(0);
                this.buildClassifier(this.allInstances, readFromModel);
                break;

            case STATEMENT:
                targetClasses = new ArrayList<String>();
                targetClasses.add(StatementTypeLexicon.ADVOCACY_STATEMENT);
                targetClasses.add(StatementTypeLexicon.COMPLAINT_STATEMENT);
                targetClasses.add(StatementTypeLexicon.SUGGESTION_STATEMENT);
                targetClasses.add(StatementTypeLexicon.OPINION_STATEMENT);
                modelFile = XpConfig.getStmtModel(this.language);
                prepareTrainingDataForClassifier(1);
                this.buildClassifier(this.allInstances, readFromModel);
                break;
            default:
                break;
        }

    }

    /**
     * Converts the Review Texts into FeatureVectors and prepares the training data for classification
     */
    public void prepareTrainingDataForClassifier(int targetClassIdx) {

        Instances rawTrainingInstances = getARFFMetaData();

        // for (Map.Entry<Double[], String[]> entry :
        // featureLabelMap.entrySet()) {
        for (Map.Entry<String, FeatureClass> entry : featureLabelMap.get(language).entrySet()) {

            // String text = entry.getKey();
            FeatureClass fc = entry.getValue();

            Double[] featureValues = fc.getX();
            //	System.out.println("FeatureValues Size: " + featureValues.length);

            String[] labelArr = fc.getY();
            String label = labelArr[targetClassIdx];
            if (label == null) {
                continue;
            }

            this.prepareClassifierInstance(featureValues, label, rawTrainingInstances);
        }
        this.allInstances = rawTrainingInstances;
        allInstances.setClassIndex(0);

    }

    public void prepareClassifierInstance(Double[] featureValues, String label, Instances instances) {

        double[] instanceValue = new double[instances.numAttributes()];

        // instanceValue[0] = instances.attribute(0).addStringValue(text);

        Double val = (Double) getClassifierClass(label);
        if (val == null) {
            val = (double) (this.targetClasses.size() - 1);
        }

        instanceValue[0] = val;
        for (int i = 0; i < featureVector.size(); ++i) {
            instanceValue[i + 1] = featureValues[i];
        }

        try {
            Instance currInstance = new DenseInstance(1.0, instanceValue);
            instances.add(currInstance);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Instances getARFFMetaData() {
        ArrayList<Attribute> features = new ArrayList<Attribute>();

        // features.add(new Attribute("Text", (ArrayList<String>) null));
        features.add(new Attribute(classLabel, targetClasses));

        for (int i = 0; i < featureVector.size(); ++i) {
            features.add(new Attribute(featureVector.get(i), i));
        }
        return new Instances("InstancesForClassification", features, 0);
    }

    public Object getClassifierClass(Object arg) {
        if (arg == null) {
            return null;
        }
        // switch (this.classificationType) {
        // case (SENTIMENT_TYPE):
        // if (this.c.equals(SENTIMENT_TYPE)) {

        switch (this.classification_type) {
            case SENTIMENT:
                switch (arg.toString()) {
                    case "0.0":
                        return XpSentiment.POSITIVE_SENTIMENT;
                    case (XpSentiment.POSITIVE_SENTIMENT):
                        return 0.0;
                    case "1.0":
                        return XpSentiment.NEGATIVE_SENTIMENT;
                    case (XpSentiment.NEGATIVE_SENTIMENT):
                        return 1.0;
                    case "2.0":
                        return XpSentiment.NEUTRAL_SENTIMENT;
                    case (XpSentiment.NEUTRAL_SENTIMENT):
                        return 2.0;
                    case "3.0":
                        // return SentiLex.NONE_SENTIMENT;
                        return XpSentiment.NEUTRAL_SENTIMENT;
                    // case (SentiLex.NONE_SENTIMENT):
                    // // return 3.0;
                    // return 2.0;
                    default:
                        return null;
                }
                // break;
                // case (STATEMENT_TYPE):
                // else if (this.classificationType.equals(STATEMENT_TYPE)) {
            case STATEMENT:
                switch (arg.toString()) {
                    case "0.0":
                        return StatementTypeLexicon.COMPLAINT_STATEMENT;
                    case (StatementTypeLexicon.COMPLAINT_STATEMENT):
                        return 0.0;
                    case "1.0":
                        return StatementTypeLexicon.SUGGESTION_STATEMENT;
                    case (StatementTypeLexicon.SUGGESTION_STATEMENT):
                        return 1.0;
                    case "2.0":
                        return StatementTypeLexicon.ADVOCACY_STATEMENT;
                    case (StatementTypeLexicon.ADVOCACY_STATEMENT):
                        return 2.0;
                    case "3.0":
                        return StatementTypeLexicon.OPINION_STATEMENT;
                    case (StatementTypeLexicon.OPINION_STATEMENT):
                        return 3.0;
                    default:
                        return null;
                }
        }
        return null;
    }

    public void printConfusionMatrix(Evaluation eval) throws Exception {
        stdout.println();
        stdout.println(eval.toMatrixString());
    }

    public Evaluation classify(Classifier model, Instances trainingSet, Instances testingSet) throws Exception {
        Evaluation evaluation = new Evaluation(trainingSet);

        model.buildClassifier(trainingSet);
        evaluation.evaluateModel(model, testingSet);

        return evaluation;
    }

    private Instances[][] crossValidationSplit(Instances data, int numberOfFolds) {

        Instances[][] split = new Instances[2][numberOfFolds];

        for (int i = 0; i < numberOfFolds; ++i) {
            split[0][i] = data.trainCV(numberOfFolds, i);
            split[1][i] = data.testCV(numberOfFolds, i);
        }

        return split;
    }

    private void printResults(Instances instancesData, List<Prediction> predictions, int startIndex, int endIndex) {
        stdout.println("Features:\t\t\t" + featureVector.toString().replaceAll(",", "\t") + "\n");
        String header = "isCorrect\tText\tPredicted\tActual\t";
        stdout.println(header);

        // for (int i = startIndex, k = 0; i < endIndex - 1; i++, k++) {
        // prediction
        for (int i = startIndex, k = 0; k < predictions.size(); i++, k++) {

            Prediction curr = predictions.get(k);
            NominalPrediction np = (NominalPrediction) curr;
            // System.out.println("Other Prediction:" + curr.predicted());
            String predictedClass = (String) getClassifierClass(np.predicted());
            String actualClass = (String) getClassifierClass(np.actual());
            Instance currInstance = instancesData.get(k);
            // boolean isCorrect = (np.predicted() == np.actual()) ? true :
            // false;
            boolean isCorrect = (predictedClass.equals(actualClass)) ? true : false;
            String outString = isCorrect + "\t" + rawTrainingData.get(language).get(i) + "\t" + predictedClass + "\t" + actualClass;
            double[] val = currInstance.toDoubleArray();
            for (int idx = 1; idx < val.length; idx++) {
                //				if (val[idx] == 1) {
                //					outString += "\t" + featureVector.get(idx - 1).toString();
                //				}
                outString += "\t" + val[idx];
            }
            stdout.println(outString);
            // }
        }

    }

    public Classifier getModel(ALGORITHM algorithm) {
        Classifier model = null;
        switch (algorithm) {
            case SVM:
                model = new weka.classifiers.functions.SMO();
                // Kernel kernel = new RBFKernel();
                // ((SMO) model).setKernel(kernel);
                break;
            case RANDOMFOREST:
                model = new weka.classifiers.trees.RandomForest();
                break;
            case LOGIT:
                model = new weka.classifiers.functions.Logistic();
                break;
            case NB:
                model = new weka.classifiers.bayes.NaiveBayes();
                break;
            default:
                model = new weka.classifiers.meta.AdaBoostM1();
        }
        return model;
    }

    public Classifier buildClassifier(Instances trainingInstances, boolean readFromModel) {
        Classifier classifier = null;
        try {
            if (readFromModel) {
                InputStream is = FileIO.getInputStreamFromFileName(modelFile);
                classifier = (Classifier) weka.core.SerializationHelper.read(is);
                //				classifier = (Classifier) weka.core.SerializationHelper.read(modelFile);
            } else {
                classifier = getModel(algorithm);
                classifier.buildClassifier(trainingInstances);
                weka.core.SerializationHelper.write(modelFile, classifier);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.classifier = classifier;
        return classifier;
    }

    public List<Prediction> testClassifier(Instances testInstances) {

        List<Prediction> predictions = null;
        try {
            Evaluation evaluation = new Evaluation(testInstances);
            evaluation.evaluateModel(classifier, testInstances);
            predictions = new ArrayList<Prediction>();
            predictions = evaluation.predictions();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return predictions;
    }

    public double calculateAccuracy(List<Prediction> predictions) {

        double correct = 0;

        for (int i = 0; i < predictions.size(); ++i) {
            NominalPrediction np = (NominalPrediction) predictions.get(i);
            if (np.predicted() == np.actual()) {
                ++correct;
            }
        }

        return 100 * correct / predictions.size();
    }

    public void runClassifier(SPLITMETHOD splitMethod) {

        // Classifier modelClassifier = null;
        List<Prediction> modelPredictions = null;
        double accuracy;
        Instances trainingInstances = null;
        Instances testInstances = null;
        Integer testIndices[] = null;

        Classifier classifier = null;

        switch (splitMethod) {
            case ALL:
                trainingInstances = this.allInstances;
                testInstances = this.allInstances;
                // testIndices = new Integer[] { 0, rawTrainingData.size() };
                testIndices = new Integer[] { 0, featureLabelMap.get(language).size() };
                break;
            case SPLIT:
                int allSize = this.allInstances.numInstances();
                int trainCutoff = (int) (allSize * trainFrac);
                int testSize = allSize - trainCutoff;
                trainingInstances = new Instances(allInstances, 0, trainCutoff);
                testInstances = new Instances(allInstances, trainCutoff, testSize);
                testIndices = new Integer[] { trainCutoff, featureLabelMap.get(language).size() };
                break;
            case KFOLD:
                Instances[][] split = crossValidationSplit(allInstances, kFold);
                Instances[] trainingSplits = split[0];
                Instances[] testingSplits = split[1];
                // testIndices = new Integer[] { 0, 0 };
                testIndices = new Integer[] { 0, featureLabelMap.get(language).size() };
                modelPredictions = new ArrayList<Prediction>();
                for (int i = 0; i < trainingSplits.length; ++i) {
                    trainingInstances = trainingSplits[i];
                    testInstances = testingSplits[i];
                    //					this.buildClassifier(trainingInstances, false);
                    classifier = this.buildClassifier(trainingInstances, false);
                    modelPredictions.addAll(this.testClassifier(testInstances));
                }
                testInstances = allInstances;
                break;
            default:
                trainingInstances = this.allInstances;
                testInstances = this.allInstances;
                // testIndices = new Integer[] { 0, rawTrainingData.size() };
                testIndices = new Integer[] { 0, featureLabelMap.get(language).size() };
                break;
            // stdout.println("Overall Accuracy=" +
            // calculateAccuracy(modelPredictions) + "\n");
        }
        if (modelPredictions == null) {
            // System.out.println("---------------------------------------------------------------");
            //			this.buildClassifier(trainingInstances, false);
            classifier = this.buildClassifier(trainingInstances, false);
            // System.out.println("---------------------------------------------------------------");
            modelPredictions = this.testClassifier(testInstances);
            // System.out.println("---------------------------------------------------------------");
        }

        // stdout.println("\n------------------------" + splitMethod +
        // "--------------------\n");
        Evaluation eval = null;
        try {
            eval = classify(classifier, trainingInstances, testInstances);

            this.printResults(testInstances, modelPredictions, testIndices[0], testIndices[1]);
            accuracy = this.calculateAccuracy(modelPredictions);
            stdout.println("-------------------------------------------");
            stdout.println("Testing on : " + modelPredictions.size() + " instances");
            stdout.println("Accuracy of the Classifier: " + accuracy);
            stdout.println("-------------------------------------------");
            printConfusionMatrix(eval);
        } catch (Exception e) {
            e.printStackTrace(stdout);
        }
        stdout.println("-------------------------------------------");

    }

    public String getPredictedClass(XpFeatureVector xpf) {
        Instances testInstances = getARFFMetaData();
        Double[] featureValues = getFeatureVectorFromXPF(xpf);
        prepareClassifierInstance(featureValues, null, testInstances);
        testInstances.setClassIndex(0);
        List<Prediction> predictions = testClassifier(testInstances);
        if (predictions != null) {
            for (Prediction currPredict : predictions) {

                return (String) this.getClassifierClass(currPredict.predicted());
            }
        }
        return null;
    }

    public static void main(String[] args) throws Exception {

        boolean readFromModel = false;
        IS_BASE_CLASS = true;

        XpEngine.init(readFromModel);
        //		Logger logger = LogFormatter.getLogger();
        //		logger.setLevel(Level.OFF);
        XpClassifierML.init(readFromModel);

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String dateStr = sdf.format(cal.getTime());

        double startTime = System.currentTimeMillis();

        // ClassifierML cml = new ClassifierML(ClassifierML.SENTIMENT_TYPE);
        //		XpFeatureSet.init();

        outputFile = "output/classifierStmtOutput_" + dateStr + "-" + LANGUAGE + ".txt";
        XpClassifierML cml = new XpClassifierML(LANGUAGE, CLASSIFICATION_TYPE.STATEMENT, ALGORITHM.RANDOMFOREST, readFromModel);
        //			cml.runClassifier(SPLITMETHOD.KFOLD);
        cml.runClassifier(SPLITMETHOD.ALL);

        outputFile = "output/classifierSentiOutput_" + dateStr + "-" + LANGUAGE + ".txt";
        XpClassifierML cml2 = new XpClassifierML(LANGUAGE, CLASSIFICATION_TYPE.SENTIMENT, ALGORITHM.RANDOMFOREST, readFromModel);
        //		cml2.runClassifier(SPLITMETHOD.KFOLD);
        cml2.runClassifier(SPLITMETHOD.ALL);

        // ClassifierML cml = new ClassifierML(ClassifierML.SENTIMENT_TYPE,
        // ALGORITHM.SVM);
        // cml.prepareTrainingDataForClassifier();

        // cml.runClassifier(SPLITMETHOD.SPLIT);
        // cml.runClassifier(SPLITMETHOD.KFOLD);

        // cml.runClassifier(ALGORITHM.SVM, SPLITMETHOD.SPLIT);

        // WekaSentiClassifierML classifierML = new WekaSentiClassifierML();
        // classifierML.prepareTrainingDataForClassifier();
        //
        // classifierML.runClassifier(ALGORITHM.SVM, SPLITMETHOD.ALL);
        // classifierML.runClassifier(ALGORITHM.SVM, SPLITMETHOD.SPLIT);
        // classifierML.runClassifier(ALGORITHM.SVM, SPLITMETHOD.KFOLD);

        // classifierML.runClassifier(ALGORITHM.NB, SPLITMETHOD.ALL);
        // classifierML.runClassifier(ALGORITHM.NB, SPLITMETHOD.SPLIT);
        // classifierML.runClassifier(ALGORITHM.NB, SPLITMETHOD.KFOLD);

        // classifierML.runClassifier(ALGORITHM.LOGIT, SPLITMETHOD.ALL);
        // classifierML.runClassifier(ALGORITHM.LOGIT, SPLITMETHOD.SPLIT);
        // classifierML.runClassifier(ALGORITHM.LOGIT, SPLITMETHOD.KFOLD);

        //		cml.setStdout(System.out); // print to console
        System.out.println("Total time taken for Weka classification: " + ((System.currentTimeMillis() - startTime) / 1000) + " seconds");

    }

}