package  com.abzooba.xpresso.machinelearning;

import java.util.ArrayList;
import java.util.List;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.textanalytics.wordvector.WordVector;

import weka.clusterers.Clusterer;
import weka.clusterers.EM;
import weka.clusterers.HierarchicalClusterer;
import weka.clusterers.SimpleKMeans;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

public class XpClustering {

    //	private static String modelFile;

    public Instances allInstances = null;

    public Clusterer clusterer;
    private int featureListSize = 300;
    private String category;
    private List<String> entityList;
    private int numClusters = 5;
    private int emaxMaxClusters = 3000;
    private int emaxMaxIterations = 100;
    private SimpleKMeans kmeans;
    private EM emax;
    private HierarchicalClusterer hierarchical;
    private Languages language;

    public enum ALGORITHM {
        EM, KMEANS, HIERARCHY, COBWEB
    }

    public static ALGORITHM algorithm = ALGORITHM.KMEANS;

    public Clusterer getClusterer() {
        return clusterer;
    }

    public XpClustering(Languages language, String category, List<String> entityList) throws Exception {
        this.language = language;
        this.category = category;
        this.entityList = entityList;
        if (XpConfig.IS_WORDVECTOR_ASPECTS_MATCH == 4 || XpConfig.IS_WORDVECTOR_ASPECTS_MATCH == 6) {
            if (entityList.size() < 10) {
                numClusters = entityList.size();
            } else {
                numClusters = 3;
            }
        } else if (XpConfig.IS_WORDVECTOR_ASPECTS_MATCH == 3) {
            numClusters = 3;
        } else if (XpConfig.IS_WORDVECTOR_ASPECTS_MATCH == 2) {
            numClusters = 5;
        }
        if (this.entityList.size() < numClusters) {
            numClusters = this.entityList.size();
        }
        initiateModel();
        List<double[]> entityVectorList = vectorizeConceptsForCategory(this.category, this.entityList);
        prepareTrainingDataForClusterer(entityVectorList);
        if (allInstances.numInstances() > 0) {
            buildClusterer();
        }
    }

    public void initiateModel() throws Exception {
        switch (algorithm) {
            case KMEANS:
                kmeans = new SimpleKMeans();
                kmeans.setNumClusters(numClusters);
                break;
            case EM:
                emax = new EM();
                emax.setMaximumNumberOfClusters(emaxMaxClusters);
                emax.setMaxIterations(emaxMaxIterations);
                break;
            case HIERARCHY:
                hierarchical = new HierarchicalClusterer();
                break;
            default:
                kmeans = new SimpleKMeans();
                kmeans.setNumClusters(numClusters);
        }
    }

    public void buildClusterer() {
        try {
            switch (algorithm) {
                case KMEANS:
                    //					System.out.println("In build clusterer, Count of instances - " + allInstances.numInstances());
                    kmeans.buildClusterer(allInstances);
                    break;
                case EM:
                    emax.buildClusterer(allInstances);
                    break;
                case HIERARCHY:
                    hierarchical.buildClusterer(allInstances);
                    break;
                default:
                    kmeans.buildClusterer(allInstances);
                    break;
            }
            //			weka.core.SerializationHelper.write(modelFile, clusterer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<double[]> listClusterRepresentatives() {
        List<double[]> centroidList = null;
        //		List<double[]> centroidList = new ArrayList<double[]>();
        if (allInstances != null && allInstances.numInstances() > 0) {
            switch (algorithm) {
                case KMEANS:
                    Instances clusterCentroids = kmeans.getClusterCentroids();
                    centroidList = new ArrayList<double[]>();
                    for (Instance clusterCentroid : clusterCentroids) {
                        centroidList.add(clusterCentroid.toDoubleArray());
                    }

                    //					clusterCentroids.forEach(centroid -> {
                    //						double[] centroidArr = centroid.toDoubleArray();
                    //						//						Double[] centroidObjArr = new Double[featureListSize];
                    //						//						for (int i = 0; i < featureListSize; i++)
                    //						//							centroidObjArr[i] = Double.valueOf(centroidArr[i]);
                    //						centroidList.add(centroidArr);
                    //					});
                    //				System.out.println("clusterCentroids : " + clusterCentroids.toString());
                    break;
                case EM:
                    System.out.println("maximum number of clusters : " + emax.getMaximumNumberOfClusters());
                    break;
                case HIERARCHY:
                    try {
                        System.out.println("maximum number of clusters : " + hierarchical.numberOfClusters());
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    break;
                case COBWEB:
                    try {
                        System.out.println("maximum number of clusters : " + hierarchical.numberOfClusters());
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    break;
            }
        } else if (this.entityList.size() < numClusters) {
            //			centroidList.addAll(vectorizeConceptsForCategory(this.category, this.entityList));
            centroidList = vectorizeConceptsForCategory(this.category, this.entityList);
        }
        return centroidList;
    }

    public Instances defineAttributes() {
        ArrayList<Attribute> features = new ArrayList<Attribute>();

        for (int i = 0; i < featureListSize; ++i) {
            features.add(new Attribute("feature_" + i, i));
        }
        return new Instances("InstancesForClustering", features, 0);
    }

    public void prepareClustererInstance(double[] featureValues, Instances instances) {

        double[] instanceValue = new double[instances.numAttributes()];
        for (int i = 0; i < featureValues.length; ++i)
            instanceValue[i] = featureValues[i];

        try {
            Instance currInstance = new DenseInstance(1.0, instanceValue);
            instances.add(currInstance);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void prepareTrainingDataForClusterer(List<double[]> entityVectorList) {

        //		Instances rawTrainingInstances = defineAttributes();
        //
        //		for (Double[] featureValues : entityVectorList)
        //			this.prepareClustererInstance(featureValues, rawTrainingInstances);
        //		allInstances = rawTrainingInstances;

        allInstances = defineAttributes();
        for (double[] featureValues : entityVectorList)
            this.prepareClustererInstance(featureValues, allInstances);

        //		System.out.println("entity vector list size : " + entityVectorList.size() + " instances size : " + allInstances.numInstances());

    }

    private List<double[]> vectorizeConceptsForCategory(String category, List<String> entityList) {
        //		System.out.println("Inside vectorizeCon");
        List<double[]> entityVectorList = new ArrayList<double[]>();
        for (String entity : entityList) {

            //			if (XpConfig.IS_GLOVE_ASPECTS_MATCH == 6) {
            //				double[] vector = GoogleW2V.getWordVector(entity);
            //				if (vector != null) {
            //					entityVectorList.add(vector);
            //				}
            //			} else {

            List<?> floatVector = WordVector.getWordVector(language, entity);
            if (floatVector != null && !floatVector.isEmpty()) {
                //				System.out.println("vector size - " + floatVector.size());
                double[] gloveVector = new double[featureListSize];
                for (int i = 0; i < featureListSize; i++) {
                    double tempX = (Double) floatVector.get(i);
                    //					Double tempObj = Double.parseDouble(floatVector.get(i).toString());
                    gloveVector[i] = tempX;
                }
                entityVectorList.add(gloveVector);
            }
            //			}
        }
        return entityVectorList;
    }

}