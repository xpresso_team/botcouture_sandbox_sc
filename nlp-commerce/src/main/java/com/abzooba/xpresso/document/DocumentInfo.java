package  com.abzooba.xpresso.document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DocumentInfo {

    Map<String, Integer> entityCountMap = new HashMap<String, Integer>();
    List<Double> documentVector = new ArrayList<Double>();
    List<DocumentCategoryInfo> categoryList = new ArrayList<DocumentCategoryInfo>();
    Set<String> categorySet = new HashSet<String>();
    public static int maxCategoryCount = 3;
    DocumentCategoryInfo tempObj = null;

    public void addEntity(String entity, int count) {
        Integer entityCount = entityCountMap.get(entity);
        if (entityCount != null)
            entityCountMap.put(entity, entityCount.intValue() + count);
        else
            entityCountMap.put(entity, count);
    }

    public Integer getEntityCount(String entity) {
        return entityCountMap.get(entity);
    }

    public List<Double> getDocumentVector() {
        return this.documentVector;
    }

    public void setDocumentVector(List<Double> documentVector) {
        this.documentVector = documentVector;
    }

    public class DocumentCategoryInfo {
        String subCategory = null;
        String category = null;
        double score = 0.0;

        public DocumentCategoryInfo(String category, double score) {
            this.category = category;
            this.score = score;
        }

        public DocumentCategoryInfo(String subCategory, String category, double score) {
            this.subCategory = subCategory;
            this.category = category;
            this.score = score;
        }

        public String getSubCategory() {
            return subCategory;
        }

        public void setSubCategory(String subCategory) {
            this.subCategory = subCategory;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public double getScore() {
            return score;
        }

        public void setScore(Double score) {
            this.score = score;
        }
    }

    public void addCategoryToSortedMap(String subCategory, String category, double score) {
        DocumentCategoryInfo docCatInfo = new DocumentCategoryInfo(subCategory, category, score);
        if (categoryList.size() == 0) {
            categoryList.add(docCatInfo);
            categorySet.add(category);
            return;
        }
        int listSize = categoryList.size();
        boolean betterUpdate = false;
        int lastIndex = -1;
        categoryList.add(docCatInfo);

        for (int i = listSize; i > 0; i--) {
            DocumentCategoryInfo currentCatg = categoryList.get(i);
            double currScore = currentCatg.getScore();
            DocumentCategoryInfo prevCatg = categoryList.get(i - 1);
            double prevScore = prevCatg.getScore();
            if (currentCatg.getCategory().equalsIgnoreCase(prevCatg.getCategory())) {
                if (currScore <= prevScore) {
                    categoryList.remove(i);
                    break;
                } else {
                    categoryList.remove(i - 1);
                    betterUpdate = true;
                }

            } else {
                if (currScore <= prevScore) {
                    lastIndex = i;
                    break;
                } else {
                    tempObj = categoryList.get(i);
                    categoryList.set(i, prevCatg);
                    categoryList.set(i - 1, tempObj);
                }
            }
        }

        if (!betterUpdate && categorySet.contains(category) && lastIndex > 0)
            categoryList.remove(lastIndex);
        categorySet.add(category);
        if (categoryList.size() > maxCategoryCount) {
            String lastCategory = categoryList.get(categoryList.size() - 1).getCategory();
            categorySet.remove(lastCategory);
            categoryList.remove(categoryList.size() - 1);
        }

    }

    public void showCategoryPreference() {
        StringBuilder sb = new StringBuilder();
        for (DocumentCategoryInfo categoryInfo : categoryList) {
            sb.append(categoryInfo.getCategory());
            sb.append(" ");
            sb.append(categoryInfo.getScore());
            sb.append(" ");
        }
        System.out.println(sb.toString());
    }

    public String categoryListToString(boolean isSubCategory) {
        StringBuilder sb = new StringBuilder();
        for (DocumentCategoryInfo categoryInfo : categoryList) {
            if (isSubCategory)
                sb.append(categoryInfo.getSubCategory());
            else
                sb.append(categoryInfo.getCategory());
            sb.append(" ");
        }
        return sb.toString();
    }
}