/**
 *
 */
package  com.abzooba.xpresso.searchengine;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.utils.FileIO;

/**
 * @author Koustuv Saha
 * 18-Oct-2014 1:47:35 pm
 * XpressoV2.7  BingSearchAPI
 */
public class BingSearchAPI implements SearchEngine {

    //	private static final String ACCOUNT_KEY = "BLv+2ANZgreuLwtiP4AcMmPNoCxVRT4z1uF3Hy0QI/M";
    //	private static final String ACCOUNT_KEY = "I54HgWcL3z3M1lZ/HR2XcBaRLfeDBlMoE5IAi5q6cFE";

    private static Map<String, Integer> queryHitsMap = new ConcurrentHashMap<String, Integer>();

    private static final String BING_CONSTANT_URL = "https://api.datamarket.azure.com/Data.ashx/Bing/Search/Composite?Sources=%27web%27&Query=%27";
    private static final String ACCOUNT_KEY = "F5gC4aEkXzRhC3MhgKg4w6KEMICmCbvtFQfWTY7cS9Y";
    private static String ENCODED_KEY;
    private static Semaphore AVAILABLE_LOCK;

    //	private static final String ACCOUNT_KEY = "S/FELy8j8iMA9fw9i9MbfubczhMs8+YhedTMCD3Z+Xg=";
    //	private static final String ACCOUNT_KEY = "RzttHzw/yhflS0CyKzEGgMLd2sqwwm8kgYEmz28pzkk=";

    //	private static final String ACCOUNT_KEY = "+HUI9COdnEjHGYHdEDta//3DGj8DgL7JHvZMTSoDQjM=";
    //	private static final String ACCOUNT_KEY = "B9weLdRz/XDLrHWN/sMTlNM8Kl0+4ksd8LbBVAWDols";

    //	private AzureSearchCompositeQuery aq;
    private String query;
    private static final String phraseScoreFileName = "data/bingSearchHits.txt";

    public static void init() throws Exception {
        //		RedisUtils.init();
        List<String> phraseScoresList = new ArrayList<String>();
        FileIO.read_file(phraseScoreFileName, phraseScoresList);
        for (String phraseScore : phraseScoresList) {
            String[] phraseScoreArr = phraseScore.split("\t");
            if (phraseScoreArr.length == 2) {
                //				phraseLogORscoreMap.put(phraseScoreArr[0], Double.parseDouble(phraseScoreArr[1]));
                queryHitsMap.put(phraseScoreArr[0], Integer.parseInt(phraseScoreArr[1]));
            }
        }
        AVAILABLE_LOCK = new Semaphore(20);
        ENCODED_KEY = String.format("Basic %s", new String(Base64.getEncoder().encode((ACCOUNT_KEY + ":" + ACCOUNT_KEY).getBytes())));

    }

    //	private static Map<String, Integer> bingQueryMap = new ConcurrentHashMap<String, Integer>();
    //	private static final String bingQueryFileName = "data/bingQuery.txt";

    //	public static void init() {
    //		List<String> phraseHitCountList = new ArrayList<String>();
    //		FileIO.read_file(bingQueryFileName, phraseHitCountList);
    //		for (String phraseScore : phraseHitCountList) {
    //			String[] phraseScoreArr = phraseScore.split("\t");
    //			bingQueryMap.put(phraseScoreArr[0], Integer.parseInt(phraseScoreArr[1]));
    //		}
    //	}

    public static String constructQuery(String phrase, String element, int distance) {
        String query = null;
        if (element == null) {
            query = "\"" + phrase + "\"";
        } else {
            query = "\"" + phrase + "\"" + " NEAR:" + distance + " " + "\"" + element + "\"";
        }
        return query;
    }

    //	public static void writeMap() {
    //		FileIO.write_file(bingQueryMap, bingQueryFileName, false);
    //	}

    public BingSearchAPI(String query) {
        //		this.query = query;
        this.query = query.replaceAll("\uFEFF", "");
    }

    //	public int getHitCount() {
    //
    //		Integer hitCount;
    //		hitCount = queryHitsMap.get(this.query);
    //		RedisUtils redis = new RedisUtils();
    //		//		if (hitCount == null) {
    //		//			String hitCountStr = redis.getValue(this.query);
    //		//			if (hitCountStr != null) {
    //		//				hitCount = Integer.parseInt(hitCountStr);
    //		//			}
    //		//		}
    //		if (hitCount == null) {
    //			try {
    //
    //				aq = new AzureSearchCompositeQuery();
    //				aq.setAppid(ACCOUNT_KEY);
    //				aq.setQuery(query);
    //				aq.setSources(new AbstractAzureSearchQuery.AZURESEARCH_QUERYTYPE[] { AbstractAzureSearchQuery.AZURESEARCH_QUERYTYPE.WEB, });
    //				//				System.out.println("Doing for: " + this.query);
    //				aq.doQuery();
    //
    //				AzureSearchResultSet<?> ars = this.aq.getQueryResult();
    //				if (ars != null) {
    //					hitCount = (int) (long) ars.getWebTotal();
    //					System.out.println(this.query + ":\t" + hitCount);
    //				} else {
    //					hitCount = 0;
    //				}
    //				queryHitsMap.put(query, hitCount);
    //				redis.addValue(query, hitCount.toString());
    //			} catch (Exception ex) {
    //				hitCount = 0;
    //			}
    //
    //			//			RedisUtils.getDB().set(query, hitCount.toString());
    //
    //			//			return (int) hitCount;
    //		}
    //		return hitCount;
    //	}

    public int getHitCount() {
        try {
            AVAILABLE_LOCK.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Integer hitCount;
        hitCount = queryHitsMap.get(this.query);
        //		RedisUtils redis = new RedisUtils();
        //		if (hitCount == null) {
        //			String hitCountStr = redis.getValue(this.query);
        //			if (hitCountStr != null) {
        //				hitCount = Integer.parseInt(hitCountStr);
        //			}
        //		}
        if (hitCount == null) {
            try {

                //				DefaultHttpClient client = new DefaultHttpClient();
                //				HttpHost _targetHost = new HttpHost("api.datamarket.azure.com", 443, "https");
                //
                //				client.getCredentialsProvider().setCredentials(new AuthScope(_targetHost.getHostName(), _targetHost.getPort()), new UsernamePasswordCredentials(ACCOUNT_KEY, ACCOUNT_KEY));
                //				URI uri;
                //				String full_path = "/Data.ashx/Bing/SearchWeb/v1/" + "Composite";
                //				String full_query = "Query='" + this.query + "'";
                //				uri = new URI("https", "api.datamarket.azure.com", full_path, full_query, null);
                //				// Bing and java URI disagree about how to represent + in query
                //				// parameters. This is what we have to do instead...
                //				uri = new URI(uri.getScheme() + "://" + uri.getAuthority() + uri.getPath() + "?" + uri.getRawQuery().replace("+", "%2b"));
                //
                //				HttpGet get = new HttpGet(uri);
                //
                //				get.addHeader("Accept", "application/xml");
                //				get.addHeader("Content-Type", "application/xml");
                //
                //				HttpResponse _responsePost = client.execute(get);
                //
                //				System.out.println(_responsePost.toString());
                //				HttpEntity _resEntity = _responsePost.getEntity();
                //
                //				System.out.println(_resEntity.getContent());

                //				ODataConsumer c = ODataConsumers.newBuilder("https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/").setClientBehaviors(OClientBehaviors.basicAuth("accountKey", "{your account key here}")).build();
                //
                //				OQueryRequest<OEntity> oRequest = c.getEntities("Web").custom("Query", "stackoverflow bing api");
                //
                //				Enumerable<OEntity> entities = oRequest.execute();

                //				String bingUrl = "https://api.datamarket.azure.com/Bing/Search/Web?Query='" + this.query + "'";
                //
                //				byte[] accountKeyBytes = Base64.getEncoder().encode((ACCOUNT_KEY + ":" + ACCOUNT_KEY).getBytes());
                //				String accountKeyEnc = new String(accountKeyBytes);
                //				accountKeyEnc = String.format("Basic %s", accountKeyEnc);
                //
                //				URL url = new URL(bingUrl);
                //				URLConnection urlConnection = url.openConnection();
                //				urlConnection.setRequestProperty("Authorization", "Basic " + accountKeyEnc);
                //				//				<d:WebTotal m:type="Edm.Int64">900000</d:WebTotal>
                //
                //				BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                //
                //				StringBuilder netLine = new StringBuilder();
                //				String inputLine;
                //				while ((inputLine = br.readLine()) != null) {
                //					netLine.append(inputLine);
                //				}
                //				System.out.println("NetLine:\n" + netLine);

                //				String bingrl = "https://api.datamarket.azure.com/Data.ashx/Bing/Search/Composite?Sources=%27web%27&Query=%27Test%27&$format=Json";

                String encodedQuery = URLEncoder.encode(this.query, "UTF-8");
                String bingUrl = BING_CONSTANT_URL + encodedQuery + "%27&$format=Json";
                //				System.out.println(bingUrl);
                //				String bingUrl = "https://api.datamarket.azure.com/Bing/Search/Web?Query='sexy'&$format=json";
                //				bingURL = URLEncoder.encode(bingUrl)

                //				String accountKey = "key";

                //				byte[] encoding = Base64.getEncoder().encode((ACCOUNT_KEY + ":" + ACCOUNT_KEY).getBytes());
                //				String accountKeyEnc = new String(encoding);

                URL url = new URL(bingUrl);

                URLConnection urlConnection = url.openConnection();
                //				urlConnection.setRequestProperty("Authorization", String.format("Basic %s", accountKeyEnc));
                urlConnection.setRequestProperty("Authorization", ENCODED_KEY);
                //				ENCODED_KEY
                BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;
                StringBuffer sb = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    //					System.out.println(inputLine);
                    sb.append(inputLine);
                }
                in.close();
                //				System.out.println(sb.toString());
                JSONObject webResult = new JSONObject(sb.toString());
                JSONObject objectD = (JSONObject) webResult.get("d");
                JSONArray results = (JSONArray) objectD.get("results");
                JSONObject firstObject = results.getJSONObject(0);
                hitCount = Integer.parseInt((String) firstObject.get("WebTotal"));
                System.out.println(query + "\t" + hitCount);
                queryHitsMap.put(query, hitCount);
            } catch (Exception ex) {
                ex.printStackTrace();
                hitCount = 0;
            }
            //			RedisUtils.getDB().set(query, hitCount.toString());

            //			return (int) hitCount;
        }
        AVAILABLE_LOCK.release();
        return hitCount;
    }

    public static void writeMap() {
        FileIO.write_file(queryHitsMap, phraseScoreFileName, false, XpConfig.TAB_DELIMITER);
    }

    public static void main(String[] args) throws Exception {
        BingSearchAPI.init();
        BingSearchAPI bsa = new BingSearchAPI("great service");
        System.out.println("Count " + bsa.getHitCount());
        writeMap();

    }

}