package  com.abzooba.xpresso.searchengine;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.expressions.emotion.EmotionLexicon;
import com.abzooba.xpresso.searchengine.SearchEngine.SEARCH_ENGINE;
import com.abzooba.xpresso.utils.FileIO;

public class LogOddRatioCalculator {

    //	private static Map<String, Double> phraseLogORscoreMap = new ConcurrentHashMap<String, Double>();
    private static Map<String, Integer> phraseSeedHitsMap = new ConcurrentHashMap<String, Integer>();
    // public static String bingUrl;
    //	public static String ACCOUNT_KEY;
    private static final String[] posSeedArr = { "good", "wonderful", "spectacular" };
    private static final String[] negSeedArr = { "bad", "horrible", "awful" };
    private static final String phraseScoreFileName = "data/phraseSeedHits.txt";

    //	private static final String phraseScoreFileName = "data/phraseSeedHits.txt";

    //	private static final String phraseScoreFileName = "data/phraseScore.txt";

    public static void init() throws Exception {
        // bingUrl = "https://www.bing.com/search?q=";
        //		ACCOUNT_KEY = "BLv+2ANZgreuLwtiP4AcMmPNoCxVRT4z1uF3Hy0QI/M";
        BingSearchAPI.init();

        List<String> phraseScoresList = new ArrayList<String>();
        FileIO.read_file(phraseScoreFileName, phraseScoresList);
        for (String phraseScore : phraseScoresList) {
            String[] phraseScoreArr = phraseScore.split("\t");
            if (phraseScoreArr.length == 2) {
                //				phraseLogORscoreMap.put(phraseScoreArr[0], Double.parseDouble(phraseScoreArr[1]));
                phraseSeedHitsMap.put(phraseScoreArr[0], Integer.parseInt(phraseScoreArr[1]));

            }
        }
    }

    //	private static long getBingHitCount(String query) throws Exception {
    //
    //		long hitCount = 0;
    //		// String ACCOUNT_KEY = "BLv+2ANZgreuLwtiP4AcMmPNoCxVRT4z1uF3Hy0QI/M";
    //		AzureSearchCompositeQuery aq2 = new AzureSearchCompositeQuery();
    //
    //		aq2.setAppid(ACCOUNT_KEY);
    //		aq2.setQuery(query);
    //		aq2.setSources(new AbstractAzureSearchQuery.AZURESEARCH_QUERYTYPE[] { AbstractAzureSearchQuery.AZURESEARCH_QUERYTYPE.WEB, });
    //		aq2.doQuery();
    //		AzureSearchResultSet<?> ars2 = aq2.getQueryResult();
    //
    //		// Long numberOfWebResults = ars2.getWebTotal();
    //		// System.out.println("Total results: " + ars2.getWebTotal());
    //		hitCount = ars2.getWebTotal();
    //		return hitCount;
    //
    //	}
    //
    //	public static double getLogORRatio(String phrase) throws Exception {
    //		double logORScore = 0.0;
    //		long totalPosCount = 0;
    //		long totalNegCount = 0;
    //		for (String element : posSeedArr) {
    //			String query = "\"" + phrase + "\"" + " NEAR:10 " + "\"" + element + "\"";
    //			long count = getBingHitCount(query);
    //			System.out.println(query + ": " + count);
    //			totalPosCount += count;
    //			Thread.sleep(5);
    //		}
    //		for (String element : negSeedArr) {
    //			String query = "\"" + phrase + "\"" + " NEAR:10 " + "\"" + element + "\"";
    //			long count = getBingHitCount(query);
    //			System.out.println(query + ": " + count);
    //			totalNegCount += count;
    //			Thread.sleep(5);
    //		}
    //		logORScore = Math.log10((double) (totalPosCount + 1) / (double) (totalNegCount + 1));
    //		return logORScore;
    //	}

    private static long getComparisionScore(String phrase, SEARCH_ENGINE engine_name, String[] seedArr) throws Exception {
        long totalCount = (long) 0;
        for (String element : seedArr) {
            totalCount += getHitCount(phrase, element, engine_name);
            //			Random rand = new Random();
            //			int randNum = rand.nextInt((20 - 6) + 1) + 5;
            //			Thread.sleep(randNum);
        }
        return totalCount;
    }

    private static int getHitCount(String phrase, String searchElement, SEARCH_ENGINE engine_name) {
        phrase = phrase.toLowerCase();
        String key = phrase;
        if (searchElement != null) {
            searchElement = searchElement.toLowerCase();
            key += "##" + searchElement;
        }

        Integer hitCount = phraseSeedHitsMap.get(key);
        if (hitCount != null) {
            return hitCount;
        } else {
            String query = null;
            //			int currCount = 0;
            SearchEngine se = null;
            switch (engine_name) {
                case BING_API:
                    query = BingSearchAPI.constructQuery(phrase, searchElement, 10);
                    se = new BingSearchAPI(query);
                    break;
                case BING_CRAWL:
                    break;
                case GOOGLE_API:
                    break;
                case GOOGLE_CRAWL:
                    query = GoogleSearchCrawler.constructQuery(phrase, searchElement, 10);
                    se = new GoogleSearchCrawler(query);
                    break;
            }

            if (se != null) {
                hitCount = se.getHitCount();
            }
            if (hitCount != null) {
                phraseSeedHitsMap.put(key, hitCount);
                return hitCount;
            } else {
                return 0;
            }
        }
    }

    private static double getPMIvalue(String phrase, SEARCH_ENGINE engine_name, List<String> seedArr) throws Exception {
        double categoryPMI = 0;
        int counter = 0;
        int maxSeedCount = 3;
        for (String element : seedArr) {

            //			System.out.println(query + ": " + currCount);
            //			double startTime = System.currentTimeMillis();

            int phraseScore = getHitCount(phrase, null, engine_name);
            int elementScore = getHitCount(element, null, engine_name);
            long comparisionScore = getHitCount(phrase, element, engine_name);
            //			System.out.println("Hit Count Received for: " + phrase + "\tElement: " + element + "\t" + comparisionScore + "\tTime: " + (System.currentTimeMillis() - startTime) + " milliseconds");

            double seedPMI;
            if (comparisionScore == 0 || phraseScore == 0 || elementScore == 0) {
                seedPMI = 0;
            } else {
                seedPMI = Math.log10((double) comparisionScore / Math.log10(((double) phraseScore * (double) elementScore)));
                //				System.out.println("PMI for " + phrase + " and " + element + " :-> " + seedPMI);
            }

            categoryPMI += seedPMI;

            //			totalCount += currentCount;

            if (++counter > (maxSeedCount - 1)) {
                break;
            }
            //			Random rand = new Random();
            //			int randNum = rand.nextInt((20 - 6) + 1) + 5;
            //			Thread.sleep(randNum);
        }
        categoryPMI = categoryPMI / maxSeedCount;
        //		System.out.println("*****PMI for " + phrase + " :-> " + categoryPMI);
        //		categoryPMI = 1 / (1 + Math.exp((-1) * categoryPMI));
        //		System.out.println("*****Sigmoid PMI for " + phrase + " :-> " + categoryPMI);
        return categoryPMI;
    }

    public static Double getLogORRatio(String phrase, SEARCH_ENGINE engine_name) throws Exception {

        //		Double logORscore = phraseLogORscoreMap.get(phrase);
        Double logORscore = null;

        if (logORscore == null) {
            //			System.out.println("Phrase not found: " + phrase);
            logORscore = 0.0;
            long totalPosCount = 0;
            long totalNegCount = 0;
            totalPosCount = getComparisionScore(phrase, engine_name, posSeedArr);
            totalNegCount = getComparisionScore(phrase, engine_name, negSeedArr);
            logORscore = Math.log10((double) (totalPosCount + 1) / (double) (totalNegCount + 1));
            //			phraseLogORscoreMap.put(phrase, logORscore);
        }
        return logORscore;
    }

    public static double getPMI(String phrase, String category, SEARCH_ENGINE engine_name) {
        //	public static Double getLogORRatio(String phrase, String category, SEARCH_ENGINE engine_name) {

        Double logORscore = null;

        //		Double logORscore = phraseLogORscoreMap.get(phrase);

        //			System.out.println("Phrase not found: " + phrase);
        logORscore = 0.0;
        double totalCount = 0;
        try {
            totalCount = getPMIvalue(phrase, engine_name, EmotionLexicon.getEmotionSeedVector(category));
            //			totalCount = getPMI(phrase, engine_name, EmotionLexicon.getEmotionSeedVector(category));

            //				totalPosCount = getComparisionScore(phrase, engine_name, posSeedArr);
            //				totalNegCount = getComparisionScore(phrase, engine_name, negSeedArr);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        logORscore = (double) totalCount;
        //			return (double) totalCount;

        //			logORscore = Math.log10((double) (totalPosCount + 1) / (double) (totalNegCount + 1));
        //		phraseLogORscoreMap.put(phrase, logORscore);
        //		}
        return logORscore;
    }

    //	public static void writeMap() {
    //		List<String> phraseScoreList = new ArrayList<String>();
    //		for (Map.Entry<String, Double> entry : phraseLogORscoreMap.entrySet()) {
    //			phraseScoreList.add(entry.getKey() + "\t" + entry.getValue());
    //		}
    //		FileIO.write_file(phraseScoreList, phraseScoreFileName, false);
    //	}
    public static void writeMap() {
        //		FileIO.write_file(phraseLogORscoreMap, phraseScoreFileName, false, XpConfig.TAB_DELIMITER);
        FileIO.write_file(phraseSeedHitsMap, phraseScoreFileName, false, XpConfig.TAB_DELIMITER);
        //		BingSearchAPI.writeMap();
    }

    public static void main(String[] args) throws Exception {
        init();

        // for (int i = 0; i < 100; i++) {
        // String phrase = "most relevant";
        // double logORScore = LogOddRatioCalculator.getLogORRatio(phrase);
        // System.out.println(phrase + " :: " + logORScore);
        //
        // }

        //		String phrase = "predictable sales";
        //		double logORScore = LogOddRatioCalculator.getLogORRatio(phrase);
        //		System.out.println(phrase + " :: " + logORScore);

    }
}