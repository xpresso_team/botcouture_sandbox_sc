/**
 *
 */
package  com.abzooba.xpresso.searchengine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Koustuv Saha
 * 18-Oct-2014 9:53:33 am
 * XpressoV2.7  GoogleSearchCrawler
 */
public class GoogleSearchCrawler implements SearchEngine {
    private static final Pattern NUM_RESULTS_PATTERN = Pattern.compile("\"resultStats\">(.*)\\sresults<", Pattern.CASE_INSENSITIVE);

    private static final String _userAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36";
    private final String _url;
    private URL _url_obj;
    private HttpURLConnection _connection;
    private int _responseCode;
    private StringBuilder _totalResults;
    private final int _quantity;
    private final int _start;
    private final String _searchTerm;

    public static String constructQuery(String phrase, String element, int distance) {
        String query = "\"" + element + "\"" + " AROUND(" + distance + ") " + "\"" + phrase + "\"";
        return query;

    }

    public GoogleSearchCrawler(String query) {
        _url = "http://www.google.com";

        //		_userAgent = "(Mozilla/5.0 (Windows; U; Windows NT 6.0;en-US; rv:1.9.2)" + " Gecko/20100115 Firefox/3.6)";
        //		_userAgent = "(Mozilla/5.0 (Windows; U; Windows NT 6.0;en-US; rv:1.9.2)" + " Gecko/20100115 Firefox/3.6)";
        //		_userAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36";

        _quantity = 100;
        _start = 0;
        _searchTerm = query;
    }

    public int responseCode() {
        return _responseCode;
    }

    public StringBuilder getResults() {
        return _totalResults;
    }

    private void getPageSourceCode() {
        try {
            //			_url_obj = new URL(_url + "/search?num=" + _quantity + "&start=" + _start + "&hl=en&meta=&q=%40\"" + _searchTerm + "\"");
            String _searchString = URLEncoder.encode(_searchTerm, "UTF-8");
            //			_url_obj = new URL(_url + "/search?num=" + _quantity + "&start=" + _start + "&hl=en&meta=&q=%40\"" + _searchString + "\"");
            //			_url_obj = new URL(_url + "/search?num=" + _quantity + "&start=" + _start + "&hl=en&meta=&q=\"" + _searchString + "\"");

            _url_obj = new URL(_url + "/search?num=" + _quantity + "&start=" + _start + "&hl=en&meta=&q=" + _searchString);
            System.out.println("Search Object: " + _url_obj.toString());

            //			System.out.println("URL called: " + _url_obj);
            _connection = (HttpURLConnection) _url_obj.openConnection();
            _connection.setRequestMethod("GET");
            _connection.setRequestProperty("User-Agent", _userAgent);
            _responseCode = _connection.getResponseCode();
            BufferedReader in = new BufferedReader(new InputStreamReader(_connection.getInputStream()));
            _totalResults = new StringBuilder();
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                _totalResults.append(inputLine);
            }
            in.close();
            System.out.println(_totalResults.toString());
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
        //		return _totalResults.toString();

    }

    public int getHitCount() {
        this.getPageSourceCode();
        String hitCountStr = null;
        //		Pattern numResultsPat = Pattern.compile("(<div id=\"resultStats\">(.*) results<nobr>)", Pattern.CASE_INSENSITIVE);
        //		Pattern numResultsPat = Pattern.compile("\"resultStats\">(.*)\\sresults<", Pattern.CASE_INSENSITIVE);
        Matcher mat = NUM_RESULTS_PATTERN.matcher(_totalResults.toString());
        if (mat.find()) {
            //			System.out.println("Found Match");
            //			hitCount = Long.parseLong(mat.group(1));
            hitCountStr = mat.group(1);
        }
        //		Long hitCount = (long) 0;
        int hitCount = 0;
        if (hitCountStr != null) {
            hitCountStr = hitCountStr.replaceAll("About", "").replaceAll(",", "").replaceAll(" ", "");
            //			hitCount = Long.parseLong(hitCountStr);
            hitCount = Integer.parseInt(hitCountStr);
        }

        return hitCount;
    }

    public static void main(String[] args) {
        GoogleSearchCrawler gsc = new GoogleSearchCrawler("sachin tendulkar");
        //		gsc.search();
        //		String pageSourceCode = gsc.getPageSourceCode();
        System.out.println("Hit Counts: " + gsc.getHitCount());
    }
}