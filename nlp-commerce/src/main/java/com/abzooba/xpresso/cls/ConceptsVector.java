/**
 *
 */
package  com.abzooba.xpresso.cls;

/**
 * @author Koustuv Saha
 * 01-Sep-2014 5:12:35 pm
 * XpressoV2.6  ConceptsVector
 */
public class ConceptsVector {

    private String sentencePat;
    private String concept;
    private String sentimentClass;
    private String sentence;
    private Boolean isAddConceptToDB;

    public ConceptsVector(String sentencePat, String concept, String sentimentClass, String sentence, Boolean isAddConceptToDB) {
        this.sentencePat = sentencePat;
        this.concept = concept;
        this.sentimentClass = sentimentClass;
        this.sentence = sentence;
        this.isAddConceptToDB = isAddConceptToDB;
    }

    /**
     * @return the sentencePat
     */
    public String getSentencePat() {
        return sentencePat;
    }

    /**
     * @param sentencePat the sentencePat to set
     */
    public void setSentencePat(String sentencePat) {
        this.sentencePat = sentencePat;
    }

    /**
     * @return the concept
     */
    public String getConcept() {
        return concept;
    }

    /**
     * @param concept the concept to set
     */
    public void setConcept(String concept) {
        this.concept = concept;
    }

    /**
     * @return the sentimentClass
     */
    public String getSentimentClass() {
        return sentimentClass;
    }

    /**
     * @param sentimentClass the sentimentClass to set
     */
    public void setSentimentClass(String sentimentClass) {
        this.sentimentClass = sentimentClass;
    }

    /**
     * @return the sentence
     */
    public String getSentence() {
        return sentence;
    }

    /**
     * @param sentence the sentence to set
     */
    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    /**
     * @return the isAddConceptToDB
     */
    public Boolean getIsAddConceptToDB() {
        return isAddConceptToDB;
    }

    /**
     * @param isAddConceptToDB the isAddConceptToDB to set
     */
    public void setIsAddConceptToDB(Boolean isAddConceptToDB) {
        this.isAddConceptToDB = isAddConceptToDB;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((concept == null) ? 0 : concept.hashCode());
        result = prime * result + ((sentencePat == null) ? 0 : sentencePat.hashCode());
        result = prime * result + ((sentimentClass == null) ? 0 : sentimentClass.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ConceptsVector other = (ConceptsVector) obj;
        if (concept == null) {
            if (other.concept != null)
                return false;
        } else if (!concept.equals(other.concept))
            return false;
        if (sentencePat == null) {
            if (other.sentencePat != null)
                return false;
        } else if (!sentencePat.equals(other.sentencePat))
            return false;
        if (sentimentClass == null) {
            if (other.sentimentClass != null)
                return false;
        } else if (!sentimentClass.equals(other.sentimentClass))
            return false;
        return true;
    }

}