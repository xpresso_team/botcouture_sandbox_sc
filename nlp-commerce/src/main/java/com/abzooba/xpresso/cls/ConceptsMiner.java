/**
 *
 */
package  com.abzooba.xpresso.cls;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.expressions.sentiment.SentimentFunctions;
import com.abzooba.xpresso.expressions.sentiment.XpSentiment;
import com.abzooba.xpresso.machinelearning.XpFeatureVector;
import com.abzooba.xpresso.sdgraphtraversal.DependencyRelationSorter;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.UniversalEnglishGrammaticalRelations;
import org.slf4j.Logger;

/**
 * @author Koustuv Saha
 * 21-Aug-2014 12:12:07 pm
 * XpressoV2.0.1  ConceptsMiner
 */
public class ConceptsMiner {

    protected static final Logger logger = XCLogger.getSpLogger();

    private static final String ADJECTIVE_TAG = "a";
    private static final String NOUN_TAG = "n";
    private static final String PRONOUN_TAG = "p";
    private static final String ADVERB_TAG = "r";

    private SemanticGraph dependencyGraph;

    private String sentenceClassificationClass;

    //Modifier goes in the Key, while the word goes in value. Top, Class
    private String sentence;
    private Set<IndexedWord> negatedConceptsHash;
    private Set<SemanticGraphEdge> checkedEdgeHash;
    private List<ConceptsVector> minedConceptsList;
    //	private List<ConceptsVector> minedAspectsList;
    private XpFeatureVector xpf;

    public ConceptsMiner(SemanticGraph depGraph, String classificationClass, String sentence, XpFeatureVector xpf) {
        this.dependencyGraph = depGraph;
        this.sentenceClassificationClass = classificationClass;
        this.sentence = sentence;
        if (xpf != null) {
            this.xpf = xpf;
        }
        negatedConceptsHash = new HashSet<IndexedWord>();
        checkedEdgeHash = new HashSet<SemanticGraphEdge>();
        minedConceptsList = new ArrayList<ConceptsVector>();
    }

    public static String getGeneralizedPOS(String stanfordPOStag) {

        if (stanfordPOStag.startsWith("JJ")) {
            return ADJECTIVE_TAG;
        } else if (stanfordPOStag.startsWith("NN")) {
            return NOUN_TAG;
        } else if (stanfordPOStag.startsWith("PRP")) {
            return PRONOUN_TAG;
        } else if (stanfordPOStag.startsWith("RB")) {
            return ADVERB_TAG;
        } else
            return null;

    }

    private String generateConcept(IndexedWord word1, IndexedWord word2) {

        String concept = null;

        if (word2.tag().startsWith("VB")) {
            return null;
        } else if (word1.tag().startsWith("RB") && word2.tag().startsWith("JJ")) {
            return null;
        } else if (!word1.tag().startsWith("NN") && !word2.tag().startsWith("NN")) {
            return null;
        } else if (word1.word().length() < 2 || word2.word().length() < 2) {
            return null;
        }

        concept = word1.word().toLowerCase() + " " + word2.word().toLowerCase();

        return concept;

    }

    private void nsubj_vectorize(IndexedWord gov, IndexedWord dep) {
        String govWord = gov.word().toLowerCase();
        //		String depWord = dep.word().toLowerCase();

        String govTag = getGeneralizedPOS(gov.tag());
        String depTag = getGeneralizedPOS(dep.tag());
        //		this.sentencePat.setGovTag(getGeneralizedPOS(gov.tag()));
        //		this.sentencePat.setDepTag(getGeneralizedPOS(dep.tag()));

        String sentencePat = "Nsubj";
        if (govTag != null && depTag != null) {
            //			this.sentencePat.isGovNsubjRelation = true;

            //			if (this.dependencyGraph.hasChildWithReln(gov, GrammaticalRelation.valueOf(DependencyRelationsLexicon.COP))) {
            if (this.dependencyGraph.hasChildWithReln(gov, UniversalEnglishGrammaticalRelations.COPULA)) {
                sentencePat += "Cop";
                //				this.sentencePat.isGovCopRelation = true;
            }

            //			IndexedWord amodDep = this.dependencyGraph.getChildWithReln(gov, GrammaticalRelation.valueOf(DependencyRelationsLexicon.AMOD));
            IndexedWord amodDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.ADJECTIVAL_MODIFIER);
            String concept = null;
            if (amodDep != null) {
                sentencePat += "Amod";
                amod_vectorize(gov, amodDep, sentencePat);
                checkedEdgeHash.add(this.dependencyGraph.getEdge(gov, amodDep));

            } else {
                //				IndexedWord advmodDep = this.dependencyGraph.getChildWithReln(gov, GrammaticalRelation.valueOf(DependencyRelationsLexicon.ADVMOD));
                IndexedWord advmodDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.ADVERBIAL_MODIFIER);
                if (advmodDep != null) {
                    sentencePat += "Advmod";
                    advmod_vectorize(gov, advmodDep, sentencePat);
                    checkedEdgeHash.add(this.dependencyGraph.getEdge(gov, advmodDep));
                }

                String conceptClassificationClass = null;
                if (this.sentenceClassificationClass != null) {
                    if (this.negationCheck(gov, dep)) {
                        conceptClassificationClass = SentimentFunctions.switchSentiment(this.sentenceClassificationClass);
                    } else {
                        conceptClassificationClass = this.sentenceClassificationClass;
                    }
                }

                if (govTag.equals(ADJECTIVE_TAG)) {
                    sentencePat += "Adj";
                    concept = govWord + " " + "NOUN";

                    ConceptsVector cv = new ConceptsVector(sentencePat, concept, conceptClassificationClass, sentence, true);
                    this.minedConceptsList.add(cv);

                    //					ConceptsAggregator.addToDB(sentencePat, concept, conceptClassificationClass, sentence, true);
                    logger.info("SentencePattern Concept(Nsubj1): " + sentencePat + "\t" + concept + "\t" + conceptClassificationClass);
                }
                if (!depTag.equals(PRONOUN_TAG)) {
                    //					concept = govWord.toLowerCase() + " " + depWord.toLowerCase();
                    concept = generateConcept(gov, dep);
                    if (concept != null) {
                        ConceptsVector cv = new ConceptsVector(sentencePat, concept, conceptClassificationClass, sentence, true);
                        this.minedConceptsList.add(cv);

                        //					ConceptsAggregator.addToDB(sentencePat, concept, conceptClassificationClass, sentence, true);
                        logger.info("SentencePattern Concept(Nsubj2): " + sentencePat + "\t" + concept + "\t" + conceptClassificationClass);
                    } //				}
                }
            }
        }

    }

    private void amod_vectorize(IndexedWord gov, IndexedWord dep, String initialSentencePat) {
        //		String amodDepWord = dep.word().toLowerCase();
        //		String concept = amodDepWord + " " + gov.word().toLowerCase();
        String concept = generateConcept(dep, gov);
        if (concept != null) {
            String sentencePat = "Amod";
            String conceptClassificationClass = null;
            if (this.sentenceClassificationClass != null) {
                if (negationCheck(gov, dep)) {
                    conceptClassificationClass = SentimentFunctions.switchSentiment(this.sentenceClassificationClass);
                } else {
                    conceptClassificationClass = this.sentenceClassificationClass;
                }
            }
            //		ConceptsAggregator.addToDB(sentencePat, concept, conceptClassificationClass, sentence, true);

            ConceptsVector cv = new ConceptsVector(sentencePat, concept, conceptClassificationClass, sentence, true);
            this.minedConceptsList.add(cv);

            logger.info("SentencePattern Concept(Amod1): " + sentencePat + "\t" + concept + "\t" + conceptClassificationClass);
            if (initialSentencePat != null) {
                cv = new ConceptsVector(sentencePat, concept, conceptClassificationClass, sentence, false);
                this.minedConceptsList.add(cv);
                //			ConceptsAggregator.addToDB(initialSentencePat, concept, conceptClassificationClass, sentence, false);
                logger.info("SentencePattern Concept(Amod2): " + sentencePat + "\t" + concept + "\t" + conceptClassificationClass);
            }
            //			IndexedWord advmodDep = this.dependencyGraph.getChildWithReln(gov, GrammaticalRelation.valueOf(DependencyRelationsLexicon.ADVMOD));
            IndexedWord advmodDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.ADVERBIAL_MODIFIER);
            if (advmodDep != null) {
                advmod_vectorize(gov, advmodDep, sentencePat + "Advmod");
            }
        }
    }

    private void advmod_vectorize(IndexedWord gov, IndexedWord dep, String initialSentencePat) {
        String sentencePat = "Advmod";
        String conceptClassificationClass = null;
        if (this.sentenceClassificationClass != null) {
            if (negationCheck(gov, dep)) {
                conceptClassificationClass = SentimentFunctions.switchSentiment(this.sentenceClassificationClass);
            } else {
                conceptClassificationClass = this.sentenceClassificationClass;
            }
        }
        //		String concept = dep.word().toLowerCase() + " " + gov.word().toLowerCase();
        String concept = generateConcept(dep, gov);
        if (concept != null) {
            ConceptsVector cv = new ConceptsVector(sentencePat, concept, conceptClassificationClass, sentence, true);
            this.minedConceptsList.add(cv);
            logger.info("SentencePattern Concept(Advmod1): " + sentencePat + "\t" + concept + "\t" + conceptClassificationClass);
            if (initialSentencePat != null) {
                cv = new ConceptsVector(sentencePat, concept, conceptClassificationClass, sentence, false);
                this.minedConceptsList.add(cv);
                logger.info("SentencePattern Concept(Advmod2): " + initialSentencePat + "\t" + concept + "\t" + conceptClassificationClass);
            }
        }
    }

    public void dep_vectorize(IndexedWord gov, IndexedWord dep) {
        String govWord = gov.value();
        String depWord = dep.value();
        if (XpSentiment.SHIFTER_TAG.equals(XpSentiment.getLexTag(Languages.EN, govWord, null, null))) {
            logger.info("NegEither " + dep + " is negated by " + gov);
            negatedConceptsHash.add(dep);
        } else if (XpSentiment.SHIFTER_TAG.equals(XpSentiment.getLexTag(Languages.EN, depWord, null, null))) {
            logger.info("NegEither " + gov + " is negated by " + dep);
            negatedConceptsHash.add(gov);
        }
    }

    public void neg_vectorize(IndexedWord gov, IndexedWord dep) {
        negatedConceptsHash.add(gov);
    }

    private boolean negationCheck(IndexedWord gov, IndexedWord dep) {
        if (negatedConceptsHash.contains(gov) || negatedConceptsHash.contains(dep)) {
            return true;
        }
        return false;
    }

    private void vectorizeSentence() {

        if (this.dependencyGraph != null) {
            logger.info("Dependency Graph: " + this.dependencyGraph.toString(SemanticGraph.OutputFormat.LIST));
            //			logger.info( "Dependency Graph: " + this.dependencyGraph.toString("plain"));

            List<SemanticGraphEdge> sortedEdgesList = this.dependencyGraph.edgeListSorted();
            Collections.sort(sortedEdgesList, new DependencyRelationSorter());

            for (SemanticGraphEdge edge : sortedEdgesList) {
                //			System.out.println("My relation: " + edge.getRelation().toString());
                if (checkedEdgeHash.contains(edge)) {
                    continue;
                }

                IndexedWord gov = edge.getGovernor();
                IndexedWord dep = edge.getDependent();
                GrammaticalRelation rel = edge.getRelation();

				/*If the sentence has a single nsubj relation it goes here.*/
                //				if (rel.equals(GrammaticalRelation.valueOf(DependencyRelationsLexicon.NSUBJ))) {
                if (rel.equals(UniversalEnglishGrammaticalRelations.NOMINAL_SUBJECT)) {
                    nsubj_vectorize(gov, dep);
                    //					break;
                }
                //				else if (rel.equals(GrammaticalRelation.valueOf(DependencyRelationsLexicon.AMOD))) {
                else if (rel.equals(UniversalEnglishGrammaticalRelations.ADJECTIVAL_MODIFIER)) {
                    amod_vectorize(gov, dep, null);
                } else if (rel.equals(UniversalEnglishGrammaticalRelations.COMPOUND_MODIFIER)) {
                    amod_vectorize(gov, dep, null);
                }
                //				else if (rel.equals(GrammaticalRelation.valueOf(DependencyRelationsLexicon.NEG))) {
                else if (rel.equals(UniversalEnglishGrammaticalRelations.NEGATION_MODIFIER)) {
                    neg_vectorize(gov, dep);
                }
                //				else if (rel.equals(GrammaticalRelation.valueOf(DependencyRelationsLexicon.DEP))) {
                else if (rel.equals(UniversalEnglishGrammaticalRelations.SEMANTIC_DEPENDENT)) {
                    dep_vectorize(gov, dep);
                }
                //				else if (rel.equals(GrammaticalRelation.valueOf(DependencyRelationsLexicon.ADVMOD))) {
                else if (rel.equals(UniversalEnglishGrammaticalRelations.ADVERBIAL_MODIFIER)) {
                    advmod_vectorize(gov, dep, null);
                }
            }
        }

    }

    public List<ConceptsVector> mineConcepts() {
        this.vectorizeSentence();
        if (xpf != null) {
            this.featurize();
        }
        return this.minedConceptsList;
    }

    private void featurize() {

        for (IndexedWord negatedWord : negatedConceptsHash) {
            String negatedWordStr = negatedWord.word().toLowerCase();
            String concept = "NEG" + " " + negatedWordStr;
            String sentencePat = "Negation";
            ConceptsVector cv = new ConceptsVector(sentencePat, concept, this.sentenceClassificationClass, sentence, true);
            this.minedConceptsList.add(cv);
        }
		/*Uncomment this method part for the use of DynamicConceptsFeature from CLS*/
        //		for (ConceptsVector cv : this.minedConceptsList) {
        //			String concept = cv.getConcept();
        //			xpf.setIsDynamicConceptFeature(concept);
        //			xpf.setIsDynamicConceptFeature(cv.getSentencePat());
        //			//			xpf.setTargetClass(cv.getSentimentClass());
        //		}
    }

    public void matchConcepts() {
        this.vectorizeSentence();
    }

}