package  com.abzooba.xpresso.utils;

import java.io.PrintWriter;

public class GenericIO {

    private static boolean WRITE_FLAG = false;
    private static PrintWriter WRITER = null;
    private static boolean PRINT_CONTROL_MASTER = true;

    //	public static Logger logger;

    public static void init() {
        WRITER = new PrintWriter(System.out);
        //		logger = LogFormatter.getLogger();
        //		logger = LogFormatter.getLogger();
    }

    public static boolean getWriteFlag() {
        return WRITE_FLAG;
    }

    public static void setWriteFlag(boolean writeFlag) {
        WRITE_FLAG = writeFlag;
    }

    public static boolean getMasterFlag() {
        return PRINT_CONTROL_MASTER;
    }

    public static void setMasterFlag(boolean masterFlag) {
        PRINT_CONTROL_MASTER = masterFlag;
    }

    public static void write(String content) {
        // System.out.println("write flag status : "+WRITE_FLAG);
        if (!WRITE_FLAG || WRITER == null)
            return;
        WRITER.write(content + "\n");
        WRITER.flush();
    }

    public static void write(String content, boolean flag) {
        // System.out.println("write flag status : "+WRITE_FLAG);
        if (!PRINT_CONTROL_MASTER || WRITER == null)
            return;
        if (!WRITE_FLAG && !flag)
            return;
        WRITER.write(content + "\n");
        WRITER.flush();
    }

    public static void close() {
        WRITER.close();
        WRITER = null;
    }

    public static void main(String[] args) {
        GenericIO.setWriteFlag(true);
        write("have fun");
    }

}