package com.abzooba.xpresso.aspect.entityextraction.ontologybuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.abzooba.xpresso.aspect.entityextraction.XpEntityCommons;
import com.abzooba.xpresso.aspect.entityextraction.XpEntityExtractorThread;
import com.abzooba.xpresso.aspect.wordvector.WordVectorBottomUp;
import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpEngine;
// import com.abzooba.xpresso.engine.core.XpLogger;
import com.abzooba.xpresso.textanalytics.wordvector.WordVector;
import com.abzooba.xpresso.utils.CollectionUtils;
import com.abzooba.xpresso.utils.FileIO;
import com.abzooba.xpresso.utils.ThreadUtils;
// import ch.qos.logback.classic.Logger;
import com.google.common.collect.Lists;

/**
 * XpOntologyBuilder can be used for extracting entities from data , finding word clusters for the entities and creating the
 * new Ontology with the entities. It be used either as a continuous pipeline or in steps. It also serves as an API for the Ontology builder UI.
 *
 * @author vivek aditya
 * @see <a href="http://xpresso.abzooba.com/ontologybuilder/">UI</a>
 * @see <a href="http://xpresso.abzooba.com/OntologyBuilder">javadoc</a>
 * @see <a href="http://xpresso.abzooba.com/OntologyBuilder/XpOntologyBuilder.html">flowchart</a>
 * @since  v3.4.0-dev8
 */

public class XpOntologyBuilder {
    // private static Logger logger;
    /**
     * Entity map for entities already existing in the Oxntology
     */
    public static Map<String, Integer> oldEntityMap = new ConcurrentHashMap<String, Integer>();
    /**
     * Entity map for new entities
     */
    public static Map<String, Integer> newEntityMap = new ConcurrentHashMap<String, Integer>();
    private static Scanner input = new Scanner(System.in);
    private static String mode;

    /**
     * Initialize XpEngine for Entity Extraction
     * @param mode The mode in which XpEngine has to be initialized
     */
    public static void init(String mode) {
        XpEngine.init(true);
        // logger = XpLogger.getXpLogger();
    }

    /**
     * Return the extracted entities for the UI
     * @param language	Input language (EN/SP)
     * @param input		The input data from which entities are to be extracted
     * @param reviewField Position of review Field
     * @return JSONObject containing the Extracted entities along with there counts
     */
    public static JSONObject getEntityExtracted(Languages language, String input, int reviewField) {
        XpEntityCommons xpEntity = new XpEntityCommons(null);
        List<String> reviewList = new ArrayList<String>();
        for (String line : input.split("\n")) {
            reviewList.add(line);
        }
        JSONObject returnJSON = new JSONObject(entityExtractorActivatorAPI(language, reviewList, reviewField, xpEntity));
        return returnJSON;
    }

    /**
     * Extract entities from the input reviews
     * @param language	Input language (EN/SP)
     * @param reviewList List(String) of reviews
     * @param reviewField Position of review Field
     * @param xpEntity	XpEntityCommons Object for the shared resources
     * @return allEntityMap The Entity count map
     */
    public static Map<String, Integer> entityExtractorActivatorAPI(Languages language, List<String> reviewList, int reviewField, XpEntityCommons xpEntity) {
        int i = 0;
        long startTime;
        startTime = System.currentTimeMillis();
        xpEntity.processedReviewCount++;

        ExecutorService executor = Executors.newFixedThreadPool(16);
        for (String oneLine : reviewList) {
            print("\n$$review count: " + i);
            i++;
            String[] lineParts = oneLine.split(XpConfig.REVIEW_DELIMITER);
            if (lineParts.length < reviewField + 1) {
                continue;
            }
            String review = lineParts[reviewField];
            print("@@review: " + review);
            int maxReviewLength = XpConfig.MAX_REVIEW_LENGTH;
            if (maxReviewLength != 0 && review.length() > maxReviewLength) {
                continue;
            }
            Runnable worker = new XpEntityExtractorThread(language, review, xpEntity);
            executor.execute(worker);
        }
        ThreadUtils.shutdownAndAwaitTermination(executor);
        print("Finished all threads");

        Map<String, Integer> allEntityMap = new ConcurrentHashMap<String, Integer>();
        allEntityMap = xpEntity.getAllEntityMap();
        allEntityMap = (HashMap<String, Integer>) CollectionUtils.sortByValue(allEntityMap, true);
        print("Total time for entity extraction: " + (System.currentTimeMillis() - startTime));
        return allEntityMap;
    }

    /**
     * Extract entities from the given file
     * @param language	Input language (EN/SP)
     * @param inputFile Input file of reviews
     * @param reviewField Position of review Field
     * @param xpEntity	XpEntityCommons Object for the shared resources
     */
    public static void entityExtractorActivator(Languages language, String inputFile, int reviewField, XpEntityCommons xpEntity) {
        int i = 0;
        long startTime;
        startTime = System.currentTimeMillis();
        xpEntity.processedReviewCount++;
        List<String> reviewList = new ArrayList<String>();
        FileIO.read_file(inputFile, reviewList);

        ExecutorService executor = Executors.newFixedThreadPool(16);
        for (String oneLine : reviewList) {
            print("\n$$review count: " + i);
            i++;
            String[] lineParts = oneLine.split(XpConfig.REVIEW_DELIMITER);
            if (lineParts.length < reviewField + 1) {
                continue;
            }
            String review = lineParts[reviewField];
            print("@@review: " + review);
            int maxReviewLength = XpConfig.MAX_REVIEW_LENGTH;
            if (maxReviewLength != 0 && review.length() > maxReviewLength) {
                continue;
            }
            Runnable worker = new XpEntityExtractorThread(language, review, xpEntity);
            executor.execute(worker);
        }
        ThreadUtils.shutdownAndAwaitTermination(executor);
        print("Finished all threads");

        Map<String, Integer> allEntityMap = new ConcurrentHashMap<String, Integer>();
        allEntityMap = xpEntity.getAllEntityMap();
        allEntityMap = (HashMap<String, Integer>) CollectionUtils.sortByValue(allEntityMap, true);
        seperateEntityMaps(language, allEntityMap, xpEntity.getAllEntityFile());

        print("Total time for entity extraction: " + (System.currentTimeMillis() - startTime));
    }

    /**
     * Separate out old and new entities from the all entities Map
     * @param language	The working language
     * @param allEntityMap  The Entity count map
     * @param filePath The file where all entities are written to.
     */
    public static void seperateEntityMaps(Languages language, Map<String, Integer> allEntityMap, String filePath) {
        List<String> entityCountList = new ArrayList<String>();
        List<String> newEntityCountList = new ArrayList<String>();
        List<String> oldEntityCountList = new ArrayList<String>();
        JSONObject outputJSON;
        try {
            for (Entry<String, Integer> entry : allEntityMap.entrySet()) {
                String entity = entry.getKey();
                outputJSON = (JSONObject) WordVectorBottomUp.getCategory(language, entity, true, false, false);
                int count = entry.getValue();
                String oneLine = entity + "\t" + count;
                entityCountList.add(oneLine);
                if ((int) outputJSON.get("Match Type") != 1) {
                    newEntityMap.put(entity, count);
                    newEntityCountList.add(oneLine);
                } else {
                    oldEntityMap.put(entity, count);
                    oldEntityCountList.add(oneLine);
                }
            }
            print("FileName: " + filePath);
            FileIO.write_file(entityCountList, filePath, false);
            print("Old Entities FileName: " + "Old" + filePath);
            FileIO.write_file(oldEntityCountList, filePath + "Old", false);
            print("New Entities FileName: " + "New" + filePath);
            FileIO.write_file(newEntityCountList, filePath + "New", false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns integer value of input entered.
     * @return int The integer value represented by the input.
     */
    public static int integerInput() {
        while (true) {
            try {
                return Integer.parseInt(input.nextLine());
            } catch (Exception e) {
                print("Please Enter an Integer again\n");
            }
        }
    }

    /**
     * Returns an existing file path.
     * @return String correct file path
     */
    public static String fileInput() {
        String path = null;
        while (true) {
            try {
                path = input.nextLine();
                File f = new File(path);
                if (!f.exists() || f.isDirectory()) {
                    print("File doesnot exist or Incorrect File Location !!\nEnter Again\n");
                } else {
                    return path;
                }
            } catch (Exception e) {
                e.printStackTrace();
                print("File doesnot exist or Incorrect File Location !!!\nEnter Again\n");
            }
        }
    }

    /**
     * Get the word category for the given word
     * @param language Input language (EN/SP)
     * @param word The word for which category is to be found
     * @return The word's aspect category
     */
    public static String getCategory(Languages language, String word) {
        return XpEngine.getWordVectorDbApp().getColumnObjectForKey(language, word, "all-category") + "";
    }

    /**
     * Get word vector for the given word
     * @param language Input language (EN/SP)
     * @param word The word for which vector is to be found
     * @return The word vector
     */
    public static List<Double> getVector(Languages language, String word) {
        return WordVector.getWordVector(language, word);
    }

    /**
     * Create the new ontology for the UI
     * @param language  Input language (EN/SP)
     * @param response The entity's word cluster
     * @return JSONObject of the new Ontology
     */
    public static JSONObject updatedOntologyJSON(Languages language, String response) {
        JSONObject output = new JSONObject();
        try {
            output.put("1", "Aspect Category \t Seed Entity \t Cluster");
            String[] lines = response.split("\n");
            int cnt = 1;
            for (String line : lines) {
                if (!line.trim().equals("") && !line.trim().isEmpty()) {
                    String[] content = line.split("\t", 2);
                    if (!content[1].contains("OutOfDictionary")) {
                        output.put("" + cnt, XpEngine.getWordVectorDbApp().getColumnObjectForKey(language, content[0], "all-category") + "\t" + content[0] + "\t" + content[1]);
                    } else {
                        output.put("" + cnt, content[0]);
                    }
                    cnt++;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        print("Updated Ontology created");
        return output;
    }

    public static void OntologyOldEntities(Languages language, String response, PrintWriter pw) {
        JSONObject outputJSON;
        try {
            String[] lines = response.split("\n");
            ArrayList<String> oldEntities = Lists.newArrayList(lines);
            BufferedReader br = new BufferedReader(new FileReader(XpConfig.getWordVectorClusterFile(language)));
            String line;
            HashMap<String, String> ontology = new HashMap<String, String>();
            while ((line = br.readLine()) != null) {
                String[] content = line.split("\t", 3);
                ontology.put(content[1], content[2]);
            }
            br.close();
            for (String key : oldEntities) {
                key = key.split("\t")[0];
                outputJSON = (JSONObject) WordVectorBottomUp.getCategory(language, key, true, false, false);
                pw.print(((JSONArray) outputJSON.get("WordVectorCategories")).getString(0) + "\t");
                pw.print(key + "\t");
                pw.print(ontology.get(key));
                pw.println();
            }
        } catch (Exception e) {

        }
    }

    /**
     * Create the new ontology
     * @param language  Input language (EN/SP)
     * @param response  The entity's word cluster
     */
    public static void updatedOntology(Languages language, String response, String path) {
        PrintWriter pw = null;
        try {
            pw = new PrintWriter("output/ontology-" + XpEngine.getCurrentDateStr() + ".tsv");
            pw.println("Aspect Category \t Seed Entity \t Cluster");
            String[] lines = response.split("\n");
            if (mode.trim().equals("4")) {
                for (String line : lines) {
                    try {
                        if (!line.trim().equals("") && !line.trim().isEmpty()) {
                            String[] content = line.split("\t", 2);
                            if (!content[1].contains("OutOfDictionary")) {
                                pw.println(XpEngine.getWordVectorDbApp().getColumnObjectForKey(language, content[0], "all-category") + "\t" + content[0] + "\t" + content[1]);
                            }
                        }
                    } catch (Exception e) {

                    }
                }
                if (path != null && !path.trim().equals("")) {
                    response = new Scanner(new File(path)).useDelimiter("\\Z").next();
                    OntologyOldEntities(language, response, pw);
                }
            } else {
                for (String line : lines) {
                    try {
                        if (!line.trim().equals("") && !line.trim().isEmpty()) {
                            String[] content = line.split("\t", 2);
                            pw.println("null\t" + content[0] + "\t" + content[1]);
                        }
                    } catch (Exception e) {

                    }
                }
                JSONObject outputJSON;
                if (oldEntityMap != null) {
                    BufferedReader br = new BufferedReader(new FileReader(XpConfig.getWordVectorClusterFile(language)));
                    String line;
                    HashMap<String, String> ontology = new HashMap<String, String>();
                    while ((line = br.readLine()) != null) {
                        String[] content = line.split("\t", 3);
                        ontology.put(content[1], content[2]);
                    }
                    br.close();
                    for (String key : oldEntityMap.keySet()) {
                        outputJSON = (JSONObject) WordVectorBottomUp.getCategory(language, key, true, false, false);
                        pw.print(((JSONArray) outputJSON.get("WordVectorCategories")).getString(0) + "\t");
                        pw.print(key + "\t");
                        pw.print(ontology.get(key));
                        pw.println();
                    }
                }
            }
            pw.close();
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        print("Updated Ontology created");
    }

    /**
     * Uploads the file to the host and get back the result from the server
     * @param fileName The file containing entities count map
     * @param host The host where word cluster creation service is running
     * @param limit The count limit till where word clusters will be created
     * @param language  Input language (EN/SP)
     * @return The Entity's word cluster<br>
     * <strong>TODO</strong> Update the postFile method to handle languages or specific argument at the time of use.
     */
    public static String postFile(String fileName, String host, String limit, String language) {
        int timeout = 60 * 60;
        RequestConfig config = RequestConfig.custom().setConnectTimeout(timeout * 1000).setConnectionRequestTimeout(timeout * 1000).setSocketTimeout(timeout * 1000).build();
        HttpClientBuilder clientBuilder = HttpClientBuilder.create();
        clientBuilder.setDefaultRequestConfig(config);
        HttpClient client = clientBuilder.build();
        HttpPost post = new HttpPost(host);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        final File file = new File(fileName);
        FileBody fb = new FileBody(file);
        builder.addPart(limit + " " + language, fb);
        final HttpEntity yourEntity = builder.build();
        class ProgressiveEntity implements HttpEntity {
            public void consumeContent() throws IOException {
                EntityUtils.consume(yourEntity);
            }

            public InputStream getContent() throws IOException, IllegalStateException {
                return yourEntity.getContent();
            }

            public Header getContentEncoding() {
                return yourEntity.getContentEncoding();
            }

            public long getContentLength() {
                return yourEntity.getContentLength();
            }

            public Header getContentType() {
                return yourEntity.getContentType();
            }

            public boolean isChunked() {
                return yourEntity.isChunked();
            }

            public boolean isRepeatable() {
                return yourEntity.isRepeatable();
            }

            public boolean isStreaming() {
                return yourEntity.isStreaming();
            }

            public void writeTo(OutputStream outstream) throws IOException {
                class ProxyOutputStream extends FilterOutputStream {
                    public ProxyOutputStream(OutputStream proxy) {
                        super(proxy);
                    }

                    public void write(int idx) throws IOException {
                        out.write(idx);
                    }

                    public void write(byte[] bts) throws IOException {
                        out.write(bts);
                    }

                    public void write(byte[] bts, int st, int end) throws IOException {
                        out.write(bts, st, end);
                    }

                    public void flush() throws IOException {
                        out.flush();
                    }

                    public void close() throws IOException {
                        out.close();
                    }
                }
                class ProgressiveOutputStream extends ProxyOutputStream {
                    public ProgressiveOutputStream(OutputStream proxy) {
                        super(proxy);
                    }

                    public void write(byte[] bts, int st, int end) throws IOException {
                        out.write(bts, st, end);
                    }
                }
                yourEntity.writeTo(new ProgressiveOutputStream(outstream));
            }
        }
        ;
        ProgressiveEntity myEntity = new ProgressiveEntity();
        post.setEntity(myEntity);
        print("File Uploading");
        HttpResponse response = null;
        try {
            response = client.execute(post);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (response != null) {
            return getContent(response);
        } else {
            return "";
        }
    }

    /**
     * Get the content from the HttpResponse as a String
     * @param response The HTTP Response object
     * @return String The content being send by the server
     */
    public static String getContent(HttpResponse response) {
        BufferedReader rd;
        String body = "";
        String content = "";
        try {
            rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            while ((body = rd.readLine()) != null) {
                content += body + "\n";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content.trim();
    }

    /**
     * Main for Starting a particular mode
     * @param args Command line arguments
     */
    // @SuppressWarnings("resource")
    public static void main(String[] args) {
        int limit, reviewField;
        String inputFile, response;

        print("Please Enter Language:\nEnglish\tEN\nSpanish\tSP");
        Languages language = XpConfig.convertLanguage(input.nextLine());
        print("Please Enter Mode:\nFull Pipeline\t1\nEntity Extraction\t2\nCluster Creation\t3\nUpdated Ontology\t4");
        mode = input.nextLine();
        XpEntityCommons xpEntity = null;
        switch (mode.trim()) {
            case "1":
                init("Standalone");
                print("Full Pipeline Selected");
                print("Enter Data Location:");
                inputFile = fileInput();
                print("Enter Limit:");
                limit = integerInput();
                print("Enter Review Field(Enter Carefully):");
                reviewField = integerInput();
                xpEntity = new XpEntityCommons(null);
                entityExtractorActivator(language, inputFile, reviewField, xpEntity);
                print("Entities File Created");
                response = postFile(xpEntity.getAllEntityFile() + "New", "http://xpresso.abzooba.com/Word2Vec/Word2Vec", limit + "", language.name());
                updatedOntology(language, response, null);
                break;
            case "2":
                init("Standalone");
                print("Entity Extraction Selected\n");
                print("Enter Data Location:");
                inputFile = fileInput();
                xpEntity = new XpEntityCommons(null);
                print("Enter Review Field(Enter Carefully):");
                reviewField = integerInput();
                entityExtractorActivator(language, inputFile, reviewField, xpEntity);
                print("Entities File Created");
                break;
            case "3":
                print("Cluster Creation Selected\n");
                print("Enter Extracted Entities Location:");
                inputFile = fileInput();
                print("Enter Limit:");
                limit = integerInput();
                response = postFile(inputFile, "http://xpresso.abzooba.com/Word2Vec/Word2Vec", limit + "", language.name());
                print("Clusters Created");
                print(response);
                break;
            case "4":
                init("Standalone");
                print("Updated Ontology Selected\n");
                print("Enter Clusters Location:");
                inputFile = fileInput();
                try {
                    Scanner scanner = new Scanner(new File(inputFile));
                    print("Enter Old Entities Location:(If not need just press enter)");
                    String path = input.nextLine();
                    updatedOntology(language, scanner.useDelimiter("\\Z").next(), path);
                    scanner.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                break;
            default:
                print(mode);
                print("Selected Mode Doesnot Exist");
                break;
        }
        input.close();
    }

    /**
     * Just a smaller System.out.println
     * @param string The String to be printed
     */
    public static void print(String string) {
        System.out.println(string);
    }

}