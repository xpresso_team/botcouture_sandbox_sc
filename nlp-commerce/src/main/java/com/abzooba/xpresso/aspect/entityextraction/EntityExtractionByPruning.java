/**
 *
 */
package  com.abzooba.xpresso.aspect.entityextraction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.expressions.sentiment.XpSentiment;
import com.abzooba.xpresso.machinelearning.XpFeatureVector;
import com.abzooba.xpresso.textanalytics.CoreNLPController;

import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.util.CoreMap;

/**
 * @author Sumit Das Apr 14, 2014 10:07:57 AM
 */
/*
 * This class is for pruning features extracted from a text
 */
public class EntityExtractionByPruning {

    /**
     * @param args
     */
    private static final int INFINITY = 99999;
    private Annotation document;

    public static void main(String[] args) throws Exception {
        // TODO Auto-generated method stub
        String propsFile = "./config/xdt.properties";
        XpConfig.init(propsFile);
        // CuratorController.init();
        CoreNLPController.init();
        XpSentiment.init();
        XpEntityExtractor.init();
        Languages language = Languages.EN;
        String text = "the staff is helpful but the wifi is slow";
        // String text =
        // "The waiters should have been more proactive in handing out beer but looked mostly in the other direction when guests waved at them.";
        EntityExtractionByPruning ep = new EntityExtractionByPruning();
        long startTime = System.currentTimeMillis();
        Set<String> entitySet = ep.extractReviewEntities(language, text);
        System.out.println("@@final entities::");
        for (String entity : entitySet) {
            System.out.println(entity);
        }
        long endTime = System.currentTimeMillis();
        long reviewExecTime = endTime - startTime;
        System.out.println("@@ExecTime-clusterFeatures(): " + reviewExecTime);
        // XpEntityExtractor.close();
    }

    public Set<String> extractReviewEntities(Languages language, String review) throws Exception {

        Set<String> reviewEntitySet = new HashSet<String>();
        this.annotateDocument(language, review);
        List<CoreMap> sentencesList = this.document.get(SentencesAnnotation.class);
        for (CoreMap sentence : sentencesList) {
            // String snippet = sentence.toString();
            System.out.println("---------------------------------------------------------------------------------");
            // XpEntityExtractor
            // .writeToOutput("---------------------------------------------------------------------------------");
            // Set<String> sentenceEntitySet = clusterFeatures(snippet);
            Set<String> sentenceEntitySet = clusterFeatures(language, sentence);
            reviewEntitySet.addAll(sentenceEntitySet);
        }

        return reviewEntitySet;
    }

    /*
     * This function clusters features based on on the dependency graph based
     * algorithm proposed
     */
    public Set<String> clusterFeatures(Languages language, CoreMap sentence) throws Exception {

        String snippet = sentence.toString();
        System.out.println("@@sentence: " + snippet);
        List<String> entityList = EntityExtraction.extractEntityList(language, sentence);
        System.out.println("@@initialFeatureSet: " + entityList);

        SemanticGraph dependencies = sentence.get(SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation.class);
        // Set<SemanticGraphEdge> edge_set = dependencies.getEdgeSet();
        List<SemanticGraphEdge> edge_set = dependencies.edgeListSorted();
        Set<String> amodEntities = getAmodEntities(edge_set);

        Set<IndexedWord> vertexSet = dependencies.vertexSet();

		/*
		 * taking care of the multi-word entities. the multi-word entities are
		 * remembered in a HashMap for future reference. creating the list of
		 * initial features.
		 */
        HashMap<String, String> multiWordEntityMap = new HashMap<String, String>();
        List<IndexedWord> initialFeatureList = new ArrayList<IndexedWord>();
        for (String entity : entityList) {
            String[] entity_words = entity.split("\\s+");
            if (entity_words.length > 1) {
                String entityHead = entity_words[entity_words.length - 1];
                multiWordEntityMap.put(entityHead, entity);
                entity = entityHead;
            }
            for (IndexedWord vertex : vertexSet) {
                String vertexWord = vertex.word();
                // String vertexWord = vertex.toString().split("-")[0];
                // String vertexPOS = vertex.toString().split("-")[1];
                if (entity.equals(vertexWord)) {
                    initialFeatureList.add(vertex);
                }
            }
        }

		/*
		 * creating initial feature clusters
		 */
        List<Set<IndexedWord>> clusterList = new ArrayList<Set<IndexedWord>>();
        for (IndexedWord feature : initialFeatureList) {
            Set<IndexedWord> newCluster = new LinkedHashSet<IndexedWord>();
            newCluster.add(feature);
            clusterList.add(newCluster);
        }

        for (IndexedWord vertex : vertexSet) {
            int min_dist = INFINITY;
            Set<IndexedWord> arg_min = null;
            for (Set<IndexedWord> cluster : clusterList) {
                IndexedWord clusterHead = getClusterHead(cluster);
                List<SemanticGraphEdge> path = dependencies.getShortestUndirectedPathEdges(vertex, clusterHead);
                int dist = path.size();
                if (dist <= min_dist) {
                    min_dist = dist;
                    arg_min = cluster;
                }
            }
            if (clusterList.size() > 0 && arg_min != null) {
                arg_min.add(vertex);
            }
        }

        // XpEntityExtractor
        // .writeToOutput("@@clusterList after other word adding: "
        // + clusterList);

        Map<Set<IndexedWord>, Integer> clusterSentiIdxMap = new HashMap<Set<IndexedWord>, Integer>();
        for (Set<IndexedWord> cluster : clusterList) {
            int sentiIdx = getClusterSentimentIndex(language, cluster);
            clusterSentiIdxMap.put(cluster, sentiIdx);
        }

        // XpEntityExtractor.writeToOutput("@@clusterSentiIdxMap: "
        // + clusterSentiIdxMap);
        List<Set<IndexedWord>> clusterListUpdated = new ArrayList<Set<IndexedWord>>();
        for (Set<IndexedWord> cluster : clusterList) {
            if (clusterSentiIdxMap.get(cluster) != 0) {
                clusterListUpdated.add(cluster);
            }
        }
        // System.out.println("@@clusterList: " + clusterList);
        // XpEntityExtractor.writeToOutput("@@clusterListUpdated: "
        // + clusterListUpdated);

        Set<String> finalFeatureSet = new HashSet<String>();
        for (Set<IndexedWord> cluster : clusterListUpdated) {
            IndexedWord clusterHead = getClusterHead(cluster);

            // String clusterHeadWord = clusterHead.toString().split("-")[0];
            String clusterHeadWord = clusterHead.word();
            if (multiWordEntityMap.containsKey(clusterHeadWord)) {
                finalFeatureSet.add(multiWordEntityMap.get(clusterHeadWord));
            } else {
                finalFeatureSet.add(clusterHeadWord);
            }
        }

        // XpEntityExtractor.writeToOutput("@@amodEntities: " + amodEntities);
        for (String amodEntity : amodEntities) {
            finalFeatureSet.add(amodEntity);
        }

        Set<String> finalFeatureSetUpdated = new HashSet<String>();
        for (String feature : finalFeatureSet) {
            if (multiWordEntityMap.containsKey(feature)) {
                finalFeatureSetUpdated.add(multiWordEntityMap.get(feature));
            } else {
                finalFeatureSetUpdated.add(feature);
            }
        }

        // System.out.println("@@finalFeatureSet: " + finalFeatureSetUpdated);
        // XpEntityExtractor.writeToOutput("@@finalFeatureSet: "
        // + finalFeatureSetUpdated);

        return finalFeatureSetUpdated;
    }

    private void annotateDocument(Languages language, String review) {
        document = new Annotation(review);
        CoreNLPController.getPipeline(language).annotate(document);
    }

    public Set<String> getAmodEntities(Collection<SemanticGraphEdge> edgeSet) throws Exception {
        Set<String> amodEntitySet = new HashSet<String>();
        for (SemanticGraphEdge edge : edgeSet) {
            if (edge.getRelation().toString().equals("amod")) {
                // amodEntitySet.add(edge.getSource().toString().split("-")[0]);
                amodEntitySet.add(edge.getGovernor().word());
                // amodEntitySet.add(edge.getSource().toString().split("/")[0]);
            }
        }
        return amodEntitySet;
    }

    public IndexedWord getClusterHead(Set<IndexedWord> cluster) throws Exception {
        IndexedWord clusterHead = null;
        clusterHead = (IndexedWord) cluster.toArray()[0];
        return clusterHead;
    }

    public int getClusterSentimentIndex(Languages language, Set<IndexedWord> cluster) throws Exception {
        int sentiIdx = 0;
        int i = 0;
        for (IndexedWord feature : cluster) {
            if (i != 0) {
                i++;
                continue;
            } else {
                // String featureWord = feature.toString().split("-")[0];
                String featureWord = feature.word();
                if (!featureWord.equals("")) {

                    XpFeatureVector featureSet = new XpFeatureVector();
                    System.out.println("@@featureWord: " + featureWord);
                    System.out.println("@@featureSet: " + featureSet);
                    //					HashMap<String, String> tagSentimentMap = new HashMap<String, String>();
                    //					XpTextObject xto = new XpTextObject(null, null, tagSentimentMap, null);
                    //					String senti = SentimentEngine.sentimentEvaluation(language, featureWord, xto, featureSet, false);
                    //					if (senti.equalsIgnoreCase("positive") || senti.equalsIgnoreCase("negative")) {
                    //
                    //						// XpEntityExtractor.writeToOutput("@@featureWord: "
                    //						// + featureWord);
                    //						// XpEntityExtractor.writeToOutput("@@senti: " + senti);
                    //
                    //						sentiIdx++;
                    //					}
                }
            }
        }
        return sentiIdx;
    }
}