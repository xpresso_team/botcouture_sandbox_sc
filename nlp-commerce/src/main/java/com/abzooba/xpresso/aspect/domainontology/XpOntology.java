/**
 *
 */
package com.abzooba.xpresso.aspect.domainontology;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpSentenceResources;
import com.abzooba.xpresso.textanalytics.CoreNLPController;

/**
 * @author Koustuv Saha 
 * 10-Mar-2014 3:52:08 pm 
 * XpressoV2 OntoWorks
 */
public class XpOntology {

    /**
     * author :Antarip Biswas
     * Set up a flag to indicate whether or not to use
     * expanded domainKB
     */
    private static boolean useExpandedDomainKB = false;

    public static boolean getExpandedDomainKBFlag() {
        return useExpandedDomainKB;
    }

    public static void setExpandedDomainKBFlag(boolean flag) {
        useExpandedDomainKB = flag;
    }

    private static Map<Languages, DomainKnowledge[]> domainConceptsArr = new HashMap<Languages, DomainKnowledge[]>();
    private static Map<String, Integer> domainNameIdxMap;

    //	public static EmbeddedGraphDBLoader eGraphDBLoader = null;
    //	public static RestGraphDBLoader rGraphDBLoader = null;
    public static final String NO_ASPECT_TAG = "NO_ASPECT";
    public static final String ALL_ASPECT = "all";

    public static void init() throws IOException {
        String domainNames = XpConfig.DOMAIN_NAMES_LIST;
        String[] domainNamesArr = domainNames.split(",");
        domainNameIdxMap = new HashMap<String, Integer>();
        for (Languages lang : XpConfig.LANGUAGES) {
            DomainKnowledge[] domainConceptsArrLang = new DomainKnowledge[domainNamesArr.length + 1];
			/*At 0, we are putting in ALL_ASPECT*/
            domainConceptsArrLang[0] = new DomainKnowledge(lang, ALL_ASPECT);
            domainNameIdxMap.put(ALL_ASPECT, 0);

            for (int i = 1; i < domainNamesArr.length + 1; i++) {
                String domainName = domainNamesArr[i - 1];
                domainConceptsArrLang[i] = new DomainKnowledge(lang, domainName);
                if (!domainNameIdxMap.containsKey(domainName)) {
                    domainNameIdxMap.put(domainName, i);
                }
                // System.out.println("Putting name " + domainNamesArr[i] + ">>" +
                // i);
            }
            domainConceptsArr.put(lang, domainConceptsArrLang);
        }
    }

    // private static HashMap<String, String> getCacheMap() {
    // // synchronized (cacheEntityMap) {
    // return cacheEntityMap;
    // // }
    // }

    // public static String secondaryKBMatch(String entity) {
    // return DomainConcepts.getSecondaryDomainMap().get(entity);
    // }

    // /**
    // * author : Antarip Biswas
    // * @param domainMap : Map containing the list of entities and their
    // corresponding aspects
    // * @param word : word to be searched in the KB
    // * @return : aspect if match found else null
    // */
    // private static String findAspectByStringSimilarity(HashMap<String,
    // String> domainMap, String word) {
    // String conceptClass = null;
    // int percentSimilarityThreshold = 80;
    // for (Map.Entry<String, String> entry : domainMap.entrySet()) {
    // String concept = entry.getKey();
    // if (word.contains(concept)) {
    // double checkPercent = ((double) (double) concept.replaceAll(" ",
    // "").length() / (double) word.replaceAll(" ", "").length() * 100.0);
    // //System.out.println("@@concept: "+concept+"\t"+"@@word: "+word+"\t"+"@@checkPercent: "+checkPercent);
    // if (checkPercent > percentSimilarityThreshold) {
    // conceptClass = entry.getValue();
    // return conceptClass;
    // }
    // } else if (word.length() >= 4 && concept.startsWith(word)) {
    // double checkPercent = ((double) (double) word.replaceAll(" ",
    // "").length() / (double) concept.replaceAll(" ", "").length() * 100.0);
    // if (checkPercent > percentSimilarityThreshold) {
    // conceptClass = entry.getValue();
    // return conceptClass;
    // }
    // }
    // }
    // return conceptClass;
    // }

    // public static String findAspect(String entityStr) {
    // // System.out.println("Finding for: " + entityStr);
    // if (XpUtils.getPronounsHash().contains(entityStr)) {
    // return null;
    // }
    // if (getCacheMap().containsKey(entityStr)) {
    // return cacheEntityMap.get(entityStr);
    // }
    //
    // HashMap<String, String> domainMap = DomainConcepts.getDomainMap();
    // // HashMap<String, String> secondaryDomainMap =
    // DomainConcepts.getSecondaryDomainMap();
    // String conceptClass = directOntoMatch(entityStr);
    // // System.out.println("initial onto map: " + entityStr + "\t" +
    // conceptClass);
    // //1st secondary match
    // // if (getExpandedDomainKBFlag() && conceptClass == null) {
    // // conceptClass = secondaryKBMatch(entityStr);
    // // //
    // System.out.println("expanded domain KB : entity - "+entityStr+" conceptClass - "+((conceptClass!=null)?conceptClass:"conceptclass not found yet"));
    // // }
    //
    // if (conceptClass == null) {
    // String lemmatized_entityStr =
    // CoreNLPController.lemmatizeToStringFromCache(entityStr);
    // conceptClass = directOntoMatch(lemmatized_entityStr);
    // // System.out.println("initial onto map lemmatized: " + entityStr + "\t"
    // + conceptClass);
    // //2nd secondary match
    // // if (getExpandedDomainKBFlag()) {
    // // conceptClass = secondaryKBMatch(lemmatized_entityStr);
    // // // System.out.println("expanded domain KB lemmatized: entity - " +
    // entityStr + " conceptClass - " + ((conceptClass != null) ? conceptClass :
    // "conceptclass not found yet"));
    // // }
    //
    // if (conceptClass == null) {
    // // List<String> strArr = ProcessString.tokenizeString(entityStr);
    // List<String> strArr = ProcessString.tokenizeString(lemmatized_entityStr);
    //
    // for (int i = 0; i < strArr.size(); i++) {
    // String word = strArr.get(i);
    // conceptClass = domainMap.get(word);
    // if (conceptClass != null) {
    // break;
    // } else {
    // for (Map.Entry<String, String> entry : domainMap.entrySet()) {
    // String concept = entry.getKey();
    // if (word.contains(concept)) {
    // double checkPercent = ((double) (double) concept.replaceAll(" ",
    // "").length() / (double) word.replaceAll(" ", "").length() * 100.0);
    // //System.out.println("@@concept: "+concept+"\t"+"@@word: "+word+"\t"+"@@checkPercent: "+checkPercent);
    // if (checkPercent > 80) {
    // conceptClass = entry.getValue();
    // break;
    // }
    // } else if (word.length() >= 4 && concept.startsWith(word)) {
    // double checkPercent = ((double) (double) word.replaceAll(" ",
    // "").length() / (double) concept.replaceAll(" ", "").length() * 100.0);
    // if (checkPercent > 80) {
    // conceptClass = entry.getValue();
    // break;
    // }
    // }
    // }
    // }
    //
    // //3rd secondary match
    // // if (getExpandedDomainKBFlag()) {
    // // conceptClass = secondaryDomainMap.get(word);
    // // if (conceptClass != null) {
    // //
    // //System.out.println("expanded domain KB : word - "+word+" conceptClass - "+conceptClass);
    // // break;
    // // }
    // // conceptClass = findAspectByStringSimilarity(secondaryDomainMap, word);
    // // if (conceptClass != null) {
    // //
    // //System.out.println("expanded domain KB : word - "+word+" conceptClass - "+conceptClass);
    // // break;
    // // }
    // // }
    // }
    // }
    // }
    // //System.out.println("entity : "+entityStr+" conceptClass : "+((conceptClass
    // != null)?conceptClass : "aspect not found"));
    // if (conceptClass != null)
    // addToCache(entityStr, conceptClass);
    // return conceptClass;
    // }

    public static boolean augmentAspect(Languages lang, String entity, String aspect, String domainName) {
        if (domainName != null) {
            return domainConceptsArr.get(lang)[domainNameIdxMap.get(domainName)].augmentAspect(entity, aspect);
        }
        return false;
    }

    //	public static boolean isDomainNonRelevantCategory(String entityStr, String domainName) {
    //		String category = DomainKnowledge.allEntitiesDomainMap.get(entityStr);
    //		System.out.println("Category: " + category);
    //		if (category != null) {
    //			if (domainName != null && domainConceptsArr[domainNameIdxMap.get(domainName)].isRelevantCategory(category)) {
    //				System.out.println("Returning " + false);
    //				return false;
    //			} else
    //				return true;
    //		}
    //		return false;
    //	}

    public static boolean isDomainRelevantCategory(Languages lang, String category, String domainName) {
        //		System.out.println("DomainIDXMap: " + domainNameIdxMap);
        //		System.out.println("Domain Name: " + domainName);
        if (domainName == null) {
            return true;
        } else if (domainNameIdxMap.containsKey(domainName)) {
            //			System.out.println("domainName : " + domainName + " domainIDXMap contains domainName and category : " + category);
            if (domainName.equals(ALL_ASPECT)) {
                return true;
            }
            //			else if (domainConceptsArr.get(lang)[domainNameIdxMap.get(domainName)].isRelevantCategory(category)) {
            else {
                //				System.out.println("category : " + category + " is a relevant category of the domain : " + domainName);
                return domainConceptsArr.get(lang)[domainNameIdxMap.get(domainName)].isRelevantCategory(category);
                //				return true;
            }
        }
        return false;
    }

    public static String getOntologyCategory(Languages language, String domainName, String entityStr, XpSentenceResources xto) {
        if (OntologyUtils.isRejectEntity(language, entityStr)) {
            return null;
        }
        if (domainName == null || !domainNameIdxMap.containsKey(domainName)) {
            String rawOntologyCategory = getRawOntologyCategory(language, entityStr);
            if (rawOntologyCategory != null) {
                return rawOntologyCategory;
            } else {
                return DomainKnowledge.getWordVectorBottomUpCategory(language, entityStr, domainName, false, false);
            }
        } else {
            return XpOntology.domainConceptsArr.get(language)[XpOntology.domainNameIdxMap.get(domainName)].getOntologyCategory(entityStr, xto, false, false, false);
        }
    }

    //	public static String getOntologyCategory(Languages lang, String domainName, String entityStr) {
    //		return XpOntology.domainConceptsArr.get(lang)[XpOntology.domainNameIdxMap.get(domainName)].getOntologyCategory(entityStr, false, false);
    //	}

    public static String getRawOntologyCategory(Languages lang, String entityStr) {
        return DomainKnowledge.allEntitiesDomainMap.get(lang).get(entityStr.toLowerCase());
    }

    public static Map<String, List<String>> getDomainAspectEntities(Languages language, String domainName) {
        return XpOntology.domainConceptsArr.get(language)[XpOntology.domainNameIdxMap.get(domainName)].getDomainAspectEntities();
    }

    /**
     * checks if an entity is part of a domain
     * @param lang	whether english or spanish
     * @param aspect	entity that is to be checked
     * @param domainName	domain for which the entity has to be checked
     * @return	flag indicating if the entity is part of the domain or not
     */
    //	public static boolean isDomainEntity(Languages lang, String entity, String domainName) {
    //		String category = XpOntology.getOntologyCategory(lang, entity, domainName);
    //		//		System.out.println("entity : " + entity + " category : " + category);
    //		if (category == null)
    //			return false;
    //		return XpOntology.isDomainRelevantCategory(lang, category, domainName);
    //	}

    public static boolean isValidAspectCategory(Languages lang, String aspect, String domainName) {
        if (domainName == null) {
            return false;
        } else if (domainName.equals(ALL_ASPECT)) {
            return true;
        } else if (!domainNameIdxMap.containsKey(domainName)) {
            return false;
        }
        return domainConceptsArr.get(lang)[domainNameIdxMap.get(domainName)].isValidAspectCategory(aspect);
    }

    public static String[] findAspect(Languages lang, String entityStr, String domainName, String subject, XpSentenceResources xto) {
        return findAspect(lang, entityStr, domainName, subject, xto, false, false);
    }

    public static String[] findAspect(Languages lang, String entityStr, String domainName, String subject, XpSentenceResources xto, boolean isTryEntity, boolean isOpinionString) {
        if (domainName == null) {
            //	logger.info("No domain name");
            return null;
        } else if (!domainNameIdxMap.containsKey(domainName)) {
            //	logger.info("Domain name not recognized");
            return null;
        }
        //	logger.info("Domain name recognized");
        return domainConceptsArr.get(lang)[domainNameIdxMap.get(domainName)].getAspect(entityStr, subject, xto, isTryEntity, isOpinionString);
    }

    public static Set<String> getPossibleAspectsHash(Languages lang, String domainName) {
        if (domainName == null) {
            return null;
        } else if (!domainNameIdxMap.containsKey(domainName)) {
            //	logger.info("Domain name not recognized");
            return null;
        }
        //	logger.info("Domain name recognized");
        return domainConceptsArr.get(lang)[domainNameIdxMap.get(domainName)].getPossibleAspects();
    }

    public static String[] findImplicitAspect(Languages lang, String opinionWord, String domainName, String subject, XpSentenceResources xto) {
        if (domainName == null) {
            return null;
        }
        if (!domainNameIdxMap.containsKey(domainName)) {
            return null;
        }
        return domainConceptsArr.get(lang)[domainNameIdxMap.get(domainName)].getImplicitAspect(opinionWord, CoreNLPController.lemmatizeToString(lang, opinionWord), subject, xto);
    }

    //	public static boolean isDomainNonSentimentPhrase(String phrase, String domainName) {
    //		if (domainName.equals(ALL_ASPECT)) {
    //			return false;
    //		}
    //		return isDomainNonSentimentPhrase(Languages.EN, phrase, domainName);
    //	}

    public static boolean isDomainNonSentimentPhrase(Languages lang, String phrase, String domainName) {
        if (domainName == null) {
            return false;
        } else if (domainName.equals(ALL_ASPECT)) {
            return false;
        }
        if (!domainNameIdxMap.containsKey(domainName)) {
            return false;
        }
        return domainConceptsArr.get(lang)[domainNameIdxMap.get(domainName)].isDomainNonSentiment(phrase);
    }

    //	public static String getDomainContextualSentiment(String entity, String opinionWord, String domainName) {
    //		return getDomainContextualSentiment(Languages.EN, entity, opinionWord, domainName);
    //	}

    public static String getDomainContextualSentimentTag(Languages lang, String entity, String opinionWord, String domainName, String subject, XpSentenceResources xto) {
        return DomainKnowledge.getContextualSentimentTag(lang, entity, opinionWord, domainName, subject, xto);

        //		if (domainName == null) {
        //			for (Map.Entry<String, Integer> entry : domainNameIdxMap.entrySet()) {
        //				String currentSentiment = domainConceptsArr[entry.getValue()].getContextualSentimentTag(entity, opinionWord);
        //				String currentSentiment = domainConceptsArr[entry.getValue()].getContextualSentimentTag(entity, opinionWord);
        //				if (currentSentiment != null) {
        //					return currentSentiment;
        //				}
        //			}
        //			return null;
        //		} else {
        //			return domainConceptsArr[domainNameIdxMap.get(domainName)].getContextualSentimentTag(entity, opinionWord);
        //		}
    }

    public static String getDomainContextualSentimentTag(Languages lang, String entity, String[] entityAspectArr, String opinionWord, String domainName, String subject, XpSentenceResources xto) {
        return DomainKnowledge.getContextualSentimentTag(lang, entity, entityAspectArr, opinionWord, domainName, subject, xto);

        //		if (domainName == null) {
        //			for (Map.Entry<String, Integer> entry : domainNameIdxMap.entrySet()) {
        //				String currentSentiment = domainConceptsArr[entry.getValue()].getContextualSentimentTag(entity, opinionWord);
        //				String currentSentiment = domainConceptsArr[entry.getValue()].getContextualSentimentTag(entity, opinionWord);
        //				if (currentSentiment != null) {
        //					return currentSentiment;
        //				}
        //			}
        //			return null;
        //		} else {
        //			return domainConceptsArr[domainNameIdxMap.get(domainName)].getContextualSentimentTag(entity, opinionWord);
        //		}
    }

    /**
     * @author Koustuv Saha
     * Apr 17, 2015
     * @param opinionWord
     * @param opinionWord
     * @param sentiment
     * @return
     *
     */
    public static boolean augmentContextualSentiment(Languages lang, String concept, String opinionWord, String sentiment) {
        // TODO Auto-generated method stub
        return DomainKnowledge.augmentContextualSentiment(lang, concept, opinionWord, sentiment);
    }

    /**
     * 20-Apr-2014 Koustuv Saha
     * Finds the entityClass of the entity.
     *
     * @param entityStr
     *            : Must be in Lower case
     * @param subject
     * @param entitySentiment
     * @param snippetSentiment
     * @return
     * @throws Exception
     *
     */
    // public static String findAspect(String entityStr, String subject, String
    // entitySentiment, String snippetSentiment) throws Exception {
    //	public static String[] findAspect(String entityStr, String subject, String domainName, String snippetSentiment, Map<String, String> wordPOSMap) {
    //		if (domainName == null) {
    //			return null;
    //		} else {
    //			String[] conceptClass = null;
    //			if (entityStr != null) {
    //				//				conceptClass = findAspect(entityStr, domainName, wordPOSMap);
    //				if (!domainName.equals("healthCare")) {
    //					conceptClass = findAspect(entityStr, domainName, wordPOSMap);
    //				} else {
    //					String entitySentiment = SentimentEngine.sentimentEvaluation(entityStr, null, null, false);
    //					conceptClass = MeSHontology.getAspect(entityStr, subject, entitySentiment, snippetSentiment);
    //				}
    //			}
    //			return conceptClass;
    //		}
    //
    //	}

    //	public static String[] getImplicitAspect(String str, String domainName) {
    //		if (domainName == null) {
    //			return null;
    //		}
    //		return domainConceptsArr[domainNameIdxMap.get(domainName)].getImplicitAspect(str);
    //	}
}