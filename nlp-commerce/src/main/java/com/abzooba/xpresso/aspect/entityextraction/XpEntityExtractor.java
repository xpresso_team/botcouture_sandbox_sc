/**
 *
 */
package  com.abzooba.xpresso.aspect.entityextraction;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.utils.FileIO;

/**
 * @author Sumit Das
 * Apr 14, 2014 10:07:57 AM
 */

public class XpEntityExtractor {
    /**
     * @param args
     */
    public static HashMap<String, Integer> allEntityMap;
    public static String allEntityFile;
    public static Map<Languages, Set<String>> stopWordSet;

    // private static Logger theLogger;

    // public static void main(String[] args) throws Exception {
    //
    // theLogger = LogFormatter.getLogger();
    // theLogger.setLevel(Level.ALL);
    //
    // long startTime;
    // long endTime;
    // long executionTime;
    //
    // startTime = System.currentTimeMillis();
    // String propsFile = "./config/xdt.properties";
    //
    // XpConfig.init(propsFile);
    // CoreNLPController.init();
    // SentimentEngine.init();
    // init();
    // endTime = System.currentTimeMillis();
    // executionTime = endTime - startTime;
    // thelogger.info( "loadup time: " + executionTime);
    //
    // // OLSetReviewSetFetcher olsetrf = new OLSetReviewSetFetcher();
    // // HashMap<String, String> olsetReviewMap = olsetrf.getReviewSet();
    // // int i = 0;
    // // for (Entry<String, String> entryReview : olsetReviewMap.entrySet()) {
    // // System.out.println("$$review_count: " + i);
    // // i++;
    // // String review = entryReview.getValue();
    // // long startTime = System.currentTimeMillis();
    // // reviewEntityExtraction(review);
    // // long endTime = System.currentTimeMillis();
    // // long reviewExecTime = endTime - startTime;
    // // System.out.println("@@ExecTime-reviewEntityExtraction(): "
    // // + reviewExecTime);
    // // }
    //
    // startTime = System.currentTimeMillis();
    //
    // // String inputFile = XpConfig.INPUT_REVIEW_FILENAME;
    // // List<String> reviewList = new ArrayList<String>();
    // // FileIO.read_file(inputFile, reviewList);
    // //
    // // EntityExtractionByPruning ep = new EntityExtractionByPruning();
    // // int i = 0;
    // // for (String oneLine : reviewList) {
    // // System.out.println("\n$$review count: " + i);
    // // i++;
    // // String[] lineParts = oneLine.split("\t");
    // // System.out.println("length: " + lineParts.length);
    // // if (lineParts.length < XpConfig.REVIEW_FIELD) {
    // // continue;
    // // }
    // // String review = lineParts[XpConfig.REVIEW_FIELD - 1];
    // // int maxReviewLength = XpConfig.MAX_REVIEW_LENGHT;
    // // if (maxReviewLength != 0 && review.length() > maxReviewLength) {
    // // continue;
    // // }
    // //
    // // long startTimeOneReview = System.currentTimeMillis();
    // // Set<String> reviewEntitySet = ep.extractReviewEntities(review);
    // // for (String entity : reviewEntitySet) {
    // //
    // // if (stopWordSet.contains(entity)) {
    // // continue;
    // // }
    // //
    // // String entityLemma = CoreNLPController.lemmatizeToString(entity);
    // //
    // // if (!allEntityMap.containsKey(entityLemma)) {
    // // allEntityMap.put(entityLemma, 1);
    // // } else {
    // // int count = allEntityMap.get(entityLemma);
    // // count++;
    // // allEntityMap.put(entityLemma, count);
    // // }
    // // }
    // // long endTimeOneReview = System.currentTimeMillis();
    // // long executionTimeOneReview = endTimeOneReview - startTimeOneReview;
    // // System.out.println("\n@@ExecTime-reviewEntityExtraction(): " +
    // // executionTimeOneReview);
    // // }
    //
    // String inputFile = XpConfig.INPUT_REVIEW_FILENAME;
    // List<String> reviewList = getReviewListfromXL(inputFile);
    //
    // EntityExtractionByPruning ep = new EntityExtractionByPruning();
    // int i = 0;
    // for (String oneLine : reviewList) {
    // System.out.println("\n$$review count: " + i);
    // i++;
    // int reviewLength = oneLine.length();
    // System.out.println("length: " + reviewLength);
    // if (oneLine.length() < XpConfig.REVIEW_FIELD) {
    // continue;
    // }
    // int maxReviewLength = XpConfig.MAX_REVIEW_LENGHT;
    // if (maxReviewLength != 0 && reviewLength > maxReviewLength) {
    // continue;
    // }
    // long startTimeOneReview = System.currentTimeMillis();
    // Set<String> reviewEntitySet = ep.extractReviewEntities(oneLine);
    // for (String entity : reviewEntitySet) {
    // if (stopWordSet.contains(entity)) {
    // continue;
    // }
    // String entityLemma = CoreNLPController.lemmatizeToString(entity);
    // if (!allEntityMap.containsKey(entityLemma)) {
    // allEntityMap.put(entityLemma, 1);
    // } else {
    // int count = allEntityMap.get(entityLemma);
    // count++;
    // allEntityMap.put(entityLemma, count);
    // }
    // }
    // long endTimeOneReview = System.currentTimeMillis();
    // long executionTimeOneReview = endTimeOneReview - startTimeOneReview;
    // System.out.println("\n@@ExecTime-reviewEntityExtraction(): " +
    // executionTimeOneReview);
    // }
    //
    // allEntityMap = (HashMap<String, Integer>)
    // CollectionUtilities.sortByValue(allEntityMap, true);
    //
    // List<String> entityCountList = new ArrayList<String>();
    // for (Entry<String, Integer> entry : allEntityMap.entrySet()) {
    // String entity = entry.getKey();
    // int count = entry.getValue();
    // String oneLine = entity + "," + count;
    // entityCountList.add(oneLine);
    // }
    //
    // allEntityFile = XpConfig.EXTRACTED_ENTITY_FILENAME;
    // FileIO.write_file(entityCountList, allEntityFile, false);
    //
    // endTime = System.currentTimeMillis();
    // executionTime = endTime - startTime;
    // thelogger.info( "Total time for entity extraction: " +
    // executionTime);
    // }
    //
    public static void init() throws Exception {
        allEntityMap = new HashMap<String, Integer>();
        stopWordSet = new HashMap<Languages, Set<String>>();
        for (Languages language : XpConfig.LANGUAGES) {
            Set<String> stopWordSetLanguage = new HashSet<String>();
            FileIO.read_file(XpConfig.getStopWords(language), stopWordSetLanguage);
            stopWordSet.put(language, stopWordSetLanguage);
        }
    }

    public static List<String> getReviewListfromXL(String inputFile) {

        List<String> reviewList = new ArrayList<String>();
        try {
            FileInputStream file = new FileInputStream(new File(inputFile));
            // Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            // Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            // Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            Map<String, String> reviewMap = new HashMap<>();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                // For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();
                int i = 1;
                String id = null;
                String srcText = null;
                while (cellIterator.hasNext()) {
                    if (i <= 2) {
                        Cell cell = cellIterator.next();
                        // Check the cell type and format accordingly
                        // switch (cell.getCellType()) {
                        // case Cell.CELL_TYPE_NUMERIC:
                        // System.out.print(cell.getNumericCellValue() + "t");
                        // break;
                        // case Cell.CELL_TYPE_STRING:
                        // System.out.print(cell.getStringCellValue() + "t");
                        // break;
                        // }
                        if (i == 1) {
                            id = cell.getStringCellValue();
                        } else {
                            srcText = cell.getStringCellValue();
                        }
                        i++;
                    } else {
                        break;
                    }
                }

                if (!reviewMap.containsKey(id)) {
                    reviewMap.put(id, srcText);
                }
            }
            file.close();

            for (Entry<String, String> entry : reviewMap.entrySet()) {
                // String id = entry.getKey();
                // System.out.println(id);
                String review = entry.getValue();
                // System.out.println(review);
                reviewList.add(review);
            }
            workbook.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return reviewList;
    }
}