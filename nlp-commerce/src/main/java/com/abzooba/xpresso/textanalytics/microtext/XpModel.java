package  com.abzooba.xpresso.textanalytics.microtext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;

import cmu.arktweetnlp.impl.Model;
import edu.berkeley.nlp.util.Triple;

public class XpModel extends Model {

    public static Model loadModelFromStream(InputStream inputStream) throws IOException {
        Model model = new Model();

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
        String line;

        ArrayList<Double> biasCoefs = new ArrayList<Double>();
        ArrayList<Triple<Integer, Integer, Double>> edgeCoefs = new ArrayList<Triple<Integer, Integer, Double>>();
        ArrayList<Triple<Integer, Integer, Double>> obsCoefs = new ArrayList<Triple<Integer, Integer, Double>>();

        while ((line = reader.readLine()) != null) {
            String[] parts = line.split("\t");
            if (!parts[0].equals("***BIAS***")) {
                break;
            }

            model.labelVocab.num(parts[1]);
            biasCoefs.add(Double.parseDouble(parts[2]));
        }
        model.labelVocab.lock();
        model.numLabels = model.labelVocab.size();
        //			do {
        //		if (reader != null) {
        while ((line = reader.readLine()) != null) {
            String[] parts = line.split("\t");

            if (!parts[0].equals("***EDGE***")) {
                break;
            }
            String[] edgePair = parts[1].split(" ");
            int prev = Integer.parseInt(edgePair[0]);
            int cur = Integer.parseInt(edgePair[1]);
            double val = Double.parseDouble(parts[2]);
            Triple<Integer, Integer, Double> t = new Triple<Integer, Integer, Double>(prev, cur, val);
            edgeCoefs.add(t);
        }
        //		while ((line = reader.readLine()) != null);
        //		}
        if (reader != null)

        {
            while ((line = reader.readLine()) != null) {

                //			do {
                String[] parts = line.split("\t");
                int f = model.featureVocab.num(parts[0]);
                int k = model.labelVocab.num(parts[1]);
                Triple<Integer, Integer, Double> t = new Triple<Integer, Integer, Double>(f, k, Double.parseDouble(parts[2]));
                obsCoefs.add(t);
            }
            //			while ((line = reader.readLine()) != null);
        }
        model.featureVocab.lock();

        model.allocateCoefs(model.labelVocab.size(), model.featureVocab.size());

        for (

                int k = 0; k < model.numLabels; k++)

        {
            model.biasCoefs[k] = biasCoefs.get(k);
        }
        for (

                Triple<Integer, Integer, Double> x : edgeCoefs)

        {
            model.edgeCoefs[x.getFirst()][x.getSecond()] = x.getThird();
        }
        for (

                Triple<Integer, Integer, Double> x : obsCoefs)

        {
            model.observationFeatureCoefs[x.getFirst()][x.getSecond()] = x.getThird();
        }
        reader.close();
        return model;
    }

}