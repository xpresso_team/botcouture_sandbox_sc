package  com.abzooba.xpresso.textanalytics.microtext;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpEngine;
import com.abzooba.xpresso.utils.FileIO;

import cmu.arktweetnlp.Tagger.TaggedToken;
import cmu.arktweetnlp.Twokenize;

public class MicroTextPreProcessor {

    private static XpTagger tagger = null;
    private static Pattern urlPattern = null;
    private static String regex = "((https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|])";

    public static void init() {
        tagger = new XpTagger();
        try {
            //			InputStream is = FileIO.getInputStreamFromFileName(XpConfig.MICROTEXT_TAGGER_MODEL);

            //			URL realData = MicroTextPreProcessor.class.getClassLoader().getResource("resources/twitter/model.ritter_ptb_alldata_fixed.20130723");
            //			URL realData = MicroTextPreProcessor.class.getClassLoader().getResource(XpConfig.MICROTEXT_TAGGER_MODEL);
            //			URL realData = Thread.currentThread().getContextClassLoader().getResource(XpConfig.MICROTEXT_TAGGER_MODEL);

            //			tagger.loadModel(XpConfig.MICROTEXT_TAGGER_MODEL);
            //			tagger.loadModel(realData.getPath());
            //			String filePath = realData.toString().replace("jar!", "jar");
            //			System.out.println("@@Path: " + filePath);
            //			tagger.loadModel(filePath);

            tagger.loadModel(FileIO.getInputStreamFromFileName(XpConfig.MICROTEXT_TAGGER_MODEL));
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        urlPattern = Pattern.compile(regex);
    }

    //	public static List<String> tokenizeMicroText(String text) {
    //		if (text == null)
    //			return null;
    //		List<String> tokenList = cmu.arktweetnlp.Twokenize.tokenizeRawtCTweetText(text);
    //		return tokenList;
    //	}

    public static List<String> tokenizeMicroText(String text) {
        if (text == null) {
            return null;
        }
        return Twokenize.tokenizeRawTweetText(text);
    }

    //	public static Map<String, String> tagMicroText(String text) {
    //		if (text == null)
    //			return null;
    //		List<TaggedToken> tokenTagList = tagger.tokenizeAndTag(text);
    //		Map<String, String> wordPosMap = new HashMap<String, String>();
    //		for (TaggedToken tagToken : tokenTagList) {
    //			if (tagToken.tag.matches("[A-Z]+"))
    //				wordPosMap.put(tagToken.token, tagToken.tag);
    //		}
    //		return wordPosMap;
    //	}

    public static String filterURL(String sentence) {
        Matcher match = urlPattern.matcher(sentence);
        int lastEnd = 0;
        StringBuilder sb = new StringBuilder();
        while (match.find()) {
            int matchStart = match.start();
            if (matchStart > lastEnd) {
                sb.append(sentence.substring(lastEnd, matchStart));
            }
            lastEnd = match.end() + 1;
        }
        if (lastEnd < sentence.length()) {
            sb.append(sentence.substring(lastEnd, sentence.length()));
        }
        return sb.toString();
    }

    public static String tagMicroText(Languages language, String text) {
        if (text == null || text.isEmpty()) {
            return "";
        }
        List<TaggedToken> tokenTagList = tagger.tokenizeAndTag(text);
        if (tokenTagList == null || tokenTagList.isEmpty()) {
            return "";
        }
        return processWordTagList(language, tokenTagList);
    }

    private static String microTextBasedProcessing(String sentence) {
        sentence = filterURL(sentence);
        return sentence;
    }

    private static String processWordTagList(Languages language, List<TaggedToken> tokenTagList) {
        boolean previousTokenUser = false;
        StringBuilder currentStr = new StringBuilder();
        for (int i = 0; i < tokenTagList.size(); i++) {
            TaggedToken tagToken = tokenTagList.get(i);
            //			System.out.println("token - " + tagToken.token + " tag - " + tagToken.tag);
            if (tagToken.tag.equals("USR")) {
                //				System.out.println("previous token user value : " + previousTokenUser);
                if (i == 0) {
                    if (i + 1 < tokenTagList.size()) {
                        if (tokenTagList.get(i + 1).tag.matches("(VB[A-Z]*)|(MD)|(,)")) {
                            currentStr.append(tagToken.token.substring(1));
                            currentStr.append(" ");
                        } else if (tokenTagList.get(i + 1).tag.equals("USR")) {
                            previousTokenUser = true;
                            //						wordPosMap.put(tagToken.token.substring(1), tagToken.tag);
                        }
                    }

                } else if (previousTokenUser) {
                    if (i + 1 == tokenTagList.size() || (i + 1 < tokenTagList.size() && tokenTagList.get(i + 1).tag.matches("(VB[A-Z]*)|(MD)"))) {
                        previousTokenUser = false;
                        currentStr.append(tagToken.token.substring(1));
                        currentStr.append(" ");
                        //						wordPosMap.put(tagToken.token.substring(1), tagToken.tag);
                    } else if ((i + 1 < tokenTagList.size() && !tokenTagList.get(i + 1).tag.equals("USR"))) {
                        previousTokenUser = false;
                        //					System.out.println("previous token user : " + tagToken.token);
                    }
                } else {
                    //					System.out.println("unprocessed user : " + tagToken.token.substring(1));
                    currentStr.append(tagToken.token.substring(1));
                    currentStr.append(" ");
                    //					wordPosMap.put(tagToken.token.substring(1), tagToken.tag);
                }
            } else if (tagToken.tag.equals("HT")) {
                List<String> tokenList = HashTagProcessor.parseHashTag(language, tagToken.token);
                if (tokenList != null) {
                    tokenList.forEach(token -> {
                        currentStr.append(token);
                        currentStr.append(" ");
                    });
                }
            } else {
                //				previousTokenUser = false;
                if (tagToken.token.matches("([A-Z]*\\$[A-Z]+)")) {
                    int dollarIndex = tagToken.token.indexOf("$");
                    if (dollarIndex == 0) {
                        tagToken.token = tagToken.token.substring(1);
                    } else {
                        StringBuilder sb = new StringBuilder(tagToken.token.substring(0, dollarIndex));
                        sb.append(tagToken.token.substring(dollarIndex + 1, tagToken.token.length()));
                        tagToken.token = sb.toString();
                    }
                }
                currentStr.append(tagToken.token);
                currentStr.append(" ");
                //				wordPosMap.put(tagToken.token, tagToken.tag);
            }
        }
        return currentStr.toString().trim();
    }

    private static String rtFix(String text) {
        if (text.startsWith("RT")) {
            String anteriorText = (text.contains(":")) ? text.substring(0, text.indexOf(":")) : "";
            String[] splitText = anteriorText.split(" ");
            if (splitText.length == 2) {
                text = text.substring(text.indexOf(":") + 1).trim();
            }
        }
        return text;
    }

    //	public static Map<String, String> tagMicroText(String text, boolean filterHashtagOnly) {
    //		Map<String, String> wordPosMap = new LinkedHashMap<String, String>();
    //		if (text == null)
    //			return wordPosMap;
    //		if (!filterHashtagOnly)
    //			return tagMicroText(text);
    //		List<TaggedToken> tokenTagList = tagger.tokenizeAndTag(text);
    //		if (tokenTagList == null || tokenTagList.isEmpty())
    //			return wordPosMap;
    //		for (TaggedToken tagToken : tokenTagList) {
    //			if (tagToken.tag.matches("HT"))
    //				wordPosMap.put(tagToken.token, tagToken.tag);
    //		}
    //		return wordPosMap;
    //	}

    public static String constructSentence(Languages language, String sentence) {
        sentence = filterURL(sentence);
        sentence = rtFix(sentence);
        //		Map<String, String> wordPosMap = tagMicroText(sentence);
        //		//		System.out.println("wordPosMap - " + wordPosMap);
        //		StringBuilder currentStr = new StringBuilder();
        //		// System.out.println("word POS map : " + wordPosMap);
        //
        //		for (Entry<String, String> entry : wordPosMap.entrySet()) {
        //			// System.out.println("token - " + entry.getKey());
        //			if (entry.getValue().equals("HT")) {
        //				List<String> tokenList = HashTagProcessor.parseHashtag(entry.getKey().toLowerCase());
        //				if (tokenList != null) {
        //					tokenList.forEach(token -> {
        //						currentStr.append(token);
        //						currentStr.append(" ");
        //					});
        //				}
        //			} else {
        //				currentStr.append(entry.getKey());
        //				currentStr.append(" ");
        //			}
        //		}
        //		return currentStr.toString();
        return tagMicroText(language, sentence);
    }

    public static String constructSentence(Languages language, String sentence, boolean isMicro) {
        sentence = filterURL(sentence);
        if (isMicro)
            sentence = microTextBasedProcessing(sentence);

        return tagMicroText(language, sentence);
    }

    public static void main(String[] args) throws Exception {
        String sentence = "@nkandhari That's a wonderful #VineVideo! So what was your favourite at the #GujaratiFoodFestival at @ITCHotels? #AtYourService";
        XpEngine.init(true);
        boolean isMicro = true;
        Languages lang = Languages.EN;
        System.out.println("sentence filtered - " + constructSentence(lang, sentence, isMicro));
    }
}