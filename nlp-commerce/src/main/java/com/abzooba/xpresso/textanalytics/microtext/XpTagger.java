package  com.abzooba.xpresso.textanalytics.microtext;

import java.io.IOException;
import java.io.InputStream;

import cmu.arktweetnlp.Tagger;
import cmu.arktweetnlp.impl.features.FeatureExtractor;

public class XpTagger extends Tagger {

    public void loadModel(InputStream inputStream) throws IOException {
        model = XpModel.loadModelFromStream(inputStream);
        featureExtractor = new FeatureExtractor(model, false);
    }

}