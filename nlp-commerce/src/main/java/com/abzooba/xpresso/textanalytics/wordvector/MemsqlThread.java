/**
 *
 */
package  com.abzooba.xpresso.textanalytics.wordvector;

/**
 * @author vivek aditya
 * 9:55:18 pm
 * 17-Nov-2015
 */
public class MemsqlThread implements Runnable {

    private String line;

    public MemsqlThread(String line) {
        this.line = line;
    }

    public void run() {
        String strSQL = createQuery();
        if (strSQL != null) {
            if (WordVectorMemsqlApp.executeQuery(strSQL, 0) == null) {
                System.out.println("Insertion failed\t" + strSQL);
            }
        }
    }

    public String createQuery() {
        String outputLine = null;
        StringBuilder output = new StringBuilder("INSERT INTO wordvectors (name, vector) VALUES ('");
        String vec[] = line.split(" ");
        output.append(vec[0].replaceAll("'", "''"));
        output.append("','{\"vector\":[");
        for (int i = 1; i < vec.length - 1; i++) {
            output.append("\"");
            output.append(vec[i]);
            output.append("\",");
        }
        output.append("\"");
        output.append(vec[vec.length - 1]);
        output.append("\"]}');");
        outputLine = output.toString();
        return outputLine;
    }

}