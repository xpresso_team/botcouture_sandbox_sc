/**
 *
 */
package  com.abzooba.xpresso.textanalytics;

import com.abzooba.xpresso.engine.config.XpConfig.Languages;


/**
 * @author Alix Melchy
 *Sep 4, 2015 19:05:46 PM
 */
public class VocabularyTests {

    public static boolean isHavePastTense(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.equals("had");
                break;
            case SP:
                answ = word.startsWith("tenía");
                break;
        }

        return answ;
    }

    public static boolean isHavePresentTense(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.toLowerCase().equals("has") || word.toLowerCase().equals("have");
                break;
            case SP:
                String wordLc = word.toLowerCase();
                answ = wordLc.equals("tengo") || wordLc.equals("tienes") || wordLc.equals("tiene") || wordLc.equals("tenemos") || wordLc.equals("tenéis") || wordLc.equals("tienen");
                break;
        }

        return answ;
    }

    public static boolean isHaveFutureTense(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.toLowerCase().matches("(will)|(shall)|(would)|(could)");
                break;
            case SP:
                answ = word.toLowerCase().startsWith("tendré") || word.toLowerCase().startsWith("tendrá") || word.toLowerCase().equals("tendremos");
                break;
        }

        return answ;
    }

    public static boolean goingToFutureTense(Languages lang, String sentence) {
        boolean answ = false;
        if (sentence == null)
            return answ;

        switch (lang) {
            case EN:
                int index = sentence.indexOf("going to");
                if (index >= 0) {
                    answ = true;
                    int pastIndex = sentence.indexOf("was");
                    pastIndex = (pastIndex < 0) ? sentence.indexOf("were") : pastIndex;
                    pastIndex = (pastIndex < 0) ? sentence.indexOf("had") : pastIndex;
                    if (pastIndex >= 0 && pastIndex < index)
                        answ = false;
                }
                break;
            case SP:
                index = sentence.indexOf("voy a");
                if (index < 0)
                    index = sentence.indexOf("vas a");
                if (index < 0)
                    index = sentence.indexOf("va a");
                if (index < 0)
                    index = sentence.indexOf("vamos a");
                if (index < 0)
                    index = sentence.indexOf("vais a");
                if (index < 0)
                    index = sentence.indexOf("van a");
                answ = index >= 0;
                break;
        }
        //		System.out.println("Is going to present ? - " + answ);
        return answ;
    }

    public static boolean containsBut(Languages lang, String text) {
        boolean answ = false;
        if (text == null)
            return answ;

        switch (lang) {
            case SP:
                answ = text.toLowerCase().contains("pero");
                break;
            case EN:
                answ = text.toLowerCase().contains("but");
                break;
        }

        return answ;
    }

    public static boolean isBut(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.equalsIgnoreCase("but");
                break;
            case SP:
                answ = word.equalsIgnoreCase("pero");
                break;
        }

        return answ;
    }

    public static boolean equalsRude(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case SP:
                answ = word.matches("(groser[o|a|os|as])");
                break;
            case EN:
                answ = word.equalsIgnoreCase("rude");
                break;
        }

        return answ;
    }

    public static boolean isBe(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.equalsIgnoreCase("be");
                break;
            case SP:
                answ = (word.equalsIgnoreCase("ser") || word.equalsIgnoreCase("estar"));
                break;
        }

        return answ;
    }

    public static boolean isToo(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.equalsIgnoreCase("too");
                break;
            case SP:
                answ = word.equalsIgnoreCase("demasiado");
                break;
        }

        return answ;
    }

    public static boolean isPlus(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.equalsIgnoreCase("plus");
                break;
            case SP:
                answ = word.equalsIgnoreCase("más");
                break;
        }

        return answ;
    }

    public static boolean isHaveToMark(Languages lang, String word) {
        //	have to in EN and tener que in SP
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.equalsIgnoreCase("to");
                break;
            case SP:
                answ = word.equalsIgnoreCase("que");
                break;
        }

        return answ;
    }

    public static boolean isHope(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.equalsIgnoreCase("hope");
                break;
            case SP:
                answ = word.startsWith("esper");
                break;
        }

        return answ;
    }

    public static boolean isFirstPersonSubject(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = (word.equalsIgnoreCase("we") || word.equalsIgnoreCase("i"));
                break;
            case SP:
                answ = (word.equalsIgnoreCase("nosotros") || word.equalsIgnoreCase("yo"));
                break;
        }

        return answ;
    }

    public static boolean isSecondPersonSubject(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.equalsIgnoreCase("you");
                break;
            case SP:
                answ = word.equalsIgnoreCase("tí") || word.equalsIgnoreCase("tu") || word.equalsIgnoreCase("usted") || word.equalsIgnoreCase("ustedes") || word.equalsIgnoreCase("vosotros");
                break;
        }

        return answ;
    }

    public static boolean isSecondThirdPersonSubject(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.toLowerCase().matches("(he)|(she)|(you)|(they)|(it)");
                break;
            case SP:
                answ = word.equalsIgnoreCase("él") || word.equalsIgnoreCase("ella") || word.equalsIgnoreCase("tí") || word.equalsIgnoreCase("usted") || word.equalsIgnoreCase("ellos") || word.equalsIgnoreCase("ellas") || word.equalsIgnoreCase("vosotros") || word.equalsIgnoreCase("ustedes");
                break;
        }

        return answ;
    }

    public static boolean isFirstPersonPossessive(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = (word.equalsIgnoreCase("my") || word.equalsIgnoreCase("our"));
                break;
            case SP:
                answ = (word.equalsIgnoreCase("mi") || word.equalsIgnoreCase("mis") || word.equalsIgnoreCase("nuestro") || word.equalsIgnoreCase("nuestra") || word.equalsIgnoreCase("nuestros") || word.equalsIgnoreCase("nuestras"));
                break;
        }

        return answ;
    }

    public static boolean isApproveDecline(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.toLowerCase().matches("(refuse[ed]*)|(approve[ed]*)|(decline[ed]*)");
                break;
            case SP:
                answ = word.toLowerCase().matches("(autoriza[do|da|dos|das]*)|(rechaza[do|da|dos|das]*)|(declina[do|da|dos|das]*)");
                break;
        }

        return answ;
    }

    public static boolean isShifterSubject(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = (word.equalsIgnoreCase("nobody") || word.equalsIgnoreCase("nothing") || word.equalsIgnoreCase("none"));
                break;
            case SP:
                answ = (word.equalsIgnoreCase("nadie") || word.equalsIgnoreCase("nada") || word.equalsIgnoreCase("ninguno") || word.equalsIgnoreCase("ninguna"));
                break;
        }

        return answ;
    }

    public static boolean isRare(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.equalsIgnoreCase("rare");
                break;
            case SP:
                answ = (word.toLowerCase().matches("(rar[o|a][s]*)") || word.toLowerCase().matches("(escas[o|a][s]*)"));
                break;
        }

        return answ;
    }

    public static boolean isIf(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.equalsIgnoreCase("if");
                break;
            case SP:
                answ = word.equalsIgnoreCase("si");
                break;
        }

        return answ;
    }

    public static boolean isThank(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.contains("thank");
                break;
            case SP:
                answ = word.equals("gracias");
                break;
        }

        return answ;
    }

    public static boolean isBeImperative(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.equals("be");
                break;
            case SP:
                answ = word.equalsIgnoreCase("está") || word.equalsIgnoreCase("estad") || word.equalsIgnoreCase("esté") || word.equalsIgnoreCase("estén") || word.equalsIgnoreCase("sé") || word.equalsIgnoreCase("sed") || word.equalsIgnoreCase("sea") || word.equalsIgnoreCase("sean");
                break;
        }

        return answ;
    }

    public static boolean isPlease(Languages lang, String snippet) {
        boolean answ = false;
        if (snippet == null)
            return answ;

        String[] words = snippet.split(" ");
        if (lang == Languages.SP && words.length > 1)
            answ = isPlease(lang, words[0], words[1]);

        return answ;
    }

    public static boolean isPlease(Languages lang, String firstWord, String secondWord) {
        boolean answ = false;
        if ((firstWord == null) | (secondWord == null))
            return answ;

        switch (lang) {
            case EN:
                answ = firstWord.equalsIgnoreCase("please");
                break;
            case SP:
                answ = firstWord.equalsIgnoreCase("por") && secondWord.equalsIgnoreCase("favor");
                break;
        }

        return answ;
    }

    public static boolean isKindly(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.equalsIgnoreCase("kindly");
                break;
            case SP:
				/*TODO: not a single word but an expression: haga el favor, tenga la amabilidad */
                break;
        }

        return answ;
    }

    public static boolean isRequireNeed(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.matches("(require[a-z]*)|(need[ed]*)");
                break;
            case SP:
                answ = CoreNLPController.lemmatizeToString(lang, word.toLowerCase()).equals("necesitar");
                break;
        }

        return answ;
    }

    public static boolean isInfo(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.toLowerCase().matches("(information)|(info)");
                break;
            case SP:
                answ = word.equalsIgnoreCase("informaciÃ³n") || word.equalsIgnoreCase("info") || word.equalsIgnoreCase("informacion");
                break;
        }

        return answ;
    }

    public static boolean isKnow(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.matches("know");
                break;
            case SP:
                answ = word.matches("saber");
                break;
        }

        return answ;
    }

    public static boolean isBuy(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.matches("(get)|(have)|(buy)|(purchase)");
                break;
            case SP:
                answ = CoreNLPController.lemmatizeToString(lang, word).matches("(comprar)|(obtener)|(adquirir)");
                break;
        }

        return answ;
    }

    public static boolean isSellBuy(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.matches("(sell)|(buy)|(purchase)");
                break;
            case SP:
                answ = CoreNLPController.lemmatizeToString(lang, word).matches("(vender)|(comprar)|(adquirir)");
                break;
        }

        return answ;
    }

    public static boolean isRecommendSuggest(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.matches("(suggest[eds]*)|(recommend)");
                break;
            case SP:
                answ = CoreNLPController.lemmatizeToString(lang, word).matches("(sugerir)|(recomendar)");
                break;
        }

        return answ;
    }

    public static boolean isHouse(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.toLowerCase().matches("(house)|(apartment)");
                break;
            case SP:
                answ = word.toLowerCase().matches("(casa)|(piso)|(apartamento)");
                break;
        }

        return answ;
    }

    public static boolean isCollege(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.toLowerCase().matches("(college)|(university)|(school)");
                break;
            case SP:
                answ = word.toLowerCase().matches("(universidad)|(escuela)");
                break;
        }

        return answ;
    }

    public static boolean isRenovate(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;
        switch (lang) {
            case EN:
                answ = word.toLowerCase().matches("(renovate)|(renovating)");
                break;
            case SP:
                answ = CoreNLPController.lemmatizeToString(lang, word.toLowerCase()).equals("renovar");
                break;
        }

        return answ;
    }

    public static boolean isMove(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.toLowerCase().matches("(move)|(shift)|(settle)|(moving)|(shifting)");
                break;
            case SP:
                answ = CoreNLPController.lemmatizeToString(lang, word.toLowerCase()).matches("(mudar)|(installar)|(establecer)");
                break;
        }

        return answ;
    }

    public static boolean isJoinStart(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.toLowerCase().matches("(join[ing]*)|(start[ing]*)|(attend[ing]*)");
                break;
            case SP:
                answ = CoreNLPController.lemmatizeToString(lang, word.toLowerCase()).matches("(ir)|(estar)|(estudiar)");
                break;
        }

        return answ;
    }

    public static boolean isDecline(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.matches("(decline[ed]*)|(refuse[ed]*)");
                break;
            case SP:
                answ = CoreNLPController.lemmatizeToString(lang, word.toLowerCase()).matches("(declinar)|(rechazar)|(rehusar)");
                break;
        }

        return answ;
    }

    public static boolean isApprove(Languages lang, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (lang) {
            case EN:
                answ = word.matches("(approve[ed]*)");
                break;
            case SP:
                answ = CoreNLPController.lemmatizeToString(lang, word.toLowerCase()).matches("aprobar");
                break;
        }

        return answ;
    }

    public static String returnNot(Languages lang) {
        String answ = "not";
        switch (lang) {
            case EN:
                answ = "not";
                break;
            case SP:
                answ = "no";
                break;
        }

        return answ;
    }

    public static boolean isCan(Languages language, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (language) {
            case SP:
                answ = CoreNLPController.lemmatizeToString(language, word.toLowerCase()).matches("poder");
                break;
            case EN:
                answ = word.equalsIgnoreCase("can");
                break;
        }

        return answ;
    }

    public static boolean isCould(Languages language, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (language) {
            case SP:
                answ = word.toLowerCase().startsWith("podr") && CoreNLPController.lemmatizeToString(language, word.toLowerCase()).matches("poder");
                break;
            case EN:
                answ = word.equalsIgnoreCase("could");
                break;
        }

        return answ;
    }

    public static boolean isShould(Languages language, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (language) {
            case SP:
                answ = CoreNLPController.lemmatizeToString(language, word.toLowerCase()).matches("deber");
                break;
            case EN:
                answ = word.equalsIgnoreCase("should");
                break;
        }

        return answ;
    }

    public static boolean isMust(Languages language, String word) {
        boolean answ = false;
        if (word == null)
            return answ;

        switch (language) {
            case SP:
                answ = CoreNLPController.lemmatizeToString(language, word.toLowerCase()).matches("deber") || CoreNLPController.lemmatizeToString(language, word.toLowerCase()).matches("tener");
                break;
            case EN:
                answ = word.equalsIgnoreCase("must");
                break;
        }

        return answ;
    }

}