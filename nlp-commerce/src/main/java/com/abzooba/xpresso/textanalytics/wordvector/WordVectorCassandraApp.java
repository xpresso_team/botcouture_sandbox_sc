
package  com.abzooba.xpresso.textanalytics.wordvector;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.NoHostAvailableException;
import com.datastax.driver.core.exceptions.QueryExecutionException;
import com.datastax.driver.core.exceptions.QueryValidationException;

/**
 * @author vivek aditya
 *
 * 3:32:02 pm
 * 21-Dec-2015
 * TODO
 */

@SuppressWarnings("unchecked")

public class WordVectorCassandraApp extends WordVectorDB {

    /**
     * @param args
     * create keyspace word2vec with replication = {'class':'SimpleStrategy','replication_factor':1} ;
     * create table wordvectors ( name text,vector list<double>,"all-category" text,"all-confidence" text,"centroid-vector" text ,primary key(name));
     */

    static Cluster cluster;
    static Session session;

    public static void init() {
        String host = "130.211.112.73";
        //		String host = "localhost";
        long start = 0;
        start = System.nanoTime();
        PoolingOptions poolingOptions = new PoolingOptions();
        poolingOptions.setConnectionsPerHost(HostDistance.LOCAL, 4, 10).setConnectionsPerHost(HostDistance.REMOTE, 2, 4);
        poolingOptions.setMaxRequestsPerConnection(HostDistance.LOCAL, 32768).setMaxRequestsPerConnection(HostDistance.REMOTE, 2000);
        cluster = Cluster.builder().withPort(9042).withCredentials("root", "abz00ba").withProtocolVersion(ProtocolVersion.V3).withPoolingOptions(poolingOptions).addContactPoint(host).build();
        session = cluster.connect("word2vec");
        System.out.println("Time Taken for connection " + (System.nanoTime() - start) * Math.pow(10, -9));
    }

    public static void populateDB(String type) throws Exception {
        String popFile = "config/xpressoConfig.properties";
        InputStream is = null;
        if (type.equals("standalone"))
            is = new FileInputStream(new File(popFile));
        else
            is = Thread.currentThread().getContextClassLoader().getResourceAsStream(popFile);
        Properties XpressoProperties = new Properties();
        XpressoProperties.load(is);
        is.close();
        String line = null;
        BufferedReader br = null;
        System.out.println("Started Reading");

        long start = 0;
        br = new BufferedReader(new FileReader(XpressoProperties.getProperty("wv.db.data", "/home/abzooba/others/db/partsaa")));
        start = System.nanoTime();
        int count = 0;
        while ((line = br.readLine()) != null) {
            if (count % 10000 == 0) {
                System.out.println(count);
            }
            executeQuery(createInsertQuery(line), "size");
            count++;
        }
        br.close();
        System.out.println("Time Taken for insertion " + (System.nanoTime() - start) * Math.pow(10, -9));
    }

    public static String createInsertQuery(String line) {
        String outputLine = null;
        StringBuilder output = new StringBuilder("INSERT INTO wordvectors (name, vector) VALUES ('");
        String vec[] = line.split(" ", 2);
        output.append(vec[0].replaceAll("'", "''"));
        output.append("',[");
        output.append(vec[1].replaceAll(" ", ","));
        output.append("]);");
        outputLine = output.toString();
        return outputLine;
    }

    public static void main(String[] args) {
        init();
        //		try {
        //			populateDB("standalone");
        //		} catch (Exception e) {
        //			e.printStackTrace();
        //		}
        WordVectorDB a = new WordVectorCassandraApp();
        Languages language = Languages.EN;
        System.out.println(WordVector.cosSimilarity(a.getWordVectorForKey(language, "bar"), a.getWordVectorForKey(language, "car")));
        System.out.println(WordVector.cosSimilarity(a.getWordVectorForKey(language, "deer"), a.getWordVectorForKey(language, "cat")));
        //		a.updateData(language, "zdog", "all-category", "ZANIMALS");
        System.out.println(a.getWordVectorCategoryForKey(language, "bar"));
        //		a.updateData(language, "bar", "all-category", "BARS");
        System.out.println(a.getWordVectorCategoryForKey(language, "bar", "all-category"));
        System.out.println(a.getColumnObjectForKey(language, "bar", "vector"));
        List<?> categoriesList = (List<?>) Arrays.asList(((String) a.getColumnObjectForKey(language, "xpresso-aspect-categories-list", "all-category")).replaceAll("\\[", "").replaceAll("\\]", "").split(","));
        System.out.println(categoriesList);
        cluster.close();
    }

    public static Object executeQuery(String... params) {

        try {
            ResultSet results = session.execute(params[0]);
            switch (params[1]) {
                case ("size"):
                    return results.all().size();
                case ("vector"):
                    for (Row row : results) {
                        return row.getObject(4);
                    }
                case ("category"):
                    for (Row row : results) {
                        return row.getObject(params[2]);
                    }
            }
        } catch (NoHostAvailableException | QueryExecutionException | QueryValidationException ex) {
            System.out.println("Insert Failed for " + params[0]);
            System.out.println(ex);
        }
        return null;
    }

    public List<Double> getWordVectorForKey(Languages language, String key) {
        key = key.replaceAll("'", "");
        String strSQL = "SELECT * from wordvectors WHERE name ='" + key + "';";
        List<Double> vector = (List<Double>) executeQuery(strSQL, "vector");
        return vector;
    }

    public String getWordVectorCategoryForKey(Languages language, String key) {
        key = key.replaceAll("'", "");
        return (String) getColumnObjectForKey(language, key, "all-category");
    }

    public String getWordVectorCategoryForKey(Languages language, String key, String columnName) {
        key = key.replaceAll("'", "");
        return (String) getColumnObjectForKey(language, key, columnName);
    }

    public Object getColumnObjectForKey(Languages language, String key, String columnName) {
        key = key.replaceAll("'", "");
        Object category = null;
        String strSQL = "SELECT * from wordvectors WHERE name ='" + key + "';";
        try {
            category = executeQuery(strSQL, "category", columnName);
            return category;
        } catch (Exception e) {
            System.out.println(e);
        }
        return category;
    }

    public void updateData(Languages language, Set<String> entityList, String columnName, String category) {
        for (String entity : entityList) {
            updateData(language, entity, columnName, category);
        }
    }

    public void updateData(Languages language, String key, String columnName, Object value) {
        key = key.replaceAll("'", "");
        String strSQL = "SELECT count(*) FROM wordvectors WHERE name = '" + key + "';";
        int cnt = (int) executeQuery(strSQL, "size");
        if (cnt != 0) {
            if (cnt == 1) {
                strSQL = "UPDATE wordvectors SET \"" + columnName + "\" = '" + value + "' where name = '" + key + "';";
                executeQuery(strSQL, "size");
            } else {
                strSQL = "INSERT INTO wordvectors (name, \"" + columnName + "\") values ('" + key + "','" + value.toString() + "');";
                executeQuery(strSQL, "size");
            }
        }
    }
}