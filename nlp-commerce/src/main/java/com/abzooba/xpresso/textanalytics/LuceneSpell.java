/**
 *
 */
package com.abzooba.xpresso.textanalytics;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.spell.PlainTextDictionary;
import org.apache.lucene.search.spell.SpellChecker;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpEngine;
import com.abzooba.xpresso.utils.FileIO;

//import org.apache.lucene.analysis.standard.StandardAnalyzer;

/**
 * @author Koustuv Saha
 * 04-Apr-2014 3:52:36 pm
 * XpressoV2.0.1 SpellChecker
 */

public class LuceneSpell {

    private static Map<Languages, IndexWriterConfig> configMap = new ConcurrentHashMap<Languages, IndexWriterConfig>();
    private static Map<Languages, java.nio.file.Path> dirMap = new ConcurrentHashMap<Languages, java.nio.file.Path>();
    private static Map<Languages, Directory> directoryMap = new ConcurrentHashMap<Languages, Directory>();
    private static Map<Languages, SpellChecker> spellCheckerMap = new ConcurrentHashMap<Languages, SpellChecker>();

    public static final Map<Languages, Set<String>> allWordsHash = new ConcurrentHashMap<Languages, Set<String>>();

    public static void init() {
        // pw = new PrintWriter(new
        // FileWriter("log/spellCorrectedStrings.txt"));

        try {
            for (Languages language : XpConfig.LANGUAGES) {
                //			config.put(language, new IndexWriterConfig(Version.LATEST, new StandardAnalyzer()));
                // configMap.put(language, new IndexWriterConfig(new StandardAnalyzer()));
                // The default constructor of StandardAnalyzer uses a default English stopwords set.

                //				configMap.put(language, new IndexWriterConfig(new StandardAnalyzer(XpConfig.getStopWords(language)))));

                try {
                    configMap.put(language, new IndexWriterConfig(new StandardAnalyzer(new InputStreamReader(FileIO.getInputStreamFromFileName(XpConfig.getStopWords(language))))));
                } catch (IOException e) {
                    configMap.put(language, new IndexWriterConfig(new StandardAnalyzer()));
                    e.printStackTrace();
                }
                //				findStreamInClasspathOrFileSystem

                Path path = FileSystems.getDefault().getPath("index-folder/" + language.toString().toLowerCase());
                dirMap.put(language, path);
                Directory indexDirectory;
                indexDirectory = FSDirectory.open(path);
                directoryMap.put(language, indexDirectory);

                SpellChecker spellChecker = new SpellChecker(indexDirectory);

                spellCheckerMap.put(language, spellChecker);
                Stack<String> tempColl = new Stack<String>();
                FileIO.read_file(XpConfig.getDictionary(language), tempColl);
                Set<String> allWordsHashLanguage = new HashSet<String>();
                while (!tempColl.isEmpty()) {
                    String text = tempColl.pop();
                    allWordsHashLanguage.add(text);
                    String lowerText = text.toLowerCase();
                    if (!lowerText.equals(text)) {
                        allWordsHashLanguage.add(lowerText);
                    }
                }
                System.out.println(language + " allWordsHashLanguage size: " + allWordsHashLanguage.size());
                allWordsHash.put(language, allWordsHashLanguage);
                //				Path dictionaryPath = FileSystems.getDefault().getPath(XpConfig.getDictionary(language));
                //				Path dictionaryPath = FileSystems.getDefault().getAbPath(XpConfig.getDictionary(language));
                //				spellCheckerMap.get(language).indexDictionary(new PlainTextDictionary(dictionaryPath), configMap.get(language), true);

                spellCheckerMap.get(language).indexDictionary(new PlainTextDictionary(FileIO.getInputStreamFromFileName(XpConfig.getDictionary(language))), configMap.get(language), true);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static boolean checkIfCorrectWord(Languages language, String str) {

        return allWordsHash.get(language).contains(str);

        //		if (!checkStatus) {
        //			checkStatus = allWordsHash.get(language).contains(str.toLowerCase());
        //		}
        //		if (!checkStatus) {
        //			checkStatus = allWordsHash.get(language).contains(CoreNLPController.lemmatizeToString(language, str.toLowerCase()));
        //		}
        //		return checkStatus;
    }

    public static boolean checkIfCorrectWord(Languages language, String str, boolean checkLemma) {

        boolean checkStatus = false;
        checkStatus = checkIfCorrectWord(language, str);

        if (!checkStatus && checkLemma) {
            checkStatus = checkIfCorrectWord(language, CoreNLPController.lemmatizeToString(language, str));
        }
        return checkStatus;
    }

    private static String rbSpellCorrector(String str) {
        String retStr = null;
        int idx = -1;

        if (str.endsWith("iest")) {
            idx = str.lastIndexOf("iest");
            retStr = str.substring(0, idx) + "y";
        } else if (str.endsWith("ier")) {
            idx = str.lastIndexOf("ier");
            retStr = str.substring(0, idx) + "y";
        } else if (str.endsWith("er")) {
            idx = str.lastIndexOf("er");
            retStr = str.substring(0, idx);
        } else if (str.endsWith("est")) {
            idx = str.lastIndexOf("est");
            retStr = str.substring(0, idx);
        }
        return retStr;

    }

    /**
     * 18-Apr-2014 Koustuv Saha corr. 09-Jul-2015
     * Returns the spell corrected word of a word
     * passed in as argument
     *
     * @param str
     * @return str
     * @throws IOException
     */
    //	public static String correctSpellingWord(String str) {
    //		return correctSpellingWord(Languages.EN, str);
    //	}

    /**
     * 18-Apr-2014 Koustuv Saha
     * Returns the spell corrected word of a word
     * passed in as argument
     *
     * @param str
     * @return str
     * @throws IOException
     */
    public static String correctSpellingWord(Languages language, String str) {
        String retStr = null;
        str = str.toLowerCase();
        try {
            if (spellCheckerMap.get(language) == null) {
                System.out.println("Spell Checker Null");
            } else {

                if (str.length() > 3 && !str.contains(" ") && !spellCheckerMap.get(language).exist(str)) {
                    String lemma = CoreNLPController.lemmatizeToString(language, str);

                    if (!spellCheckerMap.get(language).exist(lemma)) {
                        if (language == Languages.EN)
                            retStr = rbSpellCorrector(str);
                        if (retStr == null) {
                            // String[] suggestions = spellCheckerMap.get(language).suggestSimilar(str, 1, (float) 0.80);
                            String[] suggestions;
                            switch (language) {
                                case SP:
                                    suggestions = spellCheckerMap.get(language).suggestSimilar(str, 5, (float) 0.70);
                                    break;
                                default:
                                    float accuracy = 0.0F;
                                    if (str.length() <= 6) {
                                        accuracy = 0.8F;
                                    } else {
                                        accuracy = 0.7F;
                                    }
                                    suggestions = spellCheckerMap.get(language).suggestSimilar(str, 5, accuracy);
                                    //									suggestions = spellCheckerMap.get(language).suggestSimilar(str, 5, indexReader, "field", SuggestMode.SUGGEST_MORE_POPULAR, 0.7);

                                    //									System.out.println(Arrays.toString(suggestions));
                            }
                            // String[] suggestions = spellChecker.suggestSimilar(str,
                            // 5);
                            // if (suggestions == null || suggestions.length == 0) {
                            // suggestions = spellChecker.suggestSimilar(lemma, 1,
                            // (float) 0.80);
                            // }
                            if (suggestions != null && suggestions.length > 0) {
                                retStr = suggestions[0];
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // if (retStr != null) {
        // pw.println("Spell Corrected for:\t" + str + "\t" + retStr);
        // }
        return retStr;
    }

    public static String[] suggestSpellingWord(Languages language, String str, float tol, int numSug) {
        String[] suggestions = null;
        str = str.toLowerCase();
        try {
            if (spellCheckerMap.get(language) == null) {
                System.out.println("Spell Checker Null");
            } else {
                if (str.length() > 3 && !str.contains(" ") && !spellCheckerMap.get(language).exist(str)) {
                    String lemma = CoreNLPController.lemmatizeToString(language, str);

                    if (!spellCheckerMap.get(language).exist(lemma)) {
                        suggestions = spellCheckerMap.get(language).suggestSimilar(str, numSug, tol);
                    }
                }
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return suggestions;
    }

    /**
     * 17-Apr-2014 Koustuv Saha
     * Takes in a string and returns the spell
     * corrected version of the same string
     *
     * @param str
     * @return str
     * @throws IOException
     */
    public static String correctSpellingString(Languages language, String str) {
        // String mod_str = str.replaceAll("[^a-z\\sA-Z]", "");
        String mod_str = Pattern.compile("^\\p{L}\\s", Pattern.UNICODE_CHARACTER_CLASS).matcher(str).replaceAll("");
        // if (mod_str != null) {
        // HashSet<String> nounPhrases =
        // CuratorController.getNounPhrases(mod_str);
        // if (nounPhrases != null) {
        // for (String np : nounPhrases) {
        // mod_str = mod_str.replaceAll(np, "");
        // }
        // }
        // }

        StringTokenizer st = new StringTokenizer(mod_str);
        while (st.hasMoreTokens()) {
            String currWord = st.nextToken().toLowerCase();
            String corrWord = currWord;
            // if (!currWord.contains("\\\\W")) {
            // System.out.println("Doing for " + currWord);
            // System.out.println("Current Word: " + currWord);
            // if (!TestJAWS.isPresent(currWord)) {
            // if (!spellChecker.exist(currWord)) {
            // if (!allWordsHash.contains(currWord) &&
            // !TestJAWS.isPresent(currWord)) {
            corrWord = correctSpellingWord(language, currWord);
            // System.out.println("Changing " + currWord + " to " +
            // corrWord);
            if (corrWord != null && !currWord.equals(corrWord)) {
                str = str.replaceAll(currWord, corrWord);
                // System.out.println("Replacing " + currWord + " with "
                // +
                // corrWord);
            }
            // }
            // }
        }

        return str;

    }

    public static void main(String[] args) throws IOException {
        // init();
        XpEngine.init(true);
        String[] wordsEn = { "coperative", "chickn" };
        //		String[] sentences = {"Ese pastél esta tan rico", "No salío por avería.", "No salió por averia.", "No salio por averia.", "Ademas tienen una pagina web de lo mas completa."};
        //		float[] tolerances = {(float) 0.60, (float) 0.65, (float) 0.70, (float) 0.75, (float) 0.80};

        System.out.println("------------------------------");
        for (String word : wordsEn) {
            System.out.println("Word " + word + " corrected as " + correctSpellingWord(Languages.EN, word));
            System.out.println("String " + word + " corrected as " + correctSpellingString(Languages.EN, word));
        }
        System.exit(1);
        // TestJAWS.init();
        // CuratorController.init();
        //		System.out.println(correctSpellingWord("friendliest"));
        //		System.out.println(correctSpellingWord("friendlier"));
        //		System.out.println(correctSpellingWord("bestest"));
        //		System.out.println(correctSpellingWord("hughe"));
        //		System.out.println(correctSpellingWord("hughe"));
        //		System.out.println(correctSpellingWord("fustrated"));
        //		System.out.println(correctSpellingWord("frustrated"));
        //		System.out.println(correctSpellingWord("availbale"));
        //		System.out.println(correctSpellingWord("chickenn"));
        //		System.out.println(correctSpellingString("It doesn't fitt to my country Portugal it means that it doesn't work with phone app. But is a furby problem"));
        //		System.out.println(correctSpellingString("dont"));
        //		System.out.println(correctSpellingString("Thank"));
        //		System.out.println(correctSpellingString("Furby Boom is a great toy"));
        //		System.out.println(correctSpellingString("kettering"));
        //		System.out.println(correctSpellingString("im"));
        //		System.out.println(correctSpellingString("kken"));
        //		System.out.println(correctSpellingString("fracking"));
        //		System.out.println(correctSpellingString("current"));
        //		System.out.println(correctSpellingString("ba"));
        //		System.out.println(correctSpellingString("frack"));

        System.out.println("Distance(SP): " + LuceneSpell.spellCheckerMap.get(Languages.SP).getStringDistance());
        System.out.println("Comparator(SP): " + LuceneSpell.spellCheckerMap.get(Languages.SP).getComparator().getClass());

        System.out.println("Distance(EN): " + LuceneSpell.spellCheckerMap.get(Languages.EN).getStringDistance());
        System.out.println("Comparator(EN): " + LuceneSpell.spellCheckerMap.get(Languages.EN).getComparator().getClass());

        String[] words = { "admiracion", "admiración", "admración", "dependiendo", "depiendendo", "herecnia", "hernecia", "herencia", "hwrencia", "asiáitcos", "asáticos", "asiaticos", "asiátcos", "accceder", "ingles", "encontrara", "estas", "estás", "estan", "salío", "saldre", "pagina", "páguna", "pégina", "patetico" };
        //		String[] sentences = {"Ese pastél esta tan rico", "No salío por avería.", "No salió por averia.", "No salio por averia.", "Ademas tienen una pagina web de lo mas completa."};
        //		float[] tolerances = {(float) 0.60, (float) 0.65, (float) 0.70, (float) 0.75, (float) 0.80};

        System.out.println("------------------------------");
        for (String word : words) {
            System.out.println("Word " + word + " corrected as " + correctSpellingWord(Languages.SP, word));
            System.out.println("String " + word + " corrected as " + correctSpellingString(Languages.SP, word));
            //			for (float tol : tolerances) {
            //				System.out.println("++++++++++++++++");
            //				System.out.println("Tolerance: " + tol);
            //				int[] numSug = {1, 5, 8, 10};
            //				for (int n : numSug) {
            //					String[] suggestions = suggestSpellingWord(Languages.SP, word, tol, n);
            //					if (suggestions != null) {
            //						System.out.println("Suggestions for word " + word + " with n = " + n);
            //						for (String suggestion : suggestions) {
            //							System.out.println(" * " + suggestion);
            //						}
            //					} else {
            //						System.out.println("No suggestion for " + word + " with n = " + n);
            //					}
            //				}
            //			}
            System.out.println("------------------------------");
        }

        //		for (String sentence : sentences) {
        //			System.out.println("Sentence: " + sentence + "\tcorrected as: " + correctSpellingString(Languages.SP, sentence));
        //		}
    }
}