/**
 *
 */
package com.abzooba.xpresso.textanalytics;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpEngine;
import com.abzooba.xpresso.utils.FileIO;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PolarityAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.process.Americanize;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.util.CoreMap;
import org.slf4j.Logger;

/**
 * @author Koustuv Saha
 * 10-Apr-2014 2:08:30 pm
 * XpressoV2.0.1 CoreNLPController
 */
public class CoreNLPController {

	private static Logger logger;
	private static Map<Languages, StanfordCoreNLP> pipelines = new ConcurrentHashMap<Languages, StanfordCoreNLP>();

	private static Map<Languages, Map<String, String>> wordLemmaMaps = new ConcurrentHashMap<Languages, Map<String, String>>();

	public static void init() {
		logger = XCLogger.getSpLogger();
		for (Languages language : XpConfig.LANGUAGES) {
			System.out.println("Pipeline in " + language);
			StanfordCoreNLP pipeline = initPipeline(language);
			pipelines.put(language, pipeline);
			wordLemmaMaps.put(language, initWordLemmaMap(language));
		}
	}

	private static StanfordCoreNLP initPipeline(Languages language) {
		Properties props = new Properties();
		String annotators = XpConfig.getAnnotators(language);
		switch (language) {
			case SP:
				if (XpConfig.STANFORD_SP_DEPPARSE)
					annotators = annotators + ",depparse";
				props.put("annotators", annotators);
				props.put("tokenize.language", "es");
				if (annotators.contains("pos")) {
					props.put("pos.model", "edu/stanford/nlp/models/pos-tagger/spanish/spanish-distsim.tagger");
				}
				if (annotators.contains("ner")) {
					props.put("ner.model", "edu/stanford/nlp/models/ner/spanish.ancora.distsim.s512.crf.ser.gz");
					props.put("ner.applyNumericClassifiers", "false");
					props.put("ner.useSUTime", "false");
				}
				if (annotators.contains("parse")) {
					props.put("parse.model", "edu/stanford/nlp/models/lexparser/spanishPCFG.ser.gz");
				}
				if (XpConfig.STANFORD_SP_DEPPARSE) {
					props.put("depparse.model", XpConfig.STANFORD_SP_DEPPARSE_MODEL);
				}
				break;
			default:
				props.put("tokenize.options", "ptb3Escaping=false");
				props.put("parse.maxlen", "10000");
				if (XpConfig.STANFORD_USE_CASELESS) {
					props.put("pos.model", "edu/stanford/nlp/models/pos-tagger/english-caseless-left3words-distsim.tagger");
				}
				if (XpConfig.STANFORD_USE_SRPARSER) {
					props.put("parse.model", "edu/stanford/nlp/models/srparser/englishSR.ser.gz");
				}
				props.put("annotators", XpConfig.getAnnotators(language));
			}
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		return pipeline;
	}

	private static Map<String, String> initWordLemmaMap(Languages language) {
		Map<String, String> wordLemmaMap = new ConcurrentHashMap<String, String>();
		if (language.equals(Languages.SP)) {
			String lemmatizationFile = XpConfig.RESOURCES_DIR + "/sp/lemmatization-es.txt";
			FileIO.read_file(lemmatizationFile, wordLemmaMap);
		}
		return wordLemmaMap;
	}

	public static StanfordCoreNLP getPipeline(Languages language) {
		return pipelines.get(language);
	}

	//	TODO: STILL IN USE IN com.abzooba.xpresso.customer.tmobile.RuleBase
	//	TODO: STILL IN USE IN com.abzooba.xpresso.aspect.graphdb.SentimentGraphBuilder
	public static void addToLemmaMap(String word, String lemma) {
		addToLemmaMap(Languages.EN, word, lemma);
	}

	public static void addToLemmaMap(Languages language, String word, String lemma) {
		wordLemmaMaps.get(language).put(word, lemma);
	}

	/**
	 * @author Koustuv Saha
	 * Mar 21, 2016
	 * @param language
	 * @param documentText
	 * @return list_lemmas
	 * 
	 */
	public static List<String> getLemmas(Languages language, String documentText) {
		switch (language) {
			case SP:
				return getLemmasSP(documentText);
			default:
				return getLemmasEN(documentText);
		}
	}

	private static List<String> getLemmasEN(String documentText) {
		List<String> lemmas = new LinkedList<String>();
		Annotation document = new Annotation(documentText);
		getPipeline(Languages.EN).annotate(document);
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for (CoreMap sentence : sentences) {
			for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
				lemmas.add(token.get(LemmaAnnotation.class));
			}
		}
		return lemmas;
	}

	private static List<String> getLemmasSP(String documentText) {
		//		This method is different from the corresponding for English: no lemmatization in Spanish CoreNLP
		List<String> lemmas = new LinkedList<String>();
		Annotation document = new Annotation(documentText);
		getPipeline(Languages.SP).annotate(document);
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for (CoreMap sentence : sentences) {
			for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
				String pos = token.get(PartOfSpeechAnnotation.class);
				String key = token.word().toLowerCase() + "/" + pos;
				//	if (wordLemmaMaps.get(Languages.SP).containsKey(token.word())) {
				if (wordLemmaMaps.get(Languages.SP).containsKey(key)) {
					lemmas.add(wordLemmaMaps.get(Languages.SP).get(key));
				} else {
					//					Default: word itself.
					lemmas.add(token.word());
				}
			}
		}
		return lemmas;
	}

	private static String lemmaAnnotate(Languages language, String documentText) {
		switch (language) {
			case SP:
				return lemmaAnnotateSP(documentText);
			default:
				return lemmaAnnotateEN(documentText);
		}
	}

	private static String lemmaAnnotateEN(String documentText) {
		StringBuilder lemmatizedString = new StringBuilder();
		Annotation document = new Annotation(documentText);
		getPipeline(Languages.EN).annotate(document);
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for (CoreMap sentence : sentences) {
			for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
				String word = token.word();
				String lemma = token.get(LemmaAnnotation.class);
				addToLemmaMap(Languages.EN, word, lemma);
				lemmatizedString.append(lemma).append(" ");
			}
		}
		return lemmatizedString.toString().trim();
	}

	private static String lemmaAnnotateSP(String documentText) {
		/*Differs from the corresponding in CoreNLPController: no lemmatization in Spanish CoreNLP*/
		StringBuilder lemmatizedString = new StringBuilder();
		List<String> lemmas = getLemmasSP(documentText);
		for (String lemma : lemmas) {
			lemmatizedString.append(lemma);
			lemmatizedString.append(" ");
		}

		return lemmatizedString.toString().trim();
	}



	private static String polarityAnnotate(Languages language, String text) {
		//		Stanford CoreNLP in Spanish cannot calculate sentiment
		StringBuilder lemmatizedString = new StringBuilder();
		if (language.equals(Languages.EN)) {
			Annotation document = new Annotation(text);
			getPipeline(Languages.EN).annotate(document);
			List<CoreMap> sentences = document.get(SentencesAnnotation.class);
			for (CoreMap sentence : sentences) {
				for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
					String lemma = token.get(PolarityAnnotation.class);
					if (lemma != null) {
						lemmatizedString.append(lemma.toString()).append(" ");
					}
				}
			}
		}
		return lemmatizedString.toString().trim();
	}

	//	TODO: clean the project from references to the following method: methods should work in any language
	public static String lemmatizeToString(String str) {
		return lemmatizeToString(Languages.EN, str);
	}

	public static String lemmatizeToString(Languages language, String str) {
			String lemma = wordLemmaMaps.get(language).get(str);
			if (lemma == null) {
				StringBuilder lemmatizedSB = new StringBuilder();
				List<String> tokenList = ProcessString.tokenizeString(language, str);
				for (String token : tokenList) {
					lemma = wordLemmaMaps.get(language).get(token);
					if (lemma == null) {
						lemma = lemmaAnnotate(language, token);
					}
					lemmatizedSB.append(lemma).append(" ");
				}
				lemma = lemmatizedSB.toString().trim();
			}
			return lemma;
	}

	/**
	 * @author Koustuv Saha
	 * Mar 21, 2016
	 * @param documentText
	 * @return list_pos_tags
	 * @throws Exception
	 * 
	 */
	public static List<String> getPOS(String documentText) throws Exception {
		return getPOS(Languages.EN, documentText);
	}

	public static List<String> getPOS(Languages language, String documentText) throws Exception {
		List<String> posList = new LinkedList<String>();
		// Create an empty Annotation just with the given text
		Annotation document = new Annotation(documentText);
		// run all Annotators on this text
		getPipeline(language).annotate(document);
		// Iterate over all of the sentences found
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for (CoreMap sentence : sentences) {
			// traversing the words in the current sentence
			// a CoreLabel is a CoreMap with additional token-specific methods
			for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
				// this is the text of the token
				String word = token.get(TextAnnotation.class);
				// this is the POS tag of the token
				String pos = token.get(PartOfSpeechAnnotation.class);
				String word_pos = word + "/" + pos;
				posList.add(word_pos);
			}
		}
		return posList;
	}

	public static List<String> splitSentences(String documentText) {
		return splitSentences(Languages.EN, documentText);
	}

	public static List<String> splitSentences(Languages language, String documentText) {
		List<String> sentencesList = new ArrayList<String>();
		// Create an empty Annotation just with the given text
		Annotation document = new Annotation(documentText);
		// run all Annotators on this text
		getPipeline(language).annotate(document);
		// Iterate over all of the sentences found
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for (CoreMap sentence : sentences) {
			sentencesList.add(sentence.toString());
		}
		return sentencesList;
	}

	public static String parseDependency(String text) throws Exception {
		return parseDependency(Languages.EN, text);
	}

	public static String parseDependency(Languages language, String text) throws Exception {

		String dependencyParsedStr = "";
		//		No dependency parsing in Spanish
		if (language.equals(Languages.EN)) {
			// create an empty Annotation just with the given text
			Annotation document = new Annotation(text);

			// run all Annotators on this text
			getPipeline(Languages.EN).annotate(document);

			// these are all the sentences in this document
			// a CoreMap is essentially a Map that uses class objects as keys and
			// has values with custom types
			List<CoreMap> sentences = document.get(SentencesAnnotation.class);

			if (sentences.size() > 0) {
				CoreMap sentence = (CoreMap) sentences.get(0);
				Tree tree = sentence.get(TreeAnnotation.class);
				// Get dependency tree
				TreebankLanguagePack tlp = new PennTreebankLanguagePack();
				GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
				GrammaticalStructure gs = gsf.newGrammaticalStructure(tree);
				Collection<TypedDependency> td = gs.typedDependenciesCollapsed();
				for (TypedDependency item : td) {
					String item_mod = item.toString().replaceAll("-[0-9]+", "");
					dependencyParsedStr += item_mod + "\n";
				}
			}
		}

		return dependencyParsedStr;
	}

	public static void main(String[] args) throws Exception {
		XpEngine.init(true);

		String text = "This car is knocked off the bridge";
		System.out.println("polarity text : " + polarityAnnotate(Languages.EN, text));

		String str = "colour";
		System.out.println(str + "\t" + Americanize.americanize(str, true));

		str = "humour is good";
		System.out.println(str + "\t" + Americanize.americanize(str, true));
		str = "centre";
		System.out.println(str + "\t" + Americanize.americanize(str, true));

		str = "fibre";
		System.out.println(str + "\t" + Americanize.americanize(str, true));

		str = "apologise";
		System.out.println(str + "\t" + Americanize.americanize(str, true));

		str = "fuelled";
		System.out.println(str + "\t" + Americanize.americanize(str, true));
	}
}
