/**
 * @author Alix Melchy 
 * Aug 10, 2015 7:19:34 PM
 */
package com.abzooba.xpresso.textanalytics;

import com.abzooba.xpresso.engine.config.XpConfig.Languages;

public class PosTags {

	/**
	 * 
	 */

	public static boolean isNoun(Languages lang, String posTag) {
		boolean answ = false;
		if (posTag == null)
			return answ;

		switch (lang) {
			case EN:
				answ = posTag.contains("NN");
				break;
			case SP:
				answ = (posTag.startsWith("nc0s") || posTag.startsWith("nc0p") || posTag.startsWith("np0"));
				break;
		}

		return answ;
	}

	public static boolean isCommonNoun(Languages lang, String posTag) {
		boolean answ = false;
		if (posTag == null)
			return answ;

		switch (lang) {
			case EN:
				answ = posTag.equals("NN");
				//				answ = posTag.equals("NN") || posTag.equals("NNS");
				break;
			case SP:
				answ = (posTag.startsWith("nc0s") || posTag.startsWith("nc0p"));
				break;
		}

		return answ;
	}

	public static boolean isProperNoun(Languages lang, String posTag) {
		boolean answ = false;
		if (posTag == null)
			return answ;

		switch (lang) {
			case EN:
				answ = posTag.equals("NNP");
				break;
			case SP:
				answ = posTag.startsWith("np0");
				break;
		}

		return answ;
	}

	public static boolean isAdjective(Languages lang, String posTag) {
		boolean answ = false;
		if (posTag == null)
			return answ;

		switch (lang) {
			case EN:
				answ = posTag.contains("JJ");
				break;
			case SP:
				answ = posTag.startsWith("aq");
				break;
		}

		return answ;
	}

	public static boolean isComparativeAdjective(Languages lang, String posTag) {
		boolean answ = false;
		switch (lang) {
			case EN:
				answ = posTag.equals("JJR");
				break;
			case SP:
				//	currently not supported in Stanford CoreNLP but exists in EAGLES posTag
				answ = posTag.startsWith("aqc");
				break;
		}

		return answ;
	}

	public static boolean isComparativeAdverb(Languages lang, String posTag) {
		boolean answ = false;
		switch (lang) {
			case EN:
				answ = posTag.equals("RBR");
				break;
			case SP:
				//	Not supported: TODO requires looking for dependent 'más'
				break;
		}

		return answ;
	}

	public static boolean isVerb(Languages lang, String posTag) {
		boolean answ = false;
		switch (lang) {
			case EN:
				answ = posTag.matches("(VB[A-Z]*)");
				break;
			case SP:
				answ = posTag.startsWith("v");
				break;
		}

		return answ;
	}

	public static boolean isMainVerb(Languages lang, String posTag) {
		boolean answ = false;
		if (posTag == null)
			return answ;

		switch (lang) {
			case EN:
				answ = posTag.contains("VB");
				break;
			case SP:
				answ = posTag.startsWith("vm");
				break;
		}

		return answ;
	}

	public static boolean isVerbBase(Languages lang, String posTag) {
		boolean answ = false;
		if (posTag == null)
			return answ;

		switch (lang) {
			case EN:
				answ = posTag.equals("VB");
				break;
			case SP:
				answ = posTag.startsWith("vm");
				break;
		}

		return answ;
	}

	public static boolean isPastVerb(Languages lang, String posTag) {
		boolean answ = false;
		if (posTag == null)
			return answ;

		switch (lang) {
			case EN:
				answ = posTag.equals("VBD");
				break;
			case SP:
				answ = posTag.startsWith("v") && (posTag.endsWith("i000") || posTag.endsWith("s000"));
				break;
		}

		return answ;
	}

	public static boolean isAdverb(Languages lang, String posTag) {
		boolean answ = false;
		if (posTag == null)
			return answ;

		switch (lang) {
			case EN:
				answ = posTag.contains("RB");
				break;
			case SP:
				answ = (posTag.contains("rg") || posTag.contains("rn"));
				break;
		}

		return answ;
	}

	public static boolean isPersonalPronoun(Languages lang, String posTag) {
		boolean answ = false;
		switch (lang) {
			case EN:
				answ = posTag.contains("PRP");
				break;
			case SP:
				answ = posTag.startsWith("pp");
				break;
		}

		return answ;
	}

	public static boolean isNumeral(Languages lang, String posTag) {
		boolean answ = false;
		switch (lang) {
			case SP:
				answ = posTag.equals("z0");
				break;
			case EN:
				answ = posTag.equals("CD");
				break;
		}

		return answ;
	}

	public static String getValidTag(Languages language, String posTag) {
		String newTag = null;
		if (posTag == null)
			return newTag;

		switch (language) {
			case EN:
				newTag = (posTag.matches("VB[A-Z]*")) ? "verb" : newTag;
				newTag = (posTag.matches("NN[PS]*")) ? "noun" : newTag;
				newTag = (posTag.matches("JJ[RS]*")) ? "adjective" : newTag;
				newTag = (posTag.matches("RB[RS]*")) ? "adverb" : newTag;
				newTag = (posTag.matches("CC")) ? "conjunction" : newTag;
				newTag = (posTag.matches("PRP[\\$]*")) ? "pronoun" : newTag;
				newTag = (posTag.matches("UH")) ? "interjection" : newTag;
				newTag = (posTag.matches("(IN)|(TO)")) ? "preposition" : newTag;
				newTag = (posTag.matches("(MD)")) ? "modal_verb" : newTag;
				break;
			case SP:
				newTag = (posTag.matches("vm[gicfpsmn]+0+")) ? "verb" : newTag;
				newTag = (posTag.matches("n[cp]0[0ps]0{3}")) ? "noun" : newTag;
				newTag = (posTag.matches("aq0{4}")) ? "adjective" : newTag;
				newTag = (posTag.matches("r[gn]")) ? "adverb" : newTag;
				newTag = (posTag.matches("cc")) ? "conjunction" : newTag;
				newTag = (posTag.matches("p[0deinprtx]0{6}")) ? "pronoun" : newTag;
				newTag = (posTag.matches("i")) ? "interjection" : newTag;
				newTag = (posTag.matches("sp000")) ? "preposition" : newTag;
				/*The following mathes auxilliary and semi-auxilliary verbs*/
				newTag = (posTag.matches("v[as][gicfpsmn]+0+")) ? "modal_verb" : newTag;
				break;
		}
		return newTag;
	}

}
