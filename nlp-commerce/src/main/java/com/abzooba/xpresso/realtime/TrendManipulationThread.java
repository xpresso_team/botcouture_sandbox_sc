package com.abzooba.xpresso.realtime;

import java.util.Map;
import java.util.Set;

import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.aspect.domainontology.OntologyUtils;
import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Annotations;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpExpression;
import com.abzooba.xpresso.expressions.sentiment.XpSentiment;

public class TrendManipulationThread implements Runnable {

    Languages language = null;
    String reviewID = null;
    Annotations annotation = null;
    boolean isHistoricalDemoMode = false;
    String reviewStr = null;
    String source = null;
    boolean deleteTrendTopic = false;
    String domainName = null;
    String trendField = null;
    XpExpression review = null;
    String subject = null;

    public TrendManipulationThread(XpExpression review) {
        this.review = review;
        this.domainName = this.review.getDomainName();
        this.deleteTrendTopic = true;
    }

    public TrendManipulationThread(Languages language, String reviewID, Annotations annotation, String domainName, String subject, String reviewStr, String date, boolean isHistoricalDemoMode, String source, boolean deleteTrendTopic, String trendField) {
        this.language = language;
        this.reviewID = reviewID;
        this.annotation = annotation;
        this.domainName = domainName;
        this.subject = subject;
        this.isHistoricalDemoMode = isHistoricalDemoMode;
        this.reviewStr = reviewStr;
        this.source = source;
        this.deleteTrendTopic = deleteTrendTopic;
        this.trendField = trendField;
    }

    public TrendManipulationThread(Languages language, String reviewID, Annotations annotation, String domainName, String subject, String reviewStr, String date, boolean isHistoricalDemoMode, String source, boolean deleteTrendTopic) {
        this.language = language;
        this.reviewID = reviewID;
        this.annotation = annotation;
        this.domainName = domainName;
        this.subject = subject;
        this.isHistoricalDemoMode = isHistoricalDemoMode;
        this.reviewStr = reviewStr;
        this.source = source;
        this.deleteTrendTopic = deleteTrendTopic;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        if (review == null) {
            review = new XpExpression(language, annotation, isHistoricalDemoMode, reviewID, domainName, subject, reviewStr, "twitter".equalsIgnoreCase(source));
        }
        Map<String, Integer> entityMap = review.getEntityMap();
        //		Map<String, Integer> entityMap = review.mentionBasedEntities();
        //		entityMap = review.processPipelineForEntities(true);

        XCLogger.getSpLogger().info("In TrendManipulationThread entityMap : " + entityMap);
        Set<String> reviewEntities = entityMap.keySet();
        for (String topicField : reviewEntities) {
            topicField = topicField.trim();
            String lexTag = XpSentiment.getLexTag(Languages.EN, topicField, null, null);
            if (topicField.length() > KeywordRelevanceComputation.MAX_ENTITY_LENGTH || OntologyUtils.isRejectEntity(topicField))
                continue;
            else if (lexTag != null && lexTag.matches("(" + XpSentiment.INTENSIFIER_TAG + ")|(" + XpSentiment.SHIFTER_TAG + ")|(" + XpSentiment.INCREASER_TAG + ")"))
                continue;
            else if (lexTag != null && lexTag.matches("(" + XpSentiment.DECREASER_TAG + ")|(" + XpSentiment.NEED_TAG + ")|(" + XpSentiment.CONSUME_TAG + ")"))
                continue;
            else if (lexTag != null && lexTag.matches("(" + XpSentiment.RESOURCES_TAG + ")|(" + XpSentiment.SWITCHER_TAG + ")|(" + XpSentiment.POSSESSION_TAG + ")"))
                continue;
            //			TrendInfoManipulation.updateTrendInfo(topicField, 1.0, deleteTrendTopic);
            switch (XpConfig.TREND_TYPE.intValue()) {
                case 1:
                    TrendInfoManipulation.updateTrendInfo(topicField, 1.0, deleteTrendTopic, domainName);
                    break;
                case 2:
                    TrendInfoManipulation.updateAspectTrendInfo(topicField, 1.0, deleteTrendTopic, domainName);
                    break;
                case 3:
                    //					System.out.println("in Trend type case 3");
                    while (TrendInfoManipulation.isDomainPresent(domainName)) {
                        try {
                            Thread.sleep(5);
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                    TrendInfoManipulation.updateTopicDomainSet(domainName, false);
                    TrendInfoManipulation.updateDomainTrendInfo(topicField, 1.0, deleteTrendTopic, domainName);
                    TrendInfoManipulation.updateTopicDomainSet(domainName, true);

                    //					if (XpConfig.ENABLE_TREND_ACTIVATOR) {
                    //						System.out.println("trend activator enabled");
                    //						TrendInfoManipulation.outputTrendInfo("./output/intermediate_topic_sync.txt", domainName);
                    //						JSONObject trendJSON = TrendInfoManipulation.prepareTrendJSON(domainName);
                    //
                    //						try {
                    //							@SuppressWarnings("resource")
                    //							PrintWriter pw = new PrintWriter("output/MCI_auto_testcases_" + XpEngine.getCurrentDateStr() + ".txt");
                    //							pw.println("Id\tReview\tTrendingTopics");
                    //							if (trendJSON != null && trendJSON.length() > 0) {
                    //								JSONArray trendingTopics = trendJSON.getJSONArray("trending topics");
                    //								System.out.println("trend : " + trendingTopics.toString());
                    //								pw.println(reviewID + "\t" + reviewStr + "\t" + trendingTopics.toString());
                    //							} else {
                    //								pw.println(reviewID + "\t" + reviewStr + "\t" + "null");
                    //								System.out.println("null");
                    //							}
                    //						} catch (JSONException | FileNotFoundException e) {
                    //							// TODO Auto-generated catch block
                    //							e.printStackTrace();
                    //						}

                    //					}
                    break;
                default:
                    TrendInfoManipulation.updateTrendInfo(topicField, 1.0, deleteTrendTopic, domainName);
            }
            //			TrendInfoManipulation.updateTrendInfo(topicField, 1.0, deleteTrendTopic, domainName);
        }

        //		System.out.println(ParallelTrendPrediction.updateReviewsProcessed() + "\t" + reviewID);
    }
}