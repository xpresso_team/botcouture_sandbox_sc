package  com.abzooba.xpresso.realtime;

public class EntitySignificanceThread implements Runnable {

    //	private String domain;
    //	private String entity;
    private double meanTfIdfScore;
    //	private double entityScore;
    private EntityInfoClass entityInfo;

    //	public EntitySignificanceThread(String entity, double entityScore, double meanTfIdfScore, String domain) {
    //		this.domain = domain;
    //		this.entity = entity;
    //		this.meanTfIdfScore = meanTfIdfScore;
    //		this.entityScore = entityScore;
    //	}

    //	public EntitySignificanceThread(String entity, EntityInfoClass entityInfo, double meanTfIdfScore, String domain) {
    //		this.domain = domain;
    //		this.entity = entity;
    //		this.meanTfIdfScore = meanTfIdfScore;
    //		this.entityInfo = entityInfo;
    //	}

    public EntitySignificanceThread(EntityInfoClass entityInfo, double meanTfIdfScore) {
        this.meanTfIdfScore = meanTfIdfScore;
        this.entityInfo = entityInfo;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        double significance = entityInfo.getScore() / meanTfIdfScore - 1;
        //		KeywordRelevanceComputation.updateEntitySignificance(entity, finalScore, domain);
        entityInfo.augmentSignificance(significance);
        //		KeywordRelevanceComputation.augmentEntitySignificance(domain, entity, significance);
    }

}