package  com.abzooba.xpresso.realtime;

public class EntityInfoClass {

    private int docCount = 0;
    private int frequency = 0;
    private double score = 0.0;
    private double significance = 0;
    private double abundance = 0;

    public EntityInfoClass() {
    }

    public EntityInfoClass(int docCount) {
        this.docCount = docCount;
    }

    public int getDocCount() {
        return this.docCount;
    }

    public void setDocCount(int docCount) {
        this.docCount = docCount;
    }

    public void augmentDocCount(int docCount) {
        this.docCount += docCount;
    }

    public int getFrequency() {
        return this.frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public void augmentFrequency(int frequency) {
        this.frequency += frequency;
    }

    public double getScore() {
        return this.score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public void augmentScore(double score) {
        this.score += score;
    }

    public double getSignificance() {
        return this.significance;
    }

    public void setSignificance(double significance) {
        this.significance = significance;
    }

    public void augmentSignificance(double significance) {
        this.significance += significance;
    }

    public double getAbundance() {
        return this.abundance;
    }

    public void setAbundance(double abundance) {
        this.score = abundance;
    }

    public void augmentAbundance(double abundance) {
        this.abundance += abundance;
    }
}