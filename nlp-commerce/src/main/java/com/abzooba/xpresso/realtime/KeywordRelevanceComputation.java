package  com.abzooba.xpresso.realtime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Annotations;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpEngine;
import com.abzooba.xpresso.searchengine.PmiCalculator;
import com.abzooba.xpresso.utils.FileIO;
import com.abzooba.xpresso.utils.ThreadUtils;

public class KeywordRelevanceComputation {

    //	private static Map<String, Map<String, Integer>> domainEntityDocCountMap = new HashMap<String, Map<String, Integer>>();
    //	private static Map<String, Map<String, Integer>> domainEntityFrequencyMap = new ConcurrentHashMap<String, Map<String, Integer>>();
    //	private static Map<String, Map<String, Double>> domainEntityScoreCache = new HashMap<String, Map<String, Double>>();
    //	private static Map<String, Double> entityIdfCache = new HashMap<String, Double>();
    private static Map<String, Double> domainMeanIdfScoreMap = new ConcurrentHashMap<String, Double>();
    private static Map<String, Map<String, Double>> domainEntitySignificanceMap = new ConcurrentHashMap<String, Map<String, Double>>();
    private static Map<String, Map<String, Double>> domainEntityAbundanceCache = new ConcurrentHashMap<String, Map<String, Double>>();
    private static Map<String, Map<String, EntityInfoClass>> domainEntityInfoMap = new ConcurrentHashMap<String, Map<String, EntityInfoClass>>();
    public static String GENERIC_DOC_STR = "#DocumentCount";

    private static final Integer ID_IDX = 0; // null;
    private static final Integer RVW_IDX = 2; // 3 0;
    private static final Integer SUBJ_IDX = null; // 1;
    private static final Integer DATE_IDX = null;
    private static final String FIELD_DELIMITER = "\t";
    private static final boolean IS_HISTORICAL_DEMO_MODE = false;
    private static final String SOURCE = "twitter";
    public static final Double DEFAULT_IDF_DOC = 1.0;
    public static final int THRESHOLD_REVIEW_COUNT = 30000;
    public static final double DOMAIN_UP_FACTOR = 1.5;
    public static final int MAX_ENTITY_LENGTH = 60;

    public static final String RVW_FILENAME = "input/environment/sg_tweets_time_all.txt";
    private static final String CUSTOMER_NAME = "SingaporeGov";
    private static final String DOMAIN_NAME = "government";

    private static final Languages language = Languages.EN;
    private static final Annotations ANNOTATION = Annotations.SENTI;

    private static int reviewsProcessed = 0;
    private static final Integer NUM_THREADS = 10;

    public static int updateReviewsProcessed() {
        reviewsProcessed += 1;
        return reviewsProcessed;
    }

    public static void init() {
        if (XpConfig.IDF_DOWNGRADE && !XpConfig.BATCH_RUN_KEYWORD_SIGNIFICANCE)
            readEntityCountFromFile(XpConfig.ENTITY_DOC_COUNT_FILE);
        //		System.out.println("DoC Entity Loaded");
    }

    public static Double getEntitySignificance(String entity, String domain) {
        Double significance = null;
        Map<String, Double> entityScoreMap = domainEntitySignificanceMap.get(domain);
        if (entityScoreMap != null)
            significance = entityScoreMap.get(entity.toLowerCase());
        return significance;
    }

    public static EntityInfoClass getEntityInfo(String domain, String entity) {
        Map<String, EntityInfoClass> entityInfoMap = domainEntityInfoMap.get(domain);
        if (entityInfoMap != null) {
            return entityInfoMap.get(entity.toLowerCase());
        }
        return null;
    }

    public static void augmentEntityInfoMap(String domain, String entity, EntityInfoClass entityInfo) {
        Map<String, EntityInfoClass> entityInfoMap = domainEntityInfoMap.get(domain);
        if (entityInfoMap != null) {
            entityInfoMap.put(entity.toLowerCase(), entityInfo);
        } else {
            entityInfoMap = new HashMap<String, EntityInfoClass>();
            entityInfoMap.put(entity.toLowerCase(), entityInfo);
            domainEntityInfoMap.put(domain, entityInfoMap);
        }
    }

    public static void augmentEntityDocCount(String domain, String entity, int docCount) {
        Map<String, EntityInfoClass> entityInfoMap = domainEntityInfoMap.get(domain);
        if (entityInfoMap != null) {
            EntityInfoClass entityInfo = entityInfoMap.get(entity.toLowerCase());
            if (entityInfo != null)
                entityInfo.augmentDocCount(docCount);
            else {
                entityInfo = new EntityInfoClass(docCount);
                entityInfoMap.put(entity.toLowerCase(), entityInfo);
            }
        } else {
            EntityInfoClass entityInfo = new EntityInfoClass(docCount);
            entityInfoMap = new HashMap<String, EntityInfoClass>();
            entityInfoMap.put(entity.toLowerCase(), entityInfo);
            domainEntityInfoMap.put(domain, entityInfoMap);
        }
    }

    public static void augmentEntityFrequency(String domain, String entity, int frequency) {
        Map<String, EntityInfoClass> entityInfoMap = domainEntityInfoMap.get(domain);
        if (entityInfoMap != null) {
            EntityInfoClass entityInfo = entityInfoMap.get(entity.toLowerCase());
            if (entityInfo != null)
                entityInfo.augmentFrequency(frequency);
            else {
                entityInfo = new EntityInfoClass();
                entityInfo.setFrequency(frequency);
                entityInfoMap.put(entity.toLowerCase(), entityInfo);
            }
        } else {
            EntityInfoClass entityInfo = new EntityInfoClass();
            entityInfo.setFrequency(frequency);
            entityInfoMap = new HashMap<String, EntityInfoClass>();
            entityInfoMap.put(entity.toLowerCase(), entityInfo);
            domainEntityInfoMap.put(domain, entityInfoMap);
        }
    }

    public static void augmentEntityScore(String domain, String entity, double score) {
        Map<String, EntityInfoClass> entityInfoMap = domainEntityInfoMap.get(domain);
        if (entityInfoMap != null) {
            EntityInfoClass entityInfo = entityInfoMap.get(entity.toLowerCase());
            entityInfo.augmentScore(score);
        }
    }

    public static void augmentEntitySignificance(String domain, String entity, double significance) {
        Map<String, EntityInfoClass> entityInfoMap = domainEntityInfoMap.get(domain);
        if (entityInfoMap != null) {
            EntityInfoClass entityInfo = entityInfoMap.get(entity);
            entityInfo.augmentSignificance(significance);
        }
    }

    //	public static void augmentEntityAbundance(String domain, String entity, double abundance) {
    //		Map<String, EntityInfoClass> entityInfoMap = domainEntityInfoMap.get(domain);
    //		if (entityInfoMap != null) {
    //			EntityInfoClass entityInfo = entityInfoMap.get(entity);
    //			entityInfo.augmentAbundance(abundance);
    //		}
    //	}

    public static Double getMeanIdfScore(String domain) {
        return domainMeanIdfScoreMap.get(domain);
    }

    public static void updateMeanIdfScore(String domain, double score) {
        Double tempScore = domainMeanIdfScoreMap.get(domain);
        if (tempScore != null)
            domainMeanIdfScoreMap.put(domain, Double.valueOf(tempScore.doubleValue() + score));
        else
            domainMeanIdfScoreMap.put(domain, new Double(score));
    }

    public static void setDomainMeanIdfScore(String domain, double score) {
        domainMeanIdfScoreMap.put(domain, Double.valueOf(score));
    }

    //	public static Double getEntityIdfFromCache(String entity) {
    //		return entityIdfCache.get(entity);
    //	}
    //
    //	public static void updateEntityIdfCache(String entity, Double score) {
    //		synchronized (entityIdfCache) {
    //			entityIdfCache.put(entity, score);
    //		}
    //	}

    //	private static Double getEntityScoreFromCache(String domain, String entity) {
    //		Map<String, Double> entityMap = domainEntityScoreCache.get(domain);
    //		if (entityMap == null)
    //			return null;
    //		return entityMap.get(entity.toLowerCase());
    //	}
    //
    //	public static void updateEntityScoreInCache(String entity, Double score, String domain) {
    //		synchronized (domainEntityScoreCache) {
    //			Map<String, Double> entityScoreMap = domainEntityScoreCache.get(domain);
    //			if (entityScoreMap != null)
    //				entityScoreMap.put(entity.toLowerCase(), score);
    //			else {
    //				entityScoreMap = new HashMap<String, Double>();
    //				entityScoreMap.put(entity.toLowerCase(), score);
    //				domainEntityScoreCache.put(domain, entityScoreMap);
    //			}
    //		}
    //	}

    //	public static Integer getEntityDocCount(String entity, String domain) {
    //		Integer count = null;
    //		Map<String, Integer> entityCountMap = domainEntityDocCountMap.get(domain);
    //		if (entityCountMap != null)
    //			count = entityCountMap.get(entity.toLowerCase());
    //		return count;
    //	}
    //
    //	public static Integer getEntityFrequency(String entity, String domain) {
    //		Integer count = null;
    //		Map<String, Integer> entityCountMap = domainEntityFrequencyMap.get(domain);
    //		if (entityCountMap != null)
    //			count = entityCountMap.get(entity.toLowerCase());
    //		return count;
    //	}

    public static void updateEntitySignificance(String entity, Double score, String domain) {
        Map<String, Double> entityScoreMap = domainEntitySignificanceMap.get(domain);
        if (entityScoreMap != null)
            entityScoreMap.put(entity.toLowerCase(), score);
        else {
            entityScoreMap = new HashMap<String, Double>();
            entityScoreMap.put(entity.toLowerCase(), score);
            domainEntitySignificanceMap.put(domain, entityScoreMap);
        }
    }

    public static void setEntityAbundanceInCache(String entity, Double score, String domain) {
        Map<String, Double> entityAbundanceMap = domainEntityAbundanceCache.get(domain);
        if (entityAbundanceMap != null)
            entityAbundanceMap.put(entity.toLowerCase(), score);
        else {
            entityAbundanceMap = new HashMap<String, Double>();
            entityAbundanceMap.put(entity.toLowerCase(), score);
            domainEntityAbundanceCache.put(domain, entityAbundanceMap);
        }
    }

    public static Double getEntityAbundanceFromCache(String entity, String domain) {
        Map<String, Double> entityAbundanceMap = domainEntityAbundanceCache.get(domain);
        if (entityAbundanceMap != null)
            return entityAbundanceMap.get(entity.toLowerCase());
        return null;
    }

    //	public static void updateEntityDocCount(String entity, String domain) {
    //		if (entity == null || domain == null)
    //			return;
    //		synchronized (domainEntityDocCountMap) {
    //			Map<String, Integer> entityCountMap = domainEntityDocCountMap.get(domain);
    //			if (entityCountMap == null) {
    //				entityCountMap = new HashMap<String, Integer>();
    //				entityCountMap.put(entity.toLowerCase(), new Integer(1));
    //				domainEntityDocCountMap.put(domain, entityCountMap);
    //			} else {
    //				Integer entityCount = entityCountMap.get(entity.toLowerCase());
    //				if (entityCount == null)
    //					entityCountMap.put(entity.toLowerCase(), new Integer(1));
    //				else
    //					entityCountMap.put(entity.toLowerCase(), Integer.valueOf(entityCount.intValue() + 1));
    //			}
    //		}
    //	}

    //	public static void updateEntityFrequency(String entity, String domain) {
    //		if (entity == null || domain == null)
    //			return;
    //		Map<String, Integer> entityCountMap = domainEntityFrequencyMap.get(domain);
    //		if (entityCountMap == null) {
    //			entityCountMap = new HashMap<String, Integer>();
    //			entityCountMap.put(entity.toLowerCase(), new Integer(1));
    //			domainEntityFrequencyMap.put(domain, entityCountMap);
    //		} else {
    //			Integer entityCount = entityCountMap.get(entity.toLowerCase());
    //			if (entityCount == null)
    //				entityCountMap.put(entity.toLowerCase(), new Integer(1));
    //			else
    //				entityCountMap.put(entity.toLowerCase(), Integer.valueOf(entityCount.intValue() + 1));
    //		}
    //	}

    public static void readEntityCountFromFile(String fileName) {
        Stack<String> lineStack = new Stack<String>();
        FileIO.read_file(fileName, lineStack);
        //		while (!lineStack.isEmpty()) {
        //			String line = lineStack.pop();
        //			String[] lineSplit = line.split("\t");
        //			if (lineSplit.length != 3)
        //				continue;
        //			String domain = lineSplit[2];
        //			String entity = lineSplit[0];
        //			Integer entityCount = Integer.parseInt(lineSplit[1]);
        //			Map<String, Integer> entityCountMap = domainEntityDocCountMap.get(domain);
        //			if (entityCountMap == null) {
        //				entityCountMap = new HashMap<String, Integer>();
        //				entityCountMap.put(entity, entityCount);
        //				domainEntityDocCountMap.put(domain, entityCountMap);
        //			} else {
        //				entityCountMap.put(entity, entityCount);
        //			}
        //		}

        while (!lineStack.isEmpty()) {
            String line = lineStack.pop();
            String[] lineSplit = line.split("\t");
            if (lineSplit.length != 3)
                continue;
            String domain = lineSplit[2];
            String entity = lineSplit[0];
            if (entity.length() > MAX_ENTITY_LENGTH)
                continue;
            Double entitySignificance = Double.parseDouble(lineSplit[1]);
            Map<String, Double> entityCountMap = domainEntitySignificanceMap.get(domain);
            if (entityCountMap == null) {
                entityCountMap = new HashMap<String, Double>();
                entityCountMap.put(entity.toLowerCase(), entitySignificance);
                domainEntitySignificanceMap.put(domain, entityCountMap);
            } else {
                entityCountMap.put(entity.toLowerCase(), entitySignificance);
            }
        }
    }

    public static void writeEntityCountToFile(String fileName) {
        List<String> lineList = new ArrayList<String>();
        //		for (Entry<String, Map<String, Integer>> domainPair : domainEntityDocCountMap.entrySet()) {
        //			String domain = domainPair.getKey();
        //			Map<String, Integer> entityCountMap = domainPair.getValue();
        //			for (Entry<String, Integer> pair : entityCountMap.entrySet()) {
        //				String lineStr = pair.getKey() + "\t" + pair.getValue() + "\t" + domain;
        //				lineList.add(lineStr);
        //			}
        //		}

        //		for (Entry<String, Map<String, Double>> domainPair : domainEntitySignificanceMap.entrySet()) {
        //			String domain = domainPair.getKey();
        //			Map<String, Double> entitySignificanceMap = domainPair.getValue();
        //			for (Entry<String, Double> pair : entitySignificanceMap.entrySet()) {
        //				String lineStr = pair.getKey() + "\t" + pair.getValue() + "\t" + domain;
        //				lineList.add(lineStr);
        //			}
        //		}

        for (Entry<String, Map<String, EntityInfoClass>> domainPair : domainEntityInfoMap.entrySet()) {
            String domain = domainPair.getKey();
            Map<String, EntityInfoClass> entitySignificanceMap = domainPair.getValue();
            for (Entry<String, EntityInfoClass> pair : entitySignificanceMap.entrySet()) {
                String lineStr = pair.getKey() + "\t" + pair.getValue().getSignificance() + "\t" + domain;
                //				String lineStr = pair.getKey() + "\t" + pair.getValue().getDocCount() + "\t" + pair.getValue().getFrequency() + "\t" + pair.getValue().getScore() + "\t" + getMeanIdfScore(DOMAIN_NAME) + "\t" + pair.getValue().getSignificance() + "\t" + domain;
                lineList.add(lineStr);
            }
        }
        FileIO.write_file(lineList, fileName, false);
    }

    //	public static Double computeIDFScoreForEntity(String entity, String domain) {
    //		Double score = null;
    //		Double score = getEntityScoreFromCache(domain, entity);
    //		if (score != null)
    //			return score;
    //		switch (XpConfig.TREND_IDF_COMPUTE_METHOD) {
    //			case 1:
    //				Integer totalDocCount = getEntityDocCount(GENERIC_DOC_STR, domain);
    //				if (totalDocCount == null)
    //					return DEFAULT_IDF_DOC;
    //				Integer entityDocCount = getEntityDocCount(entity, domain);
    //				if (entityDocCount == null)
    //					score = XpConfig.UP_FACTOR * DEFAULT_IDF_DOC;
    //				else
    //					score = XpConfig.UP_FACTOR - entityDocCount.intValue() * 1.0 / totalDocCount.intValue();
    //				break;
    //			case 2:
    //				totalDocCount = getEntityDocCount(GENERIC_DOC_STR, domain);
    //				if (totalDocCount == null)
    //					return DEFAULT_IDF_DOC;
    //				entityDocCount = getEntityDocCount(entity, domain);
    //				if (entityDocCount == null)
    //					score = XpConfig.UP_FACTOR * DEFAULT_IDF_DOC;
    //				else
    //					score = XpConfig.UP_FACTOR - entityDocCount.intValue() * 1.0 / totalDocCount.intValue();
    //				score = Math.pow(score, XpConfig.DOWNFACTOR_POWER);
    //				break;
    //			case 3:
    //				totalDocCount = getEntityDocCount(GENERIC_DOC_STR, domain);
    //				if (totalDocCount == null)
    //					return DEFAULT_IDF_DOC;
    //				entityDocCount = getEntityDocCount(entity, domain);
    //				if (entityDocCount == null)
    //					score = XpConfig.UP_FACTOR * DEFAULT_IDF_DOC;
    //				else
    //					score = XpConfig.UP_FACTOR - entityDocCount.intValue() * 1.0 / totalDocCount.intValue();
    //				String rawCategory = XpOntology.getRawOntologyCategory(language, entity);
    //				if (rawCategory != null) {
    //					if (XpOntology.isDomainRelevantCategory(language, rawCategory, domain)) {
    //						score *= KeywordRelevanceComputation.DOMAIN_UP_FACTOR;
    //					}
    //				}
    //				//				score = Math.pow(score, XpConfig.DOWNFACTOR_POWER);
    //				break;
    //			default:
    //				totalDocCount = getEntityDocCount(GENERIC_DOC_STR, domain);
    //				if (totalDocCount == null)
    //					return DEFAULT_IDF_DOC;
    //				entityDocCount = getEntityDocCount(entity, domain);
    //				if (entityDocCount == null)
    //					score = XpConfig.UP_FACTOR * DEFAULT_IDF_DOC;
    //				else
    //					score = XpConfig.UP_FACTOR - entityDocCount.intValue() * 1.0 / totalDocCount.intValue();
    //		}
    //
    //		//		if (score >= 0.99)
    //		//			score = 1.0;
    //		updateEntityScoreInCache(entity, score, domain);
    //	return score;
    //
    //	}

    public static double computeEntityAbundance(String entity, String domain) {
        if (domain == null)
            return 1.0;
        Double cachedEntityAbundance = KeywordRelevanceComputation.getEntityAbundanceFromCache(entity, domain);
        if (cachedEntityAbundance != null)
            return cachedEntityAbundance.doubleValue();
        double entityAbundance = 1.0;
        Double score = getEntitySignificance(entity, domain);
        if (score != null) {
            //			if (score.doubleValue() > 0)
            //				entityAbundance += score;
            if (score.doubleValue() > 1) {
                switch (XpConfig.ENTITY_ABUNDANCE_TYPE.intValue()) {
                    case 1:
                        entityAbundance += 1 + Math.log(score) / Math.log(2);
                        break;
                    case 2:
                        entityAbundance += 1 + Math.log(score);
                        break;
                    case 3:
                        entityAbundance += score;
                        break;
                    default:
                        entityAbundance += 1 + Math.log(score);
                        break;
                }
            } else if (score.doubleValue() > 0)
                entityAbundance += score;
        }
        //		else
        //			entityAbundance = 0.8;
        KeywordRelevanceComputation.setEntityAbundanceInCache(entity, entityAbundance, domain);
        return entityAbundance;
    }

    public static void entityRelevanceBatch() {
        Stack<String> reviewList = new Stack<String>();
        FileIO.read_file(RVW_FILENAME, reviewList);
        int size = reviewList.size();
        int cutoffLength;
        if (SUBJ_IDX == null) {
            cutoffLength = RVW_IDX;
        } else {
            cutoffLength = (RVW_IDX > SUBJ_IDX) ? RVW_IDX : SUBJ_IDX;
        }
        ExecutorService executor = Executors.newFixedThreadPool(NUM_THREADS);
        for (Integer i = 1; i < size; i++) {
            if (i >= THRESHOLD_REVIEW_COUNT) {
                break;
            }

            String line = reviewList.pop();
            String[] strArr = line.split(FIELD_DELIMITER);

            if (strArr.length <= cutoffLength) {
                continue;
            }
            String reviewID;
            if (ID_IDX == null) {
                reviewID = i.toString();
            } else {
                reviewID = strArr[ID_IDX];
            }
            String rvw = strArr[RVW_IDX];

            String subj = null;
            if (SUBJ_IDX == null) {
                subj = CUSTOMER_NAME;
            } else {
                subj = strArr[SUBJ_IDX];
            }
            String date = null;
            if (DATE_IDX != null) {
                date = strArr[DATE_IDX];
            }
            if (rvw.length() == 0) {
                continue;
            }
            Runnable worker = new KeywordRelevanceThread(language, reviewID, ANNOTATION, DOMAIN_NAME, subj, rvw, date, IS_HISTORICAL_DEMO_MODE, SOURCE);
            executor.execute(worker);

        }
        ThreadUtils.shutdownAndAwaitTermination(executor);
        System.out.println("Number of reviews processed: " + reviewsProcessed);
    }

    public static void entityIdfScoreComputationBatch() {
        //		Map<String, Integer> entityDocMap = domainEntityDocCountMap.get(DOMAIN_NAME);
        //		Integer totalDocCount = getEntityDocCount(GENERIC_DOC_STR, DOMAIN_NAME);
        //
        //		if (totalDocCount != null) {
        //			ExecutorService executor = Executors.newFixedThreadPool(NUM_THREADS);
        //			for (String entity : entityDocMap.keySet()) {
        //				Runnable worker = new TfIdfComputationThread(entity, totalDocCount.intValue(), DOMAIN_NAME);
        //				executor.execute(worker);
        //			}
        //			ThreadUtils.shutdownAndAwaitTermination(executor);
        //			updateMeanIdfScore(DOMAIN_NAME, KeywordRelevanceComputation.getMeanIdfScore(DOMAIN_NAME).doubleValue() / totalDocCount.intValue());
        //		}

        Map<String, EntityInfoClass> entityInfoMap = domainEntityInfoMap.get(DOMAIN_NAME);
        int totalDocCount = entityInfoMap.get(GENERIC_DOC_STR.toLowerCase()).getDocCount();
        //		System.out.println("total document count : " + totalDocCount);
        if (totalDocCount != 0) {
            ExecutorService executor = Executors.newFixedThreadPool(NUM_THREADS);
            for (Entry<String, EntityInfoClass> entityInfo : entityInfoMap.entrySet()) {
                if (entityInfo.getKey().equalsIgnoreCase(GENERIC_DOC_STR))
                    continue;
                //				System.out.println("I have entered here!!!");
                Runnable worker = new TfIdfComputationThread(entityInfo.getKey(), entityInfo.getValue(), totalDocCount, DOMAIN_NAME);
                executor.execute(worker);
            }
            ThreadUtils.shutdownAndAwaitTermination(executor);
            //			System.out.println("mean Idf Score : " + KeywordRelevanceComputation.getMeanIdfScore(DOMAIN_NAME));
            int totalEntityCount = entityInfoMap.keySet().size();
            if (totalEntityCount != 0) {
                System.out.println("total entity count - " + totalEntityCount);
                System.out.println("idf score - " + KeywordRelevanceComputation.getMeanIdfScore(DOMAIN_NAME).doubleValue());
                //				updateMeanIdfScore(DOMAIN_NAME, KeywordRelevanceComputation.getMeanIdfScore(DOMAIN_NAME).doubleValue() / totalEntityCount);
                setDomainMeanIdfScore(DOMAIN_NAME, KeywordRelevanceComputation.getMeanIdfScore(DOMAIN_NAME).doubleValue() / totalEntityCount);
            }
        }

    }

    public static void entitySignificanceComputationBatch(String outputFile) {
        Map<String, EntityInfoClass> entityInfoMap = domainEntityInfoMap.get(DOMAIN_NAME);
        Double meanIdf = domainMeanIdfScoreMap.get(DOMAIN_NAME);
        if (meanIdf != null) {
            ExecutorService executor = Executors.newFixedThreadPool(NUM_THREADS);
            for (Entry<String, EntityInfoClass> entityInfo : entityInfoMap.entrySet()) {
                if (entityInfo.getKey().equalsIgnoreCase(GENERIC_DOC_STR))
                    continue;
                Runnable worker = new EntitySignificanceThread(entityInfo.getValue(), meanIdf.doubleValue());
                executor.execute(worker);
            }
            ThreadUtils.shutdownAndAwaitTermination(executor);
        }
        KeywordRelevanceComputation.writeEntityCountToFile(outputFile);
    }

    public static void trendEntitySignificanceController(String outputFile) {
        entityRelevanceBatch();
        entityIdfScoreComputationBatch();
        entitySignificanceComputationBatch(outputFile);
    }

    //	Adding a variable for language assuming work in a single language is performed with this class
    //	private static final Languages language = Languages.EN;

    public static void main(String[] args) throws IOException {

        double startTime = System.currentTimeMillis();
        try {
            System.out.println("KeyWordRelevanceActivator Started");

            XpEngine.init(true);
            System.out.println(RVW_FILENAME);
            String outputFileName = XpConfig.ENTITY_DOC_COUNT_FILE;

            startTime = System.currentTimeMillis();
            KeywordRelevanceComputation.trendEntitySignificanceController(outputFileName);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            PmiCalculator.exit();
        }
        System.out.println("Net time taken: " + ((System.currentTimeMillis() - startTime)) / (1000 * 60) + " minutes");
        System.exit(1);
    }

}