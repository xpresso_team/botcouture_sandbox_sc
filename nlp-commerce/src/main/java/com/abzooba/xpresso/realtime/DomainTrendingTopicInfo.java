package com.abzooba.xpresso.realtime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.utils.FileIO;

public class DomainTrendingTopicInfo {

    private Map<String, TrendingTopicInfo> topicInfoMap = new ConcurrentHashMap<String, TrendingTopicInfo>();
    private String domain;

    public DomainTrendingTopicInfo(String entity, int count, double score, String domain) {
        TrendingTopicInfo topicInfo = new TrendingTopicInfo(count, score);
        topicInfoMap.put(entity, topicInfo);
        this.domain = domain;
    }

    public void addTrendTopic(String entity, int count, double score) {
        TrendingTopicInfo topicInfo = new TrendingTopicInfo(count, score);
        topicInfoMap.put(entity, topicInfo);
    }

    public void updateTrendInfo(String topic, double value, boolean deleteTrendTopic) {
        if (topic == null || topic.equalsIgnoreCase("Overall"))
            return;
        boolean isTopicPresent = topicInfoMap.containsKey(topic.toLowerCase());
        Map<String, TrendingTopicInfo> tempTopicTrendMap = new HashMap<String, TrendingTopicInfo>();
        tempTopicTrendMap.putAll(topicInfoMap);
        tempTopicTrendMap.forEach((k, v) -> {
            double tempScore = v.getScore();
            String topicStr = k;
            tempScore *= (1 - XpConfig.DECAY_FACTOR);

            if (tempScore < XpConfig.TREND_THRESHOLD && deleteTrendTopic && !topicStr.equalsIgnoreCase(topic)) {
                topicInfoMap.remove(topicStr);
            } else {
                TrendingTopicInfo currentTopicInfo = v;
                if (topicStr.equalsIgnoreCase(topic)) {
                    currentTopicInfo.addCount(1);
                    if (XpConfig.IDF_DOWNGRADE) {
                        double entityAbundance = KeywordRelevanceComputation.computeEntityAbundance(topic, domain);
                        tempScore += value / entityAbundance;
                    } else
                        tempScore += value;
                }
                currentTopicInfo.setScore(tempScore);
                topicInfoMap.put(topicStr, currentTopicInfo);
            }
        });
        if (!isTopicPresent) {
            if (XpConfig.IDF_DOWNGRADE) {
                double entityAbundance = KeywordRelevanceComputation.computeEntityAbundance(topic, domain);
                TrendingTopicInfo topicInfo = new TrendingTopicInfo(domain, value / entityAbundance, 1);
                topicInfoMap.put(topic.toLowerCase(), topicInfo);
            } else {
                TrendingTopicInfo topicInfo = new TrendingTopicInfo(domain, value, 1);
                topicInfoMap.put(topic.toLowerCase(), topicInfo);
            }
        }
    }

    public JSONObject prepareTrendJSON() {
        Map<String, Double> topicScoreMap = TrendInfoManipulation.getTrendTopicList(domain, topicInfoMap);
        JSONObject trendJson = new JSONObject();
        JSONArray trendArray = new JSONArray();
        //		System.out.println("TopicScoreMap: " + topicScoreMap);
        //		System.out.println("TopicInfoMap: " + topicInfoMap);

        if (topicScoreMap != null) {
            for (String k : topicScoreMap.keySet()) {
                if (k != null && topicInfoMap.containsKey(k) && topicInfoMap.get(k).getCount() >= XpConfig.MIN_TREND_TOPIC_COUNT) {
                    JSONObject topicObj = new JSONObject();
                    try {
                        topicObj.put("topic", k);
                        topicObj.put("score", topicScoreMap.get(k));
                        trendArray.put(topicObj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            //			topicScoreMap.forEach((k, v) -> {
            //				if (topicInfoMap.get(k).getCount() >= XpConfig.MIN_TREND_TOPIC_COUNT) {
            //					JSONObject topicObj = new JSONObject();
            //					try {
            //						topicObj.put("topic", k);
            //						topicObj.put("score", v);
            //						trendArray.put(topicObj);
            //					} catch (JSONException e) {
            //						e.printStackTrace();
            //					}
            //				}
            //			});
        }

        try {
            trendJson.put("domain", domain);
            trendJson.put("trending topics", trendArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return trendJson;
    }

    public String prepareQueryForTrendTopicUpdate(String tableName) {
        Map<String, Double> trendTopicMap = TrendInfoManipulation.getTrendTopicList(domain, topicInfoMap);
        StringBuilder qBuilder = new StringBuilder("INSERT INTO " + tableName + "(topic,category,count,score,domain) VALUES ");
        int i = 0;
        if (trendTopicMap.size() > 1) {
            for (Entry<String, Double> topicPair : trendTopicMap.entrySet()) {

                if (i != 0) {
                    qBuilder.append(",");
                }
                qBuilder.append("(");
                String topic = topicPair.getKey();
                //				topic = topic.replaceAll("\\W+", "");
                qBuilder.append("'").append(topic).append("'");
                qBuilder.append(",NULL,");
                qBuilder.append(TrendInfoManipulation.round(topicInfoMap.get(topic).getCount(), 2));
                qBuilder.append(",");
                qBuilder.append(TrendInfoManipulation.round(topicPair.getValue().doubleValue(), 2));
                qBuilder.append(",");
                qBuilder.append("'").append(domain).append("'");
                qBuilder.append(") ");
                i++;
            }
            qBuilder.append(";");
            System.out.println(qBuilder.toString());
            return qBuilder.toString();
        }
        return null;
    }

    public void outputTrendInfo(String outputFile) {
        List<String> outputList = new ArrayList<String>();
        Map<String, TrendingTopicInfo> sortedTrendMap = TrendInfoManipulation.sortTrendingTopicByScores(domain, topicInfoMap);
        for (Entry<String, TrendingTopicInfo> entry : sortedTrendMap.entrySet()) {
            if (entry.getValue().getCount() < XpConfig.MIN_TREND_TOPIC_COUNT)
                continue;

            //			System.out.println("entry added");
            String line = entry.getKey() + "\t" + entry.getValue().getScore() + "\t" + entry.getValue().getCount() + "\t" + domain;
            outputList.add(line);
        }
        //		System.out.println("in outputTrendInfo");
        FileIO.write_file(outputList, outputFile, false);
        //		FileIO.write_file(outputList.toString(), outputFile, true);
    }

}