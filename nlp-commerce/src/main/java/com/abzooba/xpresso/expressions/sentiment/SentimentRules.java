/**
 *
 */
package  com.abzooba.xpresso.expressions.sentiment;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.machinelearning.XpFeatureVector;
import org.slf4j.Logger;


/**
 * @author Koustuv Saha
 * 08-Aug-2014 9:54:19 am
 * XpressoV2.0.1  SentimentClass
 */
public class SentimentRules {

    //
    //	private static final Logger logger = (Logger) LoggerFactory.getLogger(SentimentRules.class);
    //	private static final Pattern too_rule = Pattern.compile("too\\s(.*?)\\s");
    //	private static final Pattern shft_correct_pat = Pattern.compile("(SHFT\\s(null\\s?){3,})");

    //	private static final Pattern intsf_rule = Pattern.compile("INTSF\\s(.*?)\\s");

    protected static final Logger logger = XCLogger.getSpLogger();

    protected static final Pattern INTSF_RULE = Pattern.compile(XpSentiment.INTENSIFIER_TAG + "\\s(.*?)\\s");

    protected static final String CONSUME_RSRC_REGEX = XpSentiment.CONSUME_TAG + "\\s(.*?)\\s" + XpSentiment.RESOURCES_TAG;
    protected static final Pattern CONSUME_RSRC_RULE = Pattern.compile(CONSUME_RSRC_REGEX);
    protected static final String CONSUME_NORSRC_REGEX = XpSentiment.CONSUME_TAG + "\\s(.*?)\\s(?!" + XpSentiment.RESOURCES_TAG + ")";
    protected static final Pattern CONSUME_NORSRC_RULE = Pattern.compile(CONSUME_NORSRC_REGEX);

    protected static final Pattern INCR_PRE_RULE = Pattern.compile(XpSentiment.INCREASER_TAG + "\\s(.*?)\\s");
    protected static final Pattern DECR_PRE_RULE = Pattern.compile(XpSentiment.DECREASER_TAG + "\\s(.*?)\\s");

    // static final Pattern shft_rule = Pattern.compile("SHFT\\s(.*?)\\s");
    protected static final Pattern SHFT_RULE = Pattern.compile(XpSentiment.SHIFTER_TAG + "\\s((.*?)\\s){1,2}?");
    protected static final String TEST_SHFT_POST_RULE = "EN_POST_RULE";
    protected static final Pattern SHFT_POST_RULE = Pattern.compile("((.*?)\\s)" + XpSentiment.SHIFTER_TAG);

    protected static final Pattern INCR_POST_RULE = Pattern.compile("\\s?(\\b[^\\s]+?)\\s" + XpSentiment.INCREASER_TAG + "\\s");
    protected static final Pattern DECR_POST_RULE = Pattern.compile("\\s?(\\b[^\\s]+?)\\s" + XpSentiment.DECREASER_TAG + "\\s");

    protected String sentiTaggedStr;
    protected XpFeatureVector xpf;
    protected List<String> tokenList;

    public SentimentRules(String sentiTaggedStr, List<String> tokenList, XpFeatureVector xpf) {
        this.sentiTaggedStr = sentiTaggedStr;
        this.tokenList = tokenList;
        this.xpf = xpf;

    }

    protected boolean intensifierRule() {

        Matcher m1 = INTSF_RULE.matcher(sentiTaggedStr);
        boolean ret = false;
        while (m1.find()) {
            xpf.setIntsfFeature(true);
            ret = true;
            String focus_tag = m1.group(1).trim();
            switch (focus_tag) {
                case (XpSentiment.NPI_TAG):
                    xpf.setIntsfNPIFeature(true);
                    break;
                case (XpSentiment.PPI_TAG):
                    xpf.setIntsfPPIFeature(true);
                    break;
                case (XpSentiment.POSITIVE_TAG):
                    xpf.setIntsfPosFeature(true);
                    xpf.setAdvocatePresentFeature(true);
                    break;
                case (XpSentiment.NEGATIVE_TAG):
                    xpf.setIntsfNegFeature(true);
                    xpf.setAbusePresentFeature(true);
                    break;
                case (XpSentiment.UNCONDITIONAL_POSITIVE_TAG):
                    xpf.setIntsfUcPosFeature(true);
                    xpf.setAdvocatePresentFeature(true);
                    break;
                case (XpSentiment.UNCONDITIONAL_NEGATIVE_TAG):
                    xpf.setIntsfUcNegFeature(true);
                    xpf.setAbusePresentFeature(true);
                    break;
            }
            //			sentiTaggedStr = sentiTaggedStr.replaceAll("INTSF\\s(.*?)\\s", "$1 ");
            sentiTaggedStr = sentiTaggedStr.replaceAll(XpSentiment.INTENSIFIER_TAG + "\\s(.*?)\\s", "$1 ");
        }
        return ret;

    }

    protected boolean consumeResourcesRule() {
        Matcher m = CONSUME_RSRC_RULE.matcher(sentiTaggedStr);
        Matcher mBar = CONSUME_NORSRC_RULE.matcher(sentiTaggedStr);
        boolean ret = false;
        while (m.find()) {
            String focusTag = m.group(1).trim();
            switch (focusTag) {
                case (XpSentiment.INCREASER_TAG):
                    xpf.setiConsumeIncrRsrcFeature(true);
                    sentiTaggedStr = sentiTaggedStr.replaceAll(CONSUME_RSRC_REGEX, XpSentiment.NEGATIVE_TAG);
                    ret = true;
                    break;
                case (XpSentiment.INTENSIFIER_TAG):
                    xpf.setiConsumeIncrRsrcFeature(true);
                    sentiTaggedStr = sentiTaggedStr.replaceAll(CONSUME_RSRC_REGEX, XpSentiment.NEGATIVE_TAG);
                    ret = true;
                    break;
                case (XpSentiment.DECREASER_TAG):
                    xpf.setConsumeDecrRsrcFeature(true);
                    sentiTaggedStr = sentiTaggedStr.replaceAll(CONSUME_RSRC_REGEX, XpSentiment.POSITIVE_TAG);
                    ret = true;
                    break;
            }
        }
        if (!ret) {
            while (mBar.find()) {
                sentiTaggedStr = sentiTaggedStr.replaceAll(CONSUME_NORSRC_REGEX, mBar.group(1).trim() + " ");
            }
        }
        return ret;
    }

    protected boolean increaserPreRule() {

        //		sentiTaggedStr = sentiTaggedStr.replaceAll("INCR\\sPOS", SentimentLexicon.POSITIVE_TAG).replaceAll("INCR\\sNEG", SentimentLexicon.NEGATIVE_TAG);
        sentiTaggedStr = sentiTaggedStr.replaceAll(XpSentiment.INCREASER_TAG + "\\s" + XpSentiment.POSITIVE_TAG, XpSentiment.POSITIVE_TAG).replaceAll(XpSentiment.INCREASER_TAG + "\\s" + XpSentiment.NEGATIVE_TAG, XpSentiment.NEGATIVE_TAG);
        boolean ret = false;
        Matcher m2 = INCR_PRE_RULE.matcher(sentiTaggedStr);
        while (m2.find()) {
            ret = true;
            //			xpf.setPreIncrFeature(true);

            String focusTag = m2.group(1);

            switch (focusTag) {
                case (XpSentiment.PPI_TAG):
                    sentiTaggedStr = sentiTaggedStr.replaceAll("INCR\\s(.*?)\\s", "POS ");
                    xpf.setPreIncrPPIFeature(true);
                    break;
                case (XpSentiment.NPI_TAG):
                    sentiTaggedStr = sentiTaggedStr.replaceAll("INCR\\s(.*?)\\s", "NEG ");
                    xpf.setPreIncrNPIFeature(true);
                    break;
                case (XpSentiment.POSITIVE_TAG):
                    sentiTaggedStr = sentiTaggedStr.replaceAll("INCR\\s(.*?)\\s", "POS ");
                    xpf.setPreIncrPosFeature(true);
                    xpf.setIncrPosFeature(true);
                    break;
                case (XpSentiment.NEGATIVE_TAG):
                    sentiTaggedStr = sentiTaggedStr.replaceAll("INCR\\s(.*?)\\s", "NEG ");
                    xpf.setPreIncrNegFeature(true);
                    xpf.setIncrNegFeature(true);
                    break;
                case (XpSentiment.UNCONDITIONAL_POSITIVE_TAG):
                    sentiTaggedStr = sentiTaggedStr.replaceAll("INCR\\s(.*?)\\s", "UCPOS ");
                    xpf.setPreIncrUcPosFeature(true);
                    break;
                case (XpSentiment.UNCONDITIONAL_NEGATIVE_TAG):
                    sentiTaggedStr = sentiTaggedStr.replaceAll("INCR\\s(.*?)\\s", "UCNEG ");
                    xpf.setPreIncrUcNegFeature(true);
                    break;

            }
        }
        return ret;

    }

    protected boolean decreaserPreRule() {
        boolean ret = false;
        Matcher m3 = DECR_PRE_RULE.matcher(sentiTaggedStr);
        while (m3.find()) {
            ret = true;
            //			xpf.setPreDecrFeature(true);

            String focusTag = m3.group(1);

            switch (focusTag) {
                case (XpSentiment.PPI_TAG):
                    sentiTaggedStr = sentiTaggedStr.replaceAll("DECR\\s(.*?)\\s", "NEG ");
                    xpf.setPreDecrPPIFeature(true);
                    break;
                case (XpSentiment.NPI_TAG):
                    sentiTaggedStr = sentiTaggedStr.replaceAll("DECR\\s(.*?)\\s", "POS ");
                    xpf.setPreDecrNPIFeature(true);
                    break;
                case (XpSentiment.POSITIVE_TAG):
                    xpf.setPreDecrPosFeature(true);
                    break;
                case (XpSentiment.NEGATIVE_TAG):
                    xpf.setPreDecrNegFeature(true);
                    break;
                case (XpSentiment.UNCONDITIONAL_POSITIVE_TAG):
                    xpf.setPreDecrUcPosFeature(true);
                    break;
                case (XpSentiment.UNCONDITIONAL_NEGATIVE_TAG):
                    xpf.setPreDecrUcNegFeature(true);
                    break;
            }
        }
        return ret;

    }

    protected boolean increaserPostRule() {
        boolean ret = false;
        Matcher m5 = INCR_POST_RULE.matcher(sentiTaggedStr);
        while (m5.find()) {
            ret = true;
            //			xpf.setPostIncrFeature(true);

            String focusTag = m5.group(1);

            switch (focusTag) {
                case (XpSentiment.PPI_TAG):
                    sentiTaggedStr = sentiTaggedStr.replaceAll(focusTag + "\\sINCR\\s", "POS ");
                    xpf.setPostIncrPPIFeature(true);
                    break;
                case (XpSentiment.NPI_TAG):
                    sentiTaggedStr = sentiTaggedStr.replaceAll(focusTag + "\\sINCR\\s", "NEG ");
                    xpf.setPostIncrNPIFeature(true);
                    break;
                case (XpSentiment.POSITIVE_TAG):
                    sentiTaggedStr = sentiTaggedStr.replaceAll(focusTag + "\\sINCR\\s", "POS ");
                    xpf.setPostIncrPosFeature(true);
                    break;
                case (XpSentiment.NEGATIVE_TAG):
                    sentiTaggedStr = sentiTaggedStr.replaceAll(focusTag + "\\sINCR\\s", "NEG ");
                    xpf.setPostIncrNegFeature(true);
                    break;
                case (XpSentiment.UNCONDITIONAL_POSITIVE_TAG):
                    sentiTaggedStr = sentiTaggedStr.replaceAll(focusTag + "\\sINCR\\s", "UCPOS ");
                    xpf.setPostIncrUcPosFeature(true);
                    break;
                case (XpSentiment.UNCONDITIONAL_NEGATIVE_TAG):
                    sentiTaggedStr = sentiTaggedStr.replaceAll(focusTag + "\\sINCR\\s", "UCNEG ");
                    xpf.setPostIncrUcNegFeature(true);
                    break;
            }
        }
        return ret;

    }

    protected boolean decreaserPostRule() {
        boolean ret = false;
        Matcher m6 = DECR_POST_RULE.matcher(sentiTaggedStr);
        while (m6.find()) {
            //			xpf.setPostDecrFeature(true);
            ret = true;
            String focusTag = m6.group(1);

            switch (focusTag) {
                case (XpSentiment.PPI_TAG):
                    sentiTaggedStr = sentiTaggedStr.replaceAll(focusTag + "\\sDECR\\s", "NEG ");
                    xpf.setPostDecrPPIFeature(true);
                    break;
                case (XpSentiment.NPI_TAG):
                    sentiTaggedStr = sentiTaggedStr.replaceAll(focusTag + "\\sDECR\\s", "POS ");
                    xpf.setPostDecrNPIFeature(true);
                    break;
                case (XpSentiment.POSITIVE_TAG):
                    xpf.setPostDecrPosFeature(true);
                    break;
                case (XpSentiment.NEGATIVE_TAG):
                    xpf.setPostDecrNegFeature(true);
                    break;
                case (XpSentiment.UNCONDITIONAL_POSITIVE_TAG):
                    xpf.setPostDecrUcPosFeature(true);
                    break;
                case (XpSentiment.UNCONDITIONAL_NEGATIVE_TAG):
                    xpf.setPostDecrUcNegFeature(true);
                    break;
            }
        }
        return ret;
    }

    protected boolean isNeg(String focus_tag) {
        boolean ret = focus_tag.equals(XpSentiment.NEGATIVE_TAG);
        ret = ret || focus_tag.equals(XpSentiment.UNCONDITIONAL_NEGATIVE_TAG);
        ret = ret || focus_tag.equals(XpSentiment.NPI_TAG);
        return ret;
    }

    protected boolean isPos(String focus_tag) {
        boolean ret = focus_tag.equals(XpSentiment.POSITIVE_TAG);
        ret = ret || focus_tag.equals(XpSentiment.UNCONDITIONAL_POSITIVE_TAG);
        ret = ret || focus_tag.equals(XpSentiment.PPI_TAG);
        ret = ret || focus_tag.equals(XpSentiment.POSSESSION_TAG);
        return ret;
    }

    protected boolean shifterRule() {
        boolean ret = false;
        Matcher m4 = SHFT_RULE.matcher(sentiTaggedStr);

        while (m4.find()) {
            xpf.setShiftFeature(true);
            ret = true;
            String focus_tag = m4.group(1).trim();
            // System.out.println("Shifter working on " + focus_tag);
            if (isNeg(focus_tag)) {
                sentiTaggedStr = sentiTaggedStr.replaceAll(XpSentiment.SHIFTER_TAG + "\\s" + focus_tag, XpSentiment.POSITIVE_TAG + " ");
                xpf.setShiftNegFeature(true);
            } else if (isPos(focus_tag)) {
                sentiTaggedStr = sentiTaggedStr.replaceAll(XpSentiment.SHIFTER_TAG + "\\s" + focus_tag, XpSentiment.NEGATIVE_TAG + " ");
                xpf.setShiftPosFeature(true);
            }
        }
        return ret;
    }

    protected boolean shifterPostRule() {
        //		System.out.println(TEST_SHFT_POST_RULE);
        Matcher m4 = SHFT_POST_RULE.matcher(sentiTaggedStr);
        boolean ret = false;
        while (m4.find()) {
            //			System.out.println("Bhoot Bhoot Bhoot");
            xpf.setShiftFeature(true);
            ret = true;
            String focus_tag = m4.group(1).trim();
            // System.out.println("Shifter working on " + focus_tag);
            if (isNeg(focus_tag)) {
                sentiTaggedStr = sentiTaggedStr.replaceAll(focus_tag + "\\s" + XpSentiment.SHIFTER_TAG, XpSentiment.POSITIVE_TAG + " ");
                xpf.setShiftNegFeature(true);
            } else if (isPos(focus_tag)) {
                sentiTaggedStr = sentiTaggedStr.replaceAll(focus_tag + "\\s" + XpSentiment.SHIFTER_TAG, XpSentiment.NEGATIVE_TAG + " ");
                xpf.setShiftPosFeature(true);
            }
        }
        return ret;
    }

    protected String runSentimentRules() {
        String sentiment = XpSentiment.NEUTRAL_SENTIMENT;

        logger.info("Initial SentiTagged: " + sentiTaggedStr);
        if (sentiTaggedStr.length() == 0 || sentiTaggedStr.equals(" ")) {
            logger.info("Skipping SentimentRules: " + sentiment);
        } else {
            sentiTaggedStr = sentiTaggedStr.replaceAll("(" + XpSentiment.PPI_TAG + "\\s)+", XpSentiment.PPI_TAG + " ");
            sentiTaggedStr = sentiTaggedStr.replaceAll("(" + XpSentiment.NPI_TAG + "\\s)+", XpSentiment.NPI_TAG + " ");
            logger.info("Remove Redundancy: " + sentiTaggedStr);
            boolean ret = false;
            ret = consumeResourcesRule();
            logger.info("Consume Resources Rule: " + ret + ":\t" + tokenList + "\t" + sentiTaggedStr);

            ret = intensifierRule();
            logger.info("Intensifier Rule: " + ret + ":\t" + tokenList + "\t" + sentiTaggedStr);

            ret = increaserPreRule();
            logger.info("Increaser Pre Rule: " + ret + ":\t" + tokenList + "\t" + sentiTaggedStr);

            ret = decreaserPreRule();
            logger.info("Decreaser Pre Rule: " + ret + ":\t" + tokenList + "\t" + sentiTaggedStr);

            ret = increaserPostRule();
            logger.info("Increaser Post Rule: " + ret + ":\t" + tokenList + "\t" + sentiTaggedStr);

            ret = decreaserPostRule();
            logger.info("Decreaser Post Rule: " + ret + ":\t" + tokenList + "\t" + sentiTaggedStr);

            ret = shifterRule();
            logger.info("Shifter Rule: " + ret + ":\t" + tokenList + "\t" + sentiTaggedStr);

            ret = shifterPostRule();
            logger.info("Shifter Post Rule: " + ret + ":\t" + tokenList + "\t" + sentiTaggedStr);

            if (sentiTaggedStr.contains(XpSentiment.UNCONDITIONAL_NEGATIVE_TAG)) {
                sentiment = XpSentiment.NEGATIVE_SENTIMENT;
                //			sentiment = SentimentLexicon.VERY_NEGATIVE_SENTIMENT;
                xpf.setUnconNegFeature(true);
                xpf.setPresentNegFeature(true);
                xpf.setPercentageNegativeFeature(1, 1, 1);
                xpf.setHigherNegFeature(true);
                xpf.setCountNegFeature(tokenList.size());

            } else if (sentiTaggedStr.contains(XpSentiment.UNCONDITIONAL_POSITIVE_TAG)) {

                sentiment = XpSentiment.POSITIVE_SENTIMENT;
                //			sentiment = SentimentLexicon.VERY_POSITIVE_SENTIMENT;
                xpf.setUnconPosFeature(true);
                xpf.setPresentPosFeature(true);
                xpf.setPercentagePositiveFeature(1, 1, 1);
                xpf.setHigherPosFeature(true);
                xpf.setCountPosFeature(tokenList.size());

            } else {

                if (sentiTaggedStr.split(" ").length < 2 && !sentiTaggedStr.contains("POS ") && !sentiTaggedStr.contains("NEG ")) {

                    if (sentiTaggedStr.contains(XpSentiment.POSSESSION_TAG)) {
                        xpf.setPossnFeature(true);
                    } else if (sentiTaggedStr.contains(XpSentiment.PPI_TAG)) {
                        xpf.setPresentPPIFeature(true);
                    } else if (sentiTaggedStr.contains(XpSentiment.NPI_TAG)) {
                        xpf.setPresentNPIFeature(true);
                    }

                    //				sentiTaggedStr = sentiTaggedStr.replaceAll("PPI ", "POS ").replaceAll("NPI ", "NEG ");
					/*Just a "buy" can't be positive*/
                    if (tokenList.size() > 3) {
                        sentiTaggedStr = sentiTaggedStr.replaceAll(XpSentiment.POSSESSION_TAG + " ", XpSentiment.POSITIVE_TAG + " ");
                    }
                }

				/*Need of space: Because it also includes the POSSN otherwise*/
                if (sentiTaggedStr.contains(XpSentiment.POSITIVE_TAG + " ")) {
                    xpf.setPresentPosFeature(true);
                }
                if (sentiTaggedStr.contains(XpSentiment.NEGATIVE_TAG + " ")) {
                    xpf.setPresentNegFeature(true);
                }

                String[] tagArr = sentiTaggedStr.split(" ");
                int posCount = 0;
                int negCount = 0;

                for (int i = 0; i < tagArr.length; i++) {
                    if (tagArr[i].equals(XpSentiment.POSITIVE_TAG)) {
                        posCount += 1;
                    } else if (tagArr[i].equals(XpSentiment.NEGATIVE_TAG)) {
                        negCount += 1;
                    }
                }
                //			System.out.println("Negative count: " + neg_count);
                //				System.out.println("PosCount: " + posCount + "\tNegCount: " + negCount + "\tSizeCount: " + tokenList.size());

                if (xpf.isNegIdiomPresentFeature() || xpf.isNegPhrasalVerbPresentFeature()) {
                    negCount += 1;
                } else if (xpf.isPosIdiomPresentFeature() || xpf.isPosPhrasalVerbPresentFeature()) {
                    posCount += 1;
                }

                //				System.out.println("PosCount: " + posCount + "\tNegCount: " + negCount + "\tSizeCount: " + tokenList.size());
                xpf.setCountPosFeature(posCount);
                xpf.setCountNegFeature(negCount);
                //			System.out.println("CountPosFeature: " + xpf.getCountPosFeature() + "\tCountNegFeature: " + xpf.getCountNegFeature());

                int totalCount = negCount + posCount;
                xpf.setPercentagePositiveFeature(posCount, totalCount, tokenList.size());
                xpf.setPercentageNegativeFeature(negCount, totalCount, tokenList.size());

                //			System.out.println("PercentagePosFeature: " + xpf.getPercentagePositiveFeature() + "\tPercentageNegFeature: " + xpf.getPercentageNegativeFeature());

                logger.info("PosCount: " + posCount + "\tNegCount: " + negCount);
                if (posCount > negCount) {
                    sentiment = XpSentiment.POSITIVE_SENTIMENT;
                    xpf.setHigherPosFeature(true);
                } else if (negCount > posCount) {
                    sentiment = XpSentiment.NEGATIVE_SENTIMENT;
                    xpf.setHigherNegFeature(true);
                } else if ((negCount == posCount) && negCount != 0) {
                    sentiment = XpSentiment.NEUTRAL_SENTIMENT;
                }

                //			if ((posCount >= 3) && (posCount > 2 * negCount)) {
                //				sentiment = SentimentLexicon.VERY_POSITIVE_SENTIMENT;
                //			} else if ((negCount >= 3) && (negCount > 2 * posCount)) {
                //				sentiment = SentimentLexicon.VERY_NEGATIVE_SENTIMENT;
                //			}

            }
        }
        xpf.setRuleBasedSentimentFeature(sentiment);
        return sentiment;

    }

    public String determineSentiment() {
        String sentiment = this.runSentimentRules();
        return sentiment;
    }

}