/**
 * @author Alix Melchy
 * Jun 12, 2015 11:07:41 AM
 */
package com.abzooba.xpresso.expressions.sentiment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.textanalytics.CoreNLPController;
import com.abzooba.xpresso.textanalytics.LuceneSpell;
import com.abzooba.xpresso.textanalytics.PosTags;
import com.abzooba.xpresso.utils.FileIO;
import org.slf4j.Logger;


public abstract class SentimentLexicon {

    protected static final Logger logger = XCLogger.getSpLogger();

    protected Map<String, SentimentPhraseInfo> basicLexicalMap = new ConcurrentHashMap<String, SentimentPhraseInfo>();
    protected Map<String, SentimentPhraseInfo> lemmatizedLexicalMap = new ConcurrentHashMap<String, SentimentPhraseInfo>();

    protected List<String> abuseList = new ArrayList<String>();
    protected List<String> advocateList = new ArrayList<String>();
    protected List<String> suggestionList = new ArrayList<String>();

    protected Map<String, String> emoticonsMap = new HashMap<String, String>();
    protected Map<String, String> idiomsMap = new HashMap<String, String>();
    //	private static Map<String, String> phrasalVerbsMap = new HashMap<String, String>();
    protected Set<String> greetingsSalutationSet = new HashSet<String>();
    protected List<String> greetingsSalutationRegExList = new ArrayList<String>();

    protected Languages language;

    private void loadLexiconsMap(String fileName, String value, Map<String, SentimentPhraseInfo> basic_kbMap, Map<String, SentimentPhraseInfo> lemma_kbMap) throws IOException {
        List<String> wordList = new ArrayList<String>();
        FileIO.read_file(fileName, wordList);
        //		for (String str : wordList) {

        //			vectorList.stream().forEach((obj) -> {
        //			gloveVector.add((Double) obj);
        //		});
        wordList.parallelStream().forEach((str) -> {
            String[] strArr = str.split("\t");
            String word = null;
            String lemma = null;
            SentimentPhraseInfo phraseObj = new SentimentPhraseInfo();
            phraseObj.setLexTag(value);
            if (strArr.length >= 2) {
                word = strArr[0].toLowerCase().trim();
                lemma = strArr[1].toLowerCase().trim();
                CoreNLPController.addToLemmaMap(language, word, lemma);
                if (strArr.length > 2) {
                    for (int i = 2; i < strArr.length; i++) {
                        phraseObj.addToPosTagList(strArr[i]);
                    }
                }
            } else {
                word = str.toLowerCase().trim();
                lemma = CoreNLPController.lemmatizeToString(language, word).trim();
            }
            basic_kbMap.put(word, phraseObj);
            lemma_kbMap.put(lemma, phraseObj);

        });

        addSwitcher(basic_kbMap);
        //		}
    }

    protected abstract void addSwitcher(Map<String, SentimentPhraseInfo> basic_kbMap);

    protected void loadTSVtoMap(String fileName, Map<String, String> lexMap) {

        try {
            //			BufferedReader br = new BufferedReader(new FileReader(fileName));
            BufferedReader br = new BufferedReader(new InputStreamReader(FileIO.getInputStreamFromFileName(fileName)));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                String[] strArr = strLine.split("\t");
                if (strArr.length == 2) {
                    String val = SentimentFunctions.getTagFromSentiment(strArr[1]);
                    if (val != null) {
                        lexMap.put(strArr[0], val);
                        //						if (strArr[0].equals("on time")) {
                        //						}
                    }
                }
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //	protected abstract String getLexTag(String str, Map<String, String> wordPOSmap);

    //	protected abstract boolean isNP(String tag);

    //	public String getLexTag(String str, Map<String, String> wordPOSmap, Map<String, String> tagSentimentMap) {
    //		str = str.toLowerCase();
    //		/* Should fire even if it exists in the map. eg: floor staff */
    //		String returnTag = null;
    //
    //		if (wordPOSmap != null && wordPOSmap.containsKey(str) && isNP(wordPOSmap.get(str))) {
    //			returnTag = null;
    //		} else if (tagSentimentMap != null && tagSentimentMap.containsKey(str)) {
    //			String tag = tagSentimentMap.get(str);
    //			if (tag != null) {
    //				returnTag = tagSentimentMap.get(str);
    //				//				return tagSentimentMap.get(str);
    //			} else {
    //				returnTag = null;
    //				//				return null;
    //			}
    //		} else {
    //			returnTag = getLexTag(str, wordPOSmap);
    //			//			return getLexTag(str);
    //		}
    //		//		System.out.println("SentimentLexicon: Returning\t" + str + "\t" + returnTag);
    //		return returnTag;
    //	}

    public List<String> getAbuseList() {
        return abuseList;
    }

    public List<String> getAdvocateList() {
        return advocateList;
    }

    public List<String> getSuggestionList() {
        return suggestionList;
    }

    public Set<String> getGreetSalutationsList() {
        return greetingsSalutationSet;
    }

    public List<String> getGreetSalutationsRegexList() {
        return greetingsSalutationRegExList;
    }

    // public  List<String> getPositiveEmoticonsList() {
    // synchronized (positiveEmoticonsList) {
    // return positiveEmoticonsList;
    // }
    // }
    //
    // public  List<String> getNegativeEmoticonsList() {
    // synchronized (negativeEmoticonsList) {
    // return negativeEmoticonsList;
    // }
    // }

    public Map<String, String> getEmoticonsMap() {
        return emoticonsMap;
    }

    public Map<String, String> getIdiomsMap() {
        return idiomsMap;
    }

    //	public  Map<String, String> getPhrasalVerbMap() {
    //		return phrasalVerbsMap;
    //	}

    protected void regularExpressionalize(Collection<String> initialCollection, Collection<String> finalCollection) {

        for (String sequence : initialCollection) {

            String regexSeq = sequence.replaceAll("\\*", "\\\\b\\.\\*\\\\b");
            regexSeq = ".*\\b" + regexSeq + "\\b.*";

            finalCollection.add(regexSeq.toLowerCase());
        }
    }

    public void init() throws IOException {

        // for (String line : tempIdiomsList) {
        // String[] strArr = line.split("\t");
        // if (strArr.length == 2) {
        // idiomsMap.put(strArr[0], strArr[1]);
        // }
        // }

        List<String> tempAbuseList = new ArrayList<String>();
        List<String> tempAdvocateList = new ArrayList<String>();
        List<String> tempSuggestionList = new ArrayList<String>();

        loadLexiconsMap(XpConfig.getPossessionWords(language), XpSentiment.POSSESSION_TAG, basicLexicalMap, lemmatizedLexicalMap);
        loadLexiconsMap(XpConfig.getNonPossessionWords(language), XpSentiment.NON_POSSESSION_TAG, basicLexicalMap, lemmatizedLexicalMap);
        loadLexiconsMap(XpConfig.getConsumeWords(language), XpSentiment.CONSUME_TAG, basicLexicalMap, lemmatizedLexicalMap);
        loadLexiconsMap(XpConfig.getResourcesWords(language), XpSentiment.RESOURCES_TAG, basicLexicalMap, lemmatizedLexicalMap);
        loadLexiconsMap(XpConfig.getDecreaseWords(language), XpSentiment.DECREASER_TAG, basicLexicalMap, lemmatizedLexicalMap);
        loadLexiconsMap(XpConfig.getIncreaseWords(language), XpSentiment.INCREASER_TAG, basicLexicalMap, lemmatizedLexicalMap);
        loadLexiconsMap(XpConfig.getIntensifierWords(language), XpSentiment.INTENSIFIER_TAG, basicLexicalMap, lemmatizedLexicalMap);
        loadLexiconsMap(XpConfig.getNPIWords(language), XpSentiment.NPI_TAG, basicLexicalMap, lemmatizedLexicalMap);
        loadLexiconsMap(XpConfig.getPPIWords(language), XpSentiment.PPI_TAG, basicLexicalMap, lemmatizedLexicalMap);
        loadLexiconsMap(XpConfig.getShifterWords(language), XpSentiment.SHIFTER_TAG, basicLexicalMap, lemmatizedLexicalMap);
        loadLexiconsMap(XpConfig.getNegativeWords(language), XpSentiment.NEGATIVE_TAG, basicLexicalMap, lemmatizedLexicalMap);
        loadLexiconsMap(XpConfig.getPositiveWords(language), XpSentiment.POSITIVE_TAG, basicLexicalMap, lemmatizedLexicalMap);
        loadLexiconsMap(XpConfig.getUCNegativeWords(language), XpSentiment.UNCONDITIONAL_NEGATIVE_TAG, basicLexicalMap, lemmatizedLexicalMap);
        loadLexiconsMap(XpConfig.getUCPositiveWords(language), XpSentiment.UNCONDITIONAL_POSITIVE_TAG, basicLexicalMap, lemmatizedLexicalMap);
        loadLexiconsMap(XpConfig.getNeedWords(language), XpSentiment.NEED_TAG, basicLexicalMap, lemmatizedLexicalMap);
        loadLexiconsMap(XpConfig.getNonSentiWords(language), XpSentiment.NON_SENTI_TAG, basicLexicalMap, lemmatizedLexicalMap);

        //		addSwitcher(basicLexicalMap);

        FileIO.read_file(XpConfig.getGreetingsSalutations(language), greetingsSalutationSet, false, true);
        regularExpressionalize(greetingsSalutationSet, greetingsSalutationRegExList);

        //		FileIO.read_file(XpConfigSP.NON_SENTI_WORDS, nonSentiExpressions, true);

        FileIO.read_file(XpConfig.getAbuseWords(language), tempAbuseList);
        regularExpressionalize(tempAbuseList, abuseList);

        FileIO.read_file(XpConfig.getAdvocateWords(language), tempAdvocateList);
        regularExpressionalize(tempAdvocateList, advocateList);

        FileIO.read_file(XpConfig.getSuggestionWords(language), tempSuggestionList);
        regularExpressionalize(tempSuggestionList, suggestionList);

        //		loadIntentLexicons();

        //		System.out.println("Read emoticons file");
        loadTSVtoMap(XpConfig.EMOTICONS_FILE, emoticonsMap);
        loadTSVtoMap(XpConfig.getIdioms(language), idiomsMap);

        printLoaded();
    }

	/*private void loadIntentLexicons() {
	
		List<String> tempIntentActionList = new ArrayList<String>();
		FileIO.read_file(XpConfig.getIntentActionWords(language), tempIntentActionList);
	
		for (String line : tempIntentActionList) {
			String[] intentActionTuple = line.split("\t");
	
			//			if (intentActionTuple.length >= 3) {
			IntentPhraseInfo ip = new IntentPhraseInfo(intentActionTuple);
	
			intentActionWordsMap.put(intentActionTuple[0], ip);
			intentActionWordsMap.put(intentActionTuple[1], ip);
			//		}
		}
	
		List<String> tempIntentObjectList = new ArrayList<String>();
		FileIO.read_file(XpConfig.getIntentObjectWords(language), tempIntentObjectList);
	
		for (String line : tempIntentObjectList)
	
		{
			String[] intentObjectTuple = line.split("\t");
			//			if (intentObjectTuple.length >= 3) {
			IntentPhraseInfo ip = new IntentPhraseInfo(intentObjectTuple);
	
			intentObjectWordsMap.put(intentObjectTuple[0], ip);
			intentObjectWordsMap.put(intentObjectTuple[1], ip);
			//			}
	
		}
	
	}*/

    private String retrieveTag(Map<String, SentimentPhraseInfo> lexicalMap, String str, String pos) {
        String tag = null;
        //		logger.info("lexicalMap : " + lexicalMap);
        SentimentPhraseInfo sentiInfo = lexicalMap.get(str);
        if (sentiInfo != null && sentiInfo.ifValidPosTag(language, pos)) {
            tag = sentiInfo.getLexTag();
        }
        return tag;
    }

    // protected abstract String getLexTag(String str, Map<String, String> wordPOSmap);
    protected String getLexTag(String str, Map<String, String> wordPOSmap) {
        if (greetingsSalutationSet.contains(str)) {
            return XpSentiment.NON_SENTI_TAG;
        }

        String posTag = (wordPOSmap == null) ? null : wordPOSmap.get(str);
        // double tic = System.currentTimeMillis();
        String sentiTag = lookUpString(str, posTag);
        // logger.info("lookUpString '" + str + "' in " + (System.currentTimeMillis() - tic) + " ms");
        //		if (sentiTag == null) {
        //			sentiTag = idiomsMap.get(str);
        //		}
        if (sentiTag == null) {

            if (!str.contains(" ")) {
                //				if (wordPOSmap == null || (wordPOSmap != null && !("NNP").equals(wordPOSmap.get(str)))) {
                if (!PosTags.isProperNoun(language, posTag)) {
                    // tic = System.currentTimeMillis();
                    String spellSuggestedStr = LuceneSpell.correctSpellingWord(language, str);
                    if (spellSuggestedStr != null) {
                        sentiTag = lookUpString(spellSuggestedStr, posTag);
                    }
                    // logger.info("spellSuggestedStr '" + spellSuggestedStr + "' for '" + str + "'  obtained in " + (System.currentTimeMillis() - tic) + " ms");
                }
            }
            //			System.out.println("Checking with Idioms: " + str);
            if (sentiTag == null) {
                //				System.out.println("Checking with Idioms: " + str);
                //				System.out.println("IdiomsMap Size: " + idiomsMap.size());
                sentiTag = idiomsMap.get(str);
                if (sentiTag == null) {
                    sentiTag = idiomsMap.get(CoreNLPController.lemmatizeToString(language, str));
                }
                if (sentiTag != null) {
                    //					System.out.println("Found found found!!");
                    logger.info("Sentiment Tag from Idioms: " + str + "\t" + sentiTag);
                }
            }

        }
        //		System.out.println("SentimentLexicon: Returning\t" + str + "\t" + sentiTag);
        return sentiTag;
    }

    protected String lookUpString(String str, String pos) {

        String tag = retrieveTag(basicLexicalMap, str, pos);
        //		System.out.println("Str: " + str + "\tTag: " + tag);

        if (tag == null) {
            String[] strArr = str.split(" ");
            StringBuilder lemma = new StringBuilder();
            for (String word : strArr) {
                // double tic = System.currentTimeMillis();
                lemma.append(CoreNLPController.lemmatizeToString(language, word)).append(" ");
                // logger.info(word + " lemmatized in " + (System.currentTimeMillis() - tic) + " ms");
            }
            String lemmaStr = lemma.toString().trim();
            if (lemmaStr.length() > 0) {
                tag = retrieveTag(lemmatizedLexicalMap, lemmaStr, pos);
            }
        }
        return tag;
    }

    public String getLexTag(String str, Map<String, String> wordPOSmap, Map<String, String> tagSentimentMap) {

        str = str.toLowerCase();
		/* Should fire even if it exists in the map. eg: floor staff */
        String returnTag = null;

        //		if (wordPOSmap != null && wordPOSmap.containsKey(str) && wordPOSmap.get(str).equals("NNP")) {
        //			returnTag = null;
        //		}
        //	else if (tagSentimentMap != null && tagSentimentMap.containsKey(str)) {
        //		System.out.println("TagSentimentMap: " + tagSentimentMap);
        if (tagSentimentMap != null && tagSentimentMap.containsKey(str)) {
            //			logger.info("Using tagSentimentMap in getLexTag()");
            returnTag = tagSentimentMap.get(str);
            logger.info("Case 1: returnTag for '" + str + " is " + returnTag);
            //			String tag = tagSentimentMap.get(str);
            //			if (tag != null) {
            //				returnTag = tagSentimentMap.get(str);
            //			} else {
            //				returnTag = null;
            //			}
        } else {
            //			logger.info("Calling getLexTag(String, Map<String, String>)");
            returnTag = getLexTag(str, wordPOSmap);
            //  logger.info("Case 2: returnTag obtained for " + str + " is " + returnTag);
            //			return getLexTag(str);
        }

        if (XpSentiment.SHIFTER_TAG.equals(returnTag) && str.contains(" ")) {
            //			logger.info("Shifter Rule in getLexTag()");
            // double tic = System.currentTimeMillis();
            String[] strArr = str.split(" ");
            if (tagSentimentMap != null && tagSentimentMap.containsKey(strArr[1])) {
                //			if (tagSentimentMap != null && strArr.length > 1 && tagSentimentMap.containsKey(strArr[strArr.length - 1])) {
                returnTag = tagSentimentMap.get(strArr[1]);
                //  logger.info("Shifter rule: " + strArr[1] + " determines returnTag: " + returnTag);
            }
            // logger.info("returnTag obtained in " + (System.currentTimeMillis() - tic) + " ms");
        }

        //		System.out.println("TagSentimentMap:\t" + tagSentimentMap);
        return returnTag;
    }

	/*public boolean isIntentActionWord(String str, String domainName, String subject, Map<String, String> wordPOSMap, boolean isAlreadyStrong) {
		str = str.toLowerCase();
	
		IntentPhraseInfo ip = intentActionWordsMap.get(str);
		if (ip == null) {
	
			String pos = null;
			if (wordPOSMap != null) {
				pos = wordPOSMap.get(str);
			}
			If it is Past tense, we are lemmatizing it. eg: I wanted a furby
			if (pos == null || (pos != null && !pos.matches("(VBD)"))) {
				ip = intentActionWordsMap.get(CoreNLPController.lemmatizeToString(language, str));
			}
	
		}
		if (ip != null) {
			String intentionActionCategory = ip.getIntentCategory(domainName);
			if (intentionActionCategory == null) {
				intentionActionCategory = ip.getIntentCategory(domainName);
			}
	
			if (intentionActionCategory != null) {
				if (intentionActionCategory.equals(IntentLexicon.INTENT_ACTION_TAG) && isAlreadyStrong) {
					return true;
				} else if (intentionActionCategory.equals(IntentLexicon.STRONG_INTENT_ACTION_TAG)) {
					return true;
				}
			}
		}
		return false;
	}*/

    public boolean isRejectionWord(String str) {
        if (str.matches("(reject)|(deny)")) {
            logger.info("REJECTION WORD : " + str);
            return true;
        }
        String lemma = CoreNLPController.lemmatizeToString(language, str);
        if (lemma.matches("(reject)|(deny)")) {
            logger.info("REJECTION WORD : " + str);
            return true;
        }
        return false;
    }

    private void printLoaded() {
        System.out.println(language + " lexicons loaded");
    }

}