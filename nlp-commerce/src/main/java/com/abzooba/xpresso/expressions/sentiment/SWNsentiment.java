/**
 *
 */
package  com.abzooba.xpresso.expressions.sentiment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.abzooba.xpresso.cls.ConceptsMiner;
import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.utils.FileIO;

/**
 * @author Koustuv Saha
 * 16-Oct-2014 10:00:03 am
 * XpressoV2.7  SWNsentiment
 */
public class SWNsentiment {

    private static final Map<String, List<Double[]>> sentiWNmap = new HashMap<String, List<Double[]>>();
    //	private static final String sentiWordNetFileName = "resources/SentiWordNet_3.0.0_20130122.txt";

    public static void init() {

        //		loadSentiWordNet(sentiWordNetFileName);
        loadSentiWordNet(XpConfig.SENTI_WORDNET_FILE);
    }

    private static void loadSentiWordNet(String fileName) {
        List<String> swnLinesList = new ArrayList<String>();
        try {
            InputStream is = FileIO.getInputStreamFromFileName(fileName);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            //			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF8"));
            String line;

            while ((line = br.readLine()) != null) {
                swnLinesList.add(line);
            }
            br.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //		FileIO.read_file(fileName, swnLinesList);

        for (int i = 27; i < swnLinesList.size(); i++) {
            String strLine = swnLinesList.get(i);
            String[] strArr = strLine.split("\t");
            String pos = strArr[0];
            try {
                if (strArr.length >= 3) {
                    Double posScore = Double.parseDouble(strArr[2]);
                    Double negScore = Double.parseDouble(strArr[3]);
                    String[] wordArr = strArr[4].split(" ");
                    for (int j = 0; j < wordArr.length; j++) {
                        String word = wordArr[j].substring(0, wordArr[j].indexOf("#"));
                        String key = word + "#" + pos;
                        List<Double[]> sentimentList = sentiWNmap.get(key);
                        if (sentimentList == null) {
                            sentimentList = new ArrayList<Double[]>();
                        }
                        Double[] sentimentVal = new Double[2];
                        sentimentVal[0] = posScore;
                        sentimentVal[1] = negScore;
                        sentimentList.add(sentimentVal);
                        sentiWNmap.put(key, sentimentList);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static double getSWNscore(Map<String, String> wordPOSmap) {
        double posScore = 0.0;
        double negScore = 0.0;
        for (Map.Entry<String, String> entry : wordPOSmap.entrySet()) {
            String pos = ConceptsMiner.getGeneralizedPOS(entry.getValue());
            //			String[] swnMapKey = new String[] { entry.getKey(), pos };
            String swnMapKey = entry.getKey() + "#" + pos;
            List<Double[]> currScoreList = sentiWNmap.get(swnMapKey);

            //			System.out.println("Looking for: " + swnMapKey);
            if (currScoreList != null) {
                double[] currScore = new double[] { 0.0, 0.0 };
                for (Double[] score : currScoreList) {
                    //					System.out.println("Score for " + entry.getKey() + ">>" + score[0] + ": " + score[1]);
                    currScore[0] = currScore[0] + score[0];
                    currScore[1] = currScore[1] + score[1];
                }
                posScore += currScore[0];
                negScore += currScore[1];
            }
        }
        return posScore - negScore;
    }

    public static void main(String[] args) {
        init();
        //		System.out.println(sentiWNmap);
        Map<String, String> wordPOSmap = new HashMap<String, String>();
        wordPOSmap.put("great", "JJR");
        System.out.println(getSWNscore(wordPOSmap));
    }

}