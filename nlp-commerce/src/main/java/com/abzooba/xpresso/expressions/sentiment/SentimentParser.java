package com.abzooba.xpresso.expressions.sentiment;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpEngine;
import com.abzooba.xpresso.engine.core.XpLexMatch;
import com.abzooba.xpresso.machinelearning.XpClassifierML;
import com.abzooba.xpresso.machinelearning.XpFeatureVector;
import com.abzooba.xpresso.textanalytics.PosTags;
import com.abzooba.xpresso.textanalytics.ProcessString;
import com.abzooba.xpresso.textanalytics.VocabularyTests;
import org.slf4j.Logger;


/**
 * @author Koustuv Saha
 * 14-Mar-2014 10:46:39 am
 * SentimentAnalysis-RnD ParseRules
 */
public class SentimentParser {
    protected static final Logger logger = XCLogger.getSpLogger();

    //	private static final Pattern too_rule = Pattern.compile("too\\s(.*?)\\s");

    protected static final Pattern SHIFT_CORRECT_PATTERN = Pattern.compile("(SHFT\\s(null\\s?){4,})");

    protected static String sentiTagParse(Languages language, List<String> tokenList, Map<String, String> wordPOSmap, Map<String, String> tagSentimentMap) {

        //		String taggedStr = "";
        // double tic = System.currentTimeMillis();
        StringBuilder taggedStrBuilder = new StringBuilder();
        if (tokenList != null) {

            // System.out.println("Initial TokenList: " + tokenList);
            //	logger.info("Initial tokenList: " + tokenList);
            int numTokens = tokenList.size();
            for (int i = 0; i < numTokens; i++) {
                //				logger.info("Token : " + tokenList.get(i));
				/*First, it is necessary to look up greetings and idioms as they may consist of more than 3 words:
				 * greetings can consist of up to 6 words; idioms up to 10.*/
                StringBuilder phraseStrBuilder = new StringBuilder();
                for (int j = i; j < Math.min(numTokens, i + 10); j++) {
                    phraseStrBuilder.append(tokenList.get(j)).append(" ");
                }
                String phrase = phraseStrBuilder.toString().trim();
                //	logger.info("Looking up the phrase: " + phrase + "\tstarting at i = " + i);
                int offset = 0;
                offset = XpLexMatch.sequenceStartsWith(XpSentiment.getGreetSalutationsList(language), phrase);
                i += offset;
                //				logger.info("offset after greeting check: " + phrase + "\tOffset: " + offset + "\ti = " + i);
                if (offset > 0)
                    continue;
                //	logger.info("offset just before idiom check: " + offset + "\ti = " + i);
                Map<Integer, String> offsetTagMap = XpLexMatch.sequenceStartsWith(XpSentiment.getIdiomsMap(language), phrase);
                String phraseTag = null;
                if (offsetTagMap != null) {
                    for (Entry<Integer, String> entry : offsetTagMap.entrySet()) {
                        offset = entry.getKey();
                        phraseTag = entry.getValue();
                    }
                    i += offset;
                }
                //				logger.info("offset after idiom check: " + phrase + "\tOffset: " + offset + "\ti = " + i);
                //				logger.info("tag: " + phraseTag + "\tphrase: " + phrase);
                if (phraseTag != null) {
                    taggedStrBuilder.append(phraseTag).append(" ");
                    continue;
                }
                //TODO: deal with the issue of lemmatization: certain idioms can be conjugated => mismatch

                String tag = null;
                String word = tokenList.get(i);
                //				logger.info("Word to start with: " + word);
                //			StringBuilder entity = new StringBuilder();
                String entity = null;

                if (i < (numTokens - 1) && VocabularyTests.isToo(language, word)) {
                    //					logger.info("too rule");
                    String nextWord = tokenList.get(i + 1);
                    if (nextWord.matches("\\w+")) {
                        String nextWordPOS = wordPOSmap.get(nextWord);
                        /**
                         * check for "too XXX" only if the POS of the XXX is a verb, adverb, noun or adjective
                         */
                        if (nextWordPOS != null && (PosTags.isAdjective(language, nextWordPOS) || PosTags.isNoun(language, nextWordPOS) || PosTags.isAdverb(language, nextWordPOS) || PosTags.isVerb(language, nextWordPOS))) {
                            //							System.out.println("nextWord(sentiTagParse) : " + nextWord);
                            //							System.out.println("Sending 1: " + nextWord);
                            String nextWordTag = XpSentiment.getLexTag(language, nextWord, wordPOSmap, tagSentimentMap);
                            //							System.out.println("nextWordTag(sentiTagParse) : " + nextWordTag);
                            //							System.out.println("NextWord Tag " + nextWordTag);
                            if (nextWordTag == null || (!nextWordTag.equals(XpSentiment.POSITIVE_TAG) && !nextWordTag.equals(XpSentiment.UNCONDITIONAL_POSITIVE_TAG))) {
                                taggedStrBuilder.append(XpSentiment.NEGATIVE_TAG).append(" ");
                                i += 1;
                                continue;
                            }
                        }
                    }
                }

                if (i < numTokens - 2) {
                    entity = word + " " + tokenList.get(i + 1) + " " + tokenList.get(i + 2);
                    //	logger.info("Working with entity: " + entity);
                    //					System.out.println("Sending 2: " + entity);
                    tag = XpSentiment.getLexTag(language, entity, wordPOSmap, tagSentimentMap);
                    //					logger.info("Initial tag: " + tag);
                    //	logger.info("Trigram '" + entity + "' getLexTag: " + tag);
                    if (tag == null) {
                        if (ProcessString.isStopWord(language, tokenList.get(i + 1))) {
                            entity = word + " " + tokenList.get(i + 2);
                            tag = XpSentiment.getLexTag(language, entity, wordPOSmap, tagSentimentMap);
                            //							logger.info("Tag: " + tag + "\tEntity without stopword: " + entity);
                        }
                    }
                }
                if (tag != null) {
                    taggedStrBuilder.append(tag).append(" ");
                    //					System.out.println(entity + "\t1. Tag: " + tag + "\ttaggedStrBuilder: " + taggedStrBuilder.toString());
                    i += 2;
                } else {
                    if (i < numTokens - 1) {
                        entity = word + " " + tokenList.get(i + 1);
                        //						System.out.println("Sending 3: " + entity);
                        tag = XpSentiment.getLexTag(language, entity, wordPOSmap, tagSentimentMap);
                        //						logger.info("Tag: " + tag + "\tTwo-word entity: " + entity);
                        //	logger.info("Bigram '" + entity + "' getLexTag: " + tag);
                    }
                    if (tag != null) {
                        taggedStrBuilder.append(tag).append(" ");
                        //						System.out.println(entity + "\t2. Tag: " + tag + "\ttaggedStrBuilder: " + taggedStrBuilder.toString());
                        i += 1;
                    } else {
                        entity = word;
                        if (tag == null) {
                            //							System.out.println("Sending 4: " + entity);
                            tag = XpSentiment.getLexTag(language, entity, wordPOSmap, tagSentimentMap);
                            //							logger.info("Tag: " + tag + "\tEntity: " + entity);
                        }
                        //	logger.info("Unigram '" + entity + "' getLexTag: " + tag);

						/*If it is a unigram or the tag is not Null*/
                        if (!entity.contains(" ") || tag != null) {
                            taggedStrBuilder.append(tag).append(" ");
                            //							System.out.println(entity + "\t3. Tag: " + tag + "\ttaggedStrBuilder: " + taggedStrBuilder.toString());
                        }
                    }
                }

                //				logger.info("sentiTaggedStr : " + taggedStrBuilder.toString());
            }

        }
        String taggedStr = taggedStrBuilder.toString();
        Matcher m_shft_null = SHIFT_CORRECT_PATTERN.matcher(taggedStr);
        while (m_shft_null.find()) {
            taggedStr = taggedStr.replaceAll(m_shft_null.group(1), " ");
        }
        taggedStr = taggedStr.replaceAll("null", "").replaceAll(XpSentiment.NON_SENTI_TAG, "").replaceAll("\\s+", " ");
        // double tac = System.currentTimeMillis();
        // logger.info("sentiTagParse performed in " + (tac - tic) + " ms");
        return taggedStr;
    }

    //	public static LinkedHashMap<String, String> sentiTagParseToMap(String str) {
    //		LinkedHashMap<String, String> stringLabelMap = new LinkedHashMap<String, String>();
    //		List<String> strArr = ProcessString.tokenizeString(str);
    //
    //		for (int i = 0; i < strArr.size(); i++) {
    //			String tag = null;
    //			String word = strArr.get(i);
    //			String entity = null;
    //			if (i < strArr.size() - 2) {
    //				entity = word + " " + strArr.get(i + 1) + " " + strArr.get(i + 2);
    //				tag = SentimentLexicon.getLexTag(entity, null);
    //			}
    //			if (tag != null) {
    //				stringLabelMap.put(entity, tag);
    //				i += 2;
    //			} else {
    //				if (i < strArr.size() - 1) {
    //					entity = word + " " + strArr.get(i + 1);
    //					tag = SentimentLexicon.getLexTag(entity, null);
    //				}
    //				if (tag != null) {
    //					stringLabelMap.put(entity, tag);
    //					i += 1;
    //				} else {
    //					entity = word;
    //					tag = SentimentLexicon.getLexTag(entity, null);
    //					if (tag != null) {
    //						stringLabelMap.put(entity, tag);
    //					}
    //				}
    //			}
    //
    //		}
    //		return stringLabelMap;
    //	}

    private static String evaluateSentimentRules(String senti_tagged, List<String> tokenList, XpFeatureVector featureSet) {

        SentimentRules src = new SentimentRules(senti_tagged, tokenList, featureSet);
        return src.determineSentiment();
    }

    public static String sentimentVoteAggregator(String rbSentiment, String mlSentiment, String stanfordSentiment, XpFeatureVector xpf) {

        //		if (isPolarSentiment(mlSentiment) && !isPolarSentiment(rbSentiment)) {
        //			if (senti_tagged_str == null) {
        //				return mlSentiment;
        //			} else if (senti_tagged_str.split(" ").length <= 2) {
        //				return rbSentiment;
        //			} else {
        //				return mlSentiment;
        //			}
        //		} else {
        //			return rbSentiment;
        //		}

        //		if (isPolarSentiment(mlSentiment) && !isPolarSentiment(rbSentiment)) {
        if (!SentimentFunctions.isPolarSentiment(rbSentiment) && !xpf.isGreetingsSalutationPresentFeature() && !xpf.isNeedFeature()) {

            //			System.out.println("Stanford Sentiement Feature " + xpf.getStanfordSentimentStr() + "\t" + xpf.getStanfordSentimentFeature());
            if (SentimentFunctions.isPolarSentiment(stanfordSentiment)) {
				/*If mlSentiment is Polar and it is opposite of Stanford Sentiment, so send rbSentiment*/
                if (SentimentFunctions.isPolarSentiment(mlSentiment) && !mlSentiment.equals(stanfordSentiment)) {
                    xpf.setSentimentPrimarilyStanford(false);
                    return rbSentiment;
                } else {
                    xpf.setSentimentPrimarilyStanford(true);
                    return stanfordSentiment;
                }
            } else {
                xpf.setSentimentPrimarilyStanford(false);
                return mlSentiment;
            }
        } else {
            xpf.setSentimentPrimarilyStanford(false);
            return rbSentiment;
        }

        //		if (rbSentiment.equals(mlSentiment)) {
        //			return rbSentiment;
        //		}
        //
        //		else if (isPolarSentiment(rbSentiment)) {
        //			return rbSentiment;
        //		} else if (isPolarSentiment(mlSentiment)) {
        //			return mlSentiment;
        //		} else {
        //			return rbSentiment;
        //		}

        //		else
        //			return SentiLex.NEGATIVE_SENTIMENT.equals(mlSentiment) ? mlSentiment : rbSentiment;

        //		return rbSentiment;

    }

    /**
     * @author Koustuv Saha
     * 23-Apr-2014
     * @param tokenList
     * @param tagSentimentMap from SDP
     * @return
     * Passes the sentiment-tagged string via the rules
     */
    public static String rulesPipeline(Languages language, List<String> tokenList, Map<String, String> wordPOSmap, Map<String, String> tagSentimentMap, XpFeatureVector featureSet, boolean considerMLsentiment) {
        String rbSentiment = null;
        String senti_tagged_str = sentiTagParse(language, tokenList, wordPOSmap, tagSentimentMap);
        rbSentiment = evaluateSentimentRules(senti_tagged_str, tokenList, featureSet);
        //		logger.info(tokenList.toString() + "\t" + senti_tagged_str + "\t" + rbSentiment);

        String netSentiment = rbSentiment;
        String mlSentiment = null;
        String stanfordSentiment = featureSet.getStanfordSentimentStr();

        //		System.out.println("Consider ML Sentiment Boolean: " + considerMLsentiment);
        if (considerMLsentiment) {
            //			System.out.println("TokenSize: " + tokenList.size() + "\tMachineLearningMode: " + XpEngine.getMachineLearningMode());
            if (XpEngine.getMachineLearningMode() && tokenList.size() > 2 && senti_tagged_str.trim().length() > 0) {
                XpClassifierML sentiClassifier = XpEngine.getXpSentiClassifier(language);
                //			String snippet = ProcessString.sentencifyList(tokenList);
                if (sentiClassifier != null) {
                    mlSentiment = sentiClassifier.getPredictedClass(featureSet);
                    //					stanfordSentiment = featureSet.getStanfordSentimentStr();
                    if (mlSentiment != null) {
                        if (SentimentFunctions.isContainSentimentBearingTag(senti_tagged_str)) {
                            netSentiment = sentimentVoteAggregator(rbSentiment, mlSentiment, stanfordSentiment, featureSet);
                        }
                    }
                }
            }
            if (netSentiment == null) {
                netSentiment = XpSentiment.NEUTRAL_SENTIMENT;
            }
            //			if (XpActivator.PW != null) {
            //				XpActivator.PW.println(ProcessString.sentencifyList(tokenList) + "\t" + rbSentiment + "\t" + mlSentiment + "\t" + stanfordSentiment + "\t" + netSentiment);
            //			}
        }

        //		if (considerMLsentiment && XpEngine.getMachineLearningMode()) {
        //			XpClassifierML sentiClassifier = XpEngine.getXpSentiClassifier();
        //			//			String snippet = ProcessString.sentencifyList(tokenList);
        //			if (sentiClassifier != null) {
        //				mlSentiment = sentiClassifier.getPredictedClass(featureSet);
        //			}
        //		}

        logger.info("RB: " + rbSentiment + "\tML: " + mlSentiment + "\tStanford: " + stanfordSentiment + "\tNetSentiment: " + netSentiment + "\n");

        return netSentiment;
    }

    public static Map<List<String>, String> rulesPipelineAdvanced(Languages language, List<List<String>> splitTokenList, Map<String, String> tagSentimentMap, XpFeatureVector featureSet) {
        String[] sentiArr = new String[splitTokenList.size()];
        Map<List<String>, String> snippetSentiMap = new LinkedHashMap<List<String>, String>();
        for (int i = 0; i < splitTokenList.size(); i++) {
            List<String> currTokenList = splitTokenList.get(i);
            String senti_tagged_str = sentiTagParse(language, currTokenList, null, tagSentimentMap);
            sentiArr[i] = evaluateSentimentRules(senti_tagged_str, currTokenList, featureSet);
            logger.info(currTokenList.toString() + "\t" + senti_tagged_str + "\t" + sentiArr[i]);
        }

        for (int i = 1; i < sentiArr.length; i++) {
            if (sentiArr[i - 1] != null) {
                if (SentimentFunctions.isOppositeSentiment(sentiArr[i - 1], sentiArr[i])) {
                    continue;
                } else if (SentimentFunctions.isPolarSentiment(sentiArr[i]) && !SentimentFunctions.isPolarSentiment(sentiArr[i - 1])) {
                    sentiArr[i - 1] = SentimentFunctions.switchSentiment(sentiArr[i]);
                    // }
                } else if (SentimentFunctions.isPolarSentiment(sentiArr[i - 1]) && !SentimentFunctions.isPolarSentiment(sentiArr[i])) {
                    sentiArr[i] = SentimentFunctions.switchSentiment(sentiArr[i - 1]);
                    // }
                }
            }
        }

        for (int i = 0; i < splitTokenList.size(); i++) {
            // System.out.println(shiftSplitArr[i] + ">> " + sentiArr[i]);
            //			snippetSentiMap.put(splitTokenList.get(i), sentiArr[i]);
            List<String> currList = splitTokenList.get(i);
            String rbSentiment = sentiArr[i];
            String netSentiment = rbSentiment;

            //			abuseClassifier(currList, featureSet);

            String mlSentiment = null;
            String stanfordSentiment = featureSet.getStanfordSentimentStr();
            //			if (!XpEngine.getTrainingMode() && !XpEngine.getInteractiveMode()) {
            if (XpEngine.getMachineLearningMode()) {
                //				Classifier tempClassifier = XpEngine.getXpSentiClassifier();
                //				String text = ProcessString.sentencifyList(currList);

                XpClassifierML sentiClassifier = XpEngine.getXpSentiClassifier(language);
                if (sentiClassifier != null) {
                    mlSentiment = sentiClassifier.getPredictedClass(featureSet);

                    if (mlSentiment != null) {
                        netSentiment = sentimentVoteAggregator(rbSentiment, mlSentiment, stanfordSentiment, featureSet);
                    }
                }
            }
            if (netSentiment == null) {
                //				netSentiment = SentiLex.NONE_SENTIMENT;
                netSentiment = XpSentiment.NEUTRAL_SENTIMENT;
            }
            //			System.out.println("RbSentiment: " + rbSentiment + "\t" + "MlSentiment: " + mlSentiment + "\t" + "NetSentiment: " + netSentiment);
            logger.info("RbSentiment: " + rbSentiment + "\tMlSentiment: " + mlSentiment + "\tStanfordSentiment: " + stanfordSentiment + "\tNetSentiment: " + netSentiment + "\n");
            snippetSentiMap.put(currList, netSentiment);

            //			if (!rbSentiment.equals(mlSentiment) && mlSentiment != null) {
            //				try {
            //					PrintWriter pw = new PrintWriter(new FileWriter("data/SentimentMisMatch.txt", true), true);
            //					pw.println(ProcessString.sentencifyList(currList) + "\t" + rbSentiment + "\t" + mlSentiment + "\t" + netSentiment + "\t" + featureSet.toString2());
            //					pw.close();
            //				} catch (Exception ex) {
            //					ex.printStackTrace();
            //				}
            //			}

        }
        return snippetSentiMap;

    }
}