package  com.abzooba.xpresso.expressions.emotion;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.textanalytics.PosTags;

public class EmotionPhraseInfo {

    private String phrase;
    private Double[] emotionMap;
    private Set<String> posSet;
    private Languages language;

    public EmotionPhraseInfo() {
        emotionMap = new Double[EmotionLexicon.getEmotionCategoriesCount()];
        posSet = new HashSet<String>();
        language = Languages.EN;
    }

    public EmotionPhraseInfo(Languages language, Double[] emotionMap) {
        posSet = new HashSet<String>();
        this.emotionMap = emotionMap;
        this.language = language;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public Double[] getEmotionMap() {
		/*Such that where it is received, they don't get to access the same memory location as the NRC emotion vector*/
        Double[] emotionVector = new Double[EmotionLexicon.getEmotionCategoriesCount()];
        for (int i = 0; i < emotionVector.length; i++) {
            emotionVector[i] = emotionMap[i];
        }
        return emotionVector;
        //		return emotionMap;
    }

    public Double getEmotionMapElement(int i) {
        Double obj = null;
        if (i < EmotionLexicon.getEmotionCategoriesCount())
            obj = emotionMap[i];
        return obj;
    }

    public void setEmotionMapElement(int i, Double element) {
        if (i < EmotionLexicon.getEmotionCategoriesCount())
            emotionMap[i] = element;
    }

    public void setEmotionMap(Double[] emotionMap) {
        this.emotionMap = emotionMap;
    }

    public Set<String> getPosSet() {
        return posSet;
    }

    public void addPosToPosSet(String pos) {
        posSet.add(pos);
    }

    public void addMultiplePosToPosSet(Collection<String> posCollection) {
        posSet.addAll(posCollection);
    }

    public boolean checkIfValidPos(String posTag) {
        if (posSet.isEmpty())
            return true;
        String validTag = PosTags.getValidTag(language, posTag);
        if (validTag == null)
            return false;
        return posSet.contains(validTag);
    }

}