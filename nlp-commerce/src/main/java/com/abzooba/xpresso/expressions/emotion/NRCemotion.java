/**
 *
 */
package  com.abzooba.xpresso.expressions.emotion;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Annotations;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpEngine;
import com.abzooba.xpresso.textanalytics.CoreNLPController;
import com.abzooba.xpresso.utils.FileIO;

/**
 * @author Koustuv Saha
 * Jan 14, 2015 11:34:35 AM
 * XpressoV2.7  NRCemotion
 */
public class NRCemotion {

    public static int START_INDEX = -1;
    public static int END_INDEX = -1;

    // private static final String nrcFileName =
    // "resources/NRC-emotion-lexicon-wordlevel-alphabetized-v0.92.txt";

    private static Map<Languages, Map<String, EmotionPhraseInfo>> wordEmotionMap = new HashMap<Languages, Map<String, EmotionPhraseInfo>>();

    //	private static Map<String, Double[]> wordEmotionMap;

    // private static Set<String> NRCresultHash = new HashSet<String>();

    // public static void addToNRCresultHash(String content) {
    // synchronized (NRCresultHash) {
    // NRCresultHash.add(content);
    // }
    // }

    // public static void init() throws IOException {
    //
    // // wordEmotionMap = new HashMap<String, List<String>>();
    // wordEmotionMap = new HashMap<String, Double[]>();
    // List<String> nrcLines = new ArrayList<String>();
    // FileIO.read_file(nrcFileName, nrcLines);
    // int numEmotionCategories = EmotionLexicon.getEmotionCategories().size();
    //
    // for (String nrcLine : nrcLines) {
    // String[] strArr = nrcLine.split("\t");
    // if (strArr.length < 3) {
    // continue;
    // }
    // String emotionVal = strArr[2];
    // String emotion = strArr[1];
    // // if (emotionVal.equals("1") &&
    // EmotionLexicon.getEmotionCategories().contains(emotion)) {
    // if (EmotionLexicon.getEmotionCategories().contains(emotion)) {
    // String word = strArr[0];
    //
    // // Set<String> emotionTypes = wordEmotionMap.get(word);
    // // List<String> emotionTypes = wordEmotionMap.get(word);
    // Double[] emotionArr = wordEmotionMap.get(word);
    // if (emotionArr == null) {
    // // emotionTypes = new ArrayList<String>();
    // emotionArr = new Double[numEmotionCategories];
    // // emotionTypes = new HashSet<String>();
    // }
    // emotionArr[EmotionLexicon.getIndex(emotion)] =
    // Double.parseFloat(emotionVal);
    // // emotionTypes.add(emotion);
    // wordEmotionMap.put(word, emotionArr);
    // String lemma = CoreNLPController.lemmatizeToString(word);
    // wordEmotionMap.put(lemma, emotionArr);
    // }
    // }
    //
    // // PrintWriter pw = new PrintWriter(new
    // FileWriter("resources/nrc-emotions-resources.txt"));
    // // pw.println("Token\tAnger\tDisgust\tFear\tJoy\tSadness\tSurprise");
    // // for (Map.Entry<String, Double[]> entry : wordEmotionMap.entrySet()) {
    // // pw.print(entry.getKey());
    // // for (Double val : entry.getValue()) {
    // // pw.print("\t" + val);
    // // }
    // // pw.println();
    // // }
    // // pw.close();
    // // System.out.println("Written NRC FIle");
    // }

    public static void init() throws IOException {

        for (Languages lang : XpConfig.LANGUAGES) {
            Map<String, EmotionPhraseInfo> wordEmotionMapLang = new HashMap<String, EmotionPhraseInfo>();
            //		wordEmotionMap = new HashMap<String, Double[]>();
            String nrcFileName = XpConfig.getEmotionKB(lang);

            List<String> nrcLines = new ArrayList<String>();

            FileIO.read_file(nrcFileName, nrcLines);

            //		int numEmotionCategories = EmotionLexicon.getEmotionCategories().size();
            int numEmotionCategories = EmotionLexicon.getEmotionCategoriesCount();
            // for (String nrcLine : nrcLines) {
            //		for (int lineNo = 1; lineNo < nrcLines.size(); lineNo++) {
            //
            //			String[] strArr = nrcLines.get(lineNo).split("\t");
            //			if (strArr.length < 6) {
            //				continue;
            //			}
            //			String token = strArr[0];
            //
            //			Double[] emotionArr = new Double[numEmotionCategories];
            //			int countZeroes = 0;
            //			for (int i = 1; i < strArr.length; i++) {
            //				emotionArr[i - 1] = Double.parseFloat(strArr[i]);
            //				if (emotionArr[i - 1] == 0) {
            //					countZeroes += 1;
            //				}
            //			}
            //			if (countZeroes != numEmotionCategories) {
            //				wordEmotionMap.put(token, emotionArr);
            //			}
            //
            //		}

            for (int lineNo = 1; lineNo < nrcLines.size(); lineNo++) {

                String[] strArr = nrcLines.get(lineNo).split("\t");
                if (strArr.length < 6) {
                    continue;
                }

                Double[] emotionArr = new Double[numEmotionCategories];
                int countZeroes = 0;
                int i = 1;
                while (i <= numEmotionCategories) {
                    emotionArr[i - 1] = Double.parseDouble(strArr[i]);
                    //				System.out.println("emotionArr element : " + emotionArr[i - 1]);
                    if (emotionArr[i - 1] == 0) {
                        countZeroes += 1;
                    }
                    i++;
                }
                if (countZeroes < numEmotionCategories) {
                    EmotionPhraseInfo emotionInfo = new EmotionPhraseInfo(lang, emotionArr);
                    String token = strArr[0];
                    emotionInfo.setPhrase(token);
                    if (i < strArr.length) {
                        Set<String> posSet = new HashSet<String>();
                        while (i < strArr.length) {
                            posSet.add(strArr[i]);
                            i++;
                        }
                        emotionInfo.addMultiplePosToPosSet(posSet);
                    }
                    wordEmotionMapLang.put(token, emotionInfo);
                }

            }
            wordEmotionMap.put(lang, wordEmotionMapLang);
        }
    }

    // public static Set<String> getEmotion(String word) {
    // Set<String> emotionList = wordEmotionMap.get(word);
    // if (emotionList == null) {
    // emotionList =
    // wordEmotionMap.get(CoreNLPController.lemmatizeToString(word));
    // }
    // return emotionList;
    // // return wordEmotionMap.get(word);
    // }

    //	public static Double[] getEmotion(String word) {
    //		Double[] emotionArr = wordEmotionMap.get(word);
    //		if (emotionArr == null) {
    //			emotionArr = wordEmotionMap.get(CoreNLPController.lemmatizeToString(word));
    //		}
    //		return emotionArr;
    //		// return wordEmotionMap.get(word);
    //	}

    public static Double[] getEmotion(Languages lang, String word, String pos) {
        //		System.out.println("word : " + word + " POS within getEmotion : " + pos);
        EmotionPhraseInfo emotionObj = wordEmotionMap.get(lang).get(word);
        //		System.out.println("emotionObj is null : " + (emotionObj == null));
        //		if (emotionObj != null) {
        //			System.out.println("is valid pos : " + emotionObj.checkIfValidPos(pos));
        //		}
        if (emotionObj != null && emotionObj.checkIfValidPos(pos)) {
            //			System.out.println("returned emotionmap for word - " + word + " pos - " + pos);
            return emotionObj.getEmotionMap();
        }
        emotionObj = wordEmotionMap.get(lang).get(CoreNLPController.lemmatizeToString(lang, word));
        //		System.out.println("emotionObj is null : " + (emotionObj == null));
        //		if (emotionObj != null) {
        //			System.out.println("is valid pos : " + emotionObj.checkIfValidPos(pos));
        //		}
        if (emotionObj != null && emotionObj.checkIfValidPos(pos)) {
            //			System.out.println("returned emotionmap for word - " + word + " pos - " + pos);
            return emotionObj.getEmotionMap();
        }
        return null;
    }

    // public static List<String> sequenceContain(Map<String, List<String>>
    // sequenceMap, String text, EmotionVector ev) {
    //
    // for (Entry<String, List<String>> entry : sequenceMap.entrySet()) {
    // String sequence = entry.getKey();
    // if (text.matches("(.*?)\\b" + sequence + "\\b(.*?)")) {
    // // System.out.println("Searching for sequence: " + sequence);
    // // List<String> emotionList = entry.getValue();
    // // for (String emotion : emotionList) {
    // // ev.setEmotionVector(emotion);
    // // }
    // // return entry.getValue();
    // }
    // }
    // return null;
    // }

    // private static Set<String> getEmotionsFromJSON(String jsonString) {
    //
    // }

    public static void evaluateSemEval() throws IOException {
        List<String> semEvalData = new ArrayList<String>();

        FileIO.read_file("input/emotion/afectiveTextSemEvalData.txt", semEvalData);
        PrintWriter pw = new PrintWriter(new FileWriter("output/emotionComparision.txt"));

        int numCorrect = 0;
        int lineNo;
        pw.println("isCorrect\tSl No.\tSentence\tanger\tdisgust\tfear\tjoy\tsadness\tsurprise\tmanualEmotion\tanger\tdisgust\tfear\tjoy\tsadness\tsurprise\txpEmotion");
        for (lineNo = 1; lineNo < semEvalData.size(); lineNo++) {
            System.out.println(lineNo);
            String strLine = semEvalData.get(lineNo);
            String[] strArr = strLine.split("\t");
            if (strArr.length < 2)
                continue;
            // String sentence = strArr[1];
            // XpEmotionProcessor xpEmo = new XpEmotionProcessor(null, null,
            // null, sentence);
            // Set<String> xpEmotion = xpEmo.processPipeline();

            // EmotionVector evTemp = new EmotionVector(sentence.toLowerCase());
            // Set<String> xpEmotion = evTemp.getEmotionList();

            // EmotionVector evTemp = new EmotionVector(sentence.toLowerCase());
            // // String xpEmotion = evTemp.getEmotion();
            // Set<String> xpEmotion = evTemp.getEmotionList();
            // int max = 0;
            // int manualEmotionIdx = -1;
            // for (int i = 2; i < strArr.length; i++) {
            // int currVal = Integer.parseInt(strArr[i]);
            // if (currVal > max) {
            // max = currVal;
            // manualEmotionIdx = i - 2;
            // }
            // }
            // String manualEmotion =
            // EmotionLexicon.getEmotionFromIdx(manualEmotionIdx);
            // boolean isCorrect = (xpEmotion.equals(manualEmotion)) ? true :
            // false;
            // boolean isCorrect = (xpEmotion.contains(manualEmotion)) ? true :
            // false;
            // if (isCorrect) {
            // numCorrect++;
            // }
            // // pw.println(isCorrect + "\t" + strLine + "\t" + manualEmotion +
            // // "\t" + evTemp.getEmotionVectorString() + "\t" + xpEmotion);
            // pw.println(isCorrect + "\t" + strLine + "\t" + manualEmotion +
            // "\t" + xpEmotion);
        }
        pw.close();
        double accuracy = 0;
        if (lineNo == 1 || lineNo == 0) {
            accuracy = 0;
        } else {
            accuracy = ((double) numCorrect / (double) (lineNo - 1));
        }
        System.out.println("Accuracy: " + accuracy);
    }

    public static void evaluateInsuranceData() throws IOException, InterruptedException {
        List<String> semEvalData = new ArrayList<String>();

        // FileIO.read_file("input/insurance/ibc_aetna2.txt", semEvalData);
        FileIO.read_file("input/insurance/ibc_aetna2.txt", semEvalData);
        PrintWriter pw = new PrintWriter(new FileWriter("output/insuranceEmotionOutput.txt"));

        int lineNo;
        // pw.println("isCorrect\tSl No.\tSentence\tanger\tdisgust\tfear\tjoy\tsadness\tsurprise\tmanualEmotion\tanger\tdisgust\tfear\tjoy\tsadness\tsurprise\txpEmotion");
        for (lineNo = 1; lineNo < semEvalData.size(); lineNo++) {
            System.out.println(lineNo);
            String strLine = semEvalData.get(lineNo);
            String[] strArr = strLine.split("\t");
            if (strArr.length < 2)
                continue;
            String sentence = strArr[2];
            // XpEmotionProcessor xpEmo = new XpEmotionProcessor(null, null,
            // null, sentence);
            // Set<String> xpEmotion = xpEmo.processPipeline();

            XpEngine xe = new XpEngine();
            String jsonStr = xe.distillPostJSON(Annotations.EMO, "insurance", "", null, sentence, false, false);
            // System.out.println(jsonStr);

            // EmotionVector evTemp = new EmotionVector(sentence.toLowerCase());
            // Set<String> xpEmotion = evTemp.getEmotionList();

            // EmotionVector evTemp = new EmotionVector(sentence.toLowerCase());
            // // String xpEmotion = evTemp.getEmotion();
            // Set<String> xpEmotion = evTemp.getEmotionList();
            // int manualEmotionIdx = -1;
            // for (int i = 2; i < strArr.length; i++) {
            // int currVal = Integer.parseInt(strArr[i]);
            // if (currVal > max) {
            // max = currVal;
            // manualEmotionIdx = i - 2;
            // }
            // }
            // String manualEmotion =
            // EmotionVector.getEmotionFromIdx(manualEmotionIdx);
            // boolean isCorrect = (xpEmotion.equals(manualEmotion)) ? true :
            // false;
            // boolean isCorrect = (xpEmotion.contains(manualEmotion)) ? true :
            // false;
            // if (isCorrect) {
            // numCorrect++;
            // }
            // pw.println(isCorrect + "\t" + strLine + "\t" + manualEmotion +
            // "\t" + evTemp.getEmotionVectorString() + "\t" + xpEmotion);
            pw.println(strLine + "\t" + jsonStr);
        }
        pw.close();
    }

    public static void evaluateIsear(String fileName, String outputFileName) throws IOException {
        List<String> iSearData = new ArrayList<String>();

        FileIO.read_file(fileName, iSearData);
        PrintWriter pw = new PrintWriter(new FileWriter(outputFileName));

        int numCorrect = 0;
        int lineNo;
        pw.println("isCorrect\tSl No.\tSentence\tmanualEmotion\txpEmotion");
        int consideredRows = 0;
        int skippedTextCount = 0;

        if (NRCemotion.START_INDEX == -1)
            NRCemotion.START_INDEX = 0;

        if (NRCemotion.END_INDEX == -1 || NRCemotion.END_INDEX >= iSearData.size())
            NRCemotion.END_INDEX = iSearData.size() - 1;

        for (lineNo = NRCemotion.START_INDEX; lineNo <= NRCemotion.END_INDEX; lineNo++) {
            System.out.println(lineNo);
            String strLine = iSearData.get(lineNo);
            String[] strArr = strLine.split("\t");
            if (strArr.length < 3) {
                skippedTextCount++;
                continue;
            }
            String manualEmotion = strArr[1];
            if (EmotionLexicon.getIndex(manualEmotion) == -1) {
                continue;
            }
            String reviewText = strArr[2];

            XpEngine xe = new XpEngine();
            Set<String> xpEmotions = new HashSet<String>();
            try {
                String jsonStr = xe.distillPostJSON(Annotations.EMO, "insurance", manualEmotion, null, reviewText, false, false);
                // System.out.println(jsonStr);
                JSONObject jsonObj = new JSONObject(jsonStr);
                JSONArray emoJSONArr = (JSONArray) jsonObj.get("Overall Emotion");
                for (int i = 0; i < emoJSONArr.length(); i++) {
                    xpEmotions.add(emoJSONArr.getString(i));
                }

            } catch (InterruptedException | JSONException e) {
                e.printStackTrace();
            }

            boolean isCorrect = (xpEmotions.contains(manualEmotion)) ? true : false;
            if (isCorrect) {
                numCorrect++;
            }
            // pw.println(isCorrect + "\t" + "\t" + strArr[0] + "\t" +
            // reviewText + "\t" + manualEmotion + "\t" +
            // evTemp.getEmotionVectorString() + "\t" + xpEmotion);
            pw.println(isCorrect + "\t" + strArr[0] + "\t" + reviewText + "\t" + manualEmotion + "\t" + xpEmotions);
            consideredRows += 1;
        }
        pw.close();
        double accuracy = 0;
        if (consideredRows == 1 || consideredRows == 0) {
            accuracy = 0;
        } else {
            accuracy = ((double) numCorrect / (double) (consideredRows - 1));
        }
        System.out.println("skipped text : " + skippedTextCount);
        System.out.println("Accuracy: " + accuracy);
    }

    public static void evaluateInsuranceData(String fileName, String outputFileName) throws IOException {
        List<String> iSearData = new ArrayList<String>();

        FileIO.read_file(fileName, iSearData);
        PrintWriter pw = new PrintWriter(new FileWriter(outputFileName));

        int lineNo;
        pw.println("Sl No.\tCompany\tReview\txpEmotion");
        for (lineNo = 0; lineNo < iSearData.size(); lineNo++) {
            System.out.println(lineNo);
            if (lineNo > 1500) {
                break;
            }

            String strLine = iSearData.get(lineNo);
            String[] strArr = strLine.split("\t");
            if (strArr.length < 5) {
                continue;
            }
            String reviewText = strArr[4];
            // System.out.println(reviewText);
            XpEngine xe = new XpEngine();
            String jsonStr;
            try {
                jsonStr = xe.distillPostJSON(Annotations.EMO, "insurance", strArr[0], null, reviewText, false, false);
                pw.println(lineNo + "\t" + strArr[0] + "\t" + reviewText + "\t" + jsonStr);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // System.out.println(jsonStr);
            //
            // JSONObject jsonObj = new JSONObject(jsonStr);
            // JSONArray emoJSONArr = (JSONArray)
            // jsonObj.get("Overall Emotion");
            // for (int i = 0; i < emoJSONArr.length(); i++) {
            // xpEmotions.add(emoJSONArr.getString(i));
            // }

            // pw.println(isCorrect + "\t" + "\t" + strArr[0] + "\t" +
            // reviewText + "\t" + manualEmotion + "\t" +
            // evTemp.getEmotionVectorString() + "\t" + xpEmotion);
        }
        pw.close();
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        // CoreNLPController.init();
        // EmotionVector.init();
        XpEngine.init(true);
        NRCemotion.START_INDEX = 701;
        NRCemotion.END_INDEX = 800;

        // String str =
        // "We were 'arrogant and stupid' over Iraq, says US diplomat";
        // String str = "I am disappointed";
        // EmotionVector ev = new EmotionVector(str.toLowerCase());
        // System.out.println("Emotion: " + ev.getEmotionList());
        // System.out.println(sequenceContain(wordEmotionMap, str.toLowerCase(),
        // ev));
        // System.out.println(sequenceContain(wordEmotionMap, str.toLowerCase(),
        // ev));

        // evaluateSemEval();
        // evaluateInsuranceData();

        // evaluateIsear("input/emotion/ise_processed.txt");
        // evaluateIsear("input/emotion/ise_processed_sample.txt");
        // evaluateIsear("input/emotion/ise_processed.txt");
        // System.out.println(EmotionVector.COUNT);

        // evaluateIsear("input/emotion/ise_processed.txt");
        // evaluateIsear("input/emotion/ise_processed_sample.txt");

        String outputFileName = "output/emotionComparision" + XpEngine.getCurrentDateStr() + ".txt";
        evaluateIsear("input/emotion/ise_processed.txt", outputFileName);

        //
        // System.out.println("negative anger\t" + EmotionVector.NEG_ANGER);
        // System.out.println("negative disgust\t" + EmotionVector.NEG_DISGUST);
        // System.out.println("negative fear\t" + EmotionVector.NEG_FEAR);
        // System.out.println("negative joy\t" + EmotionVector.NEG_JOY);
        // System.out.println("negative sadness\t" + EmotionVector.NEG_SADNESS);
        // System.out.println("negative surprise\t" +
        // EmotionVector.NEG_SURPRISE);
        //
        // System.out.println("neutral anger\t" + EmotionVector.NEU_ANGER);
        // System.out.println("neutral disgust\t" + EmotionVector.NEU_DISGUST);
        // System.out.println("neutral fear\t" + EmotionVector.NEU_FEAR);
        // System.out.println("neutral joy\t" + EmotionVector.NEU_JOY);
        // System.out.println("neutral sadness\t" + EmotionVector.NEU_SADNESS);
        // System.out.println("neutral surprise\t" +
        // EmotionVector.NEU_SURPRISE);

        System.out.println("Processed File");
        // EmotionVector.PW.close();

        // String sentiEmoCombinationFile = "./output/emotion-sentiment.txt";
        // FileIO.write_fileNIO(NRCresultHash, sentiEmoCombinationFile, false);
        // evaluateIsear();

    }
}