/**
 *
 */
package  com.abzooba.xpresso.expressions.intention;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.aspect.domainontology.OntologyUtils;
import com.abzooba.xpresso.aspect.domainontology.XpOntology;
import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpSentenceResources;
import com.abzooba.xpresso.textanalytics.CoreNLPController;
import com.abzooba.xpresso.textanalytics.PosTags;
import com.abzooba.xpresso.utils.FileIO;
import org.slf4j.Logger;


/**
 * @author Koustuv Saha
 * Aug 10, 2015 3:08:11 PM
 * XpressoV3.0  IntentLexicon
 */
public class IntentLexicon {

    protected static final Logger logger = XCLogger.getSpLogger();

    public static final String INTENT_ACTION_TAG = "ia";
    public static final String STRONG_INTENT_ACTION_TAG = "strong-ia";
    public static final String NON_INTENT_ACTION_TAG = "nia";

    public static final String INTENT_OBJECT_TAG = "io";
    public static final String NON_INTENT_OBJECT_TAG = "nio";

    public static final String NON_INTENT_OBJECT_PATTERN = "\\b(.*?)((tion)|(ism)|(ity)|(ment)|(ness)|(age)|(ance)|(ship)|(ability)|(acy))\\b";

    protected Map<String, IntentPhraseInfo> intentActionWordsMap = new HashMap<String, IntentPhraseInfo>();
    protected Map<String, IntentPhraseInfo> intentObjectWordsMap = new HashMap<String, IntentPhraseInfo>();
    protected Languages language;

    public IntentLexicon(Languages language) {
        this.language = language;
        loadIntentLexicons();

    }

    private void loadIntentLexicons() {

        List<String> tempIntentActionList = new ArrayList<String>();
        FileIO.read_file(XpConfig.getIntentActionWords(language), tempIntentActionList);

        for (String line : tempIntentActionList) {
            String[] intentActionTuple = line.split("\t");

            //			if (intentActionTuple.length >= 3) {
            IntentPhraseInfo ip = new IntentPhraseInfo(intentActionTuple);

            intentActionWordsMap.put(intentActionTuple[0], ip);
            intentActionWordsMap.put(intentActionTuple[1], ip);
            //		}
        }

        List<String> tempIntentObjectList = new ArrayList<String>();
        FileIO.read_file(XpConfig.getIntentObjectWords(language), tempIntentObjectList);

        for (String line : tempIntentObjectList)

        {
            String[] intentObjectTuple = line.split("\t");
            //			if (intentObjectTuple.length >= 3) {
            IntentPhraseInfo ip = new IntentPhraseInfo(intentObjectTuple);

            intentObjectWordsMap.put(intentObjectTuple[0], ip);
            intentObjectWordsMap.put(intentObjectTuple[1], ip);
            //			}

        }

    }

    public boolean isNormalIntentAction(String str, String domainName, String subject, XpSentenceResources xto) {
        str = str.toLowerCase();

        IntentPhraseInfo ip = intentActionWordsMap.get(str);
        if (ip != null) {
            String intentionActionCategory = ip.getIntentCategory(domainName);
            if (intentionActionCategory != null) {
                if (intentionActionCategory.equals(IntentLexicon.INTENT_ACTION_TAG)) {
                    return true;
                } else if (intentionActionCategory.equals(IntentLexicon.STRONG_INTENT_ACTION_TAG)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isIntentActionWord(String str, String domainName, String subject, XpSentenceResources xto, boolean isAlreadyStrong) {
        str = str.toLowerCase();

        IntentPhraseInfo ip = intentActionWordsMap.get(str);
        Map<String, String> wordPOSmap = xto.getWordPOSmap();
        if (ip == null) {

            String pos = null;
            if (wordPOSmap != null) {
                pos = wordPOSmap.get(str);
            }
			/*If it is Past tense, we are lemmatizing it. eg: I wanted a furby*/
            if (pos == null || (pos != null && !PosTags.isPastVerb(language, pos))) {
                ip = intentActionWordsMap.get(CoreNLPController.lemmatizeToString(language, str));
            }

        }
        if (ip != null) {
            String intentionActionCategory = ip.getIntentCategory(domainName);
            //			System.out.println("string - " + str + " intent action category - " + intentionActionCategory);
            if (intentionActionCategory == null) {
                intentionActionCategory = ip.getIntentCategory(domainName);
            }

            if (intentionActionCategory != null) {
                if (intentionActionCategory.equals(IntentLexicon.INTENT_ACTION_TAG) && isAlreadyStrong) {
                    return true;
                } else if (intentionActionCategory.equals(IntentLexicon.STRONG_INTENT_ACTION_TAG)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isDomainRelevantStr(String str, String domainName, String subject, XpSentenceResources xto) {
        if (domainName == null) {
            return true;
        }

        //		String[] aspect = XpOntology.findAspect(language, str, domainName, subject, xto);
        //		Map<String, String[]> tagAspectMap = xto.getTagAspectMap();
        //		if (aspect != null) {
        //			tagAspectMap.put(str, aspect);
        //			logger.info("Intent Object Aspect Found:\t" + str + "\t" + Arrays.toString(aspect));
        //			return true;
        //		}
        String category = XpOntology.getOntologyCategory(language, domainName, str, xto);
		/*No need for Lemma based check any more*/
        //		if (category == null) {
        //			//			category = XpOntology.getOntologyCategory(language, CoreNLPController.lemmatizeToString(language, str), domainName);
        //		}

        if (XpOntology.isDomainRelevantCategory(language, category, domainName)) {
            logger.info("Intent Object Category Relevant:\t" + str + "\t" + category);
            return true;
        }
        return false;
    }

    public boolean isIntentObjectWord(String str, String domainName, String subject, XpSentenceResources xto) {

        //		System.out.println("Entered this method!!");
        str = str.toLowerCase();
        if (OntologyUtils.isAbstractConcept(language, str)) {
            return false;
        }

        IntentPhraseInfo ip = intentObjectWordsMap.get(str);
        if (ip == null) {
            ip = intentObjectWordsMap.get(CoreNLPController.lemmatizeToString(language, str));
        }

        if (ip != null) {
            String intentionObjectCategory = ip.getIntentCategory(domainName);

            if (intentionObjectCategory != null) {
                if (intentionObjectCategory.equals(IntentLexicon.INTENT_OBJECT_TAG)) {
                    logger.info("Intent Object Tag:\t" + str);
                    return isDomainRelevantStr(str, domainName, subject, xto);

                    //					return true;
                } else if (intentionObjectCategory.equals(IntentLexicon.NON_INTENT_OBJECT_TAG)) {
                    logger.info("Non Intent Object Tag:\t" + str);

                    //					return false;
                }
            }
        }

        //		String[] aspect = null;

        Map<String, String> wordPOSmap = xto.getWordPOSmap();
        if (wordPOSmap != null) {
            String posTag = wordPOSmap.get(str);
            //			/*If it is a noun and one of the abstract nouns, then it is non-intent-object*/
            if (posTag != null && PosTags.isNoun(language, posTag)) {

                //				System.out.println("Entered check fro DomainRelevantStr!!!!s");

                //				if (str.matches(IntentLexicon.NON_INTENT_OBJECT_PATTERN)) {
                //					logger.info("Non Intent Object Pattern:\t" + str);
                //					return false;
                //				}

                return isDomainRelevantStr(str, domainName, subject, xto);

                //				aspect = XpOntology.findAspect(language, str, domainName, subject, wordPOSmap, tagAspectMap);
                //				if (aspect != null) {
                //					tagAspectMap.put(str, aspect);
                //					logger.info("Intent Object Aspect Found:\t" + str + "\t" + Arrays.toString(aspect));
                //					isIntentObject = true;
                //					isRelevant = true;
                //					//					return true;
                //				}
            }
        }

        //		if (isIntentObject) {
        //			if (!isRelevant) {
        //				aspect = XpOntology.findAspect(language, str, domainName, subject, wordPOSmap, tagAspectMap);
        //				if (aspect != null) {
        //					isRelevant = true;
        //				}
        //			}
        //
        //			if (!isRelevant) {
        //				String category = XpOntology.getOntologyCategory(language, str);
        //				if (XpOntology.isDomainRelevantCategory(language, category, str)) {
        //					isRelevant = true;
        //				}
        //			}
        //
        //			if (isRelevant || domainName == null) {
        //				logger.info("isIntentObject (True):\t" + str + "\tisRelevant:\t" + isRelevant);
        //				return true;
        //
        //			}
        //		}
        return false;
    }

}