/**
 *
 */
package  com.abzooba.xpresso.expressions.emotion;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpEngine;
import com.abzooba.xpresso.searchengine.PmiCalculator;
import com.abzooba.xpresso.searchengine.SearchEngine.SEARCH_ENGINE;

/**
 * @author Koustuv Saha
 * Feb 20, 2015 10:17:08 AM
 * XpressoV2.7  BingEmotion
 */
public class SearchAPIEmotion {

    private static Map<String, Double[]> wordEmotionMap = new ConcurrentHashMap<String, Double[]>();

    //	private Integer categoriesProcessed;
    //	Semaphore categoriesProcessed;

    private Double[] emotionArr;

    /**
     *
     */
    public SearchAPIEmotion() {
        //		 TODO Auto-generated constructor stub
        //		categoriesProcessed = EmotionLexicon.getEmotionCategoriesCount();
        //		categoriesProcessed = new Semaphore(EmotionLexicon.getEmotionCategoriesCount());
        emotionArr = new Double[EmotionLexicon.getEmotionCategoriesCount()];
    }

    //	class SearchQueryThread implements Runnable {
    //
    //		String phrase;
    //		String emotionCategory;
    //		SEARCH_ENGINE se;
    //		int index;
    //
    //		SearchQueryThread(String phrase, String emotionCategory, SEARCH_ENGINE searchEngine, int index) {
    //			this.phrase = phrase;
    //			this.emotionCategory = emotionCategory;
    //			this.se = searchEngine;
    //			this.index = index;
    //		}
    //
    //		public void run() {
    //			//				emotionArr = null;
    //			//			emotionArr[index] = (Double) LogOddRatioCalculator.getPMI(phrase, emotionCategory, se);
    //			PmiCalculator pc = new PmiCalculator();
    //			emotionArr[index] = (Double) pc.getPMI(phrase, emotionCategory, se);
    //			categoriesProcessed -= 1;
    //			//				queryVal = (Double) LogOddRatioCalculator.getPMI(phrase, emotionCategory, se);
    //		}
    //
    //	}

    private Double[] getLogOddScore(Languages lang, String phrase) {

        //		int catsProcessed = EmotionLexicon.getEmotionCategoriesCount();

        int i = 0;
        //		int sum = 0;

        //		ExecutorService executor = Executors.newFixedThreadPool(EmotionLexicon.getEmotionCategoriesCount());

        for (String emotionCategory : EmotionLexicon.getEmotionCategories()) {
            //			System.out.println("Doing with Emotion: " + emotionCategory);
            PmiCalculator pc = new PmiCalculator();
            emotionArr[i++] = (Double) pc.getPMI(lang, phrase, emotionCategory, SEARCH_ENGINE.BING_API);

            //			Double val = (Double) (double) LogOddRatioCalculator.getLogORRatio(phrase, emotion, SEARCH_ENGINE.BING_API);

            //			Runnable worker = new SearchQueryThread(phrase, emotion, SEARCH_ENGINE.BING_API, i);
            // Runnable worker = new XpThread(i, subj, rvw);
            //			executor.execute(worker);

            //			SearchQueryThread sqt = new SearchQueryThread(phrase, emotion, SEARCH_ENGINE.BING_API, i);
            //			sqt.start();
            //			i++;

            //			Double val = (Double) LogOddRatioCalculator.getPMI(phrase, emotion, SEARCH_ENGINE.BING_API);
            //			emotionArr[i++] = val;

            //			sum += val;
        }
        //		System.out.println("Before Normalization: " + Arrays.toString(emotionArr));

        //		for (int j = 0; j < emotionArr.length; j++) {
        //			emotionArr[j] /= sum;
        //		}
        //		ThreadUtils.shutdownAndAwaitTermination(executor);

        //		while (categoriesProcessed != 0) {
        //		while(!executor.isTerminated())
        //		try {
        //			Thread.sleep(100);
        //		} catch (InterruptedException e) {
        //			e.printStackTrace();
        //		}
        //		}
        wordEmotionMap.put(phrase, emotionArr);
        return emotionArr;
    }

    public Double[] getEmotion(Languages lang, String phrase) {
        Double[] emotionArr = wordEmotionMap.get(phrase);
        if (emotionArr == null) {
            emotionArr = getLogOddScore(lang, phrase);
            //			emotionArr = wordEmotionMap.get(CoreNLPController.lemmatizeToString(word));
        }
        return emotionArr;
        // return wordEmotionMap.get(word);
    }

    public static void main(String[] args) throws Exception {
        XpEngine.init(true);
        //		LogOddRatioCalculator.init();
        PmiCalculator.init();
        SearchAPIEmotion se = new SearchAPIEmotion();
        double startTime = System.currentTimeMillis();
        System.out.println(Arrays.toString(se.getEmotion(Languages.EN, "sexy bhoot")));
        System.out.println("Time taken in Multi Threaded Environment: " + (System.currentTimeMillis() - startTime) + " milliseconds");
        //		LogOddRatioCalculator.writeMap();
        PmiCalculator.exit();
    }
}