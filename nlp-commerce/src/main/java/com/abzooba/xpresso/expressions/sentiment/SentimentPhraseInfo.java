package  com.abzooba.xpresso.expressions.sentiment;

import java.util.HashSet;
import java.util.Set;

import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.textanalytics.PosTags;

public class SentimentPhraseInfo {

    private String lexTag = null;
    private Set<String> posTagList = new HashSet<String>();

    public String getLexTag() {
        return lexTag;
    }

    public void setLexTag(String lexTag) {
        this.lexTag = lexTag;
    }

    public Set<String> getPosTagSet() {
        return posTagList;
    }

    public void addToPosTagList(String posTag) {
        posTagList.add(posTag);
    }

    public boolean ifValidPosTag(Languages language, String posTag) {
        if (posTagList.isEmpty())
            return true;
        String validTag = PosTags.getValidTag(language, posTag);
        if (validTag == null)
            return false;
        return posTagList.contains(validTag);
    }

}