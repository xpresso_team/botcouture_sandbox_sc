/**
 *
 */
package  com.abzooba.xpresso.expressions.emotion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.expressions.sentiment.XpSentiment;
import com.abzooba.xpresso.utils.FileIO;

/**
 * @author Koustuv Saha
 * Jan 22, 2015 6:38:24 PM
 * XpressoV2.7  EmotionLexicon
 */
public class EmotionLexicon {

    public static final String JOY_EMOTION = "joy";
    public static final String SADNESS_EMOTION = "sadness";
    public static final String DISGUST_EMOTION = "disgust";
    public static final String ANGER_EMOTION = "anger";
    public static final String SURPRISE_EMOTION = "surprise";
    public static final String FEAR_EMOTION = "fear";

    // private static Map<String, Integer> emotionIndexMap = new HashMap<String,
    // Integer>();
    private static List<String> emotionsCategoryList = new ArrayList<String>();
    private static Map<Languages, Map<String, List<String>>> emotionVectorMap = new HashMap<Languages, Map<String, List<String>>>();
    private static int numEmotions;

    // private static final String emotionCatNamePattern = ".*_EMOTION";

    /**
     * loads the emotion and their representative words for the glove similarity
     * @param fileName file containing the emotion and their representative words
     */
    public static void init() {
        // for (int i = 0; i < emotionsCategoryList.size(); i++) {
        // emotionIndexMap.put(emotionsCategoryList.get(i), i);
        // }
        boolean emotionsCategoryListed = false;
        for (Languages lang : XpConfig.LANGUAGES) {
            List<String> inputList = new ArrayList<String>();
            FileIO.read_file(XpConfig.getEmotionSeed(lang), inputList);
            Map<String, List<String>> emotionVectorMapLang = emotionVectorMap.get(lang);
            if (emotionVectorMapLang == null) {
                emotionVectorMapLang = new HashMap<String, List<String>>();
            }
            for (String line : inputList) {
                String[] splitLine = line.split("\t");
                if (!emotionsCategoryListed)
                    emotionsCategoryList.add(splitLine[0].trim());

                if (splitLine.length < 2) {
                    continue;
                }
                //			List<String> seedVector = new ArrayList<String>();
                List<String> seedVector = emotionVectorMapLang.get(splitLine[0]);
                if (seedVector == null) {
                    seedVector = new ArrayList<String>();
                }
                for (int i = 1; i < splitLine.length; i++) {
                    seedVector.add(splitLine[i].trim());
                }
                emotionVectorMapLang.put(splitLine[0], seedVector);
            }
            emotionVectorMap.put(lang, emotionVectorMapLang);
            emotionsCategoryListed = true;
        }
        numEmotions = emotionsCategoryList.size();
    }

//	public static Map<String, List<String>> getEmotionVectorMap() {
//		return emotionVectorMap.get(Languages.EN);
//	}

    public static Map<String, List<String>> getEmotionVectorMap(Languages lang) {
        return emotionVectorMap.get(lang);
    }

    public static List<String> getEmotionSeedVector(String emotion) {
        return emotionVectorMap.get(Languages.EN).get(emotion);
    }

    public static List<String> getEmotionSeedVector(Languages lang, String emotion) {
        return emotionVectorMap.get(lang).get(emotion);
    }

    // protected static Map<String, Integer> getEmotionCollection() {
    // return emotionIndexMap;
    // }

    protected static int getIndex(String emotion) {
        return emotionsCategoryList.indexOf(emotion);
        // return (int) emotionIndexMap.get(emotion);
    }

    protected static String getEmotionFromIdx(int emotionIndex) {

        try {
            return emotionsCategoryList.get(emotionIndex);
        } catch (Exception ex) {
            return XpSentiment.NEUTRAL_SENTIMENT;
        }
        // if (emotionIndex == -1) {
        // return SentimentLexicon.NEUTRAL_SENTIMENT;
        // }
        // if (emotionsCategoryList.size() < emotionIndex) {
        // return emotionsCategoryList.get(emotionIndex);
        // }
        // return null;
        // for (Map.Entry<String, Integer> entry : emotionIndexMap.entrySet()) {
        // if (entry.getValue() == emotionIndex) {
        // return entry.getKey();
        // }
        // }
        // return SentimentLexicon.NEUTRAL_SENTIMENT;
    }

    protected static List<String> getEmotionCategories() {
        return emotionsCategoryList;
    }

    protected static int getEmotionCategoriesCount() {
        return numEmotions;
    }

    // public static void init() {
    // for (int i = 0; i < emotionCategoriesArr.length; i++) {
    // // System.out.println(emotionsCategoryList.get(i) + "\t" + i);
    //
    // }
    //
    // Field[] varList = EmotionLexicon.class.getFields();
    // int i = 0;
    // for (Field var : varList) {
    // String varName = var.getName();
    // if (varName.matches(emotionCatNamePattern)) {
    // try {
    // emotionCategoriesArr[i++] = (String) var.get(EmotionLexicon.class);
    // } catch (IllegalArgumentException | IllegalAccessException e) {
    // e.printStackTrace();
    // }
    //
    // }
    // }
    // }

    // protected static Integer getIndex(String emotion) {
    // switch (emotion) {
    // case (JOY_EMOTION):
    // return 0;
    // case (SADNESS_EMOTION):
    // return 1;
    // case (DISGUST_EMOTION):
    // return 2;
    // case (ANGER_EMOTION):
    // return 3;
    // case (SURPRISE_EMOTION):
    // return 4;
    // case (FEAR_EMOTION):
    // return 5;
    // }
    // return null;
    // }
    //
    // protected static String getEmotionFromIdx(int index) {
    // if (index < emotionCategoriesArr.length) {
    // return emotionCategoriesArr[index];
    // }
    // return SentimentLexicon.NEUTRAL_SENTIMENT;
    //
    // }

    public static void main(String[] args) {
        init();
    }
}