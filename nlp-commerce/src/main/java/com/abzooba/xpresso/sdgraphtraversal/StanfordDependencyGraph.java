/**
 *
 */
package com.abzooba.xpresso.sdgraphtraversal;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.aspect.domainontology.OntologyUtils;
import com.abzooba.xpresso.aspect.domainontology.XpOntology;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpExpression;
import com.abzooba.xpresso.engine.core.XpSentenceResources;
import com.abzooba.xpresso.expressions.intention.IntentEngine;
import com.abzooba.xpresso.expressions.intention.XpIntention;
import com.abzooba.xpresso.expressions.sentiment.SentimentEngine;
import com.abzooba.xpresso.expressions.sentiment.SentimentFunctions;
import com.abzooba.xpresso.expressions.sentiment.XpSentiment;
import com.abzooba.xpresso.machinelearning.XpFeatureVector;
import com.abzooba.xpresso.textanalytics.CoreNLPController;
import com.abzooba.xpresso.textanalytics.LuceneSpell;
import com.abzooba.xpresso.textanalytics.PosTags;
import com.abzooba.xpresso.textanalytics.ProcessString;
import com.abzooba.xpresso.textanalytics.VocabularyTests;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.UniversalEnglishGrammaticalRelations;
import edu.stanford.nlp.util.CoreMap;
import org.slf4j.Logger;

/**
 * @author Koustuv Saha
 * 10-Apr-2014 11:50:28 am
 * XpressoV2.0.1 StanfordDependencyGraph
 */
public class StanfordDependencyGraph {

    protected static final Class<?> c = StanfordDependencyGraph.class;
    protected static final Logger logger = XCLogger.getSpLogger();
    private static final Pattern SPECIAL_CHARS_PATTERN = Pattern.compile("[^\\w\\s]", Pattern.UNICODE_CHARACTER_CLASS);

    protected SemanticGraph dependencyGraph;
    protected List<SemanticGraphEdge> sortedEdges;
    protected Set<SemanticGraphEdge> checkedEdge;

    protected String text;
    protected String subject;
    protected String domainName;
    protected Map<String, String> wordPOSmap;

    protected XpFeatureVector featureSet;

    protected Map<String, String> tagSentimentMap;
    protected Map<String, String[]> tagAspectMap;
    protected Map<IndexedWord, List<String>> entityOpinionMap;
    protected Map<String, List<String>> equivalentEntityMap;
    protected Map<String, String> negatedOpinionMap;
    protected Map<String, String> modifiedOpinionMap;
    protected Map<IndexedWord, String> multiWordEntityMap;
    protected Map<String, String> nerMap;
    protected List<CoreMap> entityMentions;

    protected XpSentenceResources xto;

    //	protected Map<IndexedWord, SemanticGraphEdge> tempEntityMap;

    protected Languages language;

    //	public Map<String, String> getTagSentimentMap() {
    //		return this.tagSentimentMap;
    //	}

    //	public Map<String, String[]> getTagAspectMap() {
    //		return this.tagAspectMap;
    //	}

    public String getEdgesAsStr() {
        if (sortedEdges != null) {
            return sortedEdges.toString();
        } else {
            return "[]";
        }
    }

    //	public StanfordDependencyGraph(Languages language, String text, String domainName, String subject, SemanticGraph dependency_graph, XpTextObject xto, XpFeatureVector featureSet) {
    public StanfordDependencyGraph(String text, SemanticGraph dependency_graph, XpSentenceResources xto, XpExpression xe) {
        checkedEdge = new HashSet<SemanticGraphEdge>();

        if (dependency_graph != null) {
            this.dependencyGraph = dependency_graph;
            this.setSortedEdges();

            this.text = text;
            this.subject = xe.getSubject();
            this.domainName = xe.getDomainName();
            this.featureSet = xto.getXpf();
            this.tagSentimentMap = xto.getTagSentimentMap();
            this.wordPOSmap = xto.getWordPOSmap();
            this.tagAspectMap = xto.getTagAspectMap();
            this.entityMentions = xto.getEntityMentions();
            this.nerMap = xto.getNerMap();
            this.language = xe.getLanguage();

            this.xto = xto;

            this.entityOpinionMap = new HashMap<IndexedWord, List<String>>();
            this.equivalentEntityMap = new HashMap<String, List<String>>();
            this.negatedOpinionMap = new HashMap<String, String>();
            this.modifiedOpinionMap = new HashMap<String, String>();
            this.multiWordEntityMap = new HashMap<IndexedWord, String>();

            this.fireDependencyRules();
        }
    }

    /**
     * @return the dependencyGraph
     */
    public SemanticGraph getDependencyGraph() {
        return dependencyGraph;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the domainName
     */
    public String getDomainName() {
        return domainName;
    }

    /**
     * @param domainName the domainName to set
     */
    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    /**
     * @return the sortedEdges
     */
    public List<SemanticGraphEdge> getSortedEdges() {
        return sortedEdges;
    }

    /**
     * @param sortedEdges the sortedEdges to set
     */
    public void setSortedEdges() {
        this.sortedEdges = this.dependencyGraph.edgeListSorted();
        Collections.sort(this.sortedEdges, new DependencyRelationSorter());
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @return the language
     */
    public Languages getLanguage() {
        return language;
    }

    //	/**
    //	 * @return the wordPOSmap
    //	 */
    //	public Map<String, String> getWordPOSmap() {
    //		return wordPOSmap;
    //	}

    //	/**
    //	 * @return the tagSentimentMap
    //	 */
    //	public Map<String, String> getTagSentimentMap() {
    //		return tagSentimentMap;
    //	}

    /**
     * @return the equivalentEntityMap
     */
    public Map<String, List<String>> getEquivalentEntityMap() {
        return equivalentEntityMap;
    }

    /**
     * @return the negatedOpinionMap
     */
    public Map<String, String> getNegatedOpinionMap() {
        return negatedOpinionMap;
    }

    /**
     * @return the modifiedOpinionMap
     */
    public Map<String, String> getModifiedOpinionMap() {
        return modifiedOpinionMap;
    }

    /**
     * @return the multiWordEntityMap
     */
    public Map<IndexedWord, String> getMultiWordEntityMap() {
        return multiWordEntityMap;
    }

    /**
     * @return the nerMap
     */
    public Map<String, String> getNERMap() {
        return nerMap;
    }

    /**
     * @param featureSet the featureSet to set
     */
    public void setFeatureSet(XpFeatureVector featureSet) {
        this.featureSet = featureSet;
    }

    /**
     * @param tagAspectMap the tagAspectMap to set
     */
    public void setTagAspectMap(Map<String, String[]> tagAspectMap) {
        this.tagAspectMap = tagAspectMap;
    }

    /**
     * @param entityOpinionMap the entityOpinionMap to set
     */
    public void setEntityOpinionMap(Map<IndexedWord, List<String>> entityOpinionMap) {
        this.entityOpinionMap = entityOpinionMap;
    }

    /**
     * @return the xto
     */
    public XpSentenceResources getXpTextObject() {
        return xto;
    }

    /**
     * @param xto the xto to set
     */
    public void setXto(XpSentenceResources xto) {
        this.xto = xto;
    }

    protected void addToOpinionMap(IndexedWord entity, String currOpinion) {
        List<String> opinionSet = this.entityOpinionMap.get(entity);
        currOpinion = currOpinion.toLowerCase();
        if (opinionSet == null) {
            opinionSet = new ArrayList<String>();
            //			System.out.println("1.1: Opinion Adding\t" + entity.word() + "\t" + currOpinion);
            opinionSet.add(currOpinion);
        } else {
            // for (String str : tempSet) {
            int length = opinionSet.size();
            boolean addFlag = true;
            for (int i = 0; i < length; i++) {
                String existingOpinion = opinionSet.get(i);
                //				System.out.println("EntityOpinionMap: \t" + entity.word() + ":\t" + existingOpinion + " && " + currOpinion);
                if (existingOpinion.contains(currOpinion)) {
                    addFlag = false;
                    break;
                } else if (currOpinion.contains(existingOpinion)) {
                    opinionSet.remove(existingOpinion);
                    opinionSet.add(currOpinion);
                    //					System.out.println("1.2: Opinion Adding " + entity.word() + "\t" + currOpinion);
                    addFlag = false;
                    break;
                }
            }
            if (addFlag) {
                //				System.out.println("1.3: Opinion Adding " + entity.word() + "\t" + currOpinion);
                opinionSet.add(currOpinion);
            }
        }
        logger.info("AddToEntityOpinionMap (1) :\t" + entity + "\t" + currOpinion);
        this.entityOpinionMap.put(entity, opinionSet);
    }

    //	public void addToOpinionMapModified(String entity, String dep, String gov) {
    private void addToOpinionMapModified(IndexedWord entity, String dep, String gov) {
        List<String> tempSet = this.entityOpinionMap.get(entity);
        String opinion = (dep + " " + gov).toLowerCase();
        if (tempSet == null) {
            tempSet = new ArrayList<String>();
            tempSet.add(opinion);
            //			System.out.println("1.1: OpinionModified Adding\t" + entity.word() + "\t" + opinion);
        } else {
            int length = tempSet.size();
            boolean addFlag = true;
            for (int i = 0; i < length; i++) {
                String str = tempSet.get(i);
                if (str.contains(gov)) {
                    //					str = str.replaceAll(gov, dep + " " + gov);
                    tempSet.remove(str);
                    str = str.replaceAll(gov, opinion);
                    tempSet.add(str);
                    //					System.out.println("1.2: OpinionModified Adding\t" + entity.word() + "\t" + str);
                    addFlag = false;
                    break;
                }
            }
            if (addFlag) {
                //				System.out.println("1.3: OpinionModified Adding\t" + entity.word() + "\t" + opinion);
                tempSet.add(opinion);
            }
        }
        logger.info("AddToEntityOpinionMap (2) :\t" + entity + "\t" + opinion);
        this.entityOpinionMap.put(entity, tempSet);
    }

    private void addToModifiedOpinionMap(String key, String value) {
        logger.info("Adding to ModifiedOpinionMap: " + key + "\t" + value);
        this.modifiedOpinionMap.put(key, value.toLowerCase());
    }

    private void addToEquivalentEntityMap(String entity, String equivalentEntity) {
        if (equivalentEntity != null) {
            List<String> tempSet = this.equivalentEntityMap.get(entity);
            if (tempSet == null) {
                tempSet = new ArrayList<String>();
            }
            tempSet.add(equivalentEntity);

            List<String> secondSet = this.equivalentEntityMap.get(equivalentEntity);
            if (secondSet != null) {
                tempSet.addAll(secondSet);
            }

            logger.info("Adding Equivalent Entity " + entity + ":\t" + equivalentEntity);
            this.equivalentEntityMap.put(entity, tempSet);
        }
    }

    private void addToMultiWordEntityMap(IndexedWord entity, String multiWordEntity) {
        logger.info("Adding MultiWordEntity " + entity + "\t" + multiWordEntity);

        //		if (this.multiWordEntityMap.containsKey(entity)) {
        //			String existingMultiWordEntity = this.multiWordEntityMap.get(entity);
        //			String prefix =
        //		}

        this.multiWordEntityMap.put(entity, multiWordEntity);
    }

    protected void addToNegatedOpinionMap(String str_negated, String negator, boolean isDirectNegation, boolean checkIsSentimentNullified) {
        // if (negator != null && !negator.equalsIgnoreCase("null")) {
        //		System.out.println("Putting in: " + str_negated + "::" + negator);
        this.negatedOpinionMap.put(str_negated, negator);
        negator = negator.toLowerCase();
        String strNegatedSentiTag = XpSentiment.getLexTag(language, str_negated, wordPOSmap, tagSentimentMap);

        if (checkIsSentimentNullified) {
            if (isDirectNegation) {
				/*It should only be from NEG relation. Fabric does not irritate my skin. It should not be nullified*/
                if (strNegatedSentiTag == null || (strNegatedSentiTag != null && !SentimentFunctions.isPolarFlexibleTag(strNegatedSentiTag))) {
                    this.tagSentimentMap.put(negator, XpSentiment.NON_SENTI_TAG);
                    logger.info("Shifter Effect Nullified: " + negator);
                    //			XpActivator.PW.println(this.text + "\t" + str_negated + "\t" + negator);
                }
                //				else {
                //					logger.info("XX -- Direct negation: shifter not nullified: " + negator);
                //				}

            } else {
				/*Didn't make any noise. In this case it is already nullified because of make. It should be reinstated because of noise
				 This is not taking care of the case - "they did not write the complaint"*/
                if (XpSentiment.NON_SENTI_TAG.equals(this.tagSentimentMap.get(negator))) {
                    //					logger.info("XX -- Negator already nullified: " + negator);
                    //				 if (strNegatedSentiTag != null) {
					/*To discuss with Koustuv*/
                    if (strNegatedSentiTag != null && SentimentFunctions.isPolarFlexibleTag(strNegatedSentiTag)) {
                        this.tagSentimentMap.put(negator, XpSentiment.SHIFTER_TAG);
                        logger.info("Shifter Effect Reinstated: " + negator + " string negated : " + str_negated + " negated senti tag : " + strNegatedSentiTag);
                        //			XpActivator.PW.println(this.text + "\t" + str_negated + "\t" + negator);
                    }
                }
            }
        }

		/*For all general Cases*/
        if (!this.tagSentimentMap.containsKey(negator)) {
            logger.info("XX -- Negator not yet in tagSentimentMap: " + negator);
            this.addToTagSentimentMap(negator, XpSentiment.SHIFTER_TAG);
        }

        logger.info("Adding to NegatedOpinion Map: " + str_negated + "\t" + negator);
        //		System.out.println("TagSentimentMap: " + tagSentimentMap);
        // }
    }

    private void addToTagSentimentMap(String key, String value) {
        if (value != null) {
            logger.info("Adding to TagSentimentMap:\t" + key + "\t" + value);
            this.tagSentimentMap.put(key, value);
        }
    }

    protected void addToTagAspectMap(String key, String[] value) {
        if (value != null) {
            logger.info("Adding to TagAspectMap:\t" + key + "\t" + Arrays.toString(value));
            this.tagAspectMap.put(key.toLowerCase(), value);
        }
    }

    public void negBasedFeaturize(String focusWord) {
        this.featureSet.setNegateFeature(true);

        String focusTag = XpSentiment.getLexTag(language, focusWord, wordPOSmap, tagSentimentMap);
        if (focusTag != null) {
            switch (focusTag) {
                case (XpSentiment.POSITIVE_TAG):
                case (XpSentiment.UNCONDITIONAL_POSITIVE_TAG):
                    this.featureSet.setNegatePosFeature(true);
                    break;
                case (XpSentiment.NEGATIVE_TAG):
                case (XpSentiment.UNCONDITIONAL_NEGATIVE_TAG):
                    this.featureSet.setNegateNegFeature(true);
                    break;
                case (XpSentiment.PPI_TAG):
                    this.featureSet.setNegatePPIFeature(true);
                    break;
                case (XpSentiment.NPI_TAG):
                    this.featureSet.setNegateNPIFeature(true);
                    break;
            }
        }

    }

    public void goeswith_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
    }

    public void ref_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
    }

    public void root_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
    }

    public void xsubj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
    }

    public void acomp_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

        String govWord = gov.value();
        String depWord = dep.value();
        String[] govAspect = XpOntology.findAspect(language, govWord, this.domainName, this.subject, this.xto);
        if (govAspect != null) {
            this.addToTagAspectMap(govWord, govAspect);
        }

        // if it is called from nsubj
        if (v2 != null) {
            String v2Word = v2.value();
            logger.info("ACOMP " + v2Word + " has " + govWord + " qualified by " + depWord);

            String opinion = govWord + " " + depWord;
            // String v4 = getTargetVertex(dep, ADVMOD);
            //			IndexedWord v4 = this.dependencyGraph.getChildWithReln(dep, GrammaticalRelation.valueOf(DependencyRelationsLexicon.ADVMOD));
            IndexedWord v4 = this.dependencyGraph.getChildWithReln(dep, UniversalEnglishGrammaticalRelations.ADVERBIAL_MODIFIER);
            if (v4 != null) {
                String v4Word = v4.value();
                logger.info("ADVMOD: " + depWord + " is qualified by " + v4Word);
                opinion = v4Word + " " + opinion;
            }
            addToOpinionMap(v2, opinion);
        } else {
            addToOpinionMap(gov, depWord);
        }
        needClassifier(gov, dep, edgeRelation);

    }

    public void advcl_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // String opinion = gov + " " + dep;
        logger.info("ADCVL: " + gov + " is advcl to " + dep);
        // clausalSentiClassifier(gov, v2, dep);

		/*interested in buying a camera*/
        IndexedWord dobjDep = this.dependencyGraph.getChildWithReln(dep, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
        if (dobjDep != null) {
            //			this.intentionClassification(dep, dobjDep, edgeRelation, true);
            //			IntentEngine.intentionClassification(dep, dobjDep, edgeRelation, this.dependencyGraph, true, this.featureSet, this.domainName, this.subject, this.negatedOpinionMap, this.tagAspectMap, tagSentimentMap, wordPOSmap, language, multiWordEntityMap);
            IntentEngine.intentionClassification(dep, dobjDep, edgeRelation, this, true, domainName);

        }
    }

    public void discourse_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
    }

    public void possessive_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
    }

    public void number_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
    }

    public void predet_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
    }

    public void punct_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
    }

    public void prt_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

        String govWord = gov.value();
        String depWord = dep.value();
        //		this.addToMultiWordEntityMap(gov, govWord + " " + depWord);
        //		this.addToOpinionMapModified(gov, govWord, depWord);

        this.addToEquivalentEntityMap(govWord, govWord + " " + depWord);

    }

    public void expl_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
    }

    public void mwe_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
    }

    public void tmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

        //	This relation is specific to English; In Spanish nmod
        String govWord = gov.word().toLowerCase();
        String depWord = dep.word().toLowerCase();

        if ((govWord.equals("called")) || (govWord.equals("call")) && depWord.equals("times")) {
            this.tagSentimentMap.put(govWord, XpSentiment.NEGATIVE_TAG);
            logger.info("TMod: " + govWord + "," + depWord + "\t" + "Sentiment Set to Negative of " + govWord);

        }

    }

    public void npmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        logger.info("NpMod: Redirecting to NpAdvMod");
		/*room not available in the hotel.*/
        String govWord = gov.value();
        if (v2 == null) {
            this.addToOpinionMap(dep, govWord);
        }
    }

    public void npadvmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        String govWord = gov.value();
        String depWord = dep.value();
        logger.info("NpAdvMod: " + govWord + "\t" + depWord + "\t" + (v2 == null));
        if (v2 != null) {
            this.addToOpinionMapModified(v2, depWord, govWord);
        } else {
            this.addToModifiedOpinionMap(govWord, depWord + " " + govWord);
        }
    }

    public void nummod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        num_fireRule(gov, v2, dep, edgeRelation);
    }

    public void num_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // System.out.println("Num: " + v1 + " has a quantity " + v3);
        String govWord = gov.word();

        String equivalentEntity = govWord + dep.word();
		/*iPhone 6, Nexus 6*/

        String[] govAspect = XpOntology.findAspect(language, govWord, this.domainName, this.subject, this.xto);

        boolean isDevice = false;
		/*Case of iPhone*/
        if (govAspect != null && govAspect[1].equals("DEVICES") && PosTags.isNumeral(language, dep.tag())) {
            isDevice = true;
        }

        if (!isDevice) {
            govAspect = XpOntology.findAspect(language, equivalentEntity, this.domainName, this.subject, this.xto);
			/*Case of Nexus*/
            if (govAspect != null && govAspect[1].equals("DEVICES") && PosTags.isNumeral(language, dep.tag())) {
                isDevice = true;
            }
        }

        //			String equivalentEntity = govWord + dep.word();
        if (isDevice) {
            logger.info("Num: Full Entity of: " + govWord + "\t" + equivalentEntity);
            this.addToMultiWordEntityMap(gov, equivalentEntity);
            govAspect[0] = equivalentEntity;
            this.addToTagAspectMap(equivalentEntity, govAspect);
        }
    }

    public void poss_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
    }

    public void case_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        String depWord = dep.word();
        if (XpSentiment.SHIFTER_TAG.equals(XpSentiment.getLexTag(language, depWord, wordPOSmap, tagSentimentMap))) {
            logger.info("Case (with Shifter): \t" + gov + "\t" + depWord);
            this.addToNegatedOpinionMap(gov.word(), depWord, true, true);
        }
    }

    public void acl_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

        logger.info("Acl: Initial words:\t" + gov.word() + "\t" + dep.word());

        if (edgeRelation.toString().contains(":")) {
            if (edgeRelation.toString().split(":")[1].equals("relcl")) {
                relcl_fireRule(gov, v2, dep, edgeRelation);
            }
        } else {
			/*waste to go in there*/
			/*Not for relcl cases: And they do not have all the items that you want. */
            //			System.out.println("Dep - " + dep.word() + " tag - " + dep.tag());
            if (PosTags.isCommonNoun(language, gov.tag()) && (PosTags.isVerbBase(language, dep.tag()) && !OntologyUtils.isWeakVerb(language, dep.word()))) {
                String govWord = gov.word().toLowerCase();
                String[] noAspectArr = { govWord, XpOntology.NO_ASPECT_TAG };
                logger.info("Acl: No Aspect Tag:\t" + govWord);
                //					XpActivator.PW.println(this.text + "\t" + govWord);
                this.addToTagAspectMap(govWord, noAspectArr);
            }

			/*Items not found*/
            logger.info("Acl: Redirecting to vmod");
            this.vmod_fireRule(gov, v2, dep, edgeRelation);
        }
    }

    public void advmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // System.out.println("Advmod " + gov + " is qualified by " + dep);

        // String posGov = this.wordPOSmap.get(gov.toLowerCase());
        this.featureSet.setAdvmodFeature(true);

        String depWord = dep.value();
        String govWord = gov.value();
        logger.info("Advmod " + govWord + " is qualified by " + depWord);

        String posGov = gov.tag();
        String posDep = dep.tag();
        if (!VocabularyTests.equalsRude(language, depWord)) {
            // if (posGov.equals("JJ")) {
            // if (!SentiLex.SHIFTER_TAG.equals(SentiLex.getLexTag(dep))) {
            // tagSentimentMap.put(dep, SentiLex.INTENSIFIER_TAG);
            // }
            // } else if (posGov.contains("VB")) {
            // if (SentimentRules.isPolarTag(SentiLex.getLexTag(gov))) {
            // tagSentimentMap.put(dep, SentiLex.INTENSIFIER_TAG);
            // }
            // }

			/*pretty well-> here pretty is an intensifier*/
            //			if ((posGov.equals("JJ") && !SentimentLexicon.SHIFTER_TAG.equals(SentimentLexicon.getLexTag(depWord.toLowerCase()))) || (posGov.contains("VB"))) {

            //			System.out.println("PosGov: " + posGov + "\tPosDep: " + posDep);
            String depSentiTag = XpSentiment.getLexTag(language, depWord, wordPOSmap, tagSentimentMap);
            if (PosTags.isAdjective(language, posGov) && !SentimentFunctions.isFlexibleDescriptor(depSentiTag) && !XpSentiment.SHIFTER_TAG.equals(depSentiTag)) {
                //			if ((posGov.equals("JJ") && !XpSentiment.SHIFTER_TAG.equals(XpSentiment.getLexTag(language, depWord, wordPOSmap, tagSentimentMap)))) {
                //				tagSentimentMap.put(depWord, SentimentLexicon.INTENSIFIER_TAG);
                this.addToTagSentimentMap(depWord, XpSentiment.INTENSIFIER_TAG);
                this.featureSet.setAdvmodIntsfFeature(true);
            } else if (PosTags.isVerbBase(language, posGov) && (PosTags.isComparativeAdjective(language, posDep) || PosTags.isComparativeAdverb(language, posDep))) {
                //				System.out.println("I am inside this Advmod");
				/*You better do this. You better improve*/
                String govLemma = CoreNLPController.lemmatizeToString(language, govWord);
				/*Walmart is better. These type of cases should not exist*/
                if (!VocabularyTests.isBe(language, govLemma)) {
                    //					String depSentiTag = XpSentiment.getLexTag(language, depWord, this.wordPOSmap, this.tagSentimentMap);
                    //					depSentiTag = XpSentiment.getLexTag(language, depWord, this.wordPOSmap, this.tagSentimentMap);
                    if (SentimentFunctions.isPolarTag(depSentiTag)) {
                        logger.info("Advmod SentimentNullifier:\t" + govWord + ", " + depWord + ": Sentiment Nullified");
                        this.tagSentimentMap.put(govWord, XpSentiment.NON_SENTI_TAG);
                        this.tagSentimentMap.put(depWord, XpSentiment.NON_SENTI_TAG);
                        this.featureSet.setAdvmodSentimentNullifierFeature(true);
                    }
                }
            }
            //	TODO: the following is specific to English: Spanish construction entirely different
            if (!this.featureSet.isIntentionFeature() && govWord.toLowerCase().matches("(look[ing]*)") && depWord.toLowerCase().matches("(forward)")) {
                IntentEngine.intentionClassification(gov, dep, edgeRelation, this, true, domainName);
            }

        }

        if (v2 != null) {
            addToOpinionMapModified(v2, depWord, govWord);
        } else {
            if (!PosTags.isAdverb(language, posDep)) {
                addToEquivalentEntityMap(govWord, depWord + " " + govWord);
            }
            if (!(VocabularyTests.isToo(language, depWord) && (!this.text.contains(depWord + " " + govWord)))) {
                //				logger.info("Advmod " + govWord + " is qualified by " + depWord);
                logger.info("Too Rule in advmod");
                this.addToModifiedOpinionMap(govWord, depWord + " " + govWord);
            }
        }

        //		this.emotionVector.dependencyVectorize(gov, dep, edgeRelation.getShortName());
        // }

    }

    public void agent_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // System.out.println("Agent: " + dep + " has done " + gov + " on " +
        // v2);
        logger.info("Agent: " + dep + " has done " + gov + " on " + v2);
        if (v2 != null) {
            addToOpinionMap(dep, gov.value() + " " + v2.word());
        } else {
            addToOpinionMap(dep, gov.value());
        }
    }

    public void amod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // System.out.println("Amod " + gov + " is qualified by " + dep);

        logger.info("AMOD: " + gov + " is qualified by " + dep + " and v2= " + v2);

        // addToOpinionMap(gov, dep);
        String govWord = gov.word().toLowerCase();
        String depWord = dep.word().toLowerCase();
        if (v2 != null) {
            addToOpinionMapModified(v2, depWord, govWord);
        }
        //		else {
        //			// System.out.println("Putting in map" + gov + ">>" + dep);
        //			addToOpinionMap(govWord, depWord);
        //			//			addToOpinionMapModified(govWord, depWord, govWord);sent
        //		}
        this.featureSet.setAmodFeature(true);

        String depSentiTag = XpSentiment.getLexTag(language, depWord, wordPOSmap, tagSentimentMap);
        String govSentiTag = XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap);
        //		if (SentimentFunctions.isPolarTag(govSentiTag) && !SentimentLexicon.UNCONDITIONAL_NEGATIVE_TAG.equals(depSentiTag) && !SentimentLexicon.UNCONDITIONAL_POSITIVE_TAG.equals(depSentiTag)) {

        logger.info("depSentiTag : " + depSentiTag + " govSentiTag : " + govSentiTag);
        if (SentimentFunctions.isPolarTag(govSentiTag)) {
			/*false promises: When there is switch in the sentiment, it should be like a shifter*/
            if (SentimentFunctions.isPolarTag(depSentiTag) && SentimentFunctions.isOppositeSentimentTag(govSentiTag, depSentiTag)) {
                //				logger.info("AMOD: Opposite Sentiment in Gov & Dep\t" + depWord + " gets SHIFTER");
                //				this.addToTagSentimentMap(depWord, SentimentLexicon.SHIFTER_TAG);

				/*false promises, best cheating bank*/
                logger.info("AMOD: Opposite Sentiment in Gov & Dep\t" + govWord + " gets NEGATIVE");
                logger.info("AMOD: Opposite Sentiment in Gov & Dep\t" + depWord + " gets INTENSIFIER");

                this.addToTagSentimentMap(depWord, XpSentiment.INTENSIFIER_TAG);
                //				this.addToTagSentimentMap(govWord, XpSentiment.NEGATIVE_TAG);
                this.addToTagSentimentMap(govWord, SentimentFunctions.switchSentimentTag(govSentiTag));

            } else {
				/*great gem*/
                logger.info("AMOD: Intensification on Gov\t" + depWord + " gets INTENSIFIER");
                this.addToTagSentimentMap(depWord, XpSentiment.INTENSIFIER_TAG);
            }

        }

        //		if (SentimentFunctions.isPolarTag(depTag)) {
        //			if (depTag.contains(SentimentLexicon.POSITIVE_TAG)) {
        //				this.featureSet.setAmodPosFeature(true);
        //			} else if (depTag.contains(SentimentLexicon.NEGATIVE_TAG)) {
        //				this.featureSet.setAmodNegFeature(true);
        //			}
        //			String[] depAspect = XpOntology.findAspect(language, depWord.toLowerCase(), domainName, wordPOSmap);
        //			if (depAspect != null) {
        //				this.featureSet.setAmodAspectFeature(true);
        //				this.addToTagAspectMap(depWord.toLowerCase(), depAspect);
        //			}
        //			this.modifiedOpinionMap.put(govWord, tryEntity);
        //		} else if (this.text.contains(tryEntity)) {
        //			tryEntity = tryEntity.toLowerCase();
        //			String[] aspect = XpOntology.findAspect(language, tryEntity, domainName, wordPOSmap);
        //			/*If it is case like french fries, it should not put into modifiedOpinion Map, else put in food: great food*/
        //			if (aspect != null) {
        //				this.addToMultiWordEntityMap(govWord, tryEntity);
        //				this.featureSet.setAmodAspectFeature(true);
        //				this.addToTagAspectMap(tryEntity, aspect);
        //			}
        //		} else {
        //			this.modifiedOpinionMap.put(govWord, tryEntity);
        //		}

        String multiWordedGov = null;
        if (multiWordEntityMap.containsKey(gov)) {
            multiWordedGov = multiWordEntityMap.get(gov);
        }

        String tryEntity = null;
        if (multiWordedGov != null) {
            tryEntity = depWord + " " + multiWordedGov;
        } else {
            tryEntity = depWord + " " + govWord;
        }
        if (!SentimentFunctions.isPolarTag(depSentiTag)) {
			/*stop word ignore, because don't want same price as entity*/
            if (!ProcessString.isStopWord(Languages.EN, depWord) && !PosTags.isMainVerb(language, dep.tag()) && !SentimentFunctions.isCombinationPolar(govSentiTag, depSentiTag) && XpOntology.getDomainContextualSentimentTag(Languages.EN, govWord, depWord, this.domainName, this.subject, this.xto) == null) {
                if (this.text.toLowerCase().contains(tryEntity)) {
                    tryEntity = tryEntity.toLowerCase();
                    //					System.out.println("Checking with tryEntity from this line: " + tryEntity);
                    String[] aspect = XpOntology.findAspect(language, tryEntity, domainName, subject, this.xto, true, false);
					/*If it is case like french fries, Aspect should be found and the AspectMatch Word needs to be exact, it should not put into modifiedOpinion Map, else put in food: great food*/
                    //					System.out.println("Dhinka chika dhinka chika");
                    logger.info("AMOD (Check with TryEntity): " + tryEntity + "\t" + ((aspect != null) ? Arrays.toString(aspect) : "NULL"));
                    if (aspect != null && aspect[0].equalsIgnoreCase(tryEntity)) {
                        //					if (aspect != null) {
                        //						System.out.println("Adding to MultiWord Entity Map from AMOD: " + gov + "\t" + tryEntity);

                        this.addToMultiWordEntityMap(gov, tryEntity);
                        this.featureSet.setAmodAspectFeature(true);
                        logger.info("AMOD (Case 1): Putting to TagAspect Map: " + tryEntity + "\t" + Arrays.toString(aspect));
                        this.addToTagAspectMap(tryEntity, aspect);
                    } else {
                        logger.info("AMOD (Case 1.1): Putting to ModifiedOpinion Map: " + govWord + "\t" + tryEntity);
                        this.addToModifiedOpinionMap(govWord, tryEntity);
                        //						this.modifiedOpinionMap.put(govWord, tryEntity);
                        this.addToOpinionMap(gov, depWord);
                    }
                }
            } else {
                logger.info("AMOD (Case 1.2): Putting to ModifiedOpinion Map: " + govWord + "\t" + depWord);
                this.addToModifiedOpinionMap(govWord, depWord);
                this.addToOpinionMap(gov, depWord);
            }

        } else {
			/*Should be checked on the governor*/
            String[] govAspect = XpOntology.findAspect(language, govWord, domainName, subject, this.xto);
            if (govAspect != null) {
                this.featureSet.setAmodAspectFeature(true);
				/*Favourite color: gov is color. We should not put favorite with Aspects of Color in tagAspectMap*/
                logger.info("Amod (Case2): Putting to TagAspect Map: " + govWord + "\t" + Arrays.toString(govAspect));
                this.addToTagAspectMap(govWord.toLowerCase(), govAspect);
                //				logger.info("Amod (Case2): Putting to TagAspect Map: " + depWord + "\t" + Arrays.toString(govAspect));
                //				this.addToTagAspectMap(depWord.toLowerCase(), govAspect);
            }
            logger.info("AMOD (Case 2.1): Putting to ModifiedOpinion Map: " + govWord + "\t" + tryEntity);
            //			this.modifiedOpinionMap.put(govWord, tryEntity);
            this.addToModifiedOpinionMap(govWord, depWord);
            this.addToOpinionMap(gov, depWord);

            if (depSentiTag.contains(XpSentiment.POSITIVE_TAG) && !depSentiTag.contains(XpSentiment.POSSESSION_TAG)) {
                this.featureSet.setAmodPosFeature(true);
            } else if (depSentiTag.contains(XpSentiment.NEGATIVE_TAG)) {
                this.featureSet.setAmodNegFeature(true);
            }
        }

        //		this.emotionVector.dependencyVectorize(gov, dep, edgeRelation.getShortName());

        //		String[] depAspect = XpOntology.findAspect(language, depWord.toLowerCase(), domainName, wordPOSmap);
        //		if (depAspect != null) {
        //			this.featureSet.setAmodAspectFeature(true);
        //			this.addToTagAspectMap(depWord.toLowerCase(), depAspect);
        //		}

		/* Don't want to add words like great food in this. */
        //		if (SentimentLexicon.getLexTag(depWord.toLowerCase()) == null) {
        //			String tryEntity = depWord + " " + govWord;
        //			if (this.text.contains(tryEntity)) {
        //				// System.out.println("Equating " + gov + " and " + tryEntity);
        //				logger.info( "Amod Equating " + govWord + " and " + tryEntity);
        //				addToEquivalentEntityMap(govWord, tryEntity); // french fries
        //			}
        //		}

        //		if (depTag == null) {
        //			if (this.text.contains(tryEntity)) {
        //				tryEntity = tryEntity.toLowerCase();
        //				String[] aspect = XpOntology.findAspect(language, tryEntity, domainName, wordPOSmap);
        //				/*If it is case like french fries, it should not put into modifiedOpinion Map, else put in food: great food*/
        //				if (aspect != null) {
        //					this.addToMultiWordEntityMap(govWord, tryEntity);
        //					this.featureSet.setAmodAspectFeature(true);
        //					this.addToTagAspectMap(tryEntity, aspect);
        //				} else {
        //					this.modifiedOpinionMap.put(govWord, tryEntity);
        //				}
        //			}
        //		} else {
        //			String[] depAspect = XpOntology.findAspect(language, depWord.toLowerCase(), domainName, wordPOSmap);
        //			if (depAspect != null) {
        //				this.featureSet.setAmodAspectFeature(true);
        //				this.addToTagAspectMap(depWord.toLowerCase(), depAspect);
        //			}
        //			this.modifiedOpinionMap.put(govWord, tryEntity);
        //		}

    }

    public void quantmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // System.out.println("QuantMod " + gov + " is qualified by " + dep);
        // addToOpinionMap(gov, dep);
        // addToEquivalentEntityMap(gov, dep + " " + gov); //french fries

    }

    public void appos_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // System.out.println("APPOS: " + gov + " is equivalent to " + dep);
        logger.info("APPOS: " + gov + " is equivalent to " + dep);
        addToEquivalentEntityMap(gov.value(), dep.value());
    }

    public void abbrev_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
    }

    public void aux_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

        SentimentEngine.auxilliarySentimentClassifier(language, gov, dep, edgeRelation, dependencyGraph, wordPOSmap, tagSentimentMap, this.featureSet);
        this.questionClassifier(gov, dep);

        if (!this.featureSet.isActionInFuture() && VocabularyTests.isHaveFutureTense(language, dep.word().toLowerCase()))
            this.featureSet.setIsActionInFuture(true);

        //		String govWord = gov.value().toLowerCase();
        //		String depWord = dep.value();
        //		if (depWord.equalsIgnoreCase("could")) {
        //			// String govWord = v1.toLowerCase();
        //			String govTag = SentimentLexicon.getLexTag(govWord);
        //
        //			/*The staff could not be better*/
        //			boolean comparativeAdjectiveCondition = gov.tag().equals("JJR");
        //
        //			/*The staff could not have been more helpful*/
        //			//			IndexedWord advmodDep = this.dependencyGraph.getChildWithReln(gov, GrammaticalRelation.valueOf(DependencyRelationsLexicon.ADVMOD));
        //			IndexedWord advmodDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.ADVERBIAL_MODIFIER);
        //
        //			boolean comparativeAdverbCondition = advmodDep != null && advmodDep.tag().equals("RBR");
        //			if (SentimentFunctions.isPolarTag(govTag) && (comparativeAdjectiveCondition || comparativeAdverbCondition)) {
        //				//			if (SentimentFunctions.isPolarTag(govTag) && (gov.tag().equals("JJR")) {
        //				if (SentimentLexicon.POSITIVE_TAG.equals(govTag) || SentimentLexicon.UNCONDITIONAL_POSITIVE_TAG.equals(govTag)) {
        //					featureSet.setCouldPosFeature(true);
        //				} else if (SentimentLexicon.NEGATIVE_TAG.equals(govTag) || SentimentLexicon.UNCONDITIONAL_NEGATIVE_TAG.equals(govTag)) {
        //					featureSet.setCouldNegFeature(true);
        //				}
        //				String sentiTag = SentimentFunctions.switchSentimentTag(govTag);
        //				logger.info( "Aux: Fired Could: " + govWord + " " + sentiTag);
        //				this.tagSentimentMap.put(govWord, sentiTag);
        //			}
        //		} else if (depWord.equalsIgnoreCase("should")) {
        //			/*They should improve. They should make a note of this. It is suggestion. Incase not a sentiment word follows then Neutral, Else it is negative. Incase no sentiment but is negated, then too it is a negative sentiment*/
        //
        //			String govTag = SentimentLexicon.getLexTag(govWord);
        //			String sentiment = SentimentLexicon.NEUTRAL_SENTIMENT;
        //			if (SentimentFunctions.isPolarTag(govTag) || negatedOpinionHash.containsKey(govWord)) {
        //				sentiment = SentimentLexicon.NEGATIVE_SENTIMENT;
        //				featureSet.setShouldNegFeature(true);
        //				featureSet.setSuggestionPresentFeature(true);
        //			}
        //			logger.info( "Aux: Fired Should: " + govWord + " " + sentiment);
        //			this.tagSentimentMap.put(SentimentLexicon.NEED_ID, sentiment);
        //		}
    }

    public void pcomp_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // System.out.println("Auxpass " + v2 + " is " + v1);
    }

    public void csubjpass_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // System.out.println("Auxpass " + v2 + " is " + v1);
    }

    public void preconj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // System.out.println("Auxpass " + v2 + " is " + v1);
    }

    public void auxpass_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // System.out.println("Auxpass " + v2 + " is " + v1);
    }

    public void cc_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // dep_fireRule(v1, v2, v3, edgeRelation);
        negationEitherClassifier(gov.value(), dep.value());
    }

    public void clausalSentiClassifier(IndexedWord gov, IndexedWord dep) {

        String govWord = gov.word().toLowerCase();
        String depWord = dep.word().toLowerCase();

        String govTag = XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap);
		/*There has been no complaint for the iphone 6 that i have ccomp(complaint, have): Clausal SentiClassifier should not consider this case*/
		/* Edit : Surendranath - 
		 * In the if condition !SentimentFunctions.isPolarTag(depWord) is replaced by !SentimentFunctions.isPolarTag(depTag) 
		 * because isPolarTag has sentiment as argument
		 * */
		/*Should not be considering dep and gov with different pos: eg: wishing to visit kfc once again*/
        if (gov.tag().equals(dep.tag())) {
            String depTag = XpSentiment.getLexTag(language, depWord, wordPOSmap, tagSentimentMap);
            if (!OntologyUtils.isWeakVerb(language, depWord) && SentimentFunctions.isPolarTag(govTag) && LuceneSpell.checkIfCorrectWord(language, depWord) && !SentimentFunctions.isPolarTag(depTag)) {
                logger.info("Clausal Sentiment Classifier:\t" + dep + " gets the Sentiment same as " + gov + " : " + govTag);
                this.addToTagSentimentMap(depWord, govTag);
            }
        }

    }

    public void ccomp_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // System.out.println("cCommp: " + gov + " ccomp " + dep);
        // addToOpinionMap(dep, gov);
        String depWord = dep.value();
        String govWord = gov.value();

        logger.info("Ccomp: " + depWord + " is ccomp to " + govWord);
        needClassifier(gov, dep, edgeRelation);
        // String opinion = govWord + " " + depWord;
        // logger.info( "CCOMP: " + dep + " is ccomp to " + opinion);
        // if (!this.text.toLowerCase().contains(" but ")) {
        if (!VocabularyTests.containsBut(language, text)) {
            // modifiedOpinionMap.put(depWord, opinion);

            String govSentiTag = XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap);
            boolean isGovNoSentimentVerb = OntologyUtils.isWeakVerb(language, govWord) || (govSentiTag == null && PosTags.isVerb(language, gov.tag()));
			/*it's not a seamless well rounded app like citibank.*/
			/*Either weak verb or nullSentiment verb, eg: I donot think this is safe*/
            if (negatedOpinionMap.containsKey(govWord) && (OntologyUtils.isWeakVerb(language, govWord) || isGovNoSentimentVerb)) {
                addToNegatedOpinionMap(depWord, negatedOpinionMap.get(govWord), false, true);
                negBasedFeaturize(depWord);
                //				logger.info("Clausal Sentiment (Negated) Classifier(Case1):\t" + depWord + " gets negated same as " + govWord + "\t" + text);
                logger.info("CComp: (Negated) Classifier(Case1):\t" + depWord + " gets negated same as " + govWord);
            } else if (negatedOpinionMap.containsKey(depWord) && OntologyUtils.isWeakVerb(language, depWord)) {
                addToNegatedOpinionMap(govWord, negatedOpinionMap.get(depWord), false, true);
                negBasedFeaturize(govWord);
                //				logger.info("Clausal Sentiment (Negated) Classifier(Case2):\t" + govWord + " gets negated same as " + depWord + "\t" + text);
                logger.info("CComp: (Negated) Classifier(Case2):\t" + govWord + " gets negated same as " + depWord);
            }
            clausalSentiClassifier(gov, dep);
        }
        negationEitherClassifier(govWord, depWord);
    }

    public void csubj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		/*Hotel is in a old historic building which is a beautiful building and inside is luxury and clean. : Attach building with clean and luxury (csubj)*/
        logger.info("Csubj: " + dep + " csubj " + gov);
        if (!PosTags.isMainVerb(language, dep.tag())) {
            this.addToOpinionMap(dep, gov.word());
        }
    }

    public void conj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // means this has been called from nsubj relation
        String govWord = gov.word();
        String conjunction_type = null;
        String relationName = edgeRelation.toString();
        if (relationName.contains("_")) {
            conjunction_type = relationName.split("_")[1];
        } else if (relationName.contains(":")) {
            conjunction_type = relationName.split(":")[1];
        }
        if (conjunction_type != null) {
            if (VocabularyTests.isBut(language, conjunction_type)) {
                this.featureSet.setShiftedCCPresentFeature(true);
                return;
            } else if (conjunction_type.equals("negcc")) {
                this.addToOpinionMap(dep, VocabularyTests.returnNot(language) + " " + gov.word());
                List<String> govOpinionList = this.entityOpinionMap.get(gov);
                if (govOpinionList != null) {
                    for (String govOpinion : govOpinionList) {
                        this.addToOpinionMap(dep, VocabularyTests.returnNot(language) + " " + govOpinion);
                    }
                }
                //				this.emotionVector.setConjunctionNegCCPresent(true);
            } else if (VocabularyTests.isPlus(language, conjunction_type) && govWord.equalsIgnoreCase("iphone")) {
                String mwEntity = this.multiWordEntityMap.get(gov);
                if (mwEntity != null) {
                    mwEntity += " " + conjunction_type;
                } else {
                    mwEntity = govWord + " " + conjunction_type;
                }
                this.addToMultiWordEntityMap(gov, mwEntity);
            } else {
                this.featureSet.setCCPresentFeature(true);
            }
        }
        String depWord = dep.word();
        //		if (v2 != null) {
        //			// System.out.println("Conj " + v2 + " is " + v1 + " and " + v3);
        //			String v2Word = v2.value();
        //			logger.info("Conj: " + v2Word + " is " + govWord + " and " + depWord);
        //			/*Incase it is a sentence like, I am good and beautiful*/
        //			addToOpinionMap(v2, govWord);
        //			addToOpinionMap(v2, depWord);
        //		} else {

        // System.out.println("Conj " + v1 + " is equivalent to " + v3);
        //		logger.info("Conj: " + govWord + " is equivalent to " + depWord);
        //		addToEquivalentEntityMap(govWord, depWord);
        //		}
        negationEitherClassifier(govWord, depWord);
    }

    public void cop_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		/* means this has been called from nsubj relation*/
        if (v2 != null) {
            //			String v2Word = v2.value();
            //			//			String depWord = dep.value();
            //			String govWord = gov.value().toLowerCase();
            //			logger.info( "COP: " + v2 + " is " + gov);
            //			addToOpinionMap(v2Word, govWord);
            //
            this.featureSet.setNsubjCopFeature(true);
            //			String govTag = SentimentLexicon.getLexTag(govWord);
            //			if (SentimentFunctions.isPolarTag(govTag)) {
            //				if (govTag.contains(SentimentLexicon.POSITIVE_TAG)) {
            //					this.featureSet.setNsubjCopPosFeature(true);
            //				} else if (govTag.contains(SentimentLexicon.NEGATIVE_TAG)) {
            //					this.featureSet.setNsubjCopNegFeature(true);
            //				}
            //			}
            //			String[] govAspect = XpOntology.findAspect(language, govWord, domainName, wordPOSmap);
            //			if (govAspect != null) {
            //				this.addToTagAspectMap(govWord, govAspect);
            //				this.featureSet.setNsubjCopAspectFeature(true);
            //			}
            //

        }

        this.questionClassifier(gov, dep);
    }

    private void questionClassifier(IndexedWord gov, IndexedWord dep) {
        if (this.featureSet.isStartsWithWeakVerbFeature()) {
            IndexedWord nsubjDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.NOMINAL_SUBJECT);
            //			if (nsubjDep != null && this.text.toLowerCase().startsWith(dep.word().toLowerCase())) {
            if (nsubjDep != null && dep.equals(this.dependencyGraph.getNodeByIndexSafe(1))) {

                this.featureSet.setQuestionFeature(true);
				/*Isn't it ridiculous. Isn't it delicious*/
                String govSentiTag = XpSentiment.getLexTag(language, gov.word(), wordPOSmap, tagSentimentMap);
                if (SentimentFunctions.isPolarTag(govSentiTag)) {
                    if (govSentiTag.contains(XpSentiment.POSITIVE_TAG)) {
                        this.featureSet.setQuestionPositiveFeature(true);
                    } else if (govSentiTag.contains(XpSentiment.NEGATIVE_TAG)) {
                        this.featureSet.setQuestionNegativeFeature(true);
                    }

                }
            }
        }
    }

    public void dep_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // System.out.println("Dep " + v2 + " is betw" + v1 + " and " + v3);
        String govWord = gov.word();
        String depWord = dep.word();
        boolean isNegated = negationEitherClassifier(govWord, depWord);
        if (!isNegated) {
            depWord = depWord.toLowerCase();
            String[] aspect = XpOntology.findAspect(language, depWord, domainName, subject, this.xto);
            if (aspect != null) {
				/*This is controversial*/
                //				addToOpinionMap(dep, govWord);
                logger.info("DEP: " + depWord + " is aspect");
                this.addToTagAspectMap(depWord, aspect);
            }
        }
        if (XpIntention.isIntentObjectWord(language, depWord, domainName, subject, xto)) {
            IntentEngine.intentionClassification(gov, dep, edgeRelation, this, false, domainName);
        }
    }

    public void det_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // System.out.println(gov + " has a det with it " + dep);
        // addToEquivalentEntityMap(v1, v3 + " " + v1);
        String depWord = dep.value();
        if (XpSentiment.SHIFTER_TAG.equals(XpSentiment.getLexTag(language, depWord, wordPOSmap, tagSentimentMap))) {
            addToNegatedOpinionMap(gov.value(), depWord, false, true);
        }
    }

    public void dobj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

        String govWord = gov.word();
        String depWord = dep.word();

        logger.info("DOBJ: " + depWord + " is qualified by " + govWord);
        //		this.intentionClassification(gov, dep, edgeRelation, false);
        //		IntentEngine.intentionClassification(gov, dep, edgeRelation, this.dependencyGraph, false, this.featureSet, this.domainName, this.subject, this.negatedOpinionMap, this.tagAspectMap, tagSentimentMap, wordPOSmap, language, multiWordEntityMap);
        //		IntentEngine.intentionClassification(gov, dep, edgeRelation, this.dependencyGraph, false, this.featureSet, this.domainName, this.subject, this.negatedOpinionMap, this.tagAspectMap, tagSentimentMap, wordPOSmap, language, multiWordEntityMap);
        IntentEngine.intentionClassification(gov, dep, edgeRelation, this, false, domainName);

        String opinion = govWord;

        IndexedWord depDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.valueOf("dep"));

        if (v2 != null) {
            String v2Word = v2.value();

            String[] v2Aspect = XpOntology.findAspect(language, v2Word, this.domainName, this.subject, this.xto);
            if (v2Aspect != null) {
                this.addToOpinionMap(v2, govWord + " " + depWord);
                this.addToTagAspectMap(v2Word, v2Aspect);
            } else {
                System.out.println("Entered here 1");
                //				IndexedWord depDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.valueOf("dep"));
                //				List<IndexedWord> ddList = this.dependencyGraph.getChildList(gov);
                //				for (IndexedWord c : ddList) {
                //					System.out.println(this.dependencyGraph.getEdge(gov, c));
                //				}
                if (depDep != null) {
                    System.out.println("Entered here 2");
					/*Incase related by dep*/
                    String opStr = govWord + " " + depDep.word().toLowerCase();
                    this.addToOpinionMap(dep, opStr);
                } else {
                    this.addToOpinionMap(dep, govWord);
                }
            }
        } else {
            // String v4 = getTargetVertex(dep, RCMOD);
            //			IndexedWord v4 = this.dependencyGraph.getChildWithReln(dep, GrammaticalRelation.valueOf(DependencyRelationsLexicon.RCMOD));
            IndexedWord v4 = this.dependencyGraph.getChildWithReln(dep, UniversalEnglishGrammaticalRelations.RELATIVE_CLAUSE_MODIFIER);
            if (v4 != null) {
                String v4Word = v4.value();
                logger.info("RCMOD: " + depWord + " which is  " + v4Word);
                opinion += " " + v4Word;
            } else if (depDep != null) {
                opinion = govWord + " " + depDep.word().toLowerCase();
            }

            addToOpinionMap(dep, opinion);
        }
        boolean isClassified = needClassifier(gov, dep, edgeRelation);
        if (!isClassified) {
            SentimentEngine.possessionClassifier(gov, dep, edgeRelation, this);
        }

		/* don't expect good service*/
        if (PosTags.isMainVerb(language, gov.tag())) {
            if (negatedOpinionMap.containsKey(govWord)) {
                addToNegatedOpinionMap(depWord, negatedOpinionMap.get(govWord), false, true);
            }
        }

        String govSentiment = XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap);
        //		String depSentiment = SentimentLexicon.getLexTag(depWord.toLowerCase());

        if (SentimentFunctions.isPolarTag(govSentiment)) {
            if (govSentiment.contains(XpSentiment.POSITIVE_TAG)) {
                this.featureSet.setDobjPosFeature(true);
                //				if (depSentiment != null) {
                //					if (depSentiment.contains(SentimentLexicon.NEGATIVE_TAG)) {
                //						this.tagSentimentMap.put(govWord.toLowerCase(), SentimentLexicon.NEGATIVE_TAG);
                //						XpActivator.PW.println(this.text + "\tDobj\tPOS#NEG = Negative\t" + govWord + "#" + depWord);
                //					}
                //				}
            } else if (govSentiment.contains(XpSentiment.NEGATIVE_TAG)) {
                this.featureSet.setDobjNegFeature(true);
                //				if (depSentiment != null) {
                //					if (depSentiment.contains(SentimentLexicon.POSITIVE_TAG)) {
                //						this.tagSentimentMap.put(depWord.toLowerCase(), SentimentLexicon.NEGATIVE_TAG);
                //						XpActivator.PW.println(this.text + "\tDobj\tNEG#POS = Positive\t" + govWord + "#" + depWord);
                //					} else if (depSentiment.contains(SentimentLexicon.NEGATIVE_TAG)) {
                //						this.tagSentimentMap.put(depWord.toLowerCase(), SentimentLexicon.POSITIVE_TAG);
                //						this.tagSentimentMap.put(govWord.toLowerCase(), SentimentLexicon.POSITIVE_TAG);
                //						XpActivator.PW.println(this.text + "\tDobj\tNEG#NEG = Positive\t" + govWord + "#" + depWord);
                //					}
                //				}
            }
        }
        //		this.emotionVector.dobjVectorize(gov, dep, edgeRelation.getShortName());
    }

    public void iobj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        String govWord = gov.value();
        String depWord = dep.value();

        logger.info("IOBJ: " + depWord + " is recipient of " + govWord);
        addToOpinionMap(dep, govWord);
    }

    public void neg_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

        String govWord = gov.value();
        String depWord = dep.value();

		/*Words can not express my disgust*/
        //		if (govWord.equalsIgnoreCase("express")) {
        //			IndexedWord dobjDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
        //			if (dobjDep != null) {
        //				logger.info("Negation on express:\t" + depWord);
        //				this.addToTagSentimentMap(depWord.toLowerCase(), SentimentLexicon.NON_SENTI_TAG);
        //			}
        //		}

        boolean checkIsSentimentNullified = true;
        IndexedWord amodDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.ADJECTIVAL_MODIFIER);
        if (amodDep == null) {
            IndexedWord dobjDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
            if (dobjDep != null) {
				/*I did not eat good food*/
                logger.info("NEG With a Word with DOBJ on AMOD: " + govWord);
                amodDep = this.dependencyGraph.getChildWithReln(dobjDep, UniversalEnglishGrammaticalRelations.ADJECTIVAL_MODIFIER);
                //				if (language == Languages.SP && XpSentiment.getLexTag(language, dobjDep.word(), wordPOSmap, tagSentimentMap) != null)
                checkIsSentimentNullified = false;
            } else {
                IndexedWord copGov = this.dependencyGraph.getParent(gov);
                if (copGov != null) {
                    logger.info("NEG with a word COP on AMOD: " + govWord);
                    amodDep = this.dependencyGraph.getChildWithReln(copGov, UniversalEnglishGrammaticalRelations.ADJECTIVAL_MODIFIER);
                    if (amodDep == null) {
                        amodDep = copGov;
                        logger.info("XX -- COP used for sentiment: " + amodDep);
                    }
                }
            }
        }
        IndexedWord advDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.ADVERBIAL_MODIFIER);
        IndexedWord xcompDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.XCLAUSAL_COMPLEMENT);
        IndexedWord nsubjDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.NOMINAL_SUBJECT);
        IndexedWord ccompDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.CLAUSAL_COMPLEMENT);
        Set<IndexedWord> nmodDepList = this.dependencyGraph.getChildrenWithRelns(gov, UniversalEnglishGrammaticalRelations.getNmods());
        if (amodDep != null && XpSentiment.getLexTag(language, amodDep.word(), wordPOSmap, tagSentimentMap) != null) {
			/*This is not a good car: In this case we don't want to nullify the shifter*/
			/*it was not a good match*/
            logger.info("NEG With a Word on SentimentBearing AMOD: " + govWord);
            checkIsSentimentNullified = false;
			/*Added 4 lines, commented out previous line*/
            // logger.info("NEG With a Word on SentimentBearing AMOD: " + govWord + "\t" + amodDep.word());
            // checkIsSentimentNullified = false;
            // addToNegatedOpinionMap(amodDep.word(), depWord, true, checkIsSentimentNullified);
            // negBasedFeaturize(amodDep.word().toLowerCase());
        } else if (advDep != null && XpSentiment.getLexTag(language, advDep.word(), wordPOSmap, tagSentimentMap) != null) {
			/*I didn't eat well.*/
            logger.info("NEG With a Word on SentimentBearing ADVMOD: " + govWord);
            checkIsSentimentNullified = false;
        } else if (xcompDep != null && XpSentiment.getLexTag(language, xcompDep.word(), wordPOSmap, tagSentimentMap) != null) {
			/*Those sunglasses never looked good on you*/
            logger.info("NEG With a Word on SentimentBearing XCOMP: " + govWord);
            checkIsSentimentNullified = false;
        } else if (ccompDep != null && XpSentiment.getLexTag(language, ccompDep.word(), wordPOSmap, tagSentimentMap) != null) {
			/*i do not think online banking is safe.*/
            logger.info("NEG With a Word on SentimentBearing CCOMP: " + govWord);
            checkIsSentimentNullified = false;
        } else if (nmodDepList != null) {
            //			else if (nmodDepList != null && XpSentiment.getLexTag(language, nmodDep.word(), wordPOSmap, tagSentimentMap) != null) {
			/*Order was not delivered at fast pace*/
            logger.info("NEG With a Word on NMOD: " + govWord);
            checkIsSentimentNullified = false;
        } else if (nsubjDep != null && PosTags.isMainVerb(language, gov.tag()) && XpSentiment.getLexTag(language, nsubjDep.word(), wordPOSmap, tagSentimentMap) != null) {
			/*help never came*/
            logger.info("NEG With a Word on SentimentBearing NSUBJ: " + govWord);
            checkIsSentimentNullified = false;
        } else if (this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.DETERMINER) != null) {
			/*Not a good car*/
            logger.info("NEG With a Word on Determiner: " + govWord);
            checkIsSentimentNullified = false;
        } else if (OntologyUtils.isWeakVerb(language, govWord)) {
			/*it is not a seamless well rounded app like citibank.*/
            logger.info("NEG With a Weak Verb: " + govWord);
            checkIsSentimentNullified = false;
        } else if (XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap) != null && SentimentFunctions.isPolarTag(XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap))) {
            logger.info("NEG With a Polar Verb: " + govWord);
            checkIsSentimentNullified = false;
        } else {

			/*Food is not very good*/
            Set<IndexedWord> advmodGovList = this.dependencyGraph.getParentsWithReln(gov, UniversalEnglishGrammaticalRelations.ADVERBIAL_MODIFIER);
            if (advmodGovList != null && !advmodGovList.isEmpty()) {
                logger.info("NEG With a Word on AdvMod: " + govWord);
                checkIsSentimentNullified = false;
            }
        }
        logger.info("NEG: " + govWord + "\t checkIsSentimentNullified : " + checkIsSentimentNullified);
        addToNegatedOpinionMap(govWord, depWord, true, checkIsSentimentNullified);
        negBasedFeaturize(govWord.toLowerCase());

        //		this.emotionVector.negationVectorize(gov, dep, edgeRelation.getShortName());
    }

    public void name_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
    }

    public void compound_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        String relationName = edgeRelation.toString();
        if (relationName.contains(":")) {
            if (relationName.split(":")[1].equals("prt")) {
                prt_fireRule(gov, v2, dep, edgeRelation);
            }
        } else {
            nn_fireRule(gov, v2, dep, edgeRelation);
        }
    }

    public void nn_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        //		String govWord = gov.value().toLowerCase();
        //		String entity = gov.value().toLowerCase();
        //		String entity = gov.value();
        //		System.out.println("EdgeRelation: " + edgeRelation);

        //		IndexedWord prefix = this.dependencyGraph.getChildWithReln(gov, edgeRelation);
        //		if (prefix != null) {
        //			String prefixWord = prefix.word();
        //			System.out.println("Prefix: " + prefixWord);
        //			entity = prefixWord + " " + entity;
        //		}
        //		IndexedWord prefix2 = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.NOUN_COMPOUND_MODIFIER);
        //		if (prefix2 != null) {
        //			System.out.println("Prefix2: " + prefix2);
        //			entity = prefix2.word() + " " + entity;
        //		}

        if (!this.multiWordEntityMap.containsKey(gov)) {
            String govWord = gov.value();
            StringBuilder netEntity = new StringBuilder();
            //		System.out.println("Gov: " + gov);
            Set<IndexedWord> prefixList = this.dependencyGraph.getChildrenWithReln(gov, edgeRelation);
            if (prefixList != null) {
                //			int i = 0;
                for (IndexedWord prefix : prefixList) {
                    netEntity.append(prefix.word()).append(" ");
                }
            }
            String entity = netEntity.append(govWord).toString();
            logger.info("NN: Full Entity of " + govWord + " is " + entity);
            this.addToMultiWordEntityMap(gov, entity);
            entity = entity.toLowerCase();
            govWord = govWord.toLowerCase();

            String depWord = dep.word().toLowerCase();
            if (depWord.equals("killer")) {
                //			this.tagSentimentMap.put("killer", SentimentLexicon.POSITIVE_TAG);
                this.addToTagSentimentMap("killer", XpSentiment.POSITIVE_TAG);
            } else if (depWord.equals("cheating")) {
				/*cheating bank*/
                this.addToTagSentimentMap(govWord, XpSentiment.NEGATIVE_TAG);
            } else {
                String sentiTag = XpSentiment.getLexTag(language, entity, wordPOSmap, tagSentimentMap);
                if (sentiTag == null) {
                    /**
                     * For cases like "perfect house with bad road connectivity" or "great house there are many facilities near it but sometimes water is just disgusting"
                     * where "great house" or "perfect house" are recognized as compound words
                     */
                    String depTag = XpSentiment.getLexTag(language, depWord, wordPOSmap, tagSentimentMap);
                    if (depTag != null && !nerMap.containsKey(depWord))
                        sentiTag = depTag;
                    else
                        sentiTag = XpSentiment.NON_SENTI_TAG;
                }
                this.addToTagSentimentMap(entity, sentiTag);
                logger.info("NN: Sentiment Tag of " + entity + " :\t" + sentiTag);
            }

            //			String[] govAspect = this.tagAspectMap.get(govWord);

			/*Don't look for paragon in Siam Paragon*/
            if (!nerMap.containsKey(entity)) {
                String[] aspect = XpOntology.findAspect(language, entity, domainName, subject, this.xto, true, false);

                if (aspect == null) {
                    aspect = XpOntology.findAspect(language, govWord, domainName, subject, this.xto);
                }
                if (aspect == null) {
                    aspect = XpOntology.findAspect(language, depWord, domainName, subject, this.xto);
                }
                if (aspect != null && !aspect[1].equals(XpOntology.NO_ASPECT_TAG)) {
                    //				String[] aspectArr = this.tagAspectMap.get(govWord);
                    aspect[0] = entity;
                    this.addToTagAspectMap(entity, aspect);
                    //				this.tagAspectMap.put(entity, aspectArr);
                }
            }
        }
    }

    public void relcl_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        logger.info("RelCl: Redirecting to rcmod");
        rcmod_fireRule(gov, v2, dep, edgeRelation);
    }

    public void rcmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // System.out.println("RCmod: " + gov + " is " + dep);
        logger.info("RCMOD: " + gov + " is " + dep);
        // System.out.println("Since found rcmod will loop on v3 now");
        String govWord = gov.value();
        String depWord = dep.value();
        addToOpinionMap(gov, depWord);
        // if (v2 != null) {
        // // addToOpinionMap(v2, dep + " " + gov);
        // addToOpinionMapModified(gov, gov, dep);
        // } else {
        // addToOpinionMap(gov, dep);
        // }
        //		IndexedWord v4 = this.dependencyGraph.getChildWithReln(dep, GrammaticalRelation.valueOf(DependencyRelationsLexicon.NSUBJ));
        IndexedWord v4 = this.dependencyGraph.getChildWithReln(dep, UniversalEnglishGrammaticalRelations.NOMINAL_SUBJECT);
        String v4Word;
        if (v4 == null) {
            //			v4 = this.dependencyGraph.getChildWithReln(dep, GrammaticalRelation.valueOf(DependencyRelationsLexicon.NSUBJPASS));
            v4 = this.dependencyGraph.getChildWithReln(dep, UniversalEnglishGrammaticalRelations.NOMINAL_PASSIVE_SUBJECT);
        }
        if (v4 != null) {
            v4Word = v4.value();
            logger.info("RCMOD: " + govWord + " is RCMOD equivalent to " + v4Word);
            addToEquivalentEntityMap(govWord, v4Word);
        }

    }

    public void parataxis_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
    }

    public void nmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        System.out.println("Entered nmod: " + gov + "\t" + dep);

        String relationName = edgeRelation.toString();
        boolean isPrepRelation = true;
        if (relationName.contains(":")) {
            String nextRelName = relationName.split(":")[1];
            switch (nextRelName) {
                case "tmod":
                    this.tmod_fireRule(gov, v2, dep, edgeRelation);
                    isPrepRelation = false;
                    break;
                case "npmod":
                    this.npmod_fireRule(gov, v2, dep, edgeRelation);
                    isPrepRelation = false;
                    break;
                case "poss":
                    this.poss_fireRule(gov, v2, dep, edgeRelation);
                    isPrepRelation = false;
                    break;
                case "agent":
                    this.agent_fireRule(gov, v2, dep, edgeRelation);
                    isPrepRelation = false;
                    break;
            }
        }
        if (isPrepRelation) {
            prep_fireRule(gov, v2, dep, edgeRelation);
        }
    }

    public void prep_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // System.out.println("Prep_: " + v2 + " has done " + v1 + " on " + v3);
        // addToOpinionMap(v2, v1 + " " + v3);
        // addToOpinionMap(v3, v1);
        //		System.out.println("Entered prepFireRule: " + gov + "\t" + dep);
        String relationName = edgeRelation.toString();
        String govWord = gov.value();
        String depWord = dep.value();

		/* fluctuations in price"-> prep_in(fluctuations, price)*/
        if (relationName.contains("_") || relationName.contains(":")) {
            //			System.out.println("Entered ifCondition: " + gov + "\t" + dep + "\t" + v2 + "\t" + relationName);

            String relation = findPrepRelation(relationName);
            prepSentiNullifier(relation);

			/*Need to understand the reason behind this "than" classification: Fails in cases like this is worse than the old irctc site*/
            //			if (relation.equals("than")) {
            //				String govSentiTag = SentimentLexicon.getLexTag(language, govWord, wordPOSmap, tagSentimentMap);
            //				if (SentimentFunctions.isPolarTag(govSentiTag)) {
            //					this.tagSentimentMap.put(govWord, SentimentFunctions.switchSentimentTag(govSentiTag));
            //					logger.info("PREP (Case 1.0): Switched Senitment tag of " + govWord + " from " + govSentiTag);
            //				}
            //			}
            if (v2 != null) {
                String v2Word = v2.value();
                logger.info("PREP (Case 1.1): " + v2Word + " has done " + govWord + " " + relation + " " + depWord);
                String opinion = govWord + " " + relation + " " + depWord;
                addToOpinionMap(v2, opinion);

                String[] depAspect = XpOntology.findAspect(language, depWord, this.domainName, this.subject, this.xto);
                if (depAspect != null) {
                    logger.info("PREP (Case 1.2): " + depWord + " has mention about " + v2.word());
                    this.addToOpinionMap(dep, v2.word());
                    this.addToTagAspectMap(depWord, depAspect);
                }

            } else {
				/*plan for cheap*/
                if (PosTags.isNoun(language, gov.tag()) && PosTags.isAdjective(language, dep.tag())) {
                    logger.info("PREP (Case 2.1): " + depWord + " prep_" + relation + " " + govWord);
                    addToOpinionMap(gov, depWord);
                } else {
                    IndexedWord dobjChild = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
                    if (dobjChild != null) {
                        String dobjChildWord = dobjChild.word().toLowerCase();
                        String[] aspect = XpOntology.findAspect(language, dobjChildWord, domainName, subject, this.xto);
                        if (aspect != null) {
							/*delivered my food on time : here food has aspect, so it becomes entity*/
                            logger.info("PREP (Case 2.2) (Dobj with nmod): " + depWord + " prep_" + relation + " " + govWord);
                            this.addToOpinionMap(dobjChild, govWord + " " + relation + " " + depWord);
                            this.addToTagAspectMap(dobjChildWord, aspect);
                            this.addToOpinionMap(dep, govWord);
                        } else {
							/*liked lifter for luggage: here lifter has no aspect, so luggage becomes entity*/
                            logger.info("PREP (Case 2.2) (nmod with dobj): " + depWord + " prep_" + relation + " " + govWord);
                            this.addToOpinionMap(dep, govWord + " " + relation + " " + dobjChildWord);
                        }

                    } else {
                        logger.info("PREP (Case 2.4): " + depWord + " prep_" + relation + " " + govWord);
                        addToOpinionMap(dep, govWord);
                    }
                }
            }
			/*I am looking for a phone | I want to invest in a mutual fund*/
            //			if (gov.tag().contains("VB") && relation.equals("for")) {
            //			System.out.println("POS tag - " + gov.tag() + " intent feature - " + featureSet.isIntentionFeature());
            if (PosTags.isMainVerb(language, gov.tag()) && !featureSet.isIntentionFeature()) {
                //				this.intentionClassification(gov, dep, edgeRelation, true);
                //				IntentEngine.intentionClassification(gov, dep, edgeRelation, this.dependencyGraph, true, this.featureSet, this.domainName, this.subject, this.negatedOpinionMap, this.tagAspectMap, tagSentimentMap, wordPOSmap, language, multiWordEntityMap);
                //				IntentEngine.intentionClassification(gov, dep, edgeRelation, this, true);
                IntentEngine.intentionClassification(gov, dep, edgeRelation, this, true, domainName);

            } else if (PosTags.isNoun(language, gov.tag()) && !featureSet.isIntentionFeature())
                IntentEngine.intentionClassification(gov, dep, edgeRelation, this, true, domainName);

			/*waste of time*/
            if (PosTags.isNoun(language, gov.tag()) && OntologyUtils.isAbstractConcept(language, depWord)) {
                String[] noAspectArr = { govWord, XpOntology.NO_ASPECT_TAG };
                logger.info("No Aspect Tag:\t" + govWord);
                //					XpActivator.PW.println(this.text + "\t" + govWord);
                this.addToTagAspectMap(govWord, noAspectArr);
            }
			/*This place is in need of good hotel -> nmod:of()*/
            needClassifier(gov, dep, edgeRelation);

        } else {
            if (v2 != null) {
                String v2Word = v2.value();
                logger.info("PREP (Case 3): " + v2Word + " has done " + govWord + " on " + depWord);
                addToOpinionMap(v2, govWord + " " + depWord);
            } else {
                logger.info("PREP (Case 3.1): " + depWord + " with preposition " + govWord);
                addToOpinionMap(dep, govWord);
            }
        }
        //		needClassifier(gov, dep, edgeRelation);
    }

    private String findPrepRelation(String edgeRelation) {
        String[] relationArr = null;
        if (edgeRelation.contains("_")) {
            relationArr = edgeRelation.split("_");
        } else if (edgeRelation.contains(":")) {
            relationArr = edgeRelation.split(":");
        }
        String relation = "";
        if (relationArr != null) {
            for (int i = 1; i < relationArr.length; i++) {
                relation += " " + relationArr[i];
            }
            relation = relation.trim();
        }
        return relation;
    }

    public void prepc_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        String relationName = edgeRelation.toString();
        String govWord = gov.value();
        String depWord = dep.value();

        if (relationName.contains("_") || relationName.contains(":")) {
            String relation = findPrepRelation(relationName);
            prepSentiNullifier(relation);

            String opinion = relation + " " + depWord;
            if (v2 != null) {
                logger.info("PrepC: " + v2 + " has done " + govWord + " " + relation + " " + depWord);
                addToOpinionMap(v2, govWord + " " + opinion);
            } else {
                addToOpinionMap(gov, opinion);
            }
        }
    }

    public void pobj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        String govWord = gov.value();
        String depWord = dep.value();

        if (v2 != null) {
            String v2Word = v2.value();
            logger.info("Pobj: " + v2Word + " has done " + govWord + " on " + depWord);
            addToOpinionMap(v2, govWord + " " + depWord);
        } else {
            addToOpinionMap(dep, govWord);
            // parking is quite a pain though
            if (SentimentFunctions.isPolarTag(XpSentiment.getLexTag(language, depWord, wordPOSmap, tagSentimentMap))) {
                String modOpnVal = this.modifiedOpinionMap.get(govWord);
                String key;
                if (modOpnVal != null) {
                    key = depWord;
                } else {
                    key = govWord;
                }
                String opinion = govWord + " " + depWord;
                this.addToModifiedOpinionMap(key, opinion);
                //				this.modifiedOpinionMap.put(key, opinion);
                logger.info("Pobj: " + key + " " + opinion);
            }
        }
        if (XpSentiment.SHIFTER_TAG.equals(XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap))) {
            // negatedOpinionHash.add(v3);
            addToNegatedOpinionMap(depWord, negatedOpinionMap.get(govWord), false, true);
        }
    }

    public void mark_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        logger.info("Mark:\t" + gov + "\t" + dep);
        SentimentEngine.markerSentimentClassifier(language, gov, dep, edgeRelation, dependencyGraph, wordPOSmap, tagSentimentMap, this.featureSet);
    }

    public void vmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

        logger.info("VMod: " + gov + "\t" + dep);
        this.addToOpinionMap(gov, dep.value());
        // System.out.println("vMod: " + gov + " vmod " + dep);
        // addToOpinionMap(v1, v3);
        // people working here are rude

    }

    public void infmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // System.out.println("vMod: " + gov + " vmod " + dep);
        // addToOpinionMap(v1, v3);
        // people working here are rude

    }

    public void partmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        // System.out.println("vMod: " + gov + " vmod " + dep);
        // addToOpinionMap(v1, v3);
        // people working here are rude

    }

    public void xcomp_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

        String govWord = gov.word();
        String depWord = dep.word();

        logger.info("xComp: " + govWord + " to " + depWord);
        // because it is clausal so v2 becomes the agent here rather than v1
        // if (v2 != null) {
        // iterativeMethodsCall(dep, v2, edgeRelation);
        // }
        boolean isClassified = false;
        //		if (v2 != null) {
        //			logger.info("v2 : " + v2.value());
        //			//			iterativeMethodsCall(dep, v2, edgeRelation);
        //			//			addToOpinionMap(v2, opinion);
        //		}
        //		else {
        //		}

        //			IndexedWord v4 = this.dependencyGraph.getChildWithReln(dep, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);

        IndexedWord dobjDep = this.dependencyGraph.getChildWithReln(dep, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);

		/* They need to have good employees: Employees: Need to have*/
        if (dobjDep != null) {
            this.checkedEdge.add(dependencyGraph.getEdge(dep, dobjDep, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT));
            String opinion = govWord + " to " + depWord;
            //			System.out.println("xcomp opinion - " + opinion);
            logger.info("DOBJ from XCOMP: " + dobjDep + " is qualified by " + opinion);
            this.addToOpinionMap(dobjDep, opinion);
            //				if (govWord.equalsIgnoreCase("have") || govWord.equalsIgnoreCase("has")) {
            //					govWord = "need";
            //				}
            isClassified = needClassifier(gov, dobjDep, edgeRelation);
            //			this.intentionClassification(dep, dobjDep, edgeRelation, true);

            if (XpIntention.isIntentActionWord(language, govWord, domainName, subject, this.xto, true)) {
                //				IntentEngine.intentionClassification(dep, dobjDep, edgeRelation, this.dependencyGraph, true, this.featureSet, this.domainName, this.subject, this.negatedOpinionMap, this.tagAspectMap, tagSentimentMap, wordPOSmap, language, multiWordEntityMap);
                //				IntentEngine.intentionClassification(dep, dobjDep, edgeRelation, this, true);
                IntentEngine.intentionClassification(dep, dobjDep, edgeRelation, this, true, domainName);

            }

            if (!this.getFeatureSet().isIntentionFeature()) {
                IntentEngine.intentionClassification(gov, dep, edgeRelation, this, true, domainName);
            }

        } else {

			/*e.g. I want to open an account with your bank.*/
            IndexedWord cCompDep = this.dependencyGraph.getChildWithReln(dep, UniversalEnglishGrammaticalRelations.CLAUSAL_COMPLEMENT);
            if (cCompDep != null) {
                String cCompWord = cCompDep.word();

                if (XpIntention.isIntentActionWord(language, govWord, domainName, subject, this.xto, true) && XpIntention.isIntentObjectWord(language, cCompWord, domainName, subject, this.xto)) {
                    IntentEngine.intentionClassification(gov, dep, edgeRelation, this, false, domainName);
                    //						IntentEngine.intentionClassification(dep, cCompDep, edgeRelation, this.dependencyGraph, true, this.featureSet, this.domainName, this.subject, this.negatedOpinionHash, this.tagAspectMap, tagSentimentMap, wordPOSmap, language, multiWordEntityMap);
                }
            }

            addToOpinionMap(dep, govWord);
        }

        this.negationEitherClassifier(govWord, depWord);
        this.clausalSentiClassifier(gov, dep);

        if (VocabularyTests.isHavePresentTense(language, govWord)) {

            IndexedWord markDep = this.dependencyGraph.getChildWithReln(dep, UniversalEnglishGrammaticalRelations.MARKER);
            if (markDep != null && VocabularyTests.isHaveToMark(language, markDep.word()))

            {
				/*has to improve -> Neutralizing the sentiment of improve*/
                logger.info("Xcomp with Mark: Sentiment neutralized:\t" + dep);
                this.featureSet.setSuggestionPresentFeature(true);
                this.featureSet.setNeedFeature(true);
                this.addToTagSentimentMap(depWord, SentimentFunctions.switchSentimentTag(XpSentiment.getLexTag(language, depWord, wordPOSmap, tagSentimentMap)));
            }

        }

        if (!isClassified) {
            isClassified = this.needClassifier(gov, dep, edgeRelation);
        }
        if (!isClassified) {
            //			isClassified = SentimentEngine.possessionClassifier(language, gov, dep, edgeRelation, dependencyGraph, wordPOSmap, tagSentimentMap, this.featureSet, negatedOpinionMap, domainName, subject, this.multiWordEntityMap, this.tagAspectMap);
            isClassified = SentimentEngine.possessionClassifier(gov, dep, edgeRelation, this);
        }

    }

    public void prepSentiNullifier(String relation) {
		/*if prep_like. We are setting NonSentimentTag for "like"*/
        String relationTag = XpSentiment.getLexTag(language, relation, wordPOSmap, tagSentimentMap);
        if (XpSentiment.POSITIVE_TAG.equals(relationTag) || XpSentiment.NEGATIVE_TAG.equals(relationTag)) {
            this.addToTagSentimentMap(relation, XpSentiment.NON_SENTI_TAG);
        }
    }

    private boolean needClassifier(IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation) {

        //		String sentiment = SentimentEngine.needClassifier(gov, dep, edgeRelation, dependencyGraph, tagSentimentMap, featureSet, subject);

        //		String govWord = gov.value().toLowerCase();
        //		if (SentimentLexicon.NEED_TAG.equals(SentimentLexicon.getLexTag(govWord))) {
        //			String depWord = dep.value().toLowerCase();
        //			logger.info( "Fired needClassifier: " + govWord + " " + depWord);
        //
        //			/*Incase the first person exists, like We need more information, these kind of things should be neutral. But for cases like, I hope that they improve, this should be like normal needClassification*/
        //			IndexedWord nsubjDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.NOMINAL_SUBJECT);
        //			if (nsubjDep != null && !govWord.equals("hope")) {
        //				String nsubjDepWord = nsubjDep.word();
        //				if (nsubjDepWord.equalsIgnoreCase("we") || nsubjDepWord.equalsIgnoreCase("i")) {
        //					return false;
        //				}
        //			}
        //
        //			this.featureSet.setNeedFeature(true);
        //
        //			String sentiment = null;
        //			if (!depWord.equalsIgnoreCase(subject)) {
        //				//				System.out.println("I am here!!");
        //
        //				/*If it is not an aspect then too, it should be negative. For eg: They need to do this*/
        //				sentiment = SentimentLexicon.NEGATIVE_SENTIMENT;

        logger.info("NeedClassifier Called:\t" + gov + "\t" + dep);
        //		String sentiment = SentimentEngine.needClassifier(language, gov, dep, edgeRelation, dependencyGraph, wordPOSmap, tagSentimentMap, featureSet, subject, negatedOpinionMap);
        String sentiment = SentimentEngine.needClassifier(gov, dep, edgeRelation, this);

        if (sentiment != null) {
            String depWord = dep.word().toLowerCase();
            String[] aspect = XpOntology.findAspect(language, depWord, domainName, subject, this.xto);
            if (aspect != null) {
                this.addToTagSentimentMap(aspect[1], sentiment);
                this.addToTagAspectMap(depWord, aspect);
            }
            return true;
        }
        return false;
    }

    //	public boolean needClassifier(IndexedWord gov, IndexedWord dep, GrammaticalRelation edgeRelation) {
    //		String govWord = gov.value().toLowerCase();
    //		if (SentimentLexicon.NEED_TAG.equals(SentimentLexicon.getLexTag(govWord))) {
    //			String depWord = dep.value().toLowerCase();
    //			logger.info( "Fired needClassifier: " + govWord + " " + depWord);
    //
    //			/*Incase the first person exists, like We need more information, these kind of things should be neutral. But for cases like, I hope that they improve, this should be like normal needClassification*/
    //			IndexedWord nsubjDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.NOMINAL_SUBJECT);
    //			if (nsubjDep != null && !govWord.equals("hope")) {
    //				String nsubjDepWord = nsubjDep.word();
    //				if (nsubjDepWord.equalsIgnoreCase("we") || nsubjDepWord.equalsIgnoreCase("i")) {
    //					return false;
    //				}
    //			}
    //
    //			this.featureSet.setNeedFeature(true);
    //
    //			String sentiment = null;
    //			if (!depWord.equalsIgnoreCase(subject)) {
    //				//				System.out.println("I am here!!");
    //
    //				/*If it is not an aspect then too, it should be negative. For eg: They need to do this*/
    //				sentiment = SentimentLexicon.NEGATIVE_SENTIMENT;
    //
    //				//				String[] aspect = XpOntology.findAspect(language, depWord, domainName);
    //				//				if (aspect != null) {
    //				//					this.tagSentimentMap.put(aspect[1], sentiment);
    //				//					this.addToTagAspectMap(depWord, aspect);
    //				//				}
    //
    //			} else {
    //				sentiment = SentimentLexicon.POSITIVE_SENTIMENT;
    //			}
    //
    //			if (negatedOpinionHash.containsKey(govWord) || negatedOpinionHash.containsKey(depWord)) {
    //				sentiment = SentimentFunctions.switchSentiment(sentiment);
    //			}
    //
    //			if (sentiment != null) {
    //				if (sentiment.equals(SentimentLexicon.POSITIVE_SENTIMENT)) {
    //					this.featureSet.setNeedPosFeature(true);
    //				} else if (sentiment.equals(SentimentLexicon.NEGATIVE_SENTIMENT)) {
    //					this.featureSet.setNeedNegFeature(true);
    //				}
    //				this.tagSentimentMap.put(SentimentLexicon.NEED_ID, sentiment);
    //
    //				String[] aspect = XpOntology.findAspect(language, depWord, domainName);
    //				if (aspect != null) {
    //					this.tagSentimentMap.put(aspect[1], sentiment);
    //					this.addToTagAspectMap(depWord, aspect);
    //				}
    //
    //				String tagFromSentiment = SentimentFunctions.getTagFromSentiment(sentiment);
    //				this.tagSentimentMap.put(govWord, tagFromSentiment);
    //				if (dep.tag().contains("VB")) {
    //					this.tagSentimentMap.put(depWord, tagFromSentiment);
    //				}
    //
    //			}
    //			return true;
    //		}
    //		return false;
    //
    //	}

    public boolean negationEitherClassifier(String gov, String dep) {

        if (XpSentiment.SHIFTER_TAG.equals(XpSentiment.getLexTag(language, gov, wordPOSmap, tagSentimentMap))) {
            // negatedOpinionHash.add(v3);
            logger.info("NegEither " + dep + " is negated by " + gov);
            if (!this.negatedOpinionMap.containsKey(dep)) {
                addToNegatedOpinionMap(dep, gov.toLowerCase(), false, true);
                negBasedFeaturize(dep);
            } else {
				/*For the case of No, I don't like. like is already negated by don't. "No" doesnt play any role of shifter*/
                logger.info("Shifter Nullified in negationEither: " + gov);
                this.addToTagSentimentMap(gov, XpSentiment.NON_SENTI_TAG);
            }
            return true;
        } else if (XpSentiment.SHIFTER_TAG.equals(XpSentiment.getLexTag(language, dep, wordPOSmap, tagSentimentMap))) {
            // negatedOpinionHash.add(v1);
            logger.info("NegEither " + gov + " is negated by " + dep);
            if (!this.negatedOpinionMap.containsKey(gov)) {
                addToNegatedOpinionMap(gov, dep.toLowerCase(), false, true);
                negBasedFeaturize(gov.toLowerCase());
            } else {
                logger.info("Shifter Nullified in negationEither: " + dep);
                this.addToTagSentimentMap(dep, XpSentiment.NON_SENTI_TAG);
            }
            return true;
        }
        return false;
    }

    public String getMethodName(String relation) {

        //		System.out.print("Relation: " + relation + "\t");

        if (relation.contains(":")) {
            relation = relation.split(":")[0];
        } else if (relation.contains("_")) {
            relation = relation.split("_")[0];
        }
        // return "fireRule_" + relation;
        //		System.out.println(relation);
        return relation + "_fireRule";
    }

    //	public void iterativeMethodsCall(IndexedWord vertex, IndexedWord secondary_vertex, GrammaticalRelation edgeRelation) {
    //		for (SemanticGraphEdge edge : this.dependencyGraph.outgoingEdgeIterable(vertex)) {
    //
    //			//			System.out.println("CheckedEdges: " + this.checkedEdge);
    //			if (this.checkedEdge.contains(edge) || (edge.getRelation().toString().endsWith("mod"))) {
    //				continue;
    //			}
    //
    //			this.checkedEdge.add(edge);
    //
    //			GrammaticalRelation newEdgeRelation = edge.getRelation();
    //			String methodName = getMethodName(newEdgeRelation.toString());
    //			IndexedWord target_vertex = edge.getTarget();
    //
    //			Method method;
    //			try {
    //				method = c.getMethod(methodName, IndexedWord.class, IndexedWord.class, IndexedWord.class, GrammaticalRelation.class);
    //				method.invoke(this, vertex, secondary_vertex, target_vertex, newEdgeRelation);
    //			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
    //				System.err.println("Not found method: " + methodName + " >> " + e.getCause());
    //				e.printStackTrace();
    //			}
    //		}
    //	}

    public void nsubj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        logger.info("NSUBJ: " + dep + " nsubj " + gov);
        String depWord = dep.word();
        String govWord = gov.word();

        if (VocabularyTests.isFirstPersonSubject(language, depWord))
            this.featureSet.setIsSubjectFirstPerson(true);
        else if (VocabularyTests.isSecondThirdPersonSubject(language, depWord))
            this.featureSet.setIsSubjectSecondThirdPerson(true);

        //		IndexedWord nnDep = this.dependencyGraph.getChildWithReln(dep, UniversalEnglishGrammaticalRelations.NOUN_COMPOUND_MODIFIER);
        //		if (nnDep != null) {
        //			this.addToOpinionMap(nnDep + " " + depWord, govWord);
        //			this.checkedEdge.add(this.dependencyGraph.getEdge(dep, nnDep));
        //		} else {
        //		}

        this.featureSet.setNsubjFeature(true);

        if (!PosTags.isProperNoun(language, gov.tag())) {
            this.addToOpinionMap(dep, govWord);

            String govTag = XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap);
            if (SentimentFunctions.isPolarTag(govTag)) {
                if (govTag.contains(XpSentiment.POSITIVE_TAG)) {
                    this.featureSet.setNsubjPosFeature(true);
                } else if (govTag.contains(XpSentiment.NEGATIVE_TAG)) {
                    this.featureSet.setNsubjNegFeature(true);
                }
            }
        }
        depWord = depWord.toLowerCase();
        String[] depAspect = XpOntology.findAspect(language, depWord, domainName, subject, this.xto);
        if (depAspect != null) {
            this.addToTagAspectMap(depWord, depAspect);
            //			this.featureSet.setNsubjAspectFeature(true);
        }
        SentimentEngine.nsubjBasedSentimentClassifier(language, gov, dep, edgeRelation, this);
        //		this.emotionVector.nsubjVectorize(gov, dep, edgeRelation.getShortName());
        //		try {
        //			iterativeMethodsCall(gov, dep, edgeRelation);
        //			// iterativeMethodsCall(dep, gov, edgeRelation);
        //		} catch (IllegalArgumentException | SecurityException e) {
        //			e.printStackTrace();
        //		}

    }

    public void nsubjpass_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
        logger.info("NsubjPass: " + dep + " nsubjPass " + gov);
        String depWord = dep.value();
        String govWord = gov.value();
        this.addToOpinionMap(dep, govWord);

        this.featureSet.setNsubjPassFeature(true);

        String govTag = XpSentiment.getLexTag(language, govWord, wordPOSmap, tagSentimentMap);
        if (SentimentFunctions.isPolarTag(govTag)) {
            if (govTag.contains(XpSentiment.POSITIVE_TAG)) {
                this.featureSet.setNsubjPassPosFeature(true);
            } else if (govTag.contains(XpSentiment.NEGATIVE_TAG)) {
                this.featureSet.setNsubjPassNegFeature(true);
            }
        }
        String[] depAspect = XpOntology.findAspect(language, depWord, domainName, subject, this.xto);
        if (depAspect != null) {
            this.addToTagAspectMap(depWord, depAspect);
            this.featureSet.setNsubjPassAspectFeature(true);
        }
        SentimentEngine.nsubjBasedSentimentClassifier(language, gov, dep, edgeRelation, this);
        if (VocabularyTests.isApproveDecline(language, govWord)) {
            IntentEngine.intentionClassification(gov, dep, edgeRelation, this, false, domainName);
        }
        //		this.emotionVector.nsubjVectorize(gov, dep, edgeRelation.getShortName());

        //		try {
        //			iterativeMethodsCall(gov, dep, edgeRelation);
        //			iterativeMethodsCall(dep, gov, edgeRelation);
        //		} catch (IllegalArgumentException | SecurityException e) {
        //			e.printStackTrace();
        //		}

    }

    // public List<DefaultEdge> prioritizeEdgesSet(Set<DefaultEdge> edgesSet) {
    // List<DefaultEdge> sortedEdgesList = new ArrayList<DefaultEdge>();
    // for (DefaultEdge ed : edgesSet) {
    // sortedEdgesList.add(ed);
    // }
    // Collections.sort(sortedEdgesList, new DependencyRelationSorter());
    // return sortedEdgesList;
    // }

    public void fireDependencyRules() {
        // double startTime = System.nanoTime();
        // System.out.println("Time to get EdgesListSorted: " +
        // (System.nanoTime() - startTime) + " ns");
        //		List<SemanticGraphEdge> sortedEdgesList = this.dependencyGraph.edgeListSorted();
        //		Collections.sort(sortedEdgesList, new DependencyRelationSorter());
        //		System.out.println(sortedEdgesList.toString());

        //		for (SemanticGraphEdge edge : sortedEdgesList) {

        for (SemanticGraphEdge edge : sortedEdges) {

            IndexedWord gov = edge.getGovernor();
            IndexedWord dep = edge.getDependent();

            //			IndexedWord nsubjDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.NOMINAL_SUBJECT);
            //			System.out.println("nsubjDep: " + nsubjDep);

            // System.out.println("Gov: " + gov + "\tGovWord: " + gov.value() +
            // gov.tag() + "\tDep: " + dep + "\tDepWord: " + dep.value());
            //			System.out.println("CheckedEdges: " + this.checkedEdge);

            if (this.checkedEdge.contains(edge)) {
                continue;
            }
            GrammaticalRelation rel = edge.getRelation();

            //			if (rel.equals(UniversalEnglishGrammaticalRelations.CLAUSAL_COMPLEMENT) || rel.equals(UniversalEnglishGrammaticalRelations.CLAUSAL_COMPLEMENT) || rel.equals(UniversalEnglishGrammaticalRelations.DIRECT_OBJECT) || rel.equals(UniversalEnglishGrammaticalRelations.XCLAUSAL_COMPLEMENT)) {
            //				System.out.println("entered Firest time");
            //			} else if (rel.equals(UniversalEnglishGrammaticalRelations.XCLAUSAL_COMPLEMENT)) {
            //				System.out.println("entered secn time");
            //			} else if (rel.equals(UniversalEnglishGrammaticalRelations.NOMINAL_SUBJECT)) {
            //				System.out.println("entered the time");
            //			}

            this.checkedEdge.add(edge);
            try {
                String relationStr = rel.toString();
                String methodName = getMethodName(relationStr);
                //	System.out.println("Calling Method: " + methodName);
                //				logger.info("Method Call:\t" + relationStr + "\t" + methodName);
                Method method = c.getMethod(methodName, IndexedWord.class, IndexedWord.class, IndexedWord.class, GrammaticalRelation.class);
                method.invoke(this, gov, null, dep, rel);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
                e.printStackTrace();
            }

        }

        //		logger.info("TagAspectMap " + this.tagAspectMap.toString());

        // for (DefaultEdge ed : sortedEdgesList) {
        // if (checkedEdge.contains(ed)) {
        // continue;
        // }
        // String edgeRelation = ed.toString();
        // checkedEdge.add(ed);
        // // String sourceV = this.dependency_graph.getEdgeSource(ed);
        // String gov = this.dependency_graph.getEdgeSource(ed);
        // // String currTargetV = this.dependency_graph.getEdgeTarget(ed);
        // String dep = this.dependency_graph.getEdgeTarget(ed);
        // try {
        // String methodName = getMethodName(edgeRelation);
        // Method method = c.getMethod(methodName, String.class, String.class,
        // String.class, String.class);
        // method.invoke(this, gov, null, dep, edgeRelation);
        // } catch (IllegalAccessException | IllegalArgumentException |
        // InvocationTargetException | NoSuchMethodException | SecurityException
        // e) {
        // e.printStackTrace();
        // }
        // }

    }

    // private static String cleanGraphNodes(String str) {
    // // str = str.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(",",
    // // "").replaceAll("#\\d+", "").replaceAll("null", "").replaceAll("\\s+",
    // // " ").toLowerCase();
    //
    // return str;
    // }

    // public HashMap<String, List<String>> getEntityOpinionMap() {

    public XpFeatureVector getFeatureSet() {
        return this.featureSet;
    }

    public Map<String, Set<String>> getEntityOpinionMap() {
        // this.fireDependencyRules();

        logger.info("Initial EntityOpinionMap:\t" + this.entityOpinionMap + "\n");

        if (this.entityOpinionMap == null) {
            return null;
        }

        for (Map.Entry<String, String> entry : this.modifiedOpinionMap.entrySet()) {

            String key = entry.getKey();
            String value = entry.getValue();
            List<String> valWords = ProcessString.tokenizeString(language, value);
            for (String currWd : valWords) {
                String iterValue = this.modifiedOpinionMap.get(currWd);
                if (iterValue != null && !iterValue.equals(value)) {
                    // System.out.println("Initial Value: " + value);
                    //	iterValue = iterValue.replaceAll("\\$", "");
                    iterValue = Pattern.compile("\\$|\\\\", Pattern.UNICODE_CHARACTER_CLASS).matcher(iterValue).replaceAll("");
                    value = Pattern.compile("\\b" + currWd + "\\b", Pattern.UNICODE_CHARACTER_CLASS).matcher(value).replaceAll(iterValue);
                    this.addToModifiedOpinionMap(key, value);
                    //					this.modifiedOpinionMap.put(key, value);
                    break;
                }

            }
            // incase modified word of a particular word is also modified.
        }
        // System.out.println("NegatedOpinionHash: " + negatedOpinionHash);

        Map<String, Set<String>> entityOpinionMap_mod = new LinkedHashMap<String, Set<String>>();

        logger.info("Negated Opinion Map:\t" + this.negatedOpinionMap);
        logger.info("ModifiedOpinion Map:\t" + this.modifiedOpinionMap);
        logger.info("MultiWordEntity Map:\t" + this.multiWordEntityMap);
        logger.info("EquivalentEntity Map:\t" + this.equivalentEntityMap + "\n");
        //		logger.info("TagAspectMap:\t" + this.tagAspectMap);

        String rootWordStr = null;
        Collection<IndexedWord> roots = this.dependencyGraph.getRoots();
        IndexedWord rootWord = null;
        if (roots.size() > 0) {
            rootWord = this.dependencyGraph.getFirstRoot();
            rootWordStr = rootWord.word().toLowerCase();
        }
        boolean isRootExistsEOmap = false;

        for (Map.Entry<IndexedWord, List<String>> entry : this.entityOpinionMap.entrySet()) {

            IndexedWord keyIdxWord = entry.getKey();
            //			String entity = keyIdxWord.word();
            String entity = keyIdxWord.word().toLowerCase();

            // boolean isEntityNegated = false;
            String entityNegator = null;
            for (String negEnt : negatedOpinionMap.keySet()) {
                //				if (entity.contains(negEnt)) {
                //	negEnt = negEnt.replaceAll("[^\\w\\s]", "");
                negEnt = SPECIAL_CHARS_PATTERN.matcher(negEnt).replaceAll("");
                if (Pattern.compile("\\b" + negEnt + "\\b", Pattern.UNICODE_CHARACTER_CLASS).matcher(entity).matches()) {
                    //	if (entity.matches("\\b" + negEnt + "\\b")) {
                    // isEntityNegated = true;
                    entityNegator = negatedOpinionMap.get(negEnt);
                    //					XpActivator.pw.println(this.text + "," + entityNegator + " " + negEnt);
                }
            }
            List<String> opinionSet = entry.getValue();

			/* equivalent opinions*/
            Set<String> opinionSet_mod = entityOpinionMap_mod.get(entity);
            if (opinionSet_mod == null) {
                opinionSet_mod = new HashSet<String>();
            }

            for (String opinion : opinionSet) {
                // System.out.println("Opinion: " + opinion);
                if (opinion != null) {
                    if (this.equivalentEntityMap.containsKey(opinion)) {
                        opinion = this.equivalentEntityMap.get(opinion).get(0);
                    }

                    if (!isRootExistsEOmap) {
                        if (entity.contains(rootWordStr) || opinion.contains(rootWordStr)) {
                            isRootExistsEOmap = true;
                        }
                    }

                    boolean neg_opFlag = false;
                    String negator = null;
                    List<String> opinionWords = ProcessString.tokenizeString(language, opinion);
                    String clean_opinionNew = opinion;

                    for (String wd : opinionWords) {
                        String modifiedWord = modifiedOpinionMap.get(wd);
                        //	logger.info("modifiedWord: " + modifiedWord);
						/*Incase equivalentEntityMap and modifiedOpinionMap has the same value for a key, we don't want repetition*/
                        if (modifiedWord != null && !modifiedWord.equals(opinion)) {
                            //	modifiedWord = modifiedWord.replaceAll("[^\\w\\s]", "");
                            modifiedWord = SPECIAL_CHARS_PATTERN.matcher(modifiedWord).replaceAll("");
                            //	wd = wd.replaceAll("[^\\w\\s]", "");
                            wd = SPECIAL_CHARS_PATTERN.matcher(wd).replaceAll("");
                            //	clean_opinionNew = clean_opinionNew.replaceAll("\\b" + wd + "\\b", modifiedWord);
                            clean_opinionNew = Pattern.compile("\\b" + wd + "\\b", Pattern.UNICODE_CHARACTER_CLASS).matcher(clean_opinionNew).replaceAll(modifiedWord);
                        } else {
                            modifiedWord = modifiedOpinionMap.get(entity);
                            if (modifiedWord != null && !clean_opinionNew.contains(modifiedWord) && !modifiedWord.contains(clean_opinionNew)) {
                                clean_opinionNew += " " + modifiedWord;
                            }
                        }
						/* such that same opinion is not negated twice. The  store did not smell*/
                        //						if (entityNegator == null && negatedOpinionMap.containsKey(wd) && !neg_opFlag) {
                        if (entityNegator == null && !neg_opFlag) {
                            for (String negEnt : negatedOpinionMap.keySet()) {
                                String negEntLower = negEnt.toLowerCase();
                                if (Pattern.compile("\\b" + negEntLower + "\\b", Pattern.UNICODE_CHARACTER_CLASS).matcher(wd).matches()) {
                                    negator = negatedOpinionMap.get(negEnt);
                                }
                            }

                            //							negator = negatedOpinionMap.get(wd);

                            if (negator != null) {
                                wd = SPECIAL_CHARS_PATTERN.matcher(wd).replaceAll("");
                                String regex = "\\b" + wd + "\\b";
                                clean_opinionNew = Pattern.compile(regex, Pattern.UNICODE_CHARACTER_CLASS).matcher(clean_opinionNew).replaceAll(negator + " " + wd);
                                neg_opFlag = true;
                            }
                        }

						/*Suppose could rule has been fired and sentiment of helpful is shifted, then it should not again switch back to the original*/
                        wd = wd.toLowerCase();
                        //	logger.info("tagSentimentMap.containsKey(" + wd + "): " + tagSentimentMap.containsKey(wd));
                        if (!tagSentimentMap.containsKey(wd)) {

                            String tag = XpOntology.getDomainContextualSentimentTag(language, entity, wd, domainName, subject, this.xto);
                            //	logger.info("tag: " + tag + "\tentity: " + entity + "\twd: " + wd);
                            if (tag == null) {
                                String equivalentEntity = this.multiWordEntityMap.get(keyIdxWord);
                                if (equivalentEntity != null) {
                                    tag = XpOntology.getDomainContextualSentimentTag(language, equivalentEntity, wd, domainName, subject, this.xto);
                                    //	logger.info("tag: " + tag + "\tequivalentEntity: " + equivalentEntity + "\twd: " + wd);
                                }
                            }
                            //	logger.info("wd " + wd + " has tag: " + tag);
                            //							System.out.println("Tag " + wd + " for " + entity + ": " + tag);
                            if (tag != null) {
                                String[] tagArr = tag.split("#");
                                String sentiTag = tagArr[0];
                                String aspect = tagArr[1];
                                // System.out.println("Tag " + entity + ": " + tag);
                                if (sentiTag != null && !sentiTag.equalsIgnoreCase("null")) {
                                    // System.out.println("Putting in tagSentiMap " + wd + ": " + tag);
                                    logger.info("Contextual Sentiment Tag of " + wd + " in " + entity + " \t " + sentiTag + "\t" + aspect);

                                    if (sentiTag.equals(XpSentiment.NN_TAG)) {
                                        //	logger.info("Case NN_TAG");
                                        String entity_senti = XpSentiment.getLexTag(language, entity, wordPOSmap, tagSentimentMap);
                                        this.addToTagSentimentMap(wd + " " + entity, entity_senti);
                                    } else {
                                        this.addToTagSentimentMap(wd, sentiTag);
                                        if (this.negatedOpinionMap.containsKey(wd)) {
                                            String shifter = this.negatedOpinionMap.get(wd);
                                            logger.info("Contextual Sentiment leads to Shifter Reapplied: " + shifter);
                                            this.tagSentimentMap.put(shifter, XpSentiment.SHIFTER_TAG);
                                        }
                                    }
                                    // System.out.println("Contextual Aspect " + aspect);

                                    if (aspect != null && !aspect.equalsIgnoreCase("null")) {
                                        String[] aspectArr = new String[2];
                                        aspectArr[0] = entity.toLowerCase();
                                        aspectArr[1] = aspect.toUpperCase();
                                        if (XpOntology.isValidAspectCategory(language, aspectArr[1], domainName)) {
                                            logger.info("Contextual Sentiment Based Valid Aspect for " + domainName + "\t" + aspectArr[1]);
                                            this.addToTagAspectMap(aspectArr[0], aspectArr);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    opinion = clean_opinionNew;
                    if (entityNegator != null && !entityNegator.equalsIgnoreCase("null")) {
                        //	logger.info("Case entityNegator: " + entityNegator);
                        opinion = entityNegator + " " + opinion;
                        negator = entityNegator;
                    }
                    opinionSet_mod.add(opinion);
                    List<String> eq_opinionSet = this.equivalentEntityMap.get(opinion);
                    if (eq_opinionSet != null) {
                        for (String eq_opinion : eq_opinionSet) {
                            if (eq_opinion != null) {
                                String clean_eq_opinion = eq_opinion;
                                if (neg_opFlag || entityNegator != null) {
                                    //	logger.info("Case neg_opFlag: " + neg_opFlag + "\t" + entityNegator);
                                    clean_eq_opinion = negator + " " + clean_eq_opinion;
                                    //	logger.info("clean_eq_opinion: " + clean_eq_opinion);
                                }
                                opinionSet_mod.add(clean_eq_opinion);
                            }
                        }
                    }
                }
            }

			/*Equivalent entities*/
            String multiWordEntity = this.multiWordEntityMap.get(keyIdxWord);
            if (multiWordEntity != null) {
                entityOpinionMap_mod.put(multiWordEntity, opinionSet_mod);

                Set<String> intentionObjectSet = this.featureSet.getIntentionObject();
                if (intentionObjectSet != null && intentionObjectSet.contains(entity)) {
                    intentionObjectSet.remove(entity);
                    intentionObjectSet.add(multiWordEntity);
                }

            } else {
                entityOpinionMap_mod.put(entity, opinionSet_mod);
            }

            List<String> eq_entitySet = this.equivalentEntityMap.get(entity);
            if (eq_entitySet != null) {
                for (String eq_entity : eq_entitySet) {
                    // System.out.println("Equivalent entity of " + entity +
                    // " >> " + eq_entity);
                    String[] aspect = XpOntology.findAspect(language, eq_entity, domainName, subject, this.xto);
                    if (aspect != null) {
                        this.addToTagAspectMap(eq_entity, aspect);
                        entityOpinionMap_mod.put(eq_entity, opinionSet_mod);
                        String[] original_aspect = XpOntology.findAspect(language, entity, domainName, subject, this.xto);

						/*sometimes it comes from conj and the entity must not be removed, if originally it had an aspect associated*/
                        if (original_aspect == null || original_aspect[1].equals(aspect[1])) {
                            entityOpinionMap_mod.remove(entity);
                        } else {
                            this.addToTagAspectMap(entity, original_aspect);
                        }
                    }
                }
            }
        }
        if (!isRootExistsEOmap && rootWordStr != null && !rootWordStr.matches("\\W+")) {
            Set<String> opinionSet = new HashSet<String>();
            String equivalentRootWordStr = this.multiWordEntityMap.get(rootWord);
            opinionSet.add(rootWordStr);
            if (equivalentRootWordStr == null) {
                equivalentRootWordStr = rootWordStr;
            }
            entityOpinionMap_mod.put(equivalentRootWordStr, opinionSet);
        }

        for (Map.Entry<IndexedWord, String> entry : this.multiWordEntityMap.entrySet()) {
            String multiWordEntity = entry.getValue();
            if (!entityOpinionMap_mod.containsKey(multiWordEntity)) {
                Set<String> opinionSet = new HashSet<String>();
                opinionSet.add(entry.getKey().word());
                entityOpinionMap_mod.put(multiWordEntity, opinionSet);
            }
        }
        logger.info("Final EntityOpinion Map:\t" + entityOpinionMap_mod + "\n");

        return entityOpinionMap_mod;
    }

}