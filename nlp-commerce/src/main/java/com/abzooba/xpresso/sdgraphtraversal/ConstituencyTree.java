package  com.abzooba.xpresso.sdgraphtraversal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.textanalytics.CoreNLPController;

import edu.stanford.nlp.international.Language;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.util.CoreMap;
import org.slf4j.Logger;

public class ConstituencyTree {

    protected static final Logger logger = XCLogger.getSpLogger();

    private Map<Integer, List<Integer>> children;
    private int[] parents;
    private Map<String, Set<Integer>> elementsByName;
    private Map<Integer, String> elementsByIndex;
    private Map<Integer, CoreLabel> tokensByIndex;
    private Map<Integer, Map<GrammaticalRelation, Set<Integer>>> childWithRelnByIndex;
    private Map<Integer, Map<GrammaticalRelation, Set<Integer>>> parentWithRelnByIndex;

    private Set<Integer> negGovIndices;

    public static final String GROUP_NOM = "grup.nom";
    public static final String ADJ_CLAUSE = "s.a";
    public static final String NOM_CLAUSE = "sn";
    //	public static final Pattern NOUN_PATTERN = Pattern.compile("n[c|p]0[s|p]000\\s(\\S+)");
    // 	public static final Pattern ADJ_PATTERN = Pattern.compile("aq0000\\s(\\S+)");
    //	public static final Pattern NOUN_PATTERN = Pattern.compile("n[c|p]0[s|p|0]000\\s(\\w+)", Pattern.UNICODE_CHARACTER_CLASS);
    public static final Pattern NOUN_PATTERN = Pattern.compile("nc0[s|p|n]000\\s(\\w+)", Pattern.UNICODE_CHARACTER_CLASS);
    public static final Pattern PROPER_NOUN_PATTERN = Pattern.compile("np00000\\s(\\w+)", Pattern.UNICODE_CHARACTER_CLASS);
    public static final Pattern ADJ_PATTERN = Pattern.compile("aq0000\\s(\\w+)", Pattern.UNICODE_CHARACTER_CLASS);
    public static final Pattern ADV_PATTERN = Pattern.compile("rg\\s(\\w+)", Pattern.UNICODE_CHARACTER_CLASS);
    public static final Pattern PREPGROUP_PATTERN = Pattern.compile("\\bsp\\b", Pattern.UNICODE_CHARACTER_CLASS);
    public static final Pattern PREP_PATTERN = Pattern.compile("sp000\\s(\\w+)", Pattern.UNICODE_CHARACTER_CLASS);
    // 	public static final Pattern SENTENCE_PATTERN = Pattern.compile("sentence");
    public static final Pattern CLAUSE_PATTERN = Pattern.compile("\\bS\\b|\\bsentence\\b");
    public static final Pattern MAIN_VERB_PATTERN = Pattern.compile("vm\\w{2}000\\s(\\w+)", Pattern.UNICODE_CHARACTER_CLASS);
    public static final Pattern SEMIAUX_PATTERN = Pattern.compile("vs\\w{2}000\\s(\\w+)", Pattern.UNICODE_CHARACTER_CLASS);
    public static final Pattern WORD_TOKEN_PATTERN = Pattern.compile("\\b\\S+\\s(\\w+)\\b", Pattern.UNICODE_CHARACTER_CLASS);
    public static final Pattern PUNCT_TOKEN_PATTERN = Pattern.compile("\\b\\S+\\s(\\p{Punct})", Pattern.UNICODE_CHARACTER_CLASS);

    public static final GrammaticalRelation amodRel = new GrammaticalRelation(Language.Spanish, "amod", "adjectival modifier", null);
    public static final GrammaticalRelation nsubjRel = new GrammaticalRelation(Language.Spanish, "nsubj", "nominal subject", null);
    public static final GrammaticalRelation nsubjpassRel = new GrammaticalRelation(Language.Spanish, "nsubjpass", "passive nominal subject", null);
    public static final GrammaticalRelation dobjRel = new GrammaticalRelation(Language.Spanish, "dobj", "direct object", null);
    public static final GrammaticalRelation negRel = new GrammaticalRelation(Language.Spanish, "neg", "negation", null);
    public static final GrammaticalRelation nmodRel = new GrammaticalRelation(Language.Spanish, "nmod", "oblique nominal object", null);
    public static final GrammaticalRelation nmodtmodRel = new GrammaticalRelation(Language.Spanish, "nmod:tmod", "temporal nominal object", null);
    public static final GrammaticalRelation nmodagentRel = new GrammaticalRelation(Language.Spanish, "nmod:agent", "noun as adverbial modifier", null);
    public static final GrammaticalRelation advmodRel = new GrammaticalRelation(Language.Spanish, "advmod", "adverb or adverbial modifier", null);

    private static final Set<String> negAdverbs = new HashSet<String>(Arrays.asList("nunca", "jamás", "apenas", "nomás", "raramente", "escasamente"));

    public ConstituencyTree(CoreMap sentence, String constituencyTree) {
        //		double start = System.currentTimeMillis();
        String[] strArr = constituencyTree.split("\\(");
        int nTokens = strArr.length - 1;
        parents = new int[nTokens];
        elementsByName = new HashMap<String, Set<Integer>>();
        elementsByIndex = new HashMap<Integer, String>();
        children = new HashMap<Integer, List<Integer>>();
        tokensByIndex = new HashMap<Integer, CoreLabel>();
        childWithRelnByIndex = new HashMap<Integer, Map<GrammaticalRelation, Set<Integer>>>();
        parentWithRelnByIndex = new HashMap<Integer, Map<GrammaticalRelation, Set<Integer>>>();
        List<CoreLabel> coreLabelList = sentence.get(TokensAnnotation.class);
        int sentenceTokenIdx = 0;
        int dissimilarities = 0;
        int current_parent = -1;
        for (int i = 0; i < nTokens; i++) {
            parents[i] = current_parent;
            for (String token : strArr[i + 1].trim().split("((?<=\\))|(?=\\)))")) {
                if (token.equals(")") && (current_parent > -1)) {
                    current_parent = parents[current_parent];
                } else {
                    Set<Integer> indices;
                    if (elementsByName.containsKey(token)) {
                        indices = elementsByName.get(token);
                    } else {
                        indices = new HashSet<Integer>();
                    }
                    indices.add(i);
                    elementsByName.put(token, indices);
                    elementsByIndex.put(i, token);
                    if (isWordOrPunct(token) && (sentenceTokenIdx < coreLabelList.size())) {
                        //						System.out.println(token);
                        // dissimilarities can arise due to the splitting of words: del -> de el, damelo -> da me lo
                        //	System.out.println("getWord(token) = " + getWord(token));
                        if (getWord(token).equalsIgnoreCase(coreLabelList.get(sentenceTokenIdx).word())) {
                            dissimilarities = 0;
                            //	System.out.println("Case 1: " + coreLabelList.get(sentenceTokenIdx).word());
                            tokensByIndex.put(i, coreLabelList.get(sentenceTokenIdx));
                            sentenceTokenIdx += 1;
                        } else {
                            if (dissimilarities == 0) {
                                //	System.out.println("Case 2: " + coreLabelList.get(sentenceTokenIdx).word());
                                sentenceTokenIdx += 1;
                                //	} else {
                                //		System.out.println("Case 3: " + coreLabelList.get(sentenceTokenIdx).word());
                            }
                            dissimilarities += 1;
                        }
                    }
                    List<Integer> childrenSet;
                    if (children.containsKey(current_parent)) {
                        childrenSet = children.get(current_parent);
                    } else {
                        childrenSet = new ArrayList<Integer>();
                    }
                    childrenSet.add(i);
                    children.put(current_parent, childrenSet);
                    current_parent = i;
                }
            }
        }
        negGovIndices = new HashSet<Integer>();
        //		System.out.println("Finished parsing in " + (System.currentTimeMillis() - start) + " ms");
    }

    public List<SemanticGraphEdge> extractEdges() {
        List<SemanticGraphEdge> edges = null;

        List<SemanticGraphEdge> amodEdges = extractAmod();
        List<SemanticGraphEdge> nsubjEdges = extractNsubj();
        List<SemanticGraphEdge> nsubjpassEdges = extractNsubjPass();
        List<SemanticGraphEdge> negEdges = extractNeg();
        List<SemanticGraphEdge> dobjEdges = extractDobj();
        List<SemanticGraphEdge> nmodEdges = extractNmod();
        List<SemanticGraphEdge> advmodEdges = extractAdvmod();

        edges = addEdges(edges, amodEdges);
        edges = addEdges(edges, nsubjEdges);
        edges = addEdges(edges, nsubjpassEdges);
        edges = addEdges(edges, negEdges);
        edges = addEdges(edges, dobjEdges);
        edges = addEdges(edges, nmodEdges);
        edges = addEdges(edges, advmodEdges);
//		edges = extractAmod();
//		if (edges == null) {
//			edges = extractNsubj();
//		} else {
//			edges.addAll(extractNsubj());
//		}
//		if (edges == null) {
//			edges = extractNsubjPass();
//		} else {
//			edges.addAll(extractNsubjPass());
//		}
//		if (edges == null) {
//			edges = extractNeg();
//		} else {
//			edges.addAll(extractNeg());
//		}
//		if (edges == null) {
//			edges = extractDobj();
//		} else {
//			edges.addAll(extractDobj());
//		}
//		if (edges == null) {
//			edges = extractNmod();
//		} else {
//			edges.addAll(extractNmod());
//		}
//		if (edges == null) {
//			edges = extractAdvmod();
//		} else {
//			edges.addAll(extractAdvmod());
//		}

        return edges;
    }

    private List<SemanticGraphEdge> addEdges(List<SemanticGraphEdge> edges, List<SemanticGraphEdge> addOns) {
        List<SemanticGraphEdge> answ = edges;
        if (addOns != null) {
            if (answ != null)
                answ.addAll(addOns);
            else
                answ = addOns;
        }

        return answ;
    }

    private boolean isWordOrPunct(String token) {
        Matcher word = WORD_TOKEN_PATTERN.matcher(token);
        Matcher punct = PUNCT_TOKEN_PATTERN.matcher(token);
        return (word.find() || punct.find());
    }

    private String getWord(String token) {
        Matcher word = WORD_TOKEN_PATTERN.matcher(token);
        Matcher punct = PUNCT_TOKEN_PATTERN.matcher(token);
        String answ;
        if (word.find()) {
            answ = word.group(1);
        } else if (punct.find()) {
            answ = punct.group(1);
        } else {
            answ = "";
        }
        return answ;
    }

    public int getNumberChildren(int idx) {
        if (children.containsKey(idx)) {
            return children.get(idx).size();
        } else {
            return 0;
        }
    }

    public String getChildren(int idx) {
        StringBuilder sb = new StringBuilder();
        if (children.containsKey(idx)) {
            for (int j : children.get(idx)) {
                sb.append(elementsByIndex.get(j));
                sb.append(", ");
            }
        } else {
            sb.append("None");
        }
        return sb.toString();
    }

    public Set<Integer> getIndexSet(IndexedWord word) {
        Set<Integer> answ = null;
        String wordStr = word.word();
        for (String key : elementsByName.keySet()) {
            if (key.endsWith(wordStr)) {
                if (answ == null) {
                    answ = new HashSet<Integer>();
                }
                answ.addAll(elementsByName.get(key));
            }
        }

        return answ;
    }

    public Set<IndexedWord> getChildWithReln(IndexedWord word, GrammaticalRelation rel) {
        Set<Integer> indices = getIndexSet(word);
        Set<IndexedWord> answ = new HashSet<IndexedWord>();
        for (int i : indices) {
            answ.addAll(getChildWithReln(i, rel));
        }

        return answ;
    }

    public Set<IndexedWord> getChildWithReln(int i, GrammaticalRelation rel) {
        Set<IndexedWord> answ = new HashSet<IndexedWord>();
        if (childWithRelnByIndex.containsKey(i) && childWithRelnByIndex.get(i).containsKey(rel)) {
            Set<Integer> indices = new HashSet<Integer>();
            indices = childWithRelnByIndex.get(i).get(rel);
            for (int idx : indices) {
                IndexedWord token = new IndexedWord(tokensByIndex.get(idx));
                answ.add(token);
            }
        }

        return answ;
    }

    public Set<IndexedWord> getParentWithReln(IndexedWord word, GrammaticalRelation rel) {
        Set<Integer> indices = getIndexSet(word);
        Set<IndexedWord> answ = new HashSet<IndexedWord>();
        for (int i : indices) {
            answ.addAll(getParentWithReln(i, rel));
        }

        return answ;
    }

    public Set<IndexedWord> getParentWithReln(int i, GrammaticalRelation rel) {
        Set<IndexedWord> answ = new HashSet<IndexedWord>();
        if (parentWithRelnByIndex.containsKey(i) && parentWithRelnByIndex.get(i).containsKey(rel)) {
            Set<Integer> indices = new HashSet<Integer>();
            indices = parentWithRelnByIndex.get(i).get(rel);
            for (int idx : indices) {
                IndexedWord token = new IndexedWord(tokensByIndex.get(idx));
                answ.add(token);
            }
        }

        return answ;
    }

    private void updateRelnTrees(int gov, int dep, GrammaticalRelation rel) {
        logger.info("gov index: " + gov + " dep index: " + dep + " rel: " + rel.toString());
        if (childWithRelnByIndex.containsKey(gov)) {
            if (childWithRelnByIndex.get(gov).containsKey(rel)) {
                childWithRelnByIndex.get(gov).get(rel).add(dep);
            } else {
                Set<Integer> indices = new HashSet<Integer>();
                indices.add(dep);
                childWithRelnByIndex.get(gov).put(rel, indices);
            }
        } else {
            Set<Integer> indices = new HashSet<Integer>();
            indices.add(dep);
            Map<GrammaticalRelation, Set<Integer>> children = new HashMap<GrammaticalRelation, Set<Integer>>();
            children.put(rel, indices);
            childWithRelnByIndex.put(gov, children);
        }
        logger.info("Updated childWithReln for " + gov + " with " + childWithRelnByIndex.get(gov).toString());

        if (parentWithRelnByIndex.containsKey(dep)) {
            if (parentWithRelnByIndex.get(dep).containsKey(rel)) {
                parentWithRelnByIndex.get(dep).get(rel).add(gov);
            } else {
                Set<Integer> indices = new HashSet<Integer>();
                indices.add(gov);
                parentWithRelnByIndex.get(dep).put(rel, indices);
            }
        } else {
            Set<Integer> indices = new HashSet<Integer>();
            indices.add(gov);
            Map<GrammaticalRelation, Set<Integer>> parents = new HashMap<GrammaticalRelation, Set<Integer>>();
            parents.put(rel, indices);
            parentWithRelnByIndex.put(dep, parents);
        }
        logger.info("Updated parentWithReln for " + dep + " with " + parentWithRelnByIndex.get(dep).toString());
    }

    public int getNegGov(String govWord) {
        int answ = 0;
        for (int i : negGovIndices) {
            String[] splitElement = elementsByIndex.get(i).split(" ");
            String word = splitElement[1];
            logger.info("Comparing " + govWord + " with " + elementsByIndex.get(i) + ": " + word);
            if (word.equals(govWord)) {
                answ = i;
                break;
            }
        }

        return answ;
    }

    // 	public String getChildren(String name) {
    // 		StringBuilder sb = new StringBuilder();
    // 		for (int j : children.get(elementsByName.get(name))) {
    // 			sb.append(elementsByIndex.get(j));
    // 			sb.append(", ");
    // 		}
    // 		return sb.toString();
    // 	}

    // 	public int sizeByName() {
    // 		return elementsByName.size();
    // 	}

    public int size() {
        return elementsByIndex.size();
    }

    private Set<Integer> findGroupNom() {
        return elementsByName.get(GROUP_NOM);
    }

    // 	private Set<Integer> findNomClause() {
    // 		return elementsByName.get(NOM_CLAUSE);
    // 	}
    //
    // 	private Set<Integer> findAdjClause() {
    // 		return elementsByName.get(ADJ_CLAUSE);
    // 	}

    private String findNounInGroupNom(int groupNomIdx) {
        StringBuilder sb = new StringBuilder();
        for (int child : children.get(groupNomIdx)) {
            Matcher noun = NOUN_PATTERN.matcher(elementsByIndex.get(child));
            if (noun.find()) {
                sb.append(elementsByIndex.get(child) + ",");
            }
            Matcher properNoun = PROPER_NOUN_PATTERN.matcher(elementsByIndex.get(child));
            if (properNoun.find()) {
                sb.append(elementsByIndex.get(child) + ",");
            }
        }
        //		sb.setLength(Math.max(sb.length() - 1, 0));
        return sb.toString();
    }

    private List<Integer> findNounInGroupNomIdx(int groupNomIdx) {
        List<Integer> answ = new ArrayList<Integer>();
        for (int child : children.get(groupNomIdx)) {
            Matcher noun = NOUN_PATTERN.matcher(elementsByIndex.get(child));
            if (noun.find()) {
                answ.add(child);
            }
            Matcher properNoun = PROPER_NOUN_PATTERN.matcher(elementsByIndex.get(child));
            if (properNoun.find()) {
                answ.add(child);
            }
        }
        return answ;
    }

    // 	private String findNounInNomClause(int i) {
    // 		String answ = null;
    // 		for (int child : children.get(i)) {
    // 			if (elementsByIndex.get(child).equals(GROUP_NOM)) {
    // 				answ = findNounInGroupNom(child);
    // 			}
    // 		}
    // 		return answ;
    // 	}
    //
    // 	private String findNounInGroup(int i) {
    // 		StringBuilder sb = new StringBuilder();
    // 		if (children.get(i) == null) {
    // 			Matcher noun = NOUN_PATTERN.matcher(elementsByIndex.get(i));
    // 			if (noun.find()) {
    // 				sb.append(noun.group(3) + " ");
    // 			} else {
    // 				sb.append("");
    // 			}
    // 		} else {
    // 			for (int child : children.get(i)) {
    //// 				Matcher noun = NOUN_PATTERN.matcher(elementsByIndex.get(child));
    // 				sb.append(findNounInGroup(child));
    // 			}
    // 		}
    // 		return sb.toString();
    // 	}

    private String findAdjInGroup(int groupIdx) {
        StringBuilder sb = new StringBuilder();
        if (children.get(groupIdx) == null) {
            Matcher adj = ADJ_PATTERN.matcher(elementsByIndex.get(groupIdx));
            if (adj.find()) {
                sb.append(elementsByIndex.get(groupIdx) + ",");
            }
        } else {
            for (int child : children.get(groupIdx)) {
                String childStr = elementsByIndex.get(child);
                Matcher adj = ADJ_PATTERN.matcher(childStr);
                if (childStr.equals("s.a") || childStr.equals("grup.a") || adj.find()) {
                    //	 				System.out.println(elementsByIndex.get(child));
                    sb.append(findAdjInGroup(child));
                }
            }
        }
        //		sb.setLength(Math.max(sb.length() - 1, 0));
        return sb.toString();
    }

    private List<Integer> findAdjInGroupIdx(int groupIdx) {
        List<Integer> answ = new ArrayList<Integer>();
        if (children.get(groupIdx) == null) {
            Matcher adj = ADJ_PATTERN.matcher(elementsByIndex.get(groupIdx));
            if (adj.find()) {
                answ.add(groupIdx);
            }
        } else {
            for (int child : children.get(groupIdx)) {
                String childStr = elementsByIndex.get(child);
                Matcher adj = ADJ_PATTERN.matcher(childStr);
                if (childStr.equals("s.a") || childStr.equals("grup.a") || adj.find()) {
                    //	 				System.out.println(elementsByIndex.get(child));
                    answ.addAll(findAdjInGroupIdx(child));
                }
            }
        }
        return answ;
    }

    private String findParticipiAdjInGroup(int groupIdx) {
        StringBuilder sb = new StringBuilder();
        if (children.get(groupIdx) == null) {
            Matcher adj = ADJ_PATTERN.matcher(elementsByIndex.get(groupIdx));
            if (adj.find()) {
                sb.append(elementsByIndex.get(groupIdx) + ",");
            }
        } else {
            for (int child : children.get(groupIdx)) {
                String childStr = elementsByIndex.get(child);
                Matcher adj = ADJ_PATTERN.matcher(childStr);
                if (childStr.equals("S") || childStr.equals("participi") || adj.find()) {
                    sb.append(findParticipiAdjInGroup(child));
                }
            }
        }
        return sb.toString();
    }

    private List<Integer> findParticipiAdjInGroupIdx(int groupIdx) {
        List<Integer> answ = new ArrayList<Integer>();
        if (children.get(groupIdx) == null) {
            Matcher adj = ADJ_PATTERN.matcher(elementsByIndex.get(groupIdx));
            if (adj.find()) {
                answ.add(groupIdx);
            }
        } else {
            for (int child : children.get(groupIdx)) {
                String childStr = elementsByIndex.get(child);
                Matcher adj = ADJ_PATTERN.matcher(childStr);
                if (childStr.equals("S") || childStr.equals("participi") || adj.find()) {
                    answ.addAll(findParticipiAdjInGroupIdx(child));
                }
            }
        }
        return answ;
    }

    // 	private String alternateFindAdjInGroup(int i) {
    // 		StringBuilder sb = new StringBuilder();
    // 		if (children.get(i) == null) {
    // 			Matcher adj = ADJ_PATTERN.matcher(elementsByIndex.get(i));
    // 			if (adj.find()) {
    //// 				System.out.println("adj.group(): " + adj.group());
    //// 				System.out.println("found adj");
    //	 			sb.append(adj.group(1) + " ");
    // 			} else {
    // 				sb.append("");
    // 			}
    // 			return sb.toString();
    // 		} else {
    // 			for (int child : children.get(i)) {
    // 				Matcher adj = ADJ_PATTERN.matcher(elementsByIndex.get(child));
    // 				if (elementsByIndex.get(child).equals(GROUP_NOM) || elementsByIndex.get(child).equals(ADJ_CLAUSE) || elementsByIndex.get(child).equals("grup.a") || elementsByIndex.get(child).equals("S") || elementsByIndex.get(child).equals("participi") || adj.find()) {
    ////	 				System.out.println(elementsByIndex.get(child));
    //	 				sb.append(findAdjInGroup(child));
    // 				}
    // 			}
    // 		}
    // 		return sb.toString();
    // 	}

    public String extractAmodStr() {
        StringBuilder sb = new StringBuilder();
        Set<Integer> indicesGN = findGroupNom();
        if (indicesGN != null) {
            for (int i : indicesGN) {
                //				localSb.append("(");
                StringBuilder localSb = new StringBuilder();
                localSb.append(findNounInGroupNom(i));
                localSb.append(",");
                localSb.append(findAdjInGroup(i));
                localSb.append(",");
                localSb.append(findParticipiAdjInGroup(i));
                sb.append(cleanAmod(localSb.toString()));
            }
        }
        sb.setLength(Math.max(sb.length() - 1, 0));
        return sb.toString();
    }

    private String cleanAmod(String rawPair) {
        String[] rawArr = rawPair.split(",");
        if (rawArr.length < 2) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        boolean hasNoun = false;
        for (String group : rawArr) {
            Matcher noun = NOUN_PATTERN.matcher(group);
            if (noun.find()) {
                hasNoun = true;
                sb.append(noun.group(1) + ",");
                continue;
            }
            Matcher properNoun = PROPER_NOUN_PATTERN.matcher(group);
            if (properNoun.find()) {
                hasNoun = true;
                sb.append(properNoun.group(1) + ",");
                continue;
            }
            if (!hasNoun) {
                break;
            }
            Matcher adj = ADJ_PATTERN.matcher(group);
            if (adj.find()) {
                sb.append(adj.group(1) + ",");
            }
        }
        if (sb.length() > 0) {
            sb.setCharAt(sb.length() - 1, ';');
        }
        return sb.toString();
    }

    public List<SemanticGraphEdge> extractAmod() {
        List<SemanticGraphEdge> answ = new ArrayList<SemanticGraphEdge>();
        // TODO replace null by GrammaticalRelation parent
        //		GrammaticalRelation amodRel = new GrammaticalRelation(Language.Spanish, "amod", "adjectival modifier", null);
        Set<Integer> indicesGN = findGroupNom();
        if (indicesGN != null) {
            for (int i : indicesGN) {
                List<Integer> nounIdx = findNounInGroupNomIdx(i);
                List<Integer> adjIdx = findAdjInGroupIdx(i);
                adjIdx.addAll(findParticipiAdjInGroupIdx(i));

                for (int n : nounIdx) {
                    IndexedWord noun = null;
                    if (tokensByIndex.containsKey(n)) {
                        noun = new IndexedWord(tokensByIndex.get(n));
                    }
                    for (int a : adjIdx) {
                        IndexedWord adj = null;
                        if (tokensByIndex.containsKey(a)) {
                            adj = new IndexedWord(tokensByIndex.get(a));
                        }
                        if (noun != null && adj != null) {
                            SemanticGraphEdge edge = new SemanticGraphEdge(noun, adj, amodRel, 1.0, false);
                            answ.add(edge);
                            updateRelnTrees(n, a, amodRel);
                        }
                    }
                }
            }
        }
        return answ;
    }

    private Set<Integer> findSentence() {
        return elementsByName.get("sentence");
    }

    private Set<Integer> findClause() {
        return elementsByName.get("S");
    }

    private String findNounInGroup(int groupIdx) {
        StringBuilder sb = new StringBuilder();
        if (children.get(groupIdx) == null) {
            Matcher noun = NOUN_PATTERN.matcher(elementsByIndex.get(groupIdx));
            if (noun.find()) {
                sb.append(elementsByIndex.get(groupIdx) + ",");
            }
            Matcher properNoun = PROPER_NOUN_PATTERN.matcher(elementsByIndex.get(groupIdx));
            if (properNoun.find()) {
                sb.append(elementsByIndex.get(groupIdx) + ",");
            }
        } else {
            for (int child : children.get(groupIdx)) {
                String childStr = elementsByIndex.get(child);
                Matcher noun = NOUN_PATTERN.matcher(childStr);
                Matcher properNoun = PROPER_NOUN_PATTERN.matcher(childStr);
                if (childStr.equals("sn") || childStr.equals("grup.nom") || noun.find() || properNoun.find()) {
                    sb.append(findNounInGroup(child));
                }
            }
        }
        return sb.toString();
    }

    private List<Integer> findNounInGroupIdx(int groupIdx) {
        List<Integer> answ = new ArrayList<Integer>();
        if (children.get(groupIdx) == null) {
            Matcher noun = NOUN_PATTERN.matcher(elementsByIndex.get(groupIdx));
            if (noun.find()) {
                answ.add(groupIdx);
                //	System.out.println("Found noun " + elementsByIndex.get(groupIdx));
            }
            Matcher properNoun = PROPER_NOUN_PATTERN.matcher(elementsByIndex.get(groupIdx));
            if (properNoun.find()) {
                answ.add(groupIdx);
                //	System.out.println("Found noun " + elementsByIndex.get(groupIdx));
            }
        } else {
            for (int child : children.get(groupIdx)) {
                String childStr = elementsByIndex.get(child);
                //	System.out.println("Looking for noun in " + childStr);
                Matcher noun = NOUN_PATTERN.matcher(childStr);
                Matcher properNoun = PROPER_NOUN_PATTERN.matcher(childStr);
                if (childStr.equals("sn") || childStr.equals("grup.nom") || noun.find() || properNoun.find()) {
                    answ.addAll(findNounInGroupIdx(child));
                }
            }
        }
        return answ;
    }

    private String findNounInClause(int clauseIdx, int minIdx, int maxIdx) {
        StringBuilder sb = new StringBuilder();
        if (children.get(clauseIdx) != null) {
            for (int child : children.get(clauseIdx)) {
                if (minIdx < child && (child < maxIdx || maxIdx < 0) && elementsByIndex.get(child).equals("sn")) {
                    sb.append(findNounInGroup(child));
                }
            }
        }
        return sb.toString();
    }

    private List<Integer> findNounInClauseIdx(int clauseIdx, int minIdx, int maxIdx) {
        List<Integer> answ = new ArrayList<Integer>();
        if (children.get(clauseIdx) != null) {
            for (int child : children.get(clauseIdx)) {
                //	System.out.println("Looking for nouns in child " + elementsByIndex.get(child) + " of clause " + clauseIdx);
                if (minIdx < child && (child < maxIdx || maxIdx < 0) && elementsByIndex.get(child).equals("sn")) {
                    answ.addAll(findNounInGroupIdx(child));
                }
            }
        }
        return answ;
    }

    private String findAdjInClause(int clauseIdx) {
        StringBuilder sb = new StringBuilder();
        if (children.get(clauseIdx) != null) {
            for (int child : children.get(clauseIdx)) {
                if (elementsByIndex.get(child).equals("s.a")) {
                    // Matching an adjective in an adjective syntagma
                    sb.append(findAdjInGroup(child));
                } else if (elementsByIndex.get(child).equals("S")) {
                    for (int grandChild : children.get(child)) {
                        if (elementsByIndex.get(grandChild).equals("participi")) {
                            // Matching a past participle used as adjective
                            sb.append(findAdjInGroup(grandChild));
                        }
                    }
                }
            }
        }
        return sb.toString();
    }

    private List<Integer> findAdjInClauseIdx(int clauseIdx) {
        List<Integer> answ = new ArrayList<Integer>();
        if (children.get(clauseIdx) != null) {
            for (int child : children.get(clauseIdx)) {
                if (elementsByIndex.get(child).equals("s.a")) {
                    // Matching an adjective in an adjective syntagma
                    answ.addAll(findAdjInGroupIdx(child));
                } else if (elementsByIndex.get(child).equals("S")) {
                    for (int grandChild : children.get(child)) {
                        if (elementsByIndex.get(grandChild).equals("participi")) {
                            // Matching a past participle used as adjective
                            answ.addAll(findAdjInGroupIdx(grandChild));
                        }
                    }
                }
            }
        }
        return answ;
    }

    private String findMainVerbInGroup(int groupIdx) {
        // TODO distinguish active and passive forms
        StringBuilder sb = new StringBuilder();
        if (children.get(groupIdx) == null) {
            Matcher mainVerb = MAIN_VERB_PATTERN.matcher(elementsByIndex.get(groupIdx));
            if (mainVerb.find() && !CoreNLPController.lemmatizeToString(Languages.SP, mainVerb.group(1)).equals("estar")) {
                sb.append(elementsByIndex.get(groupIdx) + "-" + groupIdx + "-");
            }
        } else {
            for (int child : children.get(groupIdx)) {
                sb.append(findMainVerbInGroup(child));
            }
        }

        return sb.toString();
    }

    private List<Integer> findMainVerbInGroupIdx(int groupIdx) {
        List<Integer> answ = new ArrayList<Integer>();
        if (children.get(groupIdx) == null) {
            Matcher mainVerb = MAIN_VERB_PATTERN.matcher(elementsByIndex.get(groupIdx));
            if (mainVerb.find() && !CoreNLPController.lemmatizeToString(Languages.SP, mainVerb.group(1)).equals("estar") && !CoreNLPController.lemmatizeToString(Languages.SP, mainVerb.group(1)).equals("parecer")) {
                answ.add(groupIdx);
            }
        } else {
            for (int child : children.get(groupIdx)) {
                answ.addAll(findMainVerbInGroupIdx(child));
            }
        }
        return answ;
    }

    private int findCopulaIdxInGroup(int groupIdx) {
        // Nececessary to find copula index to distinguish between subject and predicate in case all are nouns
        int answ = -1;
        if (children.get(groupIdx) == null) {
            Matcher ser = SEMIAUX_PATTERN.matcher(elementsByIndex.get(groupIdx));
            Matcher estarParecer = MAIN_VERB_PATTERN.matcher(elementsByIndex.get(groupIdx));
            if (ser.find() || (estarParecer.find() && (CoreNLPController.lemmatizeToString(Languages.SP, estarParecer.group(1)).equals("estar") || CoreNLPController.lemmatizeToString(Languages.SP, estarParecer.group(1)).equals("parecer")))) {
                answ = groupIdx;
            }
        } else {
            for (int child : children.get(groupIdx)) {
                if (answ < 0) {
                    answ = findCopulaIdxInGroup(child);
                }
            }
        }
        //		if (answ < 0) {
        //			answ = 0;
        //		}
        return answ;
    }

    //	private String findSemiAuxVerbInGroup(int i) {
    //		StringBuilder sb = new StringBuilder();
    //		if (children.get(i) == null) {
    //			Matcher semiVerb = SEMIAUX_VERB_PATTERN.matcher(elementsByIndex.get(i));
    //			if (semiVerb.find()) {
    //				sb.append(semiVerb.group(1) + " ");
    //			}
    //		} else {
    //			for (int child : children.get(i)) {
    //				sb.append(findSemiAuxVerbInGroup(child));
    //			}
    //		}
    //
    //		return sb.toString();
    //	}

    private String findMainVerbInClause(int clauseIdx) {
        StringBuilder sb = new StringBuilder();
        if (children.get(clauseIdx) != null) {
            for (int child : children.get(clauseIdx)) {
                if (elementsByIndex.get(child).equals("grup.verb")) {
                    sb.append(findMainVerbInGroup(child));
                }
            }
        }
        return sb.toString();
    }

    private List<Integer> findMainVerbInClauseIdx(int clauseIdx) {
        List<Integer> answ = new ArrayList<Integer>();
        if (children.get(clauseIdx) != null) {
            for (int child : children.get(clauseIdx)) {
                if (elementsByIndex.get(child).equals("grup.verb")) {
                    answ.addAll(findMainVerbInGroupIdx(child));
                }
            }
        }
        return answ;
    }

    private int findCopulaIdxInClause(int clauseIdx) {
        int answ = 0;
        if (children.get(clauseIdx) != null) {
            for (int child : children.get(clauseIdx)) {
                if (elementsByIndex.get(child).equals("grup.verb")) {
                    answ = findCopulaIdxInGroup(child);
                }
            }
        }
        return answ;
    }

    //	private String findAdvInGroup(int groupIdx) {
    //		StringBuilder sb = new StringBuilder();
    //		if (children.get(groupIdx) == null) {
    //			Matcher adverb = ADV_PATTERN.matcher(elementsByIndex.get(groupIdx));
    //			if (adverb.find()) {
    //				sb.append(elementsByIndex.get(groupIdx));
    //			}
    //		} else {
    //			for (int child : children.get(groupIdx)) {
    //				String childStr = elementsByIndex.get(child);
    //				Matcher adverb = ADV_PATTERN.matcher(childStr);
    //				if (childStr.equals("sadv") || childStr.equals("grup.adv") || adverb.find()) {
    //					sb.append(findNounInGroup(child));
    //				}
    //			}
    //		}
    //
    //		return sb.toString();
    //	}

    private int findAdvInGroupIdx(int groupIdx) {
        //	System.out.println("Looking for advIdx in " + groupIdx + ", " + elementsByIndex.get(groupIdx));
        int answ = 0;
        if (children.get(groupIdx) == null) {
            Matcher adverb = ADV_PATTERN.matcher(elementsByIndex.get(groupIdx));
            if (adverb.find()) {
                //	System.out.println("Found " + elementsByIndex.get(groupIdx));
                answ = groupIdx;
                return answ;
            }
        } else {
            for (int child : children.get(groupIdx)) {
                String childStr = elementsByIndex.get(child);
                Matcher adverb = ADV_PATTERN.matcher(childStr);
                if (childStr.equals("sadv") || childStr.equals("grup.adv") || adverb.find()) {
                    //	System.out.println("Going down the tree.");
                    answ = findAdvInGroupIdx(child);
                    return answ;
                }
            }
        }

        return answ;
    }

    private List<Integer> findAdvInClauseIdx(int clauseIdx) {
        List<Integer> answ = new ArrayList<Integer>();
        if (children.get(clauseIdx) != null) {
            for (int child : children.get(clauseIdx)) {
                if (elementsByIndex.get(child).equals("sadv")) {
                    int advIdx = findAdvInGroupIdx(child);
                    Matcher adverbMatcher = ADV_PATTERN.matcher(elementsByIndex.get(advIdx));
                    String word = null;
                    if (adverbMatcher.find()) {
                        word = adverbMatcher.group(1);
                        word = word.toLowerCase();
                        //	System.out.println("word: " + word);
                    }
                    if (!negAdverbs.contains(word)) {
                        answ.add(advIdx);
                    }
                }
            }
        }

        return answ;
    }

    private int findAdvSpecInGroupIdx(int groupIdx) {
        int answ = 0;
        if (children.get(groupIdx) == null) {
            Matcher adverb = ADV_PATTERN.matcher(elementsByIndex.get(groupIdx));
            if (adverb.find()) {
                //	System.out.println("Found " + elementsByIndex.get(groupIdx));
                answ = groupIdx;
                return answ;
            }
        } else {
            for (int child : children.get(groupIdx)) {
                String childStr = elementsByIndex.get(child);
                Matcher adverb = ADV_PATTERN.matcher(childStr);
                if (childStr.equals("spec") || adverb.find()) {
                    //	System.out.println("Going down the tree.");
                    answ = findAdvInGroupIdx(child);
                    return answ;
                }
            }
        }

        return answ;
    }

    //	private boolean isNegGroup(int groupIdx, Languages language, Map<String, String> wordPOSmap, Map<String, String> tagSentimentMap) {
    private int findNegInGroupIdx(int groupIdx) {
        int answ = 0;
        if ("neg".equals(elementsByIndex.get(groupIdx))) {
            answ = children.get(groupIdx).get(0);
            return answ;
        } else if (elementsByIndex.get(groupIdx).equals("sadv")) {
            int adverb = findAdvInGroupIdx(groupIdx);
            //	System.out.println("Adverb index to be looked up: " + adverb);
            //	if (XpSentiment.getLexTag(language, adverb, wordPOSmap, tagSentimentMap).equals(XpSentiment.SHIFTER_TAG)) {
            //	DANGEROUS SOLUTION AS THE LIST OF NEGATIVE ADVERB IS NOT LINKED TO THE FILE OF SHIFTERS
            Matcher adverbMatcher = ADV_PATTERN.matcher(elementsByIndex.get(adverb));
            String word = null;
            if (adverbMatcher.find()) {
                word = adverbMatcher.group(1);
                word = word.toLowerCase();
                //	System.out.println("word: " + word);
            }
            if (negAdverbs.contains(word)) {
                //	System.out.println("Adverb is negative");
                answ = adverb;
                return answ;
            }
        }

        return answ;
    }

    //	private int findNegIdxInClause(int clauseIdx, Languages language, Map<String, String> wordPOSmap, Map<String, String> tagSentimentMap) {
    private int findNegIdxInClause(int clauseIdx) {
        int answ = 0;
        if (children.get(clauseIdx) != null) {
            for (int child : children.get(clauseIdx)) {
                //	if (isNegGroup(child, language, wordPOSmap, tagSentimentMap)) {
                //	System.out.println("Looking for neg in group " + child + ", " + elementsByIndex.get(child));
                answ = findNegInGroupIdx(child);
                //	System.out.println("Found: " + answ);
                if (answ > 0) {
                    break;
                }
            }
        }

        return answ;
    }

    //	private int findNegatedIdxInClause(int clauseIdx, int negIdx) {
    //		int answ = 0;
    //		// Looking for a negation
    //		if (negIdx > 0) {
    //			System.out.println("negIdx is " + negIdx);
    //			// Looking for main verb
    //			List<Integer> verbIndices = findMainVerbInGroupIdx(clauseIdx);
    //			if (verbIndices != null) {
    //				// Picking the first main verb after the found negation
    //				for (int idx : verbIndices) {
    //					System.out.println("Testing verb idx " + idx);
    //					if (idx > negIdx) {
    //						System.out.println("First verb found " + elementsByIndex.get(idx));
    //						answ = idx;
    //						break;
    //					}
    //				}
    //			}
    //			if (answ == 0) {
    //				// In case no main verb is found, a copula is looked for
    //				System.out.println("Looking for a copula");
    //				int copulaIdx = findCopulaIdxInClause(clauseIdx);
    //				System.out.println("Testing copula " + elementsByIndex.get(copulaIdx));
    //				if (copulaIdx > negIdx) {
    //					List<Integer> adjectives = findAdjInClauseIdx(clauseIdx);
    //					for (int i : adjectives) {
    //						System.out.println("Trying adj " + i + ", " + elementsByIndex.get(i));
    //						if (i > negIdx) {
    //							answ = i;
    //							break;
    //						}
    //					}
    //					if (answ == 0) {
    //						System.out.println("No attributive adjective.");
    //						List<Integer> nouns = findNounInClauseIdx(clauseIdx, copulaIdx, -1);
    //						for (int i : nouns) {
    //							System.out.println("Trying noun " + i + ", " + elementsByName.get(i));
    //							if (i > negIdx) {
    //								answ = i;
    //								break;
    //							}
    //						}
    //					}
    //					// TODO: dealing with nouns;
    //				}
    //			}
    //
    //		}
    //
    //		return answ;
    //	}

    private List<Integer> findNegatedIdxInClause(int clauseIdx, int negIdx) {
        //	Outputting the proper neg relation but also the dependant:
        //	no es un buen coche -> [coche/nc0s000 -> no/rn (neg), buen/aq0000 -> no/rn (neg)]
        List<Integer> answ = new ArrayList<Integer>();
        // Looking for a negation
        if (negIdx > 0) {
            //	System.out.println("negIdx is " + negIdx);
            // Looking for main verb
            List<Integer> verbIndices = findMainVerbInGroupIdx(clauseIdx);
            if (verbIndices != null) {
                // Picking the first main verb after the found negation
                for (int idx : verbIndices) {
                    //	System.out.println("Testing verb idx " + idx);
                    if (idx > negIdx) {
                        //	System.out.println("First verb found " + elementsByIndex.get(idx));
                        answ.add(idx);
                        break;
                    }
                }
            }
            if (answ.size() == 0) {
                // In case no main verb is found, a copula is looked for
                //	System.out.println("Looking for a copula");
                int copulaIdx = findCopulaIdxInClause(clauseIdx);
                //	System.out.println("Testing copula " + elementsByIndex.get(copulaIdx));
                if (copulaIdx > negIdx) {
                    List<Integer> adjectives = findAdjInClauseIdx(clauseIdx);
                    for (int i : adjectives) {
                        //	System.out.println("Trying adj " + i + ", " + elementsByIndex.get(i));
                        if (i > negIdx) {
                            answ.add(i);
                            //	break;
                        }
                    }
                    if (answ.size() == 0) {
                        //	System.out.println("No attributive adjective.");
                        List<Integer> nouns = findNounInClauseIdx(clauseIdx, copulaIdx, -1);
                        for (int i : nouns) {
                            //	System.out.println("Trying noun " + i + ", " + elementsByName.get(i));
                            if (i > negIdx) {
                                answ.add(i);
                                //	break;
                            }
                        }
                        // adding epithets
                        Set<Integer> gn = findGroupNom();
                        if (gn != null) {
                            for (int i : gn) {
                                if (i > negIdx) {
                                    List<Integer> adj = findAdjInGroupIdx(i);
                                    adj.addAll(findParticipiAdjInGroupIdx(i));
                                    answ.addAll(adj);
                                }
                            }
                        }
                    }
                    // TODO: dealing with nouns;
                }
            }

        }

        return answ;
    }

    //	private List<Integer> findPrepGroupIdx(int clauseIdx) {
    //		List<Integer> answ = new ArrayList<Integer>();
    //		Matcher gpprep = PREPGROUP_PATTERN.matcher(elementsByIndex.get(clauseIdx));
    //		if (gpprep.find()) {
    //			answ.add(clauseIdx);
    //			return answ;
    //		} else if (children.get(clauseIdx) != null) {
    //			for (int child : children.get(clauseIdx)) {
    //				answ.addAll(findPrepGroupIdx(child));
    //			}
    //		}
    //
    //		return answ;
    //	}

    private List<Integer> findPrepGroupInClauseIdx(int clauseIdx) {
        List<Integer> answ = new ArrayList<Integer>();

        // Only looking for prepositional group at the clause level
        if (children.get(clauseIdx) != null) {
            for (int child : children.get(clauseIdx)) {
                //	System.out.println("Looking for sp in " + elementsByIndex.get(child) + " for clause " + clauseIdx);
                Matcher gpprep = PREPGROUP_PATTERN.matcher(elementsByIndex.get(child));
                if (gpprep.find()) {
                    answ.add(child);
                }
            }
        }

        return answ;
    }

    private int findPrepInSPGroupIdx(int gpIdx) {
        //	List<Integer> answ = new ArrayList<Integer>();
        int answ = 0;
        String currentElement = elementsByIndex.get(gpIdx);
        //	System.out.println("Looking for prep in " + currentElement);
        if (children.get(gpIdx) == null) {
            Matcher prep = PREP_PATTERN.matcher(elementsByIndex.get(gpIdx));
            if (prep.find()) {
                answ = gpIdx;
                return answ;
            }
        } else if ("prep".equals(currentElement) || "sp".equals(currentElement) || "grup.prep".equals(currentElement)) {
            for (int child : children.get(gpIdx)) {
                answ = findPrepInSPGroupIdx(child);
                if (answ != 0) {
                    return answ;
                }
            }
        }

        return answ;
    }

    //public List<SemanticGraphEdge> extractNeg(Languages language, Map<String, String> wordPOSmap, Map<String, String> tagSentimentMap) {
    public List<SemanticGraphEdge> extractNeg() {
        List<SemanticGraphEdge> answ = new ArrayList<SemanticGraphEdge>();
        Set<Integer> indicesClause = findSentence();
        if (findClause() != null) {
            indicesClause.addAll(findClause());
        }

        if (indicesClause != null) {
            for (int i : indicesClause) {
                //	int negIdx = findNegIdxInClause(i, language, wordPOSmap, tagSentimentMap);
                int negIdx = findNegIdxInClause(i);
                //	System.out.println("negIdx: " + negIdx);
                List<Integer> negatedIndices = findNegatedIdxInClause(i, negIdx);
                //	System.out.println("negatedIdx: " + negatedIndices);
                IndexedWord negation = null;
                if (tokensByIndex.containsKey(negIdx)) {
                    negation = new IndexedWord(tokensByIndex.get(negIdx));
                }
                for (int n : negatedIndices) {
                    IndexedWord governor = null;
                    if (tokensByIndex.containsKey(n)) {
                        governor = new IndexedWord(tokensByIndex.get(n));
                    }
                    if (negation != null && governor != null) {
                        SemanticGraphEdge edge = new SemanticGraphEdge(governor, negation, negRel, 1.0, false);
                        answ.add(edge);
                        negGovIndices.add(n);
                        updateRelnTrees(n, negIdx, negRel);
                    }
                }
            }
        }

        return answ;
    }

    //	public String extractNegStr() {
    //		StringBuilder sb = new StringBuilder();
    //		List<SemanticGraphEdge> edges = extractNeg();
    //		for (SemanticGraphEdge edge : edges) {
    //			sb.append(edge.toString());
    //		}
    //
    //		return sb.toString();
    //	}

    // 	private String findAdjInClause(int i, Set<Integer> visited) {
    // 		StringBuilder sb = new StringBuilder();
    // 		if (children.get(i) != null) {
    // 			for (int child : children.get(i)) {
    // 				if ((!visited.contains(child)) && (elementsByIndex.get(child).equals("s.a"))) {
    // 					// Matching an adjective in an adjective syntagma
    // 					sb.append(findAdjInGroup(child));
    // 				} else if (elementsByIndex.get(child).equals("S")) {
    // 					for (int grandChild : children.get(child)) {
    // 						if (elementsByIndex.get(grandChild).equals("participi")) {
    // 							// Matching a past participle used as adjective
    // 							sb.append(findAdjInGroup(grandChild));
    // 						}
    // 					}
    // 				}
    // 			}
    // 		}
    // 		return sb.toString();
    // 	}

    public String extractNsubjStr() {
        StringBuilder sb = new StringBuilder();
        Set<Integer> indicesClause = findSentence();
        if (findClause() != null) {
            indicesClause.addAll(findClause());
        }
        if (indicesClause != null) {
            //			System.out.println("sentence: " + indicesClause.toString());
            for (int i : indicesClause) {
                if (!isPassiveClause(i)) {
                    StringBuilder localSb = new StringBuilder();
                    String mainVerb = findMainVerbInClause(i);
                    String nouns;
                    if (mainVerb.length() > 0) {
                        String[] mainVerbArr = mainVerb.split("-");
                        for (int k = 0; k < (mainVerbArr.length / 2); k++) {
                            nouns = findNounInClause(i, -1, Integer.parseInt(mainVerbArr[2 * k + 1].trim()));
                            localSb.append(mainVerbArr[2 * k]);
                            localSb.append(",");
                            localSb.append(nouns);
                            localSb.append(",");
                        }
                    } else {
                        int copulaIdx = findCopulaIdxInClause(i);
                        nouns = findNounInClause(i, -1, copulaIdx).replaceAll(",", "-");
                        localSb.append(findAdjInClause(i).trim().replaceAll(",", "-"));
                        localSb.append(",");
                        localSb.append(findNounInClause(i, copulaIdx, -1).replaceAll(",", "-"));
                        localSb.append(",");
                        localSb.append(nouns);
                        localSb.append(",");
                    }
                    sb.append(cleanNsubj(localSb.toString()));
                }
            }
        }
        sb.setLength(Math.max(sb.length() - 1, 0));
        return sb.toString();
    }

    public List<SemanticGraphEdge> extractNsubj() {
        List<SemanticGraphEdge> answ = new ArrayList<SemanticGraphEdge>();
        Set<Integer> indicesClause = findSentence();
        if (findClause() != null) {
            indicesClause.addAll(findClause());
        }
        if (indicesClause != null) {
            //			System.out.println("sentence: " + indicesClause.toString());
            for (int i : indicesClause) {
                if (!isPassiveClause(i)) {
                    List<Integer> mainVerbs = findMainVerbInClauseIdx(i);
                    List<Integer> nouns;
                    if (mainVerbs.size() > 0) {
                        for (int v : mainVerbs) {
                            IndexedWord verb = null;
                            if (tokensByIndex.containsKey(v)) {
                                verb = new IndexedWord(tokensByIndex.get(v));
                            }
                            nouns = findNounInClauseIdx(i, -1, v);
                            for (int n : nouns) {
                                IndexedWord noun = null;
                                if (tokensByIndex.containsKey(n)) {
                                    noun = new IndexedWord(tokensByIndex.get(n));
                                }
                                if (noun != null && verb != null) {
                                    SemanticGraphEdge edge = new SemanticGraphEdge(verb, noun, nsubjRel, 1.0, false);
                                    answ.add(edge);
                                    updateRelnTrees(v, n, nsubjRel);
                                }
                            }
                        }
                    } else {
                        int copulaIdx = findCopulaIdxInClause(i);
                        nouns = findNounInClauseIdx(i, -1, copulaIdx);
                        List<Integer> adjectives = findAdjInClauseIdx(i);
                        // the attribute can be a noun
                        adjectives.addAll(findNounInClauseIdx(i, copulaIdx, -1));
                        for (int n : nouns) {
                            IndexedWord noun = null;
                            if (tokensByIndex.containsKey(n)) {
                                noun = new IndexedWord(tokensByIndex.get(n));
                            }
                            for (int a : adjectives) {
                                IndexedWord adj = null;
                                if (tokensByIndex.containsKey(a)) {
                                    adj = new IndexedWord(tokensByIndex.get(a));
                                }
                                if (noun != null && adj != null) {
                                    SemanticGraphEdge edge = new SemanticGraphEdge(adj, noun, nsubjRel, 1.0, false);
                                    answ.add(edge);
                                    updateRelnTrees(a, n, nsubjRel);
                                }
                            }
                        }
                    }
                }
            }
        }
        return answ;
    }

    private String cleanNsubj(String rawStr) {
        String[] rawArr = rawStr.split(",");
        if (rawArr.length < 2) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        boolean hasGov = false;
        String[] predicates = null;
        String[] subjects = null;
        String govVerb = null;
        for (String group : rawArr) {
            Matcher verb = MAIN_VERB_PATTERN.matcher(group);
            if (verb.find()) {
                hasGov = true;
                if (sb.length() > 0) {
                    sb.setCharAt(sb.length() - 1, ';');
                }
                //				sb.append(verb.group(1) + ",");
                govVerb = verb.group(1) + ",";
                continue;
            }
            if (hasGov) {
                // Case of a sentence with a main verb: gov=verb, dep=subject(noun)
                Matcher noun = NOUN_PATTERN.matcher(group);
                if (noun.find()) {
                    sb.append(govVerb);
                    sb.append(noun.group(1) + ";");
                    continue;
                }
                Matcher properNoun = PROPER_NOUN_PATTERN.matcher(group);
                if (properNoun.find()) {
                    sb.append(govVerb);
                    sb.append(properNoun.group(1) + ";");
                    continue;
                }
            } else if (group.length() > 0) {
                // Case of a sentence with a copula: gov=predicate(adj or noun), dep=subject(noun)
                if (predicates == null) {
                    predicates = group.split("-");
                    continue;
                }
                if (subjects == null) {
                    subjects = group.split("-");
                }
                for (String pred : predicates) {
                    for (String subj : subjects) {
                        String predicate = null;
                        String subject = null;
                        Matcher adj = ADJ_PATTERN.matcher(pred);
                        if (adj.find()) {
                            predicate = adj.group(1);
                        } else {
                            Matcher noun = NOUN_PATTERN.matcher(pred);
                            if (noun.find()) {
                                predicate = noun.group(1);
                            } else {
                                Matcher properNoun = PROPER_NOUN_PATTERN.matcher(pred);
                                if (properNoun.find()) {
                                    predicate = properNoun.group(1);
                                }
                            }
                        }
                        Matcher noun = NOUN_PATTERN.matcher(subj);
                        if (noun.find()) {
                            subject = noun.group(1);
                        } else {
                            Matcher properNoun = PROPER_NOUN_PATTERN.matcher(subj);
                            if (properNoun.find()) {
                                subject = properNoun.group(1);
                            }
                        }
                        if (sb.length() > 0) {
                            sb.setCharAt(sb.length() - 1, ';');
                        }
                        sb.append(predicate);
                        sb.append(",");
                        sb.append(subject);
                        sb.append(";");
                    }
                }
                predicates = null;
                subjects = null;
                //				Matcher adj = ADJ_PATTERN.matcher(group);
                //				if (adj.find()) {
                //					hasGov = true;
                //					sb.append(adj.group(1) + ",");
                //				} else {
                //					Matcher noun = NOUN_PATTERN.matcher(group);
                //					if (noun.find()) {
                //						if (dep == null) {
                //							dep = noun.group(1) + ";";
                //						} else {
                //							sb.append(noun.group(1) + "," + dep);
                //						}
                //					}
                //					Matcher properNoun = PROPER_NOUN_PATTERN.matcher(group);
                //					if (properNoun.find()) {
                //						if (dep == null) {
                //							dep = properNoun.group(1) + ",";
                //						} else {
                //							sb.append(properNoun.group(1) + "," + dep);
                //						}
                //					}
                //				}
            }
        }
        if (sb.length() > 0) {
            sb.setCharAt(sb.length() - 1, ';');
        }
        return sb.toString();
    }

    private boolean isPassiveVerbalGroup(int verbalGroupIdx) {
        // This does not take into account the use of a reflexive structure to indicate a passive form.
        boolean answ = false;
        if (elementsByIndex.get(verbalGroupIdx).equals("grup.verb")) {
            boolean hasSemiAux = false;
            boolean hasPastParticiple = false;
            String childStr;
            for (int child : children.get(verbalGroupIdx)) {
                childStr = elementsByIndex.get(child);
                if (childStr.startsWith("vs")) {
                    hasSemiAux = true;
                }
                if (childStr.startsWith("vmp0000")) {
                    hasPastParticiple = true;
                }
            }
            answ = hasSemiAux && hasPastParticiple;
        }
        return answ;
    }

    private boolean isPassiveClause(int clauseIdx) {
        boolean answ = false;
        if (children.get(clauseIdx) != null) {
            for (int child : children.get(clauseIdx)) {
                if (elementsByIndex.get(child).equals("morfema.verbal")) {
                    answ = true;
                    //					System.out.println("Reflexive passive form");
                    break;
                }
                if (elementsByIndex.get(child).equals("grup.verb")) {
                    answ = isPassiveVerbalGroup(child);
                    break;
                }
            }
        }
        return answ;
    }

    private boolean isReflexivePassiveClause(int clauseIdx) {
        boolean answ = false;
        if (children.get(clauseIdx) != null) {
            for (int child : children.get(clauseIdx)) {
                if (elementsByIndex.get(child).equals("morfema.verbal")) {
                    answ = true;
                    break;
                }
            }
        }
        return answ;
    }

    public String extractNsubjPassStr() {
        StringBuilder sb = new StringBuilder();
        Set<Integer> indicesClause = findSentence();
        if (findClause() != null) {
            indicesClause.addAll(findClause());
        }
        //		System.out.println("sentence: " + indicesClause.toString());
        if (indicesClause != null) {
            for (int i : indicesClause) {
                if (isPassiveClause(i)) {
                    StringBuilder localSb = new StringBuilder();
                    String mainVerb = findMainVerbInClause(i);
                    String nouns;
                    if (mainVerb.length() > 0) {
                        String[] mainVerbArr = mainVerb.split("-");
                        for (int k = 0; k < (mainVerbArr.length / 2); k++) {
                            if (isReflexivePassiveClause(i)) {
                                //In this case the subject is after the verbal group
                                nouns = findNounInClause(i, Integer.parseInt(mainVerbArr[2 * k + 1].trim()), -1);
                            } else {
                                nouns = findNounInClause(i, -1, Integer.parseInt(mainVerbArr[2 * k + 1].trim()));
                            }
                            localSb.append(mainVerbArr[2 * k]);
                            localSb.append(",");
                            localSb.append(nouns);
                            localSb.append(",");
                        }
                    }
                    sb.append(cleanNsubj(localSb.toString()));
                }
            }
        }
        sb.setLength(Math.max(sb.length() - 1, 0));
        return sb.toString();
    }

    public List<SemanticGraphEdge> extractNsubjPass() {
        List<SemanticGraphEdge> answ = new ArrayList<SemanticGraphEdge>();
        Set<Integer> indicesClause = findSentence();
        if (findClause() != null) {
            indicesClause.addAll(findClause());
        }
        //		System.out.println("sentence: " + indicesClause.toString());
        if (indicesClause != null) {
            for (int i : indicesClause) {
                if (isPassiveClause(i)) {
                    List<Integer> mainVerb = findMainVerbInClauseIdx(i);
                    List<Integer> nouns;
                    if (mainVerb.size() > 0) {
                        for (int v : mainVerb) {
                            IndexedWord verb = null;
                            if (tokensByIndex.containsKey(v)) {
                                verb = new IndexedWord(tokensByIndex.get(v));
                            }
                            if (isReflexivePassiveClause(i)) {
                                //In this case the subject is after the verbal group
                                nouns = findNounInClauseIdx(i, v, -1);
                            } else {
                                nouns = findNounInClauseIdx(i, -1, v);
                            }
                            for (int n : nouns) {
                                IndexedWord noun = null;
                                if (tokensByIndex.containsKey(n)) {
                                    noun = new IndexedWord(tokensByIndex.get(n));
                                }
                                if (verb != null && noun != null) {
                                    SemanticGraphEdge edge = new SemanticGraphEdge(verb, noun, nsubjpassRel, 1.0, false);
                                    answ.add(edge);
                                    updateRelnTrees(v, n, nsubjpassRel);
                                }
                            }
                        }
                    }
                }
            }
        }
        return answ;
    }

    public List<SemanticGraphEdge> extractDobj() {
        List<SemanticGraphEdge> answ = new ArrayList<SemanticGraphEdge>();
        Set<Integer> indicesClause = findSentence();
        if (findClause() != null) {
            indicesClause.addAll(findClause());
        }
        for (int i : indicesClause) {
            if (!isPassiveClause(i)) {
                List<Integer> mainVerbs = findMainVerbInClauseIdx(i);
                List<Integer> nouns;
                List<Integer> spGroups = findPrepGroupInClauseIdx(i);
                if (mainVerbs.size() > 0) {
                    for (int v : mainVerbs) {
                        //	System.out.println("Looking for dobj of verb " + elementsByIndex.get(v));
                        IndexedWord verb = null;
                        if (tokensByIndex.containsKey(v)) {
                            verb = new IndexedWord(tokensByIndex.get(v));
                            //	System.out.println("verb: " + verb.toString());
                            //	} else {
                            //		System.out.println("Verb " + elementsByIndex.get(v) + " not in tokens");
                            //		System.out.println(tokensByIndex.toString());
                        }
                        // The following is very naive: picks nouns after the verb: not necessarily all are dobj
                        nouns = findNounInClauseIdx(i, v, -1);
                        for (int n : nouns) {
                            IndexedWord noun = null;
                            if (tokensByIndex.containsKey(n)) {
                                noun = new IndexedWord(tokensByIndex.get(n));
                                //	System.out.println("Case 1, noun: " + noun.toString());
                                //	} else {
                                //		System.out.println("Case 1: Noun " + elementsByIndex.get(n) + " not in tokens");
                                //		System.out.println(tokensByIndex.toString());
                            }
                            if (noun != null && verb != null) {
                                SemanticGraphEdge edge = new SemanticGraphEdge(verb, noun, dobjRel, 1.0, false);
                                answ.add(edge);
                                updateRelnTrees(v, n, dobjRel);
                            }
                        }
                        for (int sp : spGroups) {
                            int p = findPrepInSPGroupIdx(sp);
                            String prep = null;
                            Matcher prepMatcher = PREP_PATTERN.matcher(elementsByIndex.get(p));
                            if (prepMatcher.find()) {
                                prep = prepMatcher.group(1);
                            }
                            if ("a".equals(prep)) {
                                List<Integer> nounIdx = findNounInGroupIdx(sp);
                                for (int n : nounIdx) {
                                    IndexedWord noun = null;
                                    if (tokensByIndex.containsKey(n)) {
                                        noun = new IndexedWord(tokensByIndex.get(n));
                                        //	System.out.println("Case 2, noun: " + noun.toString());
                                        //	} else {
                                        //		System.out.println("Case 1: Noun " + elementsByIndex.get(n) + " not in tokens");
                                        //		System.out.println(tokensByIndex.toString());
                                    }
                                    if (verb != null && noun != null) {
                                        SemanticGraphEdge edge = new SemanticGraphEdge(verb, noun, dobjRel, 1.0, false);
                                        answ.add(edge);
                                        updateRelnTrees(v, n, dobjRel);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return answ;
    }

    public List<SemanticGraphEdge> extractNmod() {
        //		Set<String> temporal = new HashSet<String>();
        //		temporal.add("durante");
        //		temporal.add("después de");
        //		temporal.add("antes de");

        List<SemanticGraphEdge> answ = new ArrayList<SemanticGraphEdge>();

        Set<Integer> indicesClause = findSentence();
        if (findClause() != null) {
            indicesClause.addAll(findClause());
        }
        for (int clause : indicesClause) {
            // Looking for prepositional group at clause level; if prep is 'a' then it is dobj
            List<Integer> verbs = findMainVerbInClauseIdx(clause);
            // In case no main verb but copula, the attribute is used as governor
            // verbs.add(findCopulaIdxInClause(clause));
            int copula = findCopulaIdxInClause(clause);
            verbs.addAll(findAdjInClauseIdx(clause));
            verbs.addAll(findNounInClauseIdx(clause, copula, -1));
            List<Integer> spGroups = findPrepGroupInClauseIdx(clause);
            //	System.out.println("Main verbs: " + verbs.toString());
            //	System.out.println("Prep groups: " + spGroups.toString());
            for (int v : verbs) {
                IndexedWord verb = null;
                //	System.out.println("Working with verb: " + elementsByIndex.get(v));
                if (tokensByIndex.containsKey(v)) {
                    verb = new IndexedWord(tokensByIndex.get(v));
                    //	System.out.println("verb: " + verb.toString());
                    //	} else {
                    //		System.out.println("Verb " + elementsByIndex.get(v) + " not found.");
                    //		System.out.println(tokensByIndex.toString());
                }
                for (int sp : spGroups) {
                    int p = findPrepInSPGroupIdx(sp);
                    String prep = null;
                    Matcher prepMatcher = PREP_PATTERN.matcher(elementsByIndex.get(p));
                    if (prepMatcher.find()) {
                        prep = prepMatcher.group(1);
                        //	System.out.println("Found prep: " + prep);
                    }
                    if ("a".equals(prep)) {
                        // dobj case
                        //	System.out.println("Dobj case");
                        continue;
                    } else {
                        List<Integer> nounIdx = findNounInGroupIdx(sp);
                        for (int n : nounIdx) {
                            //	System.out.println("Working with noun: " + elementsByIndex.get(n));
                            IndexedWord noun = null;
                            if (tokensByIndex.containsKey(n)) {
                                noun = new IndexedWord(tokensByIndex.get(n));
                                //	System.out.println("Case 1, noun: " + noun.toString());
                                //	} else {
                                //		System.out.println("Noun " + elementsByIndex.get(n) + " not found.");
                                //		System.out.println(tokensByIndex.toString());
                            }
                            if (verb != null && noun != null) {
                                SemanticGraphEdge edge = new SemanticGraphEdge(verb, noun, dobjRel, 1.0, false);
                                answ.add(edge);
                            }
                        }
                    }
                }
            }
            // Looking for prepositional group within nominal groups.
            Set<Integer> groupNom = findGroupNom();
            //	System.out.println("Looking within group.nom: " + groupNom.toString());
            if (groupNom != null && groupNom.size() > 0) {
                for (int gn : groupNom) {
                    List<Integer> govIdx = findNounInGroupIdx(gn);
                    List<Integer> spGroup = findPrepGroupInClauseIdx(gn);
                    //	System.out.println("Working with prep " + spGroup.toString() + " in group.nom");
                    //	System.out.println("Found nouns: " + govIdx.toString());
                    for (int sp : spGroup) {
                        List<Integer> depIdx = findNounInGroupIdx(sp);
                        for (int g : govIdx) {
                            IndexedWord gov = null;
                            if (tokensByIndex.containsKey(g)) {
                                gov = new IndexedWord(tokensByIndex.get(g));
                                //	System.out.println("gov: " + gov.toString());
                            }
                            for (int d : depIdx) {
                                IndexedWord dep = null;
                                if (tokensByIndex.containsKey(d)) {
                                    dep = new IndexedWord(tokensByIndex.get(d));
                                    //	System.out.println("dep: " + dep.toString());
                                }
                                if (gov != null && dep != null) {
                                    SemanticGraphEdge edge = new SemanticGraphEdge(gov, dep, nmodRel, 1.0, false);
                                    answ.add(edge);
                                }
                            }
                        }
                    }

                }
            }

            //				for (int sp : spGroups) {
            //					List<Integer> prepIdx = findPrepInSPGroupIdx(sp);
            //					for (int p : prepIdx) {
            //						String prep = null;
            //						Matcher prepMatcher = PREP_PATTERN.matcher(elementsByIndex.get(p));
            //						if (prepMatcher.find()) {
            //							prep = prepMatcher.group(1);
            //						}
            //						GrammaticalRelation rel = null;
            //						if (temporal.contains(prep)) {
            //							rel = nmodtmodRel;
            //						} else if ("por".equals(prep)) {
            //							rel = nmodagentRel;
            //						} else {
            //							rel = nmodRel;
            //						}
            //						for (int v : verbs) {
            //							if (tokensByIndex.containsKey(v)) {
            //								IndexedWord verb = new IndexedWord(tokensByIndex.get(v));
            //								for (int n : findNounInGroupIdx(sp)) {
            //									IndexedWord noun = new IndexedWord(tokensByIndex.get(n));
            //									if (noun != null && verb != null) {
            //										SemanticGraphEdge edge = new SemanticGraphEdge(verb, noun, rel, 1.0, false);
            //										answ.add(edge);
            //									}
            //								}
            //							}
            //						}
            //					}
            //				}
            // Now searching nmod like Di los juegos a tus niños: 'a tus niños' is in
            // sn - grup.nom of 'juegos'
        }

        return answ;
    }

    private Set<Integer> findAdjGroup() {
        Set<Integer> answ = new HashSet<Integer>();
        answ = elementsByName.get("s.a");
        Set<Integer> clauseIndices = findClause();
        if (clauseIndices != null) {
            for (int clauseIdx : clauseIndices) {
                for (int child : children.get(clauseIdx)) {
                    if (elementsByIndex.get(child) == "participi") {
                        answ.add(clauseIdx);
                        break;
                    }
                }
            }
        }
        return answ;
    }

    private Set<Integer> findAdvGroup() {
        return elementsByName.get("sadv");
    }

    public List<SemanticGraphEdge> extractAdvmod() {
        List<SemanticGraphEdge> answ = new ArrayList<SemanticGraphEdge>();
        IndexedWord gov = null;
        IndexedWord dep = null;

		/*advmod in an adjectival phrase*/
        Set<Integer> adjGroups = findAdjGroup();
        if (adjGroups != null) {
            for (int adjGpIdx : adjGroups) {
                int advIdx = findAdvSpecInGroupIdx(adjGpIdx);
                if (tokensByIndex.containsKey(advIdx))
                    dep = new IndexedWord(tokensByIndex.get(advIdx));
                List<Integer> adjIndices = findAdjInGroupIdx(adjGpIdx);
                for (int adjIdx : adjIndices) {
                    if (tokensByIndex.containsKey(adjIdx))
                        gov = new IndexedWord(tokensByIndex.get(adjIdx));
                    if (gov != null && dep != null) {
                        SemanticGraphEdge edge = new SemanticGraphEdge(gov, dep, advmodRel, 1.0, false);
                        gov = null;
                        dep = null;
                        answ.add(edge);
                    }
                }
            }
        }

		/*advmod in an adverbial phrase*/
        Set<Integer> advGroups = findAdvGroup();
        if (advGroups != null) {
            for (int advGpIdx : advGroups) {
                int advSpecIdx = findAdvSpecInGroupIdx(advGpIdx);
                if (tokensByIndex.containsKey(advSpecIdx))
                    dep = new IndexedWord(tokensByIndex.get(advSpecIdx));
                int advIdx = findAdvInGroupIdx(advGpIdx);
                if (tokensByIndex.containsKey(advIdx))
                    gov = new IndexedWord(tokensByIndex.get(advIdx));
                if (gov != null && dep != null) {
                    SemanticGraphEdge edge = new SemanticGraphEdge(gov, dep, advmodRel, 1.0, false);
                    gov = null;
                    dep = null;
                    answ.add(edge);
                }
            }
        }

		/*advmod: verb -> adv*/
        Set<Integer> clauseIndices = findSentence();
        if (findClause() != null) {
            clauseIndices.addAll(findClause());
        }
        for (int clauseIdx : clauseIndices) {
            List<Integer> verbIndices = findMainVerbInClauseIdx(clauseIdx);
            List<Integer> advIndices = findAdvInClauseIdx(clauseIdx);
            if (advIndices != null) {
                for (int verbIdx : verbIndices) {
                    if (tokensByIndex.containsKey(verbIdx))
                        gov = new IndexedWord(tokensByIndex.get(verbIdx));
                    for (int advIdx : advIndices) {
                        if (tokensByIndex.containsKey(advIdx))
                            dep = new IndexedWord(tokensByIndex.get(advIdx));
                        if (gov != null && dep != null) {
                            SemanticGraphEdge edge = new SemanticGraphEdge(gov, dep, advmodRel, 1.0, false);
                            gov = null;
                            dep = null;
                            answ.add(edge);
                        }
                    }
                }
            }
        }

		/*advmod: verb -> adj*/
        for (int clauseIdx : clauseIndices) {
            List<Integer> verbIndices = findMainVerbInClauseIdx(clauseIdx);
            List<Integer> adjIndices = findAdjInClauseIdx(clauseIdx);
            if (adjIndices != null) {
                for (int verbIdx : verbIndices) {
                    if (tokensByIndex.containsKey(verbIdx))
                        gov = new IndexedWord(tokensByIndex.get(verbIdx));
                    for (int adjIdx : adjIndices) {
                        if (tokensByIndex.containsKey(adjIdx))
                            dep = new IndexedWord(tokensByIndex.get(adjIdx));
                        if (gov != null && dep != null) {
                            SemanticGraphEdge edge = new SemanticGraphEdge(gov, dep, advmodRel, 1.0, false);
                            gov = null;
                            dep = null;
                            answ.add(edge);
                        }
                    }
                }
            }
        }

        return answ;
    }

    public String getTree() {
        StringBuilder sb = new StringBuilder();
        System.out.println("size of the tree: " + size());
        System.out.println("children size: " + children.size());
        for (int i = 0; i < size(); i++) {
            sb.append(elementsByIndex.get(i));
            sb.append(": ");
            sb.append(getChildren(i));
            sb.append("\n");
        }
        return sb.toString();
    }
}