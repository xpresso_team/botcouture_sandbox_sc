package com.abzooba.xcommerce.search;

import java.util.LinkedHashMap;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;

/**
 * The class contains the results of search on the Amazon CloudSearch
 * @author antarip.biswas
 *
 */
public class SearchItem {

	/**
	 * score is the search relevance score obtained from the CloudSearch
	 */

	private Map<String, String> attributeMap = new LinkedHashMap<String, String>();


	public SearchItem(Map<?, ?> dynamoDBSearchResult) {

		for (Map.Entry<?, ?> entry : dynamoDBSearchResult.entrySet()) {
			String keyStr = (String) entry.getKey();
			String valueStr = ((AttributeValue) entry.getValue()).getS();
			this.attributeMap.put(keyStr, valueStr);
		}
	}

	public SearchItem() {
		// TODO Auto-generated constructor stub
	}


	public void assignConfidence(double confidenceFactor) {
		double currConfidence = Double.parseDouble(this.getAttributeValue(SearchUtils.CONFIDENCE_KEY));
		double confidence = currConfidence * confidenceFactor / 100;
		this.addAttribute(SearchUtils.CONFIDENCE_KEY, String.valueOf(confidence));
	}

	public String getAttributeValue(String attribute) {
		return this.attributeMap.get(attribute);
	}

	public void addAttribute(String attribute, String value) {
		this.attributeMap.put(attribute, value);
	}

	public Map<String, String> getAttributeMap() {
		return attributeMap;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SearchItem [attributeMap=" + attributeMap + "]";
	}

}
