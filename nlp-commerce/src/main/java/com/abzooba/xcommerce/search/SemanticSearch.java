package com.abzooba.xcommerce.search;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCLogger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.neo4j.driver.v1.*;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;


public class SemanticSearch {
    private static Logger logger = XCLogger.getSpLogger();
    private static Driver driver = GraphDatabase.driver(XCConfig.NEO4J_SERVER_BOLT, AuthTokens.basic(XCConfig.NEO4J_USRNAME, XCConfig.NEO4J_PWD));

    private List QuerySemanticData(String label, String name, String relation, int direction, int distance) {
        Session session = driver.session();
        String distanceString, query = new String();
        List <String> resultArray = new ArrayList<String>();
        name = name.toLowerCase();
        if (name == null && name.isEmpty()) {
            return resultArray;
        }
        if (distance == 0) {
            distanceString = "";
        } else {
            distanceString = Integer.toString(distance);
        }
        if (direction == 0) {
            query = "MATCH (w1:" + label + "{ name:\"" + name.toLowerCase() + "\" })-[:" + relation.toLowerCase() + "*1.." + distanceString + "]-(w2:" + label + ") RETURN DISTINCT properties(w2)";
        }
        if (direction == 1) {
            query = "MATCH (w1:" + label + "{ name:\"" + name.toLowerCase() + "\" })-[:" + relation.toLowerCase() + "*1.." + distanceString + "]->(w2:" + label + ") RETURN DISTINCT properties(w2)";
        }
        if (direction == -1) {
            query = "MATCH (w1:" + label + "{ name:\"" + name.toLowerCase() + "\" })<-[:" + relation.toLowerCase() + "*1.." + distanceString + "]-(w2:" + label + ") RETURN DISTINCT properties(w2)";
        }
        StatementResult result = session.run(query);
        session.close();
        while (result.hasNext()) {
            Record record = result.next();
            String nameProperty = record.values().get(0).get("name").toString();
            resultArray.add(nameProperty);
        }
        return resultArray;
    }
    public JSONObject SemanticJsonUpdater(JSONObject intentJson) throws JSONException {
        long startTime = System.nanoTime();
        SemanticSearch sSearch = new SemanticSearch();
        String[] brandRelations = {"is_same_as","is_similar_to"};
        String[] entityRelations = {"is_same_as","is_similar_to"};
        String[] colorRelations = {"is_superset_of"};
        String[] detailRelations = {"is_same_as","is_similar_to"};

        JSONArray brandJsonArray = (JSONArray) intentJson.get("brand");
        JSONArray detailsJsonArray = (JSONArray) intentJson.get("details");
        JSONArray colorsJsonArray = (JSONArray) intentJson.get("colorsavailable");
        String tempEntity = intentJson.get("entity").toString();
        JSONObject tempJsonSemantic = new JSONObject();

        if (tempEntity.equalsIgnoreCase("anything")) {
            tempJsonSemantic.put("entity", tempEntity);
        }
        else {
            JSONObject tempJsonEntity = new JSONObject();
            for (int i = 0; i < 1; i++) {
                JSONObject tempJsonRelation = new JSONObject();
                for (String entityRelation : entityRelations) {
                    tempJsonRelation.put(entityRelation, new JSONArray(sSearch.QuerySemanticData("Product", tempEntity, entityRelation, 0, 10).toString()));
                }
                tempJsonEntity.put(tempEntity, tempJsonRelation);
            }
            tempJsonSemantic.put("entity", tempJsonEntity);
        }


        for(String keyName:new String[]{"price","size","xc_category","features","gender","xc_hierarchy_str"}) {
            tempJsonSemantic.put(keyName, intentJson.get(keyName));
        }


        JSONObject tempJsonBrand = new JSONObject();
        for(int i=0;i<brandJsonArray.length();i++){
            JSONObject tempJsonRelation = new JSONObject();
            String tempBrand = brandJsonArray.get(i).toString();
            for(String brandRelation:brandRelations){
                tempJsonRelation.put(brandRelation, new JSONArray(sSearch.QuerySemanticData("Brand",tempBrand,brandRelation,0,10).toString()));
            }
            tempJsonBrand.put(tempBrand, tempJsonRelation);
        }
        tempJsonSemantic.put("brand",tempJsonBrand);


        JSONObject tempJsonDetails = new JSONObject();
        for(int i=0;i<detailsJsonArray.length();i++){
            JSONObject tempJsonRelation = new JSONObject();
            String tempDetail = detailsJsonArray.get(i).toString();
            for(String detailRelation:detailRelations){
                tempJsonRelation.put(detailRelation, new JSONArray(sSearch.QuerySemanticData("Style",tempDetail,detailRelation,0,10).toString()));
            }
            tempJsonDetails.put(tempDetail, tempJsonRelation);
        }
        tempJsonSemantic.put("details", tempJsonDetails);

        JSONObject tempJsonColors = new JSONObject();
        for(int i=0;i<colorsJsonArray.length();i++){
            String tempColor = colorsJsonArray.get(i).toString();
            JSONObject tempJsonRelation = new JSONObject();
            for(String colorRelation:colorRelations){
                tempJsonRelation.put("is_subset_of", new JSONArray(sSearch.QuerySemanticData("Colour",tempColor,colorRelation,-1,10).toString()));
            }
            tempJsonColors.put(tempColor, tempJsonRelation);
        }
        tempJsonSemantic.put("colorsavailable", tempJsonColors);

        logger.info("Time taken to find all semantic results is: "+(float) (System.nanoTime() - startTime) / 1000000000);
        return tempJsonSemantic;

    }

    public JSONObject SemanticJsonUpdaterBrand(JSONObject intentJson) throws JSONException {
        long startTime = System.nanoTime();
        SemanticSearch sSearch = new SemanticSearch();
        String[] brandRelations = {"is_same_as"};
        JSONArray brandJsonArray = (JSONArray) intentJson.get("brand");

        List <String> tempBrandList = new ArrayList<>();

        for (int i = 0; i < brandJsonArray.length(); i++) {
            String tempBrand = brandJsonArray.get(i).toString();
            for(String brandRelation:brandRelations){
                tempBrandList.add(tempBrand);
                tempBrandList.addAll(sSearch.QuerySemanticData("Brand",tempBrand,brandRelation,0,10));
            }
        }
        intentJson.remove("brand");
        intentJson.put("brand", new JSONArray(tempBrandList.toString()));
        logger.info("Time taken to find all semantic results is: "+(float) (System.nanoTime() - startTime) / 1000000000);
        return intentJson;

    }
    /*public static void main(String[] args){
        SemanticSearch temp = new SemanticSearch();
        long startTime = System.nanoTime();
        System.out.println(temp.QuerySemanticData("Brand","ck","is_same_as",0,0));
        System.out.println(System.nanoTime() - startTime);

        startTime = System.nanoTime();
        System.out.println(temp.QuerySemanticData("Brand","levi's","is_same_as",0,0));
        System.out.println(System.nanoTime() - startTime);
    }*/
}