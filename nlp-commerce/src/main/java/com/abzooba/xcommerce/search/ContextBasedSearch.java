package com.abzooba.xcommerce.search;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import edu.stanford.nlp.ling.CoreLabel;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;

import com.abzooba.xcommerce.core.IntentAPI;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xcommerce.nlp.SpellChecker;
import com.abzooba.xcommerce.nlp.attribute.ColorSearch;
import com.abzooba.xcommerce.nlp.attribute.GenderSearch;
import com.abzooba.xcommerce.nlp.attribute.SearchByProductName;
import com.abzooba.xcommerce.nlp.attribute.SizeParser;
import com.abzooba.xcommerce.nlp.attribute.price.PriceParser;
import com.abzooba.xpresso.engine.core.XpText;

import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;

/**
 * Created by mayank on 27/2/17.
 */
public class ContextBasedSearch {

	private static Logger logger;
	private static XpText xpText;
	private static HashMap<CoreLabel, Double> sizeTokensWithConfidence = new HashMap<>();
	private static String priceString;
	private static String gender;
	private static String matchingProduct;
	private static String color;

	/**
	 * Based on the last query intent this function will run that functionality instead of running
	 * the whole nlp pipeline.
	 * @param queryParameters
	 * @param intentObj
	 * @return
	 */
	public static JSONObject getContextBasedDirectIntentJSON(Map<String, String[]> queryParameters, IntentAPI... intentObj) {
		logger = XCLogger.getSpLogger();
		String searchQuery = queryParameters.get("query")[0].toLowerCase();
		String spellCorrectedQuery = new SpellChecker().checkMySpelling(searchQuery);
		logger.info("XC spell correction : " + spellCorrectedQuery);
		if (!spellCorrectedQuery.equalsIgnoreCase(searchQuery)) {
			String bingSpellCheckQuery = new SpellChecker().MSSpellCheck(searchQuery);
			logger.info("Bing Spell correction : " + bingSpellCheckQuery);
			if (bingSpellCheckQuery != null)
				searchQuery = bingSpellCheckQuery;
			else
				searchQuery = spellCorrectedQuery;
		}
		xpText = new XpText(searchQuery);
		if ((queryParameters.containsKey(SearchUtils.PRICE_NUMERIC_KEY)) || (queryParameters.containsKey(SearchUtils.SIZE))) {
			List<CoreMap> sentencesList = xpText.getSentences();
			for (CoreMap sentence : sentencesList) {
				SemanticGraph dependencyGraph = xpText.getDependencyGraph(sentence);
				Tree parseTree = xpText.getParseTree(sentence);
				List<CoreLabel> tokens = xpText.getTokens(sentence);
				if (queryParameters.containsKey(SearchUtils.PRICE_NUMERIC_KEY)) {
					priceString = PriceParser.priceDetectionModuleExhaustive(parseTree, dependencyGraph, sentence.toString());
					logger.info("Price Details for Sentence : " + priceString);
				}
				if (queryParameters.containsKey(SearchUtils.SIZE)) {
					sizeTokensWithConfidence = SizeParser.sizeDetectionModule(parseTree, dependencyGraph, sentence.toString());
					logger.info("Size Details for Sentence : " + sizeTokensWithConfidence.keySet());
				}
			}
		} else if (queryParameters.containsKey(SearchUtils.GENDER_KEY)) {
			String[] matchedGender = GenderSearch.checkForGender(searchQuery);
			if (matchedGender != null)
				gender = matchedGender[1];
			logger.info("Gender Details for Sentence : " + gender);
		} else if (queryParameters.containsKey(SearchUtils.BRAND_KEY)) {
			matchingProduct = SearchByProductName.getMatchingProduct(searchQuery.toLowerCase());
			logger.info("Brand Details for Sentence : " + matchingProduct);
		} else if (queryParameters.containsKey(SearchUtils.COLOR_KEY)) {
			String colorString = searchQuery.replaceAll("[^a-zA-Z\\s]", "");
			color = ColorSearch.checkForColor(colorString);
			logger.info("Color Details for Sentence : " + color);
		}
		JSONObject intentJson = new JSONObject();
		try {
			intentJson.put("price", new JSONArray((priceString == null) ? new HashSet<String>() : new HashSet<String>(Arrays.asList(priceString))));
			if (sizeTokensWithConfidence.size() > 0)
				intentJson.put("size", new JSONArray(sizeTokensWithConfidence.size() > 0 ? new HashSet<String>(Arrays.asList(sizeTokensWithConfidence.keySet().iterator().next().word())) : new HashSet()));
			intentJson.put(SearchUtils.BRAND_KEY, new JSONArray((matchingProduct == null) ? new HashSet<String>() : new HashSet<String>(Arrays.asList(matchingProduct))));
			intentJson.put(SearchUtils.COLOR_KEY, new JSONArray((color == null) ? new HashSet<String>() : new HashSet<String>(Arrays.asList(color))));
			intentJson.put(SearchUtils.GENDER_KEY, new JSONArray((gender == null) ? new HashSet<String>() : new HashSet<String>(Arrays.asList(gender))));
		} catch (JSONException e) {
			logger.error("Error in creating intent json in ContextBasedSearch class : " + e);
		}
		logger.info("Intent Json : " + intentJson.toString());
		return intentJson;
	}
}
