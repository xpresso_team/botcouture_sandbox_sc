package com.abzooba.xcommerce.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.entity.ContentType;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.IntentAPI;
import com.abzooba.xcommerce.core.XCEntity;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xcommerce.domain.IntentUtils;
import com.abzooba.xcommerce.ontology.server.Neo4jServer;
import com.abzooba.xpresso.textanalytics.JWIController;

import edu.mit.jwi.item.POS;

/**
 * @author Sudhanshu Kumar Sep 15, 2016
 *
 */

public class SearchController {

	public static Logger logger = XCLogger.getSpLogger();

	public JSONObject startSearch(Map<String, String[]> structureQueryMap, String... customReturnAttributes) {

		int minSearchHits = XCConfig.MIN_SEARCH_HITS;
		JSONObject result = null;
		Map<String, String[]> relaxationMap = new HashMap<String, String[]>(structureQueryMap);

		try {
			if (relaxationMap.get(SearchUtils.ENTITY_KEY) != null) {
				result = structuredSearchApiCall(relaxationMap);
				int resultSize = result.getJSONArray("Results").length();
				logger.info("Result count : " + resultSize);

				String initialQueryString = structureQueryMap.get(SearchUtils.ENTITY_KEY)[0];
				if (resultSize > minSearchHits) {
					List<String> synonymousEntities = Neo4jServer.findSimilarConcept(initialQueryString);
					if (synonymousEntities != null) {
						Iterator<String> synonyms = synonymousEntities.iterator();
						while (synonyms.hasNext() && resultSize < minSearchHits) {
							String syn = synonyms.next();
							if (syn.trim().equalsIgnoreCase(initialQueryString.trim())) {
								logger.info("Synonym : " + syn + ". Main Entity : " + initialQueryString);
								continue;
							}
							relaxationMap.put(SearchUtils.ENTITY_KEY, new String[] { syn });
							relaxationMap.put(SearchUtils.PRODUCT_NAME_KEY, new String[] { syn });
							/* Step 5 */
							logger.info("Suggested Result - Neo4j synonym of the entity : " + syn);
							result = structuredSearchApiCall(relaxationMap);
						}
					}
				}

				resultSize = result.getJSONArray("Results").length();
				if (resultSize < minSearchHits) {
					relaxationMap = new HashMap<String, String[]>(structureQueryMap);
					List<String> synonymousEntities = JWIController.getSynonyms(initialQueryString, POS.NOUN);
					if (synonymousEntities != null) {
						Iterator<String> synonyms = synonymousEntities.iterator();
						while (synonyms.hasNext() && resultSize < minSearchHits) {
							String syn = synonyms.next();
							if (syn.trim().equalsIgnoreCase(initialQueryString.trim())) {
								logger.info("Synonym : " + syn + ". Main Entity : " + initialQueryString);
								continue;
							}
							relaxationMap.put(SearchUtils.ENTITY_KEY, new String[] { syn });
							relaxationMap.put(SearchUtils.PRODUCT_NAME_KEY, new String[] { syn });
							/* Step 6 */
							logger.info("Suggested Result - WordNet synonym of the entity : " + syn);
							result = structuredSearchApiCall(relaxationMap);
						}
					}
				}

				resultSize = result.getJSONArray("Results").length();
				if (resultSize < minSearchHits) {
					relaxationMap = new HashMap<String, String[]>(structureQueryMap);
					if (relaxationMap.get(SearchUtils.PRICE_NUMERIC_KEY) != null) {
						/* Step 7 Search without price filter */
						logger.info("Suggested Result - No Price");
						relaxationMap.put(SearchUtils.PRICE_NUMERIC_KEY, new String[] { "" });
						result = structuredSearchApiCall(relaxationMap);
					}
				}

				resultSize = result.getJSONArray("Results").length();
				if (resultSize < minSearchHits) {
					relaxationMap = new HashMap<String, String[]>(structureQueryMap);
					if (relaxationMap.get(SearchUtils.FEATURE_KEY) != null) {
						/* Step 8 */
						logger.info("Suggested Result - No Feature key");
						relaxationMap.put(SearchUtils.FEATURE_KEY, new String[] { "" });
						result = structuredSearchApiCall(relaxationMap);
					}
				}

				resultSize = result.getJSONArray("Results").length();
				/* "Ringer top for my sister". "ringer" is present in product name and not details. */
				if (resultSize < minSearchHits) {
					relaxationMap = new HashMap<String, String[]>(structureQueryMap);
					if (relaxationMap.get(SearchUtils.GENERIC_KEY) != null) {
						/* Step 9 */
						String genericKey = "";
						String[] genericKeySet = relaxationMap.get(SearchUtils.GENERIC_KEY);
						for (String generic : genericKeySet) {
							genericKey = generic + " ";
						}

						String genericEntity = initialQueryString + " " + genericKey.trim();
						logger.info("Suggested Result - Generic key Search with entity");
						relaxationMap.put(SearchUtils.ENTITY_KEY, new String[] { genericEntity });
						result = structuredSearchApiCall(relaxationMap);
					}
				}

				resultSize = result.getJSONArray("Results").length();
				/* "ringer top for my sister" with productname "top" return 4 results, 
				 * but if we remove it, it returns 105 results. Mostly tops are named tees in productname */
				if (resultSize < minSearchHits) {
					relaxationMap = new HashMap<String, String[]>(structureQueryMap);
					if (relaxationMap.get(SearchUtils.GENERIC_KEY) != null) {
						/* Step 9.1 */
						String genericKey = "";
						String[] genericKeySet = relaxationMap.get(SearchUtils.GENERIC_KEY);
						for (String generic : genericKeySet) {
							genericKey = generic + " ";
						}
						String genericEntity = initialQueryString + " " + genericKey.trim();
						logger.info("Suggested Result - Generic key Search with entity | No product name");
						relaxationMap.put(SearchUtils.ENTITY_KEY, new String[] { genericEntity });
						relaxationMap.put(SearchUtils.PRODUCT_NAME_KEY, new String[] { "" });
						result = structuredSearchApiCall(relaxationMap);
					}
				}

				resultSize = result.getJSONArray("Results").length();
				if (resultSize < minSearchHits) {
					relaxationMap = new HashMap<String, String[]>(structureQueryMap);
					if (relaxationMap.get(SearchUtils.GENERIC_KEY) != null) {
						/* Step 9.2 */
						logger.info("Suggested Result - No Generic key");
						relaxationMap.put(SearchUtils.GENERIC_KEY, new String[] { "" });
						result = structuredSearchApiCall(relaxationMap);
					}
				}

				resultSize = result.getJSONArray("Results").length();
				if (resultSize < minSearchHits) {
					relaxationMap = new HashMap<String, String[]>(structureQueryMap);
					if (relaxationMap.get(SearchUtils.PRODUCT_NAME_KEY) != null) {
						/* Search without product name filter */
						/* Step 10 */
						logger.info("Suggested Result - No Product Name");
						relaxationMap.put(SearchUtils.PRODUCT_NAME_KEY, new String[] { "" });
						result = structuredSearchApiCall(relaxationMap);
					}
				}

				resultSize = result.getJSONArray("Results").length();
				if (resultSize < minSearchHits) {
					relaxationMap = new HashMap<String, String[]>(structureQueryMap);
					if (relaxationMap.get(SearchUtils.FEATURE_KEY) != null) {
						/* Step 11 Search without price filter */
						logger.info("Suggested Result - No Feature | No Price)");
						relaxationMap.put(SearchUtils.FEATURE_KEY, new String[] { "" });
						relaxationMap.put(SearchUtils.PRICE_NUMERIC_KEY, new String[] { "" });
						result = structuredSearchApiCall(relaxationMap);
					}
				}

				resultSize = result.getJSONArray("Results").length();
				if (resultSize < minSearchHits) {
					relaxationMap = new HashMap<String, String[]>(structureQueryMap);
					if (relaxationMap.get(SearchUtils.COLOR_KEY) != null) {
						/* Step 12 Search without price filter */
						logger.info("Suggested Result - No Color | No Price)");
						relaxationMap.put(SearchUtils.COLOR_KEY, new String[] { "" });
						relaxationMap.put(SearchUtils.PRICE_NUMERIC_KEY, new String[] { "" });
						result = structuredSearchApiCall(relaxationMap);
					}
				}

				resultSize = result.getJSONArray("Results").length();
				if (resultSize < minSearchHits) {
					relaxationMap = new HashMap<String, String[]>(structureQueryMap);
					if (relaxationMap.get(SearchUtils.PRODUCT_NAME_KEY) != null) {
						/* Step 13 Search without price filter */
						logger.info("Suggested Result - No Product name | No Price)");
						relaxationMap.put(SearchUtils.PRODUCT_NAME_KEY, new String[] { "" });
						relaxationMap.put(SearchUtils.PRICE_NUMERIC_KEY, new String[] { "" });
						result = structuredSearchApiCall(relaxationMap);
					}
				}

				resultSize = result.getJSONArray("Results").length();
				if (resultSize < minSearchHits) {
					relaxationMap = new HashMap<String, String[]>(structureQueryMap);
					String wordVectorCategory = structureQueryMap.get(SearchUtils.XC_CATEGORY_KEY)[0];
					if (wordVectorCategory != null) {
						if (!wordVectorCategory.equalsIgnoreCase(initialQueryString)) {
							/* Step 14 */
							logger.info("Suggested Result - Word-Vector Category : " + wordVectorCategory);
							relaxationMap.put(SearchUtils.ENTITY_KEY, new String[] { wordVectorCategory });
							result = structuredSearchApiCall(relaxationMap);
						}
						resultSize = result.getJSONArray("Results").length();
						if (resultSize < minSearchHits) {
							relaxationMap = new HashMap<String, String[]>(structureQueryMap);
							/* Step 15 */
							logger.info("Suggested Result - Word-Vector Category : " + wordVectorCategory + " | No Product Name");
							relaxationMap.put(SearchUtils.ENTITY_KEY, new String[] { wordVectorCategory });
							relaxationMap.put(SearchUtils.PRODUCT_NAME_KEY, new String[] { "" });
							result = structuredSearchApiCall(relaxationMap);
						}
						resultSize = result.getJSONArray("Results").length();
						if (resultSize < minSearchHits) {
							relaxationMap = new HashMap<String, String[]>(structureQueryMap);
							/* Step 16 */
							logger.info("Suggested Result - Word-Vector Category : " + wordVectorCategory + " | No Product Name | No Price");
							relaxationMap.put(SearchUtils.ENTITY_KEY, new String[] { wordVectorCategory });
							relaxationMap.put(SearchUtils.PRODUCT_NAME_KEY, new String[] { "" });
							relaxationMap.put(SearchUtils.PRICE_NUMERIC_KEY, new String[] { "" });
							result = structuredSearchApiCall(relaxationMap);
						}
					}
				}

				resultSize = result.getJSONArray("Results").length();
				if (resultSize < minSearchHits) {
					relaxationMap = new HashMap<String, String[]>(structureQueryMap);
					if (relaxationMap.get(SearchUtils.GENERIC_KEY) != null) {
						/* Step 17 */
						logger.info("Suggested Result - No Price | Generic | Product Name");
						relaxationMap.put(SearchUtils.GENERIC_KEY, new String[] { "" });
						relaxationMap.put(SearchUtils.PRODUCT_NAME_KEY, new String[] { "" });
						relaxationMap.put(SearchUtils.PRICE_NUMERIC_KEY, new String[] { "" });
						result = structuredSearchApiCall(relaxationMap);
					}
				}

				resultSize = result.getJSONArray("Results").length();
				if (resultSize < minSearchHits) {
					relaxationMap = new HashMap<String, String[]>(structureQueryMap);
					if (relaxationMap.get(SearchUtils.FEATURE_KEY) != null) {
						/* Step 18 */
						logger.info("Suggested Result - No Price | Generic | Features | Product name");
						relaxationMap.put(SearchUtils.PRICE_NUMERIC_KEY, new String[] { "" });
						relaxationMap.put(SearchUtils.GENERIC_KEY, new String[] { "" });
						relaxationMap.put(SearchUtils.FEATURE_KEY, new String[] { "" });
						relaxationMap.put(SearchUtils.PRODUCT_NAME_KEY, new String[] { "" });
						result = structuredSearchApiCall(relaxationMap);
					}
				}

				resultSize = result.getJSONArray("Results").length();
				if (resultSize < minSearchHits) {
					relaxationMap = new HashMap<String, String[]>(structureQueryMap);
					if (relaxationMap.get(SearchUtils.COLOR_KEY) != null) {
						/* Step 19 */
						logger.info("Suggested Result - No Price | Color | Generic | Feature | Product Name");
						relaxationMap.put(SearchUtils.PRICE_NUMERIC_KEY, new String[] { "" });
						relaxationMap.put(SearchUtils.COLOR_KEY, new String[] { "" });
						relaxationMap.put(SearchUtils.GENERIC_KEY, new String[] { "" });
						relaxationMap.put(SearchUtils.FEATURE_KEY, new String[] { "" });
						relaxationMap.put(SearchUtils.PRODUCT_NAME_KEY, new String[] { "" });
						result = structuredSearchApiCall(relaxationMap);
					}
				}

				resultSize = result.getJSONArray("Results").length();
				if (resultSize < minSearchHits) {
					relaxationMap = new HashMap<String, String[]>(structureQueryMap);
					logger.info("Suggested Result - No Filters");
					/* Step 23 */
					relaxationMap.put(SearchUtils.PRODUCT_NAME_KEY, new String[] { "" });
					relaxationMap.put(SearchUtils.PRICE_NUMERIC_KEY, new String[] { "" });
					relaxationMap.put(SearchUtils.COLOR_KEY, new String[] { "" });
					relaxationMap.put(SearchUtils.GENERIC_KEY, new String[] { "" });
					relaxationMap.put(SearchUtils.BRAND_KEY, new String[] { "" });
					relaxationMap.put(SearchUtils.FEATURE_KEY, new String[] { "" });
					result = structuredSearchApiCall(relaxationMap);
				}

				/*undergarments. Cases when we identified correct category but the main entity is not present any where in the document of the item*/
				resultSize = result.getJSONArray("Results").length();
				if (resultSize < minSearchHits) {
					relaxationMap = new HashMap<String, String[]>(structureQueryMap);
					/* Step 24 */
					logger.info("Suggested Result - Only categories");
					relaxationMap.put(SearchUtils.ENTITY_KEY, new String[] { "" });
					relaxationMap.put(SearchUtils.PRODUCT_NAME_KEY, new String[] { "" });
					relaxationMap.put(SearchUtils.PRICE_NUMERIC_KEY, new String[] { "" });
					relaxationMap.put(SearchUtils.COLOR_KEY, new String[] { "" });
					relaxationMap.put(SearchUtils.GENERIC_KEY, new String[] { "" });
					relaxationMap.put(SearchUtils.BRAND_KEY, new String[] { "" });
					relaxationMap.put(SearchUtils.FEATURE_KEY, new String[] { "" });
					result = structuredSearchApiCall(relaxationMap);
				}

			} else if (relaxationMap.get(SearchUtils.BRAND_KEY) != null) {
				relaxationMap.put(SearchUtils.ENTITY_KEY, structureQueryMap.get(SearchUtils.BRAND_KEY));
				logger.info("Suggested Result - Only Brands");
				relaxationMap.put(SearchUtils.PRODUCT_NAME_KEY, new String[] { "" });
				relaxationMap.put(SearchUtils.PRICE_NUMERIC_KEY, new String[] { "" });
				relaxationMap.put(SearchUtils.COLOR_KEY, new String[] { "" });
				relaxationMap.put(SearchUtils.GENERIC_KEY, new String[] { "" });
				relaxationMap.put(SearchUtils.XC_CATEGORY_KEY, new String[] { "" });
				relaxationMap.put(SearchUtils.FEATURE_KEY, new String[] { "" });
				result = structuredSearchApiCall(relaxationMap);
			}

			JSONObject jsonObjectClarificationNode = IntentUtils.getClarification(structureQueryMap, relaxationMap);
			result.put("Clarification", jsonObjectClarificationNode);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public JSONObject structuredSearchApiCall(Map<String, String[]> relaxationQuery) {

		JSONObject result = null;
		URL obj = null;
		HttpsURLConnection con = null;
		StringBuilder urlSb = null;
		try {
			urlSb = new StringBuilder("https://");
			urlSb.append("stgapi.xpresso.ai");
			urlSb.append("/datalayer/v1/cloudsearch/structuredSearch/?");

			for (Map.Entry<String, String[]> entry : relaxationQuery.entrySet()) {
				String key = entry.getKey();
				String[] value = entry.getValue();
				//				logger.info(key.trim() + " ------ " + Arrays.deepToString(value) + " ---------- " + value.length);
				if (value != null && !value[0].isEmpty()) {
					urlSb.append("&");
					urlSb.append(key.trim());
					urlSb.append("=");
					urlSb.append(URLEncoder.encode(value[0].trim(), "UTF-8"));
				}
			}
			obj = new URL(urlSb.toString());

			if (obj != null) {
				con = (HttpsURLConnection) obj.openConnection();
				// optional default is GET
				if (con != null) {
					con.setRequestMethod("GET");
					con.setRequestProperty("Content-Type", ContentType.APPLICATION_JSON.getMimeType() + ";charset=utf-8");
					con.setRequestProperty("Accept", ContentType.APPLICATION_JSON.getMimeType());

					int statusCode = con.getResponseCode();
					logger.info("\n\n" + urlSb.toString());
					logger.info("Response Code:\t" + statusCode);

					if (statusCode >= 400 && statusCode < 500) {
						logger.info("Cloud Search Request Exception:\t" + con.getContent().toString());
					} else if (statusCode >= 500 && statusCode < 600) {
						logger.info("Internal Server Error. Please try again as this might be a transient error condition.");
					} else {
						BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
						String inputLine;
						StringBuffer responseBody = new StringBuffer();

						while ((inputLine = in.readLine()) != null) {
							responseBody.append(inputLine);
						}
						in.close();
						result = new JSONObject(responseBody.toString());

					}
				}
			}
		} catch (JSONException | IOException e) {
			// TODO : Print stacktrace as string using Throwable.printStackTrace(PrintWriter pw) 
			logger.error("Error occured in AmazonCloudSearchResult getSearch\n" + e.getMessage() + "\n");
			e.printStackTrace();
		}
		return result;
	}

	/*Removes duplicates from result set*/
	public List<SearchItem> getUniqueResultSet(List<SearchItem> resultSet) {
		Map<String, SearchItem> uniqueResultMap = new HashMap<String, SearchItem>();
		for (SearchItem item : resultSet) {
			if (item.getAttributeValue("xc_sku") != null) {
				uniqueResultMap.put(item.getAttributeValue("xc_sku"), item);
			}
		}
		List<SearchItem> list = new ArrayList<SearchItem>(uniqueResultMap.values());
		if (list.isEmpty()) {
			list = resultSet;
		}
		return list;
	}

	public void getSearchResultsForEntity(XCEntity spEntity, String... customReturnAttributes) {

		Set<String> detailKey = spEntity.getAttribute(SearchUtils.GENERIC_KEY);
		if (detailKey != null) {
			Iterator<String> iter = spEntity.getAttribute(SearchUtils.GENERIC_KEY).iterator();
			while (iter.hasNext()) {
				String key = iter.next();
				if (key.equalsIgnoreCase(SearchUtils.COLOR_KEY) || key.equalsIgnoreCase(SearchUtils.GENDER_KEY)) {
					iter.remove();
				}
			}
		}

		/*remove color and entity if it is in brand name. eg- black tie, red herring. */
		String originalQuery = spEntity.getQuery().getOriginalQuery();
		Set<String> colorKey = spEntity.getAttribute(SearchUtils.COLOR_KEY);
		if (colorKey != null) {
			Iterator<String> iter = spEntity.getAttribute(SearchUtils.COLOR_KEY).iterator();
			Set<String> brands = spEntity.getAttribute(SearchUtils.BRAND_KEY);
			if (brands != null && !brands.isEmpty())
				for (String brand : brands) {
					String colorCheckQuery = originalQuery;
					while (iter.hasNext()) {
						String key = iter.next();
						if (brand.contains(key)) {
							/* cases like "red shoes from red herring" or black shirt from black tie*/
							colorCheckQuery = colorCheckQuery.replaceFirst(key, "");
							if (!colorCheckQuery.contains(key))
								iter.remove();
						}
					}
				}
		}

		logger.info("Main Entity : " + spEntity.getEntityStr() + ". Generic Key : " + spEntity.getAttribute(SearchUtils.GENERIC_KEY));
		SearchAttributes attribute = new SearchAttributes(spEntity, customReturnAttributes);
		logger.info(attribute.toString());

		/* If Intent API is called return the map else go for structured search */
		IntentAPI intentObj = spEntity.getQuery().getIntentObject();
		if (intentObj != null) {
			logger.info("Finding intent for : " + spEntity.getAttributeMap().toString());
			intentObj.prepareIntentJson(spEntity);
		} else {
		}
	}

}
