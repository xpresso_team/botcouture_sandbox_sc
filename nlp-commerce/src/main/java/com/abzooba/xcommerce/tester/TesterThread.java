/**
 * 
 */
package com.abzooba.xcommerce.tester;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCEngine;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xcommerce.search.SearchUtils;

/**
 * @author vivek aditya
 * @author Sunit Sushil
 * TesterThread.java
 * 2:16:13 pm 03-Jun-2016
 */
public class TesterThread implements Runnable {
	List<Object> line;
	static Logger logger;
	ArrayList<String> issues = new ArrayList<String>();

	public TesterThread(List<Object> line) {
		this.line = line;
		logger = XCLogger.getSpLogger();
	}

	@Override
	public void run() {
		try {
			String[] parts = new String[12];
			int index = 0;
			for (Object value : line) {
				parts[index] = (String) value;
				index++;
			}
			while (index < 12) {
				parts[index] = "";
				index++;
			}
			String slNo = parts[0];
			String query = parts[1];
			String expectedColor = parts[2];
			String expectedPrice = parts[4];
			String expectedGender = parts[5];
			String expectedOccasion = parts[6];
			String expectedBrand = parts[7];
			String expectedProductName = parts[9];
			Map<String, String[]> queryParameters = new HashMap<String, String[]>();
			queryParameters.put("page", new String[] { "1" });
			queryParameters.put("perPage", new String[] { String.valueOf(XCConfig.MAX_SEARCH_HITS) });
			queryParameters.put("query", new String[] { query });
			queryParameters.put("src", new String[] { XCConfig.SRC });
			JSONObject json = new JSONObject(XCEngine.getSearchResultsJSON(queryParameters));
			JSONArray jArray = json.getJSONArray("Results");
			for (int i = 0; i < Math.min(jArray.length(), XCConfig.MAX_ITEMS_PROCESS); i++) {
				JSONObject projson = jArray.getJSONObject(i);
				projson = dirtyFix(projson);
				String currentPrice = projson.getString(SearchUtils.PRICE_NUMERIC_KEY);
				JSONArray currentColorArr = projson.getJSONArray(SearchUtils.COLOR_KEY);
				String currentGender = projson.getString(SearchUtils.GENDER_KEY);
				String currentProductName = projson.getString(SearchUtils.PRODUCT_NAME_KEY);
				JSONArray currentSubCategory = projson.getJSONArray(SearchUtils.SUBCATEGORY_KEY);
				String currentDetails = projson.getString(SearchUtils.GENERIC_KEY);
				String currentFeatures = projson.getString(SearchUtils.FEATURE_KEY);

				String brandAvailable = projson.getString(SearchUtils.BRAND_KEY);
				String sku = projson.getString(SearchUtils.SKU_KEY);
				float intprice = Float.parseFloat(currentPrice);
				if ((!expectedPrice.isEmpty()) && (!priceMatch(intprice, expectedPrice))) {
					String outputStr = slNo + "\t" + query + "\t" + "Price is incorrect" + "\t" + expectedPrice + "\t" + sku;
					log(projson, outputStr, "Price is incorrect");
					logger.info(outputStr);
				} else if ((!expectedProductName.isEmpty()) && (!prodMatch(currentProductName, currentSubCategory, expectedProductName))) {
					String outputStr = slNo + "\t" + query + "\t" + "Product Name is incorrect" + "\t" + expectedProductName + "\t" + sku;
					log(projson, outputStr, "Product Name is incorrect");
					logger.info(outputStr);
				} else if ((!expectedOccasion.isEmpty()) && (!occasionMatch(currentDetails, currentFeatures, currentProductName, expectedOccasion))) {
					String outputStr = slNo + "\t" + query + "\t" + "Occasion or theme is incorrect" + "\t" + expectedOccasion + "\t" + sku;
					log(projson, outputStr, "Occasion or theme is incorrect");
					logger.info(outputStr);
				} else if ((!expectedBrand.isEmpty()) && (!brandMatch(brandAvailable, expectedBrand))) {
					String outputStr = slNo + "\t" + query + "\t" + "Brand is incorrect" + "\t" + expectedBrand + "\t" + sku;
					log(projson, outputStr, "Brand is incorrect");
					logger.info(outputStr);
				}
				else if ((!expectedGender.isEmpty()) && (!stringEquals(currentGender, expectedGender))) {
					String outputStr = slNo + "\t" + query + "\t" + "Gender is incorrect" + "\t" + expectedGender + "\t" + sku;
					log(projson, outputStr, "Gender is incorrect");
					logger.info(outputStr);
				} else if ((!expectedColor.isEmpty()) && (!colorCheck(currentColorArr, expectedColor))) {
					String outputStr = slNo + "\t" + query + "\t" + "Color is incorrect" + "\t" + expectedColor + "\t" + sku;
					log(projson, outputStr, "Color is incorrect");
					logger.info(outputStr);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.info(this.line.toString());
		}
	}

	/**
	 * @param jObject Json
	 * @return Clean Json
	 */
	JSONObject dirtyFix(JSONObject jObject) {
		JSONObject result = new JSONObject();
		try {
			Iterator<?> keys = jObject.keys();
			while (keys.hasNext()) {
				String key = (String) keys.next();
				String value = (String) jObject.get(key);
				if (key.equals("colorsavailable") || key.equals("subcategory")) {
					result.put(key, Arrays.asList(value.replaceAll("\\[|\\]", "").split(",")));
				} else {
					result.put(key, value);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}

	void log(JSONObject jObject, String line, String error) {
		if (!issues.contains(error)) {
			if (XCTester.failedQueryCount.containsKey(error)) {
				XCTester.failedQueryCount.put(error, XCTester.failedQueryCount.get(error) + 1);
			} else {
				XCTester.failedQueryCount.put(error, 1);
			}
			issues.add(error);
		}
		if (XCConfig.IS_OUTPUT_NEED) {
			jObject.remove("image");
			XCTester.addToOutputLines(line + "\t" + jObject.toString());
		}
	}

	static boolean colorCheck(JSONArray jsonArray, String color) {
		try {
			for (int i = 0, size = jsonArray.length(); i < size; i++) {
				if (stringContains(jsonArray.get(i).toString(), color)) {
					return true;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	static boolean stringEquals(String t1, String t2) {
		return t1.trim().toLowerCase().equals(t2.trim().toLowerCase());
	}

	static boolean stringContains(String t1, String t2) {
		return t1.trim().toLowerCase().contains(t2.trim().toLowerCase());
	}

	static boolean prodMatch(String t1, JSONArray t2, String t3) {
		try {
			for (int i = 0, size = t2.length(); i < size; i++) {
				if (stringContains(t2.get(i).toString(), t3)) {
					return true;
				} else if (stringContains(t1.toString(), t3)) {
					return true;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	static boolean categoryMatch(JSONArray t1, JSONArray t2, String t3) {
		try {
			for (int i = 0, size = t1.length(); i < size; i++) {
				if (stringContains(t1.get(i).toString(), t3)) {
					return true;
				} else if (stringContains(t2.get(i).toString(), t3)) {
					return true;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	static boolean occasionMatch(String t1, String t2, String t3, String t4) {
		if (stringContains(t1, t4)) {
			return stringContains(t1, t4);
		} else if (stringContains(t2, t4)) {
			return stringContains(t2, t4);
		} else if (stringContains(t3, t4)) {
			return stringContains(t3, t4);
		} else
			return false;
	}

	static boolean brandMatch(String t1, String t2) {
		return t1.trim().toLowerCase().contains(t2.trim().toLowerCase());
	}

	static boolean sizeMatch(JSONArray t2, String t1) {
		try {
			for (int i = 0, size = t2.length(); i < size; i++) {
				if (stringEquals(t2.get(i).toString(), t1)) {
					return true;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	static boolean priceMatch(Float t1, String t2) {
		if (t2.contains("&")) {
			String[] splitedString = t2.split("&");
			float lowerValue = Float.parseFloat(splitedString[0]);
			float upperValue = Float.parseFloat(splitedString[1]);
			return t1 >= lowerValue || t1 <= upperValue;
		} else if (t2.contains("<")) {
			t2 = t2.replace("<", "");
			float value = Float.parseFloat(t2);
			return t1 <= value;
		} else if (t2.contains(">")) {
			t2 = t2.replace(">", "");
			float value = Float.parseFloat(t2);
			return t1 >= value;
		} else {
			float value = Float.parseFloat(t2);
			return t1 == value;
		}

	}
}
