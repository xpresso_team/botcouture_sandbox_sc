/**
 * 
 */
package com.abzooba.xcommerce.tester;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCEngine;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xcommerce.search.SearchUtils;
import com.abzooba.xcommerce.utils.FileIO;
import com.abzooba.xcommerce.utils.SpreadSheetReader;
import com.abzooba.xcommerce.utils.ThreadUtils;

/**
 * @author vivek aditya
 *
 * SpTester.java
 * 10:07:32 am 03-Jun-2016
 */
public class XCTester {
	private static Logger logger;
	static ConcurrentHashMap<String, Integer> failedQueryCount = new ConcurrentHashMap<String, Integer>();

	public static void main(String[] args) {
		try {
			XCEngine.init();
			logger = XCLogger.getSpLogger();
			SearchUtils.addToReturnAttributes(SearchUtils.FEATURE_KEY, "xcfashion");
			SearchUtils.addToReturnAttributes("xcfashion", SearchUtils.GENERIC_KEY);
			logger.info("SPREADSHEET_ID:\t" + XCConfig.SPREADSHEET_ID + "QUERY_ID\t" + XCConfig.QUERY_ID + "NO_OF_THREADS\t" + XCConfig.NO_OF_THREAD);
			PrintWriter pw = new PrintWriter(new FileWriter(XCConfig.OUTPUT_FILENAME));
			double reviewSetStartTime = System.currentTimeMillis();
			List<List<Object>> queryList = SpreadSheetReader.getTestData(XCConfig.SPREADSHEET_ID, XCConfig.RANGE);
			tester(queryList);
			double reviewSetTimeTaken = ((System.currentTimeMillis() - reviewSetStartTime) / 1000);
			logger.info("Time taken to search query set of size " + queryList.size() + ": " + reviewSetTimeTaken + " seconds. = " + (reviewSetTimeTaken / 60) + " minutes.");
			Integer count = 0;
			for (Entry<String, Integer> entry : failedQueryCount.entrySet()) {
				logger.info("Issue: " + entry.getKey() + "\tCount: " + entry.getValue());
				count = count + entry.getValue();
			}
			logger.info("Total issues: " + count);
			pw.close();
			System.exit(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void tester(List<List<Object>> queryList) {
		ExecutorService executor = Executors.newFixedThreadPool(XCConfig.NO_OF_THREAD);
		try {
			for (int i = 0; i < queryList.size(); i++) {
				List<Object> currLine = queryList.get(i);
				if (currLine.size() > XCConfig.QUERY_ID) {
					Runnable worker = new TesterThread(currLine);
					executor.execute(worker);
				}
			}
			ThreadUtils.shutdownAndAwaitTermination(executor);
			if (XCConfig.IS_OUTPUT_NEED) {
				failedQueryCount.forEach((key, value) -> {
					String out = "Issue:\t" + key + "\tCount:\t" + value;
					addToOutputLines(out);
				});
				FileIO.write_file(XCConfig.OUTPUT_FILE_LINES, XCConfig.OUTPUT_FILENAME, false);
			}
		} finally {
			executor.shutdownNow();
		}
	}

	public static void addToOutputLines(String strLine) {
		synchronized (XCConfig.OUTPUT_FILE_LINES) {
			XCConfig.OUTPUT_FILE_LINES.add(strLine);
		}
	}

}
