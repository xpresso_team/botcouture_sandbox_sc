package com.abzooba.xcommerce.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;

import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.utils.FileIO;

/**
 * @author vivek aditya
 *
 * ReadSpreadsheet.java
 * 5:40:04 pm 01-Jul-2016
 * TODO
 */

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ValueRange;

/**
 * @author vivek aditya
 *
 * SpreadSheetReader.java
 * 5:40:58 pm 11-Jul-2016
 */
public class SpreadSheetReader {
	static Logger logger;
	/** Application name. */
	private static final String APPLICATION_NAME = "Google Sheets API";
	/** Directory to store user credentials for this application. */
	private static java.io.File DATA_STORE_DIR;
	/** Global instance of the FileDataStoreFactory. */
	private static FileDataStoreFactory DATA_STORE_FACTORY;
	/** Global instance of the JSON factory. */
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	/** Global instance of the HTTP transport. */
	private static HttpTransport HTTP_TRANSPORT;
	/** Global instance of the scopes required by this quickstart.
	 *
	 * If modifying these scopes, delete your previously saved credentials
	 * at ~/.credentials/sheets.googleapis.com-java-quickstart.json
	 */
	private static final List<String> SCOPES = Arrays.asList(SheetsScopes.SPREADSHEETS);

	/**
	 * Creates an authorized Credential object.
	 * @return an authorized Credential object.
	 * @throws IOException
	 */
	public static Credential authorize() throws IOException {
		DATA_STORE_DIR = new java.io.File("conf");
		try {
			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}
		DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
		InputStream in = SpreadSheetReader.class.getResourceAsStream("client_secret.json");
		if (in == null) {
			in = FileIO.getInputStreamFromFileName("client_secret.json");
		}
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES).setDataStoreFactory(DATA_STORE_FACTORY).setAccessType("offline").build();
		Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
		logger.info("Credentials saved to " + DATA_STORE_DIR.getAbsolutePath());
		return credential;
	}

	/**
	 * Build and return an authorized Sheets API client service.
	 * @return an authorized Sheets API client service
	 * @throws IOException
	 */
	public static Sheets getSheetsService() throws IOException {
		Credential credential = authorize();
		return new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
	}

	/**
	 * 
	 * @return the Sheet data as List of List
	 * @throws IOException
	 */
	public static List<List<Object>> getTestData(String spreadsheetId, String range) throws IOException {
		logger = XCLogger.getSpLogger();
		Sheets service = getSheetsService();
		double startTime = System.currentTimeMillis();
		ValueRange response = service.spreadsheets().values().get(spreadsheetId, range).execute();
		logger.info("Time Taken to Read SpreadSheet: " + ((System.currentTimeMillis() - startTime) / 1000) + " seconds.");
		return response.getValues();
	}
}