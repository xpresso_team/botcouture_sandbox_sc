package com.abzooba.xcommerce.utils;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Stack;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.nlp.LoadNLPFiles;
import com.abzooba.xpresso.textanalytics.CoreNLPController;
import com.abzooba.xpresso.utils.FileIO;

public class StringAttributeUtils {

	private static Set<String> currencySet;

	public static void init() {
		currencySet = LoadNLPFiles.getCuurencySet();
	}

	/**
	 * The method checks if the the string is a currency
	 * @param currentStr
	 * @return
	 */
	public static boolean isCurrency(String currentStr) {
		if (currencySet.contains(currentStr))
			return true;
		if (currencySet.contains(CoreNLPController.lemmatizeToString(currentStr)))
			return true;
		String[] strArr = currentStr.replaceAll("\\(", " ").replaceAll("\\)", " ").split(" ");
		for (int i = 0; i < strArr.length; i++) {
			if (currencySet.contains(strArr[i]))
				return true;
		}
		return false;
	}

}
