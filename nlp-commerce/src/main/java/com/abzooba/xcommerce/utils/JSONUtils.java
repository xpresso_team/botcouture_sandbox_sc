package com.abzooba.xcommerce.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;

import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.utils.FileIO;

public class JSONUtils {
	public static Logger logger = XCLogger.getSpLogger();

	//
	/**
	 * The method converts the mappings of granular categories to corresponding subcategories
	 * for a single domain into a JSONObject 
	 * @param domain
	 * @param inputFile
	 * @return 
	 */
	public static JSONObject generateMappingJSON(String domain, String inputFile) {
		JSONObject domainObj = new JSONObject();
		try {
			domainObj.put("Domain Name", domain);
			Stack<String> lineStack = new Stack<String>();
			FileIO.read_file(inputFile, lineStack);
			JSONObject mapObj = new JSONObject();
			while (!lineStack.isEmpty()) {
				String line = lineStack.pop();
				String[] lineArr = line.split("\t");
				if (lineArr.length < 2)
					continue;
				String granularCatg = lineArr[0].toUpperCase();
				JSONArray mapArr = new JSONArray();
				for (int i = 1; i < lineArr.length; i++) {
					String catg = lineArr[i].toUpperCase();
					mapArr.put(catg);
				}
				mapObj.put(granularCatg, mapArr);
			}
			domainObj.put("Mappings", mapObj);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return domainObj;
	}

	/**
	 * The method takes a file of line-mapped subcategories for granular categories and converts them 
	 * to a single json.  
	 * @param inputFileMap
	 * @param outputFile
	 */
	public static void generateJSONForDomainMap(Map<String, String> inputFileMap, String outputFile) {
		JSONArray domainArr = new JSONArray();
		JSONObject detailsObj = new JSONObject();
		JSONArray attributesArr = new JSONArray();
		for (Entry<String, String> entry : inputFileMap.entrySet()) {
			JSONObject domainObj = generateMappingJSON(entry.getKey(), entry.getValue());
			domainArr.put(domainObj);
		}

		try {
			JSONObject tempObject = new JSONObject();
			tempObject.put("Attribute Name", "PRODUCT TYPE");
			JSONArray categoriesList = new JSONArray();
			tempObject.put("Categories", categoriesList);
			attributesArr.put(tempObject);
			detailsObj.put("Attributes", attributesArr);
			detailsObj.put("Domain", domainArr);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		logger.info(detailsObj.toString());
		try {
			FileWriter writer = new FileWriter(new File(outputFile));
			writer.write(detailsObj.toString());
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		Map<String, String> inputMap = new LinkedHashMap<String, String>();
		inputMap.put("iconic", "input/iconic-categories-map.txt");
		inputMap.put("xcfashion", "input/debenhams-categories-map.txt");
		logger.info(generateMappingJSON("xcfashion", "input/debenhams-categories-map.tsv").toString());
	}

}
