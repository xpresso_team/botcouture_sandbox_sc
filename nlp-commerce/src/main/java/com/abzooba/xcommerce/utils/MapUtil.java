package com.abzooba.xcommerce.utils;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Alix Melchy Sep 22, 2016
 *
 */
public class MapUtil {
	/**
	 * @param map
	 * @param desc
	 * @return
	 * This function returns a sorted version of map according to the value of its entries.
	 * This function returns a LinkedHashMap to enforce a deterministic order in the returned map.
	 * If desc the list is sorted in reverse order.
	 */
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map, boolean desc) {
		List<Map.Entry<K, V>> list = new LinkedList<>(map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
			@Override
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
				if (desc)
					return -(o1.getValue()).compareTo(o2.getValue());
				else
					return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		Map<K, V> result = new LinkedHashMap<>();
		for (Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}

	/**
	 * @param map
	 * @param desc
	 * @return
	 * This function returns a sorted list (LinkedList) of the keys in map according to the associated value in map.
	 * If desc the list is sorted in reverse order.
	 */
	public static <K, V extends Comparable<? super V>> List<K> getKeysSortedByValue(Map<K, V> map, boolean desc) {
		List<Map.Entry<K, V>> list = new LinkedList<>(map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
			@Override
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
				if (desc)
					return -(o1.getValue()).compareTo(o2.getValue());
				else
					return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		List<K> result = new LinkedList<>();
		for (Map.Entry<K, V> entry : list) {
			result.add(entry.getKey());
		}
		return result;
	}
}
