package com.abzooba.xcommerce.utils;

/**
 * 
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.abzooba.xcommerce.config.XCConfig;

/**
 * @author Koustuv Saha
 * 10-Mar-2014 4:59:38 pm
 * XpressoV2 FileIO
 */
public class FileIO {

	public static void read_file(String fileName, Collection<String> collection, int max_lines) {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF8"));
			String strLine;
			int count = 0;
			if (XCConfig.HAS_COLUMN_NAME) {
				br.readLine();
			}
			while ((strLine = br.readLine()) != null) {
				collection.add(strLine);
				count++;
				if (count >= max_lines) {
					break;
				}
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void read_file(String fileName, Collection<String> collection) {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF8"));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				collection.add(strLine);
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void write_file(List<String> triples_strList, String fileName, boolean isAppend) {
		File file = new File(fileName);
		File parentFile = file.getParentFile();
		if (!parentFile.exists()) {
			parentFile.mkdir();
		}
		try {
			PrintWriter pw;
			pw = new PrintWriter(new FileWriter(file, isAppend));
			for (String str : triples_strList) {
				pw.println(str);
			}
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void write_in_file(String reviewResult, String fileName, boolean isAppend) {
		File file = new File(fileName);
		File parentFile = file.getParentFile();
		if (!parentFile.exists()) {
			parentFile.mkdir();
		}
		try {
			PrintWriter pw = new PrintWriter(new FileWriter(file, isAppend));
			pw.println(reviewResult);
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void write_file(Map<?, ?> map, String fileName, boolean isAppend, String delimiter) {
		List<String> keyValueList = new ArrayList<String>();
		for (Map.Entry<?, ?> entry : map.entrySet()) {
			keyValueList.add(entry.getKey() + delimiter + entry.getValue());
		}
		write_file(keyValueList, fileName, isAppend);
	}

	public static synchronized void write_fileNIO(String line, String fileName, boolean isAppend) {
		Path file = null;
		AsynchronousFileChannel asynchFileChannel = null;
		String filePath = fileName;
		try {
			if (fileName != null) {
				file = Paths.get(filePath);
				asynchFileChannel = AsynchronousFileChannel.open(file, StandardOpenOption.WRITE, StandardOpenOption.CREATE);
				CompletionHandler<Integer, Object> handler = new CompletionHandler<Integer, Object>() {
					public void completed(Integer result, Object attachment) {
					}

					public void failed(Throwable e, Object attachment) {
						e.printStackTrace();
					}
				};
				asynchFileChannel.write(ByteBuffer.wrap(line.getBytes()), asynchFileChannel.size(), "", handler);
				asynchFileChannel.close();
			} else {
				System.out.println(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static synchronized void write_fileNIO(Collection<?> collection, String fileName, boolean isAppend) {
		Path file = null;
		AsynchronousFileChannel asynchFileChannel = null;
		String filePath = fileName;
		try {
			StringBuilder sb = new StringBuilder();
			for (Object str : collection) {
				sb.append(str.toString()).append("\n");
			}
			if (fileName != null) {

				file = Paths.get(filePath);
				asynchFileChannel = AsynchronousFileChannel.open(file, StandardOpenOption.WRITE, StandardOpenOption.CREATE);

				CompletionHandler<Integer, Object> handler = new CompletionHandler<Integer, Object>() {
					public void completed(Integer result, Object attachment) {
					}

					public void failed(Throwable e, Object attachment) {
						e.printStackTrace();
					}
				};
				asynchFileChannel.write(ByteBuffer.wrap(sb.toString().getBytes()), asynchFileChannel.size(), "", handler);
				asynchFileChannel.close();
			} else {
				System.out.println(sb.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		List<String> justCheckList = Arrays.asList("ab", "bc");
		write_file(justCheckList, "./bullshitchalance/bull.txt", false);
	}
}
