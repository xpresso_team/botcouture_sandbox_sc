package com.abzooba.xcommerce.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.slf4j.Logger;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCEngine;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.engine.config.XpConfig;
import com.abzooba.xpresso.textanalytics.JWIController;
import com.abzooba.xpresso.utils.FileIO;

import edu.mit.jwi.item.POS;

public class WordNetUtils {
	public static Logger logger = XCLogger.getSpLogger();

	public static void init() {
		XpConfig.WORDNET_LOC = XCConfig.WORDNET_DIR;
		try {
			JWIController.init();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * The method takes a seed list for price qualifiers and checks the wordnet to expand the list
	 * @param inputFile
	 * @param outputFile
	 */
	public static void expandPriceQualifierList(String inputFile, String outputFile) {
		Stack<String> seedStack = new Stack<String>();
		List<String> expandedList = new ArrayList<String>();
		FileIO.read_file(inputFile, seedStack);
		Set<String> alreadyFoundStrSet = new LinkedHashSet<String>();
		while (!seedStack.isEmpty()) {
			String currentStr = seedStack.pop();
			String[] splitArr = currentStr.split("\t");
			if (splitArr.length != 2)
				continue;
			expandedList.add(currentStr);
			if (splitArr[0].contains("["))
				splitArr[0] = splitArr[0].substring(0, splitArr[0].indexOf("["));
			List<String> tempList = JWIController.getSynonyms(splitArr[0], POS.ADVERB);
			if (tempList != null) {
				for (String tempStr : tempList) {
					if (alreadyFoundStrSet.contains(tempStr))
						continue;
					expandedList.add(tempStr + "\t" + splitArr[1]);
					alreadyFoundStrSet.add(tempStr);
				}
			}
			tempList = JWIController.getSynonyms(splitArr[0], POS.ADJECTIVE);
			if (tempList == null)
				continue;
			for (String tempStr : tempList) {
				if (alreadyFoundStrSet.contains(tempStr))
					continue;
				expandedList.add(tempStr + "\t" + splitArr[1]);
				alreadyFoundStrSet.add(tempStr);
			}

			tempList = JWIController.getSynonyms(splitArr[0], POS.NOUN);
			if (tempList != null) {
				for (String tempStr : tempList) {
					if (alreadyFoundStrSet.contains(tempStr))
						continue;
					expandedList.add(tempStr + "\t" + splitArr[1]);
					alreadyFoundStrSet.add(tempStr);
				}
			}
			tempList = JWIController.getSynonyms(splitArr[0], POS.VERB);
			if (tempList == null)
				continue;
			for (String tempStr : tempList) {
				if (alreadyFoundStrSet.contains(tempStr))
					continue;
				expandedList.add(tempStr + "\t" + splitArr[1]);
				alreadyFoundStrSet.add(tempStr);
			}

			tempList = JWIController.getHypernyms(splitArr[0], POS.ADJECTIVE);
			if (tempList != null) {
				for (String tempStr : tempList) {
					if (alreadyFoundStrSet.contains(tempStr))
						continue;
					expandedList.add(tempStr + "\t" + splitArr[1]);
					alreadyFoundStrSet.add(tempStr);
				}
			}

			tempList = JWIController.getHypernyms(splitArr[0], POS.ADVERB);
			if (tempList != null) {
				for (String tempStr : tempList) {
					if (alreadyFoundStrSet.contains(tempStr))
						continue;
					expandedList.add(tempStr + "\t" + splitArr[1]);
					alreadyFoundStrSet.add(tempStr);
				}
			}

			tempList = JWIController.getHypernyms(splitArr[0], POS.NOUN);
			if (tempList != null) {
				for (String tempStr : tempList) {
					if (alreadyFoundStrSet.contains(tempStr))
						continue;
					expandedList.add(tempStr + "\t" + splitArr[1]);
					alreadyFoundStrSet.add(tempStr);
				}
			}

			tempList = JWIController.getHypernyms(splitArr[0], POS.VERB);
			if (tempList != null) {
				for (String tempStr : tempList) {
					if (alreadyFoundStrSet.contains(tempStr))
						continue;
					expandedList.add(tempStr + "\t" + splitArr[1]);
					alreadyFoundStrSet.add(tempStr);
				}
			}

			tempList = JWIController.getHyponyms(splitArr[0], POS.ADJECTIVE);
			if (tempList != null) {
				for (String tempStr : tempList) {
					if (alreadyFoundStrSet.contains(tempStr))
						continue;
					expandedList.add(tempStr + "\t" + splitArr[1]);
					alreadyFoundStrSet.add(tempStr);
				}
			}

			tempList = JWIController.getHyponyms(splitArr[0], POS.ADVERB);
			if (tempList != null) {
				for (String tempStr : tempList) {
					if (alreadyFoundStrSet.contains(tempStr))
						continue;
					expandedList.add(tempStr + "\t" + splitArr[1]);
					alreadyFoundStrSet.add(tempStr);
				}
			}

			tempList = JWIController.getHyponyms(splitArr[0], POS.NOUN);
			if (tempList != null) {
				for (String tempStr : tempList) {
					if (alreadyFoundStrSet.contains(tempStr))
						continue;
					expandedList.add(tempStr + "\t" + splitArr[1]);
					alreadyFoundStrSet.add(tempStr);
				}
			}

			tempList = JWIController.getHyponyms(splitArr[0], POS.VERB);
			if (tempList != null) {
				for (String tempStr : tempList) {
					if (alreadyFoundStrSet.contains(tempStr))
						continue;
					expandedList.add(tempStr + "\t" + splitArr[1]);
					alreadyFoundStrSet.add(tempStr);
				}
			}

		}
		FileIO.write_file(expandedList, outputFile, false);
	}

	public static void main(String[] args) {
		XCEngine.init();
		init();
		System.out.println(JWIController.getSynonyms("golden", POS.ADJECTIVE));
	}

}
