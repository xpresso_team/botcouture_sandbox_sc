/**
 * 
 */
package com.abzooba.xcommerce.domain.xcfashion;

import com.abzooba.xcommerce.core.XCEntity;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xcommerce.core.XCQuery;
import com.abzooba.xcommerce.nlp.DependencyParse;
import com.abzooba.xcommerce.nlp.attribute.*;
import com.abzooba.xcommerce.search.SearchUtils;
import com.abzooba.xcommerce.utils.StringAttributeUtils;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.expressions.sentiment.XpSentiment;
import com.abzooba.xpresso.textanalytics.PosTags;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.UniversalEnglishGrammaticalRelations;
import org.slf4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.Map.Entry;

/**
 * @author Sudhanshu Kumar
 * 10-Apr-2014 11:50:28 am
 * XpressoV2.0.1 StanfordDependencyGraph
 */
public class DependencyParseXCFashion implements DependencyParse {

	protected static final Class<?> c = DependencyParseXCFashion.class;
	protected static final Logger logger = XCLogger.getSpLogger();

	protected SemanticGraph dependencyGraph;
	protected List<SemanticGraphEdge> sortedEdges;
	protected Set<SemanticGraphEdge> checkedEdge;

	protected Map<IndexedWord, String> multiWordEntityMap;
	protected Map<String, String> nerMap;

	protected XCQuery query;

	private int entityCount;

	protected Languages language;

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#getEdgesAsStr()
	 */
	@Override
	public String getEdgesAsStr() {
		if (sortedEdges != null) {
			return sortedEdges.toString();
		} else {
			return "[]";
		}
	}

	public DependencyParseXCFashion(XCQuery query, SemanticGraph dependency_graph) {
		language = Languages.EN;
		checkedEdge = new HashSet<SemanticGraphEdge>();
		if (dependency_graph != null) {
			this.dependencyGraph = dependency_graph;
			this.setSortedEdges();

			this.query = query;
			this.multiWordEntityMap = new HashMap<IndexedWord, String>();
			this.entityCount = 0;
			logger.info("Dependency Graph:\n" + this.dependencyGraph.toString(SemanticGraph.OutputFormat.LIST));
		}
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#getDependencyGraph()
	 */
	@Override
	public SemanticGraph getDependencyGraph() {
		return dependencyGraph;
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#getSortedEdges()
	 */
	@Override
	public List<SemanticGraphEdge> getSortedEdges() {
		return sortedEdges;
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#setSortedEdges()
	 */
	@Override
	public void setSortedEdges() {
		this.sortedEdges = this.dependencyGraph.edgeListSorted();
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#getMultiWordEntityMap()
	 */
	@Override
	public Map<IndexedWord, String> getMultiWordEntityMap() {
		return multiWordEntityMap;
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#getNERMap()
	 */
	@Override
	public Map<String, String> getNERMap() {
		return nerMap;
	}

	private void addToMultiWordEntityMap(IndexedWord entity, String multiWordEntity) {
		logger.info("Adding MultiWordEntity " + entity + "\t" + multiWordEntity);
		this.multiWordEntityMap.put(entity, multiWordEntity);
	}

	private XCEntity createEntity(String entityStr) {
		boolean isRejectEntity = XCEntity.isRejectEntity(entityStr, query);

		if (!isRejectEntity) {
			XCEntity entity = new XCEntity(entityStr, this.query, false);
			this.query.addToEntityMap(entityStr, entity);
			this.entityCount += 1;
			logger.info("Created an Entity for: " + entityStr);
			logger.info("entityCount = " + this.entityCount);
			return entity;
		} else {
			if (entityStr.contains(SearchUtils.ALL_CATEGORIES)) {
				this.query.setHasAllCategories(true);
				logger.info("All categories key word: " + entityStr);
				XCEntity entity = new XCEntity(SearchUtils.ALL_CATEGORIES, this.query, false);
				this.query.addToEntityMap(SearchUtils.ALL_CATEGORIES, entity);
				return entity;
			}
			logger.info("Entity rejected: " + entityStr);
			return null;
		}
	}

	private XCEntity retrieveEntity(String entityStr) {
		XCEntity entity = this.query.getEntity(entityStr);
		if (entity == null)
			entity = createEntity(entityStr);

		return entity;
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#goeswith_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void goeswith_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#ref_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void ref_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#root_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void root_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#xsubj_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void xsubj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#acomp_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void acomp_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#advcl_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void advcl_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

		/* cases like "I am going hiking and need shoes" */
		String depWord = dep.word().toLowerCase();
		Set<IndexedWord> descendants = this.dependencyGraph.descendants(gov);
		for (IndexedWord descendant : descendants) {
			XCEntity descendantEntity = retrieveEntity(descendant.word());
			if (descendantEntity != null) {
				logger.info("ADVCL: adding " + depWord + " to features of " + descendant.word());
				descendantEntity.setAttribute(SearchUtils.GENERIC_KEY, depWord);
				query.getIntentObject().setIndexMap(descendantEntity.getEntityStr(), SearchUtils.GENERIC_KEY, dep);

				break;
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#discourse_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void discourse_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#possessive_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void possessive_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#number_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void number_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#predet_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void predet_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#punct_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void punct_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#prt_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void prt_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#expl_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void expl_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#mwe_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void mwe_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#tmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void tmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#npmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void npmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#npadvmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void npadvmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#nummod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void nummod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		num_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#num_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void num_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#poss_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void poss_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#case_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 * case like "find me pair of socks", "leather laceup shoes"
	 */
	@Override
	public void case_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		String govWord = gov.word().toLowerCase();
		String depWord = dep.word().toLowerCase();

		XCEntity spEntity = retrieveEntity(govWord);
		if (spEntity != null)
			logger.info("CASE: gov: " + govWord + " dep: " + depWord + "\tentity: " + spEntity);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#acl_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void acl_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#advmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void advmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

		String depWord = dep.word().toLowerCase();
		String govWord = gov.word().toLowerCase();
		String depSentiTag = XpSentiment.getLexTag(Languages.EN, depWord, null, null);
		if (depSentiTag != null) {
			XCEntity spEntity = retrieveEntity(govWord);
			if (spEntity != null) {
				String entityStr = spEntity.getEntityStr();
				logger.info("ADVMOD: " + entityStr + "\tStripping off: " + depWord);
				entityStr = entityStr.replaceAll("\\b" + depWord + "\\b", "");
				spEntity.setEntityStr(entityStr);
				spEntity.setDirectSearchOnly(depWord.equalsIgnoreCase("only"));
			} else {
				Set<IndexedWord> parents = this.dependencyGraph.getParents(gov);
				for (IndexedWord parent : parents) {
					XCEntity parentEntity = retrieveEntity(parent.word());
					if (parentEntity != null) {
						String entityStr = parentEntity.getEntityStr();
						logger.info("ADVMOD: " + entityStr + "\tStripping off: " + depWord);
						entityStr = entityStr.replaceAll("\\b" + depWord + "\\b", "");
						parentEntity.setEntityStr(entityStr);
						parentEntity.setDirectSearchOnly(depWord.equalsIgnoreCase("only"));
						break;
					}
				}
			}
		} else {
			XCEntity spEntity = retrieveEntity(govWord);
			if (spEntity != null) {
				logger.info("ADVMOD: adding " + depWord + " to features of " + govWord);
				spEntity.setAttribute(SearchUtils.GENERIC_KEY, depWord);
				query.getIntentObject().setIndexMap(spEntity.getEntityStr(), SearchUtils.GENERIC_KEY, dep);
			} else {
				Set<IndexedWord> parents = this.dependencyGraph.getParents(gov);
				boolean foundParentEntity = false;
				for (IndexedWord parent : parents) {
					XCEntity parentEntity = retrieveEntity(parent.word());
					if (parentEntity != null) {
						foundParentEntity = true;
						logger.info("ADVMOD: adding " + depWord + " to features of " + parent.word());
						parentEntity.setAttribute(SearchUtils.GENERIC_KEY, depWord);
						query.getIntentObject().setIndexMap(parentEntity.getEntityStr(), SearchUtils.GENERIC_KEY, dep);
						break;
					}
				}
				/*
				 * show me tiaras
				 */
				if (!foundParentEntity) {
					spEntity = retrieveEntity(depWord);
					logger.info("ADVMOD: creating an entity from dependent " + depWord);
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#agent_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void agent_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#amod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void amod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

		logger.info("AMOD: " + gov + " is qualified by " + dep + " and v2= " + v2);
		String govWord = gov.word().toLowerCase();
		String depWord = dep.word().toLowerCase();
		XCEntity entity = null;

		/* multiword entity such as "Can you show me salwar suit from biba below 650 rupees" */
		entity = retrieveEntity(depWord + " " + govWord);

		if (!govWord.equals("color") && entity == null) {
			entity = retrieveEntity(govWord);
			logger.info("Obtaining entity (case 1): " + entity);
		}
		if (entity == null) {
			/*watches in purple color*/
			Set<IndexedWord> govWordsList = this.dependencyGraph.getParents(gov);
			for (IndexedWord currGov : govWordsList) {
				entity = retrieveEntity(currGov.word().toLowerCase());
				if (entity != null) {
					logger.info("Obtaining entity (case 2): " + entity);
					govWord = currGov.word();
					break;
				}
			}
		}

		if (entity == null) {
			/* <products> that have <color> color in it
			 * Necessary to search for the grandparents of this gov */
			Set<IndexedWord> parentsList = this.dependencyGraph.getParents(gov);
			for (IndexedWord parent : parentsList) {
				Set<IndexedWord> govWordsList = this.dependencyGraph.getParents(parent);
				for (IndexedWord currGov : govWordsList) {
					entity = retrieveEntity(currGov.word().toLowerCase());
					if (entity != null) {
						logger.info("Obtaining entity (case 3): " + entity);
						govWord = currGov.word();
						break;
					}
				}
			}
		}

		if (entity != null) {
			logger.info("Setting govWord " + govWord + " in entity " + entity);
			entity.setGovWord(govWord);

			/* Issue with Silver pendant: silver is a color and a material but only color identified */
			String originalEntityStr = entity.getEntityStr();
			boolean isColor = ColorSearch.spotColor(dep, entity);
			String noColorEntityStr = entity.getEntityStr();
			logger.info("noColorEntityStr: " + noColorEntityStr);
			entity.setEntityStr(originalEntityStr);
			boolean isMaterial = MaterialSearch.spotMaterial(depWord, entity);
			String noMaterialEntityStr = entity.getEntityStr();
			logger.info("noMaterialEntityStr: " + noMaterialEntityStr);
			if (isColor && isMaterial) {
				logger.info("Exit AMOD on color and material");
				return;
			}
			if (isColor) {
				entity.setEntityStr(noColorEntityStr);
				logger.info("Exit AMOD on color");
				return;
			}
			if (isMaterial) {
				logger.info("Exit AMOD on material");
				return;
			}
			boolean isGender = GenderSearch.spotGender(dep, entity, false);
			if (isGender) {
				logger.info("Exit AMOD on gender");
				return;
			}
			boolean isTheme = ThematicSearch.spotTheme(depWord, entity);
			if (isTheme) {
				logger.info("Exit AMOD on theme");
				return;
			}
			/*
			 * ISSUE: "skinny" is NEG, skinny jeans is thus not properly processed
			 * TODO
			 * POSSIBLE SOLUTION: file with adjectives existing in the name of sub-categories to create exceptions to this filtering
			 */
			String depSentiTag = XpSentiment.getLexTag(Languages.EN, depWord, null, null);
			logger.info("depSentiTag: " + depSentiTag);
			if (XpSentiment.POSITIVE_TAG.equals(depSentiTag) || XpSentiment.UNCONDITIONAL_POSITIVE_TAG.equals(depSentiTag) || XpSentiment.NEGATIVE_TAG.equals(depSentiTag) || XpSentiment.UNCONDITIONAL_NEGATIVE_TAG.equals(depSentiTag)) {
				logger.info("Exit AMOD on sentiment: " + depSentiTag);
				return;
			}

			logger.info("Set AMOD Attribute: " + depWord + " in entity " + entity);
			entity.setAttribute(SearchUtils.GENERIC_KEY, depWord);
			query.getIntentObject().setIndexMap(entity.getEntityStr(), SearchUtils.GENERIC_KEY, dep);
			/* E.g. flat boots */
			if (depWord != null && depWord.matches("\\w+")) {
				/* In case there is a nn multiWordEntity, e.g. "full sleeve shirt" */
				String multiWordEntity = entity.getMultiWordEntity();
				logger.info("multiWordEntity prior to AMOD update: " + multiWordEntity);
				if (multiWordEntity == null)
					multiWordEntity = govWord;
				String netEntity = depWord + " " + multiWordEntity;
				logger.info("AMOD: multiWordEntity for " + entity + " is " + netEntity);
				entity.setMultiWordEntity(netEntity);
			}
		}

		/* "I am going to the beach and I need flip flops" */
		if (entity == null) {
			entity = retrieveEntity(depWord + " " + govWord);
		}
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#quantmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void quantmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#appos_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void appos_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#abbrev_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void abbrev_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#aux_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void aux_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#pcomp_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void pcomp_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		// logger.info("Auxpass " + v2 + " is " + v1);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#csubjpass_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void csubjpass_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		// logger.info("Auxpass " + v2 + " is " + v1);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#preconj_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void preconj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		// logger.info("Auxpass " + v2 + " is " + v1);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#auxpass_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void auxpass_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		// logger.info("Auxpass " + v2 + " is " + v1);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#cc_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void cc_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#clausalSentiClassifier(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord)
	 */
	@Override
	public void clausalSentiClassifier(IndexedWord gov, IndexedWord dep) {

	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#ccomp_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void ccomp_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		/* I want to see some socks 
		 * 'socks' lemmatized as 'sock' and then tagged as VBP
		 * Necessary to create an entity for 'sock' and it only appears in ccomp 
		 * */
		logger.info("CCOMP: gov: " + gov + "\tdep: " + dep);
		String depWord = dep.word();
		XCEntity entity = retrieveEntity(depWord);

		/* "the color code for my upcoming office party is black, need a dress urgently." */
		if (entity == null) {
			IndexedWord modifier = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
			if (modifier != null) {
				entity = retrieveEntity(modifier.word());
				if (entity != null) {
					ColorSearch.spotColor(dep.word(), dep.tag(), entity);
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#csubj_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void csubj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#conj_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void conj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		/* red and green dresses */
		logger.info("CONJ: gov: " + gov + "\tdep: " + dep);
		String govWord = gov.word();
		String depWord = dep.word();
		String colorGov = ColorSearch.checkForColor(govWord);
		String colorDep = ColorSearch.checkForColor(depWord);
		if (colorGov != null && colorDep != null) {
			Set<IndexedWord> parents = this.dependencyGraph.getParents(gov);
			for (IndexedWord parent : parents) {
				XCEntity entity = retrieveEntity(parent.word());
				if (entity != null) {
					entity.setAttribute(SearchUtils.COLOR_KEY, colorGov);
					query.getIntentObject().setIndexMap(entity.getEntityStr(), SearchUtils.COLOR_KEY, gov);
					logger.info("Added color: " + colorGov + " to entity " + entity);
					entity.setAttribute(SearchUtils.COLOR_KEY, colorDep);
					query.getIntentObject().setIndexMap(entity.getEntityStr(), SearchUtils.COLOR_KEY, dep);
					logger.info("Added color " + colorDep + " to entity " + entity);
					return;
				}
			}
			Set<IndexedWord> children = this.dependencyGraph.getChildren(gov);
			for (IndexedWord child : children) {
				XCEntity entity = retrieveEntity(child.word());
				if (entity != null) {
					entity.setAttribute(SearchUtils.COLOR_KEY, colorGov);
					query.getIntentObject().setIndexMap(entity.getEntityStr(), SearchUtils.COLOR_KEY, gov);
					logger.info("Added color: " + colorGov + " to entity " + entity);
					entity.setAttribute(SearchUtils.COLOR_KEY, colorDep);
					query.getIntentObject().setIndexMap(entity.getEntityStr(), SearchUtils.COLOR_KEY, dep);
					logger.info("Added color " + colorDep + " to entity " + entity);
					return;
				}
			}
		} else if (colorGov != null) {
			/* checking whether dep is an entity: find me red and green dress */
			XCEntity entity = retrieveEntity(depWord);
			if (entity != null) {
				entity.setAttribute(SearchUtils.COLOR_KEY, colorGov);
				query.getIntentObject().setIndexMap(entity.getEntityStr(), SearchUtils.COLOR_KEY, gov);
				logger.info("Added color: " + colorGov + " to entity " + entity);
				return;
			}
		} else {
			/* checking whether dep is an entity: Can you show me red and yellow dresses */
			XCEntity entity = retrieveEntity(depWord);
			if (entity != null) {
				Set<IndexedWord> children = this.dependencyGraph.getChildren(gov);
				for (IndexedWord child : children) {
					String color = ColorSearch.checkForColor(child.word());
					if (color != null) {
						entity.setAttribute(SearchUtils.COLOR_KEY, color);
						query.getIntentObject().setIndexMap(entity.getEntityStr(), SearchUtils.COLOR_KEY, child);
						logger.info("Added color " + color + " to entity " + entity);
					}
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#cop_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void cop_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#dep_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void dep_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		logger.info("DEP: " + gov + "\t" + dep);
		/* Processing adjectives in case of erroneous POS tagging of the noun;
		 * e.g. "white socks" as "sock is tagged VBP, dep(white, sock) */
		if (dep.tag().equals("JJ"))
			amod_fireRule(gov, v2, dep, edgeRelation);
		if (gov.tag().equals("JJ"))
			amod_fireRule(dep, v2, gov, edgeRelation);
		/* Processing thematic attributes;
		 * Due to erroneous tagging, dep and gov are symmetric */
		String govWord = gov.word();
		String depWord = dep.word();
		XCEntity spEntity = retrieveEntity(govWord);
		boolean isTheme = ThematicSearch.spotTheme(depWord, spEntity);
		if (isTheme) {
			logger.info("DEP: spotted theme: " + depWord + "; for entity: " + govWord);
			return;
		}

		spEntity = retrieveEntity(depWord);
		isTheme = ThematicSearch.spotTheme(govWord, spEntity);
		if (isTheme) {
			logger.info("DEP: spotted theme: " + govWord + "; for entity: " + depWord);
			return;
		}
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#det_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void det_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#dobj_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void dobj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		logger.info("Dobj: " + gov + "\t" + dep);

		/*Swimming costume*/
		String govWord = gov.word().toLowerCase();
		String depWord = dep.word().toLowerCase();

		XCEntity spEntity = retrieveEntity(depWord);

		if (spEntity != null) {
			if (!XCEntity.isRejectEntity(govWord, query) && !XpSentiment.NEED_TAG.equals(XpSentiment.getLexTag(Languages.EN, govWord, null, null))) {
				spEntity.setAttribute(SearchUtils.GENERIC_KEY, govWord);
				query.getIntentObject().setIndexMap(spEntity.getEntityStr(), SearchUtils.GENERIC_KEY, gov);
				spEntity.setGovWord(govWord);
				String netEntity = govWord + " " + depWord;
				spEntity.setMultiWordEntity(netEntity);
				logger.info("DOBJ: setting multiWoldEntity to: " + netEntity + " in entity: " + spEntity);
			}
			boolean isTheme = ThematicSearch.spotTheme(govWord, spEntity);
			if (isTheme) {
				logger.info("DOBJ: found theme: " + govWord + "; for entity: " + spEntity.toString());
			} else {
				/* hiking jacket */
				if (this.sortedEdges.size() == 1) {
					spEntity.setAttribute(SearchUtils.GENERIC_KEY, govWord);
					query.getIntentObject().setIndexMap(spEntity.getEntityStr(), SearchUtils.GENERIC_KEY, gov);
				}
			}
		}

		/* "flip flops" */
		if (spEntity == null) {
			logger.info(depWord + "_" + govWord);
			spEntity = retrieveEntity(govWord + " " + depWord);
		}
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#iobj_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void iobj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#neg_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void neg_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#name_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void name_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#compound_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void compound_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		String relationName = edgeRelation.toString();
		String govWord = gov.word().toLowerCase();
		String depWord = dep.word().toLowerCase();
		if (relationName.contains(":")) {
			if (relationName.split(":")[1].equals("prt")) {
				prt_fireRule(gov, v2, dep, edgeRelation);
			}
		} else {
			if (dep.tag().equals("CD"))
				return;

			/* Multi word entity "Have you got light pink ghagra choli with golden thread work above 3000 rupees?" */
					XCEntity entity = retrieveEntity(depWord + " " + govWord);
					if (entity == null)
							entity = retrieveEntity(govWord);

			/* Issue with silver,gold, etc.: they are a color and a material but only color identified */
			if (entity != null) {
				String originalEntityStr = entity.getEntityStr();
				boolean isColor = ColorSearch.spotColor(dep, entity);
				String noColorEntityStr = entity.getEntityStr();
				logger.info("noColorEntityStr: " + noColorEntityStr);
				entity.setEntityStr(originalEntityStr);
				boolean isMaterial = MaterialSearch.spotMaterial(depWord, entity);
				String noMaterialEntityStr = entity.getEntityStr();
				logger.info("noMaterialEntityStr: " + noMaterialEntityStr);
				if (isColor && isMaterial) {
					logger.info("Exit COMPOUND on color and material");
					return;
				}
				if (isColor) {
					entity.setEntityStr(noColorEntityStr);
					logger.info("Exit COMPOUND on color");
					return;
				}
				if (isMaterial) {
					logger.info("Exit COMPOUND on material");
					return;
				}

				boolean isTheme = ThematicSearch.spotTheme(depWord, entity);
				if (isTheme) {
					logger.info("Exit COMPOUND on theme");
					return;
				} else {
					/* tube top*/
					if (this.sortedEdges.size() == 1) {
						entity.setAttribute(SearchUtils.GENERIC_KEY, depWord);
						query.getIntentObject().setIndexMap(entity.getEntityStr(), SearchUtils.GENERIC_KEY, dep);
					}
				}
				nn_fireRule(gov, v2, dep, edgeRelation);
			} else {
				/* show me your boot collection */
				entity = retrieveEntity(depWord);
				if (entity != null) {
					logger.info("COMPOUND: retrieved entity for dep: " + depWord);
					/*formal jacket mens*/
					IndexedWord amodDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.ADJECTIVAL_MODIFIER);
					if (amodDep != null) {
						entity.setAttribute(SearchUtils.GENERIC_KEY, amodDep.word());
						query.getIntentObject().setIndexMap(entity.getEntityStr(), SearchUtils.GENERIC_KEY, amodDep);
					}
				} else {
					/* rose color watches */
					Set<IndexedWord> parents = this.dependencyGraph.getParents(gov);
					for (IndexedWord currGov : parents) {
						entity = retrieveEntity(currGov.word());
						if (entity != null) {
							compound_fireRule(currGov, v2, dep, edgeRelation);
							break;
						}
					}
				}
			}
		}

	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#nn_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void nn_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

		if (!this.multiWordEntityMap.containsKey(gov)) {
			String govWord = gov.word().toLowerCase();
			StringBuilder netEntity = new StringBuilder();
			XCEntity entity = retrieveEntity(govWord);

			if (entity != null)
				entity.setGovWord(govWord);
			/* In case AMOD updates multiWordEntity, e.g. "full sleeve shirt" */
			String multiWordEntity = null;
			if (entity != null) {
				multiWordEntity = entity.getMultiWordEntity();
				logger.info("multiWordEntity prior to NNMOD update: " + multiWordEntity);
				if (multiWordEntity != null) {
					String[] arr = multiWordEntity.split(" ");
					for (int i = 0; i < Math.max(arr.length - 1, 0); i++)
						netEntity.append(arr[i]).append(" ");
				}
			}
			Set<IndexedWord> prefixList = this.dependencyGraph.getChildrenWithReln(gov, edgeRelation);
			if (prefixList != null) {
				for (IndexedWord prefix : prefixList) {
					String prefixWord = prefix.word();
					logger.info("Prefix Word: " + prefixWord);

					if (prefixWord != null && prefixWord.matches("\\w+") && !XCEntity.isRejectEntity(prefixWord, query)) {
						netEntity.append(prefixWord).append(" ");
					}
					/* Setting the whole prefix group instead of individual prefixes?
					 *  */
					if (entity != null) {
						entity.removeAttribute(SearchUtils.GENERIC_KEY);
						if (!netEntity.toString().isEmpty()) {
							entity.setAttribute(SearchUtils.GENERIC_KEY, netEntity.toString().trim());
						}

						/* "tube top", "palazzo pants" */
						if (entity.getAttribute(SearchUtils.GENERIC_KEY) == null || entity.getAttribute(SearchUtils.GENERIC_KEY).isEmpty()) {
							entity.setAttribute(SearchUtils.GENERIC_KEY, dep.word());
						}
					}
				}
			}
			String entityStr = netEntity.append(govWord).toString();
			logger.info("NN: Full Entity of " + govWord + " is " + entityStr);
			this.addToMultiWordEntityMap(gov, entityStr);
			if (entity != null)
				entity.setMultiWordEntity(entityStr);
		}
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#relcl_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void relcl_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		logger.info("RelCl: Redirecting to rcmod");
		rcmod_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#rcmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void rcmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#parataxis_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void parataxis_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#nmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void nmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		String relationName = edgeRelation.toString();
		boolean isPrepRelation = true;
		if (relationName.contains(":")) {
			String nextRelName = relationName.split(":")[1];
			switch (nextRelName) {
				case "tmod":
					this.tmod_fireRule(gov, v2, dep, edgeRelation);
					isPrepRelation = false;
					break;
				case "npmod":
					this.npmod_fireRule(gov, v2, dep, edgeRelation);
					isPrepRelation = false;
					break;
				case "poss":
					this.poss_fireRule(gov, v2, dep, edgeRelation);
					isPrepRelation = false;
					break;
				case "agent":
					this.agent_fireRule(gov, v2, dep, edgeRelation);
					isPrepRelation = false;
					break;
			}
		}
		if (isPrepRelation) {
			prep_fireRule(gov, v2, dep, edgeRelation);
		}
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#prep_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void prep_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		String govWord = gov.value();
		String depWord = dep.value();
		boolean isEntityFromDep = false;
		logger.info("PREP: " + gov + " \t" + dep);
		String currentEntityWord = null;
		String govPos = gov.tag();

		if (XpSentiment.NEED_TAG.equals(XpSentiment.getLexTag(Languages.EN, govWord, null, null)) || !PosTags.isNoun(Languages.EN, govPos)) {
			IndexedWord dobjDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
			if (dobjDep != null) {
				logger.info("Updating with dobj word");
				currentEntityWord = dobjDep.word().toLowerCase();
			} else {
				/*I am craving for yellow bikini*/
				if (!govWord.equals(SearchUtils.ALL_CATEGORIES) && XCEntity.isRejectEntity(govWord, this.query)) {
					if (this.query.getEntityMap().containsKey(govWord))
						this.entityCount -= 1;
					this.query.removeFromEntityMap(govWord);
					currentEntityWord = depWord;
					isEntityFromDep = true;
					logger.info("PREP: removing gov group: " + govWord + " from entity: " + currentEntityWord);
				} else {
					currentEntityWord = govWord;
				}
			}
		} else {
			/* I am craving for yellow bikini
			 * I want something in red */
			if (!govWord.equals(SearchUtils.ALL_CATEGORIES) && XCEntity.isRejectEntity(govWord, this.query)) {
				if (this.query.getEntityMap().containsKey(govWord))
					this.entityCount -= 1;
				this.query.removeFromEntityMap(govWord);
				currentEntityWord = depWord;
				isEntityFromDep = true;
				logger.info("PREP: removing gov group: " + govWord + " from entity: " + currentEntityWord);
			} else {
				currentEntityWord = govWord;
			}
		}
		logger.info("currentEntityWord after checking for NEED_TAG on govWord: " + currentEntityWord);
		XCEntity entity = retrieveEntity(currentEntityWord);
		if (entity == null) {
			/*watch below 500 in black color*/
			Set<IndexedWord> govGov = this.dependencyGraph.getParents(gov);
			Iterator<IndexedWord> govGovIter = govGov.iterator();
			IndexedWord govGovWord = null;
			while (entity == null && govGovIter.hasNext()) {
				govGovWord = govGovIter.next();
				entity = retrieveEntity(govGovWord.word());
			}
		}

		if (entity != null) {
			boolean isGender = GenderSearch.spotGender(dep, entity, false);
			if (isGender)
				return;
			/* Issue with Silver pendant: silver is a color and a material but only color identified */
			String originalEntityStr = entity.getEntityStr();
			boolean isColor = ColorSearch.spotColor(dep, entity);
			String noColorEntityStr = entity.getEntityStr();
			logger.info("noColorEntityStr: " + noColorEntityStr);
			entity.setEntityStr(originalEntityStr);
			boolean isMaterial = MaterialSearch.spotMaterial(depWord, entity);
			String noMaterialEntityStr = entity.getEntityStr();
			logger.info("noMaterialEntityStr: " + noMaterialEntityStr);
			if (isColor && isMaterial) {
				logger.info("Exit PREP on color and material");
				return;
			}
			if (isColor) {
				entity.setEntityStr(noColorEntityStr);
				logger.info("Exit PREP on color");
				return;
			}
			if (isMaterial) {
				logger.info("Exit PREP on material");
				return;
			}
			boolean isTheme = ThematicSearch.spotTheme(depWord, entity);
			if (isTheme) {
				logger.info("Exit PREP on theme");
				return;
			}
			IndexedWord modifier = this.dependencyGraph.getChildWithReln(dep, UniversalEnglishGrammaticalRelations.ADJECTIVAL_MODIFIER);
			if (modifier == null)
				modifier = this.dependencyGraph.getChildWithReln(dep, UniversalEnglishGrammaticalRelations.COMPOUND_MODIFIER);
			if (modifier != null) {
				String modifierWord = modifier.word();
				isGender = GenderSearch.spotGender(modifier, entity, false);
				if (isGender)
					return;
				isColor = ColorSearch.spotColor(modifier, entity);
				if (isColor)
					return;
				isMaterial = MaterialSearch.spotMaterial(modifierWord, entity);
				if (isMaterial)
					return;
				isTheme = ThematicSearch.spotTheme(modifierWord, entity);
				if (isTheme)
					return;
				depWord = modifier.word() + " " + depWord;
			}

			if (!isEntityFromDep && !dep.tag().equals("CD") && !dep.tag().equals("$") && !StringAttributeUtils.isCurrency(depWord)) {
				logger.info("depWord: " + depWord);
				entity.setAttribute(SearchUtils.GENERIC_KEY, depWord);
				query.getIntentObject().setIndexMap(entity.getEntityStr(), SearchUtils.GENERIC_KEY, dep);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#prepc_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void prepc_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#pobj_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void pobj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#mark_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void mark_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#vmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void vmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#infmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void infmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#partmod_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void partmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#xcomp_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 * eg. find me shirts. Queries with "find me..."
	 */
	@Override
	public void xcomp_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		String govWord = gov.word().toLowerCase();
		String depWord = dep.word().toLowerCase();

		XCEntity spEntity = retrieveEntity(depWord);
		if (spEntity != null)
			logger.info("XCOMP: gov: " + govWord + " dep: " + depWord + "\tentity: " + spEntity);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#prepSentiNullifier(java.lang.String)
	 */
	@Override
	public void prepSentiNullifier(String relation) {
	}

	public static String getMethodName(String relation) {
		if (relation.contains(":")) {
			relation = relation.split(":")[0];
		} else if (relation.contains("_")) {
			relation = relation.split("_")[0];
		}
		return relation + "_fireRule";
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#nsubj_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void nsubj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		/* For entities tagged as verb:
		 * red watches 
		 */
		XCEntity entity = retrieveEntity(gov.word());
		if (entity == null) {
			/*
			 * I want pants please
			 * nsubj(please-4, pants-3): only dependency where "pants" appears
			 */
			XCEntity depEntity = retrieveEntity(dep.word());
			if (depEntity != null)
				logger.info("NSUBJ: created entity from dep: " + dep.word());
			return;
		} else {
			/*pashmina wraps*/
			if (this.sortedEdges.size() == 1) {
				// TODO : add pashmina to list of material
				logger.info("Adding : " + dep.word() + " to features for " + entity.getEntityStr());
				entity.setAttribute(SearchUtils.FEATURE_KEY, dep.word());
				query.getIntentObject().setIndexMap(entity.getEntityStr(), SearchUtils.FEATURE_KEY, dep);
			}
		}
		if (dep.tag().matches("NN[S]*"))
			nn_fireRule(gov, v2, dep, edgeRelation);
		if (dep.tag().equals("JJ"))
			amod_fireRule(gov, v2, dep, edgeRelation);
	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#nsubjpass_fireRule(edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.ling.IndexedWord, edu.stanford.nlp.trees.GrammaticalRelation)
	 */
	@Override
	public void nsubjpass_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

	}

	/* (non-Javadoc)
	 * @see com.abzooba.synaptica.customer.debenhams.DependencyParse#processDependencyGraph()
	 */
	@Override
	public void processDependencyGraph() {

		/* ROOT edge not included in the list of edges */
		Collection<IndexedWord> roots = this.getDependencyGraph().getRoots();
		logger.info("No. of edges in dep parser : " + sortedEdges.size() + ". Roots size: " + roots.size());
		for (IndexedWord root : roots) {
			logger.info("root: " + root.word());
			String rootWord = root.word().toLowerCase();
			if (!XCEntity.isRejectEntity(rootWord, query)) {
				XCEntity entity = this.query.getEntity(rootWord);
				if (entity == null) {
					entity = new XCEntity(rootWord, this.query, false);
					this.entityCount += 1;
					logger.info("Created an entity; entityCount = " + this.entityCount);
					Iterator<Entry<String, XCEntity>> iter = this.query.getEntityMap().entrySet().iterator();
					while (iter.hasNext()) {
						Entry<String, XCEntity> entry = iter.next();
						if (entry.getValue().hasAllCategoriesAttribute())
							iter.remove();
					}
					this.query.addToEntityMap(rootWord, entity);
				}
			}
		}

		for (SemanticGraphEdge edge : sortedEdges) {

			IndexedWord gov = edge.getGovernor();
			IndexedWord dep = edge.getDependent();
			logger.info("Gov: " + gov.word() + ". Dep: " + dep.word() + ". Edge relation: " + edge.getRelation());
			if (this.checkedEdge.contains(edge)) {
				continue;
			}
			GrammaticalRelation rel = edge.getRelation();

			this.checkedEdge.add(edge);
			String relationStr = rel.toString();
			String methodName = getMethodName(relationStr);
			try {
				Method method = c.getMethod(methodName, IndexedWord.class, IndexedWord.class, IndexedWord.class, GrammaticalRelation.class);
				logger.info("Method Name: " + methodName);
				method.invoke(this, gov, null, dep, rel);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				logger.info("Method Name: " + methodName);
				e.printStackTrace();
			}
		}

		XCEntity entity = this.query.getEntity(SearchUtils.ALL_CATEGORIES);
		if (entity != null && entityCount > 1) {
			logger.info("Removing all-category entity");
			this.query.removeFromEntityMap(SearchUtils.ALL_CATEGORIES);
		}
		logger.info("Entity count: " + entityCount);
		if (entityCount == 0) {
			logger.info("All categories entity " + (this.query.getHasAllCategories() ? "with" : "without") + " all-category keyword");
			if (entity == null) {
				logger.info("Creating all-category entity");
				entity = new XCEntity(SearchUtils.ALL_CATEGORIES, query, false);
				this.query.addToEntityMap(SearchUtils.ALL_CATEGORIES, entity);
				/*
				 * The following is a term-based search for attributes
				 * brand
				 * gender
				 * material
				 * color
				 * Will probably be replaced by naive search
				 */
				SearchByProductName.searchByProductName(entity);
				GenderSearch.spotGender(this.query.getQueryString(), entity, true);
				MaterialSearch.spotMaterial(this.query.getQueryString(), entity);
				ColorSearch.spotColor(this.query.getQueryString(), "", entity); // lookup in the whole query without adjectified terms refinement
			}
		}
	}
}