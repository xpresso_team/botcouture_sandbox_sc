package com.abzooba.xcommerce.domain.xcelectronics;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;

import com.abzooba.xcommerce.core.XCEntity;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xcommerce.core.XCQuery;
import com.abzooba.xcommerce.nlp.DependencyParse;
import com.abzooba.xcommerce.nlp.domain.DomainKnowledgeBase;
import com.abzooba.xcommerce.search.SearchUtils;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.expressions.sentiment.XpSentiment;
import com.abzooba.xpresso.textanalytics.PosTags;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.UniversalEnglishGrammaticalRelations;

public class DependencyParseXCElectronics implements DependencyParse {

	protected static final Class<?> c = DependencyParseXCElectronics.class;
	protected static final Logger logger = XCLogger.getSpLogger();

	protected SemanticGraph dependencyGraph;
	protected List<SemanticGraphEdge> sortedEdges;
	protected Set<SemanticGraphEdge> checkedEdge;

	protected Map<IndexedWord, String> multiWordEntityMap;
	protected Map<String, String> nerMap;
	protected XCQuery query;

	private int entityCount;
	protected Languages language;
	/**
	 * @return the dependencyGraph
	 */
	public SemanticGraph getDependencyGraph() {
		return dependencyGraph;
	}

	/**
	 * @return the sortedEdges
	 */
	public List<SemanticGraphEdge> getSortedEdges() {
		return sortedEdges;
	}

	/**
	 */
	public void setSortedEdges() {
		this.sortedEdges = this.dependencyGraph.edgeListSorted();
	}

	/**
	 * @return the multiWordEntityMap
	 */
	public Map<IndexedWord, String> getMultiWordEntityMap() {
		return multiWordEntityMap;
	}

	/**
	 * @return the nerMap
	 */
	public Map<String, String> getNERMap() {
		return nerMap;
	}

	private XCEntity createEntity(String entityStr) {
		boolean isRejectEntity = XCEntity.isRejectEntity(entityStr, query);

		if (!isRejectEntity && hasCategory(entityStr)) {
			XCEntity entity = new XCEntity(entityStr, this.query, false);
			this.query.addToEntityMap(entityStr, entity);
			this.entityCount += 1;
			logger.info("Created an Entity for: " + entityStr);
			logger.info("entityCount = " + this.entityCount);
			return entity;
		} else {
			if (entityStr.contains(SearchUtils.ALL_CATEGORIES)) {
				this.query.setHasAllCategories(true);
				logger.info("All categories key word: " + entityStr);
				XCEntity entity = new XCEntity(SearchUtils.ALL_CATEGORIES, this.query, false);
				this.query.addToEntityMap(SearchUtils.ALL_CATEGORIES, entity);
				return entity;
			}
			logger.info("Entity rejected: " + entityStr);
			return null;
		}
	}

	private XCEntity retrieveEntity(String entityStr) {
		XCEntity entity = this.query.getEntity(entityStr);
		if (entity == null)
			entity = createEntity(entityStr);

		return entity;
	}

	public DependencyParseXCElectronics(XCQuery query, SemanticGraph dependencyGraph) {
		// TODO Auto-generated constructor stub
		language = Languages.EN;
		checkedEdge = new HashSet<SemanticGraphEdge>();
		if (dependencyGraph != null) {
			this.dependencyGraph = dependencyGraph;
			this.setSortedEdges();

			this.query = query;
			this.multiWordEntityMap = new HashMap<IndexedWord, String>();
			this.entityCount = 0;
			//			this.priceModified = false;
			logger.info("Dependency Graph:\n" + this.dependencyGraph.toString(SemanticGraph.OutputFormat.LIST));
		}
	}

	public void amod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

		logger.info("AMOD: " + gov + " is qualified by " + dep + " and v2= " + v2);
		String depWord = dep.word().toLowerCase();
		String govWord = gov.word().toLowerCase();

		XCEntity entity = null;
		if (govWord != null) {
			entity = retrieveEntity(govWord);
			logger.info("Obtaining entity : " + entity);
		}
		if (entity != null) {
			logger.info("Setting govWord " + govWord + " in entity " + entity);
			entity.setGovWord(govWord);
			/* flat television */
			boolean isScreenType = depWord.matches("flat|curved");
			if (isScreenType) {
				entity.setAttribute(SearchUtils.SCREEN_TYPE, depWord);
				logger.info("Exit AMOD on Screen Type");
				return;
			} else
				entity.setEntityStr(govWord);

			if (depWord.equalsIgnoreCase("led") || depWord.equalsIgnoreCase("lcd")) {
				logger.info("DOBJ: setting LED/LCD display type in entity " + entity.toString());
				entity.setAttribute(SearchUtils.DISPLAY_TYPE, depWord);
			} else if (depWord.equals("720p") || depWord.equals("1080p") || depWord.equals("4")) {
				entity = retrieveEntity(govWord);
				if (depWord.equals("4"))
					depWord = "4k";
				if (entity != null) {
					entity.setAttribute(SearchUtils.NAME, depWord);
				}
			}
		}
	}

	public void goeswith_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void ref_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void root_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void xsubj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void acomp_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void advcl_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void discourse_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void possessive_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void number_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void predet_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void punct_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

		logger.info("PUNCT: " + gov + " is qualified by " + dep + " and v2= " + v2);
		String depWord = dep.word().toLowerCase();
		String govWord = gov.word().toLowerCase();
		XCEntity entity = retrieveEntity(govWord);
		if (entity == null)
			entity = retrieveEntity(depWord);

		if (entity == null) {
			Set<IndexedWord> parents = this.dependencyGraph.getParents(gov);
			for (IndexedWord parent : parents) {
				entity = retrieveEntity(parent.word());
				if (entity != null) {
					break;
				}
			}
		}

		/* i want TV in 50" */
		if (entity == null) {
			logger.info("entity is null");
			if (this.query.getEntityMap() != null)
				entity = this.query.getEntityMap().entrySet().iterator().next().getValue();
			logger.info("Getting entity from entity map : " + entity.toString());
		}

		Set<IndexedWord> numModWords = this.getDependencyGraph().getChildrenWithReln(gov, GrammaticalRelation.valueOf("nummod"));
		if (depWord.equals("\"")) {
			if (numModWords != null && !numModWords.isEmpty()) {
				for (IndexedWord go : numModWords) {
					logger.info("go.word : " + go.word());
					if (go.word().matches("[0-9]+")) {
						entity.setAttribute(SearchUtils.SCREEN_SIZE, go.word());
						logger.info("Setting govWord " + go.word() + " in entity " + entity);
						break;
					}
				}
			} else if (govWord.matches("[0-9]+")) {
				entity.setAttribute(SearchUtils.SCREEN_SIZE, govWord);
			}
		}
	}

	public void prt_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void expl_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void mwe_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void tmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void npmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void npadvmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void nummod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		logger.info("NUMMODE: " + gov + " is qualified by " + dep + " and v2= " + v2);
		String depWord = dep.word().toLowerCase();
		String govWord = gov.word().toLowerCase();
		int depInt = 0;
		try {
			depInt = Integer.parseInt(depWord);
		} catch (NumberFormatException e) {
			logger.info("depWord " + depWord + " not a number");
		}

		if (govWord != null) {
			if (govWord.equals("size") || govWord.matches("inch[es]*")) {
				Set<IndexedWord> parents = this.dependencyGraph.getParents(gov);
				for (IndexedWord parent : parents) {
					XCEntity parentEntity = retrieveEntity(parent.word());
					if (parentEntity != null) {
						String entityStr = parentEntity.getEntityStr();
						logger.info("NUMMOD: setting size: " + govWord + ", depWord: " + depWord + " for entity " + entityStr);
						if (depWord.matches("[0-9]+"))
							parentEntity.setAttribute(SearchUtils.SCREEN_SIZE, depWord);
						break;
					}
				}
				Set<IndexedWord> children = this.dependencyGraph.getChildren(gov);
				for (IndexedWord child : children) {
					XCEntity childEntity = retrieveEntity(child.word());
					if (childEntity != null) {
						String entityStr = childEntity.getEntityStr();
						logger.info("NUMMOD: setting size: " + govWord + ", depWord: " + depWord + " for entity " + entityStr);
						if (depWord.matches("[0-9]+"))
							childEntity.setAttribute(SearchUtils.SCREEN_SIZE, depWord);
						break;
					}
				}
			} else if (govWord.equals("display")) {
				Set<IndexedWord> parents = this.dependencyGraph.getParents(gov);
				for (IndexedWord parent : parents) {
					XCEntity parentEntity = retrieveEntity(parent.word());
					if (parentEntity != null) {
						String entityStr = parentEntity.getEntityStr();
						logger.info("NUMMOD: setting display type:" + govWord + ", depWord: " + depWord + " for entity " + entityStr);
						parentEntity.setAttribute(SearchUtils.SCREEN_TYPE, depWord);
						break;
					}
				}
				Set<IndexedWord> children = this.dependencyGraph.getChildren(gov);
				for (IndexedWord child : children) {
					XCEntity childEntity = retrieveEntity(child.word());
					if (childEntity != null) {
						String entityStr = childEntity.getEntityStr();
						logger.info("NUMMOD: setting display type: " + govWord + ", depWord: " + depWord + " for entity " + entityStr);
						childEntity.setAttribute(SearchUtils.SCREEN_TYPE, depWord);
						break;
					}
				}
			} else if (depWord.equals("720p") || depWord.equals("1080p") || depWord.equals("4")) {
				XCEntity entity = retrieveEntity(govWord);
				if (depWord.equals("4"))
					depWord = "4k";
				if (entity != null) {
					entity.setAttribute(SearchUtils.NAME, depWord);
				}

			} else if (govWord.equalsIgnoreCase("k") && this.query.getGlobalPriceStr() != null) {
				/* television below 20 K 
				 * there is an issue with queries like 'tv below 4k' as '4k' is also a resolution,
				 * which is filtered above */
				String[] priceArr = this.query.getGlobalPriceStr().split(" ");
				for (int i = 1; i < priceArr.length; i++) {
					if (depWord.equalsIgnoreCase(priceArr[i]))
						priceArr[i] = priceArr[i] + "000";
				}
				StringBuilder sb = new StringBuilder();
				for (String s : priceArr) {
					sb.append(s);
					sb.append(" ");
				}
				logger.info("New price string: [" + sb.substring(0, sb.length() - 1) + "]");
				this.query.setGlobalPriceStr(sb.substring(0, sb.length() - 1));
			} else if (depInt > 0) {
				/* 2 USB point tv */
				XCEntity govEntity = retrieveEntity(govWord);
				if (govWord.equalsIgnoreCase("usb")) {
					Set<IndexedWord> parents = this.dependencyGraph.getParents(gov);
					for (IndexedWord parent : parents) {
						XCEntity parentEntity = retrieveEntity(parent.word());
						if (parentEntity != null) {
							String entityStr = parentEntity.getEntityStr();
							logger.info("NUMMOD: setting USB:" + govWord + ", depWord: " + depWord + " for entity " + entityStr);
							parentEntity.setAttribute(SearchUtils.USB_INPUT, depWord);
							return;
						}
					}
					Set<IndexedWord> children = this.dependencyGraph.getChildren(gov);
					for (IndexedWord child : children) {
						XCEntity childEntity = retrieveEntity(child.word());
						if (childEntity != null) {
							String entityStr = childEntity.getEntityStr();
							logger.info("NUMMOD: setting USB: " + govWord + ", depWord: " + depWord + " for entity " + entityStr);
							childEntity.setAttribute(SearchUtils.USB_INPUT, depWord);
							return;
						}
					}
				} else if (govWord.equalsIgnoreCase("hdmi")) {
					Set<IndexedWord> parents = this.dependencyGraph.getParents(gov);
					for (IndexedWord parent : parents) {
						XCEntity parentEntity = retrieveEntity(parent.word());
						if (parentEntity != null) {
							String entityStr = parentEntity.getEntityStr();
							logger.info("NUMMOD: setting HDMI:" + govWord + ", depWord: " + depWord + " for entity " + entityStr);
							parentEntity.setAttribute(SearchUtils.HDMI_INPUT, depWord);
							return;
						}
					}
					Set<IndexedWord> children = this.dependencyGraph.getChildren(gov);
					for (IndexedWord child : children) {
						XCEntity childEntity = retrieveEntity(child.word());
						if (childEntity != null) {
							String entityStr = childEntity.getEntityStr();
							logger.info("NUMMOD: setting HDMI: " + govWord + ", depWord: " + depWord + " for entity " + entityStr);
							childEntity.setAttribute(SearchUtils.HDMI_INPUT, depWord);
							return;
						}
					}
				} else if (govWord.equals("screen") || govWord.equals("display")) {
					/* samsung tv which has 55 inch screen 
					 * Necessary to check up to the grandparents of gov */
					//					TODO: implement a more configurable test case using e.g. WordNet synonyms
					Set<IndexedWord> parents = this.dependencyGraph.getParents(gov);
					for (IndexedWord parent : parents) {
						XCEntity parentEntity = retrieveEntity(parent.word());
						if (parentEntity != null) {
							String entityStr = parentEntity.getEntityStr();
							logger.info("NUMMOD: setting screen size: " + govWord + ", depWord: " + depWord + " for entity " + entityStr);
							parentEntity.setAttribute(SearchUtils.SCREEN_SIZE, depWord);
							return;
						} else {
							Set<IndexedWord> grandParents = this.dependencyGraph.getParents(parent);
							for (IndexedWord grandParent : grandParents) {
								XCEntity grandParentEntity = retrieveEntity(grandParent.word());
								if (grandParentEntity != null) {
									String entityStr = grandParentEntity.getEntityStr();
									logger.info("NUMMOD: setting screen size: " + govWord + ", depWord: " + depWord + " for entity " + entityStr);
									grandParentEntity.setAttribute(SearchUtils.SCREEN_SIZE, depWord);
								}
							}
						}
					}
				} else if (govEntity != null) {
					Set<IndexedWord> parents = this.dependencyGraph.getParents(gov);
					for (IndexedWord parent : parents) {
						if (parent.word().equalsIgnoreCase("usb")) {
							logger.info("NUMMOD: setting USB:" + govWord + ", depWord: " + depWord);
							govEntity.setAttribute(SearchUtils.USB_INPUT, depWord);
							return;
						}
						if (parent.word().equalsIgnoreCase("hdmi")) {
							logger.info("NUMMOD: setting display type:" + govWord + ", depWord: " + depWord);
							govEntity.setAttribute(SearchUtils.HDMI_INPUT, depWord);
							return;
						}
					}
					Set<IndexedWord> children = this.dependencyGraph.getChildren(gov);
					for (IndexedWord child : children) {
						if (child.word().equalsIgnoreCase("usb")) {
							logger.info("NUMMOD: setting USB:" + govWord + ", depWord: " + depWord);
							govEntity.setAttribute(SearchUtils.USB_INPUT, depWord);
							return;
						}
						if (child.word().equalsIgnoreCase("hdmi")) {
							logger.info("NUMMOD: setting HDMI:" + govWord + ", depWord: " + depWord);
							govEntity.setAttribute(SearchUtils.HDMI_INPUT, depWord);
							return;
						}
					}
				}
			}
		}
	}

	public void num_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void poss_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void case_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		logger.info("CASE: " + gov + " is qualified by " + dep + " and v2= " + v2);
		String depWord = dep.word().toLowerCase();
		String govWord = gov.word().toLowerCase();
		XCEntity entity = retrieveEntity(govWord);
		if (govWord.equalsIgnoreCase("led") || govWord.equalsIgnoreCase("lcd")) {
			if (entity != null) {
				logger.info("CASE: setting LED/LCD display type in entity " + entity.toString());
				entity.setAttribute(SearchUtils.DISPLAY_TYPE, govWord);
			} else {
				Set<IndexedWord> parents = this.dependencyGraph.getParents(gov);
				for (IndexedWord parent : parents) {
					XCEntity parentEntity = retrieveEntity(parent.word());
					if (parentEntity != null) {
						String entityStr = parentEntity.getEntityStr();
						logger.info("CASE: setting size:" + govWord + ", depWord: " + depWord + " for entity " + entityStr);
						parentEntity.setAttribute(SearchUtils.DISPLAY_TYPE, govWord);
						break;
					}
				}
			}
		}
	}

	public void acl_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		logger.info("ACL: " + gov + " is qualified by " + dep + " and v2= " + v2);
		String depWord = dep.word().toLowerCase();
		String govWord = gov.word().toLowerCase();

		XCEntity entity = retrieveEntity(govWord);
		if (depWord.equalsIgnoreCase("led") || depWord.equalsIgnoreCase("lcd")) {
			if (entity != null) {
				logger.info("CASE: setting LED/LCD display type in entity " + entity.toString());
				entity.setAttribute(SearchUtils.DISPLAY_TYPE, depWord);
			} else {
				Set<IndexedWord> parents = this.dependencyGraph.getParents(gov);
				for (IndexedWord parent : parents) {
					XCEntity parentEntity = retrieveEntity(parent.word());
					if (parentEntity != null) {
						String entityStr = parentEntity.getEntityStr();
						logger.info("CASE: setting size:" + govWord + ", depWord: " + depWord + " for entity " + entityStr);
						parentEntity.setAttribute(SearchUtils.DISPLAY_TYPE, depWord);
						break;
					}
				}
			}
		}
	}

	public void advmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void agent_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void quantmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void appos_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void abbrev_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void aux_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void pcomp_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void csubjpass_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void preconj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void auxpass_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void cc_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void ccomp_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void csubj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void conj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void cop_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void dep_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

		logger.info("DEP: " + gov + " is qualified by " + dep + " and v2= " + v2);
		String depWord = dep.word().toLowerCase();
		String govWord = gov.word().toLowerCase();

		if (govWord.toLowerCase().matches("inch[es]*") || govWord.equalsIgnoreCase("size")) {
			XCEntity entity = retrieveEntity(depWord);
			Set<IndexedWord> numModWords = this.getDependencyGraph().getChildrenWithReln(gov, GrammaticalRelation.valueOf("nummod"));
			for (IndexedWord go : numModWords) {
				if (go.word().matches("[0-9]+")) {
					entity.setAttribute(SearchUtils.SCREEN_SIZE, go.word());
					logger.info("Setting govWord " + go.word() + " in entity " + entity);
					break;
				}
			}
		}
	}

	public void det_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void dobj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {

		logger.info("DOBJ: " + gov + " is qualified by " + dep + " and v2= " + v2);
		String depWord = dep.word().toLowerCase();
		String govWord = gov.word().toLowerCase();

		XCEntity spEntity = retrieveEntity(depWord);
		if (spEntity != null) {
			if (depWord.equals("tv")) {
				logger.info("DOBJ: setting entityStr to " + depWord + " in entity: " + spEntity.toString());
				spEntity.setEntityStr(depWord);
			}

			boolean isScreenType = govWord.matches("flat|curved");
			if (isScreenType) {
				logger.info("DOBJ: setting screen type: " + govWord + " in entity: " + spEntity.toString());
				spEntity.setAttribute(SearchUtils.SCREEN_TYPE, govWord);
			}

			if (govWord.equalsIgnoreCase("led") || govWord.equalsIgnoreCase("lcd")) {
				logger.info("DOBJ: setting LED/LCD display type in entity " + spEntity.toString());
				spEntity.setAttribute(SearchUtils.DISPLAY_TYPE, govWord);
			}
		}
	}

	public void iobj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void neg_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void name_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void compound_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		logger.info("COMPOUND: " + gov + " is qualified by " + dep + " and v2= " + v2);
		String depWord = dep.word().toLowerCase();
		String govWord = gov.word().toLowerCase();
		XCEntity entity = retrieveEntity(govWord);
		if (entity == null)
			entity = retrieveEntity(depWord);

		if (entity == null) {
			Set<IndexedWord> parents = this.dependencyGraph.getParents(gov);
			for (IndexedWord parent : parents) {
				entity = retrieveEntity(parent.word());
				if (entity != null) {
					break;
				}
			}
		}

		if (entity != null) {
			if (govWord != null) {
				if (govWord.equals("size") || govWord.matches("inch[es]*")) {
					if (depWord.matches("[0-9]+")) {
						entity.setAttribute(SearchUtils.SCREEN_SIZE, depWord);
						String entityStr = entity.getEntityStr();
						entity.setEntityStr(entityStr + " " + depWord);
					}
				}

				if (govWord.equals("resolution") || govWord.equals("hd")) {
					String entityStr = entity.getEntityStr();
					entity.setEntityStr(entityStr + " " + depWord);
					entity.setAttribute(SearchUtils.NAME, depWord);
				}

				Set<IndexedWord> compoundDepWords = this.getDependencyGraph().getChildrenWithReln(gov, edgeRelation);
				if (indexedWordSetContainsString(compoundDepWords, "resolution") || indexedWordSetContainsString(compoundDepWords, "hd") || indexedWordSetContainsString(compoundDepWords, "1080p") || indexedWordSetContainsString(compoundDepWords, "720p") || indexedWordSetContainsString(compoundDepWords, "4k")) {
					for (IndexedWord go : compoundDepWords) {
						logger.info("go.word : " + go.word());
						if (go.word().equals("1080p") || go.word().equals("720p") || go.word().equals("4k") || go.word().equals("full") || go.word().equals("ultra")) {
							if (go.word().equals("ultra") || go.word().equals("full"))
								go.setWord("4k");
							entity.setAttribute(SearchUtils.NAME, go.word().toLowerCase());
							logger.info("Setting govWord " + go.word() + " in entity " + entity);
							break;
						}
					}
				}

				if (indexedWordSetContainsString(compoundDepWords, "inch[es]*") || indexedWordSetContainsString(compoundDepWords, "size")) {
					for (IndexedWord go : compoundDepWords)
						if (!go.word().equals("size") && !go.word().matches("inch[es]*")) {
							if (go.word().matches("[0-9]+")) {
								entity.setAttribute(SearchUtils.SCREEN_SIZE, go.word().toLowerCase());
								logger.info("Setting govWord " + go.word() + " in entity " + entity);
								break;
							}
						}
				}

				if (depWord.equals("lcd") || depWord.equals("led")) {
					entity.setAttribute(SearchUtils.DISPLAY_TYPE, depWord);
				}

				if (depWord.equals("curved") || depWord.equals("flat")) {
					entity.setAttribute(SearchUtils.SCREEN_TYPE, depWord);
				}

				Set<IndexedWord> numModWords = this.getDependencyGraph().getChildrenWithReln(gov, GrammaticalRelation.valueOf("nummod"));
				if (depWord.equalsIgnoreCase("resolution") || depWord.equalsIgnoreCase("hd")) {
					for (IndexedWord go : numModWords) {
						logger.info("go.word : " + go.word());
						if (go.word().equals("1080p") || go.word().equals("720p") || go.word().equals("4k")) {
							entity.setAttribute(SearchUtils.NAME, go.word().toLowerCase());
							logger.info("Setting govWord " + go.word() + " in entity " + entity);
							break;
						}
					}
				} else if (depWord.equalsIgnoreCase("size") || depWord.toLowerCase().matches("inch[es]*")) {
					for (IndexedWord go : numModWords) {
						if (go.word().matches("[0-9]+")) {
							entity.setAttribute(SearchUtils.SCREEN_SIZE, go.word().toLowerCase());
							logger.info("Setting govWord " + go.word() + " in entity " + entity);
							break;
						}
					}
				}

				Set<IndexedWord> aModWords = this.getDependencyGraph().getChildrenWithReln(gov, GrammaticalRelation.valueOf("amod"));
				if (depWord.equalsIgnoreCase("resolution") || depWord.equalsIgnoreCase("hd")) {
					for (IndexedWord go : aModWords) {
						logger.info("go.word : " + go.word());
						if (go.word().equals("1080p") || go.word().equals("720p") || go.word().equals("4k") || go.word().equals("full") || go.word().equals("ultra")) {
							if (go.word().equals("full") || go.word().equals("ultra"))
								go.setWord("4k");
							entity.setAttribute(SearchUtils.NAME, go.word().toLowerCase());
							logger.info("Setting govWord " + go.word() + " in entity " + entity);
							break;
						}
					}
				}
			}
		}
	}

	private boolean indexedWordSetContainsString(Set<IndexedWord> compoundDepWords, String regex) {
		for (IndexedWord word : compoundDepWords) {
			if (word.word().matches(regex)) {
				return true;
			}
		}
		return false;
	}

	public void nn_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void relcl_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void rcmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void parataxis_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void nmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		String relationName = edgeRelation.toString();
		boolean isPrepRelation = true;
		if (relationName.contains(":")) {
			String nextRelName = relationName.split(":")[1];
			switch (nextRelName) {
				case "tmod":
					this.tmod_fireRule(gov, v2, dep, edgeRelation);
					isPrepRelation = false;
					break;
				case "npmod":
					this.npmod_fireRule(gov, v2, dep, edgeRelation);
					isPrepRelation = false;
					break;
				case "poss":
					this.poss_fireRule(gov, v2, dep, edgeRelation);
					isPrepRelation = false;
					break;
				case "agent":
					this.agent_fireRule(gov, v2, dep, edgeRelation);
					isPrepRelation = false;
					break;
			}
		}
		if (isPrepRelation) {
			prep_fireRule(gov, v2, dep, edgeRelation);
		}
	}

	public void prep_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		String govWord = gov.value();
		logger.info("PREP: " + gov + " \t" + dep);
		String currentEntityWord = null;
		String govPos = gov.tag();

		if (XpSentiment.NEED_TAG.equals(XpSentiment.getLexTag(Languages.EN, govWord, null, null)) || !PosTags.isNoun(Languages.EN, govPos)) {
			IndexedWord dobjDep = this.dependencyGraph.getChildWithReln(gov, UniversalEnglishGrammaticalRelations.DIRECT_OBJECT);
			if (dobjDep != null) {
				logger.info("Updating with dobj word");
				currentEntityWord = dobjDep.word().toLowerCase();
			} else {
				currentEntityWord = govWord;
			}
		} else {
			currentEntityWord = govWord;
		}
		logger.info("currentEntityWord after checking for NEED_TAG on govWord: " + currentEntityWord);

		XCEntity entity = retrieveEntity(currentEntityWord);
		if (entity == null) {
			Set<IndexedWord> govGov = this.dependencyGraph.getParents(gov);
			Iterator<IndexedWord> govGovIter = govGov.iterator();
			IndexedWord govGovWord = null;
			while (entity == null && govGovIter.hasNext()) {
				govGovWord = govGovIter.next();
				entity = retrieveEntity(govGovWord.word());
			}
		}
	}

	public void prepc_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void pobj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void mark_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void vmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void infmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void partmod_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void xcomp_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void nsubj_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
		logger.info("NSUBJ: " + gov + " is qualified by " + dep + " and v2= " + v2);
		String depWord = dep.word().toLowerCase();
		String govWord = gov.word().toLowerCase();
		XCEntity entity = retrieveEntity(govWord);
		logger.info("NSUBJ: govWord: " + govWord + " depWord: " + depWord + "\tentity: " + entity);
	}

	public void nsubjpass_fireRule(IndexedWord gov, IndexedWord v2, IndexedWord dep, GrammaticalRelation edgeRelation) {
	}

	public void processDependencyGraph() {
		Collection<IndexedWord> roots = this.getDependencyGraph().getRoots();
		for (IndexedWord root : roots) {
			String rootWord = root.word().toLowerCase();
			if (!XCEntity.isRejectEntity(rootWord, query) && hasCategory(rootWord)) {
				XCEntity entity = this.query.getEntity(rootWord);
				if (entity == null) {
					entity = new XCEntity(rootWord, this.query, false);
					this.query.addToEntityMap(rootWord, entity);
					entityCount += 1;
				}
			}
		}

		for (SemanticGraphEdge edge : sortedEdges) {

			IndexedWord gov = edge.getGovernor();
			IndexedWord dep = edge.getDependent();

			if (this.checkedEdge.contains(edge)) {
				continue;
			}
			GrammaticalRelation rel = edge.getRelation();

			this.checkedEdge.add(edge);
			String relationStr = rel.toString();
			String methodName = com.abzooba.xcommerce.domain.xcfashion.DependencyParseXCFashion.getMethodName(relationStr);
			try {
				Method method = c.getMethod(methodName, IndexedWord.class, IndexedWord.class, IndexedWord.class, GrammaticalRelation.class);
				method.invoke(this, gov, null, dep, rel);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				logger.info("Method Name: " + methodName);
				e.printStackTrace();
			}
		}

		XCEntity entity = this.query.getEntity(SearchUtils.ALL_CATEGORIES);
		if (entity != null && entityCount > 1) {
			logger.info("Removing all-category entity");
			this.query.removeFromEntityMap(SearchUtils.ALL_CATEGORIES);
		}

		if (entityCount == 0) {
			logger.info("All categories entity " + (this.query.getHasAllCategories() ? "with" : "without") + " all-category keyword");
			if (entity == null) {
				logger.info("Creating all-category entity");
				entity = new XCEntity(SearchUtils.ALL_CATEGORIES, query, false);
				this.query.addToEntityMap(SearchUtils.ALL_CATEGORIES, entity);
			}
		}
	}

	private boolean hasCategory(String entityStr) {
		List<String> categories = DomainKnowledgeBase.getWordVectorCategory(entityStr, "", false);
		Set<String> domainCategory = DomainKnowledgeBase.getDomainCategory(this.query.getDomainName(), categories);
		logger.info("domainCategory: " + ((domainCategory == null) ? "null" : domainCategory.toString()));

		return domainCategory != null && !domainCategory.isEmpty();
	}

	@Override
	public String getEdgesAsStr() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clausalSentiClassifier(IndexedWord gov, IndexedWord dep) {
		// TODO Auto-generated method stub

	}

	@Override
	public void prepSentiNullifier(String relation) {
		// TODO Auto-generated method stub

	}
}
