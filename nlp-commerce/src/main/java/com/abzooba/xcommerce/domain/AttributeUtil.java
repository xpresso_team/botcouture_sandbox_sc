package com.abzooba.xcommerce.domain;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;

import com.abzooba.xcommerce.core.XCEntity;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xcommerce.nlp.ColorSimilarity;
import com.abzooba.xcommerce.nlp.attribute.ColorSearch;
import com.abzooba.xcommerce.nlp.attribute.GenderSearch;
import com.abzooba.xcommerce.search.SearchUtils;
import com.abzooba.xpresso.engine.core.XpLexMatch;

/**
 * @author Alix Melchy Sep 1, 2016
 * 
 * This is a utils class. All the public methods are controlling the actual customer-specific
 * private methods called as required per domain/customer specificities.
 *
 */
public class AttributeUtil {
	private static Logger logger;
	private static Set<String> quantifiers = Stream.of("\\bpair(s)*\\b", "\\bcouple(s)*\\b", "\\bload(s)*\\b", "\\bton(s)*\\b", "\\bdeal\\b", "\\blot(s)*\\b").collect(Collectors.toCollection(HashSet::new));

	/* Non-instantiable class */
	private AttributeUtil() {
		throw new AssertionError();
	}

	public static void init() {
		logger = XCLogger.getSpLogger();
	}

	/**
	 * Orders CloudSearch results for entity according to specific attribute(s) 
	 * according to domain specificities
	 * 
	 * @param domain
	 * @param entity
	 * 
	 */
	public static void orderByAttribute(String domain, XCEntity entity) {
		switch (domain) {
			case "xcfashion":
				orderByAttributeXCFashion(entity);
				break;
		}
	}

	/**
	 * Disambiguates numerical string price in entity string
	 * using specificities of domain.
	 * It operates at processEntity level
	 * 
	 * @param domain
	 * @param entity
	 * @param price
	 * @see com.abzooba.xcommerce.core.XCEntity#processEntity(com.abzooba.xcommerce.core.XCQuery)
	 * 
	 */
	public static void disambiguateNumericAttributesAndPrice(String domain, XCEntity entity, String price) {
		switch (domain) {
			case "xcelectronics":
				disambiguateNumericAttributesAndPriceXCElectronics(entity, price);
				break;
			case "xcfashion":
				dismbiguateNumericAttributesAndPriceXCFashion(entity, price);
				break;
		}
	}

	/**
	 * Checks whether entity is a quantifier.
	 * @param entity
	 * @return
	 */
	public static boolean isQuantifier(String entity) {
		entity = entity.toLowerCase();
		return XpLexMatch.sequenceContain(quantifiers, entity) != null;
	}

	/**
	 * Processes entity string to extract global attributes
	 * according to domain specificities
	 * 
	 * @param domain
	 * @param entity
	 * @see com.abzooba.xcommerce.core.XCEntity#processEntity(com.abzooba.xcommerce.core.XCQuery)
	 * 
	 */
	public static void extractGlobalAttribute(String domain, XCEntity entity) {
		switch (domain) {
			case "xcfashion":
				extractGlobalAttributeXCFashion(entity);
				break;
			case "xcelectronics":
				extractGlobalAttributeXCElectronics(entity);
				break;
		}
	}

	/**
	 * Checks whether the entityStr is an attribute to be rejected as an entity;
	 * It depends on the domain.
	 * 
	 * @param domain
	 * @param entityStr
	 * @return
	 * @see com.abzooba.xcommerce.core.XCEntity#isRejectEntity(String, com.abzooba.xcommerce.core.XCQuery)
	 * 
	 */
	public static boolean isRejectByAttribute(String domain, String entityStr) {
		switch (domain) {
			case "xcfashion":
				return isRejectByAttributeXCFashion(entityStr);
			default:
				return false;
		}
	}

	private static void orderByAttributeXCFashion(XCEntity entity) {
		Set<String> color = entity.getAttribute(SearchUtils.COLOR_KEY);
		logger.info("Color:\t" + entity.getAttribute(SearchUtils.COLOR_KEY));
		if (color != null && entity.getStructuredSearchResults() != null) {
			ColorSimilarity.assignColorSimilarity(entity.getStructuredSearchResults(), color);
		}
	}

	private static void disambiguateNumericAttributesAndPriceXCElectronics(XCEntity entity, String price) {
		/*To avoid detection of screen size as price*/
		if (entity.getAttribute(SearchUtils.SCREEN_SIZE) != null) {
			int screenSize = Integer.parseInt(entity.getAttribute(SearchUtils.SCREEN_SIZE).iterator().next());
			if (!price.contains(screenSize + "") && !price.contains((screenSize + 100) + ""))
				entity.setAttribute(SearchUtils.PRICE_NUMERIC_KEY, price);
		} else if (entity.getAttribute(SearchUtils.NAME) != null && entity.getAttribute(SearchUtils.NAME).iterator().next().equalsIgnoreCase("4k")) {
			if (!price.contains("104"))
				entity.setAttribute(SearchUtils.PRICE_NUMERIC_KEY, price);
		} else
			entity.setAttribute(SearchUtils.PRICE_NUMERIC_KEY, price);
	}

	private static void dismbiguateNumericAttributesAndPriceXCFashion(XCEntity entity, String price) {
		entity.setAttribute(SearchUtils.PRICE_NUMERIC_KEY, price);
	}

	private static void extractGlobalAttributeXCFashion(XCEntity entity) {
		GenderSearch.spotGender(entity.getEntityStr(), entity, true);
	}

	private static void extractGlobalAttributeXCElectronics(XCEntity entity) {
		String mainQuery = entity.getQuery().getQueryString().toLowerCase();
		if (mainQuery.contains("smart")) {
			entity.setAttribute(SearchUtils.SMART_TV, "yes");
		}
		if (mainQuery.contains("wifi") || mainQuery.contains("wi-fi")) {
			entity.setAttribute(SearchUtils.WIFI_ENABLED, "yes");
		}
	}

	private static boolean isRejectByAttributeXCFashion(String entityStr) {
		/* As is an entity is created for NP like 'black color', which shouldn't be the case */
		if (entityStr.matches(".*\\bcolor\\b"))
			return true;

		if (!entityStr.contains(" ")) {
			if (AttributeUtil.isQuantifier(entityStr))
				return true;
			String[] gender = GenderSearch.checkForGender(entityStr);
			if (gender != null)
				return true;
			String color = ColorSearch.checkForColor(entityStr);
			if (color != null)
				return true;
		}
		return false;
	}
}
