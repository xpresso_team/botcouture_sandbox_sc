package com.abzooba.xcommerce.domain.xcfashion;


import java.util.Arrays;
import java.util.List;

/**
 * Created by mayank on 2/5/17.
 */
public class XCHierarchy {

    private String H1_level;
    private String H2_level;
    private String H3_level;
    private List<String> H4_level;
    private String H1_level_lemma;
    private String H2_level_lemma;
    private String H3_level_lemma;
    private List<String> H4_level_lemmas;

    public XCHierarchy(String h1_level, String h2_level, String h3_level, List<String> h4_level) {
        H1_level = h1_level;
        H2_level = h2_level;
        H3_level = h3_level;
        H4_level = h4_level;
    }

    public XCHierarchy(String h1_level, String h2_level, String h3_level, List<String> h4_level,
                       String h1_level_lemma, String h2_level_lemma, String h3_level_lemma, List<String> h4_level_lemmas) {
        H1_level = h1_level;
        H2_level = h2_level;
        H3_level = h3_level;
        H4_level = h4_level;
        H1_level_lemma = h1_level_lemma;
        H2_level_lemma = h2_level_lemma;
        H3_level_lemma = h3_level_lemma;
        H4_level_lemmas = h4_level_lemmas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof XCHierarchy)) return false;

        XCHierarchy that = (XCHierarchy) o;

        if(this.getH1_level() != null && that.getH1_level() != null) {
            if(this.getH3_level() != null && that.getH3_level() != null)
                return (H1_level.equals(that.H1_level) || H1_level_lemma.equals(that.H1_level)) &&
                        (H2_level.equals(that.H2_level) || H2_level_lemma.equals(that.H2_level)) &&
                        (H3_level.equals(that.H3_level) || H3_level_lemma.equals(that.H3_level));
            else
                return (H1_level.equals(that.H1_level) || H1_level_lemma.equals(that.H1_level)) &&
                        (H2_level.equals(that.H2_level) || H2_level_lemma.equals(that.H2_level));
        }
        else if(this.getH3_level() != null && that.getH3_level() != null)
            return (H3_level.equals(that.H3_level) || H3_level_lemma.equals(that.H3_level)) &&
                    (H2_level.equals(that.H2_level) || H2_level_lemma.equals(that.H2_level));

        return (H2_level.equals(that.H2_level) || H2_level_lemma.equals(that.H2_level));
    }

    @Override
    public int hashCode() {
        return H2_level.hashCode();
    }

    @Override
    public String toString() {
        return "XCHierarchy{" +
                "H1_level='" + H1_level + '\'' +
                ", H2_level='" + H2_level + '\'' +
                ", H3_level='" + H3_level + '\'' +
                ", H4_level=" + H4_level +
                '}';
    }

    public String getH1_level() {
        return H1_level;
    }

    public String getH2_level() {
        return H2_level;
    }

    public String getH3_level() {
        return H3_level;
    }

    public List<String> getH4_level() {
        return H4_level;
    }

    public String getH1_level_lemma() {
        return H1_level_lemma;
    }

    public void setH1_level_lemma(String h1_level_lemma) {
        H1_level_lemma = h1_level_lemma;
    }

    public String getH2_level_lemma() {
        return H2_level_lemma;
    }

    public void setH2_level_lemma(String h2_level_lemma) {
        H2_level_lemma = h2_level_lemma;
    }

    public String getH3_level_lemma() {
        return H3_level_lemma;
    }

    public void setH3_level_lemma(String h3_level_lemma) {
        H3_level_lemma = h3_level_lemma;
    }

    public List<String> getH4_level_lemmas() {
        return H4_level_lemmas;
    }

    public void setH4_level_lemmas(List<String> h4_level_lemmas) {
        H4_level_lemmas = h4_level_lemmas;
    }
}
