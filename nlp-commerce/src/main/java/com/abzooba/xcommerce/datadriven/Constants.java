package com.abzooba.xcommerce.datadriven;

/**
 * Created by mayank on 20/7/17.
 */
public class Constants {
    public static final String PRICE = "price";
    public static final String PRICE_QUALIFIER = "price_qualifier";
    public static final String BRAND = "brand";
    public static final String GENDER = "gender";
    public static final String COLOR = "color";
    public static final String CURRENCY = "currency";
    public static final String PRODUCT = "product";
    public static final String PRODUCT_QUALIFIER = "product_qualifier";
    public static final String ENTITIES = "entities";
    public static final String ENTITY = "entity";
    public static final String START = "start";
    public static final String END = "end";
    public static final String VALUE = "value";
}
