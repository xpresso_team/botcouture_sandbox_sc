/**
 * 
 */
package com.abzooba.xcommerce.nlp.domain;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCEngine;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xcommerce.nlp.SpellChecker;
import com.abzooba.xcommerce.search.EsHttpSearch;
import com.abzooba.xcommerce.utils.MapUtil;
import com.abzooba.xcommerce.utils.SpreadSheetReader;
import com.abzooba.xpresso.engine.config.XpConfig.Languages;
import com.abzooba.xpresso.engine.core.XpEngine;
import com.abzooba.xpresso.textanalytics.CoreNLPController;
import com.abzooba.xpresso.textanalytics.wordvector.WordVector;
import com.abzooba.xpresso.textanalytics.wordvector.WordVectorDB;
import com.abzooba.xpresso.utils.ThreadUtils;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Koustuv Saha
 * May 4, 2016 4:32:39 PM
 * Synaptica  DomainKnowledgeBase
 */
public class DomainKnowledgeBase {
	protected static Logger logger;
	private static final double SIMILARITY_THRESHOLD = 0.6;
	private static final double RECOMMENDED_SIMILARITY_THRESHOLD = 0.4;
	private static Languages LANGUAGE = Languages.EN;
	private static final Map<String, Set<String>> entityClustersMap = new ConcurrentHashMap<String, Set<String>>();
	private static Map<String, Object> categoryCentroidVectorMap = new ConcurrentHashMap<String, Object>();
	private static final String SYN_CATEGORY = "syn-category";
	private static final String SYN_ATTRIBUTE = "syn-attribute";
	private static final String SYN_CONFIDENCE = "syn-confidence";
	private static final String SYN_CENTROID_VECTOR = "syn-centroid-vector";

	class MongoUpdateThread implements Runnable {
		List<Object> line;
		int lineNo;

		public MongoUpdateThread(List<Object> line, int lineNo) {
			this.line = line;
			this.lineNo = lineNo;
		}

		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@SuppressWarnings("unchecked")
		@Override
		public void run() {
			String[] strArr = new String[line.size()];
			int index = 0;
			for (Object value : line) {
				strArr[index] = (String) value;
				index++;
			}
			if (strArr.length > 3) {
				String attribute = strArr[0];
				String categoryName = strArr[1];
				if (categoryName.equals("#N/A") || categoryName.equals("NA") || categoryName.length() == 0) {
					return;
				}
				if (categoryName.length() > 0) {
					List<Double> categoryCentroidVector = new ArrayList<Double>();

					WordVectorDB wvd = XpEngine.getWordVectorDbApp();

					String categoryNameLower = categoryName.toLowerCase();
					wvd.updateData(LANGUAGE, categoryNameLower, SYN_CATEGORY, categoryName);
					wvd.updateData(LANGUAGE, categoryNameLower, SYN_CONFIDENCE, 1.0);
					wvd.updateData(LANGUAGE, categoryNameLower, SYN_ATTRIBUTE, attribute);

					String seedWord = strArr[2];
					Double seedConfidence = 1.0;

					String existingMongoCategory = (String) wvd.getColumnObjectForKey(LANGUAGE, seedWord, SYN_CATEGORY);
					if (existingMongoCategory == null || !existingMongoCategory.equals(categoryName)) {
						wvd.updateData(LANGUAGE, seedWord, SYN_CATEGORY, categoryName);
						wvd.updateData(LANGUAGE, seedWord, SYN_CONFIDENCE, seedConfidence);
						wvd.updateData(LANGUAGE, seedWord, SYN_ATTRIBUTE, attribute);
					}
					List<Double> seedWordVector = XpEngine.getWordVectorDbApp().getWordVectorForKey(LANGUAGE, seedWord);
					WordVector.addVectors(categoryCentroidVector, seedWordVector);
					logger.info("Language: " + LANGUAGE + "\tProcessing: " + lineNo);

					for (int i = 2; i < strArr.length; i++) {
						String[] wordArr = strArr[i].split(":");
						if (wordArr.length == 2) {
							String word = wordArr[0];
							List<Double> currWordVector = XpEngine.getWordVectorDbApp().getWordVectorForKey(LANGUAGE, word);
							WordVector.addVectors(categoryCentroidVector, currWordVector);

							Double currConfidence = Double.parseDouble(wordArr[1]);
							existingMongoCategory = (String) wvd.getColumnObjectForKey(LANGUAGE, word, SYN_CATEGORY);
							if (existingMongoCategory != null && existingMongoCategory.equals(categoryName)) {
								continue;
							}
							Object existingConf = wvd.getColumnObjectForKey(LANGUAGE, word, SYN_CONFIDENCE);
							if (existingConf != null) {
								Double existingConfidence = new Double(existingConf.toString());
								if (existingConfidence != null) {
									if (currConfidence < existingConfidence) {
										continue;
									}
								}
							}
							wvd.updateData(LANGUAGE, word, SYN_CONFIDENCE, currConfidence);
							wvd.updateData(LANGUAGE, word, SYN_CATEGORY, categoryName);
							wvd.updateData(LANGUAGE, word, SYN_ATTRIBUTE, attribute);
							logger.info("Updated for word: " + word + "\t" + lineNo + "\t" + categoryName + "\t" + currConfidence);

						}
					}
					WordVector.averageVector(categoryCentroidVector, strArr.length - 1);
					List<List<?>> categoryCentroidVectorList = null;
					categoryCentroidVectorList = (List<List<?>>) categoryCentroidVectorMap.get(categoryName);
					if (categoryCentroidVectorList == null) {
						categoryCentroidVectorList = new ArrayList<List<?>>();
					}
					categoryCentroidVectorList.add(categoryCentroidVector);
					categoryCentroidVectorMap.put(categoryName, categoryCentroidVectorList);
				}
			}
		}
	}

	public static void init() {
		logger = XCLogger.getSpLogger();
		List<List<Object>> lines = null;
		try {
			lines = SpreadSheetReader.getTestData(XCConfig.DOMAINKB_SPREADSHEET_ID, XCConfig.DOMAINKB_RANGE);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (lines != null) {
			for (List<Object> line : lines) {
				String[] strArr = new String[line.size()];
				int index = 0;
				for (Object value : line) {
					strArr[index] = (String) value;
					index++;
				}
				if (strArr.length > 2) {
					if (strArr[0].length() > 1) {
						String seedEntity = strArr[2];
						Set<String> clustersList = entityClustersMap.get(seedEntity);
						if (clustersList == null) {
							clustersList = new HashSet<String>();
						}
						for (int i = 3; i < strArr.length; i++) {
							String[] clusterEntityArr = strArr[i].split(":");
							if (clusterEntityArr.length == 2) {
								String clusterEntity = clusterEntityArr[0];
								try {
									double confidence = Double.parseDouble(clusterEntityArr[1]);

									if (confidence < SIMILARITY_THRESHOLD) {
										break;
									}
									clustersList.add(clusterEntity);
								} catch (NullPointerException | NumberFormatException ex) {
									ex.printStackTrace();
								}
							}
						}
						entityClustersMap.put(seedEntity, clustersList);
					}
				}
			}
		}
		WordVectorDB wvd = XpEngine.getWordVectorDbApp();
		if (XCConfig.IS_WORDVECTOR_CATEGORIES_LOAD) {
			int lineNo = 0;
			ExecutorService executor = Executors.newFixedThreadPool(1);
			DomainKnowledgeBase wb = new DomainKnowledgeBase();
			logger.info("No. of Lines: " + lines.size());
			for (List<Object> line : lines) {
				Runnable worker = wb.new MongoUpdateThread(line, lineNo++);
				executor.execute(worker);
			}
			ThreadUtils.shutdownAndAwaitTermination(executor);
			for (Map.Entry<String, Object> entry : categoryCentroidVectorMap.entrySet()) {
				String categoryName = entry.getKey();
				Object centroidVectorList = entry.getValue();
				wvd.updateData(LANGUAGE, categoryName, SYN_CENTROID_VECTOR, centroidVectorList);
				logger.info("Centroid Vector Calculated for Category: " + categoryName + "\t" + ((centroidVectorList == null)));
			}
		}
	}

	private static Object getCentroidVector(String category) {
		Object centroidVectorList = categoryCentroidVectorMap.get(category);
		if (centroidVectorList == null) {
			centroidVectorList = XpEngine.getWordVectorDbApp().getColumnObjectForKey(LANGUAGE, category, SYN_CENTROID_VECTOR);
			if (centroidVectorList != null) {
				categoryCentroidVectorMap.put(category, centroidVectorList);
			}
		}
		return centroidVectorList;
	}

	private static String getWordVectorCategoryFromCacheAndDB(String entityStr, String govWord, String entityStrLemma) {
		String returnCategory = null;
		EsHttpSearch EsSearch = new EsHttpSearch();
		if (returnCategory == null) {
			//returnCategory = WordVector.getWordVectorCategory(LANGUAGE, entityStr, entityStrLemma, SYN_CATEGORY);
			returnCategory = EsSearch.ElasticSearchCategory(entityStr);
			if (returnCategory == null && govWord != null) {
				String govWordLemma = CoreNLPController.lemmatizeToString(LANGUAGE, govWord);
				//returnCategory = WordVector.getWordVectorCategory(LANGUAGE, govWord, govWordLemma, SYN_CATEGORY);
				returnCategory = EsSearch.ElasticSearchCategory(entityStrLemma);
			}
		}
		return returnCategory;
	}

	public static List<String> getWordVectorCategory(String entityStr, String govWord, boolean isProperNoun) {

		List<String> rawCategoriesList = new ArrayList<String>();
		String entityStrLemma = CoreNLPController.lemmatizeToString(LANGUAGE, entityStr);

		String returnCategory = getWordVectorCategoryFromCacheAndDB(entityStr, govWord, entityStrLemma);
		int matchCase = -1;
		if (returnCategory != null) {
			matchCase = 1;
			rawCategoriesList.add(returnCategory);
		}
		/*commenting below code to avoid unnecessary intent generation
		 * eg. "I am travelling to a beach and i need some nice shorts", "This is not my style" */
		//		else if (!isProperNoun) {
		//			/*No need to predict categories if ProperNoun*/
		//			predictCategories(entityStr, entityStrLemma, rawCategoriesList);
		//			if (rawCategoriesList.size() > 0) {
		//				matchCase = 2;
		//			}
		//		}

		logger.info("WordVector BottomUp Category (Match Case: " + matchCase + "): Entity: " + entityStr + "\tGov: " + govWord + "\t" + rawCategoriesList);
		return rawCategoriesList;

	}

	public static Set<String> getDomainCategory(String domain, List<String> rawCategoriesList) {
		Set<String> domainCategoriesList = new HashSet<String>();
		for (String rawCategory : rawCategoriesList) {
			List<String> domainCategory = DomainCategoryMapper.getMappedCategories(domain, rawCategory);
			if (domainCategory != null) {
				domainCategoriesList.addAll(domainCategory);
			}
		}
		return domainCategoriesList;
	}

	private static void predictCategories(String entityStr, String entityStrLemma, List<String> predictedCategories) {
		String returnCategory = null;
		logger.info("entityStr: " + entityStr + " entityStrLemma: " + entityStrLemma);
		List<?> entityVector = WordVector.getWordVector(LANGUAGE, entityStrLemma);
		logger.info("WordVector BottomUp predictCategories entityStr: " + entityStr + "\tentityStrLemma: " + entityStrLemma + "\tisEntityVectorNull: " + ((entityVector == null)));
		if (entityVector != null && entityVector.size() > 0) {
			double maxSimilarity = 0;

			Map<String, List<String>> attributesCategoriesMap = DomainCategoryMapper.getAttributesCategoriesMap();
			List<String> possibleCategoriesList = attributesCategoriesMap.get("PRODUCT TYPE");

			for (Object currCategory : possibleCategoriesList) {
				String currCategoryStr = (String) currCategory;
				List<?> centroidVectorList = (List<?>) getCentroidVector(currCategoryStr);
				if (centroidVectorList != null) {
					for (Object centroidVector : centroidVectorList) {
						double currSimilarity = WordVector.cosSimilarity((List<?>) centroidVector, entityVector);
						if (currSimilarity > 0) {
							if (currSimilarity > maxSimilarity) {
								returnCategory = currCategoryStr;
								maxSimilarity = currSimilarity;
							}
						}
					}

				}
			}

			final double maximumSimilarityFound = maxSimilarity;
			if (maximumSimilarityFound > RECOMMENDED_SIMILARITY_THRESHOLD && returnCategory != null) {
				logger.info("WordVector BottomUp Category (Match Case: 2): (First Match): " + entityStr + ": " + maximumSimilarityFound + "\t" + returnCategory + "\tFirstMaxSimilarity: " + maximumSimilarityFound);
				predictedCategories.add(returnCategory);
				/*If it is a tryEntity, don't look for predictive match with multiple categories. eg: first episodes*/
				possibleCategoriesList.parallelStream().forEach((category) -> {
					List<?> centroidVectorList = (List<?>) getCentroidVector(category);
					if (centroidVectorList != null) {
						for (Object centroidVector : centroidVectorList) {
							double currSimilarity = WordVector.cosSimilarity((List<?>) centroidVector, entityVector);
							if (currSimilarity > 0) {
								double absoluteDifference = Math.abs(currSimilarity - maximumSimilarityFound);
								if (!predictedCategories.contains(category) && absoluteDifference < 0.08) {
									predictedCategories.add(category);
									break;
								}
							}
						}
					}
				});
			}
			/*If similarity is very close across a lot of categories, it isn't distinctive of any particular category. eg: "print"*/
			if (predictedCategories.size() >= 5) {
				predictedCategories.clear();
			}

		}
	}

	public static Set<String> narrowDownSubCategoryList(String entityStr, Set<String> subCategories) {
		Set<String> approx = new HashSet<String>(new SpellChecker().generateConfusionSetDamerau(entityStr, subCategories));
		if (!approx.isEmpty()) {
			logger.info("Approximate matching top category");
			return approx;
		}

		List<?> entityVector = WordVector.getWordVector(LANGUAGE, entityStr);
		logger.info("WordVector BottomUp predictCategories entityStr: " + entityStr + "\tisEntityVectorNull: " + (entityVector == null));

		Map<String, Double> similarityMap = new HashMap<String, Double>();
		for (String category : subCategories) {
			List<?> categoryVector = WordVector.getWordVector(LANGUAGE, category);
			double similarity = WordVector.cosSimilarity(categoryVector, entityVector);
			similarityMap.put(category, similarity);
		}
		boolean desc = true;
		List<String> orderedCategories = MapUtil.getKeysSortedByValue(similarityMap, desc);
		int nbTopCategories = Math.min(XCConfig.NB_TOP_CATEGORIES, orderedCategories.size());
		Set<String> answ = new HashSet<String>(orderedCategories.subList(0, nbTopCategories));
		return answ;
	}

	public static Set<String> getSimilarEntities(String seedEntity) {
		return entityClustersMap.get(seedEntity);
	}

	public static void main(String[] args) {
		XCEngine.init();
		logger.info(entityClustersMap.toString());
		String entityStr = "dress shirt";
		String govWord = "shirt";
		Scanner s = new Scanner(System.in);
		while (true) {
			if (entityStr != null) {
				List<String> categories = getWordVectorCategory(entityStr, govWord, false);
				logger.info("categories: " + categories.toString());
				Set<String> subCategories = getDomainCategory("xcfashion", categories);
				Set<String> topSubCategories = narrowDownSubCategoryList(entityStr, subCategories);
				logger.info("Top SubCategories: " + topSubCategories);
				entityStr = s.nextLine();
				govWord = s.nextLine();
			} else {
				s.close();
				break;
			}
		}
	}
}
