package com.abzooba.xcommerce.nlp.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.abzooba.xcommerce.nlp.LoadNLPFiles;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCEngine;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xpresso.utils.FileIO;

public class DomainCategoryMapper {

	private static final Map<String, List<String>> attributeCategoriesMap = new LinkedHashMap<String, List<String>>();
	private static final Map<String, Integer> domainIdxMap = new HashMap<String, Integer>();
	public static XCDomain[] domainObjectsArr;

	public static Logger logger = XCLogger.getSpLogger();

	public static void init() {
		try {
			List<String> allLines = LoadNLPFiles.getDomainConfigPath();
			StringBuilder sb = new StringBuilder();
			for (String line : allLines) {
				sb.append(line);
			}
			JSONObject ontologyObj = new JSONObject(sb.toString());
			JSONArray attributesArr = ontologyObj.getJSONArray("Attributes");

			for (int i = 0; i < attributesArr.length(); i++) {
				JSONObject currAttributesObj = attributesArr.getJSONObject(i);
				String attributeName = currAttributesObj.getString("Attribute Name");
				JSONArray categories = currAttributesObj.getJSONArray("Categories");
				List<String> categoriesList = attributeCategoriesMap.get(attributeName);
				if (categoriesList == null) {
					categoriesList = new ArrayList<String>();
				}
				for (int k = 0; k < categories.length(); k++) {
					categoriesList.add(categories.getString(k));
				}

				attributeCategoriesMap.put(attributeName, categoriesList);
			}

			JSONArray ontologyArr = ontologyObj.getJSONArray("Domain");
			domainObjectsArr = new XCDomain[ontologyArr.length()];
			for (int i = 0; i < ontologyArr.length(); i++) {
				JSONObject domainObj = ontologyArr.getJSONObject(i);
				XCDomain currDomain = new XCDomain(domainObj, i);
				String domainName = currDomain.getDomainName();
				domainIdxMap.put(domainName, i);
				domainObjectsArr[i] = currDomain;
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public static Map<String, List<String>> getAttributesCategoriesMap() {
		return attributeCategoriesMap;
	}

	private static XCDomain getDomainObject(String domain) {
		if (domain == null)
			return null;
		int index = domainIdxMap.get(domain);
		return domainObjectsArr[index];
	}

	/**
	 * The method retrieves the domain-specific subcategories corresponding to the granular
	 * subcategory
	 * @param domain
	 * @param granularCatg
	 * @return
	 */
	public static List<String> getMappedCategories(String domain, String granularCatg) {
		XCDomain domainObj = getDomainObject(domain);
		if (domainObj == null)
			return null;
		return domainObj.getMappedCategories(granularCatg);
	}

	/**
	 * Checks whether the granular category belongs to the domain.
	 * @param domain
	 * @param granularCatg
	 * @return
	 */
	public static boolean isCategoryOfDomain(String domain, String granularCatg) {
		XCDomain domainObj = getDomainObject(domain);
		if (domainObj == null)
			return false;
		return domainObj.isCategoryOfDomain(granularCatg);
	}

	public static String getTableName(String domain) {
		XCDomain domainObj = getDomainObject(domain);
		if (domainObj == null) {
			return null;
		}
		return domainObj.getTableName();
	}

	public static String getEndPoint(String domain) {
		XCDomain domainObj = getDomainObject(domain);
		if (domainObj == null) {
			return null;
		}
		return domainObj.getEndPoint();
	}

	public static String getSuggesterEndPoint(String domain) {
		XCDomain domainObj = getDomainObject(domain);
		if (domainObj == null) {
			return null;
		}
		return domainObj.getSuggesterEndPoint();
	}

	public static ArrayList<String> getAllSubCategories() {
		List<String> allCategories = new ArrayList<>();
		ArrayList<String> allSubCategories = new ArrayList<>();
		for (List<String> categoriesMappedToOneAttribute : attributeCategoriesMap.values())
			allCategories.addAll(categoriesMappedToOneAttribute);
		for (String domain : domainIdxMap.keySet())
			for (String category : allCategories) {
				if (getMappedCategories(domain, category) != null) {
					allSubCategories.addAll(getMappedCategories(domain, category));
				}

			}
		allSubCategories.addAll(allCategories);
		return allSubCategories;
	}

	public static void main(String[] args) {
		XCEngine.init();
		logger.info(getMappedCategories("iconic", "dress").toString());
	}

	public static String getS3Url(String domainName) {
		XCDomain domainObj = getDomainObject(domainName);
		if (domainObj == null) {
			return null;
		}
		return domainObj.getS3Url();
	}
}
