package com.abzooba.xcommerce.nlp.attribute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.abzooba.xcommerce.nlp.LoadNLPFiles;
import org.slf4j.Logger;

import com.abzooba.xcommerce.core.XCLogger;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.tregex.TregexMatcher;
import edu.stanford.nlp.trees.tregex.TregexPattern;

public class SizeParser {

	private static Logger logger = XCLogger.getSpLogger();
	private static final TregexPattern tgrepPatternNP = TregexPattern.compile("NP");
	private static final String SIZE = "size";

	private static ArrayList<String> sizeList;
	private static Map<String, String> qualifierMap;

	public static void init() {
		sizeList = LoadNLPFiles.getSizeList();
		logger.info("Size of size list : " + sizeList.size());

		qualifierMap = LoadNLPFiles.getPriceQualifierMap();
		logger.info("Size of QualifierMap : " + qualifierMap.size());
	}

	public static HashMap<CoreLabel, Double> sizeDetectionModule(Tree parseTree, SemanticGraph dependencyGraph, String sentenceStr) {
		HashMap<CoreLabel, Double> sizeTokensWithConfidence = new HashMap<>();
		TregexMatcher tregexMatcher = tgrepPatternNP.matcher(parseTree);
		Set<Tree> trees = new HashSet<>();
		while (tregexMatcher.find()) {
			Tree match = tregexMatcher.getMatch();
			logger.info("NP match : " + match.toString());
			TregexMatcher matcher1 = tgrepPatternNP.matcher(match);
			int counter = 0;
			while (matcher1.find()) {
				counter++;
			}
			if (counter == 1) {
				trees.add(match);
				logger.info("lower level NP : " + match.toString());
			}
		}
		for (Tree tree : trees) {
			List<CoreLabel> taggedWords = tree.taggedLabeledYield();
			boolean flag = false;
			List<CoreLabel> sizeListTokens = new ArrayList<>();
			List<CoreLabel> cardinalSizeTokens = new ArrayList<>();
			for (CoreLabel taggedWord : taggedWords) {
				if (taggedWord.lemma().toLowerCase().equals(SIZE))
					flag = true;
				if (sizeList.contains(taggedWord.originalText().toLowerCase().trim()))
					sizeListTokens.add(taggedWord);
				if (taggedWord.tag().equals("CD"))
					cardinalSizeTokens.add(taggedWord);
			}
			if (flag) {
				if (sizeListTokens.size() > 0) {
					for (CoreLabel sizeToken : sizeListTokens)
						sizeTokensWithConfidence.put(sizeToken, 0.8);
				}
				if (cardinalSizeTokens.size() > 0) {
					for (CoreLabel cardinalSizeToken : cardinalSizeTokens)
						sizeTokensWithConfidence.put(cardinalSizeToken, 0.8);
				}
			} else if (!(sizeTokensWithConfidence.size() > 0)) {
				if (sizeListTokens.size() > 0) {
					for (CoreLabel sizeToken : sizeListTokens)
						sizeTokensWithConfidence.put(sizeToken, 0.6);
				}
				// todo will implement the below code after integrating wit.ai api results also
				/*if (cardinalSizeTokens.size() > 0 && !tree.toString().contains("($") && !StringAttributeUtils.isCurrency(tree.toString()) &&
				        !containsPriceQualifier(tree.toString()) && !(sizeTokensWithConfidence.size() >0)) {
				    for (String cardinalSizeToken : cardinalSizeTokens) {
				        double number = -1;
				        try {
				            number = Double.parseDouble(cardinalSizeToken);
				        } catch (Exception e) {
				            logger.info(cardinalSizeToken + " is not a numeric value");
				        }
				        if ((number < 100) && (number != -1))
				            sizeTokensWithConfidence.put(cardinalSizeToken, 0.6);
				    }
				}*/
			}
		}
		Iterator<SemanticGraphEdge> iterator = dependencyGraph.edgeIterable().iterator();
		while (iterator.hasNext()) {
			SemanticGraphEdge next = iterator.next();
			if (next.getDependent().lemma().equals("size") || next.getGovernor().lemma().equals("size")) {
				if ((next.getRelation().toString().equals("nsubj")) || (next.getRelation().toString().equals("dobj"))) {
					if (next.getDependent().lemma().equals("size")) {
						if (next.getGovernor().tag().equals("CD") || sizeList.contains(next.getGovernor().originalText().toLowerCase().trim()))
							sizeTokensWithConfidence.put(next.getGovernor().backingLabel(), 0.8);
					} else {
						if (next.getDependent().tag().equals("CD") || sizeList.contains(next.getDependent().originalText().toLowerCase().trim()))
							sizeTokensWithConfidence.put(next.getDependent().backingLabel(), 0.8);
					}
				}

			}
		}
		logger.info("Predicted size tokens with confidence : " + sizeTokensWithConfidence);
		return sizeTokensWithConfidence;
	}

	public static ArrayList<String> getSizeList() {
		return sizeList;
	}

	public static void setSizeList(ArrayList<String> sizeList) {
		SizeParser.sizeList = sizeList;
	}

	private static boolean containsPriceQualifier(String snippet) {
		boolean contains = false;
		for (String qualifier : qualifierMap.keySet()) {
			if (snippet.matches("(.*)"+qualifier+"(.*)")) {
				contains = true;
				break;
			}
		}
		return contains;
	}
}
