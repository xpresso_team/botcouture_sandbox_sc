package com.abzooba.xcommerce.nlp.attribute.price;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xcommerce.nlp.LoadNLPFiles;
import com.abzooba.xcommerce.utils.StringAttributeUtils;
import com.abzooba.xpresso.engine.core.XpLexMatch;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.UniversalEnglishGrammaticalRelations;
import edu.stanford.nlp.trees.tregex.TregexMatcher;
import edu.stanford.nlp.trees.tregex.TregexPattern;
import org.slf4j.Logger;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author Sudhanshu Kumar
 * 12-Apr-2014 10:00 AM
 * 
 */
public class PriceParser {

	public static final String LESS_ATTRIBUTE = "less";
	public static final String GREATER_ATTRIBUTE = "greater";
	public static final String BETWEEN_ATTRIBUTE = "between";
	public static final String LESSER_SIGN = "<";
	public static final String GREATER_SIGN = ">";
	public static final String BETWEEN_SIGN = "<>";
	public static final String EQUALS_SIGN = "=";

	private static final TregexPattern tgrepPatternADVQP = TregexPattern.compile("ADVP");
	private static final TregexPattern tgrepPatternADJQP = TregexPattern.compile("ADJP");
	private static final TregexPattern tgrepPatternPP = TregexPattern.compile("PP");
	private static final TregexPattern tgrepPatternQP = TregexPattern.compile("QP");
	/* For NP, the match should be restricted to explicit price using currency or price qualifiers */
	//	private static final TregexPattern tgrepPatternNP = TregexPattern.compile("NP < /\\$/");
	private static final TregexPattern tgrepPatternNP = TregexPattern.compile("NP");
	private static final TregexPattern tgrepPatternFRAG = TregexPattern.compile("FRAG");
	private static Map<String, String> qualifierMap;
	private static Map<String, String> ambiguousQualifierMap;
	private static Map<String, String> multiplierMap;
	private static Logger logger;

	private static Map<String, String[]> numberTextMap;

	public static String[] getNumberForText(String text) {
		return numberTextMap.get(text);
	}

	public static void init() {
		logger = XCLogger.getSpLogger();

		qualifierMap = LoadNLPFiles.getPriceQualifierMap();
		logger.info("Size of QualifierMap : " + qualifierMap.size());

		ambiguousQualifierMap = LoadNLPFiles.getAmbiguousPriceQualifierMap();
		logger.info("Size of Ambiguous QualifierMap : " + ambiguousQualifierMap.size());

		multiplierMap = LoadNLPFiles.getNumberMultiplierMap();
		numberTextMap = LoadNLPFiles.getTextNumberMap();
	}

	/**
	 * The method returns price qualifier tags from the identified price tags
	 * @param valueSet
	 * @return
	 */
	public static String getPriceQualifier(Set<String> valueSet) {
		for (String value : valueSet) {
			value = value.trim();
			if (value.startsWith(PriceParser.BETWEEN_SIGN))
				return PriceParser.BETWEEN_SIGN;
			else if (value.startsWith(PriceParser.LESSER_SIGN))
				return PriceParser.LESSER_SIGN;
			else if (value.startsWith(PriceParser.GREATER_SIGN))
				return PriceParser.GREATER_SIGN;
			else if (value.startsWith(PriceParser.EQUALS_SIGN))
				return PriceParser.EQUALS_SIGN;
		}
		return "";
	}

	public static String getPriceValue(Set<String> valueSet) {
		for (String value : valueSet) {
			value = value.trim().replaceAll("[^\\d\\.]+", " ");
			value = value.replaceAll("[\\s]+", " ");
			return value.trim();
		}
		return "";

	}

	/**
	 * The method checks if the price qualifier is negated
	 * @param graph
	 * @param qualifier
	 * @return
	 */
	private static boolean isQualifierNegated(SemanticGraph graph, String qualifier) {
		logger.info("Qualifier - " + qualifier);
		if (graph == null)
			return false;
		IndexedWord negationWord = graph.getNodeByWordPattern("not");
		if (graph.containsVertex(negationWord)) {
			logger.info("Negation is there with price qualifier");
			return true;
		}
		return false;
	}

	public static String priceDetectionModuleExhaustive(Tree parseTree, SemanticGraph dependencyGraph, String sentenceStr) {
		logger = XCLogger.getSpLogger();
		logger.info(parseTree.toString());
		TregexMatcher matcher = tgrepPatternADVQP.matcher(parseTree);
		Map<String, String> priceStrMap = new HashMap<String, String>();
		HashMap<Tree, ArrayList<Tree>> matchedTrees = new HashMap<>();
		Set<Tree> upperLevelTree = new HashSet<>();
		Set<Tree> lowerLevelTree = new HashSet<>();
		String priceStr = null;
		String value = null;
		Tree matchedTree = null;
		while (matcher.find()) {
			matchedTree = matcher.getMatch();
			TregexMatcher matcher1 = tgrepPatternADVQP.matcher(matchedTree);
			int counter = 0;
			while (matcher1.find()) {
				counter++;
			}
			if (counter == 1) {
				logger.info("matched ADVQP: " + matchedTree);
				lowerLevelTree.add(matchedTree);
				if (containsCurrencySymbol(matchedTree.toString()) || containsPriceQualifier(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString())) {
					matchedTrees.put(matchedTree, null);
				} else if (containsAmbiguousPriceQualifier(matchedTree.toString()) && (containsCurrencySymbol(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString()))) {
					matchedTrees.put(matchedTree, null);
				}
			} else {
				if (containsCurrencySymbol(matchedTree.toString()) || containsPriceQualifier(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString())) {
					upperLevelTree.add(matchedTree);
				} else if (containsAmbiguousPriceQualifier(matchedTree.toString()) && (containsCurrencySymbol(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString()))) {
					upperLevelTree.add(matchedTree);
				}
			}
		}

		matcher = tgrepPatternADJQP.matcher(parseTree);
		while (matcher.find()) {
			matchedTree = matcher.getMatch();
			TregexMatcher matcher1 = tgrepPatternADJQP.matcher(matchedTree);
			int counter = 0;
			while (matcher1.find()) {
				counter++;
			}
			if (counter == 1) {
				/* 100% cotton shirts */
				lowerLevelTree.add(matchedTree);
				if (!matchedTree.toString().contains("%")) {
					logger.info("matched ADJQP: " + matchedTree);
					if (containsCurrencySymbol(matchedTree.toString()) || containsPriceQualifier(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString())) {
						matchedTrees.put(matchedTree, null);
					} else if (containsAmbiguousPriceQualifier(matchedTree.toString()) && (containsCurrencySymbol(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString()))) {
						matchedTrees.put(matchedTree, null);
					}
				}
			} else {
				if (!matchedTree.toString().contains("%")) {
					if (containsCurrencySymbol(matchedTree.toString()) || containsPriceQualifier(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString())) {
						upperLevelTree.add(matchedTree);
					} else if (containsAmbiguousPriceQualifier(matchedTree.toString()) && (containsCurrencySymbol(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString()))) {
						upperLevelTree.add(matchedTree);
					}
				}
			}
		}

		matcher = tgrepPatternPP.matcher(parseTree);
		while (matcher.find()) {
			matchedTree = matcher.getMatch();
			logger.info("matched parent PP: " + matchedTree);
			TregexMatcher matcher1 = tgrepPatternPP.matcher(matchedTree);
			int counter = 0;
			while (matcher1.find()) {
				counter++;
			}
			if (counter == 1) {
				lowerLevelTree.add(matchedTree);
				logger.info("matched PP: " + matchedTree);
				if (containsCurrencySymbol(matchedTree.toString()) || containsPriceQualifier(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString())) {
					matchedTrees.put(matchedTree, null);
				} else if (containsAmbiguousPriceQualifier(matchedTree.toString()) && (containsCurrencySymbol(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString()))) {
					matchedTrees.put(matchedTree, null);
				}
			} else {
				if (containsCurrencySymbol(matchedTree.toString()) || containsPriceQualifier(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString())) {
					upperLevelTree.add(matchedTree);
				} else if (containsAmbiguousPriceQualifier(matchedTree.toString()) && (containsCurrencySymbol(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString()))) {
					upperLevelTree.add(matchedTree);
				}
			}
		}

		matcher = tgrepPatternQP.matcher(parseTree);
		while (matcher.find()) {
			matchedTree = matcher.getMatch();
			TregexMatcher matcher1 = tgrepPatternQP.matcher(matchedTree);
			int counter = 0;
			while (matcher1.find()) {
				counter++;
			}
			if (counter == 1) {
				lowerLevelTree.add(matchedTree);
				logger.info("matched QP: " + matchedTree);
				if (containsCurrencySymbol(matchedTree.toString()) || containsPriceQualifier(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString())) {
					matchedTrees.put(matchedTree, null);
				} else if (containsAmbiguousPriceQualifier(matchedTree.toString()) && (containsCurrencySymbol(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString()))) {
					matchedTrees.put(matchedTree, null);
				}
			} else {
				if (containsCurrencySymbol(matchedTree.toString()) || containsPriceQualifier(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString())) {
					upperLevelTree.add(matchedTree);
				} else if (containsAmbiguousPriceQualifier(matchedTree.toString()) && (containsCurrencySymbol(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString()))) {
					upperLevelTree.add(matchedTree);
				}
			}
		}

		matcher = tgrepPatternNP.matcher(parseTree);
		while (matcher.find()) {
			matchedTree = matcher.getMatch();
			/* The following test excludes noun phrases containing PRP$ (possessives) etc.
			 * It also matches NP containing price qualifiers to avoid catching all NP with numbers
			 * It also matches NP containing currency */
			TregexMatcher matcher1 = tgrepPatternNP.matcher(matchedTree);
			int counter = 0;
			while (matcher1.find()) {
				counter++;
			}
			if (counter == 1) {
				lowerLevelTree.add(matchedTree);
				if (containsCurrencySymbol(matchedTree.toString()) || containsPriceQualifier(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString())) {
					logger.info("matched NP: " + matchedTree);
					matchedTrees.put(matchedTree, null);
				} else if (containsAmbiguousPriceQualifier(matchedTree.toString()) && (containsCurrencySymbol(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString()))) {
					matchedTrees.put(matchedTree, null);
				}
			} else {
				if (containsCurrencySymbol(matchedTree.toString()) || containsPriceQualifier(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString())) {
					upperLevelTree.add(matchedTree);
				} else if (containsAmbiguousPriceQualifier(matchedTree.toString()) && (containsCurrencySymbol(matchedTree.toString()) || StringAttributeUtils.isCurrency(matchedTree.toString()))) {
					upperLevelTree.add(matchedTree);
				}
			}
		}

		matcher = tgrepPatternFRAG.matcher(parseTree);
		while (matcher.find()) {
			matchedTree = matcher.getMatch();
			TregexMatcher matcher1 = tgrepPatternFRAG.matcher(matchedTree);
			int counter = 0;
			while (matcher1.find()) {
				counter++;
			}
			if (counter == 1) {
				lowerLevelTree.add(matchedTree);
				if (matchedTree.toString().contains("(CD") || containsCurrencySymbol(matchedTree.toString())) {
					logger.info("matched FRAG: " + matchedTree);
					matchedTrees.put(matchedTree, null);
				} else if (containsAmbiguousPriceQualifier(matchedTree.toString()) && (containsCurrencySymbol(matchedTree.toString()) || matchedTree.toString().contains("(CD"))) {
					matchedTrees.put(matchedTree, null);
				}
			} else {
				if (matchedTree.toString().contains("(CD") || containsCurrencySymbol(matchedTree.toString())) {
					upperLevelTree.add(matchedTree);
				} else if (containsAmbiguousPriceQualifier(matchedTree.toString()) && (containsCurrencySymbol(matchedTree.toString()) || matchedTree.toString().contains("(CD"))) {
					upperLevelTree.add(matchedTree);
				}
			}
		}
		addTopLevelPatternWithOverlapPattern(matchedTrees, upperLevelTree, lowerLevelTree);

		for (Entry<Tree, ArrayList<Tree>> mTree : matchedTrees.entrySet()) {
			logger.info("Lower level pattern tree " + mTree.getKey().value() + " : " + mTree.getValue());
			priceStr = getPriceStr(mTree, dependencyGraph, logger);
			if (priceStr != null && priceStr.split(" ", 2).length > 1) {
				value = priceStr.split(" ", 2)[1];
				if (priceStrMap.containsKey(value)) {
					String op1 = priceStrMap.get(value).split(" ")[0];
					String op2 = priceStr.split(" ")[0];
					String op = combineOperators(op1, op2, true);
					priceStrMap.put(value, op + " " + value);
				} else {
					/* Case 500 1000 -> <> 500 1000 in the map and trying to put 500 -> = 500 or similar */
					boolean isPartOfRange = false;
					for (String key : priceStrMap.keySet()) {
						if (key.contains(value)) {
							isPartOfRange = true;
							break;
						}
					}
					if (!isPartOfRange)
						priceStrMap.put(value, priceStr);
				}
			}

		}

		return combinePriceStrings(priceStrMap);

	}

	private static void addTopLevelPatternWithOverlapPattern(HashMap<Tree, ArrayList<Tree>> matchedTrees, Set<Tree> upperLevelTree, Set<Tree> lowerLevelTree) {
		if ((upperLevelTree.size() > 0) && (lowerLevelTree.size() > 0)) {
			Iterator<Tree> iterator = upperLevelTree.iterator();
			if (iterator.hasNext()) {
				Tree tree = iterator.next();
				ArrayList<Tree> overlapTree = new ArrayList<>();
				Iterator<Tree> iterator1 = tree.iterator();
				for (Tree lowerLevel : lowerLevelTree) {
					Iterator<Tree> iterator2 = lowerLevel.iterator();
					while (iterator2.hasNext()) {
						Tree next2 = iterator2.next();
						while (iterator1.hasNext()) {
							Tree next1 = iterator1.next();
							logger.info(next1.toString() + " : " + next2.toString());
							if (next2.equals(next1)) {
								logger.info("Main tree " + tree.toString() + " and overlap tree " + next1.toString());
								overlapTree.add(next1);
							}
						}
					}
				}
				matchedTrees.put(tree, overlapTree);
			}
		}
	}

	/**
	 * The method extracts the price qualifier and the price value from the string marked in the
	 * query as that containing price-related text
	 * @param t
	 * @param graph
	 * @param logger
	 * @return
	 */
	private static String getPriceStr(Entry<Tree, ArrayList<Tree>> t, SemanticGraph graph, Logger logger) {
		List<CoreLabel> lowerLevelTreeLabelsList = new ArrayList<>();
		if (t.getValue() != null) {
			for (Tree tree : t.getValue()) {
				lowerLevelTreeLabelsList.addAll(tree.taggedLabeledYield());
			}
		}
		List<CoreLabel> labelList = (ArrayList<CoreLabel>) t.getKey().taggedLabeledYield();
		if (lowerLevelTreeLabelsList.size() > 0) {
			List<CoreLabel> union = new ArrayList<>(labelList);
			union.addAll(lowerLevelTreeLabelsList);
			List<CoreLabel> intersection = new ArrayList<>(labelList);
			Iterator<CoreLabel> iterator = intersection.iterator();
			while (iterator.hasNext()) {
				if (!lowerLevelTreeLabelsList.contains(iterator.next()))
					intersection.remove(iterator);
			}
			//			intersection.retainAll(lowerLevelTreeLabelsList);
			union.removeAll(intersection);
			labelList = new ArrayList<>(union);
		}
		StringBuilder builder = new StringBuilder();
		if (labelList.size() > 0) {
			for (CoreLabel tWord : labelList) {
				if (builder.length() > 0) {
					builder.append(" ");
				}
				builder.append(tWord.word());
			}
			String str = builder.toString();
			if (str.contains("$") || str.contains("rs") || str.contains("₹") || containsPriceQualifier(str) || StringAttributeUtils.isCurrency(str)) {
				StringBuilder sb = new StringBuilder();
				StringBuilder valueSb = new StringBuilder();
				String comparisonOp = null;
				String tempOp = null;
				boolean isNegated = false;
				int nbValues = 0;
				for (CoreLabel word : labelList) {

					String tag = word.tag();
					String wordStr = word.word();
					if (tag.matches("(JJ[RS]*)|(RB[RS]*)|(IN)|(NN[PS]*)")) {
						String[] numberArr = getNumberForText(wordStr);
						if (numberArr == null) {
							tempOp = PriceParser.isPriceQualifier(wordStr);
							if ((tempOp == null) && (comparisonOp == null))
								tempOp = PriceParser.isAmbiguousPriceQualifier(wordStr);
							logger.info("tempOp: " + tempOp);
							if (tempOp != null) {
								comparisonOp = tempOp;
								isNegated = isQualifierNegated(graph, wordStr);
							}
						}
					} else if (tag.matches("CD")) {
						String targetWord = word.word();
						logger.info("tempWord: " + targetWord);
						String[] strArr = targetWord.split("-");
						for (String tempWord : strArr) {
							if (tempWord.matches("[\\d\\.]+")) {
								nbValues += 1;
								if (valueSb.length() > 0)
									valueSb.append(" ");
								valueSb.append(tempWord);
							} else if (isMultiplier(tempWord) != null) {
								if (valueSb.length() > 0)
									valueSb.append(isMultiplier(tempWord));
							}
						}
					}
				}
				if (comparisonOp == null && nbValues == 2) {
					logger.info("catching missing price range");
					comparisonOp = BETWEEN_SIGN;
				}
				if (comparisonOp == null && nbValues == 1) {
					String priceSnippet = Sentence.listToString(t.getKey().yield());
					logger.info("catching missing multi-word comparison operator in " + priceSnippet);
					comparisonOp = qualifierMap.get(XpLexMatch.sequenceContain(qualifierMap.keySet(), priceSnippet));
				}
				logger.info("comparisonOp - " + comparisonOp + " value - " + valueSb);

				if (valueSb.length() == 0) {
					PriceStruct ps = extractTextPriceValue(t.getKey(), graph, logger);
					if (!ps.isAllFieldsNull())
						valueSb.append(ps.toNumber());
				}

				logger.info("valueSb: " + valueSb);
				if (valueSb.length() > 0) {

					if (!valueSb.toString().contains(" ")) {
						IndexedWord valueWord = graph.getNodeByWordPattern(valueSb.toString().trim());
						if (valueWord != null) {
							IndexedWord ccWord = graph.getChildWithReln(valueWord, UniversalEnglishGrammaticalRelations.CONJUNCT);
							if (ccWord != null && ccWord.tag().equals("CD")) {
								valueSb.append(" ");
								valueSb.append(ccWord.word());
							}
						}
					}

					if (comparisonOp != null) {
						comparisonOp = comparisonOpMap(comparisonOp, isNegated);
						/* When 2 values are identified, forcing the input of <> */
						if (valueSb.toString().split(" ").length > 1)
							sb.append(BETWEEN_SIGN);
						else
							sb.append(comparisonOp);
						sb.append(" ");
						sb.append(valueSb);
					} else {
						sb.append(EQUALS_SIGN);
						sb.append(" ");
						sb.append(valueSb);
					}
				}
				return sb.toString();
			}
		}
		return null;
	}

	private static String combineOperators(String op1, String op2, boolean areSameValues) {
		if ((LESSER_SIGN.equals(op1) && (EQUALS_SIGN.equals(op2) || op2 == null)) || (LESSER_SIGN.equals(op2) && (EQUALS_SIGN.equals(op1) || op1 == null)) || (LESSER_SIGN.equals(op2) && LESSER_SIGN.equals(op1)))
			return LESSER_SIGN;

		if ((GREATER_SIGN.equals(op1) && (EQUALS_SIGN.equals(op2) || op2 == null)) || (GREATER_SIGN.equals(op2) && (EQUALS_SIGN.equals(op1) || op1 == null)) || (GREATER_SIGN.equals(op2) && GREATER_SIGN.equals(op1)))
			return GREATER_SIGN;

		if (areSameValues) {
			if (BETWEEN_SIGN.equals(op1) || BETWEEN_SIGN.equals(op2))
				return BETWEEN_SIGN;
			else
				return EQUALS_SIGN;
		}

		return BETWEEN_SIGN;
	}

	/**
	 * The method combines extracted price strings into a single price string
	 * @param priceStrMap
	 * @return priceStr
	 */
	private static String combinePriceStrings(Map<String, String> priceStrMap) {
		if (priceStrMap.size() == 1) {
			logger.info("Case 1: Single price string");
			for (Entry<String, String> entry : priceStrMap.entrySet()) {
				String priceStr = entry.getValue();
				if (priceStr.split(" ")[0].equals(EQUALS_SIGN)) {
					logger.info("Case 1: single value, equal sign");
					StringBuilder sb = new StringBuilder();
					sb.append(BETWEEN_SIGN);
					sb.append(" ");
					String value = entry.getKey();
					sb.append(Math.max(0, Integer.parseInt(value) * (100 - XCConfig.PRICE_DELTA) / 100));
					sb.append(" ");
					sb.append(Integer.parseInt(value) * (100 + XCConfig.PRICE_DELTA) / 100);
					return sb.toString();
				}
				return priceStr;
			}
		}
		if (priceStrMap.size() == 2) {
			logger.info("Case 2: Two price strings");
			List<String> opArr = new ArrayList<String>();
			List<String> values = new ArrayList<String>();
			for (Entry<String, String> entry : priceStrMap.entrySet()) {
				opArr.add(entry.getValue().split(" ")[0]);
				values.add(entry.getKey());
			}
			boolean areSameValues = values.get(0).equals(values.get(1));
			String finalOp = combineOperators(opArr.get(0), opArr.get(1), areSameValues);
			StringBuilder sb = new StringBuilder();
			sb.append(finalOp);
			sb.append(" ");
			sb.append(values.get(0));
			sb.append(" ");
			sb.append(values.get(1));
			return sb.toString();
		}
		logger.info("Case 3: priceStrMap.size() = " + priceStrMap.size());
		return null;
	}

	/**
	 * The method checks if the given word is a price qualifier or not
	 * @param word
	 * @return
	 */
	public static String isPriceQualifier(String word) {
		word = word.toLowerCase();
		if (qualifierMap.containsKey(word))
			return qualifierMap.get(word);
		for (Entry<String, String> entry : qualifierMap.entrySet()) {
			if (word.matches(entry.getKey()))
				return entry.getValue();
		}
		return null;
	}

	public static String isAmbiguousPriceQualifier(String word) {
		word = word.toLowerCase();
		if (ambiguousQualifierMap.containsKey(word))
			return ambiguousQualifierMap.get(word);
		for (Entry<String, String> entry : ambiguousQualifierMap.entrySet()) {
			if (word.matches(entry.getKey()))
				return entry.getValue();
		}
		return null;
	}

	private static boolean containsPriceQualifier(String snippet) {
		boolean contains = false;
		for (String qualifier : qualifierMap.keySet()) {
			if (snippet.matches("(.*)" + qualifier + "(.*)")) {
				contains = true;
				break;
			}
		}
		return contains;
	}

	private static boolean containsAmbiguousPriceQualifier(String snippet) {
		boolean contains = false;
		for (String qualifier : ambiguousQualifierMap.keySet()) {
			if (snippet.matches("(.*)" + qualifier + "(.*)")) {
				contains = true;
				break;
			}
		}
		return contains;
	}

	private static boolean containsCurrencySymbol(String snippet) {
		boolean contains = false;
		String[] currencySymbols = new String[] {"($", "₹", "rs"};
		for (String currencySymbol : currencySymbols) {
			if (snippet.toLowerCase().contains(currencySymbol)){
				logger.info("Currency Symbol found: " + currencySymbol);
				contains = true;
				break;
			}
		}
		return contains;
	}

	private static String isMultiplier(String word) {
		word = word.toLowerCase();
		if (multiplierMap.containsKey(word))
			return multiplierMap.get(word);
		for (Entry<String, String> entry : multiplierMap.entrySet()) {
			if (word.matches(entry.getKey()))
				return entry.getValue();
		}
		return null;
	}

	/**
	 * The method computes the price qualifier based on the operator found and the negation flag
	 * @param op
	 * @param isNegated
	 * @return
	 */
	private static String comparisonOpMap(String op, boolean isNegated) {
		String opVal = null;
		if (op.matches("[<]+"))
			opVal = (!isNegated) ? LESSER_SIGN : GREATER_SIGN;
		else if (op.matches("[>]+"))
			opVal = (!isNegated) ? GREATER_SIGN : LESSER_SIGN;
		else if (op.matches("<>"))
			opVal = BETWEEN_SIGN;
		else if (op.matches("="))
			opVal = EQUALS_SIGN;
		return opVal;
	}

	/**
	 * The method extracts price in numerical form from the price in text format query string
	 * @param t
	 * @param graph
	 * @param logger
	 * @return
	 */
	public static PriceStruct extractTextPriceValue(Tree t, SemanticGraph graph, Logger logger) {
		PriceStruct ps = new PriceStruct();
		List<CoreLabel> tagWordList = t.taggedLabeledYield();
		StringBuilder tempSB = new StringBuilder();
		for (CoreLabel tagWord : tagWordList) {
			String wordStr = tagWord.word();
			String tag = tagWord.tag();
			if (tag.equals("CD")) {
				if (wordStr.matches("(thousand[s]*)")) {
					if (ps.getTens() != null) {
						tempSB.append(ps.getTensStr());
						ps.setTensNull();
					}
					if (tempSB.length() > 0) {
						tempSB.append(" ");
					}
					tempSB.append("thousand");
					String[] numberArr = getNumberForText(tempSB.toString());
					if (numberArr != null)
						ps.setThousand(Integer.parseInt(numberArr[1]));

					tempSB = new StringBuilder();

				} else if (wordStr.matches("(hundred[s]*)")) {
					if (ps.getTens() != null) {
						tempSB.append(ps.getTensStr());
						ps.setTensNull();
					}
					if (tempSB.length() > 0) {
						tempSB.append(" ");
					}
					tempSB.append("hundred");
					String[] numberArr = getNumberForText(tempSB.toString());
					if (numberArr != null) {
						//						logger.info("numberArr length - " + numberArr.length);
						ps.setHundred(Integer.parseInt(numberArr[1]));
					}
					tempSB = new StringBuilder();
				} else {
					String[] numberArr = getNumberForText(wordStr);
					if (numberArr != null && numberArr[0].equals("ten")) {
						ps.setTens(Integer.parseInt(numberArr[1]));
						ps.setTensStr(wordStr);
					} else
						tempSB.append(wordStr);
				}
			} else if (tag.matches("(NN[S]*)|(JJ)")) {
				String[] numberArr = getNumberForText(wordStr);
				if (numberArr != null && numberArr[1].equals("tens")) {
					ps.setTens(Integer.parseInt(numberArr[1]));
					ps.setTensStr(wordStr);
				}
			}
		}
		if (tempSB.length() > 0) {
			String[] numberArr = getNumberForText(tempSB.toString());
			if (numberArr != null)
				ps.setUnit(Integer.parseInt(numberArr[1]));
		}
		return ps;
	}
}
