
package com.abzooba.xcommerce.nlp.attribute;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.abzooba.xcommerce.core.XCLogger;
import edu.stanford.nlp.ling.IndexedWord;
import org.slf4j.Logger;

public class SubjectiveSearch {

	private static Logger logger = XCLogger.getSpLogger();
	protected static final Class<?> c = SubjectiveSearch.class;
	private static Set<String> modifyWords = Stream.of("adorable", "astonishing", "attractive", "average", "awesome", "bland", "blushing", "bright", "charming", "cheap", "cheerful", "clean", "clear", "colorful", "comfortable", "contemplative", "convincing", "costly", "creepy", "dashing", "delightful", "distinct", "dull", "ecstatic", "elated", "elegant", "enchanting", "encouraging", "energetic", "enormous", "enthusiastic", "excited", "fancy", "fantastic", "fashioned", "filthy", "flat", "flat", "floppy", "foolish", "foolish", "frantic", "fresh", "fresh", "friendly", "friendly", "frightened", "frothy", "frustrating", "frustrating", "funny", "funny", "fuzzy", "fuzzy", "gaudy", "gaudy", "gentle", "gentle", "ghastly", "ghastly", "giddy", "gigantic", "gigantic", "glamorous", "glamorous", "gleaming", "gleaming", "glorious", "glorious", "gorgeous", "gorgeous", "graceful", "graceful", "greasy", "grieving", "gritty", "grotesque", "grubby", "grumpy", "handsome", "handsome", "happy", "happy", "harebrained", "healthy", "healthy", "helpful", "helpful", "helpless", "high", "high", "high", "hollow", "hollow", "homely", "homely", "horrific", "huge", "huge", "hungry", "hurt", "icy", "icy", "ideal", "ideal", "immense", "impressionable", "impressionable", "intrigued", "jealous", "jittery", "jittery", "jolly", "jolly", "joyous", "joyous", "juicy", "jumpy", "kind", "large", "lazy", "lethal", "little", "lively", "livid", "lonely", "loose", "lovely", "lucky", "ludicrous", "macho", "magnificent", "mammoth", "maniacal", "massive", "miniature", "minute", "mistaken", "misty", "moody", "motionless", "muddy", "mysterious", "narrow", "nasty", "naughty", "nervous", "nonchalant", "nonsensical", "nutty", "obedient", "oblivious", "obnoxious", "odd", "old", "outrageous", "panicky", "perfect", "perplexed", "petite", "petty", "plain", "pleasant", "pompous", "precious", "proud", "puny", "quaint", "quizzical", "ratty", "reassured", "relieved", "repulsive", "responsive", "responsive", "ripe", "robust", "rotten", "rotund", "rough", "round", "salty", "sarcastic", "scant", "scary", "scattered", "scrawny", "selfish", "shaggy", "shaky", "shallow", "sharp", "shiny", "short", "silky", "silly", "skinny", "slimy", "slippery", "small", "smarmy", "smiling", "smoggy", "smooth", "smug", "soggy", "solid", "sore", "sour", "sparkling", "splendid", "spotless", "strange", "strong", "stunning", "swanky", "sweet", "teeny", "terrible", "thick", "tight", "wacky", "wonderful").collect(Collectors.toCollection(HashSet::new));
	private static Set<String> catalogWords = Stream.of("backpack", "bag", "belt", "bikini", "blazer", "blouse", "boardshort", "bodysuit", "boots", "bra", "bracelet", "bralette", "briefs", "cap", "coat", "cuff", "denim", "dress", "earring", "hoodie", "jacket", "jean", "jeans", "jeggings", "joggers", "jumpsuit", "knickers", "lace", "leather", "leggings", "maillot", "necklace", "nightdress", "one piece", "pant", "panties", "pants", "pantsuit", "piece", "playsuit", "ring", "sandal", "shirt", "shirt", "shoe", "shoe", "short", "shorts", "shoulderbag", "sleeve", "sock", "sweater", "sweatshirt", "swimshort", "swimsuit", "tee", "tie", "tights", "towel", "track", "trackpant", "trousers", "trunk", "two piece", "vest", "waistbag", "walkshort", "walkshorts", "wallet", "watch", "wetsuit", "woman", "yoga").collect(Collectors.toCollection(HashSet::new));

	public static String amod_fireRule(IndexedWord gov, IndexedWord dep) {
		String depWordLemma = dep.lemma().toLowerCase();
		String govWordLemma = gov.lemma().toLowerCase();
		logger.info("Amod: " + govWordLemma + " to " + depWordLemma);
		if (catalogWords.contains(govWordLemma)) {
			logger.info(govWordLemma + " is govWordLemma");
			if (modifyWords.contains(depWordLemma)) {
				logger.info(depWordLemma + " is depWordLemma");
			}
		}
		return null;
	}
}
