package com.abzooba.xcommerce.nlp.attribute.price;

import com.abzooba.xcommerce.core.XCEngine;
import com.abzooba.xpresso.engine.core.XpText;
import edu.emory.mathcs.backport.java.util.Collections;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.Label;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.Annotator;
import edu.stanford.nlp.util.ArraySet;
import edu.stanford.nlp.util.CoreMap;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Created by mayank on 18/4/17.
 */
public class PriceAnnotator implements Annotator {
    @Override
    public void annotate(Annotation annotation) {
        List<CoreMap> coreMaps = annotation.get(CoreAnnotations.SentencesAnnotation.class);
        for(CoreMap coreMap:coreMaps)
        {
            List<CoreLabel> coreLabels = coreMap.get(CoreAnnotations.TokensAnnotation.class);
            for(CoreLabel coreLabel:coreLabels) {
                if(coreLabel.tag().equals("CD")) {
                    coreLabel.set(CoreAnnotations.NamedEntityTagAnnotation.class, "PRICE");
//                    coreLabel.set(CoreAnnotations., "SIZE");
                }
            }
        }
    }

    @Override
    public Set<Requirement> requirementsSatisfied() {
        return Collections.singleton(CoreAnnotations.NamedEntityTagAnnotation.class);
    }

    @Override
    public Set<Requirement> requires() {
        return Collections.unmodifiableSet(new ArraySet<>(Arrays.asList(
            CoreAnnotations.TextAnnotation.class,CoreAnnotations.TokensAnnotation.class,
                CoreAnnotations.SentencesAnnotation.class,CoreAnnotations.PartOfSpeechAnnotation.class
        )));
    }

    public static void main(String[] args) {
        XCEngine.init();
        XpText xpText = new XpText("I need a shirt for 50$");
        Annotation document = xpText.getDocument();
        PriceAnnotator priceAnnotator = new PriceAnnotator();
        priceAnnotator.annotate(document);
        List<CoreLabel> coreLabels = document.get(CoreAnnotations.TokensAnnotation.class);
        for(CoreLabel coreLabel:coreLabels)
        {
            System.out.println(coreLabel.get(CoreAnnotations.NamedEntityTagAnnotation.class));
        }
    }
}
