package com.abzooba.xcommerce.nlp.attribute;

import com.abzooba.xcommerce.core.XCEntity;
import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xcommerce.ontology.server.Neo4jServer;
import com.abzooba.xcommerce.search.SearchUtils;
import com.abzooba.xpresso.textanalytics.CoreNLPController;
import edu.stanford.nlp.ling.IndexedWord;
import org.slf4j.Logger;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Sudhanshu Kumar
 * 12-Apr-2014 10:00 AM
 * 
 */
public class GenderSearch {

	public static String MALE_TAG = "men";
	public static String FEMALE_TAG = "women";
	public static String UNISEX_TAG = "unisex";
	private static Pattern MALE_PATTERN = Pattern.compile("(\\bfiance\\b)|(^man[\'s]*\\b)|(\\bman[\'s]*\\b)|(\\bman[\'s]*$)|(^male[\'s]*)|(\\bmale[\'s]*)|(^men[\'s]*)|(\\bmen[\'s]*)|(^boy[\'s | friend]*)|(\\bboy[\'s]*)|(husband[\'s]*)|(\\bbro[ther\'s]*\\b)|(uncle[\'s]*)|(nephew[\'s]*)|(father[\'s]*)|(\\bgent[\'s]*)|(^gent[\'s]*)|(^dad[\'s]*)|(\\bdad[\'s]*)|(^son[\'s]*)|(\\bson[\'s]*)|(^grand[ad|pa][\'s]*)|(\\bgrand[ad|pa][\'s]*)|(guy[\'s])|(\\bhim[self]*\\b)|(\\bhis\\b)|(\\bhe\\b)|(\\bpapa\\b)");
	private static Pattern FEMALE_PATTERN = Pattern.compile("(\\bfiancee\\b)|(woman[\'s]*)|(female[\'s]*)|(women[\'s]*)|(girl[\'s | friend]*)|(wife[\'s]*)|(sis[ter\'s]*)|(aunt(y|(ie))*)|(niece[\'s]*)|(mother[\'s]*)|(lad[y\'ies]*)|(mom[\'s]*)|(daughter[\'s]*)|(^granny[\'s]*)|(\\bgranny[\'s]*)|(\\bher[self]*\\b)|(\\bshe\\b)|(\\bgrandma\\b)");
	private static Pattern UNISEX_PATTERN = Pattern.compile("(\\bunisex\\b)");
	public static Set<String> IMPLICIT_WOMEN_ITEMS = new HashSet<String>();
	public static Set<String> IMPLICIT_MEN_ITEMS = new HashSet<String>();
	public static Set<String> NOT_ALLOWED_LEVENSHTEIN = new HashSet<String>();
	//private static Set<String> IMPLICIT_WOMEN_ITEMS = new HashSet<String>(Arrays.asList("lipstick", "churidar", "salwar suit", "lehenga choli", "salwar kameez", "ghagra choli", "choli", "palazzo", "dupatta", "frock", "eyeliner", "heel", "stiletto", "saree", "kurti", "mangalsutra", "petticoat", "lehenga", "ghagra", "salwar", "wedge", "necklace", "shapewear", "earring", "shrug", "hosiery", "skirt", "skirts", "scarf", "jumpsuit", "swimsuit", "purse", "gown", "nighty", "bloomers", "skorts", "bikini", "bra", "panty", "corset", "rompers", "leggings", "blouse", "tote bags", "bridesmaid", "dress", "fascinator", "top", "jegging", "lingerie", "tankinis", "tiaras", "petite", "stilletos", "boyfriend jeans", "kaftans", "shrugs", "clutch", "churidar suit", "churidaar suits", "dresses", "anarkali suit", "maxi dress", "maxi dresses"));
	//private static Set<String> IMPLICIT_MEN_ITEMS = new HashSet<String>(Arrays.asList("pagri", "kurta pajama", "nehru jacket", "pathani kurta", "jodhpuri pants", "dhoti", "wallet", "aftershave", "cufflink", "kilt"));
	//private static Set<String> NOT_ALLOWED_LEVENSHTEIN = new HashSet<>(Arrays.asList("pant", "shirt", "pants", "shirts", "shorts", "short"));
	public static Logger logger = XCLogger.getSpLogger();

	/**
	 * The method checks for the gender within the query
	 * @param isGlobal
	 * @param entity
	 * @return matched gender pattern
	 */
	//function to calculate Levenshtein distance
	public static int distance(String a, String b) {
		a = a.toLowerCase();
		b = b.toLowerCase();
		// i == 0
		int [] costs = new int [b.length() + 1];
		for (int j = 0; j < costs.length; j++)
			costs[j] = j;
		for (int i = 1; i <= a.length(); i++) {
			// j == 0; nw = lev(i - 1, j)
			costs[0] = i;
			int nw = i - 1;
			for (int j = 1; j <= b.length(); j++) {
				int cj = Math.min(1 + Math.min(costs[j], costs[j - 1]), a.charAt(i - 1) == b.charAt(j - 1) ? nw : nw + 1);
				nw = costs[j];
				costs[j] = cj;
			}
		}
		return costs[b.length()];
	}

	public static boolean spotGender(String word, XCEntity entity, boolean isGlobal) {
		String[] matchedGender = checkForGender(word);
		if (checkForGender(entity, isGlobal, matchedGender))
			return true;
		return false;
	}

	private static boolean checkForGender(XCEntity entity, boolean isGlobal, String[] matchedGender) {
		if (entity != null) {
			String queryString = entity.getQuery().getQueryString();
			if (matchedGender == null && isGlobal) {
				matchedGender = checkForGender(queryString);
			}
			if (matchedGender != null) {
				String entityStr = entity.getEntityStr();
				entityStr = entityStr.replaceAll("\\b" + matchedGender[0] + "\\b", "").trim();
				entityStr = entityStr.replaceFirst("('s)", "").trim();
				entity.setEntityStr(entityStr);
				entity.setAttribute(SearchUtils.GENDER_KEY, matchedGender[1]);
				Map<String, XCEntity> entityMap = entity.getQuery().getEntityMap();
				entityMap.remove(matchedGender[0]);

				/*2nd gender. eg: men and women shoes*/
				queryString = queryString.replaceAll("\\b" + matchedGender[0] + "\\b", "").trim();
				queryString = queryString.replaceFirst("('s)", "").trim();
				matchedGender = checkForGender(queryString);
				if (matchedGender != null) {
					entityStr = entity.getEntityStr();
					entityStr = entityStr.replaceAll("\\b" + matchedGender[0] + "\\b", "").trim();
					entityStr = entityStr.replaceFirst("('s)", "").trim();
					entity.setEntityStr(entityStr);
					entity.setAttribute(SearchUtils.GENDER_KEY, matchedGender[1]);
					entityMap = entity.getQuery().getEntityMap();
					entityMap.remove(matchedGender[0]);
				}

				return true;
			}
		}
		return false;
	}

	/**
	 * The method checks for the gender within the query
	 * @param isGlobal
	 * @param entity
	 * @return matched gender pattern
	 */
	public static boolean spotGender(IndexedWord indexedWord, XCEntity entity, boolean isGlobal) {
		String[] matchedGender = checkForGender(indexedWord.word());
		if (checkForGender(entity, isGlobal, matchedGender))
			return true;
		return false;
	}

	/**
	 * The method checks for the gender within the query by matching pre-defined patterns
	 * for male and female
	 * @param queryStr entire query string
	 * @return matched gender pattern
	 */
	public static String[] checkForGender(String queryStr) {
		String lowerStr = queryStr.toLowerCase();
		Matcher matcher = FEMALE_PATTERN.matcher(lowerStr);
		String[] matchedGender = null;
		if (matcher.find()) {
			logger.info("Gender found " + matcher.group());
			matchedGender = new String[2];
			matchedGender[0] = matcher.group();
			matchedGender[1] = FEMALE_TAG;
		}
		matcher = MALE_PATTERN.matcher(lowerStr);
		if (matcher.find()) {
			logger.info("Gender found " + matcher.group());
			matchedGender = new String[2];
			matchedGender[0] = matcher.group();
			matchedGender[1] = MALE_TAG;
		}
		matcher = UNISEX_PATTERN.matcher(lowerStr);
		if (matcher.find()) {
			logger.info("Gender found " + matcher.group());
			matchedGender = new String[2];
			matchedGender[0] = matcher.group();
			matchedGender[1] = UNISEX_TAG;
			//			return matchedGender;
		}
		return matchedGender;
	}

	/*Set implicit gender of items like skirt, etc*/
	public static Set<String> getImplicitGender(String entity) {
		Set<String> gender;
		String lemmaEntity = CoreNLPController.lemmatizeToString(entity);
		if (IMPLICIT_WOMEN_ITEMS.contains(entity) || IMPLICIT_WOMEN_ITEMS.contains(lemmaEntity)) {
			gender = new HashSet<String>(Arrays.asList("women", "unisex"));
		} else if (IMPLICIT_MEN_ITEMS.contains(entity) || IMPLICIT_MEN_ITEMS.contains(lemmaEntity)) {
			gender = new HashSet<String>(Arrays.asList("men", "unisex"));
		} else {
			gender = null;
			for (String implicitWomenItem :  IMPLICIT_WOMEN_ITEMS) {
				if (!NOT_ALLOWED_LEVENSHTEIN.contains(lemmaEntity) && distance(lemmaEntity, implicitWomenItem) <= 1) {
					logger.info("Gender found using Levenshtein distance: lemmaEntity - " + lemmaEntity + " - implicitWomenItem - " + implicitWomenItem);
					gender = new HashSet<String>(Arrays.asList("women", "unisex"));
					break;
				}
			}
			if (gender == null) {
				for (String implicitMenItem :  IMPLICIT_MEN_ITEMS) {
					if (!NOT_ALLOWED_LEVENSHTEIN.contains(lemmaEntity) && distance(lemmaEntity, implicitMenItem) <= 1) {
						logger.info("Gender found using Levenshtein distance: lemmaEntity - " + lemmaEntity + " - implicitMenItem - " + implicitMenItem);
						gender = new HashSet<String>(Arrays.asList("men", "unisex"));
						break;
					}
				}
			}
		}

		logger.info("entity : " + entity + ". Lemma entity : " + lemmaEntity + ". Implicit gender : " + gender);
		return gender;
	}
	public static void init() {
		logger = XCLogger.getSpLogger();
		IMPLICIT_MEN_ITEMS.addAll(Neo4jServer.returnImplicitMen());
		IMPLICIT_WOMEN_ITEMS.addAll(Neo4jServer.returnImplicitWomen());
		NOT_ALLOWED_LEVENSHTEIN.addAll(Neo4jServer.notAllowedLevensthein());
		logger.info("Size of IMPLICIT_WOMEN_ITEMS " + IMPLICIT_WOMEN_ITEMS.size());
		logger.info("Size of IMPLICIT_MEN_ITEMS " + IMPLICIT_MEN_ITEMS.size());
		logger.info("Size of NOT_ALLOWED_LEVENSHTEIN " + NOT_ALLOWED_LEVENSHTEIN.size());

	}
}
