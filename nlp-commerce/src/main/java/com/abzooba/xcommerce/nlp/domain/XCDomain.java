/**
 * 
 */
package com.abzooba.xcommerce.nlp.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * @author Koustuv Saha
 * Jun 6, 2016 1:52:44 PM
 * Synaptica  Domain
 */
public class XCDomain {

	int index;
	String domainName;
	String tableName;
	String endPoint;
	String suggesterEndPoint;
	String s3Url;
	String searchReturnAttributes;

	private Map<String, List<String>> categoriesMap;

	public XCDomain(JSONObject domainObj, int index) {

		this.index = index;
		this.categoriesMap = new LinkedHashMap<String, List<String>>();
		try {
			this.domainName = domainObj.getString("Domain Name").toLowerCase().trim();
			this.tableName = domainObj.getString("Table Name").trim();
			this.endPoint = domainObj.getString("Endpoint");
			this.suggesterEndPoint = domainObj.has("Suggester Endpoint") ? domainObj.getString("Suggester Endpoint") : "";
			this.s3Url = domainObj.has("S3 URL") ? domainObj.getString("S3 URL") : "";

			this.searchReturnAttributes = domainObj.getString("Return Attributes");
			JSONObject mapObj = domainObj.getJSONObject("Mappings");

			Iterator<?> granularCategories = mapObj.keys();
			while (granularCategories.hasNext()) {
				String currentCategory = ((String) granularCategories.next());
				JSONArray mappedCatg = mapObj.getJSONArray(currentCategory);
				List<String> mapSet = new ArrayList<String>();
				for (int i = 0; i < mappedCatg.length(); i++) {
					mapSet.add(mappedCatg.getString(i).toLowerCase().trim());
				}
				categoriesMap.put(currentCategory, mapSet);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public List<String> getMappedCategories(String granularCategory) {
		return categoriesMap.get(granularCategory);
	}

	/**
	 * Returns whether the granularCategory belongs to permissible categories in this domain.
	 * @param granularCategory
	 * @return
	 */
	public boolean isCategoryOfDomain(String granularCategory) {
		return categoriesMap.containsKey(granularCategory);
	}

	/**
	 * @return the domainName
	 */
	public String getDomainName() {
		return domainName;
	}

	/**
	 * @return the tableName
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * @return the searchEndPoint
	 */
	public String getEndPoint() {
		return endPoint;
	}

	/**
	 * @return the suggesterEndPoint
	 */
	public String getSuggesterEndPoint() {
		return suggesterEndPoint;
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @return the s3Url
	 */
	public String getS3Url() {
		return s3Url;
	}

	public String getSearchReturnAttributes() {
		return searchReturnAttributes;
	}
}
