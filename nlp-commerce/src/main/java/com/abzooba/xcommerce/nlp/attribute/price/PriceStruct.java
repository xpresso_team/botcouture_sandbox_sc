package com.abzooba.xcommerce.nlp.attribute.price;

/**
 * The class is used to represent a number written in text to 
 * a numerical form
 * @author antarip.biswas
 *
 */

public class PriceStruct {

	private Integer thousand = null;
	private Integer hundred = null;
	private Integer tens = null;
	private String tensStr = null;
	private Integer unit = null;

	public String getTensStr() {
		return tensStr;
	}

	public void setTensStr(String tensStr) {
		this.tensStr = tensStr;
	}

	public Integer getThousand() {
		return thousand;
	}

	public void setThousand(int thousand) {
		this.thousand = thousand;
	}

	public Integer getHundred() {
		return hundred;
	}

	public void setHundred(int hundred) {
		this.hundred = hundred;
	}

	public Integer getTens() {
		return tens;
	}

	public void setTens(int tens) {
		this.tens = tens;
	}

	public void setTensNull() {
		this.tens = null;
		this.tensStr = null;
	}

	public Integer getUnit() {
		return unit;
	}

	public void setUnit(int unit) {
		this.unit = unit;
	}

	public String toNumString() {
		StringBuilder sb = new StringBuilder();
		if (thousand != null)
			sb.append(thousand.intValue());
		else
			sb.append("0");
		if (hundred != null)
			sb.append(hundred.intValue());
		else
			sb.append("0");
		if (tens != null)
			sb.append(tens.intValue());
		else
			sb.append("0");
		if (unit != null)
			sb.append(unit.intValue());
		else
			sb.append("0");
		return sb.toString();
	}

	public int toNumber() {
		int num = 0;
		if (thousand != null)
			num = thousand.intValue() * 1000;
		if (hundred != null)
			num = hundred.intValue() * 100;
		if (tens != null)
			num += tens.intValue() * 10;
		if (unit != null)
			num += unit.intValue();
		return num;
	}

	public boolean isAllFieldsNull() {
		return ((thousand == null) & (hundred == null) & (tens == null) & (unit == null));
	}
}
