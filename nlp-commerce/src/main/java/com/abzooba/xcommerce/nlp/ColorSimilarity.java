package com.abzooba.xcommerce.nlp;

import java.awt.Color;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;

import com.abzooba.xcommerce.core.XCLogger;
import com.abzooba.xcommerce.search.SearchItem;
import com.abzooba.xcommerce.search.SearchUtils;
import com.abzooba.xcommerce.utils.MapUtil;

/**
 * @author Sudhanshu Kumar
 * May 06, 2016
 * @return Returns similar colors
 * 
 */
public class ColorSimilarity {

	private static Logger logger;
	public static Map<String, Color> COLOR_OBJECT_MAP;

	public static void main(String[] args) {
		init();
		String color = "mustard";
		Map<String, Double> similarColorsMap = getSimilarColors(color);
		// To sort in descending order
		boolean desc = true;
		if (similarColorsMap != null) {
			similarColorsMap = MapUtil.sortByValue(similarColorsMap, desc);
		}
		System.out.println(similarColorsMap);
	}

	/**
	 * Initializes the COLOR_OBJECT_MAP 
	 */
	public static void init() {
		XCLogger.init();
		logger = XCLogger.getSpLogger();
		COLOR_OBJECT_MAP = LoadNLPFiles.getColorObjectMap();
	}

	/**
	 * Assigns color similarity score to each item in the result set
	 * @param res - Result set of items retrieved from the search query
	 * @param color - Color asked for in the query
	 */
	public static void assignColorSimilarity(List<SearchItem> res, Set<String> color) {

		Map<String, Double> colorMap = null;
		for (String col : color) {
			colorMap = ColorSimilarity.getSimilarColors(col);
			if (colorMap != null)
				break;
		}
		if (colorMap != null) {
			logger.info("Sorting result on similar color for " + color);
			for (SearchItem item : res) {
				String tempColor = item.getAttributeValue(SearchUtils.COLOR_KEY);
				if (tempColor != null) {
					tempColor = tempColor.replaceAll("[^\\w\\s,]", "");
					tempColor = tempColor.replace("SS", "").trim();
					String[] colorArr = tempColor.split(",");
					double colorSim = 100.0;
					for (String colorFromArr : colorArr) {
						colorFromArr = colorFromArr.trim();
						double tempColorSim = 0.0;
						if (colorMap.containsKey(colorFromArr)) {
							tempColorSim = colorMap.get(colorFromArr);
						}
						if (tempColorSim < colorSim) {
							colorSim = tempColorSim;
						}
					}
					double confidenceFactor = 0;
					if (colorSim < 10) {
						confidenceFactor = 120 - colorSim;
					} else if (colorSim > 50) {
						confidenceFactor = 80 - colorSim;
					} else {
						confidenceFactor = 100 - colorSim;
					}
					item.assignConfidence(Math.max(confidenceFactor, 0));
				}
			}
		}
	}

	/**
	 * Prepares map of color similarity in ascending order. More similar or close color to queried color are given more score
	 * @param colorStr - Color whose similarity has to be calculated
	 * @return - Map of Color with similarity score for the queried color
	 */
	public static Map<String, Double> getSimilarColors(String colorStr) {
		Map<String, Double> colorMap = null;
		colorStr = colorStr.toLowerCase();
		logger.info("Finding Similarity for:\t" + colorStr);

		Color color = COLOR_OBJECT_MAP.get(colorStr);
		logger.info("Using L'UV method for color similarity:\t" + colorStr);
		if (color != null) {
			colorMap = new HashMap<String, Double>();
			for (Map.Entry<String, Color> entry : COLOR_OBJECT_MAP.entrySet()) {
				double score = getLUVScore(entry.getValue(), color);
				colorMap.put(entry.getKey(), score);
			}

		}
		return colorMap;
	}

	private static double getLUVScore(Color color1, Color color2) {
		return getLUVScore(color1.getRed(), color1.getGreen(), color1.getBlue(), color2.getRed(), color2.getGreen(), color2.getBlue());
	}

	/**
	 * 
	 * @param r1 - R value of 1st color
	 * @param g1 - G value of 1st color
	 * @param b1 - B value of 1st color
	 * @param r2 - R value of 1st color
	 * @param g2 - G value of 1st color
	 * @param b2 - B value of 1st color
	 * @return - Similarity score
	 */
	private static double getLUVScore(int r1, int g1, int b1, int r2, int g2, int b2) {
		double meanR = (r1 + r2) / 2;
		int diffR = r1 - r2;
		int diffG = g1 - g2;
		int diffB = b1 - b2;

		double rComp = (2 + meanR / 256) * (diffR ^ 2);
		double gComp = 4 * (diffG ^ 2);
		double bComp = (2 + ((255 - meanR) / 256)) * (diffB ^ 2);
		//		logger.info(rComp + " " + gComp + " " + bComp);
		double score = Math.sqrt(Math.abs(rComp) + Math.abs(gComp) + Math.abs(bComp));

		return score;
	}

	/**
	 * Converts RGB value Lab value
	 * @param r 
	 * @param g
	 * @param b
	 * @return
	 */
	protected static double[] RGBToLab(int r, int g, int b) {
		double varR = r / 255.0;
		double varG = g / 255.0;
		double varB = b / 255.0;

		if (varR > 0.04045)
			varR = Math.pow(((varR + 0.055) / 1.055), 2.4);
		else
			varR = varR / 12.92;
		if (varG > 0.04045)
			varG = Math.pow(((varG + 0.055) / 1.055), 2.4);
		else
			varG = varG / 12.92;
		if (varB > 0.04045)
			varB = Math.pow(((varB + 0.055) / 1.055), 2.4);
		else
			varB = varB / 12.92;

		varR = varR * 100;
		varG = varG * 100;
		varB = varB * 100;

		//Observer. = 2°, Illuminant = D65
		double X = varR * 0.4124 + varG * 0.3576 + varB * 0.1805;
		double Y = varR * 0.2126 + varG * 0.7152 + varB * 0.0722;
		double Z = varR * 0.0193 + varG * 0.1192 + varB * 0.9505;

		double varX = X / 95.047; //ref_X =  95.047   Observer= 2°, Illuminant= D65
		double varY = Y / 100.000; //ref_Y = 100.000
		double varZ = Z / 108.883; //ref_Z = 108.883

		if (varX > 0.008856)
			varX = Math.pow(varX, (1 / 3.0));
		else
			varX = (7.787 * varX) + (16 / 116.0);
		if (varY > 0.008856)
			varY = Math.pow(varY, (1 / 3.0));
		else
			varY = (7.787 * varY) + (16 / 116.0);
		if (varZ > 0.008856)
			varZ = Math.pow(varZ, (1 / 3.0));
		else
			varZ = (7.787 * varZ) + (16 / 116.0);

		double LValue = (116 * varY) - 16;
		double aValue = 500 * (varX - varY);
		double bValue = 200 * (varY - varZ);
		double[] labValue = { LValue, aValue, bValue };
		return labValue;
	}

	/**
	 * Calculates CIE lab score from lab values of two color
	 * @param l1
	 * @param a1
	 * @param b1
	 * @param l2
	 * @param a2
	 * @param b2
	 * @return - CIE score for color similarity
	 */
	protected static double getCIELabScore(double l1, double a1, double b1, double l2, double a2, double b2) {

		double xC1 = Math.sqrt(Math.pow(a1, 2) + Math.pow(b1, 2));
		double xC2 = Math.sqrt(Math.pow(a2, 2) + Math.pow(b2, 2));
		double xDL = l2 - l1;
		double xDC = xC2 - xC1;
		double xDE = Math.sqrt(((l1 - l2) * (l1 - l2)) + ((a1 - a2) * (a1 - a2)) + ((b1 - b2) * (b1 - b2)));

		double xDH = 0;
		if (Math.sqrt(xDE) > (Math.sqrt(Math.abs(xDL)) + Math.sqrt(Math.abs(xDC))))
			xDH = Math.sqrt((xDE * xDE) - (xDL * xDL) - (xDC * xDC));
		else
			xDH = 0;

		double xSC = 1 + (0.045 * xC1);
		double xSH = 1 + (0.015 * xC1);

		xDC /= xSC;
		xDH /= xSH;
		double deltaE94 = Math.sqrt(Math.pow(xDL, 2) + Math.pow(xDC, 2) + Math.pow(xDH, 2));

		return deltaE94;
	}

}
