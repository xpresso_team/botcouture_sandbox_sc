package com.abzooba.xcommerce.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XCLogger {

	private static Logger spLogger;
	private static Logger serverLogger;

	public static void init() {
		spLogger = LoggerFactory.getLogger("synaptica");
		serverLogger = LoggerFactory.getLogger("server");
	}

	public static Logger getSpLogger() {
		return spLogger;
	}

	public static Logger getServerLogger() {
		return serverLogger;
	}

}
