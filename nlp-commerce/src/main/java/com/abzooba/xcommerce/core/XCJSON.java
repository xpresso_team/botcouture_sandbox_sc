/**
 * 
 */
package com.abzooba.xcommerce.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;

import com.abzooba.xcommerce.search.SearchController;
import com.abzooba.xcommerce.search.SearchItem;
import com.abzooba.xcommerce.search.SearchUtils;

import edu.emory.mathcs.backport.java.util.Arrays;

/**
 * @author Sudhanshu Kumar
 * May 19, 2016 2:49:46 PM
 * Updated : 13th Oct 2016, 12:54 PM
 * Synaptica  SpJSON
 */
public class XCJSON {

	private static Logger logger = XCLogger.getSpLogger();

	public static final String INTENT_NODE = "Intent";
	public static final String CLARIFICATION_NODE = "Clarification";
	public static final String QUERY_NODE = "Query";
	public static final String SPELL_CORRECTION_NODE = "Spell Corrected";
	public static final String ORIGINAL_QUERY_NODE = "Original Query";
	public static final String RESULTS_NODE = "Results";
	public static final String RESULTS_COUNT_NODE = "Results Count";
	public static final String DIRECT_RESULTS_NODE = "Direct Results";
	public static final String DIRECT_RESULTS_COUNT_NODE = "Direct Results Count";
	public static final String SUGGESTED_RESULTS_NODE = "Suggested Results";
	public static final String SUGGESTED_RESULTS_COUNT_NODE = "Suggested Results Count";

	/**
	 * @param queryText
	 * @param domainName
	 * @param resultItems
	 * @param pageNo
	 * @param perPage
	 * @param intent
	 * @return 
	 */

	public static JSONObject processResultsToJSON(String originalQuery, String spellCorrectedQuery, String queryText, String domainName, List<SearchItem> resultItems, int pageNo, int perPage, JSONObject intent, JSONObject clarification) {

		if (resultItems != null) {
			logger.info("Structured result size with duplicates : " + resultItems.size());
			SearchController objSearchController = new SearchController();
			resultItems = objSearchController.getUniqueResultSet(resultItems);
			logger.info("Structured result size without duplicates : " + resultItems.size());
		}

		JSONObject netJSONObject = new JSONObject();
		String max_price = "0.0", min_price = "9999.9";
		JSONArray searchResultsJSONArr = new JSONArray();
		JSONArray directResultsJSONArr = new JSONArray();
		JSONArray suggestedResultsJSONArr = new JSONArray();
		int start = (pageNo - 1) * perPage;
		int end = pageNo * perPage;
		int size = 0;

		try {
			int directResultCount = 0;
			int suggestedResultCount = 0;
			if (resultItems != null) {
				size = resultItems.size();
				for (int i = start; i < end; i++) {
					if (i > size - 1) {
						break;
					}
					SearchItem currResult = resultItems.get(i);
					JSONObject mainObj = new JSONObject();
					String sku = currResult.getAttributeValue(SearchUtils.SKU_KEY);
					String price = currResult.getAttributeValue(SearchUtils.PRICE_NUMERIC_KEY);
					boolean isSuggested = Boolean.valueOf(currResult.getAttributeValue(SearchUtils.SUGGESTED_KEY));
					if (sku != null) {
						Map<String, String> attributeMap = currResult.getAttributeMap();
						for (Entry<String, String> entry : attributeMap.entrySet()) {
							mainObj.put(entry.getKey(), entry.getValue());
						}
						if (price != null) {
							if (price.compareTo(min_price) < 0) {
								min_price = price;
							}
							if (price.compareTo(max_price) > 0) {
								max_price = price;
							}
						}

						if (isSuggested) {
							suggestedResultsJSONArr.put(mainObj);
							suggestedResultCount++;
						} else {
							directResultsJSONArr.put(mainObj);
							directResultCount++;
						}
						searchResultsJSONArr.put(mainObj);
					}
				}
			}
			netJSONObject.put(QUERY_NODE, spellCorrectedQuery);
			if (!spellCorrectedQuery.equals(originalQuery)) {
				netJSONObject.put(SPELL_CORRECTION_NODE, true);
				netJSONObject.put(ORIGINAL_QUERY_NODE, originalQuery);
			} else {
				netJSONObject.put(SPELL_CORRECTION_NODE, false);
			}
			if (intent != null) {
				intent.put("pricerange", min_price + "-" + max_price);
				netJSONObject.put(INTENT_NODE, intent);
			}
			if (clarification != null) {
				netJSONObject.put(CLARIFICATION_NODE, clarification);
			}

			/* To get diverse result set */
			JSONArray maxEntResult = maximumEntropyResult(directResultsJSONArr);
			if (maxEntResult != null && maxEntResult.length() == directResultsJSONArr.length()) {
				netJSONObject.put(RESULTS_NODE, maxEntResult);
			} else {
				netJSONObject.put(RESULTS_NODE, directResultsJSONArr);
			}
			maxEntResult = maximumEntropyResult(suggestedResultsJSONArr);
			if (maxEntResult != null && maxEntResult.length() == suggestedResultsJSONArr.length()) {
				netJSONObject.put(SUGGESTED_RESULTS_NODE, maxEntResult);
			} else {
				netJSONObject.put(SUGGESTED_RESULTS_NODE, suggestedResultsJSONArr);
			}

			netJSONObject.put(DIRECT_RESULTS_COUNT_NODE, directResultCount);
			netJSONObject.put(SUGGESTED_RESULTS_COUNT_NODE, suggestedResultCount);
			netJSONObject.put(RESULTS_COUNT_NODE, size);

			logger.info(DIRECT_RESULTS_COUNT_NODE + " " + directResultCount);
			logger.info(SUGGESTED_RESULTS_COUNT_NODE + " " + suggestedResultCount);
			logger.info(RESULTS_COUNT_NODE + " " + size);

			return netJSONObject;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	public JSONObject searchItemToJSON(List<SearchItem> resultItems, boolean itemDetails) {

		JSONObject netJSONObject = new JSONObject();
		if (resultItems != null) {
			int size = resultItems.size();
			logger.info("Result searchItem size  " + size);

			try {
				if (itemDetails == true) {
					JSONArray itemsWithDetails = new JSONArray();
					for (int i = 0; i < size; i++) {
						SearchItem currResult = resultItems.get(i);
						Map<String, String> attributeMap = currResult.getAttributeMap();
						JSONObject singleItemObject = new JSONObject(attributeMap);
						itemsWithDetails.put(singleItemObject);
					}
					netJSONObject.put("Results", itemsWithDetails);
				} else {
					Map<String, Set<String>> multiReturnFields = new HashMap<String, Set<String>>();

					for (int i = 0; i < size; i++) {
						SearchItem currResult = resultItems.get(i);
						Map<String, String> attributeMap = currResult.getAttributeMap();

						for (Entry<String, String> entry : attributeMap.entrySet()) {
							String[] values = {};
							if (entry.getValue().startsWith("[") && entry.getValue().endsWith("]")) {
								values = entry.getValue().substring(1, entry.getValue().length() - 1).split(",");
							} else {
								values = entry.getValue().split(",");
							}
							@SuppressWarnings("unchecked")
							Set<String> keyValues = new HashSet<String>(Arrays.asList(values));

							if (multiReturnFields.get(entry.getKey()) == null || multiReturnFields.get(entry.getKey()).isEmpty()) {
								multiReturnFields.put(entry.getKey(), keyValues);
							} else {
								Set<String> tempSet = multiReturnFields.get(entry.getKey());
								tempSet.addAll(keyValues);
								multiReturnFields.put(entry.getKey(), tempSet);
							}
						}
					}

					for (Entry<String, Set<String>> entry : multiReturnFields.entrySet()) {
						netJSONObject.put(entry.getKey(), entry.getValue());
						logger.info("Unique " + entry.getKey() + " count\t" + entry.getValue().size());
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return netJSONObject;
	}

	/* To get diverse set of results */
	private static JSONArray maximumEntropyResult(JSONArray result) {
		JSONArray maxEntropyResult = new JSONArray();
		int resultSize = result.length();

		JSONArray[] brackets = new JSONArray[10];
		for (int count = 0; count < 10; count++) {
			brackets[count] = new JSONArray();
		}

		try {
			for (int i = 0; i < resultSize; i++) {
				JSONObject obj = result.getJSONObject(i);
				int index = i % 10;
				brackets[index].put(obj);
			}
			for (JSONArray jArr : brackets) {
				for (int j = 0; j < jArr.length(); j++) {
					maxEntropyResult.put(jArr.getJSONObject(j));
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return maxEntropyResult;
	}
}
