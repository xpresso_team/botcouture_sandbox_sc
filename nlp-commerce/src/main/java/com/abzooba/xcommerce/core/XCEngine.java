/**
 * 
 */
package com.abzooba.xcommerce.core;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.datadriven.ExtractCustomEntities;
import com.abzooba.xcommerce.datadriven.ResponseAPI;
import com.abzooba.xcommerce.domain.AttributeUtil;
import com.abzooba.xcommerce.domain.xcfashion.LoadXCHierarchy;
import com.abzooba.xcommerce.nlp.ColorSimilarity;
import com.abzooba.xcommerce.nlp.LoadNLPFiles;
import com.abzooba.xcommerce.nlp.SpellChecker;
import com.abzooba.xcommerce.nlp.attribute.*;
import com.abzooba.xcommerce.nlp.attribute.price.PriceParser;
import com.abzooba.xcommerce.nlp.domain.DomainCategoryMapper;
import com.abzooba.xcommerce.nlp.domain.DomainKnowledgeBase;
import com.abzooba.xcommerce.ontology.server.Neo4jServer;
import com.abzooba.xcommerce.search.SearchAttributes;
import com.abzooba.xcommerce.search.SearchItem;
import com.abzooba.xcommerce.search.SearchUtils;
import com.abzooba.xcommerce.utils.DetailsParser;
import com.abzooba.xcommerce.utils.StringAttributeUtils;
import com.abzooba.xcommerce.utils.WordNetUtils;
import com.abzooba.xpresso.engine.core.XpEngine;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Sudhanshu Kumar
 * Apr 1, 2016 12:13:48 PM
 * XpressoCommerce XCEngine
 */
public class XCEngine {
	private static Logger logger;

	/**
	 * Initializes all static fields.
	 * Optional argument to input configuration files directory.
	 * @param values
	 * @see com.abzooba.xcommerce.config.XCConfig#init(String...)
	 */
	public static void init(String... values) {

		double time0 = System.currentTimeMillis();
		XCLogger.init();
		logger = XCLogger.getSpLogger();
		double time1 = System.currentTimeMillis();
		logger.info("Logger initialized:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		IntentAPI.init();
		time1 = System.currentTimeMillis();
		logger.info("Intent API Initialized:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		XCConfig.init(values);
		time1 = System.currentTimeMillis();
		logger.info("Synaptica Config Initialized:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		XpEngine.init(true, XCConfig.XPRESSO_CONFIGURATION_FILE);
		time1 = System.currentTimeMillis();
		logger.info("Xpresso Loaded:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		LoadNLPFiles.init();
		time1 = System.currentTimeMillis();
		logger.info("Attributes files loaded:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		DomainCategoryMapper.init();
		time1 = System.currentTimeMillis();
		logger.info("Domain category Mapper loaded:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		LoadXCHierarchy.init();
		time1 = System.currentTimeMillis();
		logger.info("XCHierarchy loaded:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		XCQuery.init();
		time1 = System.currentTimeMillis();
		logger.info("Query init done:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		Neo4jServer.init();
		time1 = System.currentTimeMillis();
		logger.info("Neo4jServer init done:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		SearchAttributes.init();
		time1 = System.currentTimeMillis();
		logger.info("SearchAttributes Loaded!:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		AttributeUtil.init();
		time1 = System.currentTimeMillis();
		logger.info("GlobalAttributeSearch Loaded!:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		ColorSimilarity.init();
		time1 = System.currentTimeMillis();
		logger.info("ColorSimilarity init done:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		ThematicSearch.init();
		time1 = System.currentTimeMillis();
		logger.info("ThematicSearch init done:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		MaterialSearch.init();
		time1 = System.currentTimeMillis();
		logger.info("MaterialSearch initialized:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		SearchByProductName.init();
		time1 = System.currentTimeMillis();
		logger.info("ProductNameSearch initialized:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		GenderSearch.init();
		time1 = System.currentTimeMillis();
		logger.info("GenderSearch initialized:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		PriceParser.init();
		time1 = System.currentTimeMillis();
		logger.info("PriceSearch init done:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		SizeParser.init();
		time1 = System.currentTimeMillis();
		logger.info("SizeSearch init done:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		DomainKnowledgeBase.init();
		time1 = System.currentTimeMillis();
		logger.info("DomainKnowledgeBase initialized:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		SpellChecker.init();
		time1 = System.currentTimeMillis();
		logger.info("SpellChecker init done:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		SearchUtils.init(DomainCategoryMapper.domainObjectsArr);
		time1 = System.currentTimeMillis();
		logger.info("SearchUtils loaded:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		StringAttributeUtils.init();
		time1 = System.currentTimeMillis();
		logger.info("String Attribute Utils loaded:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		WordNetUtils.init();
		time1 = System.currentTimeMillis();
		logger.info("WordNet loaded:\t" + (time1 - time0) / 1000 + " seconds");

		time0 = System.currentTimeMillis();
		DetailsParser.init();
		time1 = System.currentTimeMillis();
		logger.info("Generic StopWords loaded:\t" + (time1 - time0) / 1000 + " seconds");
	}

	/**
	 * Using a map of query parameters, performs a query and returns JSON of results as a string.
	 * @param queryParameters
	 * @return JSON results as a string
	 */
	public static String getSearchResultsJSON(Map<String, String[]> queryParameters, IntentAPI... intentObj) {
		logger.info("In XCEngine -> getSearchResultsJSON");
		logger.info("Query parameters size: " + queryParameters.size());
		String[] clientNameArr = queryParameters.get("src");
		String domainName = XCConfig.DEFAULT_DATA_DOMAIN;
		String clientName = "all";
		if (clientNameArr != null) {
			clientName = clientNameArr[0];
		}

		String searchQuery = queryParameters.get("query")[0].toLowerCase();
		String originalSearchQuery = searchQuery;
		logger.info("Search Query: " + searchQuery);
		// TODO: preserving brands in spell checker
		// TODO: generalizing SpellChecker to other domains/customers
		String spellCorrectedQuery;
		//		String spellCorrectedQuery = searchQuery;
		//		logger.info("XC spell correction : " + spellCorrectedQuery);
		//		if (!spellCorrectedQuery.equalsIgnoreCase(searchQuery)) {

		//TODO: implement timeout when api is taking too long
		String bingSpellCheckQuery = new SpellChecker().MSSpellCheck(searchQuery);

		logger.info("Bing Spell correction : " + bingSpellCheckQuery);
		if (bingSpellCheckQuery != null) {
			spellCorrectedQuery = bingSpellCheckQuery;
			searchQuery = bingSpellCheckQuery;
		} else {
			spellCorrectedQuery = new SpellChecker().checkMySpelling(searchQuery);
			searchQuery = spellCorrectedQuery;
		}

		//in size s
		if (originalSearchQuery.contains("size s") && spellCorrectedQuery.contains("sizes")) {
			spellCorrectedQuery = spellCorrectedQuery.replaceAll("\\bsizes\\b", "size s");
			searchQuery = spellCorrectedQuery;
		}

		//t shirt
		if (spellCorrectedQuery.contains("t shirt") || spellCorrectedQuery.contains("t shirts")) {
			spellCorrectedQuery = spellCorrectedQuery.replaceAll("\\bt shirt\\b", "t-shirt").replaceAll("\\bt shirts\\b", "t-shirts");
			searchQuery = spellCorrectedQuery;
		}

		searchQuery = normalizePriceString(searchQuery);
		logger.info("Query after normalizing price : " + searchQuery);
		
		if (XCConfig.DATA_DRIVEN_ACTIVE) {
			boolean beautyProductQuery = queryTypeResolver(searchQuery);
			if (beautyProductQuery) {
				try {
					logger.info("BEAUTY PRODUCT query idenified");
					ExtractCustomEntities extractCEObj = new ExtractCustomEntities(searchQuery);
					JSONObject beautyIntent = null;
					if (intentObj != null) {
						if (intentObj[0].intentJsonSet == null) {
							intentObj[0].intentJsonSet = new JSONArray();
						}
						beautyIntent = extractCEObj.getEntityJson();
					}
					if (beautyIntent != null) {
						logger.info(beautyIntent.toString());
						String beautyEntity = beautyIntent.get("entity").toString();
						if (beautyEntity != null && !beautyEntity.equalsIgnoreCase("anything") && !beautyEntity.isEmpty()) {
							if (beautyIntent.getJSONArray("gender").length() == 0) {
								beautyIntent.remove("gender");
								beautyIntent.put("gender", new JSONArray(GenderSearch.getImplicitGender(beautyEntity)));
							}
							//proceeding to rule based if no category found using data driven or if any category other than beauty products is found
							try {
								if (!beautyIntent.getJSONArray("xc_category").getString(0).equalsIgnoreCase("beauty products")) {
									logger.info("non beauty products category or no category found using RASA");
								} else {
									if (XCEntity.isRejectEntity(beautyEntity, "xcfashion")) {
										logger.info("entity is rejected by isRejectEntity");
									}
									else {
										intentObj[0].intentJsonSet.put(beautyIntent);
										return intentObj[0].intentJsonSet.toString();
									}
								}
							}
							catch (Exception ex) {
								logger.info("null category found using RASA");
							}

						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.info("BEAUTY PRODUCT query: NO ENTITY found using rasa. Trying with rule based approach");
			}
		}

		//				}
		//		String spellCorrectedQuery = bingSpellCheckQuery;
		logger.info("Query after spell checking : " + searchQuery);

		searchQuery = replaceAllCategoriesKeyWords(searchQuery);
		logger.info("Query after normalizing all category key word : " + searchQuery);



		// TODO: evaluate performance difference with and without lemmatization

		XCQuery q = null;
		String pageNo = null;
		String perPage = null;
		boolean isChatbotQuery = false;
		String customReturnAttributes = null;

		if (queryParameters.containsKey("page")) {
			pageNo = queryParameters.get("page")[0];
		}
		if (queryParameters.containsKey("perPage")) {
			perPage = queryParameters.get("perPage")[0];
		}
		if (queryParameters.containsKey("isChatbotQuery")) {
			isChatbotQuery = Boolean.parseBoolean(queryParameters.get("isChatbotQuery")[0]);
		}
		if (queryParameters.containsKey("customReturnAttributes")) {
			customReturnAttributes = queryParameters.get("customReturnAttributes")[0];
		}

		try {
			if (intentObj.length > 0) {
				q = new XCQuery(queryParameters.get("query")[0], spellCorrectedQuery, searchQuery, clientName, domainName, isChatbotQuery, customReturnAttributes, intentObj);
			} else if (pageNo == null || perPage == null) {
				q = new XCQuery(queryParameters.get("query")[0], spellCorrectedQuery, searchQuery, clientName, domainName, isChatbotQuery, customReturnAttributes);
			} else {
				q = new XCQuery(queryParameters.get("query")[0], spellCorrectedQuery, searchQuery, clientName, domainName, isChatbotQuery, customReturnAttributes, pageNo, perPage);
			}
		} catch (Exception e) {
			// TODO : Print stacktrace as string using Throwable.printStackTrace(PrintWriter pw) 
			logger.error("Error occured while creating SpQuery\n" + e.getMessage() + "\n" + e.getStackTrace());
			e.printStackTrace();
		}
		if (q != null) {
			return q.getSearchJSON();
		} else {
			JSONObject emptyJSON = new JSONObject();
			List<SearchItem> emptyList = new ArrayList<SearchItem>();
			return XCJSON.processResultsToJSON(queryParameters.get("query")[0], spellCorrectedQuery, searchQuery, domainName, emptyList, 1, XCConfig.MAX_SEARCH_HITS, emptyJSON, emptyJSON).toString();
		}
	}

	private static boolean queryTypeResolver(String searchQuery) {

		Map<String, String> params = new HashMap<>();
		params.put("q", searchQuery);
		params.put("token", XCConfig.RASA_TOKEN);
		ResponseAPI apiCall = new ResponseAPI(params, XCConfig.RASA_SERVER);
		JSONObject response = apiCall.getResponse();
		try {
			logger.info(response.get("intent").toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		JSONObject jObject;
		try {
			jObject = new JSONObject(response.toString());
			JSONObject intent = jObject.getJSONObject("intent");
			String category = intent.getString("name");
			if (category.equalsIgnoreCase("beauty product"))
				return true;
			else
				return false;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	private static String replaceAllCategoriesKeyWords(String query) {
		return query.replaceAll("something", SearchUtils.ALL_CATEGORIES).replaceAll("everything", SearchUtils.ALL_CATEGORIES);
	}

	private static String normalizePriceString(String query) {
		/* Normalizes 20k into 20 k
		 * Necessary to ensure proper working of PriceParser as dependency parsing differs for the two expressions.
		 *  */
		/*
		Replacing 5k with 5000
		 */
		Pattern pattern = Pattern.compile("( )(\\d+)([k|K])");
		Matcher match = pattern.matcher(query);
		if (match.find()) {
			return match.replaceAll("$1$2000");
		}
		pattern = Pattern.compile("(\\d+)([k|K])");
		match = pattern.matcher(query);
		if (match.find())
			return match.replaceAll("$1 $2");
		return query;
	}

	private static String getDynamoDBName(String domain) {
		if (domain != null && domain.length() > 0)
			return "com.abzooba.xcommerce.domain." + domain.toLowerCase() + ".AmazonDynamoDB" + domain.substring(0, 3).toUpperCase() + domain.substring(3).toLowerCase();
		return "";
	}
}