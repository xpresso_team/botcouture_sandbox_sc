/**
 * @author: Alix Melchy
 * Mar 30, 2016
 */
package com.abzooba.xcommerce.ontology;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.slf4j.Logger;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCLogger;

public class OntologyGraphBuilder {
	private static final String DB_PATH = XCConfig.NEO4J_DB_PATH;
	private static GraphDatabaseService graphDB;
	public static Logger logger = XCLogger.getSpLogger();

	public static void init() {
		graphDB = new GraphDatabaseFactory().newEmbeddedDatabase(new File(DB_PATH));
		registerShutdownHook(graphDB);
		logger.info("graphDB created");
	}

	private static void registerShutdownHook(final GraphDatabaseService graphDb) {
		// Registers a shutdown hook for the Neo4j instance so that it
		// shuts down nicely when the VM exits (even if you "Ctrl-C" the
		// running application).
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				graphDb.shutdown();
			}
		});
	}

	public static void addNodeWithLabel(String nodeName, String labelStr) {
		ResourceIterator<Node> resultIterator = null;
		Node result = null;
		try (Transaction tx = graphDB.beginTx()) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("name", labelStr);
			parameters.put("nodeName", nodeName);
			String parentNodeQuery = "MATCH (parent:" + labelStr + " {name : {name}})";
			String queryStr = parentNodeQuery + " MERGE (n:" + labelStr + " {name : {nodeName}}) -[:isA]-> parent RETURN n";
			resultIterator = graphDB.execute(queryStr, parameters).columnAs("n");
			while (resultIterator.hasNext()) {
				logger.info("Created node:");
				result = resultIterator.next();
				logger.info(result.getAllProperties().toString());
			}
			tx.success();
		}
	}

	public static void main(String[] args) {
		XCConfig.init();
		init();
	}

}
