package com.abzooba.xcommerce.ontology.server;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;

import com.abzooba.xcommerce.core.XCLogger;

import edu.emory.mathcs.backport.java.util.Arrays;

/**
 * @author Alix Melchy Jul 5, 2016
 *
 */
public class ConceptRelation {
	protected static Logger logger;

	public enum Relations {
		HYPONYM, HYPERNYM, SYNONYM, ATTRIBUTE, RELATIVE
	};

	private static Set<String> hyponyms = null;
	private static Set<String> hypernyms = null;
	private static Set<String> synonyms = null;
	private static Set<String> attributes = null;
	private static Set<String> relatives = null;

	private String start;
	private String end;
	private Relations relation;

	@SuppressWarnings("unchecked")
	public static void init() {
		logger = XCLogger.getSpLogger();
		String[] hyponym_values = { "DerivedFrom", "InstanceOf", "IsA", "MemberOf", "PartOf", "SubclassOf" };
		hyponyms = new HashSet<String>(Arrays.asList(hyponym_values));
		String[] hypernym_values = { "HasA", "HasProperty", "MadeOf" };
		hypernyms = new HashSet<String>(Arrays.asList(hypernym_values));
		String[] synonym_values = { "Synonym" };
		synonyms = new HashSet<String>(Arrays.asList(synonym_values));
		attributes = new HashSet<String>();
		attributes.add("Attribute");
		String[] relative_values = { "RelatedTo", "CapableOf", "CreatedBy", "UsedFor", "Causes" };
		relatives = new HashSet<String>(Arrays.asList(relative_values));
		logger.info("ConceptRelation init done");
	}

	public ConceptRelation(String start, String end, Relations relation) {
		this.start = start;
		this.end = end;
		this.relation = relation;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.start);
		sb.append(" -[");
		sb.append(this.relation.toString());
		sb.append("]-> ");
		sb.append(this.end);
		return sb.toString();
	}

	public static Relations toRelation(String relation) {
		Relations answ = null;
		if (hyponyms.contains(relation))
			answ = Relations.HYPONYM;
		if (hypernyms.contains(relation))
			answ = Relations.HYPERNYM;
		if (synonyms.contains(relation))
			answ = Relations.SYNONYM;
		if (attributes.contains(relation))
			answ = Relations.ATTRIBUTE;
		if (relatives.contains(relation))
			answ = Relations.RELATIVE;
		return answ;
	}

	public String getStart() {
		return this.start;
	}

	public String getEnd() {
		return this.end;
	}

	public ConceptRelation.Relations getRelation() {
		return this.relation;
	}

	public String getOtherEnd(ConceptObject concept) {
		if (concept.getConceptName().equalsIgnoreCase(this.start))
			return this.end;
		if (concept.getConceptName().equals(this.end))
			return this.start;
		return null;
	}

	public String getOtherEnd(String conceptName) {
		if (conceptName.equalsIgnoreCase(this.start))
			return this.end;
		if (conceptName.equalsIgnoreCase(this.end))
			return this.start;
		return null;
	}

}
