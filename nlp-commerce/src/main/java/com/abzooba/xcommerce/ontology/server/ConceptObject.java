package com.abzooba.xcommerce.ontology.server;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.neo4j.driver.internal.value.RelationshipValue;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
import org.slf4j.Logger;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCLogger;

/**
 * @author Alix Melchy Jul 5, 2016
 *
 */
public class ConceptObject {
	protected static Logger logger;
	private String conceptName;
	private Map<ConceptRelation.Relations, Set<ConceptRelation>> semanticNetwork;
	private Set<String> similarTerms;

	private enum Database {
		NEO4J, REST_API
	};

	private static Database db;
	private static String apiServer;
	private static String neo4jServer;

	public static void init() {
		logger = XCLogger.getSpLogger();
		db = toDatabase(XCConfig.CONCEPTNET_DATABASE);
		apiServer = XCConfig.CONCEPTNET_API;
		neo4jServer = XCConfig.CONCEPTNET_NEO4J;
		logger.info("ConceptNet with database " + db.toString() + "\tinit done");
		logger.info("REST API: " + apiServer);
		logger.info("Neo4j server: " + neo4jServer);
	}

	private static Database toDatabase(String db) {
		logger.info("Creating db with " + db);
		if (db.equalsIgnoreCase("neo4j"))
			return Database.NEO4J;
		return Database.REST_API;
	}

	private ConceptObject(String query_term) {
		conceptName = query_term;
		semanticNetwork = new HashMap<ConceptRelation.Relations, Set<ConceptRelation>>();
		similarTerms = new HashSet<String>();
	}

	private static String URIStandardize(String query) {
		Document doc = null;
		String uri = null;
		try {
			logger.info("URI url: " + apiServer + "uri?language=en&text=" + query);
			doc = Jsoup.connect(apiServer + "uri?language=en&text=" + query).timeout(30000).ignoreContentType(true).get();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (doc != null) {
			String text1 = doc.body().text();
			JSONObject text1json = new JSONObject(text1);
			if (text1json.has("uri")) {
				uri = text1json.getString("uri");
				uri = uri.replaceAll("\"", "").split("/")[3];
			}
		}
		return uri;
	}

	//Since both start and end of the json are in the format : "/c/en/term" so to get the result, this string has to be parsed
	private static String getEndPoint(String str) {
		String language = str.split("/")[2];
		if (language.equals("en")) {
			return str.split("/")[3].replaceAll("_", " "); //since all the spaces are replaced by underscore during standardization, we remove them so that they revert back to their original spelling
		} else {
			return null;
		}
	}

	private static String similarTermExtraction(String term) {
		term = term.split(",")[0].replaceAll("\\[", "").replaceAll("\"", "");
		String[] splitResult = term.split("/");
		if (splitResult[2].equals("en")) {
			return splitResult[3];
		} else {
			return null;
		}

	}

	private static ConceptRelation.Relations relationExtraction(String relation) {
		String[] relationSplitResult = relation.replaceAll("\"", "").split("/");

		String finalRelation = null;
		if (relationSplitResult.length == 3) {
			finalRelation = relationSplitResult[2];
		} else if (relationSplitResult.length == 4) {
			finalRelation = relationSplitResult[2] + "/" + relationSplitResult[3];
		}

		return ConceptRelation.toRelation(finalRelation);

	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.conceptName);
		sb.append("\nSemantic network");
		sb.append(this.semanticNetwork.toString());
		sb.append("\nSimilar terms: ");
		sb.append(this.similarTerms.toString());

		return sb.toString();
	}

	public String getConceptName() {
		return this.conceptName;
	}

	private Set<ConceptObject> getConceptWithRelation(ConceptRelation.Relations relation) {
		Set<ConceptObject> answ = new HashSet<ConceptObject>();
		for (ConceptRelation triple : this.semanticNetwork.get(relation)) {
			answ.add(build(triple.getOtherEnd(this)));
		}
		return answ;
	}

	private Set<String> getConceptNamesWithRelation(ConceptRelation.Relations relation) {
		Set<String> answ = new HashSet<String>();
		for (ConceptRelation triple : this.semanticNetwork.get(relation)) {
			answ.add(triple.getOtherEnd(this.conceptName));
		}
		return answ;
	}

	public Set<ConceptObject> getHyponyms() {
		return getConceptWithRelation(ConceptRelation.Relations.HYPONYM);
	}

	public Set<ConceptObject> getHypernyms() {
		return getConceptWithRelation(ConceptRelation.Relations.HYPERNYM);
	}

	public Set<ConceptObject> getSynonyms() {
		return getConceptWithRelation(ConceptRelation.Relations.SYNONYM);
	}

	public Set<ConceptObject> getAttributes() {
		return getConceptWithRelation(ConceptRelation.Relations.ATTRIBUTE);
	}

	public Set<ConceptObject> getRelatives() {
		return getConceptWithRelation(ConceptRelation.Relations.RELATIVE);
	}

	public Set<String> getHyponymTerms() {
		return getConceptNamesWithRelation(ConceptRelation.Relations.HYPONYM);
	}

	public Set<String> getHypernymTerms() {
		return getConceptNamesWithRelation(ConceptRelation.Relations.HYPERNYM);
	}

	public Set<String> getSynonymTerms() {
		return getConceptNamesWithRelation(ConceptRelation.Relations.SYNONYM);
	}

	public Set<String> getAttributeTerms() {
		return getConceptNamesWithRelation(ConceptRelation.Relations.ATTRIBUTE);
	}

	public Set<String> getRelativeTerms() {
		return getConceptNamesWithRelation(ConceptRelation.Relations.RELATIVE);
	}

	public static ConceptObject build(String query_term) {
		logger.info("Building concept of " + query_term);
		//		query_term = URIStandardize(query_term);
		ConceptObject concept = null;
		switch (ConceptObject.db) {
			case NEO4J:
				try {
					logger.info("Neo4j construction");
					concept = Neo4jBuild(query_term);
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case REST_API:
				try {
					logger.info("Rest API construction");
					concept = RestAPIBuild(query_term);
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			default:
				logger.info("Please check ConceptNet configuration: no proper database");
		}
		return concept;
	}

	private static ConceptObject Neo4jBuild(String query_term) throws IOException {

		ConceptObject concept = new ConceptObject(query_term);

		Driver driver = GraphDatabase.driver(neo4jServer, AuthTokens.basic("neo4j", "abzooba"));
		Session session = driver.session();
		String query = "MATCH(p:en)-[r]-(p1) WHERE p1.name = \"" + query_term + "\" RETURN p.name,r"; //returns all incoming edges (relationship) and also the adjacent nodes 
		StatementResult session_result = session.run(query);

		ConceptRelation.Relations relation;
		String start = null, end = null;
		while (session_result.hasNext()) {
			Record r = session_result.next();
			start = r.get("p.name").toString().replaceAll("_", " ").replaceAll("\"", "");
			end = query_term.replaceAll("_", " ");
			logger.info("start: " + start + "\tend: " + end);
			RelationshipValue node = (RelationshipValue) r.get("r");

			relation = relationExtraction(node.get("rel").toString());
			logger.info("relation: " + relation);

			if (start != null && end != null) {
				if (!concept.semanticNetwork.containsKey(relation)) {
					concept.semanticNetwork.put(relation, new HashSet<ConceptRelation>());
					concept.semanticNetwork.get(relation).add(new ConceptRelation(start, end, relation));
				} else {
					concept.semanticNetwork.get(relation).add(new ConceptRelation(start, end, relation));
				}
			}

		}
		logger.info("Semantic Network of " + query_term + " has " + concept.semanticNetwork.size() + " relations");

		session.close();
		driver.close();

		return concept;
	}

	private static ConceptObject RestAPIBuild(String query_term) throws IOException {
		ConceptObject concept = new ConceptObject(query_term);

		// start and end give a sense of the direction of the relationship between the query term and the result

		ConceptRelation.Relations relation = null;
		String start = null, end = null;

		Document doc = Jsoup.connect(apiServer + "c/en/" + query_term + "?limit=300&language=en").timeout(30000).ignoreContentType(true).get();

		String text1 = doc.body().text();
		int results_found = new JSONObject(text1).getInt("numFound");
		logger.info(results_found + " results found for " + query_term);
		JSONArray resultarray = new JSONObject(text1).getJSONArray("edges");

		int j;
		for (j = 0; j < results_found; j++) {

			start = getEndPoint(resultarray.getJSONObject(j).get("start").toString());
			end = getEndPoint(resultarray.getJSONObject(j).get("end").toString());

			relation = relationExtraction(resultarray.getJSONObject(j).get("rel").toString());

			if (relation != null && start != null && end != null) {
				if (!concept.semanticNetwork.containsKey(relation)) {
					concept.semanticNetwork.put(relation, new HashSet<ConceptRelation>());
					concept.semanticNetwork.get(relation).add(new ConceptRelation(start, end, relation));
				} else {
					concept.semanticNetwork.get(relation).add(new ConceptRelation(start, end, relation));
				}
			}

		}
		logger.info("Semantic Network of " + query_term + " has " + concept.semanticNetwork.size() + " relations");
		logger.info("\n====================================================================");
		return concept;
	}

	public void findSimilarTerms(String... strings) {
		StringBuilder sb = new StringBuilder();
		sb.append(URIStandardize(this.conceptName));
		sb.append(",");

		for (String s : strings) {
			s = URIStandardize(s);
		}
		sb.append(String.join(",", strings));
		String commaSeparatedTerms = sb.toString();

		Document doc = null;
		try {
			doc = Jsoup.connect(apiServer + "assoc/list/en/" + commaSeparatedTerms + "?limit=10&filter=/c/en").timeout(30000).ignoreContentType(true).get();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (doc != null) {
			String text1 = doc.body().text();

			JSONObject results = new JSONObject(text1);
			JSONArray resultarray = results.getJSONArray("similar");
			String similarterm = null;
			int numberOfResults = resultarray.length();
			logger.info("Found " + numberOfResults + " associated concepts");
			for (int j = 0; j < numberOfResults; j++) {
				similarterm = similarTermExtraction(resultarray.get(j).toString());
				if (similarterm != null)
					this.similarTerms.add(similarterm.replaceAll("_", " "));
			}
			logger.info("\n====================================================================");
		}
	}

	public static void main(String[] args) {
		XCLogger.init();
		XCConfig.init();
		ConceptRelation.init();
		init();

		logger.info("Test with Wikipedia homepage");
		try {
			Document doc = Jsoup.connect("http://en.wikipedia.org/").get();
			Elements newsHeadlines = doc.select("#mp-itn b a");
			for (Element headline : newsHeadlines) {
				if (headline.hasText())
					logger.info(headline.text());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		Scanner s = new Scanner(System.in);
		String query = null;
		String query_term = null;
		String[] association_terms = null;

		while (true) {
			logger.info("\n====================================================================");
			logger.info("Enter a query term (with comma-separated association terms after a colon)");
			logger.info("\n====================================================================");
			query = s.nextLine();
			logger.info("Query: " + query);

			if (query != null) {
				String[] temp = query.split(":");
				query_term = temp[0];
				logger.info("query_term: " + query_term);
				if (temp.length > 1) {
					association_terms = temp[1].split(",");
					for (String item : association_terms)
						logger.info("association term: " + item);
				}

				if (query_term != null) {
					ConceptObject concept = build(query_term);
					logger.info("Created concept: " + concept);
					if (association_terms != null)
						concept.findSimilarTerms(association_terms);
					logger.info("concept: " + concept.toString());
				}
			} else {
				s.close();
				break;
			}
		}
	}
}
