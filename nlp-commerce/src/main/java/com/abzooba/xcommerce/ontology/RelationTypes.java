/**
 * @author: Alix Melchy
 * Apr 11, 2016
 */
package com.abzooba.xcommerce.ontology;

import org.neo4j.graphdb.RelationshipType;

public enum RelationTypes implements RelationshipType{
	/* These are the relations extracted from Servive ontology */
	hasBodyType,
	hasEyeBrowsColour,
	hasEyeColour,
	hasFit,
	hasGarmentButtons,
	hasGarmentColour,
	hasHairColour,
	hasOccasion,
	hasSkinColour,
	hasSleeves,
	hasStyle,
	hasUpperBodyType,
	isA,
	manufacturedBy,
	similarTo,
	/* The sameAs relation is added to link synonyms/equivalent concepts*/
	sameAs,
	/* The goesWellWith relation is added to embed fashion recommendations */
	goesWellWith
}
