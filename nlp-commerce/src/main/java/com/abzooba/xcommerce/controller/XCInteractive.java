package com.abzooba.xcommerce.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.slf4j.Logger;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCEngine;
import com.abzooba.xcommerce.core.XCLogger;

/**
 * Entry point for interactive use of Synaptica engine
 *
 */
public class XCInteractive {
	public static Logger logger;
	/*Makes to display 20 results*/
	private static boolean IS_PAGINATED = true;
	public static Scanner s = new Scanner(System.in);

	public static void main(String[] args) {
		init();
		String query, source, line;
		query = source = line = null;
		String domainName = XCConfig.DEFAULT_DATA_DOMAIN;
		Map<String, String[]> queryParameters = new HashMap<String, String[]>();

		while (true) {
			logger.info("\n====================================================================");
			logger.info("Enter the query in the format : <client>##<querystr>##<page>##<perPage>");
			logger.info("Values for clients can be macys, forever21, nordstrom and express");
			logger.info("\n====================================================================");
			line = s.nextLine();
			if (line != null) {
				logger.info(line);
				String[] lineArr = line.split("##");

				if (lineArr.length == 4) {
					query = lineArr[1];
					source = lineArr[0];
					queryParameters.put("page", new String[] { lineArr[2] });
					queryParameters.put("perPage", new String[] { lineArr[3] });
				} else {
					if (lineArr.length == 2) {
						query = lineArr[1];
						source = lineArr[0];
					} else if (lineArr.length == 1) {
						query = lineArr[0];
						source = "all";
					} else {
						continue;
					}
					if (IS_PAGINATED) {
						queryParameters.put("page", new String[] { "1" });
						queryParameters.put("perPage", new String[] { "20" });
					}
				}

				queryParameters.put("query", new String[] { query });
				queryParameters.put("src", new String[] { source });
				try {
					XCEngine.getSearchResultsJSON(queryParameters);
				} catch (Exception e) {
					// TODO : Print stacktrace as string using Throwable.printStackTrace(PrintWriter pw) 
					logger.error("Error occured in SpInteractive\n" + e.getMessage());
					e.printStackTrace();
				}
			} else {
				s.close();
				break;
			}
		}
	}

	private static void init() {
		XCEngine.init();
		logger = XCLogger.getSpLogger();
	}

}
