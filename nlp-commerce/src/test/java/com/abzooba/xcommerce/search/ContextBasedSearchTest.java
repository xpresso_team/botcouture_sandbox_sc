package com.abzooba.xcommerce.search;

import com.abzooba.xcommerce.core.XCEngine;
import com.abzooba.xcommerce.search.ContextBasedSearch;
import com.abzooba.xcommerce.search.SearchUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by mayank on 27/2/17.
 */
public class ContextBasedSearchTest {

    @Test
    public void getSearchResultsJSON() throws Exception {
        init();
        String query = "in 50$";
        Map<String, String[]> queryParameters = new HashMap<String, String[]>();
        if (query != null) {
            queryParameters.put(SearchUtils.PRICE_NUMERIC_KEY, new String[]{});
            queryParameters.put("query", new String[] { query });
            try {
                JSONObject contextBasedDirectIntentJSON = ContextBasedSearch.getContextBasedDirectIntentJSON(queryParameters);
                assertEquals(new JSONArray((new HashSet<String>(Arrays.asList("< 50")))),contextBasedDirectIntentJSON.get(SearchUtils.PRICE_NUMERIC_KEY));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        query = "in medium";
        queryParameters = new HashMap<String, String[]>();
        if (query != null) {
            queryParameters.put(SearchUtils.SIZE, new String[]{});
            queryParameters.put("query", new String[] { query });
            try {
                JSONObject contextBasedDirectIntentJSON = ContextBasedSearch.getContextBasedDirectIntentJSON(queryParameters);
                assertEquals(new JSONArray((new HashSet<String>(Arrays.asList("medium")))),contextBasedDirectIntentJSON.get(SearchUtils.SIZE));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        query = "adidas";
        queryParameters = new HashMap<String, String[]>();
        if (query != null) {
            queryParameters.put(SearchUtils.BRAND_KEY, new String[]{});
            queryParameters.put("query", new String[] { query });
            try {
                JSONObject contextBasedDirectIntentJSON = ContextBasedSearch.getContextBasedDirectIntentJSON(queryParameters);
                assertEquals(new JSONArray((new HashSet<String>(Arrays.asList("adidas")))),contextBasedDirectIntentJSON.get(SearchUtils.BRAND_KEY));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        query = "yellow one";
        queryParameters = new HashMap<String, String[]>();
        if (query != null) {
            queryParameters.put(SearchUtils.COLOR_KEY, new String[]{});
            queryParameters.put("query", new String[] { query });
            try {
                JSONObject contextBasedDirectIntentJSON = ContextBasedSearch.getContextBasedDirectIntentJSON(queryParameters);
                assertEquals(new JSONArray((new HashSet<String>(Arrays.asList("yellow")))),contextBasedDirectIntentJSON.get(SearchUtils.COLOR_KEY));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        query = "for my girlfriend";
        queryParameters = new HashMap<String, String[]>();
        if (query != null) {
            queryParameters.put(SearchUtils.GENDER_KEY, new String[]{});
            queryParameters.put("query", new String[] { query });
            try {
                JSONObject contextBasedDirectIntentJSON = ContextBasedSearch.getContextBasedDirectIntentJSON(queryParameters);
                assertEquals(new JSONArray((new HashSet<String>(Arrays.asList("women")))),contextBasedDirectIntentJSON.get(SearchUtils.GENDER_KEY));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void init() {
        XCEngine.init();
    }
}