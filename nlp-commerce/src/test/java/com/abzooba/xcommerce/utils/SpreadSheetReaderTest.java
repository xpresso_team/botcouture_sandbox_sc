package com.abzooba.xcommerce.utils;

import com.abzooba.xcommerce.core.XCLogger;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by mayank on 2/5/17.
 */
public class SpreadSheetReaderTest {
    @Test
    public void getTestData() throws Exception {
        XCLogger.init();
        List<List<Object>> values = SpreadSheetReader.getTestData("19RrLcLwVs43pBwm4uMTjYBunVhH5c1VboPyClxSVQDA", "With_H3-H4!A2:Y347");
        assertEquals(true,values.size() > 0);
    }
}