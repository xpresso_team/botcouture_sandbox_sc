package com.abzooba.xcommerce.nlp.attribute;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.abzooba.xcommerce.core.XCEngine;

/**
 * Created by mayank on 9/3/17.
 */
public class SearchByProductNameTest {
	@Test
	public void getMatchingProduct() throws Exception {
		XCEngine.init();
		assertEquals("adidas", SearchByProductName.getMatchingProduct("adidas shoe"));
		assertEquals("levi's", SearchByProductName.getMatchingProduct("levi's jeans"));
		assertEquals("puma", SearchByProductName.getMatchingProduct("shoes by Puma for my daughter"));
		assertEquals("parfois", SearchByProductName.getMatchingProduct("was thinking to buy a Handbag from parfois for my wife"));
		assertEquals("Forever21", SearchByProductName.getMatchingProduct("I want to buy a cute dress by Forever21"));
		assertEquals("anne cole", SearchByProductName.getMatchingProduct("just the ones by anne cole"));
		assertEquals("Michael kors", SearchByProductName.getMatchingProduct("Michael kors tops for daughter"));
		assertEquals("tommy hilfiger", SearchByProductName.getMatchingProduct("he only wears tommy hilfiger"));
		assertEquals("Park Avenue", SearchByProductName.getMatchingProduct("can you show me a shirt by Park Avenue for men"));
		assertEquals("Kalt", SearchByProductName.getMatchingProduct("I would like it in Kalt"));
		assertEquals("Kappa", SearchByProductName.getMatchingProduct("show me some warm jackets preferably by Kappa for men"));
		assertEquals("armour", SearchByProductName.getMatchingProduct("i am under armour fan"));
		assertEquals("ralph lauren", SearchByProductName.getMatchingProduct("i have heard that the ones by lauren ralph lauren are very good"));
		assertEquals("nine west", SearchByProductName.getMatchingProduct("can you show me caps for men by nine west?"));
		assertEquals("jockey", SearchByProductName.getMatchingProduct("i need boxers by jockey for men"));
		assertEquals("calvin klein", SearchByProductName.getMatchingProduct("just the ones by calvin klein"));
		assertEquals("levi's", SearchByProductName.getMatchingProduct("show me levi's denim skirts in red in size L under 1000$"));
		assertEquals("michael kors", SearchByProductName.getMatchingProduct("do you have the michael kors ones"));
		assertEquals("Kalt", SearchByProductName.getMatchingProduct("I would like it by Kalt in black"));

	}
}