package com.abzooba.xcommerce.nlp.attribute;

import com.abzooba.xcommerce.config.XCConfig;
import com.abzooba.xcommerce.core.XCLogger;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by mayank on 18/5/17.
 */
public class RedisBrandTest {
    @Test
    public void getBrands() throws Exception {
        XCConfig.init();
        XCLogger.init();
        RedisBrand redisBrand = new RedisBrand();
        assertEquals(true, redisBrand.getBrandList().size() > 0);
    }
}