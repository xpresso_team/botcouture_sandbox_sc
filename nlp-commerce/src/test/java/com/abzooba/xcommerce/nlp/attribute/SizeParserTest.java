package com.abzooba.xcommerce.nlp.attribute;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Test;

import com.abzooba.xcommerce.core.XCEngine;
import com.abzooba.xpresso.engine.core.XpText;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;

/**
 * Created by mayank on 20/2/17.
 */
public class SizeParserTest {
	@Test
	public void sizeDetectionModule() throws Exception {
		XCEngine.init();
		XpText xpText = new XpText("size should be 10");
		List<CoreMap> sentencesList = xpText.getSentences();
		for (CoreMap sentence : sentencesList) {
			SemanticGraph dependencyGraph = xpText.getDependencyGraph(sentence);
			Tree parseTree = xpText.getParseTree(sentence);
			HashMap<CoreLabel, Double> sizeMap = SizeParser.sizeDetectionModule(parseTree, dependencyGraph, sentence.toString());
			assertEquals(sizeMap.size() > 0, true);
			List<String> sizeStrings = new ArrayList<>();
			for (CoreLabel coreLabel : sizeMap.keySet()) {
				sizeStrings.add(coreLabel.word());
			}
			assertEquals(sizeStrings.contains("10"), true);
		}

		xpText = new XpText("in size m");
		sentencesList = xpText.getSentences();
		for (CoreMap sentence : sentencesList) {
			SemanticGraph dependencyGraph = xpText.getDependencyGraph(sentence);
			Tree parseTree = xpText.getParseTree(sentence);
			HashMap<CoreLabel, Double> sizeMap = SizeParser.sizeDetectionModule(parseTree, dependencyGraph, sentence.toString());
			assertEquals(sizeMap.size() > 0, true);
			List<String> sizeStrings = new ArrayList<>();
			for (CoreLabel coreLabel : sizeMap.keySet()) {
				sizeStrings.add(coreLabel.word());
			}
			assertEquals(sizeStrings.contains("m"), true);
		}

		xpText = new XpText("show me one in size medium");
		sentencesList = xpText.getSentences();
		for (CoreMap sentence : sentencesList) {
			SemanticGraph dependencyGraph = xpText.getDependencyGraph(sentence);
			Tree parseTree = xpText.getParseTree(sentence);
			HashMap<CoreLabel, Double> sizeMap = SizeParser.sizeDetectionModule(parseTree, dependencyGraph, sentence.toString());
			assertEquals(sizeMap.size() > 0, true);
			List<String> sizeStrings = new ArrayList<>();
			for (CoreLabel coreLabel : sizeMap.keySet()) {
				sizeStrings.add(coreLabel.word());
			}
			assertEquals(sizeStrings.contains("medium"), true);
		}

		xpText = new XpText("with size 10");
		sentencesList = xpText.getSentences();
		for (CoreMap sentence : sentencesList) {
			SemanticGraph dependencyGraph = xpText.getDependencyGraph(sentence);
			Tree parseTree = xpText.getParseTree(sentence);
			HashMap<CoreLabel, Double> sizeMap = SizeParser.sizeDetectionModule(parseTree, dependencyGraph, sentence.toString());
			assertEquals(sizeMap.size() > 0, true);
			List<String> sizeStrings = new ArrayList<>();
			for (CoreLabel coreLabel : sizeMap.keySet()) {
				sizeStrings.add(coreLabel.word());
			}
			assertEquals(sizeStrings.contains("10"), true);
		}

		xpText = new XpText("do you have a top for a small frame girl");
		sentencesList = xpText.getSentences();
		for (CoreMap sentence : sentencesList) {
			SemanticGraph dependencyGraph = xpText.getDependencyGraph(sentence);
			Tree parseTree = xpText.getParseTree(sentence);
			HashMap<CoreLabel, Double> sizeMap = SizeParser.sizeDetectionModule(parseTree, dependencyGraph, sentence.toString());
			assertEquals(sizeMap.size() > 0, true);
			List<String> sizeStrings = new ArrayList<>();
			for (CoreLabel coreLabel : sizeMap.keySet()) {
				sizeStrings.add(coreLabel.word());
			}
			assertEquals(sizeStrings.contains("small"), true);
		}

		xpText = new XpText("show me levi's denim skirts in red in size L under 1000$");
		sentencesList = xpText.getSentences();
		for (CoreMap sentence : sentencesList) {
			SemanticGraph dependencyGraph = xpText.getDependencyGraph(sentence);
			Tree parseTree = xpText.getParseTree(sentence);
			HashMap<CoreLabel, Double> sizeMap = SizeParser.sizeDetectionModule(parseTree, dependencyGraph, sentence.toString());
			assertEquals(sizeMap.size() > 0, true);
			List<String> sizeStrings = new ArrayList<>();
			for (CoreLabel coreLabel : sizeMap.keySet()) {
				sizeStrings.add(coreLabel.word());
			}
			assertEquals(sizeStrings.contains("l"), true);
		}

		xpText = new XpText("can you help me get a really nice stilettos in black in size 7?");
		sentencesList = xpText.getSentences();
		for (CoreMap sentence : sentencesList) {
			SemanticGraph dependencyGraph = xpText.getDependencyGraph(sentence);
			Tree parseTree = xpText.getParseTree(sentence);
			HashMap<CoreLabel, Double> sizeMap = SizeParser.sizeDetectionModule(parseTree, dependencyGraph, sentence.toString());
			assertEquals(sizeMap.size() > 0, true);
			List<String> sizeStrings = new ArrayList<>();
			for (CoreLabel coreLabel : sizeMap.keySet()) {
				sizeStrings.add(coreLabel.word());
			}
			assertEquals(sizeStrings.contains("7"), true);
		}

		xpText = new XpText("show some tank tops for women in black color in size M");
		sentencesList = xpText.getSentences();
		for (CoreMap sentence : sentencesList) {
			SemanticGraph dependencyGraph = xpText.getDependencyGraph(sentence);
			Tree parseTree = xpText.getParseTree(sentence);
			HashMap<CoreLabel, Double> sizeMap = SizeParser.sizeDetectionModule(parseTree, dependencyGraph, sentence.toString());
			assertEquals(sizeMap.size() > 0, true);
			List<String> sizeStrings = new ArrayList<>();
			for (CoreLabel coreLabel : sizeMap.keySet()) {
				sizeStrings.add(coreLabel.word());
			}
			assertEquals(sizeStrings.contains("m"), true);
		}

	}

}