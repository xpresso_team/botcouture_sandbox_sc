package com.abzooba.xcommerce.core;

import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by mayank on 4/4/17.
 */
public class IntentAPITest {
	@Test
	public void intentAPIForChatBot() throws Exception {
		XCEngine.init();

		String query = "need a kanjeevaram saree";
		JSONObject intentJson = getIntentAPIResults(query);
		assertEquals("kanjeevaram saree",intentJson.get("entity"));
		assertEquals("[\"saree\"]",intentJson.get("xc_category").toString());
		query = "need a red pagri";
		intentJson = getIntentAPIResults(query);
		assertEquals("pagri",intentJson.get("entity"));
		assertEquals("[\"ethnic hats\"]",intentJson.get("xc_category").toString());
		assertEquals("[\"red\"]",intentJson.get("colorsavailable").toString());

		query = "red ghagra choli";
		intentJson = getIntentAPIResults(query);
		//{"entity":"ghagra choli","price":[],"size":[],"xc_category":["ethnic suit"],"features":[],"brand":[],"details":[],"colorsavailable":["red"],"gender":["unisex","women"]}
		assertEquals("ghagra choli", intentJson.get("entity"));
		assertEquals("[\"ethnic suit\"]", intentJson.get("xc_category").toString());
		assertEquals("[\"red\"]", intentJson.get("colorsavailable").toString());
		assertEquals("[\"unisex\",\"women\"]", intentJson.get("gender").toString());

		query = "red shirt for men under $50";
		intentJson = getIntentAPIResults(query);
		assertEquals("shirt", intentJson.get("entity"));
		assertEquals("[\"< 50\"]", intentJson.get("price").toString());
		assertEquals("[\"shirt\"]", intentJson.get("xc_category").toString());
		assertEquals("[\"red\"]", intentJson.get("colorsavailable").toString());
		assertEquals("[\"unisex\",\"men\"]", intentJson.get("gender").toString());

		query = "blue jeans";
		intentJson = getIntentAPIResults(query);
		assertEquals("jeans", intentJson.get("entity"));
		assertEquals("[\"blue\"]", intentJson.get("colorsavailable").toString());

		query = "Show me a partywear dress for the upcoming new year party in blue color in small size";
		intentJson = getIntentAPIResults(query);
		//{"entity":"dress","price":[],"size":["small"],"xc_category":["dress"],"features":[],"brand":[],"details":["party","new upcoming"],"colorsavailable":["blue"],"gender":["unisex","women"]}
		assertEquals("dress", intentJson.get("entity"));
		assertEquals("[\"small\"]", intentJson.get("size").toString());
		assertEquals("[\"dress\"]", intentJson.get("xc_category").toString());
		assertEquals("[\"party\",\"new upcoming\"]", intentJson.get("details").toString());
		assertEquals("[\"blue\"]", intentJson.get("colorsavailable").toString());
		assertEquals("[\"unisex\",\"women\"]", intentJson.get("gender").toString());

		query = "I need some good beach shorts for the beach party next week";
		intentJson = getIntentAPIResults(query);
		assertEquals("shorts", intentJson.get("entity"));
		assertEquals("[\"shorts\"]", intentJson.get("xc_category").toString());
		assertEquals("[\"beach\",\"party\"]", intentJson.get("details").toString());

		query = "Show me pink bras in large";
		intentJson = getIntentAPIResults(query);
		assertEquals("bra", intentJson.get("entity"));
		assertEquals("[\"large\"]", intentJson.get("size").toString());
		assertEquals("[\"undergarments\"]", intentJson.get("xc_category").toString());
		assertEquals("[\"pink\"]", intentJson.get("colorsavailable").toString());
		assertEquals("[\"unisex\",\"women\"]", intentJson.get("gender").toString());

		query = "Show me some stylish purses to flaunt in a party";
		intentJson = getIntentAPIResults(query);
		assertEquals("purse", intentJson.get("entity"));
		assertEquals("[\"bag\"]", intentJson.get("xc_category").toString());
		assertEquals("[\"women\"]", intentJson.get("gender").toString());

		query = "I want sunglasses for my father between 200$ and 500$";
		//{"entity":"sunglass","price":["<> 200 500"],"size":[],"xc_category":["eyewear"],"features":[],"brand":[],"details":[],"colorsavailable":[],"gender":["unisex","men"]}
		intentJson = getIntentAPIResults(query);
		assertEquals("sunglass", intentJson.get("entity"));
		assertEquals("[\"<> 200 500\"]", intentJson.get("price").toString());
		assertEquals("[\"eyewear\"]", intentJson.get("xc_category").toString());
		assertEquals("[\"unisex\",\"men\"]", intentJson.get("gender").toString());

		query = "Please give me sports bra in black color small size for a cricket match";
		//{"entity":"bra","price":[],"size":["small"],"xc_category":["undergarments"],"features":[],"brand":[],"details":["sports","cricket match"],"colorsavailable":["black"],"gender":["unisex","women"]}
		intentJson = getIntentAPIResults(query);
		assertEquals("bra", intentJson.get("entity"));
		assertEquals("[\"small\"]", intentJson.get("size").toString());
		assertEquals("[\"undergarments\"]", intentJson.get("xc_category").toString());
		assertEquals("[\"sports\",\"cricket match\"]", intentJson.get("details").toString());
		assertEquals("[\"black\"]", intentJson.get("colorsavailable").toString());
		assertEquals("[\"unisex\",\"women\"]", intentJson.get("gender").toString());

		query = "Give me heels in white for a formal meeting";
		//{"entity":"heel","price":[],"size":[],"xc_category":["footwear"],"features":[],"brand":[],"details":["formal"],"colorsavailable":["white"],"gender":["unisex","women"]}
		intentJson = getIntentAPIResults(query);
		assertEquals("heel", intentJson.get("entity"));
		assertEquals("[\"footwear\"]", intentJson.get("xc_category").toString());
		assertEquals("[\"formal\"]", intentJson.get("details").toString());
		assertEquals("[\"white\"]", intentJson.get("colorsavailable").toString());

		query = "get me a yellow scarf under 85 bucks please";
		//{"entity":"scarf","price":["< 85"],"size":[],"xc_category":["scarf"],"features":[],"brand":[],"details":[],"colorsavailable":["yellow"],"gender":["unisex","women"]}
		intentJson = getIntentAPIResults(query);
		assertEquals("scarf", intentJson.get("entity"));
		assertEquals("[\"< 85\"]", intentJson.get("price").toString());
		assertEquals("[\"scarf\"]", intentJson.get("xc_category").toString());
		assertEquals("[\"yellow\"]", intentJson.get("colorsavailable").toString());
		assertEquals("[\"unisex\",\"women\"]", intentJson.get("gender").toString());

		query = "my brother wants cool looking polarized sunglasses.";
		//{"entity":"sunglass","price":[],"size":[],"xc_category":["eyewear"],"features":[],"brand":[],"details":["looking","polarized"],"colorsavailable":[],"gender":["men"]}
		intentJson = getIntentAPIResults(query);
		assertEquals("sunglass", intentJson.get("entity"));
		assertEquals("[\"eyewear\"]", intentJson.get("xc_category").toString());
		assertEquals("[\"looking\",\"polarized\"]", intentJson.get("details").toString());
		assertEquals("[\"men\"]", intentJson.get("gender").toString());

		query = "i need a good quality wristlet for about 70 dollars";
		//{"entity":"wristlet","price":["<> 63 77"],"size":[],"xc_category":["bag"],"features":[],"brand":[],"details":["quality"],"colorsavailable":[],"gender":[]}
		intentJson = getIntentAPIResults(query);
		assertEquals("wristlet", intentJson.get("entity"));
		assertEquals("[\"<> 63 77\"]", intentJson.get("price").toString());
		assertEquals("[\"bag\"]", intentJson.get("xc_category").toString());
		assertEquals("[\"quality\"]", intentJson.get("details").toString());

		query = "i want to buy a baseball cap, do have any?";
		//{"entity":"cap","price":[],"size":[],"xc_category":["caps and hats"],"features":[],"brand":[],"details":["baseball"],"colorsavailable":[],"gender":[]}
		intentJson = getIntentAPIResults(query);
		assertEquals("cap", intentJson.get("entity"));
		assertEquals("[\"caps and hats\"]", intentJson.get("xc_category").toString());
		assertEquals("[\"baseball\"]", intentJson.get("details").toString());

		query = "i need a pink camisole for my sister in size m";
		//{"entity":"camisole","price":[],"size":["m"],"xc_category":["undergarments"],"features":[],"brand":[],"details":[],"colorsavailable":["pink"],"gender":["unisex","women"]}
		intentJson = getIntentAPIResults(query);
		assertEquals("camisole", intentJson.get("entity"));
		assertEquals("[\"m\"]", intentJson.get("size").toString());
		assertEquals("[\"undergarments\"]", intentJson.get("xc_category").toString());
		assertEquals("[\"pink\"]", intentJson.get("colorsavailable").toString());
		assertEquals("[\"unisex\",\"women\"]", intentJson.get("gender").toString());

		query = "i need thongs above 20$";
		//{"entity":"thong","price":["> 20"],"size":[],"xc_category":["undergarments"],"features":[],"brand":[],"details":[],"colorsavailable":[],"gender":[]}
		intentJson = getIntentAPIResults(query);
		assertEquals("thong", intentJson.get("entity"));
		assertEquals("[\"> 20\"]", intentJson.get("price").toString());
		assertEquals("[\"undergarments\"]", intentJson.get("xc_category").toString());

	}

	private JSONObject getIntentAPIResults(String query) {
		IntentAPI objIntentAPI = new IntentAPI();
		Map<String, String[]> queryParameters = new HashMap<String, String[]>();
		queryParameters.put("query", new String[] { query });
		return objIntentAPI.intentAPIForChatBot(queryParameters);
	}
}