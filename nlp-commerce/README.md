# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
1. Download and install apache maven 3.3.9
2. Download and install git 2.7.0
3. Download and install mongoDB (3.0+)
4. Install the M2E plugin in eclipse  
   help-> install new software  
   url: http://download.eclipse.org/technology/m2e/releases
5. Start mongoDB server
6. Get the Synaptica codebase from Git  
   open terminal: git clone https://bitbucket.org/rndabzooba/synaptica  
   open eclipse: import synaptica folder into eclipse as existing maven project

### Run application ###
1. Set mongo.wv.load=false in both synapticaConfig.properties and xpressoConfig.properties 
2. Run SpInteractive as java application 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact