# README #

### REPOSITORY ###

* Analytics Dashboard for ChatBot
* Version : 1.4.0.rc.1

### SOFTWARE REQUIREMENT ###

* Requires node(>=6.0.0) and npm

### SET UP ###

* git clone <repo-url>
* cd analyticsdashboard
* npm install
* node forever.js environment (dev or prod) 

### MORE INFORMATION ###
Read the [Wiki](https://bitbucket.org/rndabzooba/analyticsdashboard/wiki/Home) for more details.