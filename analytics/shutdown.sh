#!/usr/bin/env bash
kill $(cat forever.pid)
rm -f forever.pid
kill $(cat server.pid)
rm -f server.pid