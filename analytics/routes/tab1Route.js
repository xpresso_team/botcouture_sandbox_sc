var express = require('express');
var router = express.Router();
var tab1 = require('../controllers/overview');

router.route('/getPieChartData').post(tab1.getPieChartData);
router.route('/getTotalUsers').post(tab1.getTotalUsers);
router.route('/getNewUsers').post(tab1.getNewUsers);
router.route('/getTotalSession').post(tab1.getTotalSession);
router.route('/getAvgSession').post(tab1.getAvgSession);
router.route('/getLineChartData').post(tab1.getLineChartData);

module.exports = router;