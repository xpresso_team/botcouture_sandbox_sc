/**
 * Created by rounak on 30/5/17.
 */
const Config = require('config-js');

module.exports = (function(env){
    var config;
    env=env.trim();
    switch(env){
        case 'dev':
            config = new Config('./config/dev/environment.js');
            break;
        case 'prod':
            config = new Config('./config/prod/environment.js');
            break;
        case 'itc':
            config = new Config('./config/itc/environment.js');
            break;
        case 'ust_doc':
            config = new Config('./config/ust_doc/environment.js');
            break;
        case 'vero_moda':
            config = new Config('./config/vero_moda/environment.js');
            break;
        case 'comcast_uat':
            config = new Config('./config/comcast_uat/environment.js');
            break;
        default:
            console.error('environment invalid');
            process.exit(1);
    }

    return config;
});
