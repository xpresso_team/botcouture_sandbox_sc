
/**
 * Created by abzooba on 20/6/17.
 */
const Sequelize = require('sequelize');
var configuration = require('./config/index.js')(process.argv[2]);

var sequelize = new Sequelize(configuration.get('database'), configuration.get('dbuser') , configuration.get('password'), {
    host: configuration.get('host'),
    port: configuration.get('port'),
    dialect: 'mysql'
});

sequelize
    .authenticate()
    .then(function (){
        console.log('Connection to ' + configuration.get('database') + ' hosted at ' + configuration.get('host')+':'+ configuration.get('port') +' has been established successfully.');
    }).catch(function (err){
    console.error('Unable to connect to the database:' + configuration.get('database') + ' hosted at ' + configuration.get('host')+':'+ configuration.get('port'), err);
});

module.exports = sequelize;
