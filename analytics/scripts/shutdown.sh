#!/usr/bin/env bash
cd ${ROOT_FOLDER}/app
kill $(cat forever.pid)
rm -f forever.pid
kill $(cat server.pid)
rm -f server.pid