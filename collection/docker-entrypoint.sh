#! /bin/bash
set -e
cd app

if [ "$1" = 'test' ]; then
    python main.py &
    sleep 5
    exec /collectionapi/integration_test.sh
elif [ "$1" = 'experimentl' ]; then
    python main.py
elif [ "$1" = 'production' ]; then
    python main.py
fi

exec "$@"
