"""
Handles level request.
Simply fetched uninque collections and trends from dynamodb
"""
import tornado.web
from AWSDynamoDB import dynamo_db
import config
import json


# Fetches all collection name for the given gender
class Level1Handler(tornado.web.RequestHandler):
    def get(self):
        gender = self.get_argument("gender", "women").lower()
        collection_type = self.get_argument("type", "collections")

        collection_name_list = dynamo_db.get_available_collection(gender=gender,
                                                                  collection_type=collection_type)

        collection_list = []
        COLLECTION_COUNT = {
          "men" : 0,
          "women" :0
        }
        # Assign images  to collections in round robin format
        for item in collection_name_list:
            name = item["name"]
            gender = item["gender"]
            key=gender+"-"+name
            if key not in config.COLLECTION_IMAGE:
                if gender in config.DEFAULT_IMAGE:
                   index = (COLLECTION_COUNT[gender])%len(config.DEFAULT_IMAGE[gender])
                   key=gender+"-"+name
                   config.COLLECTION_IMAGE[key] = config.DEFAULT_IMAGE[gender][index]
                   COLLECTION_COUNT[gender]+=1
                else:
                   config.COLLECTION_IMAGE[name] = config.DEFAULT_IMAGE["image"]
            collection_list.append({"image": config.COLLECTION_IMAGE[key],
                                    "name": name})

        resp = {
            "collection_list": collection_list,
            "status_code": 200,
            "status_msg": "OK"}
        self.write(json.dumps(resp))
