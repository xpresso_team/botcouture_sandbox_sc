"""
It fetches the available category list for a collection or trend
"""

import json

import tornado.web
import traceback
from AWSDynamoDB import dynamo_db

# Fetches the list of available category for a given collection or trend name
class Level2Handler(tornado.web.RequestHandler):
    def get(self):
        gender = self.get_argument("gender", "women").lower()
        category_list = []
        try:
            collection = self.get_argument("collections")
            category_list = dynamo_db.get_available_category(gender=gender,
                                                             curation=collection)
        except:
            traceback.print_exc()
            try:
                trend = self.get_argument("trends")
                category_list = dynamo_db.get_available_category(gender=gender,
                                                                 curation=trend)
            except:
                traceback.print_exc()
        print(category_list)
        resp = {
            "category_list": category_list,
            "status_code": 200,
            "status_msg": "OK"}
        self.write(json.dumps(resp))
