from setuptools import setup, find_packages

VERSION = '1.0.1'

setup(
    name='collectionapi',
    version=VERSION,
    description="Collection API to fetch unique collection available for men and women. This is needed to implement browsing on chatbot",
    long_description="""Collection API which fetches unique collection from dybamodb using boto3. This is used by chatbot to implement browsing functionality""",
    author='Naveen Sinha',
    author_email='naveen.sinha@abzooba.com',
    url='https://naveensinha@bitbucket.org/rndabzooba/collectionapi.git',
    download_url='https://naveensinha@bitbucket.org/rndabzooba/collectionapi.git',
    packages=find_packages(exclude=['ez_setup', 'testes']),
    include_package_data=True,
    zip_safe=True,
    install_requires=[
        'appdirs==1.4.0',
        'backports-abc==0.5',
        'backports.ssl-match-hostname==3.5.0.1',
        'certifi==2017.1.23',
        'docutils==0.13.1',
        'futures==3.0.5',
        'jmespath==0.9.1',
        'packaging==16.8',
        'pyparsing==2.1.10',
        'python-dateutil==2.6.0',
        'singledispatch==3.4.0.3',
        'six==1.10.0',
        'tornado==4.4.2'
    ],
    dependency_links=[],
)

