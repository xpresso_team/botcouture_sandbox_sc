import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

import config
from Level1Handler import Level1Handler
from Level2Handler import Level2Handler
from Level3Handler import Level3Handler


def main():
    tornado.options.parse_command_line()
    application = tornado.web.Application([
        (r"/l1/", Level1Handler),
        (r"/l2/", Level2Handler),
        (r"/l3/", Level3Handler),
    ])
    print( "starting server")
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(config.PORT)
    print("Started on port",config.PORT)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
