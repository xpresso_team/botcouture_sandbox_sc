PORT = 6100

L1_CURATION = {
    "men": {
      "trends":["new arrival", "dealoftheday","sale"],
      "collections":["comfortworkwear","ut-graphictshirts", "wedding","big tall"]
    },
    "women": {
      "trends":["newarrivals", "dealoftheday","sale"],
      "collections":["comfortworkwear","ut-graphictshirts","plus", "plus size", "wedding"]
    }
}

CATEGORY_LIST = {
    "men": {
        "sale": ["jeans", "suit","shirt"], "wedding": ["suit","shirt"],
        "new arrival": ["jeans", "shirt"], "bit tall": ["jeans", "suit","shirt"],
        "dealoftheday":["jeans", "trousers","shirt"],
        "comfortworkwear":["coats and jackets", "trousers","shirt"],
        "ut-graphictshirts":["t-shirt"]
    },
    "women": {
        "plus": ["top", "jeans", "scarf","t-shirt"],
        "plus size": ["top", "jeans", "scarf","t-shirt"],
        "new arrival": ["jeans", "t-shirt"],
        "wedding": ["dress"],
        "sale": ["scarf", "jeans", "shirt"],
        "newarrivals": ["coats and jackets", "dress", "top", "t-shirt", "trousers"],
        "dealoftheday":["dress", "top", "trousers","shirt"],
        "comfortworkwear":["shirt", "trousers"],
        "ut-graphictshirts":["t-shirt"]
    }
}

COLLECTION_IMAGE = {
    "women-plus": "https://scstylecaster.files.wordpress.com/2013/09/plus-size-fashion.png",
    "women-plus size": "https://scstylecaster.files.wordpress.com/2013/09/plus-size-fashion.png",
    "women-petite": "http://cdn.gurl.com/wp-content/uploads/2015/12/petitefashiontips13.jpg",
    "men-big tall": "http://oldnavy.gap.com/Asset_Archive/ONWeb/content/0009/426/201/assets/021715_US_EN_landingpage_vi_mtall.jpg",
    "women-new arrival": "http://priiincesss.com/wp-content/uploads/2015/07/new-arrivals4.jpg",
    "men-new arrival": "http://priiincesss.com/wp-content/uploads/2015/07/new-arrivals4.jpg",
    "women-wedding": "http://inspiredboy.com/uploads/201510/full/fresh-wedding-posters-1-801ee710cce0765af6047f942f8b1a6f.jpg",
    "men-wedding": "http://inspiredboy.com/uploads/201510/full/fresh-wedding-posters-1-801ee710cce0765af6047f942f8b1a6f.jpg",
    "women-sale": "https://s-media-cache-ak0.pinimg.com/736x/c9/6e/93/c96e934a52c65eacbcca9cd8ed15cc4e.jpg",
    "men-sale": "https://s-media-cache-ak0.pinimg.com/736x/c9/6e/93/c96e934a52c65eacbcca9cd8ed15cc4e.jpg"
}

DEFAULT_IMAGE = {
 "men" : ["http://i2.cdn.cnn.com/cnnnext/dam/assets/160613162533-london-mens-fashion-7-super-169.jpg","http://4.bp.blogspot.com/-Wlhcbrnp1ow/VnFIsIK3CdI/AAAAAAACFxI/paeSmgVyq2o/s1600/Yellow-Autumn-Winter-Outfits-2016-www.fashionhuntworld.blogspot.co%2Bm%2B%2B%25286%2529.jpg","https://s-media-cache-ak0.pinimg.com/originals/d7/a4/3f/d7a43f7ff578287e9542adc354fee584.jpg", "http://www.mensfashionmagazine.com/wp-content/uploads/2012/03/british-summer-mens-fashion.jpg"],
 "women" :["http://asset2.cxnmarksandspencer.com/is/image/mands/4b50d337a68a84706f74735db759cfe72a467a30?$GEO_LOCATE_1032x470$", "https://fashion360.pk/wp-content/uploads/2015/04/Outfitters-Spring-Summer-Wear-Collection-2015-for-Women-Men-8.jpg", "https://s-media-cache-ak0.pinimg.com/originals/74/e6/ca/74e6caf044d286e005997eb3a56ce98b.jpg"],
 "image": "http://www.heliummagazine.com/wp-content/uploads/2011/07/CollectionAdWide.jpg"
}

CATEGORY_KEY = "xc_category"
PRODUCT_COUNT = 30
# DyamoDB
COLLECTION_DB = "botcouturecollections1"
CATALOG_DB = "botcouturecatalog1"
#ELASTIC_HOST = "botcouture-sandbox--botcouture-elasticsearch.botcouture-sandbox"
#ELASTIC_PORT = '9200'
ELASTIC_HOST='172.16.6.51'
ELASTIC_PORT='31839'
ELASTIC_USER = 'elastic'
ELASTIC_PASSWORD = 'changeme'
ES_INDEX = 'botcouture'
DOC_TYPE= 'docket'
