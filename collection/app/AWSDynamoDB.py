"""
Dynamo DB related transactions happens herre
"""
import config
from elasticsearch import Elasticsearch


class AWSDynamoDB:
    _dynamo_DB = None
    _collection_table = None
    _catalog_table = None

    def __init__(self):
        self.elastic_host = config.ELASTIC_HOST
        self.elastic_port = config.ELASTIC_PORT
        self.elastic_user = config.ELASTIC_USER
        self.elastic_password = config.ELASTIC_PASSWORD
        self.index = config.ES_INDEX
        self.doc_type = config.DOC_TYPE
        self.es = Elasticsearch([{'host': self.elastic_host, 'port': self.elastic_port}], http_auth=(self.elastic_user, self.elastic_password))

    def get_possible_gender(self, gender_str):
        return_list = gender_str.split(',')
        return return_list


    def check_if_repeated(self, key, collection_list):
        ret = False
        for item in collection_list:
            if key == item["name"]:
                ret = True
                break
        return ret

    def get_available_collection(self, gender, collection_type):
        collection_list = []
        curation_list = config.L1_CURATION[gender][collection_type]
        for curation in curation_list:
            collection_list.append({"name": curation, "gender": gender})
        return collection_list


    def get_available_category(self, gender, curation):
        category_list = []
        if not curation in config.CATEGORY_LIST[gender]:
            category_list = ["jeans", "shirt"]
            return category_list
        category_list = config.CATEGORY_LIST[gender][curation]
        return category_list

    def get_available_product_list(self, gender, curation, category):
        product_list = []
        print( gender)
        print( curation)
        print( category)

        category_query = ''
        if isinstance(category, list):
            xc_category_list = ['xc_category:'+ t_category for t_category in category]
            for category in category:
                category_query = ' OR '.join(xc_category_list)
        elif isinstance(category, str):
            category_query = 'xc_category:'+ category

        gender_query = 'gender:'+ gender
        query_string = category_query + ' AND '+ gender_query + ' AND (' + 'collections:'+ str(curation)+' OR ' + 'trends:'+ str(curation)  + ')'

        query = {
            "query": {
                    "bool": {
                    "must": [{
                    "query_string": {
                            "query": query_string
                        }
                    }]
                }
            },
            "size": 10
        }
        print( query)
        search_results = self.es.search(index=self.index, body=query)
        if search_results:
            products = search_results['hits']['hits']
            for item in products:
                i = item['_source']
                product_list.append({
                    "image": i["image"],
                    "product_url": i["product_url"],
                    "gender": i["gender"],
                    "xc_sku": i['xc_sku'],
                    "_score": "0.0",
                    "client_id": i["client_id"],
                    "colorsavailable": list(i["colorsavailable"]) if "colorsavailable" in i else list(),
                    "size": list(i["size"]) if "size" in i else list(),
                    "price": float(i["price"]),
                    "productname": i["productname"],
                    "subcategory": list(i["subcategory"]),
                    "sku": i["sku"],
                    "client_name": i["client_name"],
                    "brand": i["brand"],
                    "confidence": "0.0",
                    "suggested": False,
                    "xc_category": list(i["xc_category"]),
                })
        return product_list 

dynamo_db = AWSDynamoDB()
