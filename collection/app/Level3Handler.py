"""
It fetches top 10 list of products from the dynamodb
"""
import json

import tornado.web

from AWSDynamoDB import dynamo_db

# Fetches a list of product for category, gender and collection/trends
class Level3Handler(tornado.web.RequestHandler):
    def get(self):
        gender = str(self.get_argument("gender", "women").lower())
        try:
            category = str(self.get_argument("category"))
        except:
            category = None

        try:
            curation = str(self.get_argument("collections"))
        except:
            try:
                curation = str(self.get_argument("trends"))
            except:
                curation = None

        product_list = dynamo_db.get_available_product_list(gender=gender,
                                                            curation=curation,
                                                            category=category)

        resp = {
            "product_list": product_list,
            "status_code": 200,
            "status_msg": "OK"}
        self.write(json.dumps(resp))
