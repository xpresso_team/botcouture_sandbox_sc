import json
import falcon


def parse(req):

    """
    Parse the json body as per the falcon request format
    """

    try:
        raw_json = req.stream.read()
        json_data = raw_json.decode('utf-8')
        json_body = json.loads(json_data, encoding='utf-8')
        return json_body
    except Exception:
        raise falcon.HTTPError(falcon.HTTP_400, 'Invalid JSON',
                               'Could not decode the request body. The JSON was incorrect.')
