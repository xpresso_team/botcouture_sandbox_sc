"""
Schema of json body which we get on post request
for generic resource
"""
schema = {
    "type": "object",
    "properties": {
        "is_personalized": {"type": "boolean"},
        "psid": {"type": "string"},
        "page_id": {"type": "string"},
        "channel_id": {"type": "string"},
        "intent": {"type": "object"},
        "qtype": {"type": "string"},
        "qcontext": {"type": "string"},
        "qmethod": {"type": "string"},
        "qparameter": {"type": "object"}
    }
}