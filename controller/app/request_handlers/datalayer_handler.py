__author__ = 'naveen'

import json
import falcon

from app.request_handlers.personalization_handler import re_rank
from app.relaxation.RelaxationManager import relaxation_manager
from app.request_handlers.external_request import perform_generic_redirect
from app.config import config


def check_if_reranking_is_required(json_resp, json_body):
    return bool("productDetails" in json_body["qparameter"] and
                json_body["qparameter"]["productDetails"] and
                "Results" in json_resp and "Results" in json_resp["Results"] and
                len(json_resp["Results"]["Results"]) > 0)


# TODO remove the need of it
def make_results_compatible(results):
    """
    Result format should be compatible downstream services
    TODO remove the need of this function
    """
    for result in results:
        if type(result) is list:
            result = str(result)
    return results


# Refactor query
def refactor_input(json_body):

    # add unisex if not present
    if "gender" in json_body["intent"] and "unisex" not in json_body["intent"]["gender"]:
       if type(json_body["intent"]["gender"]) is list:
           json_body["intent"]["gender"].append("unisex")
       else:
           json_body["intent"]["gender"] = [json_body["intent"]["gender"], "unisex"]

    if "gender" in json_body["qparameter"] and "unisex" not in json_body["qparameter"]["gender"]:
       if type(json_body["qparameter"]["gender"]) is list:
           json_body["qparameter"]["gender"].append("unisex")
       else:
           json_body["qparameter"]["gender"] = [json_body["qparameter"]["gender"], "unisex"]


def process(req, resp, json_body):
    """
    Process the datalayer based request.
    Query datalayer api, relax if needed and sort the result if required.
    """

    refactor_input(json_body)
    json_resp = {}
    if "generic" in json_body["qcontext"]:
        json_resp = perform_generic_redirect(base_uri=config["API"]["DATALAYER"],
                                             json_body=json_body)
        if check_if_reranking_is_required(json_resp=json_resp, json_body=json_body):
            json_resp["Results"]["Results"] = re_rank(results=json_resp["Results"]["Results"],
                                                      json_body=json_body)

    elif "structuredSearch" in json_body["qcontext"]:
        print(json_body)
        json_resp = relaxation_manager.process(search_uri=config["API"]["DATALAYER"],
                                               parameter_body=json_body)

        json_resp["Results"] = re_rank(results=json_resp["Results"],
                                       json_body=json_body)

    # Create a JSON representation of the resource
    resp.body = json.dumps(json_resp, ensure_ascii=True)

    # The following line can be omitted because 200 is the default
    # status returned by the framework, but it is included here to
    # illustrate how this may be overridden as needed.
    resp.status = falcon.HTTP_200
