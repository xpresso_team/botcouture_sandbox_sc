__author__ = 'naveen'

import json
import falcon
import logging

from app.request_handlers.external_request import perform_generic_redirect
from app.request_handlers.personalization_handler import re_rank
from app.filter.generic_filter import filter_results
from app.caching import cache_utils, cache_manager
from app.config import config


def process(req, resp, json_body):
    """
    Process the collection based request.
    Query collection api and sort the result if required.
    """

    # Check if filtration is required
    cached_results, hash_key = cache_utils.get_cached_external_request(json_body)
    if cached_results is None:
        json_resp = perform_generic_redirect(base_uri=config["API"]["COLLECTION"],
                                             json_body=json_body)
        cache_manager.insert_results_in_cache(hash_key, json_resp)

    else:
        logging.info("[CACHED]")
        json_resp = cached_results

    if "product_list" in json_resp:
        # Filter results
        logging.info(json_body["intent"])
        json_resp["product_list"] = filter_results(json_body["intent"], json_resp["product_list"])

        # Re-rank results
        json_resp["product_list"] = re_rank(results=json_resp["product_list"], json_body=json_body)

    # Create a JSON representation of the resource
    resp.body = json.dumps(json_resp, ensure_ascii=True)

    # The following line can be omitted because 200 is the default
    # status returned by the framework, but it is included here to
    # illustrate how this may be overridden as needed.
    resp.status = falcon.HTTP_200
