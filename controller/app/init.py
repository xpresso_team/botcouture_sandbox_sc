from app.config import config
from app.relaxation.RelaxationManager import relaxation_manager
from app.caching.cache_manager import setup_cache

"""
Initialization is done here
"""


def init():
    # Initialize relaxation manager
    relaxation_manager.init(config["RELAXATION"]["JSON_PATH"])

    # Initialize cache
    setup_cache(config["CACHE"]["SIZE"])


