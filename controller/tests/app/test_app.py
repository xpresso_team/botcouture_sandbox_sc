__author__ = 'naveen'

import falcon
from falcon import testing
import pytest
import json

from app.app import app


@pytest.fixture
def client():
    return testing.TestClient(app)


# pytest will inject the object returned by the "client" function
# as an additional parameter.
def test_datalayer_api(client):
    """
    Testing datalayer api
    :param client:
    :return:
    """

    expected_doc = {
        "xc_category": [
            "dress"
        ],
        "Results": {
            "size": [
                "xxx-small",
                "xx-small",
                "x-small",
                "small",
                "medium",
                "large",
                "x-large",
                "2x-large",
                "3x-large",
                "4x-large",
                "5x-large",
                "6x-large"
            ]
        },
        "msg": "ok",
        "gender": [
            "women"
        ],
        "status": "ok",
    }

    body = {
        "is_personalized": True,
        "psid": "1201923129",
        "page_id": "1829192112",
        "channel_id": "facebook",
        "intent": {
            "xc_category": "bag",
            "gender": "men",
            "price": "< 10"
        },
        "qtype": "datalayer",
        "qcontext": "/genericAPI/",
        "qmethod": "GET",
        "qparameter": {
            "gender": "women",
            "returnField": "size",
            "xc_category": "dress"
        }
    }
    response = client.simulate_post('/v1/generic/',
                                    body=json.dumps(body),
                                    headers={'content-type': 'application/json'})

    resp_doc = response.content.decode('utf-8')
    result_doc = json.loads(resp_doc)

    assert response.status == falcon.HTTP_OK
    assert all(item in result_doc.items() for item in expected_doc.items())


# pytest will inject the object returned by the "client" function
# as an additional parameter.
def test_nlp_structured_api(client):
    """
    Testing structured search API of NLP
    :param client:
    :return:
    """
    body = {
        "is_personalized": True,
        "psid": "1201923129",
        "page_id": "1829192112",
        "channel_id": "facebook",
        "intent": {
            "xc_category": "bag",
            "gender": "men",
            "price": "< 10",
            "entity":"bag"
        },
        "qtype": "nlp",
        "qcontext": "/nlpstgstructuredSearch/",
        "qmethod": "GET",
        "qparameter": {
            "details": "women",
            "entity": "dress",
            "xc_category": "dress"
        }
    }

    response = client.simulate_post('/v1/generic/',
                                    body=json.dumps(body),
                                    headers={'content-type': 'application/json'})

    resp_doc = response.content.decode('utf-8')
    result_doc = json.loads(resp_doc)

    assert response.status == falcon.HTTP_OK
    assert len(result_doc['Results']) > 100


# pytest will inject the object returned by the "client" function
# as an additional parameter.
def test_nlp_intent_api(client):
    """
    Testing intent API of NLP
    :param client:
    :return:
    """
    body = {
        "is_personalized": True,
        "psid": "1201923129",
        "page_id": "1829192112",
        "channel_id": "facebook",
        "intent": {
            "xc_category": "bag",
            "gender": "men",
            "price": "< 10"
        },
        "qtype": "nlp",
        "qcontext": "/nlpstgintent/",
        "qmethod": "GET",
        "qparameter": {
            "query": "women shirt"
        }
    }

    response = client.simulate_post('/v1/generic/',
                                    body=json.dumps(body),
                                    headers={'content-type': 'application/json'})

    resp_doc = response.content.decode('utf-8')
    result_doc = json.loads(resp_doc)

    assert response.status == falcon.HTTP_OK
    assert result_doc['entity'] == "shirt"
    assert "women" in result_doc["gender"]
    assert "shirts" in result_doc["xc_hierarchy_str"]
    assert "shirt" in result_doc["xc_category"]


# pytest will inject the object returned by the "client" function
# as an additional parameter.
def test_collection_l1_API(client):
    """
    Testing collection L1 API of NLP
    :param client:
    :return:
    """
    body = {
        "is_personalized": True,
        "psid": "1201923129",
        "page_id": "1829192112",
        "channel_id": "facebook",
        "intent": {
            "xc_category": "bag",
            "gender": "men",
            "price": "< 10"
        },
        "qtype": "collection",
        "qcontext": "/l1/",
        "qmethod": "GET",
        "qparameter": {
            "gender": "women",
            "type": "collections"
        }
    }

    response = client.simulate_post('/v1/generic/',
                                    body=json.dumps(body),
                                    headers={'content-type': 'application/json'})

    resp_doc = response.content.decode('utf-8')
    result_doc = json.loads(resp_doc)

    assert response.status == falcon.HTTP_OK
    assert len(result_doc['collection_list']) > 0


# pytest will inject the object returned by the "client" function
# as an additional parameter.
def test_collection_l2_API(client):
    """
    Testing collection L2 API of NLP
    :param client:
    :return:
    """
    body = {
        "is_personalized": True,
        "psid": "1201923129",
        "page_id": "1829192112",
        "channel_id": "facebook",
        "intent": {
            "xc_category": "bag",
            "gender": "men",
            "price": "< 10"
        },
        "qtype": "collection",
        "qcontext": "/l2/",
        "qmethod": "GET",
        "qparameter": {
            "gender": "women",
            "collections": "cocktail"
        }
    }

    response = client.simulate_post('/v1/generic/',
                                    body=json.dumps(body),
                                    headers={'content-type': 'application/json'})

    resp_doc = response.content.decode('utf-8')
    result_doc = json.loads(resp_doc)

    assert response.status == falcon.HTTP_OK
    assert len(result_doc['category_list']) > 0


# pytest will inject the object returned by the "client" function
# as an additional parameter.
def test_collection_l3_API(client):
    """
    Testing collection L3 API of NLP
    :param client:
    :return:
    """
    body = {
        "is_personalized": True,
        "psid": "1201923129",
        "page_id": "1829192112",
        "channel_id": "facebook",
        "intent": {
            "xc_category": "bag",
            "gender": "men",
            "price": "< 10"
        },
        "qtype": "collection",
        "qcontext": "/l3/",
        "qmethod": "GET",
        "qparameter": {
            "gender": "women",
            "collections": "cocktail",
            "category": "bag"
        }
    }

    response = client.simulate_post('/v1/generic/',
                                    body=json.dumps(body),
                                    headers={'content-type': 'application/json'})

    resp_doc = response.content.decode('utf-8')
    result_doc = json.loads(resp_doc)

    assert response.status == falcon.HTTP_OK
    assert len(result_doc['product_list']) > 0


def test_emotion_API(client):
    """
    Testing emotion API of NLP
    :param client:
    :return:
    """
    body = {
        "is_personalized": True,
        "psid": "1201923129",
        "page_id": "1829192112",
        "channel_id": "facebook",
        "intent": {
            "xc_category": "bag",
            "gender": "men",
            "price": "< 10"
        },
        "qtype": "emotion",
        "qcontext": "",
        "qmethod": "GET",
        "qparameter": {
            "query": "hi"
        }
    }

    response = client.simulate_post('/v1/generic/',
                                    body=json.dumps(body),
                                    headers={'content-type': 'application/json'})

    resp_doc = response.content.decode('utf-8')
    result_doc = json.loads(resp_doc)

    assert response.status == falcon.HTTP_OK
    assert "entities" in result_doc
