#! /bin/bash

gunicorn app --env CONTROLLER_SERVICE_CONFIG_FILE=./config/stg.json --reload \
  --workers 1 \
  --worker-class gthread \
  --threads 1 \
  --bind 0.0.0.0:8040

